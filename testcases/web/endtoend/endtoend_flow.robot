*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers
Suite Teardown    Close All Browsers

*** Keyword ***
Login to CDS by member and remove product in shopping cart
    [Arguments]     ${number}=${EMPTY}
    Run Keyword And Ignore Error    Remove customer cart and generate new cart    ${e2e_username_${number}}    ${e2e_password_${number}}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success   ${e2e_username_${number}}  ${e2e_password_${number}}  ${e2e_information_${number}.firstname}
    Wait Until Page Is Completely Loaded

Test setup access system by member type
    [Arguments]    ${member_type}    ${number}=${EMPTY}
    Run Keyword If  '${member_type}' == 'personal_member'   Login to CDS by member and remove product in shopping cart    ${number}
    ...    ELSE IF    '${member_type}' == 'guest'  common_cds_web.Setup - Open browser
    Set Test Variable   ${member_type}  ${member_type}

Pay by credit card full payment
    checkout_page.Select Payment Method With Credit Card
    Order successful with credit card

###### standard delivery #####
Select standard shipping and go to payment page
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Run Keyword If  '${member_type}' == 'guest'   Input customer details
    checkout_page.Select shipping to address option
    Run Keyword If  '${member_type}' == 'guest'   Input shipping details
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed

Select COD payment method
    checkout_page.Select Payment Method COD
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For COD Payment Method
    Wait Until Page Loader Is Not Visible

Select standard shipping request tax invoice and go to payment page
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Run Keyword If  '${member_type}' == 'guest'   Input customer details
    checkout_page.Select shipping to address option
    Run Keyword If  '${member_type}' == 'guest'   Input shipping details
    Wait Until Page Loader Is Not Visible
    delivery_details_page.Click request tax invoice
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed

##### Express delivery
select next-day delivery and go to payment page
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Run Keyword If    '${member_type}' == 'guest_member'    Input customer details
    checkout_page.Select shipping to address option
    Run Keyword If    '${member_type}' == 'guest_member'    Input shipping details
    Run Keyword If    '${member_type}' == 'guest_member'    Click on standard delivery button
    Run Keyword If    '${member_type}' == 'personal_member'    checkout_page.Select default address
    Run Keyword If    '${member_type}' == 'personal_member'    Click on Next-day delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible

##### Click collect ######
Select pick up at store and go to payment page
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Wait Until Page Loader Is Not Visible
    Select pick at store    ${central_store.central_bangna}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    
*** Test Cases ***
### standard ####
E2E01_Order product with cart price rule subtotal+fixed discount, paid via COD
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : ${coupon.five_bath} - CDS: Rule 215 CDS11133773 - RBS: Rule 5087 RBS20265274
    ...                - *Delivery type*    : Standard delivery
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    [Tags]    e2e_flow    testrailid:C302964    testrailid:C273742    e2e1
    Test setup access system by member type    ${personal_member}    01
    common_cds_web.Search product, add product to cart, go to checkout page    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    my_cart_page.Apply coupon code  ${coupon.five_bath}
    my_cart_page.Verify coupon code display in cart    ${coupon.five_bath}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${coupon.five_bath}    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}   ${5.0}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_keywords.Select Same day or Next day delivery shipping method
    checkout_page.Click Continue Payment Button
    Select COD payment method
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}    ${discount_amount}
    common_cds_web.Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product    ${increment_id}    ${cart_price_rule_sku.apply_coupon['5BAHT']}

E2E02_Order product with cart price rule percent discount+Freebie, paid via 123 bank transfer with T1 redeem
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : ${coupon.coupon10} - CDS Rule 213 CDS12063437 - RBS Rule 5088 RBS23927858
    ...                - *Product freebie*  : CDS Rule 211 (QAAT-buy CDS11008187 free CDS10774373) - RBS Rule 5089 (QAAT-buy RBS37753641 free RBS22058096)
    ...                - *Delivery type*    : Standard delivery
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Bank transfer
    [Tags]    e2e_flow    testrailid:C302965    e2e1
    Test setup access system by member type    ${personal_member}    01
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    common_cds_web.Search product, add product to cart, go to checkout page     ${cart_price_rule_sku.non_coupon['freebie']}[buy_item]
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${cart_price_rule_sku.apply_coupon['COUPON10']}    ${cart_price_rule_sku.non_coupon['freebie']}[buy_item]
    my_cart_page.Apply coupon code    ${coupon.coupon10}
    my_cart_page.Verify e-coupon discount in cart page(%)   ${coupon.coupon10}        ${${cart_price_rule_sku.apply_coupon['COUPON10']}.price}   ${10.0}
    my_cart_page.Verify free gift product in cart page    ${${cart_price_rule_sku.non_coupon['freebie']}[free_item]}[name]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_keywords.Select Same day or Next day delivery shipping method
    checkout_page.Click Continue Payment Button
    checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${${50}+${discount_amount}}    ${5}
    common_cds_web.Verify order status MCOM 'ONHOLD', order status reason MDC 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}
    payment_validated.Send 2c2p payment validated request    ${increment_id}
    common_cds_web.Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product    ${increment_id}    ${cart_price_rule_sku.apply_coupon['COUPON10']}    ${cart_price_rule_sku.non_coupon['freebie']}[buy_item]    ${cart_price_rule_sku.non_coupon['freebie']}[free_item]

E2E03_Order product, paid via T1 Full redeem (Zero pay)
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : No
    ...                - *Delivery type*    : Pick up at store
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Full redeem point
    [Tags]    e2e_flow    testrailid:C302966    e2e1    full_redeem
    Test setup access system by member type    ${personal_member}    01
    common_cds_web.Search product, add product to cart, go to checkout page    ${non_cart_price_rule_sku.zero_pay}
    my_cart_page.Click Secure Checkout Button
    Select pick at store    ${pick_at_store.central_bangna}
    checkout_page.Click Continue Payment Button
    checkout_keywords.Full redeem point and verify T1C point discount    ${t1c_information_4.email}    ${t1c_information_4.password}
    checkout_page.Click continue payment for full redeemtion
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_redeem_point}
    checkout_page.Verify order total in thank you page    ${0}
    common_cds_web.Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product    ${increment_id}    ${non_cart_price_rule_sku.zero_pay}

E2E04_Order product with cart price rule subtotal + percent discount, piad via Credit card with T1 redeem
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : ${coupon.coupon10} - CDS Rule 213 CDS12063437 - RBS Rule 5088 RBS23927858
    ...                - *Delivery type*    : Standard delivery
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Credit card full payment
    [Tags]    e2e_flow    testrailid:C302967    e2e1
    Test setup access system by member type    ${personal_member}    01
    common_cds_web.Search product and add product to cart    ${CDS11127628.sku}
    common_cds_web.Search product, add product to cart, go to checkout page     ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${CDS11127628.sku}    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Apply coupon code    ${coupon.coupon10}
    my_cart_page.Verify e-coupon discount in cart page(%)   ${coupon.coupon10}    ${${cart_price_rule_sku.apply_coupon['COUPON10']}.price}    ${10.0}
    Select standard shipping and go to payment page
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_page.Select Payment Method With Credit Card
    checkout_keywords.Order successful with credit card
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${${50}+${discount_amount}}    ${0}
    common_cds_web.Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product    ${increment_id}    ${CDS11127628.sku}    ${cart_price_rule_sku.apply_coupon['COUPON10']}

E2E05_Order product with mix products (own delivery type) + cart price rule, paid via credit card on top with T1C redeem + request full tax invoice
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : ${coupon.five_bath} - CDS: Rule 215 CDS11133773 - RBS: Rule 5087 RBS20265274
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : Yes
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Cerdit card full payment
    [Tags]    e2e_flow    testrailid:C349694    mix_products    e2e3
    Test setup access system by member type    ${personal_member}    03
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['5BAHT']}    # Retail Product
    common_cds_web.Search product, add product to cart, go to checkout page    ${MKP0183257.sku}    # Marketplace Product
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${cart_price_rule_sku.apply_coupon['5BAHT']}   ${MKP0183257.sku}
    my_cart_page.Apply coupon code  ${coupon.five_bath}
    my_cart_page.Verify coupon code display in cart    ${coupon.five_bath}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${coupon.five_bath}    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}   ${5.0}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    delivery_details_page.Click request tax invoice
    checkout_page.Click Continue Payment Button
    # checkout_keywords.Redeem point and verify T1C point discount    ${80}   ${10}
    checkout_page.Select Payment Method With Credit Card
    checkout_keywords.Order successful with credit card
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${discount_amount}    ${0}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}

E2E06_Order product with mix products (3PL type) + cart price rule + free item, paid via COD + request full tax invoice
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : Rule id 1052 - QAAT - Buy CDS10395578 Apply coupon QAATfreeitem get freeitem
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : Yes
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    [Tags]    e2e_flow    testrailid:C349695    mix_products    e2e3
    Test setup access system by member type    ${personal_member}    03
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['QAATfreeitem']}[buy_item]    # Retail Product
    common_cds_web.Search product and add product to cart    ${non_cart_price_rule_sku.cod}    # Retail Product
    common_cds_web.Search product and add product to cart    ${non_cart_price_rule_sku.marketplace}    # Marketplace Product
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${cart_price_rule_sku.apply_coupon['QAATfreeitem']}[buy_item]    ${non_cart_price_rule_sku.cod}   ${non_cart_price_rule_sku.marketplace}
    my_cart_page.Apply coupon code  ${coupon.QAATfreeitem}
    my_cart_page.Verify coupon code display in cart    ${coupon.QAATfreeitem}
    my_cart_page.Verify free gift product in cart page    ${${cart_price_rule_sku.apply_coupon['QAATfreeitem']}[free_item]}[name]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    delivery_details_page.Click request tax invoice
    checkout_page.Click Continue Payment Button
    Select COD payment method
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${50}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}
######################

### Express delivery ###
E2E07_Order product with cart price rule subtotal+ percent discount, paid via Bank transfer with T1 redeem
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : ${coupon.coupon10} - CDS Rule 213 CDS12063437 - RBS Rule 5088 RBS23927858
    ...                - *Delivery type*    : Express shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Bank transfer
    [Tags]    e2e_flow    testrailid:C302972    e2e4
    Test setup access system by member type    ${personal_member}    04
    common_cds_web.Search product and add product to cart    ${CDS11127628.sku}
    common_cds_web.Search product, add product to cart, go to checkout page    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${CDS11127628.sku}   ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Apply coupon code    ${coupon.coupon10}
    my_cart_page.Verify coupon code display in cart    ${coupon.coupon10}
    my_cart_page.Verify e-coupon discount in cart page(%)   ${coupon.coupon10}    ${${cart_price_rule_sku.apply_coupon['COUPON10']}.price}   ${10.0}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_keywords.Select Same day or Next day delivery shipping method
    checkout_page.Click Continue Payment Button
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${${50}+${discount_amount}}    ${0}   ${150}
    common_cds_web.Verify order status MCOM 'ONHOLD', order status reason MDC 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}
    payment_validated.Send 2c2p payment validated request    ${increment_id}
    common_cds_web.Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product    ${increment_id}    ${CDS11127628.sku}    ${cart_price_rule_sku.apply_coupon['COUPON10']}

E2E08_Order product with credit cart on-top +percent discount, paid via credit card full payment with T1 redeem
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : ${coupon.coupon10} - CDS Rule 213 CDS12063437 - RBS Rule 5088 RBS23927858
    ...                - *Delivery type*    : Express shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Credit card full payment
    [Tags]    e2e_flow    testrailid:C302973    e2e4
    Test setup access system by member type    ${personal_member}    04
    common_cds_web.Search product and add product to cart    ${CDS10395578.sku}
    common_cds_web.Search product, add product to cart, go to checkout page    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${CDS10395578.sku}    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Apply coupon code    ${coupon.coupon10}
    my_cart_page.Verify coupon code display in cart    ${coupon.coupon10}
    my_cart_page.Verify e-coupon discount in cart page(%)    ${coupon.coupon10}    ${CDS12063437.price}   ${10.0}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_keywords.Select Same day or Next day delivery shipping method
    checkout_page.Click Continue Payment Button
    # checkout_keywords.Redeem point and verify T1C point discount    ${80}   ${10}
    checkout_page.Select Payment Method With Credit Card
    checkout_keywords.Order successful with credit card
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${${50}+${discount_amount}}    ${0}    ${150}
    common_cds_web.Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product    ${increment_id}    ${CDS10395578.sku}    ${cart_price_rule_sku.apply_coupon['COUPON10']}

E2E10_Order product with cart price rule subtotal+fixed discount, paid via 2C2P Installment
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : No
    ...                - *Delivery type*    : Express shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Credit card Installment
    [Tags]    e2e_flow    testrailid:C303115    e2e5
    Test setup access system by member type    ${personal_member}    05
    common_cds_web.Search product, add product to cart, go to checkout page    ${CDS17397872.sku}
    my_cart_page.Click Secure Checkout Button
    select next-day delivery and go to payment page
    Checkout_keywords.Order successful with credit card installment    ${credit_card_installment.bank.bangkok_bank}    ${credit_card_installment.plan.six_months}
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    price=${CDS17397872.price}    discount=${50}    shipping=${150}
    common_cds_web.Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product    ${increment_id}    ${CDS17397872.sku}

E2E011_Order product with cart price rule fixed Coupon, paid via Credit card with T1 redeem
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : ${coupon.five_bath} - CDS: Rule 215 CDS11133773 - RBS: Rule 5087 RBS20265274
    ...                - *Delivery type*    : Pickup at store
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Credit card full payment
    [Tags]    e2e_flow    testrailid:C302969    e2e6
    Test setup access system by member type    ${personal_member}    06
    common_cds_web.Search product, add product to cart, go to checkout page    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    my_cart_page.Apply coupon code  ${coupon.five_bath}
    my_cart_page.Verify coupon code display in cart    ${coupon.five_bath}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${coupon.five_bath}    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}   ${5.0}
    Select pick up at store and go to payment page
    # checkout_keywords.Redeem point and verify T1C point discount    ${80}   ${10}
    Pay by credit card full payment
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}    ${discount_amount}    ${0}
    common_cds_web.Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product    ${increment_id}    ${cart_price_rule_sku.apply_coupon['5BAHT']}

E2E012_Order product with cart price rule bundle, paid via 123 bank transfer
    [Documentation]    - *Auto coupon*      : Rule 417 (QAAT -bundle pay ฿750 for CDS11133773, CDS11215721)
    ...                - *Apply coupon*     : No
    ...                - *Delivery type*    : Pickup at store
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Bank transfer
    [Tags]    e2e_flow    testrailid:C302970    e2e6
    Test setup access system by member type    ${personal_member}    06
    common_cds_web.Search product and add product to cart    ${CDS11133773.sku}
    common_cds_web.Search product, add product to cart, go to checkout page    ${CDS11215721.sku}
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${CDS11133773.sku}   ${CDS11215721.sku}
    Select standard shipping and go to payment page
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${750}
    common_cds_web.Verify order status MCOM 'ONHOLD', order status reason MDC 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}
    payment_validated.Send 2c2p payment validated request    ${increment_id}
    common_cds_web.Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product    ${increment_id}    ${CDS11133773.sku}    ${CDS11215721.sku}
# #########################

# #### Cancel order #####
E2E013_Partially cancel order with cart price rule subtotal+ percent discount, paid via COD
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : ${coupon.coupon10} - CDS Rule 213 CDS12063437 - RBS Rule 5088 RBS23927858
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    [Tags]    e2e_flow    testrailid:C302976    testrailid:C504266    testrailid:C504267    e2e7
    Test setup access system by member type    ${personal_member}    07
    common_cds_web.Search product and add product to cart    ${CDS10395578.sku}
    common_cds_web.Search product, add product to cart, go to checkout page    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${CDS10395578.sku}    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Apply coupon code    ${coupon.coupon10}
    my_cart_page.Verify e-coupon discount in cart page(%)   ${coupon.coupon10}    ${${product_sku_list}[4]}[price]   ${10.0}
    Select standard shipping and go to payment page
    Select COD payment method
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${${50}+${discount_amount}}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    common_cds_web.Retry verify that order create on WMS success    ${increment_id}
    force_shipment.Cancel item and partial force shipment via MCOM by sku number    ${increment_id}    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    common_cds_web.Verify order status MCOM 'COMPLETE, PARTIALLYSHIPPED', MDC 'COMPLETE, PARTIALLYSHIPPED'    ${increment_id}
    
E2E014_Partially cancel order (same SKU) with cart price rule subtotal+ percent discount, paid via credit card with T1 redeem
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : ${coupon.coupon10} - CDS Rule 213 CDS12063437 - RBS Rule 5088 RBS23927858
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Credit card full payment
    [Tags]    e2e_flow    testrailid:C302975    e2e7
    Test setup access system by member type    ${personal_member}    07
    common_cds_web.Search product and add product to cart    ${CDS10395578.sku}
    common_cds_web.Search product, add product to cart, go to checkout page    ${cart_price_rule_sku.apply_coupon['COUPON10']}    ${3}
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${CDS10395578.sku}    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Apply coupon code  ${coupon.coupon10}
    my_cart_page.Verify coupon code display in cart    ${coupon.coupon10}
    my_cart_page.Verify e-coupon discount in cart page(%)    ${coupon.coupon10}    ${${cart_price_rule_sku.apply_coupon['COUPON10']}.price}   ${30.0}
    Select standard shipping and go to payment page
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    Pay by credit card full payment
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${${50}+${discount_amount}}  ${0}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    common_cds_web.Retry verify that order create on WMS success    ${increment_id}
    force_shipment.Cancel item and partial force shipment via MCOM by sku number and qty    ${increment_id}    ${cart_price_rule_sku.apply_coupon['COUPON10']}    ${2}
    force_shipment.Fully force shipment via MCOM by order number    ${increment_id}
    common_cds_web.Verify order status MCOM 'COMPLETE, PARTIALLYSHIPPED', MDC 'COMPLETE, PARTIALLYSHIPPED'    ${increment_id}
    fms_db.Verify that order number exist in FMS DB    ${increment_id}
    #### Disable compare in kibana and FMS because the db of FMS still the issue data not update
    #Compare tax, discount, total amount for whole cart on kibana log with FMS db    ${increment_id}
    #### tax calculation by line item on mdc and fms db not equal
    # Compare tax, discount, total amount by line item for multiple sku on kibana log with FMS db    ${increment_id}    ${product_sku_list}[4]    ${product_sku_list}[30]

E2E015_Fully cancel order with cart price rule subtotal+ percent discount, paid via Credit Card
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : ${coupon.coupon10} - CDS Rule 213 CDS12063437 - RBS Rule 5088 RBS23927858
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Credit card full payment
    [Tags]    e2e_flow    testrailid:C302975    testrailid:C307761    testrailid:C273776    e2e8
    Test setup access system by member type    ${personal_member}    08
    common_cds_web.Search product and add product to cart    ${CDS10395578.sku}
    common_cds_web.Search product, add product to cart, go to checkout page    ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${CDS10395578.sku}   ${cart_price_rule_sku.apply_coupon['COUPON10']}
    my_cart_page.Apply coupon code    ${coupon.coupon10}
    my_cart_page.Verify e-coupon discount in cart page(%)   ${coupon.coupon10}    ${${cart_price_rule_sku.apply_coupon['COUPON10']}.price}   ${10.0}
    Select standard shipping and go to payment page
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    Pay by credit card full payment
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${${50}+${discount_amount}}  ${0}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    common_cds_web.Retry verify that order create on WMS success    ${increment_id}
    force_shipment.Cancel item and partial force shipment via MCOM by sku number and qty    ${increment_id}    ${CDS10395578.sku}    ${1}
    force_shipment.Cancel item and partial force shipment via MCOM by sku number and qty    ${increment_id}    ${cart_price_rule_sku.apply_coupon['COUPON10']}    ${1}
    common_cds_web.Verify order status MCOM 'COMPLETE, FULLYCANCELLED', MDC 'COMPLETE, FULLYCANCELLED'    ${increment_id}
    fms_db.Verify that order number exist in FMS DB    ${increment_id}
    #### Disable compare in kibana and FMS because the db of FMS still the issue data not update
    #Compare tax, discount, total amount for whole cart on kibana log with FMS db    ${increment_id}
    #### tax calculation by line item on mdc and fms db not equal
    # Compare tax, discount, total amount by line item for multiple sku on kibana log with FMS db    ${increment_id}    ${product_sku_list}[4]    ${product_sku_list}[30]

E2E016_Fully cancel order (difference SKU) with cart price rule percent coupon, paid via credit card full payment with T1 redeem
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : ${coupon.coupon10} - CDS Rule 213 CDS12063437 - RBS Rule 5088 RBS23927858
    ...                - *Delivery type*    : Pickup at store
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Credit card full payment
    [Tags]    e2e_flow    testrailid:C302968    e2e8
    Test setup access system by member type    ${personal_member}    08
    common_cds_web.Search product and add product to cart    ${CDS10395578.sku}
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['COUPON10']}    ${2}
    common_cds_web.Search product, add product to cart, go to checkout page    ${CDS11133773.sku}
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${CDS10395578.sku}    ${cart_price_rule_sku.apply_coupon['COUPON10']}    ${CDS11133773.sku}
    my_cart_page.Apply coupon code    ${coupon.coupon10}
    my_cart_page.Verify e-coupon discount in cart page(%)    ${coupon.coupon10}    ${${cart_price_rule_sku.apply_coupon['COUPON10']}.price}    ${20.0}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_keywords.Select Same day or Next day delivery shipping method
    checkout_page.Click Continue Payment Button
    # checkout_keywords.Redeem point and verify T1C point discount    ${80}   ${10}
    Pay by credit card full payment
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${${50}+${discount_amount}}  ${0}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    common_cds_web.Retry verify that order create on WMS success    ${increment_id}
    force_shipment.Fully force cancel via MCOM by order number    ${increment_id}
    common_cds_web.Verify order status MCOM 'COMPLETE, FULLYCANCELLED', MDC 'COMPLETE, FULLYCANCELLED'    ${increment_id}
    fms_db.Verify that order number exist in FMS DB    ${increment_id}
    #### Disable compare in kibana and FMS because the db of FMS still the issue data not update
    #Compare tax, discount, total amount for whole cart on kibana log with FMS db    ${increment_id}
    #### tax calculation by line item on mdc and fms db not equal
    # Compare tax, discount, total amount by line item for multiple sku on kibana log with FMS db    ${increment_id}    ${product_sku_list}[4]    ${product_sku_list}[30]    ${product_sku_list}[7]

E2E017_Fully cancel order with cart price rule subtotal+fixed discount, paid via Installment
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : No
    ...                - *Delivery type*    : Standard delivery
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Credit card installment
    [Tags]    e2e_flow    testrailid:C302971    e2e9
    Test setup access system by member type    ${personal_member}    09
    common_cds_web.Search product, add product to cart, go to checkout page    ${CDS17397872.sku}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    Checkout_keywords.Order successful with credit card installment    ${credit_card_installment.bank.bangkok_bank}    ${credit_card_installment.plan.six_months}
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${CDS17397872.price}    ${50}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    common_cds_web.Retry verify that order create on WMS success    ${increment_id}
    force_shipment.Fully force cancel via MCOM by order number    ${increment_id}
    common_cds_web.Verify order status MCOM 'COMPLETE, FULLYCANCELLED', MDC 'COMPLETE, FULLYCANCELLED'    ${increment_id}
    fms_db.Verify that order number exist in FMS DB    ${increment_id}
    #### Disable compare in kibana and FMS because the db of FMS still the issue data not update
    #Compare tax, discount, total amount for whole cart on kibana log with FMS db    ${increment_id}
    #### tax calculation by line item on mdc and fms db not equal
    # Compare tax, discount, total amount by line item for multiple sku on kibana log with FMS db    ${increment_id}    ${product_sku_list}[26]
