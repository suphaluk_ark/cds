*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser
Suite Teardown    Close All Browsers

*** Variables ***
${wrong_email_format}    tester@gg
${cust_name}    ${shipping_address_th.firstname}${SPACE}${shipping_address_th.lastname}
${address_line1}    ${shipping_address_th.address}
${address_line2}    ${shipping_address_th.sub_district}\,${SPACE}${shipping_address_th.district}\,${SPACE}${shipping_address_th.region}${SPACE}${shipping_address_th.zip_code}

${cust_name_update}    ${shipping_address_th.firstname_update}${SPACE}${shipping_address_th.lastname_update}
${address_line1_update}    ${shipping_address_th.address_update}
${address_line2_update}    ${shipping_address_th.sub_district_update}\,${SPACE}${shipping_address_th.district_update}\,${SPACE}${shipping_address_th.region_update}${SPACE}${shipping_address_th.zip_code_update}

*** Keywords ***
Initialize System
    [Arguments]    ${email}    ${password}
    ${user_token}=    Get login token    ${email}    ${password}
    ${response}=    Get customer details    ${user_token}
    ${payload}=    Evaluate    {"customer":${response}}
    Set Suite Variable    ${user_token}    ${user_token}
    Set Suite Variable    ${customer_payload}    ${payload}

Update Customer Profile To Default
    Update customer details    ${user_token}    ${customer_payload}

Verify login error message is displayed correctly when invalid login
    [Arguments]    ${username}    ${password}
    Login Keywords - Login with Email    ${username}    ${password}
    Login Keyword - Verify Error Message When Login Failed    ${error_message.login_email.login_failed}
    Header Web - Click Login on Header
    Header Web - Verify Error Message Not Visible

Login to CDS
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${login_username}    ${login_password}    ${login_display_name}
    Wait Until Page Is Completely Loaded

Login to CDS and go to Overview tab
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    Login to CDS    ${login_username}    ${login_password}    ${login_display_name}
    my_account_page.Click on the user icon
    my_account_page.Click on My Account page from menu

Login to CDS and go to profile tab
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    Login to CDS    ${login_username}    ${login_password}    ${login_display_name}
    my_account_page.Click on the user icon
    my_account_page.Click on my order tab
    my_account_page.Click on my profile tab

Login to CDS and go to Address Book tab
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    Login to CDS    ${login_username}    ${login_password}    ${login_display_name}
    my_account_page.Click on the user icon
    my_account_page.Click on My Account page from menu
    my_account_page.Click on my address book page

*** Test Cases ***
To verify Search Suggestion
    [Tags]    regression    suite_sanity    search    testrailid:C145261
    home_page_web_keywords.Input text into search box    ${product_sku_list}[0]
    home_page_web_keywords.Product should be displayed in search box popup    ${product_sku_list}[0]

To verify Search Result
    [Tags]    regression    suite_sanity    search    testrailid:C145262
    home_page_web_keywords.Input text into search box    ${product_sku_list}[0]
    home_page_web_keywords.Press Enter key to search a product
    plp_page.Verify product name should be displayed correctly in search product    ${${product_sku_list}[0]}[name]

To verify log in function
    [Tags]    regression    suite_sanity    authentication    testrailid:C145268
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Input Email on Header    ${sanity_user2}
    header_web_keywords.Header Web - Input Password on Header    ${sanity_password1}
    header_web_keywords.Header Web - Click Submit Button on Header
    header_web_keywords.Verify display name with welcome message should be displayed
    Wait Until Page Is Completely Loaded
    login_keywords.Login Keywords - Sign Out Success
    Wait Until Page Is Completely Loaded
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Facebook Button on Header
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_5]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window locator page title    ${homepage_title_locator}
    Wait Until Page Is Completely Loaded
    login_keywords.Login Keywords - Sign Out Success
    Wait Until Page Is Completely Loaded
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Input Email on Header  ${wrong_email_format}
    header_web_keywords.Header Web - Click Submit Button on Header
    header_web_keywords.Hearder Web - Email should be displayed error message  ${error_message.account_registration.invalid_email}
    Header Web - Click Login on Header
    Verify login error message is displayed correctly when invalid login    ${sanity_user2}    wrong_${sanity_password1}
    Verify login error message is displayed correctly when invalid login    wrong_${sanity_user2}    ${sanity_password1}

To verify log out function
    [Tags]    regression    suite_sanity    authentication    testrailid:C145269
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Input Email on Header    ${sanity_user2}
    header_web_keywords.Header Web - Input Password on Header    ${sanity_password1}
    header_web_keywords.Header Web - Click Submit Button on Header
    login_keywords.Login Keywords - Sign Out Success
    Wait Until Page Is Completely Loaded
    home_page_web_keywords.Verify homepage page should be displayed

To verify forgot password function
    [Tags]    regression    suite_sanity    authentication    testrailid:C145270
    forgot_password_keywords.Forgot Password Success    ${sanity_user2}

To verify overview page
    [Tags]    regression    suite_sanity    account_management    testrailid:C145263
    [Setup]    Login to CDS and go to Overview tab    ${sanity_user1}    ${sanity_password1}    ${sanity_information_1.firstname}
    my_account_page.Customer name should be displayed in personal information    ${sanity_information_2.firstname} ${sanity_information_2.lastname}
    my_account_page.Customer e-mail should be displayed in personal information    ${sanity_information_2.email}
    my_account_page.Customer phone no should be displayed in personal information    ${sanity_information_2.tel}
    my_account_page.Verify connect to The 1 Account button should be displayed
    my_account_page.Click Connect To The 1 Account Button
    my_account_page.Login to The 1 Account Dialog should be displayed
    my_account_page.Click close button on the The 1 Account Dialog
    my_account_page.Default shipping address label should be displayed
    my_account_page.Full tax invoice address label should be displayed
    my_account_page.Lasted order label should be displayed

To verify my profile page
    [Tags]    regression    suite_sanity    account_management    testrailid:C145264
    [Setup]    Run Keywords    Initialize System    ${sanity_user2}    ${sanity_password1}
    ...    AND    Login to CDS and go to profile tab    ${sanity_user2}    ${sanity_password1}    ${sanity_information_2.firstname}
    my_account_page.Firstname field should be displayed customer data    ${sanity_information_2.firstname}
    my_account_page.Lastname field should be displayed customer data    ${sanity_information_2.lastname}
    my_account_page.Phone field should be displayed customer data    ${sanity_information_2.tel}
    my_account_page.Date of birth field should be displayed customer data    ${sanity_information_2.dob}
    my_account_page.E-mail field should be displayed customer data and disabled    ${sanity_information_2.email}
    my_account_page.Gender radio button should be displayed
    my_account_page.Language radio button should be displayed
    my_account_page.Input text for Firstname field    update_${sanity_information_2.firstname}
    my_account_page.Input text for Lastname field    update_${sanity_information_2.lastname}
    my_account_page.Input text for Phone number field    ${sanity_information_2.tel_update}
    my_account_page.Input text for Date of birth field    ${sanity_information_2.dob_update}
    my_account_page.Click Save Changes button
    my_account_page.Firstname field should be displayed customer data    update_${sanity_information_2.firstname}
    my_account_page.Lastname field should be displayed customer data    update_${sanity_information_2.lastname}
    my_account_page.Phone field should be displayed customer data    ${sanity_information_2.tel_update}
    my_account_page.Date of birth field should be displayed customer data    ${sanity_information_2.dob_update}
    my_account_page.E-mail field should be displayed customer data and disabled    ${sanity_information_2.email}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    Close Browser
    ...    AND    Update Customer Profile To Default

To verify address book page
    [Tags]    regression    suite_sanity    account_management    testrailid:C145265
    [Setup]  Login to CDS and go to Address Book tab  ${sanity_user2}  ${sanity_password1}  ${sanity_information_2.firstname}
    ### add a new address ###
    my_account_page.Verify Add New address button should be displayed on address book page
    my_account_page.Click Add New address button on address book page
    my_account_page.Click and input text for first name on address book page    ${shipping_address_th.firstname}
    my_account_page.Click and input text for last name on address book page    ${shipping_address_th.lastname}
    my_account_page.Click and input text for telephone number on address book page    ${shipping_address_th.phone}
    my_account_page.Click and input text for house number and street on address book page    ${shipping_address_th.address}
    my_account_page.Click and input text for postcode on address book page    ${shipping_address_th.zip_code}
    my_account_page.Select a province from the drop-down list    ${shipping_address_th.region}
    my_account_page.Select a district from the drop-down list    ${shipping_address_th.district}
    my_account_page.Select a sub-district from the drop-down list    ${shipping_address_th.sub_district}
    my_account_page.Click and input text for address name on address book page    ${shipping_address_th.address_name}
    my_account_page.Click SAVE CHANGES button on the Address Book page
    my_account_page.Verify the lastest address name should be displayed correctly    ${shipping_address_th.address_name}
    my_account_page.Verify the lastest customer name should be displayed correctly    ${cust_name}
    my_account_page.Verify the lastest customer phone should be displayed correctly    ${shipping_address_th.phone}
    my_account_page.Verify the lastest address line 1 should be displayed correctly    ${address_line1}
    my_account_page.Verify the lastest address line 2 should be displayed correctly    ${address_line2}
    #### delete an address from the list ###
    Reload Page
    ${random_address_id}=  my_account_page.Get a random address id from the address list
    address_keywords.Delete Address by API    ${random_address_id}
    Reload Page
    my_account_page.Verify an adress should be not displayed by id    ${random_address_id}
    Reload Page
    ### edit an address ##
    my_account_page.Get a random address and click edit on it
    ${current_id}=    my_account_page.Get the current address id from url
    my_account_page.Click and input text for first name on address book page    ${shipping_address_th.firstname_update}
    my_account_page.Click and input text for last name on address book page    ${shipping_address_th.lastname_update}
    my_account_page.Click and input text for telephone number on address book page    ${shipping_address_th.phone_update}
    my_account_page.Click and input text for house number and street on address book page    ${shipping_address_th.address_update}
    my_account_page.Click and input text for postcode on address book page    ${shipping_address_th.zip_code_update}
    my_account_page.Select a province from the drop-down list    ${shipping_address_th.region_update}
    my_account_page.Select a district from the drop-down list    ${shipping_address_th.district_update}
    my_account_page.Select a sub-district from the drop-down list  ${shipping_address_th.sub_district_update}
    my_account_page.Click and input text for address name on address book page    ${shipping_address_th.address_name_update}
    my_account_page.Click SAVE CHANGES button on the Address Book page
    my_account_page.Verify address name should be displayed correctly by id    ${current_id}    ${shipping_address_th.address_name_update}
    my_account_page.Verify name should be displayed correctly by id    ${current_id}    ${cust_name_update}
    my_account_page.Verify phone should be displayed correctly by id    ${current_id}    ${shipping_address_th.phone_update}
    my_account_page.Verify address line 1 should be displayed correctly by id    ${current_id}    ${address_line1_update}
    my_account_page.Verify address line 2 should be displayed correctly by id    ${current_id}    ${address_line2_update}

To verify Wishlist page
    [Tags]    regression    suite_sanity    account_management    testrailid:C145266
    [Setup]    Run Keywords    Add wishlist item by SKU number    ${sanity_user2}    ${sanity_password1}    ${product_sku_list}[0]
    ...    AND    Login to CDS    ${sanity_user2}    ${sanity_password1}    ${sanity_information_2.firstname}
    wishlist_page.Go to wishlist page
    wishlist_page.Added product should be displayed in wishlist    ${${product_sku_list}[0]}[name]
    wishlist_page.Clicking add to cart button on wishlist page    ${product_sku_list}[0]
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    wishlist.Remove all wishlist items in wishlist group    ${sanity_user2}    ${sanity_password1}    ${EMPTY}
    ...    AND    Close Browser

To verify my orders page
    [Tags]    regression    suite_sanity    account_management    testrailid:C145267
    order_tracking_page.Click on Tracking your orders
    order_tracking_page.Input email    ${track_order.guest_user_1.email}
    order_tracking_page.Input Order number    ${track_order.guest_user_1.order_number}
    order_tracking_page.Click Track order button
    order_tracking_page.Click on Tracking your orders
    order_tracking_page.Guest Order detail page should be displayed
    Login to CDS    ${sanity_user1}    ${sanity_password1}    ${sanity_information_1.firstname}
    order_tracking_page.Click on Tracking your orders
    order_tracking_page.Click on the first order from menu
    order_tracking_page.Order detail page should be displayed
