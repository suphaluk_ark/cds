*** Settings ***
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown
...    AND    Close Browser
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Force Tags    product_PDP

*** Keywords ***
Template - Verify Get Estimated Delivery Options With Product Type On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Postcode field and apply button should be visible
    product_PDP_page.Use my current location field should be visible
    product_PDP_page.Input value into postcode textbox    ${personal_username}
    product_PDP_page.The invalid postcode message should be visible
    product_PDP_page.The postcode textfield allows maximum 5 characters
    product_PDP_page.Input value into postcode textbox    ${personal_information.tel}
    product_PDP_page.The postcode textfield allows maximum 5 characters
    [Teardown]    Run Keywords    Close Browser
    ...    AND    common_cds_web.Setup - Open browser

Template - Verify Available Delivery Options With Product Type On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Postcode field and apply button should be visible
    product_PDP_page.Verify available delivery options by default lead time label    ${sku}
    product_PDP_page.Input value into postcode textbox    ${post_code_list}[0]
    product_PDP_page.Click postcode apply button
    product_PDP_page.Verify available delivery options by postcode value    ${sku}    ${post_code_list}[0]
    [Teardown]    Run Keywords    Close Browser
    ...    AND    common_cds_web.Setup - Open browser

Template - Verify Available Delivery Options Be Hidden When Product Sku Is Not Enabled For Any Delivery Method
    [Arguments]    ${sku}    ${msgLine}
    common_keywords.Go To PDP Directly By Product Sku    ${sku}
    Wait Until Page Is Completely Loaded
    product_PDP_page.Verify message line for out of stock or preorder or byorder was displayed    ${msgLine}
    product_PDP_page.Verify available delivery options section was not display

Template - Verify Location Will Be Saved During The Customer Whole Customer Session
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Postcode field and apply button should be visible
    product_PDP_page.Input value into postcode textbox    ${post_code_list}[0]
    product_PDP_page.The postcode textfield allows maximum 5 characters
    product_PDP_page.Click postcode apply button
    product_PDP_page.Verify Postcode Are Still Saved During Customer Session    ${product_type}
    [Teardown]    Run Keywords    Close Browser
    ...    AND    common_cds_web.Setup - Open browser

Template - Verify Location Will Be Cleared When Customer Session End
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Postcode field and apply button should be visible
    product_PDP_page.Input value into postcode textbox    ${post_code_list}[0]
    product_PDP_page.The postcode textfield allows maximum 5 characters
    product_PDP_page.Click postcode apply button
    SeleniumLibrary.Close Browser
    common_cds_web.Setup - Open browser
    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    [Teardown]    Run Keywords    Close Browser
    ...    AND    common_cds_web.Setup - Open browser

Template - Verify Available Delivery Options Be Hidden When Postcode Entry Is Not Supported
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Postcode field and apply button should be visible
    product_PDP_page.Input value into postcode textbox    ${post_code_list}[4]
    product_PDP_page.Click postcode apply button
    product_PDP_page.Verify available delivery options by postcode value    ${sku}    ${post_code_list}[4]
    product_PDP_page.The invalid postcode message should be visible

Directly go to PDP page and verify Check stock at store option not display
    [Arguments]    ${product_sku}
    common_keywords.Go To PDP Directly By Product Sku    ${product_sku}
    Verify product id display on product PDP page
    product_PDP_page.Verify Check stock at store option should not be displayed

Template - Verify Format Time (UTC+7) in Find pickup Locations On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Verify view pickup location link display correctly
    product_PDP_page.Verify view more pickup location button display correctly    ${sku}
    product_PDP_page.Verify format lead time for each store    ${sku}

Template - Verify Check stock at store On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Verify Check stock at store should be visible
    product_PDP_page.Click Check stock at store
    product_PDP_page.Verify search field should be visible
    product_PDP_page.Verify Use my current location in check stock at store should be visible
    product_PDP_page.Verify list store in check stock at store should be visible    ${sku}

Template - Verify search on check stock at store On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Verify Check stock at store should be visible
    product_PDP_page.Click Check stock at store
    product_PDP_page.Input store name into postcode textbox    ${pick_at_store_name.central_changwattana1}
    product_PDP_page.Verify store are searched by store name    ${pick_at_store_name.central_changwattana1}
    product_PDP_page.Input postcode into postcode textbox    ${post_code_list}[6]
    product_PDP_page.Click postcode apply button
    product_PDP_page.Verify store are searched by postcode value    ${sku}    ${post_code_list}[6]
    product_PDP_page.Input postcode into postcode textbox    ${post_code_list}[4]
    product_PDP_page.Click postcode apply button
    product_PDP_page.The invalid postcode message should be visible on check stock at store

Template - Verify stock status on Check stock at store On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Verify Check stock at store should be visible
    product_PDP_page.Click Check stock at store
    product_PDP_page.Verify stock status on Check stock at store    ${sku}

*** Test Cases ***
Verify Available Delivery Options Be Hidden When Product Sku Is Out Of Stock
    [Tags]    jenkins_group1    testrailid:C872581    testrailid:C843171    testrailid:C843176    Omni_PDP
    [Template]    Template - Verify Available Delivery Options Be Hidden When Product Sku Is Not Enabled For Any Delivery Method
    CDS10827994    ${dictproductPDP}[lbl_out_of_stock]

Verify Available Delivery Options Be Hidden When Product Sku Is PreOrder
    [Tags]    jenkins_group1    testrailid:C872581    testrailid:C878023    Omni_PDP
    [Template]    Template - Verify Available Delivery Options Be Hidden When Product Sku Is Not Enabled For Any Delivery Method
    CDS10814406    ${dictproductPDP}[lbl_pre_order]

Verify Available Delivery Options Be Hidden When Product Sku Is ByOrder
    [Tags]    jenkins_group1    testrailid:C872581    testrailid:C878024    Omni_PDP
    [Template]    Template - Verify Available Delivery Options Be Hidden When Product Sku Is Not Enabled For Any Delivery Method
    CDS15116833    ${dictproductPDP}[lbl_out_of_stock]

Verify intransit status at order status and progress bar
    [Tags]    jenkins_group1    regression    product_detail_page    testrailid:C355678    testrailid:C413276    defect:CDS-8337
    product_PDP_keywords.Search product and show on product PDP page    ${product_sku_list}[7]
    product_PDP_keywords.Verify product details is display information in PDP page correct by sku    ${product_sku_list}[7]
    product_PDP_keywords.Verify social icon on PDP page
    product_PDP_page.Verify add to bag button on product PDP page    ${product_sku_list}[7]

Verify product slider less than or equal to 5 items or on product PDP page
    [Tags]    jenkins_group1     regression    product_detail_page    testrailid:C415728
    home_page_web_keywords.Search product by product sku    ${product_sku_list}[5]
    product_PDP_keywords.Verify complete the look section when product is less than order equal to zero

Verify product slider if more than 5 items on product PDP page
    [Tags]    jenkins_group1    regression    product_detail_page    testrailid:C415764
    home_page_web_keywords.Search product by product sku    ${product_sku_list}[9]
    product_PDP_keywords.Verify product relate if moren than 5 items with SKU on product slider

Verify promotion on product PDP page
    [Tags]    jenkins_group1    regression    product_detail_page    testrailid:C413133
    product_PDP_keywords.Search product and show on product PDP page    ${product_sku_list}[15]
    product_PDP_page.Verify product name on product PDP page    ${product_sku_list}[15]
    product_PDP_keywords.Verify promotion of SKU    ${order_PDP_${product_sku_list}[15]}

Verify Preview Product Discount
    [Tags]    jenkins_group1    product_detail_page    testrailid:C413133
    ${product}=    Get Dictionary Of Discount Product From Catalog Service    ${category_id.sale}
    Search product by product sku    ${product}[product_sku]
    Search product by product sku    ${product_sku_list}[0]
    Scroll To Center Page
    Verify Product Preview Discount Should Display    ${product}[product_sku]    ${product}[product_saving_price]

Verify Get Estimated Delivery Options With Product Type On PDP
    [Tags]    jenkins_group1    testrailid:C843275    testrailid:C843262    testrailid:C843263    testrailid:C843264    testrailid:C843268    testrailid:C843269    testrailid:C843270
    ...    testrailid:C843271    testrailid:C843271    testrailid:C843272    testrailid:C843273    testrailid:C843274    testrailid:C887286    testrailid:C843276    testrailid:C843277    Omni_PDP
    [Template]    Template - Verify Get Estimated Delivery Options With Product Type On PDP
    ${plp_page_section_scope.simple_product}
    ${plp_page_section_scope.configurable_product}

Verify Available Delivery Options With Product Type On PDP
    [Tags]    jenkins_group1    testrailid:C989666    testrailid:C989667    testrailid:C989669    testrailid:C989670    testrailid:C989671    testrailid:C989673    testrailid:C843178    testrailid:C843177    testrailid:C843175    testrailid:C843174    testrailid:C843173    testrailid:C843172    testrailid:C843170    testrailid:C843169    testrailid:C848355    testrailid:C848356    testrailid:C848357    testrailid:C869594    testrailid:C848358    testrailid:C869595    testrailid:C848359    testrailid:C848360
    ...    testrailid:C848361    testrailid:C869596    testrailid:C848362    testrailid:C869598    testrailid:C848363    testrailid:C848364    testrailid:C955583    testrailid:C955585    testrailid:C955586    testrailid:C955588    testrailid:C955589    testrailid:C955590    testrailid:C955592    testrailid:C955593    testrailid:C955594    testrailid:C955595    Omni_PDP    testrailid:C1014519    testrailid:C1014510    testrailid:C1014511    testrailid:C1014520    testrailid:C1014521    testrailid:C1014522    testrailid:C1014523
    [Template]    Template - Verify Available Delivery Options With Product Type On PDP
    ${plp_page_section_scope.simple_product}
    ${plp_page_section_scope.configurable_product}

Verify Location Will Be Saved During The Customer Whole Customer Session
    [Tags]    jenkins_group1    testrailid:C843266    Omni_PDP
    [Template]    Template - Verify Location Will Be Saved During The Customer Whole Customer Session
    ${plp_page_section_scope.simple_product}
    ${plp_page_section_scope.configurable_product}

Verify Location Will Be Cleared When Customer Session End
    [Tags]    jenkins_group1    testrailid:C843267    Omni_PDP
    [Template]    Template - Verify Location Will Be Cleared When Customer Session End
    ${plp_page_section_scope.simple_product}
    ${plp_page_section_scope.configurable_product}

Verify Pickup Location On PDP Page
    [Tags]    Omni_PDP    testrailid:C982111    testrailid:C982113    testrailid:C982114    testrailid:C982117    testrailid:C982119    testrailid:C982120    testrailid:C982121
    ...     testrailid:C1029198     testrailid:C1029199     testrailid:C1029202     testrailid:C1029203
    ${response_method}=    Get delivery methods response by sku and postcode    ${store_view.name.en}    ${sku_list_with_2hr}[2]    0
    product_PDP_keywords.Verify sku contain click and collect method    ${response_method}
    ${response_store}=    Get Store pickup location    ${sku_list_with_2hr}[2]
    common_keywords.Go To PDP Directly By Product Sku    ${sku_list_with_2hr}[2]
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Verify view pickup location link display correctly
    product_PDP_page.Verify close button display correctly
    product_PDP_keywords.Verify store name display correctly with filter    ${response_store}    ${product_detail_page.filter_option.central}
    product_PDP_keywords.Verify store name display correctly with filter    ${response_store}    ${product_detail_page.filter_option.robinson}
    product_PDP_keywords.Verify store name display correctly with postcode search    ${response_store}    ${post_code_list}[5]
    
Verify Available Delivery Options Be Hidden When Postcode Entry Is Not Supported
    [Tags]    testrailid:C1014524
    [Template]    Template - Verify Available Delivery Options Be Hidden When Postcode Entry Is Not Supported
    ${plp_page_section_scope.simple_product}
    ${plp_page_section_scope.configurable_product}

Verify Check stock at store option not display when product sku is by-order
    [Tags]    testrailid:C1019783   testrailid:C1029209    Omni_PDP
    Directly go to PDP page and verify Check stock at store option not display       ${product_byorder_sku}[1]

Verify Check stock at store option not display when product sku is pre-order
    [Tags]    testrailid:C1019786   testrailid:C1029210    Omni_PDP
    Directly go to PDP page and verify Check stock at store option not display       ${product_preorder_sku}[0]

Verify Format Time (UTC+7) in Find pickup Locations On PDP
    [Tags]    testrailid:C1026319    Omni_PDP
    [Template]    Template - Verify Format Time (UTC+7) in Find pickup Locations On PDP
    ${plp_page_section_scope.simple_product}

Verify Check stock at store on PDP
    [Tags]    testrailid:C1014362    Omni_PDP
    [Template]    Template - Verify Check stock at store On PDP
    ${plp_page_section_scope.simple_product}

Verify search on Check stock at store
    [Tags]    testrailid:C1014363    Omni_PDP
    [Template]    Template - Verify search on check stock at store On PDP
    ${plp_page_section_scope.simple_product}

Verify stock status on Check stock at store page
    [Tags]    testrailid:C1014366    Omni_PDP
    [Template]    Template - Verify stock status on Check stock at store On PDP
    ${plp_page_section_scope.simple_product}
