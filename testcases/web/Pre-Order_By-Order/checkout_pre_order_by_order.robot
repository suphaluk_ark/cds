*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

Test Setup    common_cds_web.Setup - Open browser

Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

Suite Teardown    Close All Browsers

*** Variables ***
${login_username}    ${cds_checkout_pre_order_username}
${login_password}    ${cds_checkout_pre_order_password}
${login_display_name}    ${cds_checkout_pre_order_information.firstname}

*** Test Cases ***
To verify that COD payment method is suppressed when there's a Pre-order item in cart
    [Tags]    regression    Pre-order/By-order    testrailid:C178074
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_web.Setup - Login successfully, create product data file and get customer profile    ${login_username}    ${login_password}    ${login_display_name}
    header_web_keywords.Header Web - Click Mini Cart Button
    Search pre-order product by product sku and add to cart  ${product_sku_list}[36]
    product_page.Click OK to close pre-oder popup
    Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.COD payment method should not be visible

To verify that COD payment method is suppressed when there's a By order item in cart
    [Tags]    regression    Pre-order/By-order    testrailid:C178075
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_web.Setup - Login successfully, create product data file and get customer profile    ${login_username}    ${login_password}    ${login_display_name}
    header_web_keywords.Header Web - Click Mini Cart Button
    Search pre-order product by product sku and add to cart  ${product_sku_list}[37]
    product_page.Click OK to close pre-oder popup
    Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.COD payment method should not be visible

To verify that COD payment method is back available when Pre-order item has been removed from cart
    [Tags]    regression    Pre-order/By-order    testrailid:C178076
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_web.Setup - Login successfully, create product data file and get customer profile    ${login_username}    ${login_password}    ${login_display_name}
    Search pre-order product by product sku and add to cart  ${product_sku_list}[36]
    product_page.Click OK to close pre-oder popup
    common_cds_web.Search product and add product to cart  ${product_sku_list}[7]
    Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.COD payment method should not be visible
    payment_page.Click Edit Bag in Order Summary
    my_cart_page.Remove product from shopping bag  ${product_sku_list}[36]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Verify COD payment method should be visible

To verify that COD payment method is back available when By order item has been removed from cart
    [Tags]    regression    Pre-order/By-order    testrailid:C178077
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_web.Setup - Login successfully, create product data file and get customer profile    ${login_username}    ${login_password}    ${login_display_name}
    Search pre-order product by product sku and add to cart  ${product_sku_list}[37]
    product_page.Click OK to close pre-oder popup
    common_cds_web.Search product and add product to cart  ${product_sku_list}[7]
    Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.COD payment method should not be visible
    payment_page.Click Edit Bag in Order Summary
    my_cart_page.Remove product from shopping bag  ${product_sku_list}[37]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Verify COD payment method should be visible

To verify that only common payment methods for all items in cart are available when the cart has Pre-order and/or By order item
    [Tags]    regression    Pre-order/By-order    testrailid:C178078
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_web.Setup - Login successfully, create product data file and get customer profile    ${login_username}    ${login_password}    ${login_display_name}
    Search pre-order product by product sku and add to cart  ${product_sku_list}[37]
    product_page.Click OK to close pre-oder popup
    common_cds_web.Search product and add product to cart  ${product_sku_list}[14]
    Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.COD payment method should not be visible
    checkout_page.Verify credit card payment method should not be visible
    checkout_page.Verify Bank Transfer shoud be visible

*** Keywords ***
Search pre-order product by product sku and add to cart
    [Arguments]    ${product_sku}    ${quantity}=1
    home_page_web_keywords.Search product by product sku    ${product_sku}
    Add product quantity    ${product_sku}    ${quantity}
    product_page.Click on add to cart button    ${product_sku}