*** Settings ***
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Suite Teardown    Close All Browsers

*** Keywords ***
Search pre-order product by product sku and add to cart
    [Arguments]    ${product_sku}    ${quantity}=1
    home_page_web_keywords.Search product by product sku    ${product_sku}
    Add product quantity    ${product_sku}    ${quantity}
    product_page.Click on add to cart button    ${product_sku}

*** Test Cases ***
To Verify that Pre-Order shipping information on PDP displays as product setup for Pre-Order item
    [Tags]    regression    Pre-order/By-order    testrailid:C183077    testrailid:C183078    testrailid:C183080    testrailid:C183081
    Search pre-order product by product sku and add to cart     ${product_sku_list}[34]
    product_page.Verify Pre-order popup    ${preorder_details.popup_txt_cds10814406}

To Verify that Pre-Order shipping information on PDP is suppressed if the purchase date surpass Pre-Order shipping date
    [Tags]    regression    Pre-order/By-order    testrailid:C183079    testrailid:C183082
    Search pre-order product by product sku and add to cart     ${product_sku_list}[35]
    product_page.Verify Pre-order is suppressed    ${preorder_details.Pre_order}