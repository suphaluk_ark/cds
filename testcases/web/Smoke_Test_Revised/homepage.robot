*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Test Cases ***
Verify that product must be displayed in flash deal section correctly if it already set on MDC
    [Tags]    smoke_test_revised    flashdeal    testrailid:C1002667
    home_page_keywords.Verify ui for smoke test - 'flash deal product' should be displayed correctly via api