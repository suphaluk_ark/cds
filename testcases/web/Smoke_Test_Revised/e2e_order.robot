*** Settings ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Variables ***
${outside_bkk}    77110

*** Test Cases ***
Order product (member) with cart price rule subtotal+fixed discount + T1 redeem and paid via Credit Card
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : ${coupon.five_bath} - CDS: Rule 215 CDS11133773 - RBS: Rule 5087 RBS20265274
    ...                - *Delivery type*    : shipping to address
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Credit card
    [Tags]    smoke_test_revised    e2e_order_ui    testrailid:C1002640
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_03}  ${e2e_password_03}
    login_keywords.Login Keywords - Login Success  ${e2e_username_03}  ${e2e_password_03}    ${e2e_information_03.firstname}
    common_cds_web.Search product and add product to cart  ${cart_price_rule_sku.apply_coupon['5BAHT']}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Apply coupon code  ${coupon.five_bath}
    my_cart_page.Verify coupon code display in cart    ${coupon.five_bath}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${coupon.five_bath}    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}   ${5.0}
    checkout_keywords.Checkout Product With T1 Redeem Until Payment With Credit Card Method    ${40}    ${5}    ${t1c_information_4.email}   ${t1c_information_4.password}
    checkout_page.Verify order total in thank you page    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}    ${discount_amount}    ${5}
    checkout_page.Get order ID from thank you page
    # API
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}
    Retry verify that order create on WMS success    ${increment_id}
    fms_db.Verify that order number exist in FMS DB    ${increment_id}

Order product (member) with cart price rule (fix coupon)+Freebie, paid via 123 bank transfer
    [Documentation]    - *Cart price rule*    :
    ...                | =Rule=                                                     | =Coupon Code=         | =Type=                                        | =BU ENV ID=                               |
    ...                | QAAT_ROBOT_E2E-Buy >= 2000 - Discount 50฿                  | Non coupon            | Fix amount discount for whole cart            | CDS UAT 830, CDS SIT 830, RBS UAT 5090    |
    ...                | QAAT_ROBOT_E2E-Buy specific SKU, Free 1 product            | Non coupon            | Auto add promo items with products (freebie)  | CDS UAT 211, CDS SIT 211, RBS UAT 5089    |
    ...                | QAAT_ROBOT_E2E-Buy specific SKU, discount 5฿ with coupon   | ${coupon.five_bath}   | Fixed amount discount                         | CDS UAT 215, CDS SIT 215, RBS UAT 5087    |
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Bank transfer
    [Tags]    smoke_test_revised    e2e_order_ui    testrailid:C1002641    support_sit
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_03}  ${e2e_password_03}
    login_keywords.Login Keywords - Login Success  ${e2e_username_03}  ${e2e_password_03}    ${e2e_information_03.firstname}
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.non_coupon['freebie']}[buy_item]
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${cart_price_rule_sku.non_coupon['freebie']}[buy_item]    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    my_cart_page.Apply coupon code  ${coupon.five_bath}
    my_cart_page.Verify coupon code display in cart    ${coupon.five_bath}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${coupon.five_bath}    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}   ${5.0}
    shopping_bag_keywords.Verify free gift with this item purchase should be display correctly    ${cart_price_rule_sku.non_coupon.freebie}[buy_item]    ${cart_price_rule_sku.non_coupon.freebie}[free_item]
    my_cart_page.Click Secure Checkout Button
    E2E_flow_keywords.Select standard shipping address and go to payment page
    E2E_flow_keywords.Confirm payment by banktransfer
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${${50}+${discount_amount}}
    checkout_page.Get order ID from thank you page 

Order product (Guest) paid via COD
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : No
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    [Tags]    smoke_test_revised    e2e_order_ui    testrailid:C1007393
    common_cds_web.Search product and add product to cart    ${non_cart_price_rule_sku.cod}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details    postcode=${outside_bkk}
    CommonWebKeywords.Scroll To Center Page
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method COD
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For COD Payment Method
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_cod}
    checkout_page.Order status from thank you page should be    ${thankyou_page.status_pending}
    checkout_page.Verify order total in thank you page    ${${non_cart_price_rule_sku.cod}}[price]
    checkout_page.Get order ID from thank you page

Order product with mix products (3PL type) + cart price rule (percent discount) + free item, paid via COD + request full tax invoice
    [Documentation]    - *Cart price rule*    :
    ...                | =Rule=                                                                     | =Coupon Code=                     | =Type=                                            | =BU ENV ID=               |
    ...                | QAAT_ROBOT_E2E-Buy >= 2000 - Discount 50฿                                  | Non coupon                        | Fix amount discount for whole cart                | CDS UAT 830, CDS SIT 830  |
    ...                | QAAT_ROBOT_E2E-Buy specific SKU, Qty is 3 - Free 1 product                 | Non coupon                        | Auto add promo items for whole cart (free item)   | CDS UAT 613, CDS SIT 613  |
    ...                | QAAT_ROBOT_E2E-whole cart discount 10% only for retail product with coupon | ${coupon.coupon10percentretail}   | Percent of product price discount                 | CDS UAT 1097, CDS SIT 991 |
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : Yes
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    [Tags]    smoke_test_revised    e2e_order_ui    testrailid:C1002645    support_sit
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_03}  ${e2e_password_03}
    login_keywords.Login Keywords - Login Success  ${e2e_username_03}  ${e2e_password_03}    ${e2e_information_03.firstname}
    # Retail Product, QAAT_ROBOT_E2E-Buy specific SKU, Qty is 3 - Free 1 product
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[buy_item]    ${3} 
    # Any Retail Product
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon}[COUPON10] 
    # Any Marketplace Product
    common_cds_web.Search product and add product to cart    ${non_cart_price_rule_sku}[marketplace]
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[buy_item]    ${cart_price_rule_sku.apply_coupon}[COUPON10]    ${non_cart_price_rule_sku}[marketplace]
    shopping_bag_keywords.Verify free items with order purchase should be display correctly    ${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[free_item]
    my_cart_page.Apply coupon code  ${coupon.coupon10percentretail}
    my_cart_page.Verify coupon code display in cart    ${coupon.coupon10percentretail}
    ${expected_discount_10percent_for_retail_products}=    Evaluate    (${${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[buy_item].price}*${3})+${${cart_price_rule_sku.apply_coupon}[COUPON10].price}
    my_cart_page.Verify e-coupon discount in cart page(%)    ${coupon.coupon10percentretail}    ${expected_discount_10percent_for_retail_products}    ${10.0}
    checkout_keywords.Check Out Product Until Payment With COD Method And Request Full Tax Invoice(Personal)
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    discount=${${50}+${discount_amount}}
    checkout_page.Get order ID from thank you page

Order product (login fb) with cart price rule subtotal + percent discount, paid via Credit card with T1 redeem
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : ${coupon.coupon10} - CDS Rule 213 CDS12063437 - RBS Rule 5088 RBS23927858
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Credit Card
    [Tags]    smoke_test_revised    e2e_order_ui    testrailid:C1003410
    Header Web - Click Login on Header
    registration_page.Header Web - Click Facebook login Button
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_7]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window locator page title    ${windows_locator_title}
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product and add product to cart    ${non_cart_price_rule_sku.login_facebook['item_1']}    # Retail Product
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['COUPON10']}    # Retail Product
    common_cds_web.Search product and add product to cart    ${non_cart_price_rule_sku.login_facebook['item_2']}    # Retail Product
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${non_cart_price_rule_sku.login_facebook['item_1']}    ${cart_price_rule_sku.apply_coupon['COUPON10']}   ${non_cart_price_rule_sku.login_facebook['item_2']}
    my_cart_page.Apply coupon code  ${coupon.coupon10}
    my_cart_page.Verify coupon code display in cart    ${coupon.coupon10}
    my_cart_page.Verify e-coupon discount in cart page(%)    ${coupon.coupon10}    ${${cart_price_rule_sku.apply_coupon['COUPON10']}.price}   ${10.0}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    E2E_flow_keywords.Select standard shipping address and go to payment page
    E2E_flow_keywords.Confirm payment by credit card
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    discount=${${50}+${discount_amount}}
    checkout_page.Get order ID from thank you page

Order product with cart price rule subtotal+fixed discount, paid via 2C2P Installment
    [Documentation]    - *Auto coupon*      : (QAAT - buy > = 2000 discount 50฿) - CDS Rule 830 - RBS Rule 5090
    ...                - *Apply coupon*     : No
    ...                - *Delivery type*    : Express shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Credit card Installment
    ...                - *sku*              : CDS17397872 - RBS22161338
    [Tags]    smoke_test_revised    e2e_order_ui    testrailid:C1002650
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_03}  ${e2e_password_03}
    login_keywords.Login Keywords - Login Success  ${e2e_username_03}  ${e2e_password_03}    ${e2e_information_03.firstname}
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.non_coupon['discount_50b']}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_keywords.Select Same day or Next day delivery shipping method
    checkout_page.Click Continue Payment Button
    Checkout_keywords.Order successful with credit card installment    ${credit_card_installment.bank.bangkok_bank}    ${credit_card_installment.plan.six_months}
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    price=${${cart_price_rule_sku.non_coupon['discount_50b']}.price}    discount=${50}    shipping=${150}
    checkout_page.Get order ID from thank you page

Order product with cart price rule (fix coupon), paid via pay at store
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : ${coupon.five_bath} - CDS: Rule 215 CDS11133773 - RBS: Rule 5087 RBS20265274
    ...                - *Delivery type*    : Click & Collect
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Pay at store
    [Tags]    smoke_test_revised    e2e_order_ui    testrailid:C1002653    support_sit
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_03}  ${e2e_password_03}
    login_keywords.Login Keywords - Login Success  ${e2e_username_03}  ${e2e_password_03}    ${e2e_information_03.firstname}
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    my_cart_page.Apply coupon code  ${coupon.five_bath}
    my_cart_page.Verify coupon code display in cart    ${coupon.five_bath}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${coupon.five_bath}    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}   ${5.0}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    checkout_keywords.Pickup at store, standard pickup, go to payment page    ${pick_at_store.central_bangna}
    payment_keywords.Confirm payment by pay at store
    payment_keywords.Verify payment type and payment status in Thank you page after pay by pay at store
    checkout_page.Get order ID from thank you page

Order product, paid via T1 Full redeem (Zero pay)
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : No
    ...                - *Delivery type*    : Pick up at store
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Full redeem point
    [Tags]    smoke_test_revised    e2e_order_ui    testrailid:C1025703
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_02}  ${e2e_password_02}
    login_keywords.Login Keywords - Login Success    ${e2e_username_02}  ${e2e_password_02}    ${e2e_information_02.firstname}
    common_cds_web.Search product and add product to cart    ${non_cart_price_rule_sku.zero_pay}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_keywords.Pickup at store, standard pickup, go to payment page    ${pick_at_store.central_bangna}
    checkout_keywords.Full redeem point and verify T1C point discount    ${t1c_information_4.email}    ${t1c_information_4.password}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click continue payment for full redeemtion
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_redeem_point}
    checkout_page.Verify order total in thank you page    ${0}