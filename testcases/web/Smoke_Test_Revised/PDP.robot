*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Test Cases ***
Verify that the Configurable product display correctly
    [Documentation]    - *Configurable Product*    : CONFIG1602FTSG031
    [Tags]    pdp    pdp_ui    smoke_test_revised    testrailid:C996398    support_sit
    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PDP page by product name    ${CONFIG1602FTSG031.name}

    ${url}=    common_cds_web.Get product urlkey by current url
    ${response}=    pdp_falcon.Api Product PDP By Url Key   ${url}    ${language}    ${BU}

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    ${dict_simple_product}=    product_PDP_keywords.Get configurable's simple product displayed via api   ${response}
    # 1. Data Validation points
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product brand name' should display correctly via api    ${response}
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product name' should display correctly via api    ${response}    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product overlay image' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : redeem point' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag new / promotion' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag only at / online exclusive' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product price' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product market place' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : pay by installment' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product configurable options' should be displayed    ${response}    ${dict_simple_product}[id]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product id' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product customer reviews' should be displayed via api    ${response}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

    # 2. UI Validation points
    ${dict_random_product_sku}=    product_PDP_keywords.Select ramdom configurable options via api, return product    ${response}    
    Set Test Variable    ${${dict_random_product_sku}[sku]}    ${dict_random_product_sku}
    common_cds_web.Retry add product to cart, product should be displayed in mini cart    ${dict_random_product_sku}[sku]    ${2}

Verify that the Simple product display correctly
    [Documentation]    - *Simple Product*    : CDS10774373
    [Tags]    pdp    pdp_ui    smoke_test_revised    testrailid:C996397        support_sit
    Run Keyword And Ignore Error    common_cds_web.Test Setup - Remove all product in shopping cart    ${cds_shopping_cart_username}  ${cds_shopping_cart_password}
    wishlist.Remove all wishlist items in wishlist group    ${cds_shopping_cart_username}  ${cds_shopping_cart_password}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success  ${cds_shopping_cart_username}  ${cds_shopping_cart_password}    ${cds_shopping_cart_information.firstname}
    search_keyword.Search product for go to PDP page by product name    ${${non_cart_price_rule_sku.pdp}.name}

    ${url}=    common_cds_web.Get product urlkey by current url
    ${response}=    pdp_falcon.Api Product PDP By Url Key   ${url}    ${language}    ${BU}
    ${dict_simple_product}=    product_api.Get simple product data    ${response}

    # 1. Data Validation points
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product brand name' should display correctly via api    ${response}
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product name' should display correctly via api    ${response}    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : image' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product overlay image' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : redeem point' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag new / promotion' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag only at / online exclusive' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product price' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : pay by installment' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product id' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product customer reviews' should be displayed via api    ${response}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

    # 2. UI Validation points
    # Social icon
    product_PDP_page.Verify social icon facebook on product PDP page
    product_PDP_page.Verify social icon twitter on product PDP page
    product_PDP_page.Verify social icon email on product PDP page
    product_PDP_page.Verify social icon line on product PDP page

    # Promotion tab
    product_PDP_page.Click 'promotions' tab
    product_PDP_page.Verify 'promotions tab details' should be displayed
    
    # Delivery & Return tab
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Verify 'delivery & return tab details' should be displayed

    # Installment pop up
    product_PDP_page.Click 'pay by installment' button
    product_PDP_page.Verify 'installment pop up' should be displayed
    product_PDP_page.Click 'OK' installment pop up button

    # Wishlist
    product_PDP_page.Verify 'add wishlist button' should be displayed    ${dict_simple_product}[sku]
    product_PDP_page.Click 'add wishlist' button    ${dict_simple_product}[sku]
    product_PDP_page.Verify 'remove wishlist button' should be displayed    ${dict_simple_product}[sku]

    # Add to cart
    common_cds_web.Retry add product to cart, product should be displayed in mini cart    ${dict_simple_product}[sku]    ${2}

Verify that if product has a discount price, a discount price must be displayed on a product preview correctly
    [Documentation]    - *Product*    : Search by brand name, filter discount high-low
    [Tags]    pdp    pdp_ui    smoke_test_revised    testrailid:C996399
    Set Test Variable    ${test_brand_name}    ${search_by_keyword.defry01}
    # get product with discount on plp via api
    ${response}=    search_falcon.Api Search Product By Brand Name And Filter Sort 'Discount High-Low'    ${test_brand_name}    ${language}    ${BU.lower()}    ${1}
    ${dict_random_product}=    JSONLibrary.Get Value From Json    ${response}   $..search.products[0]
    ${dict_random_product}=    Set Variable    ${dict_random_product}[0]

    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PLP page    ${test_brand_name}
    search_keyword.Select sort discount high-low discount
    plp_page.Verify 'product with discount' should be displayed    ${dict_random_product}[sku]
    plp_page.Click Product Name    ${dict_random_product}[sku]

    ${url}=    common_cds_web.Get product urlkey by current url
    ${response}=    pdp_falcon.Api Product PDP By Url Key   ${url}    ${language}    ${BU}
    
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    ${dict_simple_product}=    Run Keyword If    '${dict_random_product}[type_id]' == 'configurable'    product_PDP_keywords.Get configurable's simple product displayed via api   ${response}
    ...    ELSE IF     '${dict_random_product}[type_id]' == 'simple'    product_api.Get simple product data   ${response}
    ...    ELSE    Fail    Incorrect type_id

    product_PDP_keywords.Verify ui for smoke test - 'product details : product price' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Ignore Error    Enable Testability Automatic Wait
