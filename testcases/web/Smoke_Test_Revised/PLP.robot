*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Keywords ***
Open browser for specify url
    ${url}=    Set Variable If     '${BU.lower()}'=='${bu_type.rbs}'    ${rbs_url}    ${central_url}
    CommonWebKeywords.Open Chrome Browser to page  ${url}${catagory_url.women}    ${seleniumSpeed}    headless=${FALSE}    extension_full_path=${CURDIR}/../../../resources/cds_extension/${ENV.lower()}    sleep_loading_extension=2
    Run Keyword If    '${BU.lower()}'=='${bu_type.rbs}'    common_keywords.SignIn Web Authentication

*** Test Cases ***
Verify that a searching by keyword will go to search page and search page UI is displayed with data correctly
    [Tags]    search    search_ui    smoke_test_revised    testrailid:C995861
    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PLP page    ${search_by_keyword.cosmetic}
    search_keyword.Verify ui for smoke test - 'product keyword' should be displayed correctly via api   ${search_by_keyword.cosmetic}

Verify that searching by brand name will go to Normal Brand PLP and Normal Brand PLP UI is displayed with data correctly
    [Tags]    search    search_ui    smoke_test_revised    testrailid:C1005735
    common_cds_web.Setup - Open browser
    ${brand}=    Set Variable If    '${BU.lower()}'=='${bu_type.rbs}'    ${search_by_keyword.aldo}    ${search_by_keyword.adidas}
    shop_by_brand_page.Click on shop by brands category
    shop_by_brand_page.Click on brand    ${brand}
    search_keyword.Verify ui for smoke test - 'product brand name' should be displayed correctly via api    ${brand}

Verify that ui on category page is displayed correctly
    [Tags]    smoke_test_revised    testrailid:C1010765
    common_cds_web.Setup - Open browser
    home_page_web_keywords.Click women category in main menu
    ${sub_category}=    Set Variable If    '${BU.lower()}'=='${bu_type.rbs}'    ${sub_category.dresses}    ${sub_category.clothing}
    category_landing_page.Click subcategory clothing    ${sub_category}
    ${sub_category}=    Set Variable If    '${BU.lower()}'=='${bu_type.rbs}'    ${category_plp.subcategory.dresses}    ${category_plp.subcategory.breadcrumb}
    plp_page.Verify that category PLP is displayed correctly    ${sub_category}

Verify that all attributes, which are set in products, must be displayed on product preview correctly on Category PLP
    [Tags]    smoke_test_revised    testrailid:C996400
    Open browser for specify url
    Run Keyword And Ignore Error    home_page_web_keywords.Close Popup
    category_landing_page.Click subcategory clothing    ${sub_category.clothing}
    plp_page.Verify that category PLP is displayed correctly    ${category_plp.subcategory.breadcrumb}
    Verify ui for smoke test - 'category product' should be displayed correctly via api    ${category_id.woman_clothing}

Verify that if product has a discount price, a discount price must be displayed on a product preview correctly
    [Tags]    search    search_ui    sort    sort_ui    smoke_test_revised    testrailid:C996403
    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PLP page    ${search_by_keyword.nike}
    search_keyword.Select sort discount high-low discount
    search_keyword.Verify ui for smoke test - 'product brand name sort by discount high-low' should be displayed with discount correctly via api   ${search_by_keyword.nike}    ${1}

Verify that standard boutique must be displayed and work correctly
    [Tags]    smoke_test_revised    PLP    PLP_ui    testrailid:C996404
    common_cds_web.Setup - Open browser
    common_cds_web.Go to Shop By Brand page
    shop_by_brand_page.Click on brand    ${product_brand.aveda}
    shop_by_brand_page.Verify product in What's new section is shown correctly    ${product_brand.aveda}
    standard_boutique_page.Click on Shop All Product link    ${product_brand.aveda}
    standard_boutique_page.Verify correct brand name on PLP    ${product_brand.aveda}

Verify that all mandatory filters are visible and search result are correct by sorting and filters
    [Tags]    filter    filter_ui    smoke_test_revised    testrailid:C996411
    Set Test Variable    ${test_keyword}    ${search_by_keyword.lips}
    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PLP page    ${test_keyword}
    # 1. verify filter value 'price range'
    ${min_price}    ${max_price}    plp_api.Get min, max of filter price range by product keyword    ${test_keyword}
    search_keyword.Click filter price range
    plp_page.Verify 'filter price range value' should be displayed correctly    ${min_price}    ${max_price}

    # 2. verify filter value 'brand name'
    ${list_of_brand_name}    plp_api.Get list brand name of filter brand name by product keyword    ${test_keyword}
    search_keyword.Click filter brand name
    search_keyword.Verify 'filter brand name value' should be displayed correctly by list of brand name    ${list_of_brand_name}

    # 3. apply filter 'color'
    ${color}=    plp_api.Get random color of filter color by product keyword   ${test_keyword}
    search_keyword.Select filter color option    ${color}
    search_keyword.Verify ui for smoke test - 'product keyword with filter color' should be displayed correctly via api    ${test_keyword}    ${color}

    # 4. clear filter 'color'
    search_keyword.Click filter color 'clear' button 
    Verify ui for smoke test - 'product keyword' should be displayed correctly via api    ${test_keyword}

Verify that sorting is displayed in UI and FE displays products correctly after sorting price asc on PLP
    [Tags]    sort    sort_ui    smoke_test_revised    testrailid:C1003515
    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PLP page    ${search_by_keyword.adidas}
    search_keyword.Select sort price low-high
    search_keyword.Verify ui for smoke test - 'product brand name and sorting price low to high' should be displayed correctly via api    ${search_by_keyword.adidas}
