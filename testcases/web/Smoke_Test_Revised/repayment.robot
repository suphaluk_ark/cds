*** Settings ***
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

*** Test Cases ***
Verify that a re-payment can be clicked and redirected to a correct page
    [Tags]    smoke_test_revised    repayment_ui    testrailid:C996413
    order_details.Create order for verify order details with credit card incomplete    ${cds_order_status_username_2}    ${cds_order_status_password_2}    ${cds_order_status_information_2.firstname}
    Get order ID from thank you page
    thankyou_page.Verify re-payment button displayed on thank you page
    thankyou_page.Click on re-payment button
    checkout_page.Verify Repayment page is displayed
    Order unsuccessful with credit card
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${increment_id}
    Order_details_page.Verify order status bar is awaiting payment
    Order_details_page.Verify order progress bar is payment
    Order_details_page.Verify re-payment button displayed on order details page
    Order_details_page.Click on re-payment button
    checkout_page.Verify Repayment page is displayed