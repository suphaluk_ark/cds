*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    CommonWebKeywords.Test Teardown
Suite Teardown    Close All Browsers

*** Variables ***
${product_exist}    เสื้อ
${product_not_exist}    abc
${num_page}    2

*** Test Cases ***
To verify page navigation on PLP page can display follow result and working correctly
    [Tags]    jenkins_group1    search    pagination    testrailid:C25284
    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PLP page    ${product_exist}
    search_keyword.Verify pagination working correctly on PLP page    ${num_page}

To verify page navigation on PLP page when no result navigation of page not visible
    [Tags]    jenkins_group1    search    pagination    testrailid:C25285
    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PLP page    ${product_not_exist}
    plp_page.Verify pagination should not be displayed in PLP