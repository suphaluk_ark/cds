*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    CommonWebKeywords.Test Teardown
Suite Teardown    Close All Browsers

*** Variables ***
${store}=    ${store_view.name.en}
@{quantities}    1    1
@{lst_skus_standard_pickup_sku_and_standard_delivery_sku}   ${omni_sku.sku_have_only_standard_pickup}    ${omni_sku.sku_have_only_standard_delivery}
@{lst_skus_standard_pickup_sku_and_full_delivery_sku}    ${omni_sku.sku_have_only_standard_pickup}    ${omni_sku.sku_have_full_delivery}
@{lst_skus_standard_delivery_sku_and_full_delivery_sku}    ${omni_sku.sku_have_only_standard_delivery}    ${omni_sku.sku_have_full_delivery}            
@{lst_skus_standard_pickup_and_2_hours_pickup_sku_and_full_delivery_sku}    ${omni_sku.sku_have_only_standard_pickup_and_2_hours_pickup}    ${omni_sku.sku_have_only_standard_delivery}

*** Keywords ***
Test Setup - Log in, add product to cart and navigate to checkout page
    [Arguments]       ${skus}   
    ${user_token}    ${quote_id}    common_keywords.Add skus to cart and return user token    ${e2e_username_02}    ${e2e_password_02}    ${store}    ${quantities}    ${skus}
    common_keywords.Go To Specific Page By Member    checkout    ${e2e_username_02}    ${e2e_password_02}
    Wait Until Page Loader Is Not Visible

*** Test Cases ***
To guest checkout flow: log in from register page
    [Tags]    checkout    regression    checkout_process       branch9      testrailid:C192699
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    Header Web - Click Login on Header
    Header Web - Click Register Button
    checkout_page.Input member email   ${cds_checkout_process_username}
    checkout_page.Input member password    ${cds_checkout_process_password}
    checkout_page.Click member login
    checkout_page.Verify home page for checkout process
    Wait until page is completely loaded
    checkout_page.Verify cart merged for checkout process    ${1}

 To guest checkout flow: log in with facebook from register page
    [Tags]    checkout    regression    checkout_process   branch9    testrailid:C192700
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    Header Web - Click Login on Header
    Header Web - Click Register Button
    registration_page.Header Web - Click Facebook login Button
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_1]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window locator page title    ${windows_locator_title}
    checkout_page.Verify home page for checkout process
    Wait until page is completely loaded
    checkout_page.Verify cart merged for checkout process    ${1}

To guest checkout flow: Log in from Login bubble
    [Tags]    checkout    regression    checkout_process   branch9    testrailid:C192701
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    Header Web - Click Login on Header
    checkout_page.Input member email   ${cds_checkout_process_username}
    checkout_page.Input member password    ${cds_checkout_process_password}
    checkout_page.Click member login
    checkout_page.Verify home page for checkout process
    Wait until page is completely loaded
    checkout_page.Verify cart merged for checkout process    ${1}

To guest checkout flow: Log in with facebook from Login bubble
    [Tags]    checkout    regression    checkout_process   branch9    testrailid:C192702
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    Header Web - Click Login on Header
    registration_page.Header Web - Click Facebook login Button
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_1]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button

To guest checkout flow: Click on Mini Cart when cart is empty
    [Tags]    checkout    regression    checkout_process   branch9    testrailid:C192703
    Wait Until Page Is Completely Loaded
    checkout_page.click mini cart icon
    checkout_page.Verify member login botton

To guest checkout flow: Click on Mini Cart when cart is not empty
    [Tags]    checkout    regression    checkout_process   branch9    testrailid:C192704
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    checkout_page.click mini cart icon
    header_web_keywords.Verify view cart button should be displayed correctly as a guest

To guest checkout flow: On Shopping Bag page, click on SECURE CHECKOUT button
    [Tags]    checkout    regression    checkout_process   branch9    testrailid:C192705
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Verify log in box text displayed on delivery page

To guest checkout flow: Guest-Login page, click on CHECKOUT AS GUEST button
    [Tags]    checkout    regression    checkout_process   branch9    testrailid:C192706
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    thankyou_page.Click guest login checkout Thank you page
    guest_login_page.Click login as guest
    checkout_page.Verify Delivery Details page should be displayed
    checkout_page.verify product after CHECKOUT AS GUEST     ${CDS11875437.name}

To guest checkout flow: Guest-Login page, log in as member
    [Tags]    checkout    regression    checkout_process   branch9    testrailid:C192707
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    thankyou_page.Click guest login checkout Thank you page
    guest_login_page.Input username, password and click login button at guest login page    ${cds_checkout_process_username}     ${cds_checkout_process_password}
    checkout_page.Verify Delivery Details page should be displayed
    checkout_page.verify product after CHECKOUT AS GUEST     ${CDS11875437.name}

To guest checkout flow: Guest-Login page, log in with facebook
    [Tags]    checkout    regression    checkout_process   branch9    testrailid:C192708
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    Wait until page is completely loaded
    checkout_page.Check Out Page Should Be Visible
    thankyou_page.Click guest login checkout Thank you page
    registration_page.Header Web - Click Facebook login Button
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_1]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window locator page title    ${web_common.windows_locator_checkout_title_cds}
    checkout_page.Verify Delivery Details page should be displayed
    checkout_page.verify product after CHECKOUT AS GUEST     ${CDS11875437.name}

To verify checkout option not show Home Delivery when cart include 1 sku has only Standard Pickup (without 2hr pickup) and 1 sku has only Standard Delivery
    [Tags]    regression    testrailid:C1014084     testrailid:C1029211    Omni_checkout
    [Setup]    Test Setup - Log in, add product to cart and navigate to checkout page    ${lst_skus_standard_pickup_sku_and_standard_delivery_sku}  
    checkout_page.Verify Home Delivery section should not display in shipping 
    
To verify checkout option not show Home Delivery when cart include 1 sku has both Standard Pickup and 2hr pickup and 1 sku has only Standard Delivery
    [Tags]    regression    testrailid:C1014085     testrailid:C1029212    Omni_checkout
    [Setup]    Test Setup - Log in, add product to cart and navigate to checkout page    ${lst_skus_standard_pickup_and_2_hours_pickup_sku_and_full_delivery_sku}    
    checkout_page.Verify Home Delivery section should not display in shipping

To verify checkout option only show common delivery method as Standard Pickup when cart include 1 sku has full delivery options and 1 sku has Standard Pickup 
    [Tags]    regression    testrailid:C1014086              
    [Setup]    Test Setup - Log in, add product to cart and navigate to checkout page    ${lst_skus_standard_pickup_sku_and_full_delivery_sku}
    checkout_page.Verify Standard Pickup option should display at Click & Collect option in shipping
    checkout_page.Verify Home Delivery section should not display in shipping  

To verify checkout option only show common delivery method as Standard Delivery when cart include 1 sku has full delivery options and 1 sku has Standard Delivery
    [Tags]    regression    testrailid:C1014087          
    [Setup]    Test Setup - Log in, add product to cart and navigate to checkout page    ${lst_skus_standard_delivery_sku_and_full_delivery_sku}        
    checkout_page.Verify Standard Delivery option should display at Home Delivery option in shipping

To verify that Out-of-stock SKUs (free items) should not be displayed on the Checkout
    [Tags]    testrailid:C1014500    testrailid:C1012970    Omni_checkout_process       
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with standard pickup    ${e2e_username_10}    ${e2e_password_10}
    ...    ${lst_skus_standard_pickup_sku_and_standard_delivery_sku}    ${quantities}    ${shipping_address_en.store_location_id}    ${store}
    ${free_item_sku}    product_api.Get free item sku in cart    ${user_token}
    checkout_page.Select pick at store option with omni design 
    ${store_data}=    delivery_details_keywords.Click standard pickup active store and return store data    ${response}    ${api_ver3}   
    Wait Until Page Loader Is Not Visible
    checkout_page.Verify product view is not visible in order summary    ${free_item_sku}    