*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown
...    AND    Close Browser
Suite Teardown    Close All Browsers

*** Variables ***
${login_username}    ${cds_checkout_thank_you_username}
${login_password}    ${cds_checkout_thank_you_password}
${login_display_name}    ${cds_checkout_thank_you_information.firstname}
${product_type_sku}=    ${sku_list_with_2hr}[1]
${sku_quantity}    1
@{lst_split_skus}    ${sku_list_with_2hr}[1]    ${sku_list_without_2hr}[0]
@{lst_split_skus_std}    ${sku_standard_delivery}[0]    ${sku_list_with_2hr}[1]
@{lst_split_full_redeem_skus}    ${sku_for_reddem}[0]    ${sku_for_reddem}[1]
@{list_split_with_mkp_sku}    ${sku_for_reddem}[0]    ${sku_list_mkp}[0]
@{lst_qtys}    1    1
${store}=    ${store_view.name.en}
${api_version}=    ${api_ver3}
${invalid_pass_no_number}     Robotabcd@
${invalid_pass_lowercase}     robotabcd@
${invalid_pass_uppercase}     ROBOT@12345
${invalid_pass_less_8_char}     Ro@1
${valid_pass}    Ro@123456

*** Keywords ***
Test setup - register new account
    common_cds_web.Suite Setup - Open browser and load product data file
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Register Button
    registration_page.Input Firstname    ${delivery_registration_info.firstname}
    registration_page.Input Lastname    ${delivery_registration_info.lastname}
    ${randomUder}=  FakerLibrary.email
    registration_page.Input Email    ${randomUder}
    registration_page.Input Password For Register Page    ${delivery_registration_info.password}
    registration_page.Verify PDPA Checkbox is able to clickable
    registration_page.Click Submit Button

Template - To verify 2Hr Pickup Omnichannel checkout flow on UI
    [Arguments]    ${payment_method}
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${sku_list_with_2hr}[1]
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup   ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_method}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_method}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    Verify each section on Thank you page displays correctly

Template - To verify Home Delivery on thank you page
    [Arguments]    ${shipping_method}    ${shipping_carrier}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${sku_list_with_2hr}[1]
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_carrier}    ${shipping_method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_method}    ${shipping_carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${store}
     ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    Verify each section on Thank you page displays correctly

Template - To verify Payment Failed page
    [Arguments]    ${shipping_option}
    Maximize Browser Window
    Go To PDP Directly By Product Sku    ${sku_list_with_2hr}[1]
    Add product to cart and go to the check out page    ${sku_list_with_2hr}[1]
    Select shipping to address option
    Input customer information and shipping address
    Select shipping option while checking out with home delivery    ${shipping_option}
    checkout_page.Click Continue Payment Button
    Payment Page Should Be Displayed
    Select credit card option and input invalid card information    ${invalid_credit_card.invalid_card_name}    ${invalid_credit_card.invalid_card_number}    ${invalid_credit_card.invalid_card_expiry_date}    ${invalid_credit_card.invalid_card_cvv}
    Click Pay now Button
    Verify each section on Failed payment page displays correctly

Template - Go To Thank You Page And Check Input Password Field
    [Arguments]    ${password}    ${err_msg}    ${store}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    Go To Direct Url    checkout/completed/${increment_id}[0]
    thankyou_page.Input password personal details for the registration    ${password}
    thankyou_page.Verify password input field should display error message    ${err_msg}

*** Test Cases ***
To verify 2Hr Pickup Omnichannel checkout flow on UI
    [Tags]    checkout    testrailid:C986505    testrailid:C986504    testrailid:C986503    testrailid:C986502    testrailid:C986500    testrailid:C1010884
    ...    testrailid:C986499    testrailid:C986498    testrailid:C986501    testrailid:C986497    Omni_thankyou
    [Template]    Template - To verify 2Hr Pickup Omnichannel checkout flow on UI
    ${payment_methods.pay_at_store}
    ${payment_methods.bank_transfer}
    ${payment_methods.full_payment}

To verify complete page by guest show correctly details
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C83163
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify bank transfer form thank you page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify order date form thank you page
    thankyou_page.Verify phone number from thank you page  ${change_shipping_address}[telephone]

To verify standard delivery method show correctly on thank you page
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C83422
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    2
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_keywords.Input guest infomation at checkout page
    checkout_keywords.Add shipping address with guest
    checkout_keywords.Verify delivery shipping option are displayed correctly
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    payment_keywords.Thank you page payment with credit card
    thankyou_page.Verify thankyou page display standard delivery
    [Teardown]    common_cds_web.Tear down with cancel order

To verify that complete page by member show section correctly
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C83467
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    thankyou_page.Click guest login checkout Thank you page
    thankyou_page.Login User for checkout Thank you page   ${login_username}    ${login_password}
    checkout_page.Select shipping to address option
    checkout_page.Click on Same-day delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Three Icon Status On Thank You Page
    checkout_page.Thank You Page Should Be Visible
    thankyou_page.Verify ordered successfully messenger
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify print this order details messenger
    thankyou_page.Verify Continue shopping messenger

To verify that complete page by guest show section correctly
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C80263
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify bank transfer form thank you page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify order date form thank you page
    thankyou_page.Verify phone number from thank you page  ${change_shipping_address}[telephone]
    thankyou_page.Verify ordered successfully messenger
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify print this order details messenger
    thankyou_page.Verify Continue shopping messenger

To verify that default value of check box is checked on register section
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C80265
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify subscribe to central newsletters
    thankyou_page.Verify check box subscribe to Central newsletters is checked
    thankyou_page.Verify i agree to central terms of service and privacy policy
    thankyou_page.Verify check box i agree to central terms of service and privacy policy

To validate create your central account now section show detail correctly
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C80264
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Title as "create new your central account now"
    thankyou_page.Verify under title personal details
    thankyou_page.Verify email personal details    ${register_email}
    thankyou_page.Verify password personal details
    thankyou_page.Verify subscribe to central newsletters
    thankyou_page.Verify check box subscribe to Central newsletters is checked
    thankyou_page.Verify i agree to central terms of service and privacy policy
    thankyou_page.Verify check box i agree to central terms of service and privacy policy
    thankyou_page.Verify register for guest

To verify that information of Shipping Details show on complete page correctly based on customer added it via checkout page
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C83483
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select shipping to address option
    change_shipping_address_popup.Input building to update shipping address    ${shipping_address_Guest_1}[building]
    change_shipping_address_popup.Input address no to update shipping address    ${shipping_address_Guest_1}[address]
    change_shipping_address_popup.Input postcode to update shipping address    ${shipping_address_Guest_1}[zip_code]
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify shipping details for guest
    thankyou_page.Verify shipping standard delivery for guest
    thankyou_page.Verify button register for guest

To verify that information of pick up at store show on complete page correctly
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C80273
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify bank transfer form thank you page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify order date form thank you page
    thankyou_page.Verify phone number info from thank you page   ${guest_information_1}[tel]
    thankyou_page.Verify that information of pick up at store show on complete page correctly

To validate order information show information correctly incase pay by credit card
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C83162
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_keywords.Thank you page payment with credit card
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify pay by credit card from thank you page for guest

To verify that Collector info show correctly and match with information of guest
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C80272
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_keywords.Thank you page payment with credit card
    thankyou_page.Verify costomer name for guest   ${guest_information_1.firstname}    ${guest_information_1.lastname}
    thankyou_page.Verify email personal details    ${guest_information_1.email}
    thankyou_page.Verify phone number info from thank you page   ${guest_information_1}[tel]
    [Teardown]    common_cds_web.Tear down with cancel order

To verify that Pickup Location section show information correctly based on guest select from checkout page
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C80274
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_keywords.Thank you page payment with credit card
    thankyou_page.Verify pickup location contact    ${pick_at_store_2.contact}
    thankyou_page.Verify pickup location address
    [Teardown]    common_cds_web.Tear down with cancel order

To verify Pickup at Store method show correctly based on customer selected
    [Tags]    checkout    regression    checkout_thankyou_1    testrailid:C359604
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_keywords.Thank you page payment with credit card
    thankyou_page.Verify pickup location contact    ${pick_at_store_2.contact}
    thankyou_page.Verify pickup location address
    thankyou_page.Verify pay by credit card from thank you page for guest
    thankyou_page.Verify that information of pick up at store show on complete page correctly
    thankyou_page.Verify payment status
    [Teardown]    common_cds_web.Tear down with cancel order

To verify Same-day Delivery method show correctly based on customer selected
    [Tags]    checkout    regression    sameday_delivery    checkout_thankyou_1    testrailid:C359603
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on Same-day delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify bank transfer form thank you page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify same day delivery

To verify Next-day Delivery method show correctly based on customer selected
    [Tags]    checkout    regression    checkout_thankyou    branch10     testrailid:C359600
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[6]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on Next-day delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify bank transfer form thank you page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify next day delivery

To verify that Gift Options field is hided if customer doesn't check on gift wrapping
    [Tags]    checkout    regression    checkout_thankyou    branch10    testrailid:C83420
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    checkout_page.Click gift wrapping checkbox
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select shipping to address option
    change_shipping_address_popup.Input building to update shipping address    ${shipping_address_Guest_1}[building]
    change_shipping_address_popup.Input address no to update shipping address    ${shipping_address_Guest_1}[address]
    change_shipping_address_popup.Input postcode to update shipping address    ${shipping_address_Guest_1}[zip_code]
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify shipping details for guest
    thankyou_page.Verify gift wrapping details

To verify that T1C Redemption field is hided if customer doesn't pay with T1C redeem points
    [Tags]    checkout    regression    checkout_thankyou    branch10    testrailid:C83421    testrailid:C83419
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify bank transfer form thank you page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify order date form thank you page
    checkout_page.Get grand total from order summary

To verify that Payment summary show information correctly if guest user promo code, gift option, T1C redemption and pickup at store
    [Tags]    checkout    regression    checkout_thankyou    branch10    testrailid:C83389    full_redeem
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    checkout_page.Click gift wrapping checkbox
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Input T1C point    ${t1c_information_2.point}
    checkout_page.Click Apply Redeem
    checkout_page.Click Pay now Button
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify order date form thank you page
    thankyou_page.Verify shipping details for guest
    thankyou_page.Verify gift wrapping details
    checkout_page.Get grand total from order summary
    thankyou_page.Verify pickup location contact    ${pick_at_store_2.contact}
    thankyou_page.Verify pickup location address
    thankyou_page.Verify that information of pick up at store show on complete page correctly
    thankyou_page.Verify payment T1 full redeem type
    [Teardown]    common_cds_web.Tear down with cancel order

To verify that Full Tax Invoice Details show correctly as guest input information on checkout page
    [Tags]    checkout    regression    checkout_thankyou    branch10    testrailid:C83106    full_redeem
    [Setup]    Setup - For guest, create product data file and get customer profile
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    delivery_details_keywords.Input cutomer contact information
    checkout_page.Select shipping to address option
    delivery_details_keywords.Input shipping address information
    delivery_details_page.Click request tax invoice
    delivery_details_page.Click billing invoice option
    delivery_details_keywords.Input tax invoice address information    ${tax_type_company}
    checkout_page.Click on standard delivery button
    delivery_details_page.Click continue to payment button
    payment_keywords.Verify shipping address is display on shipping summary correctly
    payment_keywords.Verify tax invoice address is display on shipping summary correctly
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Click connect to T1C link
    checkout_page.Login to T1C account    ${t1c_information_2}[email]    ${t1c_information_2}[password]
    checkout_page.Click apply point after login T1C
    checkout_page.Select on full redeem point
    checkout_page.Click apply T1C point
    checkout_page.Total redeem point and discount for the 1 redemption should be    ${200}    ${25}
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    thankyou_keywords.Verify shipping address in thank you page is displayed correctly    ${thankyou_page}[standard_delivery]
    thankyou_keywords.Verify billing address in thank you page is displayed correctly for company
    thankyou_keywords.Verify shipping address in thank you page is dispayed consistency with MDC
    thankyou_keywords.Verify billing address in thank you page is dispayed consistency with MDC    ${tax_type_company}
    [Teardown]    common_cds_web.Tear down with cancel order

To validate order information show information correctly incase pay by full Redeem point
    [Tags]    checkout    regression    checkout_thankyou    branch10    testrailid:C80233    full_redeem
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Click apply point after login T1C
    checkout_page.Select on full redeem point
    checkout_page.Click apply T1C point
    checkout_page.Total redeem point and discount for the 1 redemption should be    ${200}    ${25}
    checkout_page.Click Pay now Button
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify order date form thank you page
    thankyou_page.Verify payment T1 full redeem type
    thankyou_page.Verify phone number from thank you page  ${change_shipping_address}[telephone]
    thankyou_page.Verify T1 card no    ${t1c_information_2.t1c_number}
    [Teardown]    common_cds_web.Tear down with cancel order

To validate order information show information correctly incase pay by Redeem point and Bank Transfer
    [Tags]    checkout    regression    checkout_thankyou    branch10    testrailid:C83153
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Click apply point after login T1C
    checkout_page.Input T1C point    ${25}
    checkout_page.Click apply T1C point
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify order date form thank you page
    thankyou_page.Verify bank transfer form thank you page
    thankyou_page.Verify phone number from thank you page  ${change_shipping_address}[telephone]
    thankyou_page.Verify T1 card no    ${t1c_information_2.t1c_number}

To validate order information show information correctly incase pay by Redeem point and credit card
    [Tags]    checkout    regression    checkout_thankyou    branch10    testrailid:C83145
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[9]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Click apply point after login T1C
    checkout_page.Input T1C point    ${50}
    checkout_page.Click apply T1C point
    payment_keywords.Thank you page payment with credit card
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify order information form thank you page
    thankyou_page.Verify order date form thank you page
    thankyou_page.Verify payment credit card type
    thankyou_page.Verify phone number from thank you page  ${change_shipping_address}[telephone]
    thankyou_page.Verify T1 card no    ${t1c_information_2.t1c_number}
    [Teardown]    common_cds_web.Tear down with cancel order

To verify that purchased items show correctly
    [Tags]    checkout    regression    checkout_thankyou    branch10    testrailid:C80266    testrailid:C80267    full_redeem
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    checkout_page.Click gift wrapping checkbox
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Click apply point after login T1C
    checkout_page.Select on full redeem point
    checkout_page.Click apply T1C point
    checkout_page.Total redeem point and discount for the 1 redemption should be    ${200}    ${25}
    checkout_page.Click Pay now Button
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify purchased items title
    thankyou_page.Verify purchased items image    ${product_img.CDS11008187}
    thankyou_page.Verify purchased items quantity    ${1}    ${CDS11008187.sku}
    thankyou_page.Verify purchased items gift wrapping    ${CDS11008187.sku}
    thankyou_page.Verify payment T1 full redeem type
    thankyou_page.Verify phone number from thank you page  ${change_shipping_address}[telephone]
    thankyou_page.Verify T1 card no    ${t1c_information_2.t1c_number}
    [Teardown]    common_cds_web.Tear down with cancel order

To verify that purchased items show correctly if have free item for whole cart and free item price is zero
    [Tags]    checkout    regression    checkout_thankyou     branch10   testrailid:C80271    testrailid:C80269    testrailid:C80268    testrailid:C80270
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[20]
    checkout_page.Click gift wrapping checkbox
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Three Icon Status On Thank You Page
    checkout_page.Thank You Page Should Be Visible
    thankyou_page.Verify purchased items title
    thankyou_page.Verify purchased items product name     ${CDS14751875.sku}     ${CDS14751875.name}
    thankyou_page.Verify purchased items image    ${product_img.CDS14751875}
    thankyou_page.Verify purchased items quantity    ${1}    ${CDS14751875.sku}
    thankyou_page.Verify purchased items gift wrapping    ${CDS14751875.sku}
    thankyou_page.Verify purchased items free
    thankyou_page.Verify purchased items free product name    ${CDS13960865}[sku]    ${CDS13960865}[name]
    thankyou_page.Verify purchased items image free    ${product_img.CDS13960865}
    thankyou_page.Verify purchased items gift wrapping free    &{CDS13960865}[sku]

To verify that the Thank you page for Guest Registration show correctly
    [Tags]    checkout    regression    checkout_thankyou   testrailid:C963192    testrailid:C963189    testrailid:C963188    testrailid:C963190    testrailid:C973981    testrailid:C973985    testrailid:C973986    Omni_thankyou
    ${checkout_delivery_username}=    FakerLibrary.Email
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    checkout.Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.cod}    ${checkout_delivery_username}    ${store}
    ${enity_id}=    checkout.Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.cod}    ${checkout_delivery_username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_keywords.Verify registration section for guest checkout at thank you page    ${checkout_delivery_username} 
    thankyou_keywords.Verify account registration for guest user successful    ${valid_pass}
    [Teardown]    common_cds_web.Tear down with cancel order

To verify Click and Collect on thank you page
    [Tags]    checkout    regression    checkout_thankyou    testrailid:C963288    testrailid:C963295    testrailid:C963301    testrailid:C963298
    ...    testrailid:C963299    testrailid:C963300    testrailid:C963359    testrailid:C963360    testrailid:C963361    testrailid:C963362    testrailid:C963363    Omni_thankyou
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${sku_list_with_2hr}[0] 
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content   ${quote_id}    ${shipping_methods.click_n_collect.method}    ${shipping_methods.click_n_collect.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.pay_at_store}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.pay_at_store}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    Verify Click and Collect section at thank you page
    [Teardown]    common_cds_web.Tear down with cancel order

To verify Home Delivery on thank you page
    [Tags]    Omni_thankyou    testrailid:C980439    testrailid:C980440    testrailid:C980443    testrailid:C980449    testrailid:C980450
    ...    testrailid:C980452    testrailid:C980451    testrailid:C980453    testrailid:C980441    testrailid:C980442    testrailid:C980444    
    ...    testrailid:C980445    testrailid:C980446    testrailid:C980447    testrailid:C980448    testrailid:C980454
    [Template]    Template - To verify Home Delivery on thank you page
    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}

To verify Payment Failed page
    [Tags]    Omni_thankyou    testrailid:C980470    testrailid:C980472    testrailid:C980473    testrailid:C980474    testrailid:C980475
    [Template]    Template - To verify Payment Failed page
    ${filters.shipping_options.standard_delivery}

To Guest CheckOut: Disable Email Input
    [Tags]    regression    checkout_thankyou     testrailid:C984479    Omni_thankyou
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    checkout_page.Thank You Page Should Be Visible
    thankyou_page.verify Email Input Field Disable

To Verify Checkout With Design Enhancement After Add Coupon For Click And Collect
    [Tags]    Omni_thankyou    testrailid:C980432    testrailid:C980433    testrailid:C980434    testrailid:C980436
    checkout_keywords.Place pay at store order by api and login by ui    ${e2e_username_11}    ${e2e_password_11}    ${product_type_sku}    10    ${coupon.automation10}
    payment_keywords.Verify payment type and payment status in Thank you page after pay by pay at store with new design
    payment_keywords.Store payment status in Thank you page
    thankyou_page.Verify customer information displayed on thank you page with new design
    calculation.Calculate discount(%) from original total price    ${${product_type_sku}.price}    ${10}
    checkout_page.Verify order total in thank you page with new design    ${${product_type_sku}.price}    ${discount_amount}
    thankyou_page.Click continue shopping button
    Wait Until Page Is Completely Loaded
    order_details.Go to order detail page and verify order status    ${oder_id}    ${order_status}

To verify Pickup Instruction at the Thank You page
    [Tags]    regression    checkout_thankyou    Omni_thankyou    testrailid:C989971    testrailid:C991259    testrailid:C991265    testrailid:C991266    testrailid:C991276    testrailid:C991274
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Get order ID after completely order
    thankyou_page.Verify Stock Availability Notice Should Be Visible
    thankyou_page.Verify How To PickUp Title Should Be Visible
    thankyou_page.Verify Pickup Instruction Should Be Visible

To Verify That Use Same Validation For Email And Password That We Use On Current Product Website.
    [Tags]    checkout_thankyou    Omni_thankyou    testrailid:C980381    testrailid:C980393
    [Template]    Template - Go To Thank You Page And Check Input Password Field
    ${invalid_pass_no_number}     ${error_message.guest_login.invalid_password}    ${store}
    ${invalid_pass_lowercase}     ${error_message.guest_login.invalid_password}    ${store}
    ${invalid_pass_uppercase}     ${error_message.guest_login.invalid_password}    ${store}
    ${invalid_pass_less_8_char}   ${error_message.guest_login.invalid_password}    ${store}

To Verify That Register Fail When Email Has Already Register
    [Tags]    checkout_thankyou    Omni_thankyou    testrailid:C980382    testrailid:C980387    testrailid:C980385    testrailid:C980387    testrailid:C980392    testrailid:C980383
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Verify create account section are display correctly
    thankyou_page.Input password personal details for the registration    ${valid_pass}
    thankyou_page.Click button register for guest
    thankyou_page.Verify the duplicate email message should be visible    ${error_message.account_registration.email_existed}
    thankyou_page.Click on continue shopping button
    home_page_web_keywords.Verify homepage page should be displayed

To Verify Create Account Successfully
    [Tags]    checkout_thankyou    Omni_thankyou    testrailid:C980384    testrailid:C980386    testrailid:C980389    testrailid:C980390    testrailid:C980396
    ...    testrailid:C980395    testrailid:C980397
    ${checkout_delivery_username}=    FakerLibrary.Email
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${checkout_delivery_username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${checkout_delivery_username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id} 
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Verify the order is completed
    thankyou_page.Verify the each section are display correctly    ${thankyou_page.status_awaiting_payment}    ${thankyou_page.payment_transfer}
    thankyou_page.Input password personal details for the registration    ${valid_pass}
    thankyou_page.Click button register for guest
    thankyou_page.Verify account create successfully   ${success_message.thank_you.create_account_success}

To Verify Payment Failed Page By Credit Cart
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke    testrailid:C990808    testrailid:C990807    testrailid:C980473    testrailid:C990811
    ...    testrailid:C1010927
    ${response}    ${user_token}    Login, add product to cart, return and go to checkout delivery page with 2hr pickup    ${login_username}    ${login_password}
    ...    ${sku_list_with_2hr}[1]    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Click on cancel payment button
    checkout_page.Verify section on payment failed page after click cancel otp
    checkout_page.Click pay now button at payment failed page
    checkout_page.Verify Repayment page is displayed
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    thankyou_page.Verify the each section are display correctly    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order By Credit Cart
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke    testrailid:C1010873
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    checkout_page.Verify order total in thank you page with split order    ${lst_split_skus}    ${lst_qtys}    discount=${50}
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order By Bank Transfer
    [Tags]    checkout_thankyou     smoke_test_revised    Omni_thankyou
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method 1 2 3 bank transfer
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${personal_information.tel}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    bank_transfer.Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button
    Wait Until Page Loader Is Not Visible
    checkout_page.Verify order total in thank you page with split order    ${lst_split_skus}    ${lst_qtys}    discount=${50}
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_awaiting_payment}    ${payment_page.payment_banktransfer}

To Verify Payment Successfully With Split Order - T1C Partial Redemption
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke    testrailid:C1016305
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    ${response_redeem}=    checkout.Log in with The1 account   ${user_token}    ${t1c_information_4.email}    ${t1c_information_4.password}
    ${total_point}    ${point}    ${baht}    checkout_page.Get redeem point from account T1C    ${response_redeem}
    ${discount_redeem}=    E2E_flow_keywords.Get Partial redeem point and verify T1C discount rule and pay by full payment    ${total_point}    ${point}    ${baht}    ${40}    tc1_email=${t1c_information_4.email}    tc1_pwd=${t1c_information_4.password}
    checkout_page.Verify order total in thank you page with split order   ${lst_split_skus}    ${lst_qtys}    discount=${50}    redeem=${discount_redeem}
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order - T1C Full Redemption
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke    testrailid:C1016302
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_full_redeem_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_keywords.Full redeem point and verify T1C point discount    ${t1c_information_4.email}    ${t1c_information_4.password}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click continue payment for full redeemtion
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_redeem_point}
    checkout_page.Verify order total in thank you page    ${0}

To Verify Payment Successfully With 2 Hour Pickup - T1C Partial Redemption
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke    testrailid:C1016300
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with 2hr pickup    ${e2e_username_10}    ${e2e_password_10}
    ...    ${sku_list_with_2hr}[1]    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    ${response_redeem}=    checkout.Log in with The1 account   ${user_token}    ${t1c_information_4.email}    ${t1c_information_4.password}
    ${total_point}    ${point}    ${baht}    checkout_page.Get redeem point from account T1C    ${response_redeem}
    ${discount_redeem}=    E2E_flow_keywords.Get Partial redeem point and verify T1C discount rule and pay by full payment    ${total_point}    ${point}    ${baht}    ${80}    tc1_email=${t1c_information_4.email}    tc1_pwd=${t1c_information_4.password}
    checkout_page.Verify order total in thank you page with new design   ${${sku_list_with_2hr}[1].price}    redeem=${discount_redeem}

To Verify Payment Successfully With 2 Hour Pickup - T1C Full Redemption
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke    testrailid:C1016297
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with 2hr pickup    ${e2e_username_10}    ${e2e_password_10}
    ...    ${sku_for_reddem}[0]    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_keywords.Full redeem point and verify T1C point discount    ${t1c_information_4.email}    ${t1c_information_4.password}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click continue payment for full redeemtion
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_redeem_point}
    checkout_page.Verify order total in thank you page    ${0}

To Verify Split Order With MKP Product
    [Tags]    checkout_thankyou    Omni_thankyou    testrailid:C1016301    Omni_smoke
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${list_split_with_mkp_sku}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order - WMS flow
    [Tags]    checkout_thankyou    Omni_thankyou
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    thankyou_page.Get order ID and sub order ID from thank you page
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}
    common_cds_web.Retry verify that order create on WMS success    ${increment_id}
    kibana_api.Retry ESB update shipment status to shipped by sku    ${increment_id}    ${sku_list_with_2hr}[1]    source=pickingtool
    kibana_api.Retry ESB update shipment status to delivered by sku    ${increment_id}    ${sku_list_with_2hr}[1]    source=pickingtool
    common_cds_web.Verify order status from MCOM should be 'COMPLETE, FULLYSHIPPED'   ${increment_id}
    common_cds_web.Verify order status from MDC should be 'COMPLETE, FULLYSHIPPED'    ${increment_id}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}
    common_cds_web.Retry verify that order create on WMS success    ${increment_sub_id}
    kibana_api.Retry ESB update shipment status to shipped by sku    ${increment_sub_id}   ${sku_list_without_2hr}[0]
    kibana_api.Retry ESB update shipment status to delivered by sku    ${increment_sub_id}   ${sku_list_without_2hr}[0]
    common_cds_web.Verify order status from MCOM should be 'COMPLETE, FULLYSHIPPED'   ${increment_sub_id}
    common_cds_web.Verify order status from MDC should be 'COMPLETE, FULLYSHIPPED'    ${increment_sub_id}

To Verify Payment Successfully With Split Order - Apply Coupon Code
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised   testrailid:C994717    testrailid:C994715
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to cart page with split order     ${e2e_username_11}    ${e2e_password_11}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    my_cart_page.Apply coupon code    ${coupon.automation10}
    ${discount_price}=    checkout_page.Get discount price by apply coupon    ${coupon.automation10}
    checkout_page.Click gift wrapping checkbox
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    thankyou_page.Verify gift wrapping details
    checkout_page.Verify order total in thank you page with split order   ${lst_split_skus}    ${lst_qtys}    discount=${discount_price}
    thankyou_page.Verify coupon code display correctly    ${coupon.automation10}

To Verify Payment Successfully With Split Order - 2 Hour Pickup & Standard Delivery
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke
    ...    testrailid:C994712    testrailid:C1010872
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order     ${e2e_username_11}    ${e2e_password_11}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.click Standard delivery sub header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    checkout_page.Verify order total in thank you page with split order   ${lst_split_skus}    ${lst_qtys}
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order - Standard Pickup & Standard Delivery
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke    testrailid:C1010874
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with standard pickup     ${e2e_username_11}    ${e2e_password_11}    ${lst_split_skus_std}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click standard pickup active store and return store data   ${response}    ${api_ver3}
    Wait Until Page Loader Is Not Visible
    delivery_details_page.Verify the continue payment button enable
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}