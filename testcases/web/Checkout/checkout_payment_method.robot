*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser
Suite Teardown    Close All Browsers

*** Variables ***
${login_username}    ${cds_checkout_payment_username}
${login_password}    ${cds_checkout_payment_password}
${login_display_name}    ${cds_checkout_payment_information.firstname}

*** Test Cases ***
To verify that user selects a bank to transfer and clicks "Pay Now" button
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C88726
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${register_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${register_information.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member

To verify payment method when order total is less than minimum order total for COD payment
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C94548
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    login_keywords.Login Keywords - Login Success    ${login_username}    ${login_password}    ${login_display_name}
    Wait Until Page Is Completely Loaded
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Verify Payment Method COD should not be displayed

To verify payment method when order total is equal or more than minimum order and less than maximum total for COD payment
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C94549    testrailid:C663534
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    login_keywords.Login Keywords - Login Success    ${login_username}    ${login_password}    ${login_display_name}
    Wait Until Page Is Completely Loaded
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method COD
    Wait Until Page Is Completely Loaded
    checkout_page.Click Confirm Order For COD Payment Method
    [Teardown]    common_cds_web.Tear down with cancel order

To verify payment method when order total is equal or more than maximum order total for COD payment
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C663531
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    login_keywords.Login Keywords - Login Success    ${login_username}    ${login_password}    ${login_display_name}
    Wait Until Page Is Completely Loaded
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[4]    3
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Verify Payment Method COD should not be displayed

To verify that 2c2p page is shown if customer select payment method as Credit card
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49047
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    [Teardown]    common_cds_web.Tear down with cancel order

To validate payment as credit card show detail in column correctly
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49029
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${login_username}  ${login_password}
    login_keywords.Login Keywords - Login Success  ${login_username}  ${login_password}  ${login_display_name}
    Wait Until Page Is Completely Loaded
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Verify payment icon of full payment method should be displayed
    payment_page.Verify on top discount icon shows beside Credit Card (Full Payment) should be displayed
    payment_page.Verify radio button with text as Credit Card (Full Payment) should be displayed

To verify that customer got order detail on complete page after pay by credit card is successful
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49145
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    2
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    thankyou_page.Verify purchased items quantity    2    ${product_sku_list}[2]
    thankyou_page.Verify purchased items product name     ${product_sku_list}[2]     ${${product_sku_list}[2]}[name]
    [Teardown]    common_cds_web.Tear down with cancel order

To validate "SHIPPING SUMMARY" on payment page show all fields correctly
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49178
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Verify shipping summary title should be displayed
    payment_page.Verify shipping name should be displayed
    payment_page.Verify shipping address should be displayed
    payment_page.Verify shipping telephone should be displayed
    payment_page.Verify billing summary title should be displayed
    payment_page.Verify billing name should be displayed
    payment_page.Verify billing address should be displayed
    payment_page.Verify billing telephone should be displayed
    payment_page.Verify edit shipping summary should be displayed

To verify that shipping address show information on payment page correctly based on selected from delivery page
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49180
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Verify shipping address shows information on payment page correctly  ${guest_checkout_payment_information.firstname} ${guest_checkout_payment_information.lastname}  ${pick_at_store.central_bangna_address}  ${guest_checkout_payment_information}[tel]

To verify that billing address show information on payment page correctly based on selected from delivery page
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49182
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    ${cust_billing_address}=  Set Variable  ${guest_contact_info}[tax_invoice_building], ${guest_contact_info}[tax_invoice_address_no], ${tax_invoice_district}, ${tax_invoice_subdistrict}, ${tax_invoice_province}, ${guest_contact_info}[tax_invoice_postcode]
    payment_page.Verify billing address shows information on payment page correctly  ${guest_checkout_payment_information.firstname} ${guest_checkout_payment_information.lastname}  ${cust_billing_address}  ${guest_checkout_payment_information}[tel]

To verify that customer can edit shipping address on payment page
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49183
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Click Edit shipping address link
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select shipping to address option
    checkout_page.Input shipping building  ${guest_contact_info_update.shipping_building}
    checkout_page.Input shipping street  ${guest_contact_info_update.shipping_address_no}
    checkout_page.Input shipping postcode  ${guest_contact_info_update.shipping_postcode}
    Wait Until Page Is Completely Loaded
    checkout_page.Select region    ${region.bkk}
    checkout_page.Select district   ${district.district_id_16}
    checkout_page.Select Subdistrict    ${subdistrict.subdistrict_id_80}
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    checkout_page.Click Continue Payment Button
    ${shipping_address}=  Set Variable  ${guest_contact_info_update.shipping_building}, ${guest_contact_info_update.shipping_address_no}, ${subdistrict.subdistrict_id_80}, ${district.district_id_16}, ${region.bkk}, ${guest_contact_info_update.shipping_postcode}
    payment_page.Verify shipping address shows information on payment page correctly  ${guest_checkout_payment_information.firstname} ${guest_checkout_payment_information.lastname}  ${shipping_address}  ${guest_checkout_payment_information}[tel]

To verify that customer can edit billing address on payment page
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49184
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Click Edit shipping address link
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Update tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    ${cust_billing_address}=  Set Variable  ${guest_contact_info_update}[tax_invoice_building], ${guest_contact_info_update}[tax_invoice_address_no], ${tax_invoice_district}, ${tax_invoice_subdistrict}, ${tax_invoice_province}, ${guest_contact_info_update}[tax_invoice_postcode]
    payment_page.Verify billing address shows information on payment page correctly  ${guest_checkout_payment_information.firstname} ${guest_checkout_payment_information.lastname}  ${cust_billing_address}  ${guest_checkout_payment_information}[tel]

To verify that customer can add billing address on payment page in case not select billing address on delivery page before
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49185
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Click Edit shipping address link
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    ${cust_billing_address}=  Set Variable  ${guest_contact_info}[tax_invoice_building] ${guest_contact_info}[tax_invoice_address_no], ${tax_invoice_district}, ${tax_invoice_subdistrict}, ${tax_invoice_province} ${guest_contact_info}[tax_invoice_postcode]
    payment_page.Verify billing address shows information on payment page correctly  ${guest_checkout_payment_information.firstname} ${guest_checkout_payment_information.lastname}  ${cust_billing_address}  ${guest_checkout_payment_information}[tel]

To validate "ORDER SUMMARY" on payment page show all fields correctly
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49194
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[15]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Verify product name should be displayed  ${product_sku_list}[15]
    payment_page.Verify product image should be displayed  ${product_img.CDS11082736}
    payment_page.Verify product quanity should be displayed  ${product_sku_list}[15]
    payment_page.Verify product price should be displayed  ${product_sku_list}[15]

To verify that item in "ORDER SUMMARY" section show correctly same as delivery page
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49195
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]  10
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Verify product quantity should be the same as delivery page  ${product_sku_list}[0]  10

To verify that shopping bag page is shown if customer click "Edit Bag" in Order summary section
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49196
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]  10
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Click Edit Bag in Order Summary
    my_cart_page.Wait Until Page Loader Is Not Visible
    my_cart_page.Verify quantity of product should be displayed correctly  ${product_sku_list}[0]  10

To validate "PAYMENT SUMMARY" on payment page show all fields correctly
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49197
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]  10
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Verify payment summary title should be displayed
    payment_page.Verify total text should be displayed from payment summary
    payment_page.Verify order total text should be displayed from payment summary
    payment_page.Verify total should displayed in correct format
    payment_page.Verify order total should displayed in correct format

To verify that Value of total is total amount before add fee or discount
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49198
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]  10
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    my_cart_page.Wait Until Page Loader Is Not Visible
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Verify subtotal is calculated correctly by sku  ${product_sku_list}[0]

To verify that Value of Order Total is total amount after calculate of fee and discount
    [Tags]    checkout    regression    checkout_payment_method    testrailid:C49199
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]  10
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    my_account_page.Click and input text for first name on address book page    ${guest_checkout_payment_information.firstname}
    my_account_page.Click and input text for last name on address book page    ${guest_checkout_payment_information.lastname}
    registration_page.Input guest email    ${guest_checkout_payment_information.email}
    registration_page.Input guest telephone    ${guest_checkout_payment_information}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Verify subtotal should be equal grand total on payment summary
