*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

Suite Teardown    Close All Browsers

*** Variables ***
${login_username}    ${personal_suite_sit.username}
${login_password}    ${personal_suite_sit.password}
${login_display_name}    ${personal_suite_sit.firstname}

*** Test Cases ***
To verify that PDPA checkbox must be unticked by default for register page
    [Tags]    jenkins_group1    regression    pdpa    register    testrailid:C877868
    [Setup]    common_cds_web.Setup - Open browser
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Register Button
    registration_page.Verify PDPA Checkbox is unselected as a defaut
    registration_page.Verify PDPA Checkbox is able to clickable
    registration_page.Verify pada text message is dislay correctly

To verify that PDPA checkbox must be unticked by default for delivery detail page
    [Tags]    jenkins_group1    regression    pdpa    delivery_detail    testrailid:C877866
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${login_username}    ${login_password}    ${login_display_name}
    checkout_keywords.Add product to cart and go to checkout page    ${product_sku_list}[4]
    checkout_keywords.Verify delivery options are displayed correctly
    registration_page.Verify PDPA Checkbox is unselected as a defaut
    registration_page.Verify PDPA Checkbox is able to clickable
    registration_page.Verify pada text message is dislay correctly

To verify that PDPA checkbox must be unticked by default for guest in delivery detail page
    [Tags]    jenkins_group1    regression    pdpa    delivery_detail    testrailid:C877867
    [Setup]    common_cds_web.Setup - Open browser
    checkout_keywords.Add product to cart and go to checkout page    ${product_sku_list}[4]
    checkout_keywords.Verify delivery options are displayed correctly
    registration_page.Verify PDPA Checkbox is unselected as a defaut
    registration_page.Verify PDPA Checkbox is able to clickable
    registration_page.Verify pada text message is dislay correctly