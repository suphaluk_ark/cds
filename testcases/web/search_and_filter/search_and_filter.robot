*** Settings ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Variables ***
${keyword_1}   pillow
${keyword_2}    beauty and the beast
${keyword_3}    beauty
${brand_name}    burt's bees
${brand_name_2}    sony
${color}     pink
${size}    100
${product_1_th}  DISNEY หมอนอิง Beauty and the Beast รุ่น DPC503050
${product_1_en}  DISNEY Beauty and the Beast Cushion Pillow DPC503050
${product_2_th}  DP Air Jordan OW chicaco
${product_2_en}  DP Air Jordan OW chicaco
${2_characters}  be
${3_characters}  bea
${random_characters}    asas231121zaA$
${recommendation_keyword_1}    beauty
${recommendation_keyword_2}    bear
${recommendation_keyword_3}    beauty cosmetic
${nike}    nike
${url_api_search_keyword}    https://staging.central.co.th/api/search/improvement?lang=en&keyword=${nike}
${url_web_search_keyword}    https://staging.central.co.th/en/search/${nike}


*** Test Cases ***
To verify the filter category when select Thai or English language
    [Tags]    jenkins_group1    regression    filter_product    testrailid:C72417
    search_keyword.Search product for go to PLP page  ${keyword_2}
    header_web_keywords.Switch to English language
    plp_page.Verify product name should be displayed correctly in search product  ${product_1_en}
    header_web_keywords.Switch to Thai language
    plp_page.Verify product name should be displayed correctly in search product  ${product_1_th}

To verify that user clicks at filter category
    [Tags]    jenkins_group1    regression    filter_product    testrailid:C72418
    product_category_page.Move over a main category on menu  ${product_category.main_category.home}
    product_category_page.Click on a sub-category on Category filter  ${home_category.home_appliances}
    plp_page.Verify that color of Filter should be red
    plp_page.Click on brand name arrow-down button
    plp_page.Click on a brand name  ${brand_name_2}
    plp_page.Verify that color of Brand name should be red
    plp_page.Click on brand name arrow-down button
    plp_page.Click on a brand name  ${brand_name_2}
    plp_page.Verify that color of Brand name should be not red

To verify that user clicks at sub categories
    [Tags]    jenkins_group1    regression    filter_product    testrailid:C72419
    product_category_page.Click on a main category name  ${product_category.main_category.kids}
    product_category_page.Click on a sub-category on pop-up  ${kid_category.baby_essentials}
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.kids.baby_sleeping}
    plp_page.Verify sub category should be not displayed from the category list  ${product_category.kids.baby_carriers}
    plp_page.Click on a category  ${product_category.kids.prams_travel}
    plp_page.Verify sub category should be displayed from the category list  ${product_category.kids.baby_carriers}

To verify that user clicks at the last sub categories
    [Tags]    jenkins_group1    regression    filter_product    testrailid:C72421
    product_category_page.Click on a main category name  ${product_category.main_category.kids}
    product_category_page.Click on a sub-category on pop-up  ${kid_category.baby_essentials}
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.kids.baby_sleeping}
    plp_page.Verify red check mark should be displayed  ${product_category.kids.baby_sleeping}
    plp_page.Click on a category  ${product_category.kids.pillows_blanket}
    product_category_page.Verify last sub category direct to correct page  ${product_category.kids.pillows_blanket}

To verify that user clicks a check mark to select the sub category
    [Tags]    jenkins_group1    regression    filter_product    testrailid:C72422
    product_category_page.Click on a main category name  ${product_category.main_category.kids}
    product_category_page.Click on a sub-category on pop-up  ${kid_category.baby_essentials}
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.kids.baby_sleeping}
    plp_page.Verify red check mark should be displayed  ${product_category.kids.baby_sleeping}
    plp_page.Click on a category  ${product_category.kids.baby_sleeping}
    plp_page.Verify red check mark should be not displayed  ${product_category.kids.baby_sleeping}

To verify that check mark must be displayed correctly
    [Tags]    jenkins_group1    regression    filter_product    testrailid:C72423
    product_category_page.Click on a main category name  ${product_category.main_category.kids}
    product_category_page.Click on a sub-category on pop-up  ${kid_category.baby_essentials}
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.kids.baby_sleeping}
    plp_page.Verify red check mark should be displayed  ${product_category.kids.baby_sleeping}
    plp_page.Click on brand name arrow-down button
    plp_page.Click on category arrow-down button
    plp_page.Verify red check mark should be displayed  ${product_category.kids.baby_sleeping}
    plp_page.Click on a category  ${product_category.kids.prams_travel}
    plp_page.Verify red check mark should be displayed  ${product_category.kids.prams_travel}
    plp_page.Click on brand name arrow-down button
    plp_page.Click on category arrow-down button
    plp_page.Verify red check mark should be displayed  ${product_category.kids.prams_travel}

To verify the sub categories that there is no product in that sub categories
    [Tags]    jenkins_group1    regression    filter_product    testrailid:C72424
    product_category_page.Click on a main category name  ${product_category.main_category.kids}
    product_category_page.Click on a sub-category on pop-up  ${kid_category.baby_essentials}
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.kids.baby_sleeping}
    plp_page.Verify sub category should be not displayed from the category list  ${product_category.kids.mattresses_bed_protector_sheet}

To verify that if any option in filter is seleted on PLP
    [Tags]    jenkins_group1    regression    filter_product    testrailid:C117750
    search_keyword.Search product for go to PLP page  ${keyword_3}
    plp_page.Click on sorter arrow-down button
    plp_page.Select New arrivals from sorter
    plp_page.Verify that color of Filter should be red
    plp_page.Click on price range arrow-down button
    plp_page.Click on brand name arrow-down button
    plp_page.Click on a brand name  ${brand_name}
    plp_page.Verify that color of Brand name should be red
    plp_page.click on size arrow-down button
    plp_page.Click on a size  ${size}
    plp_page.Verify that color of Size should be red
    plp_page.Click on color arrow-down button
    plp_page.Click on a color  ${color}
    plp_page.Verify that color of Color should be red

To verify that Trending Search is hidden from Website
    [Tags]    jenkins_group1    regression    search_product    search_result    testrailid:C143079
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${keyword_3}
    plp_page.Verify trending search should be not displayed

To verify that result search when user type < 3 characters, system should not show result search
    [Tags]    jenkins_group1    regression    search_product    search_popup    testrailid:C23947
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${2_characters}
    plp_page.Verify product image should be not displayed in popup

To verify that result search when user type => 3 character, system should display result search
    [Tags]    jenkins_group1    regression    search_product    search_popup    testrailid:C23948
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${3_characters}
    plp_page.Verify product image should be displayed in popup  ${product_1_th}
    plp_page.Verify search keyword should be displayed in pop-up
    plp_page.Verify categories should be displayed in pop-up

To verify that result search when user type => 3 character but not math result, system show only recent search
    [Tags]    jenkins_group1    regression    search_product    search_popup    testrailid:C23949
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${random_characters}
    home_page_web_keywords.Press Enter key to search a product
    home_page_web_keywords.Click close button in search box
    plp_page.Verify recent search should be displayed in pop-up

To verify that CSS style of result of product when result less than 6 or more than 6
    [Tags]    jenkins_group1    regression    search_product    search_popup    testrailid:C23950
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${3_characters}
    plp_page.Verify the product images should be six  6

To verify the search keyword section must display result from serch team's search keyword suggestion api
    [Tags]    jenkins_group1    regression    search_product    brand_blacklist    testrailid:C130480
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${3_characters}
    plp_page.Verify a recommendation keyword should be displayed in pop-up  ${recommendation_keyword_1}
    plp_page.Verify a recommendation keyword should be displayed in pop-up  ${recommendation_keyword_2}
    plp_page.Verify a recommendation keyword should be displayed in pop-up  ${recommendation_keyword_3}

To verify that search result will be redirected to URL which is return from search team api
    [Tags]    jenkins_group1    regression    search_product    brand_blacklist    testrailid:C130555
    [Setup]  NONE 
    CommonWebKeywords.Open Chrome Browser to page  ${url_api_search_keyword}    ${seleniumSpeed}    headless=${FALSE}    extension_full_path=${CURDIR}/../../../resources/cds_extension/${ENV.lower()}    sleep_loading_extension=2
    plp_page.Verify api should return valid keyword  ${nike}
    CommonWebKeywords.Open Chrome Browser to page  ${url_web_search_keyword}    ${seleniumSpeed}    headless=${FALSE}    extension_full_path=${CURDIR}/../../../resources/cds_extension/${ENV.lower()}    sleep_loading_extension=2
    plp_page.Verify product name should be displayed correctly in search product  ${product_2_th}
