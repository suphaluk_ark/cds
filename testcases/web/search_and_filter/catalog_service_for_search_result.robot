*** Settings ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Variables ***
${keyword_1}   pillow
${keyword_2}    beauty and the beast
${keyword_3}    beauty
${brand_name}    burt's bees
${color}     pink
${size}    100

*** Keywords ***
Template - Verify elastic search result
    [Arguments]    ${search_keyword}    ${result_keyword}
    search_keyword.Search product for go to PLP page    ${search_keyword}
    common_keywords.Wait Until Page Is Completely Loaded
    plp_page.Verify elastic search result is displayed product that contain search keyword    ${result_keyword}

*** Test Cases ***
To verify that if catalog service for new search is turned off
    [Tags]    jenkins_group1    regression    search_product    catalog_service    testrailid:C360138
    search_keyword.Search product for go to PLP page    ${search_by_keyword.beauty}
    plp_page.Click on sorter arrow-down button
    plp_page.Select New arrivals from sorter
    plp_page.Click on price range arrow-down button
    plp_page.Click on brand name arrow-down button
    plp_page.Click on a brand name    ${search_by_keyword.burt_bee}
    plp_page.click on size arrow-down button
    plp_page.Click on a size    ${search_by_keyword.size_100}
    plp_page.Click on color arrow-down button
    plp_page.Click on a color    ${search_by_keyword.pink}

To verify that a "view by" function on a search result page can work correctly
    [Tags]    jenkins_group1    regression    search_product    catalog_service    testrailid:C335748
    search_keyword.Search product for go to PLP page    ${search_by_keyword.pillow}
    plp_page.Click on arrow-down on view by section
    plp_page.Click 50 from view by section
    plp_page.Verify the total of products should be displayed correctly    ${search_result_number.total_50}
    plp_page.Click on arrow-down on view by section
    plp_page.Click 100 from view by section
    plp_page.Verify the total of products should be displayed correctly    ${search_result_number.total_100}
    plp_page.Click on arrow-down on view by section
    plp_page.Click 200 from view by section

To verify that product count must be displayed on a search result page correctly
    [Tags]    jenkins_group1    regression    search_product    catalog_service    testrailid:C360138
    search_keyword.Search product for go to PLP page    ${search_by_keyword.beauty_the_beast}
    common_keywords.Wait Until Page Is Completely Loaded
    plp_page.Verify the total of products should be displayed correctly    ${search_result_number.total_5}

To verify elastic search result is displayed on PLP page
    [Tags]    jenkins_group1    regression    search_product    catalog_service    elastic_search    testrailid:C697580
    [Template]    Template - Verify elastic search result
    ${search_by_keyword.sanria}    ${search_by_keyword.sanrio}
    ${search_by_keyword.casia}    ${search_by_keyword.casio}
    ${search_by_keyword.adidos}    ${search_by_keyword.adidas}
    ${search_by_keyword.diox}    ${search_by_keyword.dior}