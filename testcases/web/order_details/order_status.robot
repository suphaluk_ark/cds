*** Settings ***
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown
...    AND    Close Browser
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

*** Test Cases ***
Verify intransit status at order status and progress bar
    [Tags]    regression    order_status_2    testrailid:C71512    testrailid:C398383    testrailid:C117855    testrailid:C117859
    Order_details.Create order for verify order details with credit card    ${cds_order_status_username_4}    ${cds_order_status_password_4}    ${cds_order_status_information_4.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    Force_shipment.Fully force shipment via MCOM by order number    ${order_id}
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is intransit
    Order_details_page.Verify order status progress bar is intransit

Verify ready to ship status at order status and progress bar
    [Tags]    regression    order_status_1    testrailid:C71513
    Order_details.Create order for verify order details with cod    ${cds_order_status_username}    ${cds_order_status_password}    ${cds_order_status_information.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    Force_shipment.Fully force shipment via MCOM by order number    ${order_id}
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is intransit
    Order_details_page.Verify order status progress bar is ready to ship

Verify ready to ship status with payment complete at order status and progress bar
    [Tags]    regression    order_status_1    testrailid:C117855
    Order_details.Create order for verify order details with credit card    ${cds_order_status_username_2}    ${cds_order_status_password_2}    ${cds_order_status_information_2.firstname}
    ${order_id}=    Get order ID after completely order
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is processing
    Order_details_page.Verify order status progress bar is processing

Verify ready to pickup status at order status and progress bar
    [Tags]    regression    order_status_1    testrailid:C146055
    Order_details.Create order for verify order details with pickup at store    ${cds_order_status_username_3}    ${cds_order_status_password_3}    ${cds_order_status_information_3.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    Force_shipment.Fully force shipment via MCOM by order number    ${order_id}
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is intransit
    Order_details_page.Verify order status progress bar is intransit
    Verify order details page delivery option is pickup at store

Verify derived status at order status and progress bar
    [Tags]    regression    order_status_2    testrailid:C71515    testrailid:C71514    testrailid:C117860    testrailid:C273777
    Order_details.Create order for verify order details with cod    ${cds_order_status_username_5}    ${cds_order_status_password_5}    ${cds_order_status_information_5.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    kibana_api.ESB update shipment status to shipped by sku    ${order_id}    ${product_sku_list}[2]
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is intransit
    Order_details_page.Verify order status progres s bar is intransit
    kibana_api.ESB update shipment status to delivered by sku    ${order_id}    ${product_sku_list}[2]
    Order_details_page.Verify order status bar is deliver
    Order_details_page.Verify order status progress bar is deliver

Verify processing status with payment incomplete at order status and progress bar
    [Tags]    regression    order_status_2    testrailid:C71511    testrailid:C117856    testrailid:C106190    testrailid:C273738
    Order_details.Create order for verify order details with credit card incomplete   ${cds_order_status_username_4}    ${cds_order_status_password_4}    ${cds_order_status_information_4.firstname}
    ${order_id}=    Order_details_page.Get order id from checkout page
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is awaiting payment
    Order_details_page.Verify order progress bar is payment
    Order_details_page.Verify tracking number should not display in order details page

Verify intransit status with multiple orders at order status and progress bar
    [Tags]    regression    order_status_1    testrailid:C117861    testrailid:C106200    testrailid:C498642
    Order_details.Create multiple orders for verify order details with cod    ${cds_order_status_username}    ${cds_order_status_password}    ${cds_order_status_information.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    kibana_api.ESB update shipment status to shipped by sku    ${order_id}    ${product_sku_list}[2]
    kibana_api.ESB update shipment status to shipped by sku    ${order_id}    ${product_sku_list}[4]
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is intransit
    Order_details_page.Verify order status progress bar is intransit
    Order_details.Verify all of tracking number display in order details page

Verify delivered status with multiple orders at order status and progress bar
    [Tags]    regression    order_status_1    testrailid:C117860    testrailid:C117862
    Order_details.Create multiple orders for verify order details with cod    ${cds_order_status_username_2}    ${cds_order_status_password_2}    ${cds_order_status_information_2.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    Force_shipment.Fully force shipment via MCOM by order number    ${order_id}
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is intransit
    Order_details_page.Verify order status progress bar is intransit
    kibana_api.ESB update shipment status to delivered by sku    ${order_id}    ${product_sku_list}[2]
    kibana_api.ESB update shipment status to delivered by sku    ${order_id}    ${product_sku_list}[4]
    Order_details_page.Verify order status bar is deliver
    Order_details_page.Verify order status progress bar is deliver

Verify partial intransit status with multiple orders at order status and progress bar
    [Tags]    regression    order_status_1    testrailid:C117863    testrailid:C106203
    Order_details.Create multiple orders for verify order details with credit card   ${cds_order_status_username_3}    ${cds_order_status_password_3}    ${cds_order_status_information_3.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    kibana_api.ESB update shipment status to shipped by sku    ${order_id}    ${product_sku_list}[2]
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is processing
    Order_details_page.Verify order status progress bar is intransit 1 box
    Order_details_page.Verify order status progress bar is processing 2 box
    Order_details.Verify partial of tracking number display in order details page

Verify partial delivered status with multiple orders at order status and progress bar
    [Tags]    regression    order_status_2    testrailid:C117864
    Order_details.Create multiple orders for verify order details with cod    ${cds_order_status_username_5}    ${cds_order_status_password_5}    ${cds_order_status_information_5.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    Force_shipment.Fully force shipment via MCOM by order number    ${order_id}
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is intransit
    Order_details_page.Verify order status progress bar is intransit
    kibana_api.ESB update shipment status to delivered by sku    ${order_id}    ${product_sku_list}[2]
    Order_details_page.Verify order status bar is deliver
    Order_details_page.Verify order status progress bar is delivered 1 box
    Order_details_page.Verify order status progress bar is shipped 2 box

Verify cancel status at order status and progress bar
    [Tags]    regression    order_status_1    testrailid:C117858    testrailid:C106213
    Order_details.Create order for verify order details with cod    ${cds_order_status_username}    ${cds_order_status_password}    ${cds_order_status_information.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    common_keywords.Force cancel order via MCOM directly
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status progress bar is cancel
    Order_details_page.Verify tracking number should not display in order details page

Verify tracking number when order shipped at order details page
    [Tags]    regression    order_status_1    testrailid:C106210     testrailid:C125855    testrailid:C125853
    Order_details.Create order for verify order details with cod    ${cds_order_status_username_2}    ${cds_order_status_password_2}    ${cds_order_status_information_2.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    kibana_api.ESB update shipment status to shipped by sku    ${order_id}    ${product_sku_list}[2]
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is intransit
    Order_details.Verify tracking number and tracking message display in order details page
    Order_details_page.Click tracking number

Verify tracking number when order delivered at order details page
    [Tags]    regression    order_status    testrailid:C106211    branch10    testrailid:C125855    testrailid:C125853
    Order_details.Create order for verify order details with cod    ${cds_order_status_username_3}    ${cds_order_status_password_3}    ${cds_order_status_information_3.firstname}
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    kibana_api.ESB update shipment status to shipped by sku    ${order_id}    ${product_sku_list}[2]
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is intransit
    kibana_api.ESB update shipment status to delivered by sku    ${order_id}    ${product_sku_list}[2]
    Order_details_page.Verify order status bar is deliver
    Order_details.Verify tracking number and tracking message display in order details page
    Order_details_page.Click tracking number

Verify tracking number with Guest at order details page
    [Tags]    regression    order_status    testrailid:C125855    branch10    testrailid:C125854
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    Order_details.Checkout order and pay by credit card for guest member
    ${order_id}=    Get order ID after completely order
    Common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id}
    kibana_api.ESB update shipment status to delivered by sku    ${order_id}    ${product_sku_list}[2]
    Order_details_page.Click continue button on payment page
    Order_details.Guest verify tracking_id    ${order_id}
    Order_details_page.Verify order status bar is intransit
    Order_details.Verify tracking number and tracking message display in order details page
    Order_details_page.Click tracking number
    Order_details_page.Verify tracking page when click tracking number

Verify that a re-payment can be clicked and redirected to a correct page
    [Tags]    regression    order_status    testrailid:C419439    testrailid:C419440    testrailid:C419441
    Order_details.Create order for verify order details with credit card incomplete    ${cds_order_status_username_2}    ${cds_order_status_password_2}    ${cds_order_status_information_2.firstname}
    ${order_id}=    Order_details_page.Get order id from checkout page
    Order_details_page.Click continue button on payment page
    Order_details_page.Click my customer name on home page
    Order_details_page.Click my order on home page
    Order_details_page.Select order at my order page    ${order_id}
    Order_details_page.Verify order status bar is awaiting payment
    Order_details_page.Verify order progress bar is payment
    Order_details_page.Verify re-payment button displayed on order details page
    Order_details_page.Click on re-payment button
    checkout_page.Verify Repayment page is displayed
