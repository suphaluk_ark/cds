*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

Test Teardown    Run Keywords    common_cds_web.Tear down with cancel order    AND    Order_details_page.Click continue button on payment page

Suite Setup    common_cds_web.Login to CDS and remove product in shopping cart    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}

Suite Teardown    Close All Browsers

*** Test Cases ***
To Verify that order created on each system when user place order with Click & Collect + Credit card payment
    [Tags]    regression    suite_SIT    PK_source    testrailid:C273765
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[32]
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

To Click & Collect + T1C full redemption
    [Tags]    regression    suite_SIT    PK_source    partial_redeem    testrailid:C273767
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[32]
    E2E_flow_keywords.Select Click & Collect pick at store    ${pick_at_store.central_bangna}
    # E2E_flow_keywords.Partial redeem point and verify T1C point discount and pay other by credit card    ${40}    ${5}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order at MDC MCOM FMS and WMS with no shipping fee and payment by credit card
    [Tags]    regression    suite_SIT    PK_source    testrailid:C273708
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[32]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order at MDC MCOM FMS and WMS with shipping fee and payment by credit card
    [Tags]    regression    suite_SIT    PK_source    testrailid:C273709
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[32]
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order at MDC MCOM FMS and WMS with no shipping fee and payment by cod
    [Tags]    regression    suite_SIT    PK_source    testrailid:C273712
    E2E_flow_keywords.Search and create order for verify order details with cod    ${product_sku_list}[2]
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order creation on MDC MCOM FMS and WMS with no shipping fee and T1C full redemption
    [Tags]    regression    suite_SIT    PK_source    Ship_to_address    partial_redeem     testrailid:C273713
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[32]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    # E2E_flow_keywords.Partial redeem point and verify T1C point discount and pay other by credit card    ${40}    ${5}
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order creation on MDC MCOM FMS and WMS with with shipping fee and T1C full redemption
    [Tags]    regression    suite_SIT    PK_source    Ship_to_address    partial_redeem     testrailid:C273714
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[32]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    # E2E_flow_keywords.Partial redeem point and verify T1C point discount and pay other by credit card    ${40}    ${5}
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database