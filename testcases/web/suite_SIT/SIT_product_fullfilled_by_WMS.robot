*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    common_cds_web.Tear down with cancel order    AND    Close Browser

*** Test Cases ***
To verify that order created on each system when user place order with Click & Collect + Credit card payment
    [Tags]    regression    suite_SIT    WMS_source    testrailid:C273762
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username1}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To verify that order created on each system when user place order with Click & Collect + 123 Payment
    [Tags]    regression    suite_SIT    WMS_source    testrailid:C273763    testrailid:C273740
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username1}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    Checkout, select pick at store and pay by 1 2 3 bank transfer(check shipping charge)    ${pick_at_store.central_bangna}
    E2E_flow_keywords.Verify order status in MDC and MCOM are pending payment

To verify that order created on each system when user place order with Click & Collect + T1C full redemption
    [Tags]    regression    suite_SIT    WMS_source    partial_redeem    testrailid:C273764
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username1}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Select pick at store    ${pick_at_store.central_bangna}
    Wait Until Page Loader Is Not Visible
    Go to payment page
    # E2E_flow_keywords.Partial redeem point and verify T1C point discount and pay other by credit card    ${40}    ${5}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To verify that order created on each system when user place order with Pickup at Store (no shipping fee) + Credit card payment
    [Tags]    regression    suite_SIT    WMS_source    partial_redeem    testrailid:C273770
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username1}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To verify that order created on each system when user place order with Pickup at Store (no shipping fee) + 123 Payment
    [Tags]    regression    suite_SIT    WMS_source    partial_redeem    testrailid:C273772
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    Select pickup at store option then confim payment by 123 bank transfer    ${pick_at_store.central_bangna}
    E2E_flow_keywords.Verify order status in MDC and MCOM are pending payment

To verify that order created on each system when user place order with Pickup at store (no shipping fee) + T1C full redemption
    [Tags]    regression    suite_SIT    WMS_source    partial_redeem    testrailid:C273774
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    E2E_flow_keywords.Select pickup at store then confim payment by T1C redeem    ${pick_at_store.central_bangna}    ${40}    ${5}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To Ship to address (no shipping fee) + credit card payment
    [Tags]    regression    suite_SIT    WMS_source    Ship_to_address    testrailid:C273747    testrailid:C273737
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To Ship to address (with shipping fee) + credit card payment
    [Tags]    regression    suite_SIT    WMS_source    Ship_to_address    testrailid:C273748
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select Next-day shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To Verify that order created on each system when user place order with ship to address (no shipping fee) + 123 payment
    [Tags]    regression    suite_SIT    WMS_source    Ship_to_address    testrailid:C273749
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    E2E_flow_keywords.Confirm payment by banktransfer
    checkout_page.Get order ID from thank you page
    E2E_flow_keywords.Verify order status in MDC and MCOM are pending payment

To Verify that order created on each system when user place order with Ship to address (with shipping fee) + 123 payment
    [Tags]    regression    suite_SIT    WMS_source    Ship_to_address    testrailid:C273750
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username3}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select Next-day shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    E2E_flow_keywords.Confirm payment by banktransfer
    checkout_page.Get order ID from thank you page
    E2E_flow_keywords.Verify order status in MDC and MCOM are pending payment

To Verify that order created on each system when user place order with ship to address (shipping fee) + COD
    [Tags]    regression    suite_SIT    WMS_source    Ship_to_address    testrailid:C273751
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username3}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method COD
    Wait Until Page Is Completely Loaded
    checkout_page.Click Confirm Order For COD Payment Method
    checkout_page.Get order ID from thank you page
    E2E_flow_keywords.Verify order status in MDC and MCOM are pending

To Verify that order created on each system when user place order with ship to address (no shipping fee) + T1C full redemption
    [Tags]    regression    suite_SIT    WMS_source    Ship_to_address    partial_redeem     testrailid:C273752
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username3}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    # E2E_flow_keywords.Partial redeem point and verify T1C point discount and pay other by credit card    ${40}    ${5}
    E2E_flow_keywords.Verify order status in MDC and MCOM are pending
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To Verify that order created on each system when user place order with ship to address (with shipping fee) + T1C full redemption
    [Tags]    regression    suite_SIT    WMS_source    Ship_to_address    partial_redeem     testrailid:C273753
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username3}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select Next-day shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    # E2E_flow_keywords.Partial redeem point and verify T1C point discount and pay other by credit card    ${40}    ${5}
    E2E_flow_keywords.Verify order status in MDC and MCOM are pending payment
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database