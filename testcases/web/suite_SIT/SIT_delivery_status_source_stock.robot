*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Library    Dialogs

Test Teardown    CommonWebKeywords.Test Teardown


Suite Teardown    Close All Browsers



*** Test Cases ***
To verify order status on each system when have pick-pack completed and ready to delivery to Click & Collect store (MDC + MCOM + ESB + WMS), source stock 10138
    [Tags]    regression    suite_SIT    WMS_source    testrailid:C273723    testrailid:C273724    testrailid:C273725    testrailid:C273778    testrailid:C273780
    [Setup]    common_cds_web.Setup - Login successfully, create product data file and get customer profile    ${WMS_source_username}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    E2E_flow_keywords.Go to order detail page from thank you page by order number    ${increment_id}
    E2E_flow_keywords.Verify order status in MDC, MCOM and order progress bar after create order successfully    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database
    E2E_flow_keywords.Update order status to shipped and verify status in MDC, MCOM and order progress bar is complete fullyshipped    ${increment_id}    ${product_sku_list}[2]
    E2E_flow_keywords.Update order status to ready to collect and verify status in MDC and order progress bar is fullyshipped    ${increment_id}
    E2E_flow_keywords.Update order status to delivered and verify status in MDC and order progress bar is fullyshipped    ${increment_id}    ${product_sku_list}[2]

To verify order status on each system when the order in transit, ready to collect to Store and customer received products at store
    [Tags]    regression    suite_SIT    PK_source    testrailid:C273779    testrailid:C273778    testrailid:C273780
    [Setup]    common_cds_web.Setup - Login successfully, create product data file and get customer profile    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${CDS10219928.sku}    # CDS10219928 source stock == CDS_SO_10116
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    E2E_flow_keywords.Go to order detail page from thank you page by order number    ${increment_id}
    E2E_flow_keywords.Verify order status in MDC, MCOM and order progress bar after create order successfully    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database
    E2E_flow_keywords.Update order status to shipped and verify status in MDC, MCOM and order progress bar is complete fullyshipped    ${increment_id}    ${CDS10219928.sku}
    E2E_flow_keywords.Update order status to ready to collect and verify status in MDC and order progress bar is fullyshipped    ${increment_id}