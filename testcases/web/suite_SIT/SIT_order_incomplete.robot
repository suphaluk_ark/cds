*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Login to CDS and remove product in shopping cart    ${WMS_source_username}    ${WMS_source_password}    ${WMS_source_information.firstname}
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown
...    AND    Close Browser

*** Test Cases ***
To verify that order will be "pending payment" on MCOM when payment incomplete
    [Tags]    regression    suite_SIT    WMS_source    testrailid:C273744
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Go to checkout page
    E2E_flow_keywords.Select standard shipping address and go to payment page
    E2E_flow_keywords.Confirm payment by banktransfer
    E2E_flow_keywords.Verify order status in MDC and MCOM are pending payment

To verify that able to customer cancel order before invoice
    [Tags]    regression    suite_SIT    WMS_source    testrailid:C273746
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    E2E_flow_keywords.Fully cancel order from MCOM and verify