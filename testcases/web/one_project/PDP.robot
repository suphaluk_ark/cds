*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Keywords ***
Template - Verify that the Simple product display correctly
    [Arguments]    ${test_dict_one_project}
    [Teardown]    CommonWebKeywords.Keyword Teardown
    ${expected_sku}    Set Variable    ${test_dict_one_project.sku}
    ${urlkey}    Set Variable    ${test_dict_one_project.urlkey}

    # 1. Get response
    ${response}=    pdp_falcon.Api Product PDP By Url Key   ${urlkey}    ${language}    ${BU}
    REST.String    $..sku    ${expected_sku}   

    # 2. Go to pdp page, 404 page : Fail
    SeleniumLibrary.Go To    ${central_url}/${language}/${urlkey}
    SeleniumLibrary.Element Should Not Be Visible    //*[text()='404 page']   msg=This page 404 

    # capture
    ${sc_fname}=    CommonKeywords.Get Valid File Name     ${TEST_NAME}
    Run Keyword And Ignore Error    SeleniumLibrary.Capture Page Screenshot    ${sc_fname}_{index}.png
    
    # 3. Transform data to verify
    ${dict_simple_product}=    product_api.Get simple product data    ${response}
    
    # 4. Data Validation points products, The displayed product should display data from API correctly
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product brand name' should display correctly via api    ${response}
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product name' should display correctly via api    ${response}    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : image' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product overlay image' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : redeem point' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag new / promotion' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag only at / online exclusive' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product price' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : pay by installment' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product id' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product customer reviews' should be displayed via api    ${response}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Template - Verify that old urlkey redirect to new urlkey, the Simple product display NEW SKU correctly
    [Arguments]    ${test_dict_one_project}
    ${expected_old_data}    Set Variable    ${test_dict_one_project.old}
    ${expected_new_data}    Set Variable    ${test_dict_one_project.new}

    ${old_urlkey}    Set Variable    ${expected_old_data.urlkey}
    ${new_urlkey}    Set Variable    ${expected_new_data.urlkey}

    # 1. Go to OLD urlkey
    SeleniumLibrary.Go To    ${central_url}/${language}/${old_urlkey}
    SeleniumLibrary.Element Should Not Be Visible    //*[text()='404 page']   msg=This page 404 
    
    ${current_urlkey}=    common_cds_web.Get product urlkey by current url
    Should Be Equal As Strings    ${current_urlkey}    ${new_urlkey}
    
    Template - Verify that the Simple product display correctly    ${expected_new_data}

*** Test Cases ***
# [Before merging] - Verify that the Simple product display correctly (CASE 1 - OLD SKU)
#     [Tags]    one_project    one_project_pdp    testing
#     [Setup]    common_cds_web.Setup - Open browser
#     [Template]    Template - Verify that the Simple product display correctly
#     FOR    ${product_sku}    IN    @{one_project_product_pdp.simple.CASE_1}
#         ${one_project_product_pdp.simple.CASE_1}[${product_sku}][old]
#     END

# [Before merging] - Verify that the Simple product display correctly (CASE 2 - OLD SKU)
#     [Tags]    one_project    one_project_pdp
#     [Setup]    common_cds_web.Setup - Open browser
#     [Template]    Template - Verify that the Simple product display correctly
#     FOR    ${product_sku}    IN    @{one_project_product_pdp.simple.CASE_2}
#         ${one_project_product_pdp.simple.CASE_2}[${product_sku}][old]
#     END

[After merging] - Verify that old urlkey redirect to new urlkey, the Simple product display NEW SKU correctly (CASE 1 - OLD to NEW SKU)
    # This test case will run after sync all of data old to new
    [Tags]    one_project    one_project_pdp
    [Setup]    common_cds_web.Setup - Open browser
    [Template]    Template - Verify that old urlkey redirect to new urlkey, the Simple product display NEW SKU correctly
    FOR    ${product_sku}    IN    @{one_project_product_pdp.simple.CASE_1}
        ${one_project_product_pdp.simple.CASE_1}[${product_sku}]
    END

[After merging] - Verify that the Simple product display correctly (CASE 1 - NEW SKU)
    # This test case will run after sync all of data old to new
    [Tags]    one_project    one_project_pdp
    [Setup]    common_cds_web.Setup - Open browser
    [Template]    Template - Verify that the Simple product display correctly
    FOR    ${product_sku}    IN    @{one_project_product_pdp.simple.CASE_1}
        ${one_project_product_pdp.simple.CASE_1}[${product_sku}][new]
    END

[After merging] - Verify that the Simple product display correctly (CASE 2 - NEW SKU)
    # This test case will run after sync all of data old to new
    [Tags]    one_project    one_project_pdp
    [Setup]    common_cds_web.Setup - Open browser
    [Template]    Template - Verify that the Simple product display correctly
    FOR    ${product_sku}    IN    @{one_project_product_pdp.simple.CASE_2}
        ${one_project_product_pdp.simple.CASE_2}[${product_sku}][new]
    END

