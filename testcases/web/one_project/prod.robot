*** Setting ***
Resource        ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown   Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Keywords ***
Verify that the product display correctly
    ${url}=    common_cds_web.Get product urlkey by current url
    ${response}=    pdp_falcon.Api Product PDP By Url Key    ${url}    ${language}    ${BU}

    # Get type of product
    ${type_id}=    JSONLibrary.Get Value From Json    ${response}    $..product.type_id
    ${type_id}=    Set Variable    ${type_id}[0]
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    ${dict_simple_product}=    Run Keyword If    '${type_id}' == 'configurable'    product_PDP_keywords.Get configurable's simple product displayed via api   ${response}
    ...    ELSE IF     '${type_id}' == 'simple'    product_api.Get simple product data   ${response}
    ...    ELSE    Fail    Incorrect type_id

    # capture
    ${sc_fname}=    CommonKeywords.Get Valid File Name     ${TEST_NAME}
    Run Keyword And Ignore Error    SeleniumLibrary.Capture Page Screenshot    ${sc_fname}_{index}.png

    # Data Validation points products, The displayed product should display data from API correctly
    # 1. Data Validation points
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product brand name' should display correctly via api    ${response}
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product name' should display correctly via api    ${response}    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : image' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product overlay image' should display correctly via api    ${dict_simple_product}[json_object]
    # 4 Nov 2020 ** GTM is hiding redeem point on prod ** Should be FAIL 
    # Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : redeem point' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag new / promotion' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag only at / online exclusive' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product price' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : pay by installment' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product id' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product customer reviews' should be displayed via api    ${response}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

    # 2. UI Validation points
    # Wishlist
    product_PDP_page.Verify 'add wishlist button' should be displayed    ${dict_simple_product}[sku]

    # Add to cart
    product_PDP_page.Verify add to bag button on product PDP page    ${dict_simple_product}[sku]

    # Social icon
    product_PDP_page.Verify social icon facebook on product PDP page
    product_PDP_page.Verify social icon twitter on product PDP page
    product_PDP_page.Verify social icon email on product PDP page
    product_PDP_page.Verify social icon line on product PDP page

    # Promotion tab ** 16 Nov 2020 no promotion tab on prod
    # product_PDP_page.Click 'promotions' tab
    # product_PDP_page.Verify 'promotions tab details' should be displayed
    
    # Delivery & Return tab
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Verify 'delivery & return tab details' should be displayed

Verify that the product display correctly - Chanel
    ${url}=    common_cds_web.Get product urlkey by current url
    ${response}=    pdp_falcon.Api Product PDP By Url Key    ${url}    ${language}    ${BU}

    # Get type of product
    ${type_id}=    JSONLibrary.Get Value From Json    ${response}    $..product.type_id
    ${type_id}=    Set Variable    ${type_id}[0]
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    ${dict_simple_product}=    Run Keyword If    '${type_id}' == 'configurable'    product_PDP_keywords.Get configurable's simple product displayed via api   ${response}
    ...    ELSE IF     '${type_id}' == 'simple'    product_api.Get simple product data   ${response}
    ...    ELSE    Fail    Incorrect type_id

    # capture
    ${sc_fname}=    CommonKeywords.Get Valid File Name     ${TEST_NAME}
    Run Keyword And Ignore Error    SeleniumLibrary.Capture Page Screenshot    ${sc_fname}_{index}.png

    # Data Validation points products, The displayed product should display data from API correctly
    # 1. Data Validation points
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product brand name' should display correctly via api    ${response}
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product name' should display correctly via api    ${response}    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : image' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product overlay image' should display correctly via api    ${dict_simple_product}[json_object]
    # 4 Nov 2020 ** GTM is hiding redeem point on prod ** Should be FAIL 
    # Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : redeem point' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag new / promotion' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag only at / online exclusive' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product price' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : pay by installment' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product id' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product customer reviews' should be displayed via api    ${response}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

    # 2. UI Validation points
    # Wishlist
    product_PDP_page.Verify 'add wishlist button' should be displayed    ${dict_simple_product}[sku]

    # Add to cart
    product_PDP_page.Verify add to bag button on product PDP page    ${dict_simple_product}[sku]

    # Social icon ** 30 Dec 2020 social icon not display on CHANEL pdp
    # product_PDP_page.Verify social icon facebook on product PDP page
    # product_PDP_page.Verify social icon twitter on product PDP page
    # product_PDP_page.Verify social icon email on product PDP page
    # product_PDP_page.Verify social icon line on product PDP page

    # Promotion tab ** 16 Nov 2020 no promotion tab on prod
    # product_PDP_page.Click 'promotions' tab
    # product_PDP_page.Verify 'promotions tab details' should be displayed
    
    # Delivery & Return tab
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Verify 'delivery & return tab details' should be displayed

Template - search term, click 1st product, verify pdp page
    [Arguments]    ${keyword}
    [Teardown]    CommonWebKeywords.Keyword Teardown
    search_keyword.Search product for go to PLP page    ${keyword}
    plp_page.Click 1st product
    Verify that the product display correctly

Template - search term 'stanard boutique brand', click shop all products
    [Arguments]    ${dict_brand}
    [Teardown]    CommonWebKeywords.Keyword Teardown
    search_keyword.Search product for go to PLP page    ${dict_brand.name}
    standard_boutique_page.Click on Shop All Product link    ${dict_brand.href}
    plp_page.Click 1st product
    Verify that the product display correctly

*** Test Cases ***
Search term, click 1st product, verify pdp page
    [Tags]    one_project_prod
    [Setup]    common_cds_web.Setup - Open browser
    [Template]    Template - search term, click 1st product, verify pdp page
    # able to seach and plp page is avaliable (product is existing)
    FOR    ${keyword}    IN    @{search_term.keywords}
        ${keyword}
    END
    FOR    ${brand}    IN    @{search_term.normal_brand}
        ${brand}
    END

Search term - Stanard boutique brand, click shop all products, click 1st product, verify pdp page
    [Tags]    one_project_prod
    [Setup]    common_cds_web.Setup - Open browser
    [Template]    Template - search term 'stanard boutique brand', click shop all products
    # able to search, click shop all product
    FOR    ${brand}    IN    @{search_term.standard_boutique_brand}
        ${search_term.standard_boutique_brand.${brand}}
    END

Search term - Full boutique brand, verify pdp page : Chanel
    [Tags]    one_project_prod
    [Setup]    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PLP page    chanel
    full_boutique_page.Chanel - click category img     WOMEN'S FRAGRANCE
    full_boutique_page.Chanel - click category img     N°5
    plp_page.Click 1st product
    Verify that the product display correctly - Chanel

Search term - Full boutique brand, verify pdp page : Dior
    [Tags]    one_project_prod
    [Setup]    common_cds_web.Setup - Open browser
    search_keyword.Search product for go to PLP page    dior
    full_boutique_page.Dior - click product img    GLOW FACE PALETTE #005 COPPER GOLD
    plp_page.Click 1st product
    Verify that the product display correctly