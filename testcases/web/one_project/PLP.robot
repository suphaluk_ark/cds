*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Keywords ***
Template - Verify that a searching by simple SKU, go to search page, search page UI is displayed with data correctly
    [Arguments]    ${test_dict_one_project}
    [Teardown]    CommonWebKeywords.Keyword Teardown
    ${expected_sku}    Set Variable    ${test_dict_one_project.sku}
    ${urlkey}    Set Variable    ${test_dict_one_project.urlkey}

    search_keyword.Search product for go to PLP page    ${expected_sku}

    # 1. Get response, Expected SKU should be 1st response product
    ${response}=    search_falcon.Api Search Product By Keyword    ${expected_sku}    ${language}    ${BU.lower()}    ${1}
    REST.String    $..products[0].sku    ${expected_sku}
    
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]

    # 2. Data Validation points header
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Verify 'search keyword label' should be displayed correctly    ${expected_sku.lower()}
    plp_page.Verify 'product found label' should be displayed correctly    ${result_total_search_product}

    # 3. Data Validation points products, The displayed product should display data from API correctly
    ${dict_display_sku}=    plp_api.Get data-product-sku, data-product-id data by sku    ${response}    ${expected_sku}    ${expected_sku}
    Run Keyword And Continue On Failure    Should Be Equal As Strings    ${dict_display_sku}[data_index]    ${0}

    Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${expected_sku}    ${expected_sku}
    Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product brand' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${expected_sku}
    Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product name' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${expected_sku}
    
    Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product overlay image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${expected_sku}    ${expected_sku}
    Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product price' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${expected_sku}    ${expected_sku}
    Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag new / promotion' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${expected_sku}    ${expected_sku}
    Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag only at / online exclusive' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${expected_sku}    ${expected_sku}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Template - Verify that a searching by simple SKU, should be failed
    [Arguments]    ${test_dict_one_project}
    Set Test Variable    ${GLOBALTIMEOUT}    ${5}
    ${status}=    Run Keyword And Return Status    Template - Verify that a searching by simple SKU, go to search page, search page UI is displayed with data correctly    ${test_dict_one_project}
    Should Not Be True    ${status}

*** Test Cases ***
# [Before merging] - Verify that a searching by simple SKU (CASE 1 - OLD SKU)
#     [Tags]    one_project    one_project_plp
#     [Setup]    common_cds_web.Setup - Open browser
#     [Template]    Template - Verify that a searching by simple SKU, go to search page, search page UI is displayed with data correctly
#     FOR    ${product_sku}    IN    @{one_project_product_plp.simple.CASE_1}
#         ${one_project_product_plp.simple.CASE_1}[${product_sku}][old]
#     END

# [Before merging] - Verify that a searching by simple SKU (CASE 2 - OLD SKU)
#     [Tags]    one_project    one_project_plp
#     [Setup]    common_cds_web.Setup - Open browser
#     [Template]    Template - Verify that a searching by simple SKU, go to search page, search page UI is displayed with data correctly
#     FOR    ${product_sku}    IN    @{one_project_product_plp.simple.CASE_2}
#         ${one_project_product_plp.simple.CASE_2}[${product_sku}][old]
#     END

[After merging] - Verify that a searching by simple SKU, should be failed (CASE 1 - OLD SKU)
    # This test case will run after sync all of data old to new
    [Tags]    one_project    one_project_plp
    [Setup]    common_cds_web.Setup - Open browser
    [Template]    Template - Verify that a searching by simple SKU, should be failed
    FOR    ${product_sku}    IN    @{one_project_product_plp.simple.CASE_1}
        ${one_project_product_plp.simple.CASE_1}[${product_sku}][old]
    END

[After merging] - Verify that a searching by simple SKU (CASE 1 - NEW SKU)
    # This test case will run after sync all of data old to new
    [Tags]    one_project    one_project_plp
    [Setup]    common_cds_web.Setup - Open browser
    [Template]    Template - Verify that a searching by simple SKU, go to search page, search page UI is displayed with data correctly
    FOR    ${product_sku}    IN    @{one_project_product_plp.simple.CASE_1}
        ${one_project_product_plp.simple.CASE_1}[${product_sku}][new]
    END

[After merging] - Verify that a searching by simple SKU (CASE 2 - NEW SKU)
    # This test case will run after sync all of data old to new    
    [Tags]    one_project    one_project_plp
    [Setup]    common_cds_web.Setup - Open browser
    [Template]    Template - Verify that a searching by simple SKU, go to search page, search page UI is displayed with data correctly
    FOR    ${product_sku}    IN    @{one_project_product_plp.simple.CASE_2}
        ${one_project_product_plp.simple.CASE_2}[${product_sku}][new]
    END
