*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Test Cases ***
# [Before merging] - Order product with mix products (3PL type) + OLD SKU One project (CASE 1) + cart price rule (percent discount) + free item, paid via COD + request full tax invoice
#     [Documentation]    - *Cart price rule*    :
#     ...                | =Rule=                                                                     | =Coupon Code=                     | =Type=                                            | =BU ENV ID=               |
#     ...                | QAAT_ROBOT_E2E-Buy >= 2000 - Discount 50฿                                  | Non coupon                        | Fix amount discount for whole cart                | CDS UAT 830, CDS SIT 830  |
#     ...                | QAAT_ROBOT_E2E-Buy specific SKU, Qty is 3 - Free 1 product                 | Non coupon                        | Auto add promo items for whole cart (free item)   | CDS UAT 613, CDS SIT 613  |
#     ...                | QAAT_ROBOT_E2E-whole cart discount 10% only for retail product with coupon | ${coupon.coupon10percentretail}   | Percent of product price discount                 | CDS UAT 1097, CDS SIT 991 |
#     ...                - *Delivery type*    : Standard shipping
#     ...                - *Full tax invoice* : Yes
#     ...                - *Redeem point*     : No
#     ...                - *Payment type*     : COD
#     ...                - *Testrailid*       : C1002645
#     [Tags]    one_project    one_project_e2e_order
#     Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_03}  ${e2e_password_03}
#     login_keywords.Login Keywords - Login Success  ${e2e_username_03}  ${e2e_password_03}    ${e2e_information_03.firstname}
#     # Retail Product, QAAT_ROBOT_E2E-Buy specific SKU, Qty is 3 - Free 1 product
#     common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[buy_item]    ${3} 
#     # Any Retail Product
#     common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon}[COUPON10] 
#     # Any Marketplace Product
#     common_cds_web.Search product and add product to cart    ${non_cart_price_rule_sku}[marketplace]
#     # One project Product (CASE 1)
#     common_cds_web.Search product and add product to cart    ${one_project_e2e_order.simple.CASE_1.CDS19211893.old.sku}
#     header_web_keywords.Header Web - Click View Cart Button
#     my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[buy_item]    ${cart_price_rule_sku.apply_coupon}[COUPON10]    ${non_cart_price_rule_sku}[marketplace]    ${one_project_e2e_order.simple.CASE_1.CDS19211893.old.sku}
#     shopping_bag_keywords.Verify free items with order purchase should be display correctly    ${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[free_item]
#     my_cart_page.Apply coupon code  ${coupon.coupon10percentretail}
#     my_cart_page.Verify coupon code display in cart    ${coupon.coupon10percentretail}
#     ${expected_discount_10percent_for_retail_products}=    Evaluate    (${${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[buy_item].price}*${3}) + ${${cart_price_rule_sku.apply_coupon}[COUPON10].price} + ${${one_project_e2e_order.simple.CASE_1.CDS19211893.old.sku}.price}
#     my_cart_page.Verify e-coupon discount in cart page(%)    ${coupon.coupon10percentretail}    ${expected_discount_10percent_for_retail_products}    ${10.0}
#     checkout_keywords.Check Out Product Until Payment With COD Method And Request Full Tax Invoice(Personal)
#     checkout_page.Thank You Page Should Be Visible
#     checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    discount=${${50}+${discount_amount}}
#     checkout_page.Get order ID from thank you page

[After merging] - Order product with mix products (3PL type) + NEW SKU One project (CASE 1) + cart price rule (percent discount) + free item, paid via COD + request full tax invoice
    [Documentation]    - *Cart price rule*    :
    ...                | =Rule=                                                                     | =Coupon Code=                     | =Type=                                            | =BU ENV ID=               |
    ...                | QAAT_ROBOT_E2E-Buy >= 2000 - Discount 50฿                                  | Non coupon                        | Fix amount discount for whole cart                | CDS UAT 830, CDS SIT 830  |
    ...                | QAAT_ROBOT_E2E-Buy specific SKU, Qty is 3 - Free 1 product                 | Non coupon                        | Auto add promo items for whole cart (free item)   | CDS UAT 613, CDS SIT 613  |
    ...                | QAAT_ROBOT_E2E-whole cart discount 10% only for retail product with coupon | ${coupon.coupon10percentretail}   | Percent of product price discount                 | CDS UAT 1097, CDS SIT 991 |
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : Yes
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    ...                - *Testrailid*       : C1002645
    [Tags]    one_project    one_project_e2e_order
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_03}  ${e2e_password_03}
    login_keywords.Login Keywords - Login Success  ${e2e_username_03}  ${e2e_password_03}    ${e2e_information_03.firstname}
    # Retail Product, QAAT_ROBOT_E2E-Buy specific SKU, Qty is 3 - Free 1 product
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[buy_item]    ${3} 
    # Any Retail Product
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon}[COUPON10] 
    # Any Marketplace Product
    common_cds_web.Search product and add product to cart    ${non_cart_price_rule_sku}[marketplace]
    # One project Product (CASE 1)
    common_cds_web.Search product and add product to cart    ${one_project_e2e_order.simple.CASE_1.CDS19211893.new.sku}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[buy_item]    ${cart_price_rule_sku.apply_coupon}[COUPON10]    ${non_cart_price_rule_sku}[marketplace]    ${one_project_e2e_order.simple.CASE_1.CDS19211893.new.sku}
    shopping_bag_keywords.Verify free items with order purchase should be display correctly    ${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[free_item]
    my_cart_page.Apply coupon code  ${coupon.coupon10percentretail}
    my_cart_page.Verify coupon code display in cart    ${coupon.coupon10percentretail}
    ${expected_discount_10percent_for_retail_products}=    Evaluate    (${${cart_price_rule_sku.non_coupon.buy_item_3_qty_freeitem}[buy_item].price}*${3}) + ${${cart_price_rule_sku.apply_coupon}[COUPON10].price} + ${${one_project_e2e_order.simple.CASE_1.CDS19211893.new.sku}.price}
    my_cart_page.Verify e-coupon discount in cart page(%)    ${coupon.coupon10percentretail}    ${expected_discount_10percent_for_retail_products}    ${10.0}
    checkout_keywords.Check Out Product Until Payment With COD Method And Request Full Tax Invoice(Personal)
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    discount=${${50}+${discount_amount}}
    checkout_page.Get order ID from thank you page