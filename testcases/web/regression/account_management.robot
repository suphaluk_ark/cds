*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Variables ***
${member_type}    personal_member

*** Keyword ***
Login to CDS
    Run Keyword And Ignore Error     Remove customer cart and generate new cart   ${regresstion_username_1}    ${regresstion_password_1}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success   ${regresstion_username_1}    ${regresstion_password_1}    ${regresstion_information_1.firstname}
    Wait Until Page Is Completely Loaded

Login to CDS and go to account overview page
    Login to CDS
    Click my account button
    Click my account menu under my account dropdown

Setup - Login to CDS and go to account overview page
    ${status}=    Run Keyword And Return Status     my_account_page.Account overview page should be displayed
    Return From Keyword If    '${status}'=='${True}'
    Login to CDS and go to account overview page

Successfully order products with payment 1 2 3 transfer and successfully pickup at watson
    [Arguments]     ${product_sku}
    Login to CDS
    Wait Until Page Is Completely Loaded
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku}
    common_cds_web.Checkout, select pick at watson and pay by 1 2 3 bank transfer by Member Type    ${member_type}
    Thank You Page Should Be Visible
    Set Suite Variable  ${order_id_transfer_${member_type}}  ${increment_id}
    Return From Keyword If  '${ENV.lower()}'=='prod'
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_transfer}
    checkout_page.Order status from thank you page should be    ${thankyou_page.status_pending}
    checkout_page.Pickup location should be displayed in thank you page    ${pick_at_watson.family_rama}
    common_cds_web.Order status from mdc should be 'pending payment' status    ${increment_id}
    [Teardown]  Run Keywords    CommonWebKeywords.Keyword Teardown  AND     Close All Browsers

*** Test Cases ***
TEST_001 Verify customet personal information on overview page
    [Tags]    regression
    [Setup]    Login to CDS and go to account overview page
    my_account_page.Customer name should be displayed in personal information    ${regresstion_information_1.firstname} ${regresstion_information_1.lastname}
    my_account_page.Customer e-mail should be displayed in personal information     ${regresstion_information_1.email}
    my_account_page.Customer phone no should be displayed in personal information     ${regresstion_information_1.tel}
    [Teardown]    CommonWebKeywords.Test Teardown

TEST_002 Verify The 1 information and able to connect to T1 account on overview page, and able to disconnect
    [Tags]    regression
    [Setup]    Setup - Login to CDS and go to account overview page
    my_account_page.Account overview page should be displayed
    my_account_page.Click Connect To The 1 Account Button
    my_account_page.Login to The 1 Account Dialog should be displayed
    my_account_page.Input T1C e-mail account    ${t1c_information_2}[email]
    my_account_page.Input T1C password account    ${t1c_information_2}[password]
    my_account_page.Click Login to T1C account button on account overview page
    my_account_page.Customer T1C Information should be displayed correctly    ${t1c_information_2}[t1c_number]
    my_account_page.Click Disconnect The 1 Account Button
    my_account_page.Verify connect to The 1 Account button should be displayed
    [Teardown]    CommonWebKeywords.Test Teardown

TEST_003 Verify default shipping address on overview page
    [Tags]    regression
    [Setup]    Setup - Login to CDS and go to account overview page
    Customer name on Default shipping address should be displayed   ${shipping_address_th.firstname} ${shipping_address_th.lastname}
    Customer phone on Default shipping address should be displayed    ${shipping_address_th.phone}
    Customer address on Default shipping address should be displayed    ${shipping_address_th.address}
    Customer region on Default shipping address should be displayed    ${shipping_address_th.sub_district}, ${shipping_address_th.district}, ${shipping_address_th.region} ${shipping_address_th.zip_code}
    [Teardown]    CommonWebKeywords.Test Teardown

TEST_004 Verify default full tax on overview page
    [Tags]    regression
    [Setup]    Setup - Login to CDS and go to account overview page
    Customer name on Full tax invoice address should be displayed   ${billing_address_th.firstname} ${billing_address_th.lastname}
    Customer phone on Full tax invoice address should be displayed    ${billing_address_th.phone}
    Customer address on Full tax invoice address should be displayed    ${billing_address_th.address}
    Customer region on Full tax invoice address should be displayed    ${billing_address_th.sub_district}, ${billing_address_th.district}, ${billing_address_th.region} ${billing_address_th.zip_code}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

TEST_005 Verify lastest order should be displayed on overview page
    [Tags]    regression    run
    [Setup]    Successfully order products with payment 1 2 3 transfer and successfully pickup at watson   ${product_sku_list}[0]
    Login to CDS and go to account overview page
    my_account_page.Scroll to lastest orders section
    my_account_page.Order number of lastest order should be displayed     ${order_id_transfer_personal_member}
    my_account_page.Order status of lastest order should be displayed   ${order_history.pending_payment}
    my_account_page.Delivery option of lastest order should be displayed    ${order_history.pick_up_familymart}
    my_account_page.Payment type of lastest order should be displayed   ${thankyou_page.payment_transfer}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

#Verify my profile page
TEST_006 Verify my profile page able to display current personal information of that account
    [Tags]    regression
    [Setup]    Login to CDS and go to account overview page
    my_account_page.Click on my profile tab
    my_account_page.Firstname field should be displayed customer data    ${regresstion_information_1.firstname}
    my_account_page.Lastname field should be displayed customer data    ${regresstion_information_1.lastname}
    my_account_page.E-mail field should be displayed customer data and disabled    ${regresstion_information_1.email}
    my_account_page.Phone field should be displayed customer data    ${regresstion_information_1.tel}
    my_account_page.Gender radio button should be displayed
    my_account_page.Language radio button should be displayed
    my_account_page.Edit button should be displayed

TEST_007 Verify address book page able to display all address book of that account
    [Tags]    regression
    [Setup]    Login to CDS and go to account overview page
    my_account_page.Click on my address book page
    my_account_page.Address book page should be displayed customer default shipping address book
    my_account_page.Address book page should be displayed customer default billing address book
    my_account_page.Verify default shipping address book should be selected  ${regresstion_username_1}    ${regresstion_password_1}
    my_account_page.Verify default billing address book should be selected  ${regresstion_username_1}    ${regresstion_password_1}

TEST_008 Verify wishlist page able to remove wishlist product
    [Tags]    regression
    Login to CDS
    home_page_web_keywords.Search product by product sku    ${CDS11008187}[sku]
    product_page.Click add product to wishlist button
    Click my account button
    Click my account menu under my account dropdown
    my_account_page.Click on wishlist tab
    wishlist_page.Product should be displayed on wishlist page    ${CDS11008187}[name]
    wishlist_page.Remove wishlist product
    wishlist_page.Product should not be visible on wishlist page    ${CDS11008187}[name]

#Verify Wishlist page
TEST_009 Verify wishlist page able to display wishlist product, able to add product to cart
    [Tags]    regression
    Login to CDS
    home_page_web_keywords.Search product by product sku    ${CDS11008187}[sku]
    product_page.Click add product to wishlist button
    Click my account button
    Click my account menu under my account dropdown
    my_account_page.Click on wishlist tab
    wishlist_page.Product should be displayed on wishlist page    ${CDS11008187}[name]
    wishlist_page.Clicking add to cart button on wishlist page    ${CDS11008187}[sku]
    common_cds_web.Click view cart button should not be visble
    Header Web - Click Mini Cart Button
    header_web_keywords.Product should be displayed in mini cart    ${CDS11008187}[sku]    ${CDS11008187}[name]
    Header Web - Click View Cart Button
    my_cart_page.Product should be displayed in my cart page    ${CDS11008187}[sku]    ${CDS11008187}[name]

TEST_010 Verify orders page able to search order by number of sale order
    [Tags]    regression
    [Setup]    Login to CDS and go to account overview page
    Click my account button
    Click my account menu under my account dropdown
    my_account_page.Click on my order tab
    Search order by order number    ${order_id_transfer_personal_member}
    Wait Until Page Loader Is Not Visible
    Order number should be displayed in order history   ${order_id_transfer_personal_member}
    [Teardown]    CommonWebKeywords.Test Teardown

TEST_011 Verify orders page able to display sale orders detail correctly
    [Tags]    regression
    order_history.Order number should be displayed in order history    ${order_id_transfer_personal_member}
    order_history.Order status should be displayed    ${order_history.pending_payment}
    order_history.Delivery option should be displayed    ${order_history.pick_up_familymart}
    order_history.Payment type should be displayed    ${thankyou_page.payment_transfer}

