*** Settings ***
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

*** Variables ***
${username}      sirinapa@central.tech
${password}      Cen1234!
${displayname}    kook
${cust_email}    cds_robot03@mail.com
${wrong_format}      wrong_format
${wrong_email}       wrongemail@gmail.com
${wrong_email_format}     tester@gg
${wrong_email_format1}     test
${wrong_email_format2}     tester@
${wrong_email_format3}     test@g
${wrong_email_format4}     @gmil.com
${wrong_email_format5}     test#gmail.com
${firstname}     firstname
${lastname}     lastname
${non_existed_email}     cds_regresstion_30@mail.com
${existed_email}     cds_regresstion_01@mail.com
${invalid_pass_no_number}     Robotabcd@
${invalid_pass_lowercase}     robotabcd@
${invalid_pass_uppercase}     ROBOT@12345
${invalid_pass_less_8_char}     Ro@1
&{dict_2_products}    ${product_sku_list}[4]=1   ${product_sku_list}[8]=1
&{dict_3_products}    ${product_sku_list}[3]=1  ${product_sku_list}[10]=1  ${product_sku_list}[11]=1
&{dict_4_products}    ${product_sku_list}[4]=1  ${product_sku_list}[8]=1  ${product_sku_list}[7]=1  ${product_sku_list}[9]=1
${website_url}    url=${URL_website}/${language}

*** Keywords ***
Verify forgot password error message is displayed correctly
    [Arguments]    ${email}    ${error_msg}
    forgot_password_keywords.Forgot Password With Email    ${email}
    Forgot Password - E-mail Field Display Error Message    ${error_msg}

Verify login error message is displayed correctly when invalid login
    [Arguments]    ${username}    ${password}
    Login Keywords - Login with Email    ${username}    ${password}
    Login Keyword - Verify Error Message When Login Failed    ${error_message.login_email.login_failed}
    Header Web - Click Login on Header
    Header Web - Verify Error Message Not Visible

Verify register error message is displayed correctly when input invalid value
    [Arguments]    ${firstname}    ${lastname}    ${email}   ${password}    ${err_msg}
    registration_page.Input Firstname    ${firstname}
    registration_page.Input Lastname     ${lastname}
    registration_page.Input Email    ${email}
    registration_page.Input Password For Register Page    ${password}
    registration_page.Click Submit Button
    registration_page.Verify registration page should display error message    ${err_msg}

*** Test Cases ***
To verify that Web or Mobile user should be automatically logged-in to the system after wait for 3 seconds
    [Tags]    regression    authentication    testrailid:C71505    testrailid:C26434
    Run Keyword And Ignore Error  customer_api.Delete registeration account from mdc    ${register_email}    ${register_password}
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Register Button
    registration_page.Input Firstname    ${register_information.firstname}
    registration_page.Input Lastname    ${register_information.lastname}
    registration_page.Input Email    ${register_email}
    registration_page.Input Password For Register Page    ${register_password}
    registration_page.Click Submit Button
    registration_keywords.verify registration success message after register
    customer_api.Delete registeration account from mdc    ${register_email}    ${register_password}

To verify warning message of registration page when missing important information
    [Tags]    regression    authentication    testrailid:C26436
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Register Button
    registration_page.Input Lastname    ${cds_authentication_information_1.lastname}
    registration_page.Input Password For Register Page    ${cds_authentication_password_1}
    registration_page.Click Submit Button
    registration_page.Verify registeration page with error message in firstname field
    registration_page.Verify registeration page with error message in email field
    registration_page.Verify registeration fail and should not contain welcome message

To clicking login with facebook button in register for individual customer or login page to access facebook login pop-up
    [Tags]    regression    authentication    testrailid:C22631
    header_web_keywords.Header Web - Click Login on Header
    registration_page.Header Web - Click Facebook login Button
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_4]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window by URL and verify title    ${website_url}  ${windows_locator_title}
    registration_page.Verify registeration message success with welcome message

To validation connecting not success with facebook account, new account don't be created for individual type
    [Tags]    regression    authentication    testrailid:C22634
    header_web_keywords.Header Web - Click Login on Header
    registration_page.Header Web - Click Facebook login Button
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_3]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Verify registeration Error box facebook email

To validation connecting success with facebook email didn't exist in individual type system to register individual account
    [Tags]    regression    authentication    testrailid:C22632
    header_web_keywords.Header Web - Click Login on Header
    registration_page.Header Web - Click Facebook login Button
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_4]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window by URL and verify title    ${website_url}  ${windows_locator_title}
    registration_page.Verify registeration message success with welcome message

To validation connecting success with facebook email matches with existing email in individual type to associate profile
    [Tags]    regression    authentication    testrailid:C22633
    header_web_keywords.Header Web - Click Login on Header
    registration_page.Header Web - Click Facebook login Button
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_5]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window by URL and verify title    ${website_url}  ${windows_locator_title}
    registration_page.Verify registeration message success with welcome message

To verify when doesn't input any data then click log in button
    [Tags]    regression    authentication    testrailid:C22892
    Login Keywords - Login with Email    ${EMPTY}    ${EMPTY}
    Login Keyword - Email field Should Be Visible Error    ${error_message.login_email.required_field}
    Login Keyword - Password field Should Be Visible Error    ${error_message.login_email.required_field}

To verify when username are incorrect
    [Tags]    regression    authentication    testrailid:C22895
    [Template]    Verify login error message is displayed correctly when invalid login
    wrong_${username}    ${password}

To verify when password are incorrect
    [Tags]    regression    authentication    testrailid:C22895
    [Template]    Verify login error message is displayed correctly when invalid login
    ${username}    wrong_${password}

To verify that email address field on forgot password page when use input wrong email, the field should be display error message
    [Tags]    regression    authentication    testrailid:C23699
    [Template]    Verify forgot password error message is displayed correctly
    ${wrong_format}    ${error_message.forgot_password.invalid_format}

To verify that forgot password with email not register, the page should be display error message
    [Tags]    regression    authentication    testrailid:C23700
    [Template]    Verify forgot password error message is displayed correctly
    ${wrong_email}    ${error_message.forgot_password.invalid_email}

To verify retry sent email for forgot password can working correctly
    [Tags]    regression    authentication    testrailid:C23703
    Forgot Password Success    ${cust_email}
    Send E-mail Success Message Should Be Visible    ${success_message.forgot_password.sent_email_success}

To verify system automatically logged-in after click continue
    [Tags]    regression    authentication    testrailid:C71506
    Run Keyword And Ignore Error  customer_api.Delete registeration account from mdc    ${register_email}    ${register_password}
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Register Button
    registration_page.Input Firstname    ${register_information.firstname}
    registration_page.Input Lastname    ${register_information.lastname}
    registration_page.Input Email    ${register_email}
    registration_page.Input Password For Register Page    ${register_password}
    registration_keywords.Submit And Verify Registeration Success
    customer_api.Delete registeration account from mdc    ${register_email}    ${register_password}

To verify register fail when email has already register
    [Tags]    regression    authentication    testrailid:C26435
    [Setup]    Run Keywords  common_cds_web.Setup - Open browser  AND  registration_page.Test Setup - Click login and register on header
    [Template]    Verify register error message is displayed correctly when input invalid value
    ${firstname}    ${lastname}   ${existed_email}    ${register_password}    ${error_message.account_registration.email_existed}

To verify register fail when user input password without number, lowercase , uppercase and less than 8 character
    [Tags]    regression    authentication    testrailid:C26437
    [Setup]    Run Keywords  common_cds_web.Setup - Open browser  AND  registration_page.Test Setup - Click login and register on header
    [Template]    Verify register error message is displayed correctly when input invalid value
    ${firstname}    ${lastname}   ${non_existed_email}    ${invalid_pass_no_number}    ${error_message.account_registration.invalid_password}
    ${firstname}    ${lastname}   ${non_existed_email}    ${invalid_pass_lowercase}    ${error_message.account_registration.invalid_password}
    ${firstname}    ${lastname}   ${non_existed_email}    ${invalid_pass_uppercase}    ${error_message.account_registration.invalid_password}
    ${firstname}    ${lastname}   ${non_existed_email}    ${invalid_pass_less_8_char}    ${error_message.account_registration.invalid_password}

To verify register with invalid email
    [Tags]    regression    authentication    testrailid:C26438
    [Setup]    Run Keywords  common_cds_web.Setup - Open browser  AND  registration_page.Test Setup - Click login and register on header
    [Template]    Verify register error message is displayed correctly when input invalid value
    ${firstname}    ${lastname}   ${wrong_format}    ${register_password}    ${error_message.account_registration.invalid_email}

To verify when input invalid email format
    [Tags]    regression    authentication    testrailid:C22893
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Input Email on Header  ${wrong_email_format}
    header_web_keywords.Header Web - Click Submit Button on Header
    header_web_keywords.Hearder Web - Email should be displayed error message  ${error_message.account_registration.invalid_email}

To verify that able to login via email and password successfuly
    [Tags]    regression    authentication    testrailid:C22896
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Input Email on Header  ${cds_authentication_username_1}
    header_web_keywords.Header Web - Input Password on Header  ${cds_authentication_password_1}
    header_web_keywords.Header Web - Click Submit Button on Header
    header_web_keywords.Verify display name with welcome message should be displayed

To verify when click outside pop-up to close the popup
    [Tags]    regression    authentication    testrailid:C23189
    header_web_keywords.Header Web - Click Login on Header
    home_page_web_keywords.Click on search box
    header_web_keywords.Verify pop-up should not be displayed

To verify that guest click view cart button on mini cart
    [Tags]    regression    authentication    testrailid:C85170
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]  1
    my_cart_page.Verify shopping bag page should be displayed

To validate LOG IN OR CONTINUE AS GUEST page show information correctly
    [Tags]    regression    authentication    testrailid:C85171
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]  1
    Wait Until Page Is Completely Loaded
    my_cart_page.Click Secure Checkout Button
    checkout_page.Click guest login button
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.title}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.registered_customer}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.email}
    guest_login_page.Verify email place holder display correctly
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.password}
    guest_login_page.Verify password place holder display correctly
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.forgot_password}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.login}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.login_facebook}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.new_to_central}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.new_acc_description}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.checkout_guest}

To verify that guest can continue checkout process in case Member has no item in cart
    [Tags]    regression    authentication    testrailid:C85211
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]  1
    my_cart_page.Click Secure Checkout Button
    checkout_page.Click guest login button
    guest_login_page.Input username, password and click login button at guest login page  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    checkout_page.Verify that product and quantity should be displayed correctly  ${product_sku_list}[2]  1
    checkout_page.Verify total of products should be displayed correctly at order summary on Delivery Details page  1

To verify that guest can login success via email at checkout step1 (Delivery Detail)
    [Tags]    regression    authentication    testrailid:C85234
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    checkout_keywords.Add multiple product to cart    ${product_sku_list}[2]    ${product_sku_list}[3]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Click guest login button
    guest_login_page.Input username, password and click login button at guest login page  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    checkout_page.Verify that product and quantity should be displayed correctly  ${product_sku_list}[2]  1
    checkout_page.Verify that product and quantity should be displayed correctly  ${product_sku_list}[3]  1
    checkout_page.Verify total of products should be displayed correctly at order summary on Delivery Details page  2

To verify that guest can login with facebook at checkout step1 (Delivery Detail)
    [Tags]    regression    authentication    testrailid:C85235
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    checkout_keywords.Add multiple product to cart    ${product_sku_list}[2]    ${product_sku_list}[3]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Click guest login button
    guest_login_page.Click login with facebook
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_5]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window locator page title    ${dictCheckoutPage}[windows_checkout_locator]
    checkout_page.Verify that product and quantity should be displayed correctly  ${product_sku_list}[2]  1
    checkout_page.Verify that product and quantity should be displayed correctly  ${product_sku_list}[3]  1
    checkout_page.Verify total of products should be displayed correctly at order summary on Delivery Details page  2

To verify that page will redirect to Delivery page after login successful
    [Tags]    regression    authentication    testrailid:C85239
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]  1
    my_cart_page.Click Secure Checkout Button
    checkout_page.Click guest login button
    guest_login_page.Input username, password and click login button at guest login page  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    checkout_page.Verify Delivery Details page should be displayed

To verify that error message is shown correctly if guest input invalid information
    [Tags]    regression    authentication    testrailid:C85238
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Facebook Button on Header
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_5]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window by URL and verify title    ${website_url}  ${windows_locator_title}
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product and add multiple products to cart  ${dict_4_products}
    login_keywords.Login Keywords - Sign Out Success
    Close All Browsers
    common_cds_web.Setup - Open browser
    common_cds_web.Search product and add multiple products to cart  ${dict_3_products}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Click guest login button
    guest_login_page.Input text and verify email error message should be displayed  ${wrong_email_format1}
    guest_login_page.Input text and verify email error message should be displayed  ${wrong_email_format2}
    guest_login_page.Input text and verify email error message should be displayed  ${wrong_email_format3}
    guest_login_page.Input text and verify email error message should be displayed  ${wrong_email_format4}
    guest_login_page.Input text and verify email error message should be displayed  ${wrong_email_format5}
    guest_login_page.Input username, password and click login button at guest login page  ${existed_email}  ${invalid_pass_no_number}
    guest_login_page.Verify incorrect account error message should be displayed

To verify that system can merge cart after guest login correctly
    [Tags]    regression    authentication    testrailid:C85236
    common_cds_web.Test Setup - Remove all product in shopping cart   ${cds_authentication_username_1}    ${cds_authentication_password_1}
    login_keywords.Login Keywords - Login Success   ${cds_authentication_username_1}    ${cds_authentication_password_1}    ${cds_authentication_information_1.firstname}
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product and add multiple products to cart  ${dict_3_products}
    login_keywords.Login Keywords - Sign Out Success
    common_keywords.Wait Until Page Is Completely Loaded
    common_cds_web.Search product and add multiple products to cart  ${dict_2_products}
    login_keywords.Login Keywords - Login Success   ${cds_authentication_username_1}    ${cds_authentication_password_1}    ${cds_authentication_information_1.firstname}
    header_web_keywords.Header Web - Click Mini Cart Button
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Verify total of products should be displayed correctly at order summary on Delivery Details page  5

To verify that system can merge cart after guest login with facebook correctly
    [Tags]    regression    authentication    testrailid:C85237
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Facebook Button on Header
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_5]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window by URL and verify title    ${website_url}  ${windows_locator_title}
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product and add multiple products to cart  ${dict_4_products}
    login_keywords.Login Keywords - Sign Out Success
    Close All Browsers
    common_cds_web.Setup - Open browser
    common_cds_web.Search product and add multiple products to cart  ${dict_3_products}
    header_web_keywords.Header Web - Click View Cart Button
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Facebook Button on Header
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_5]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window locator page title    ${cart_page_locator}
    my_cart_page.Click Secure Checkout Button
    payment_keywords.Verify product total after guest and member is merged cart after login successfully    7
