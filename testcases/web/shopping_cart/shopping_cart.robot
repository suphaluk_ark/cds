*** Settings ***
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Force Tags    ruleid:654

*** Variables ***
@{sku_list}    ${product_sku_list}[2]  ${product_sku_list}[0]
${dict_2_products}    ${product_sku_list}[0]=1   ${product_sku_list}[2]=1

#buy 2 will be discounted
${discount_product}    ${product_sku_list}[0]
# gify only has qty of 5
${product_not_enough_gift}  ${product_sku_list}[17]
${product_with_free_item}    ${product_sku_list}[17]
${free_gift_sku}    ${product_sku_list}[12]
${coupon}    COUPON10

*** Test Cases ***
To verify the ordered price for free shipping that user sets value in MDC (Standard Delivery)
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C24789
    common_cds_web.Search product and add product to cart  ${product_sku_list}[0]
    header_web_keywords.Verify message if add item has total price less than 699 THB on mini cart
    common_cds_web.Search product and add product to cart  ${product_sku_list}[2]
    header_web_keywords.Verify FREE standard delivery message

To verify that "CHECKOUT" button must be changed to view cart and url must be redirected correctly (Only Desktop and Only Guest)
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C24790
    common_cds_web.Search product and add product to cart  ${product_sku_list}[0]  2
    header_web_keywords.Verify view cart button should be displayed correctly as a guest
    login_keywords.Login Keywords - Login Success   ${cds_shopping_cart_username}    ${cds_shopping_cart_password}    ${cds_shopping_cart_information.firstname}
    header_web_keywords.Header Web - Click Mini Cart Button
    header_web_keywords.Verify view cart button should be displayed correctly when member login
    header_web_keywords.Header Web - Click View Cart Button
    Wait Until Page Is Completely Loaded
    my_cart_page.Verify shopping bag page should be displayed
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    cart_api.Remove All Products From Cart Using API    ${cds_shopping_cart_username}    ${cds_shopping_cart_password}
    ...    AND    Close Browser

To verify the display of total quantity and total price (Not include promotional discount price)
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C25261
    common_cds_web.Search product and add multiple products to cart  ${dict_2_products}
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[3]
    Wait Until Page Is Completely Loaded
    my_cart_page.Verify total quantity must be displayed correctly
    my_cart_page.Verify price grand total should be caculated on cart page correctly

To verify the display of grand total
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C25262
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${cds_shopping_cart_username}  ${cds_shopping_cart_password}
    login_keywords.Login Keywords - Login Success   ${cds_shopping_cart_username}    ${cds_shopping_cart_password}    ${cds_shopping_cart_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page  @{sku_list}[0]  1
    my_cart_page.Verify price grand total should be caculated on cart page correctly
    common_cds_web.Search product, add product to cart, go to checkout page  ${discount_product}  2
    my_cart_page.Verify price grand total should be caculated on cart page correctly

To verify the remaining price for free shipping must be calculated and displayed correctly
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C282788
    common_cds_web.Search product and add product to cart    ${product_sku_list}[16]    2
    common_cds_web.Search product and add product to cart    ${product_sku_list}[0]    2
    header_web_keywords.Verify FREE standard delivery message
    header_web_keywords.Header Web - Click Mini Cart Button
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product and add product to cart    ${product_sku_list}[0]    1
    header_web_keywords.Verify message if add item has total price less than 699 THB on mini cart

To verify that user clicks "SECURE CHECKOUT" button
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C25263
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]  1
    my_cart_page.Click Secure Checkout Button
    checkout_page.Verify Delivery Details page should be displayed

To verify the order summary section and payment summary section must be displayed correctly
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C25859
    common_cds_web.Search product and add product to cart  ${product_sku_list}[2]  2
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[4]
    my_cart_page.Click Secure Checkout Button
    delivery_details_page.Verify order summary should be displayed at delivery details page
    delivery_details_page.Verify payment summary should be displayed at delivery details page
    header_web_keywords.Switch to English language
    delivery_details_page.Verify order summary should be displayed at delivery details page
    delivery_details_page.Verify payment summary should be displayed at delivery details page

To verify the order summary section must be displayed correctly
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C25860
    common_cds_web.Search product and add product to cart  ${product_sku_list}[2]  1
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[4]
    my_cart_page.Verify brand of product should be displayed on cart page
    my_cart_page.Verify quantity of product should be displayed
    my_cart_page.Click Secure Checkout Button
    delivery_details_page.Verify brand name should be displayed
    delivery_details_page.Verify image of product should be displayed
    delivery_details_page.Verify price should be displayed
    delivery_details_page.Verify product name should be displayed
    delivery_details_page.Verify quantity should be displayed
    delivery_details_page.Verify edit bag should be displayed

To verify the "Edit Bag" link
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C25861
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    my_cart_page.Click Secure Checkout Button
    delivery_details_page.Click on edit bag on delivery page
    my_cart_page.Verify shopping bag page should be displayed

To verify the total and order total
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C26443
    common_cds_web.Search product and add product to cart  ${product_sku_list}[2]  2
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]  5
    my_cart_page.Verify price grand total should be caculated on cart page correctly

To verify the shipping message
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C26423
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]  2
    my_cart_page.Verify FREE standard delivery message
    header_web_keywords.Remove all product in shopping cart
    home_page_web_keywords.Search product by product sku    ${${product_sku_list}[0]}[sku]
    product_page.Add product quantity    ${product_sku_list}[0]    1
    product_page.Click on add to cart button    ${product_sku_list}[0]
    home_page_web_keywords.Search product by product sku    ${${product_sku_list}[0]}[sku]
    product_page.Add product quantity    ${product_sku_list}[0]    1
    product_page.Click on add to cart button    ${product_sku_list}[0]
    header_web_keywords.Header Web - Click Mini Cart Button
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Verify message if add item has total price less than 699 THB

To verify that the remaining price must be calculated correctly
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C86804
    common_cds_web.Search product and add product to cart  ${product_sku_list}[0]  2
    header_web_keywords.Verify message if add item has total price less than 699 THB on mini cart
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Verify message if add item has total price less than 699 THB
    my_cart_page.Verify price grand total should be caculated on cart page correctly
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product and add product to cart  ${product_sku_list}[4]  2
    header_web_keywords.Verify FREE standard delivery message
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Verify FREE standard delivery message

To verify that user changes the quantity
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C49251
    common_cds_web.Search product and add product to cart  ${product_sku_list}[2]  2
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    my_cart_page.Change quantity of product from the drop-down list  ${product_sku_list}[2]  2
    my_cart_page.Change quantity of product from the drop-down list  ${product_sku_list}[0]  3
    my_cart_page.Verify sub-total of multiple items is displayed correcty on cart page  @{sku_list}
    my_cart_page.Verify price sub-total should be displayed correctly (not include promo discount)  @{sku_list}
    my_cart_page.Verify price grand total should be caculated on cart page correctly

To verify that user clicks "Remove" button
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C49252
    common_cds_web.Search product and add product to cart  ${product_sku_list}[2]  2
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]  5
    my_cart_page.Remove product from shopping bag  ${product_sku_list}[2]
    @{product_list}=  my_cart_page.Get new product sku list after removing a product  @{sku_list}  ${product_sku_list}[2]
    Wait Until Page Is Completely Loaded
    my_cart_page.Verify sub-total of multiple items is displayed correcty on cart page  @{product_list}
    my_cart_page.Verify price sub-total should be displayed correctly (not include promo discount)  @{product_list}
    my_cart_page.Verify price grand total should be caculated on cart page correctly

To verify that if products has a gift and there is available gift in a stock
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C79569
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_with_free_item}
    my_cart_page.Free gift message should be displayed in cart page
    my_cart_page.Click on free gift message

To verify that a gift is not enough while user buying a product
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C79573
    login_keywords.Login Keywords - Login Success    ${cds_shopping_cart_username}    ${cds_shopping_cart_password}    ${cds_shopping_cart_information.firstname}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_not_enough_gift}    6
    my_cart_page.Click on free gift message
    my_cart_page.Verify not enough free gift message should be displayed
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    cart_api.Remove All Products From Cart Using API    ${cds_shopping_cart_username}    ${cds_shopping_cart_password}
    ...    AND    Close Browser

To verify that freebies item will not count on mini cart if user is guest
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C75012
    common_cds_web.Search product and add product to cart  ${product_with_free_item}
    header_web_keywords.Verify product quantity should be displayed correctly on mini cart  ${product_with_free_item}  1

To verify freebies item will not count on mini cart if user is member
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C86714
    login_keywords.Login Keywords - Login Success   ${cds_shopping_cart_username}    ${cds_shopping_cart_password}    ${cds_shopping_cart_information.firstname}
    Wait Until Page Is Completely Loaded
    common_cds_web.Search product and add product to cart  ${product_with_free_item}
    header_web_keywords.Verify product quantity should be displayed correctly on mini cart  ${product_with_free_item}  1
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    cart_api.Remove All Products From Cart Using API    ${cds_shopping_cart_username}    ${cds_shopping_cart_password}
    ...    AND    Close Browser

To validate free gift with this purchase item shows correctly in case added product by guest
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C79544
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_with_free_item}
    Wait Until Page Is Completely Loaded
    my_cart_page.Click on free gift message
    my_cart_page.Verify free gift image should be displayed on cart page  ${product_sku_list}[12]
    my_cart_page.Verify free gift product name should be displayed on cart page  ${product_sku_list}[12]
    my_cart_page.Verify free gift brand name should be displayed on cart page  ${product_sku_list}[12]
    my_cart_page.Verify free gift quantity should be displayed on cart page  ${product_sku_list}[12]
    my_cart_page.Verify free gift size or color should be displayed on cart page  ${product_sku_list}[12]
    my_cart_page.Verify free gift price should be displayed on cart page  ${product_sku_list}[12]

To validate free gift with this purchase item shows correctly incase added product by member
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C86711
    login_keywords.Login Keywords - Login Success   ${cds_shopping_cart_username}    ${cds_shopping_cart_password}    ${cds_shopping_cart_information.firstname}
    Wait Until Page Is Completely Loaded
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_with_free_item}
    Wait Until Page Is Completely Loaded
    my_cart_page.Click on free gift message
    my_cart_page.Verify free gift image should be displayed on cart page  ${free_gift_sku}
    my_cart_page.Verify free gift product name should be displayed on cart page  ${free_gift_sku}
    my_cart_page.Verify free gift brand name should be displayed on cart page  ${free_gift_sku}
    my_cart_page.Verify free gift quantity should be displayed on cart page  ${free_gift_sku}
    my_cart_page.Verify free gift size or color should be displayed on cart page  ${free_gift_sku}
    my_cart_page.Verify free gift price should be displayed on cart page  ${free_gift_sku}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    cart_api.Remove All Products From Cart Using API    ${cds_shopping_cart_username}    ${cds_shopping_cart_password}
    ...    AND    Close Browser

To validate free gift with this purchase item shows correctly after merging cart guest to member
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C86712
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_with_free_item}
    login_keywords.Login Keywords - Login Success   ${cds_shopping_cart_username}    ${cds_shopping_cart_password}    ${cds_shopping_cart_information.firstname}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Header Web - Click Mini Cart Button
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Click on free gift message
    my_cart_page.Verify free gift image should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift product name should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift brand name should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift quantity should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift size or color should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift price should be displayed on cart page    ${free_gift_sku}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    cart_api.Remove All Products From Cart Using API    ${cds_shopping_cart_username}    ${cds_shopping_cart_password}
    ...    AND    Close Browser

To validate free gift with this purchase item shows correctly after merging cart guest to facebook account
    [Tags]    jenkins_group1    regression    shopping_cart    testrailid:C86713
    common_cds_web.Search product, add product to cart, go to checkout page  ${product_with_free_item}
    Header Web - Click Login on Header
    Header Web - Click Facebook Button on Header
    registration_page.Select Window locator page title    ${dictRegistrationPage}[locator_facebook]
    registration_page.Input Email With Facebook    ${facebook_email}[facebook_email_test_5]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Select Window locator page title    ${cart_page_locator}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Header Web - Click Mini Cart Button
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Click on free gift message
    my_cart_page.Verify free gift image should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift product name should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift brand name should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift quantity should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift size or color should be displayed on cart page    ${free_gift_sku}
    my_cart_page.Verify free gift price should be displayed on cart page    ${free_gift_sku}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser
