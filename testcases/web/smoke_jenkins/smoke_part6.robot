*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers
Test Template    Template - Verify Canceled Order
Force Tags    smoke    production_smoke

*** Keywords ***
Template - Verify Canceled Order
    [Documentation]    ${user} | dict of user info
    ...                ${proudct} | dict of product
    ...                ${member_type} - guest | member
    ...                ${shipping_method} - standard | pickup
    ...                ${payment_method} - 2c2p | 123
    [Arguments]    ${user}    ${product}    ${member_type}    ${shipping_method}    ${payment_method}
    Run Keyword And Ignore Error     Remove customer cart and generate new cart    ${user.email}    ${user.password}
    common_cds_web.Setup - Open browser
    Smoke Template - Login With CDS Account    ${user}    ${member_type}
    common_cds_web.Search product and add product to cart    ${product}
    # Smoke Template - Verify Cart Fee Text
    Header Web - Click View Cart Button
    common_keywords.Wait Until Page Is Completely Loaded
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Smoke Template - Input Customer Detail    ${user}    ${member_type}
    Smoke Template - Select Shipping Method And Input Info    ${member_type}    ${shipping_method}
    checkout_keywords.Verify subtotal in order summary is calculated correctly    ${product}
    checkout_keywords.Verify grand total in order summary is calculated correctly without any discount    ${shipping_method}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Smoke Template - Select Payment Method    ${payment_method}
    Smoke Template - Verify Thank You Page    ${payment_method}

Smoke Template - Login With CDS Account
    [Arguments]    ${user}    ${member_type}
    Run Keyword If    '${member_type}' == 'member'    login_keywords.Login Keywords - Login Success    ${user.email}
    ...    ${user.password}
    ...    ${user.firstname}

Smoke Template - Verify Cart Fee Text
    ${grand_total}=    Get grand price total on mini cart
    Run Keyword If    ${grand_total} < 699    header_web_keywords.Verify message if add item has total price less than 699 THB on mini cart
    ...    ELSE    header_web_keywords.Verify FREE standard delivery message

Smoke Template - Input Customer Detail
    [Arguments]    ${user}    ${member_type}
    Run Keyword If    '${member_type}' == 'guest'    Input customer details    ${user.firstname}
    ...    ${user.lastname}
    ...    ${user.email}
    ...    ${user.tel}

Smoke Template - Select Shipping Method And Input Info
    [Arguments]    ${member_type}    ${shipping_method}
    Run Keyword If    '${shipping_method}' == 'standard'    Smoke Template - Select Standard Delivery    ${member_type}
    ...    ELSE    Select pick at store    ${pick_at_store.central_bangna}

Smoke Template - Select Standard Delivery
    [Arguments]    ${member_type}
    checkout_page.Select shipping to address option
    Run Keyword If    '${member_type}' == 'guest'    Run Keywords    Input shipping details
    ...    AND    Click on standard delivery button
    ...    ELSE    checkout_page.Select default address

Smoke Template - Select Payment Method
    [Arguments]    ${payment_method}
    Run Keyword If    '${payment_method}' == '2c2p'    Smoke Template - Select And Cancel 2C2P
    ...    ELSE    Smoke Template - Select And Submit Bank Transfer

Smoke Template - Select And Cancel 2C2P
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    2c2p.Click on cancel payment button

Smoke Template - Select And Submit Bank Transfer
    Pay by 1 2 3 kbank ATM transfer
    Wait Until Page Loader Is Not Visible

Smoke Template - Verify Thank You Page
    [Arguments]    ${payment_method}
    Run Keyword If    '${payment_method}' == '2c2p'    'Sorry your order is not complete' Msg should be displayed
    ...    ELSE    Thank You Page Should Be Visible

*** Test Cases ***
Member Incomplete Checkout Order With Standard 123
    [Tags]    testrailid:C549552    smoke
    ${member_cancel_order}    ${product_sku_list}[0]    member    standard    123

Member Incomplete Checkout Order With Pickup 123
    [Tags]    testrailid:C549553    covid-19    smoke
    ${member_cancel_order}    ${product_sku_list}[2]    member    pickup    123

Member Incomplete Checkout Order With Standard 2c2p
    [Tags]    testrailid:C549554    smoke
    ${member_cancel_order}    ${product_sku_list}[2]    member    standard    2c2p

Member Incomplete Checkout Order With Pickup 2c2p
    [Tags]    testrailid:C549555    covid-19    smoke
    ${member_cancel_order}    ${product_sku_list}[2]    member    pickup    2c2p

Guest Incomplete Checkout Order With Standard 123
    [Tags]    testrailid:C549556    smoke
    ${guest_cancel_order}    ${product_sku_list}[2]    guest    standard    123

Guest Incomplete Checkout Order With Pickup 123
    [Tags]    testrailid:C549557    covid-19    smoke
    ${guest_cancel_order}    ${product_sku_list}[2]    guest    pickup    123

Guest Incomplete Checkout Order With Standard 2c2p
    [Tags]    testrailid:C549558    smoke
    ${guest_cancel_order}    ${product_sku_list}[2]    guest    standard    2c2p

Guest Incomplete Checkout Order With Pickup 2c2p
    [Tags]    testrailid:C549559    covid-19    smoke
    ${guest_cancel_order}    ${product_sku_list}[2]    guest    pickup    2c2p