*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown       Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

Suite Teardown    Close All Browsers

*** Variables ***

${personal_member}    personal_member
${guest_member}    guest_member
${status_in_transit}    in_transit
${status_delivered}    delivered
${tracking_no}    ROBOT_TRACKING

*** Keyword ***
Go to order history and search order
    Click my account button
    Click my orders button
    Search order by order number    ${order_id_creditcard_personal_member}
    Wait Until Page Loader Is Not Visible
    Order number should be displayed in order history    ${order_id_creditcard_personal_member}

Order history details should be displayed correctly
    SeleniumLibrary.Go Back
    Wait Until Page Loader Is Not Visible
    Search order by order number    ${order_id_creditcard_personal_member}
    Wait Until Page Loader Is Not Visible
    Order status should be displayed correctly in order history    ${order_id_creditcard_personal_member}    ${order_history.status_complete}

Go to order history details by member type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    Run Keywords    Go to order history and search order
    ...    AND    Order status should be displayed correctly in order history    ${order_id_creditcard_personal_member}    ${order_history.status_processing}
    ...    AND    Click view details button by order no.    ${order_id_creditcard_personal_member}
    ...    ELSE IF    '${member_type}' == 'guest_member'    common_cds_web.Guest member search order number    ${guest_information}[email]    ${order_id_creditcard_guest_member}

Successfully order products with credit card method and pickup at store
    [Arguments]    ${member_type}    ${product_sku}
    Test setup access system by Credit card member type    ${member_type}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku}
    common_cds_web.Checkout, select pick at store and pay by credit card by Member Type    ${member_type}
    Thank You Page Should Be Visible
    Set Suite Variable    ${order_id_creditcard_${member_type}}    ${increment_id}
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_credit}
    common_cds_web.Order status from thank you page should be processing
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]
    [Teardown]    Run Keywords    CommonWebKeywords.Keyword Teardown    AND    Close All Browsers

Verify order status in order history page
    [Arguments]    ${member_type}    ${order_no}
    Test setup access system by Credit card member type    ${member_type}
    Go to order history details by member type    ${member_type}
    Wait Until Page Is Completely Loaded
    common_cds_web.Order status on progress bar should be processing status
    E2E_flow_keywords.Update order status to shipped and verify status in MDC, MCOM and order progress bar is complete fullyshipped    ${order_no}    ${product_sku_list}[0]
    E2E_flow_keywords.Update order status to ready to collect and verify status in MDC and order progress bar is fullyshipped    ${order_no}
    E2E_flow_keywords.Update order status to delivered and verify status in MDC and order progress bar is fullyshipped    ${order_no}    ${product_sku_list}[0]
    # 11 Aug 2020 Bug CDS-7883
    Order history details should be displayed correctly
    [Teardown]    Run Keywords    CommonWebKeywords.Keyword Teardown    AND    Close All Browsers

Login to CDS by Credit card and remove product in shopping cart
    Run Keyword And Ignore Error    Remove customer cart and generate new cart    ${credit_card_username}    ${credit_card_password}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${credit_card_username}    ${credit_card_password}    ${credit_card_information.firstname}
    Wait Until Page Is Completely Loaded

Test setup access system by Credit card member type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    Login to CDS by Credit card and remove product in shopping cart
    ...    ELSE IF    '${member_type}' == 'guest_member'    common_cds_web.Setup - Open browser
    Set Test Variable    ${member_type}    ${member_type}

*** Test Cases ***
CDS_SMOKE_01_Successfully order products with credit card method and pickup at store
    [Tags]    smoke    credit_card    testrailid:C379493
    [Template]    Successfully order products with credit card method and pickup at store
    ${guest_member}    ${product_sku_list}[0]
    ${personal_member}    ${product_sku_list}[0]

CDS_SMOKE_05_Order status from mdc should be 'LOGISTICS' status (SMOKE_01 creditcard payment)
    [Tags]    smoke    credit_card    testrailid:C379494
    [Template]    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card
    ${order_id_creditcard_guest_member}
    ${order_id_creditcard_personal_member}

# *** Order history page is changed design, so it makes this test script fail. Will update later after done implement ***
CDS_SMOKE_06_Verify order status in order history page (SMOKE_01 creditcard payment)
   [Tags]    smoke    credit_card
   [Template]    Verify order status in order history page
   ${guest_member}    ${order_id_creditcard_guest_member}
   ${personal_member}    ${order_id_creditcard_personal_member}
