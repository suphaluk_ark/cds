*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

Suite Teardown    Close All Browsers

*** Variables ***
${personal_member}    personal_member
${guest_member}    guest_member
${status_in_transit}    in_transit
${status_delivered}    delivered
${tracking_no}    ROBOT_TRACKING

*** Keyword ***
Verify payment credit card fail and Re-payment
    [Arguments]    ${member_type}    ${product_sku}
    Test setup access system by re_payment member type    ${member_type}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku}
    common_cds_web.Checkout and go to payment page by member type    ${member_type}
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Cancel payment credit card
    checkout_page.'Sorry your order is not complete' Msg should be displayed
    Run Keyword If    '${ENV.lower()}'=='staging'    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status from MCOM should be 'ONHOLD' status    ${increment_id}
    Run Keyword If    '${ENV.lower()}'=='staging'    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}
    # 11 Aug 2020 - In progress CDS-6233
    Verify Re-payment by credit card    ${member_type}    ${product_sku}
    [Teardown]    Run Keywords    CommonWebKeywords.Keyword Teardown    AND    Close All Browsers

Verify Re-payment by credit card
    [Arguments]    ${member_type}    ${product_sku}
    Re-payment by credit card
    Return From Keyword If    '${ENV.lower()}'=='prod'
    Wait Until Page Is Completely Loaded
    Thank You Page Should Be Visible
    Set Suite Variable    ${order_id_creditcard_${member_type}}    ${increment_id}
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_credit}
    common_cds_web.Order status from thank you page should be processing
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_id_creditcard_${member_type}}

Login to CDS by re_payment member and remove product in shopping cart
    Run Keyword And Ignore Error    Remove customer cart and generate new cart    ${re_payment_username}    ${re_payment_password}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${re_payment_username}    ${re_payment_password}    ${re_payment_information.firstname}
    Wait Until Page Is Completely Loaded

Test setup access system by re_payment member type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    Login to CDS by re_payment member and remove product in shopping cart
    ...    ELSE IF    '${member_type}' == 'guest_member'    common_cds_web.Setup - Open browser
    Set Test Variable    ${member_type}    ${member_type}

*** Test Cases ***
CDS_SMOKE_04_Payment credit card fail and Re-payment
    [Tags]    smoke    production    re-payment    testrailid:C379520
    [Template]    Verify payment credit card fail and Re-payment
    ${guest_member}    ${product_sku_list}[2]
    ${personal_member}    ${product_sku_list}[2]