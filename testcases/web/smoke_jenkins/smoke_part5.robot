*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Suite Teardown    Close All Browsers

*** Variables ***
${personal_member}    personal_member
${guest_member}    guest_member
${status_in_transit}    in_transit
${status_delivered}    delivered
${tracking_no}    ROBOT_TRACKING

*** Keyword ***
Successfully order products with 1 2 3 bank transfer method and pickup at skybox and family mart
    [Arguments]    ${member_type}    ${product_sku}
    Test setup access system by bank_transfer member type    ${member_type}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku}
    common_cds_web.Checkout, select pick at store and pay by 1 2 3 bank transfer by Member Type    ${member_type}
    Thank You Page Should Be Visible
    Return From Keyword If    '${ENV.lower()}'=='prod'
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_transfer}
    checkout_page.Order status from thank you page should be    ${thankyou_page.status_pending}
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]    shipping=${0}
    common_cds_web.Order status from mdc should be 'pending payment' status    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status from MCOM should be 'ONHOLD' status    ${increment_id}
    Wait Until Keyword Succeeds    60 x    10 sec    common_cds_web.Order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}
    [Teardown]    Run Keywords    CommonWebKeywords.Keyword Teardown    AND    Close All Browsers

Login to CDS by bank_transfer member and remove product in shopping cart
    Run Keyword And Ignore Error    Remove customer cart and generate new cart    ${bank_transfer_username}    ${bank_transfer_password}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${bank_transfer_username}    ${bank_transfer_password}    ${bank_transfer_information.firstname}
    Wait Until Page Is Completely Loaded

Test setup access system by bank_transfer member type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    Login to CDS by bank_transfer member and remove product in shopping cart
    ...    ELSE IF    '${member_type}' == 'guest_member'    common_cds_web.Setup - Open browser
    Set Test Variable    ${member_type}    ${member_type}

# *** Test Cases ***
# 14 Aug 2020 -- Obsolete case 
# CDS_SMOKE_03_Successfully order products with 1 2 3 bank transfer method and pickup at store
#     [Tags]    smoke    bank_transfer    run    covid-19
#     [Template]    Successfully order products with 1 2 3 bank transfer method and pickup at skybox and family mart
#     ${guest_member}    ${product_sku_list}[2]
#     ${personal_member}    ${product_sku_list}[2]