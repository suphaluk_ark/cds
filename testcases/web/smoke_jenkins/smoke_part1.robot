*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

Suite Teardown    Close All Browsers

*** Variables ***
${personal_member}    personal_member
${guest_member}    guest_member
${outside_bkk}    77110
${order_id_cod_personal_member}    STGCO1901210005784
#this order should be updated
${tracking_no}    ROBOT_TRACKING


*** Keyword ***
Login to CDS by outside bkk member and remove product in shopping cart
    Run Keyword And Ignore Error     Remove customer cart and generate new cart    ${outside_bkk_username}    ${outside_bkk_password}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    Wait Until Page Is Completely Loaded

Test setup access system by outside bkk member type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    Login to CDS by outside bkk member and remove product in shopping cart
    ...    ELSE IF    '${member_type}' == 'guest_member'    common_cds_web.Setup - Open browser
    Set Test Variable    ${member_type}    ${member_type}

Verify shipping method for outside bangkok address
    [Arguments]    ${member_type}    ${product_sku}
    Test setup access system by outside bkk member type    ${member_type}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Run Keyword If    '${member_type}' == 'guest_member'    Input customer details
    checkout_page.Select shipping to address option
    Run Keyword If    '${member_type}' == 'guest_member'    Input shipping details    postcode=${outside_bkk}
    Run Keyword If    '${member_type}' == 'guest_member'    Click on standard delivery button
    Run Keyword If    '${member_type}' == 'personal_member'    checkout_page.Select default address
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Verify delivery shipping option are displayed correctly
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    Pay by 1 2 3 kbank ATM transfer
    Wait Until Page Loader Is Not Visible
    Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_transfer}
    checkout_page.Order status from thank you page should be    ${thankyou_page.status_pending}
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]    shipping=${0}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}
    [Teardown]    Run Keywords    CommonWebKeywords.Keyword Teardown    AND    Close All Browsers

*** Test Cases ***
CDS_SMOKE_07_Verify shipping method for outside bangkok address
    [Tags]    smoke    outside_bkk     testrailid:C377394    testrailid:C985022
    [Template]    Verify shipping method for outside bangkok address
    ${guest_member}    ${product_sku_list}[2]
    ${personal_member}    ${product_sku_list}[2]



