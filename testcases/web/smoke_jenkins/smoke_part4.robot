*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot

Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

Suite Teardown    Close All Browsers

*** Variables ***
${personal_member}    personal_member
${guest_member}    guest_member
${outside_bkk}    77110
${order_id_cod_personal_member}    STGCO1901210005784
${tracking_no}    ROBOT_TRACKING

*** Keyword ***
Login to CDS by COD user and remove product in shopping cart
    Run Keyword And Ignore Error     Remove customer cart and generate new cart    ${cod_payment_username}    ${cod_payment_password}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${cod_payment_username}    ${cod_payment_password}    ${cod_payment_information.firstname}
    Wait Until Page Is Completely Loaded

Test setup access system by COD member type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    Login to CDS by COD user and remove product in shopping cart
    ...    ELSE IF    '${member_type}' == 'guest_member'    common_cds_web.Setup - Open browser
    Set Test Variable    ${member_type}    ${member_type}

Successfully order products with payment COD and successfully ship by standard
    [Arguments]    ${member_type}    ${product_sku}
    Test setup access system by COD member type    ${member_type}
    Log To Console    ${language}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku}
    common_cds_web.Check Out Product Until Payment With COD Method by Member Type    ${member_type}
    checkout_page.Thank You Page Should Be Visible
    Set Suite Variable    ${order_id_cod_${member_type}}    ${increment_id}
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_cod}
    checkout_page.Order status from thank you page should be    ${thankyou_page.status_pending}
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]
    common_cds_web.Order status from mdc should be 'pending' status    ${increment_id}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    [Teardown]    Run Keywords    CommonWebKeywords.Keyword Teardown    AND    Close All Browsers

*** Test Cases ***
CDS_SMOKE_02_common_cds_web - Successfully order products with payment COD and successfully ship by standard
    [Tags]    smoke    cod    testrailid:C379522    testrailid:C985021
    [Template]    Successfully order products with payment COD and successfully ship by standard
    ${guest_member}    ${product_sku_list}[2]
    ${personal_member}    ${product_sku_list}[2]
