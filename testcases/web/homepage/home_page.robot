*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown
...    AND    Close Browser

*** Test Cases ***
Verify Homepage Hot Deal And Best Sellers
    [Tags]    jenkins_group1    home_page    testrailid:C397675    testrailid:C396070    testrailid:C394610
    ${product_name}=    Get Recommend Product Name By Api
    Scroll To Center Page
    Verify Best Seller Section Should Display
    Verify Hot Deal Section Should Display
    Verify Hot Deal Show 20 Items
    Verify Hot Deal Should Contain Product    ${product_name}