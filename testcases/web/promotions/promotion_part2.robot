*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown
...    AND    Close Browser

*** Variables ***
${TEST02}     TEST02
${online_price_product}    ${product_online_price}[0]

*** Keywords ***
Test Setup - Add Dior Product To Cart
    [Arguments]     ${username}    ${password}    ${display_name}
    ${skus}=    Get Product Sku Without Special Price By Brand Name Via Api    dior
    Add Product To Cart Using API    ${username}    ${password}    ${skus}[1]    3
    Add Product To Cart Using API    ${username}    ${password}    ${${product_online_price}[0]}[sku]    3
    ${online_price_total}=    Evaluate    ${${product_online_price}[0]}[price] * 3
    Calculate discount(%) from original total price    ${online_price_total}    10
    ${total_discount}=    Evaluate    500 + ${discount_amount}
    Set Test Variable    ${total_discount}    ${total_discount}
    login_keywords.Login Keywords - Login Success    ${username}    ${password}    ${display_name}

*** Test Cases ***
Verify that discount amount on shopping cart and order are correct
    [Tags]    promotion    jenkins_group1    regression    promotion_auto_discount    testrailid:C138472    testrailid:C138473    ruleid:864    ruleid:865
    [Setup]    Run Keywords    common_cds_web.Setup - Open browser
    ...    AND    Test Setup - Add Dior Product To Cart    ${promotions_simple_username}    ${promotions_simple_password}    ${promotions_simple_information.firstname}
    common_cds_web.Click view cart button should not be visble
    Header Web - Click Mini Cart Button
    Header Web - Click View Cart Button
    Wait Until Page Is Completely Loaded
    Verify other discount in cart page(Baht)    ${total_discount}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    Remove All Products From Cart Using API    ${promotions_simple_username}    ${promotions_simple_password}
    ...    AND    Close Browser

Verify that customer can use e-coupon until limit amount per coupon
    [Tags]    promotion    jenkins_group1    regression    promotion_coupon    testrailid:C70821    ruleid:829
    [Setup]    Run Keywords    common_cds_web.Setup - Open browser
    ...    AND    Add Product To Cart Using API    ${promotions_simple_username}    ${promotions_simple_password}    ${product_sku_list}[2]
    ...    AND    login_keywords.Login Keywords - Login Success    ${promotions_simple_username}    ${promotions_simple_password}    ${promotions_simple_information.firstname}
    common_cds_web.Click view cart button should not be visble
    Header Web - Click Mini Cart Button
    Header Web - Click View Cart Button
    Wait Until Page Is Completely Loaded
    Apply coupon code    ${TEST02}
    Coupon Error Message Should Display
    Login Keywords - Logut Success
    Add Product To Cart Using API    ${cds_promotion_coupon_username}    ${cds_promotion_coupon_password}    ${product_sku_list}[2]
    login_keywords.Login Keywords - Login Success    ${cds_promotion_coupon_username}    ${cds_promotion_coupon_password}    ${cds_promotion_coupon_information.firstname}
    common_cds_web.Click view cart button should not be visble
    Header Web - Click Mini Cart Button
    Header Web - Click View Cart Button
    Wait Until Page Is Completely Loaded
    Apply coupon code    ${TEST02}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${TEST02}    ${${product_sku_list}[7]}[price]   ${10.0}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    Remove All Products From Cart Using API    ${promotions_simple_username}    ${promotions_simple_password}
    ...    AND    Remove All Products From Cart Using API    ${cds_promotion_coupon_username}    ${cds_promotion_coupon_password}
    ...    AND    Close Browser