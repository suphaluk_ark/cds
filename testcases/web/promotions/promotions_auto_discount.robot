*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown
...    AND    Remove All Products From Cart Using API    ${login_username}    ${login_password}
...    AND    Close All Browsers

*** Variables ***
${login_username}    ${promotions_simple_redeem_username}
${login_password}    ${promotions_simple_redeem_password}
${login_display_name}    ${promotions_simple_redeem_information.firstname}


*** Test Cases ***
To verify that Subtotal with discount incl tax can be applied with 'Percent of product price discount'
    [Tags]    promotion    jenkins_group1    regression    promotion_auto_discount    testrailid:C144443    ruleid:832    ruleid:830
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${login_username}    ${login_password}    ${login_display_name}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[15]    ${qty_product.Q4} 
    my_cart_page.Verify auto discount by percent(%) is displayed amount correctly    ${percent_discount.five}    ${${product_sku_list}[15]}[price]    ${qty_product.Q4}
    my_cart_page.Change quantity of product from the drop-down list    ${product_sku_list}[15]     ${qty_product.Q5}
    my_cart_page.Verify auto discount and grand total is displayed amount correctly after apply mulitple promotion rule    ${50}     ${percent_discount.five}    ${${product_sku_list}[15]}[price]    ${qty_product.Q5}
    my_cart_page.Change quantity of product from the drop-down list    ${product_sku_list}[15]     ${qty_product.Q3}
    my_cart_page.Verify other discount should not display
    my_cart_page.Verify grand total price is display correctly after deduct discount    ${0}    ${${product_sku_list}[15]}[price]   ${qty_product.Q3}

To verify that Subtotal with discount incl tax can be applied with 'Fixed amount discount'
    [Tags]    promotion    jenkins_group1    regression    promotion_auto_discount    testrailid:C144448    ruleid:836    ruleid:837
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${login_username}    ${login_password}    ${login_display_name}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[11]    ${qty_product.Q1} 
    my_cart_page.Verify auto discount by fix amount is displayed amount correctly    ${200}    ${${product_sku_list}[11]}[price]    ${qty_product.Q1}
    my_cart_page.Change quantity of product from the drop-down list    ${product_sku_list}[11]     ${qty_product.Q2}
    my_cart_page.Verify auto discount and grand total is displayed amount correctly after apply mulitple fixed discount promotion rules    ${200}     ${50}    ${${product_sku_list}[11]}[price]    ${qty_product.Q2}
    my_cart_page.Change quantity of product from the drop-down list    ${product_sku_list}[11]     ${qty_product.Q1}
    my_cart_page.Verify grand total price is display correctly after deduct discount    ${200}    ${${product_sku_list}[11]}[price]   ${qty_product.Q1}

To verify that Subtotal with discount incl tax can be applied with 'Fixed amount discount for whole cart'
    [Tags]    promotion    jenkins_group1    regression    promotion_auto_discount    testrailid:144449    ruleid:838    ruleid:839
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${login_username}    ${login_password}    ${login_display_name}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[1]    ${qty_product.Q2} 
    my_cart_page.Verify auto discount by fix amount is displayed amount correctly    ${650}    ${${product_sku_list}[1]}[price]    ${qty_product.Q1}
    my_cart_page.Change quantity of product from the drop-down list    ${product_sku_list}[1]     ${qty_product.Q3}
    my_cart_page.Verify auto discount and grand total is displayed amount correctly after apply mulitple fixed discount promotion rules    ${650}     ${50}    ${${product_sku_list}[1]}[price]    ${qty_product.Q3}
    my_cart_page.Change quantity of product from the drop-down list    ${product_sku_list}[1]     ${qty_product.Q2}
    my_cart_page.Verify grand total price is display correctly after deduct discount    ${650}    ${${product_sku_list}[1]}[price]   ${qty_product.Q2}