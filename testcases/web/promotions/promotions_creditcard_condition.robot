*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers
Suite Teardown    Close All Browsers

*** Variables ***
${price_discount}    950
${discount_BLL_5%}    0.05
${price}    1,000
${dictPromotion}
...    txt_price_grand_total=xpath=//div[@id='inf-viewPrice-grand_total']

*** Keywords ***
verify discount price on credit card BLL 5%
    [Arguments]    ${price}
    ${discount} =    Evaluate    ${price}*${discount_BLL_5%}
    ${result} =    Evaluate    ${price}-${discount}
    ${price_discount} =    Convert To Number    ${price_discount}
    Should Be Equal    ${result}    ${price_discount}

verify not discount price
    [Arguments]    ${price}
    CommonWebKeywords.Scroll To Element    ${dictPromotion}[txt_price_grand_total]
    BuiltIn.wait until keyword succeeds    3 x    15 sec    SeleniumLibrary.Wait Until Page Contains Element    ${dictPromotion}[txt_price_grand_total]
    ${price_grand_total}=    SeleniumLibrary.Get Element Attribute    ${dictPromotion}[txt_price_grand_total]    innerText
    ${price_grand_total}=    Remove String    ${price_grand_total}    ฿
    Should Be Equal    ${price_grand_total}    ${price}

*** Test Cases ***
To verify that delivery fee must not be applied with credit card on-top promotion
    [Tags]    promotion    jenkins_group1    regression    promotion    promotion_creditcard   testrailid:C85391    ruleid:574
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${promotions_creditcard_condition_username}    ${promotions_creditcard_condition_password}    ${promotions_creditcard_condition_information.firstname}
    Wait Until Page Is Completely Loaded
    common_cds_web.Test Setup - Remove all product in shopping cart    ${promotions_creditcard_condition_username}    ${promotions_creditcard_condition_password}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    my_cart_page.Click Secure Checkout Button
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Select Credit Card BBL VISA
    verify discount price on credit card BLL 5%    ${CDS11133773.price}

To verify that if user changes a payment method from "Credit Card" to other method such as COD
    [Tags]    promotion    jenkins_group1    regression    promotion    promotion_creditcard    testrailid:C85392    ruleid:574
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${promotions_creditcard_condition_username}    ${promotions_creditcard_condition_password}    ${promotions_creditcard_condition_information.firstname}
    Wait Until Page Is Completely Loaded
    common_cds_web.Test Setup - Remove all product in shopping cart    ${promotions_creditcard_condition_username}    ${promotions_creditcard_condition_password}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[7]
    my_cart_page.Click Secure Checkout Button
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Select Credit Card BBL VISA
    verify discount price on credit card BLL 5%    ${CDS11133773.price}
    checkout_page.Select Payment Method COD
    verify not discount price    ${price}

