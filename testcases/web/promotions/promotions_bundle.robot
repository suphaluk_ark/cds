*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    common_cds_web.Tear down with cancel order    AND    Close All Browsers

*** Variables ***
&{dict_bundle_product}    ${product_sku_list}[7]=1    ${product_sku_list}[30]=1
&{dict_mix_product}    ${product_sku_list}[4]=1    ${product_sku_list}[7]=1    ${product_sku_list}[30]=1
&{bundle_product_834}    ${product_sku_list}[31]=1    ${product_sku_list}[39]=1

*** Test Cases ***
Verify that able to get discount is correctly when buy product in set matching with conditions
    [Tags]    promotion    jenkins_group1    promotions    promotion_bundle    testrailid:C100228    ruleid:417
    Run Keyword And Ignore Error     Remove customer cart and generate new cart   ${cds_promotion_bundle_username}  ${cds_promotion_bundle_password}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success   ${cds_promotion_bundle_username}    ${cds_promotion_bundle_password}    ${cds_promotion_bundle_information.firstname}
    Search product and add multiple products to cart    ${dict_bundle_product}
    header_web_keywords.Header Web - Click View Cart Button
    Verify grand total displayed correctly     ${750}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method COD
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For COD Payment Method
    Wait Until Page Loader Is Not Visible
    Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page     ${750}

Verify that able to get the discount correctly when apply more than one promotion in a cart
    [Tags]    promotion    jenkins_group1    promotions    promotion_bundle    testrailid:C100229    ruleid:213
    Run Keyword And Ignore Error     Remove customer cart and generate new cart   ${cds_promotion_bundle_username}  ${cds_promotion_bundle_password}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success   ${cds_promotion_bundle_username}    ${cds_promotion_bundle_password}    ${cds_promotion_bundle_information.firstname}
    Search product and add multiple products to cart    ${dict_mix_product}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Apply coupon code     COUPON10
    Wait Until Page Loader Is Not Visible
    my_cart_page.Verify e-coupon discount in cart page(%)   COUPON10    ${${product_sku_list}[4]}[price]   ${10.0}
    Verify grand total displayed correctly     ${3270}

To verify that Subtotal with discount incl tax can be applied with 'Fixed price for product set'
    [Tags]    promotion    jenkins_group1    regression    promotion_bundle    testrailid:C144451    ruleid:834    ruleid:835
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${cds_promotion_bundle_username}    ${cds_promotion_bundle_password}    ${cds_promotion_bundle_information.firstname}
    common_cds_web.Search product and add multiple products to cart    ${bundle_product_834}
    header_web_keywords.Header Web - Click View Cart Button
    common_keywords.Wait Until Page Is Completely Loaded
    my_cart_page.Verify auto discount by fix price is displayed amount correctly    ${fix_discount.THB_1000}    ${${product_sku_list}[31]}[price]    ${qty_product.Q1}    ${${product_sku_list}[39]}[price]    ${qty_product.Q1}
    my_cart_page.Change quantity of product from the drop-down list    ${product_sku_list}[31]     ${qty_product.Q2}
    my_cart_page.Change quantity of product from the drop-down list    ${product_sku_list}[39]     ${qty_product.Q2}
    my_cart_page.Verify auto discount by fix price is displayed amount correctly    ${fix_discount.THB_4550}    ${${product_sku_list}[31]}[price]    ${qty_product.Q2}    ${${product_sku_list}[39]}[price]    ${qty_product.Q2}
    my_cart_page.Verify grand total price is display correctly after deduct discount from bundle promotion    ${fix_discount.THB_4550}    ${total_price}