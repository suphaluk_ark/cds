*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Variables ***
${login_username}    ${cds_promotion_coupon_username}
${login_password}    ${cds_promotion_coupon_password}
${login_display_name}    ${cds_promotion_coupon_information.firstname}
${coupon_id_826}    AU100
${coupon_id_841}    AUTKTC10

*** Test Cases ***
To verify that user apply promotion coupon code more than 1 per order
    [Tags]    promotion    jenkins_group1    regression    promotion_coupon    testrailid:C132756    ruleid:826
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${login_username}    ${login_password}    ${login_display_name}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[5]    ${qty_product.Q2} 
    my_cart_page.Apply coupon code     ${coupon_id_826}
    my_cart_page.Verify discount amount is displayed correctly after apply e-coupon    ${coupon_id_826}    ${fix_discount.THB_100}    ${${product_sku_list}[5]}[price]   ${qty_product.Q2} 
    my_cart_page.Verify grand total price is display correctly after deduct discount    ${fix_discount.THB_100}    ${${product_sku_list}[5]}[price]   ${qty_product.Q2} 
    my_cart_page.Verify button apply coupon is disabled after apply coupon code

Verify Promo code with specific Credit Card on Shopping Bag page
    [Tags]    promotion    jenkins_group1    regression    promotion_coupon    testrailid:C195028    ruleid:841    ruleid:843
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${login_username}    ${login_password}    ${login_display_name}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[5]    ${qty_product.Q2} 
    my_cart_page.Apply coupon code     ${coupon_id_841}
    my_cart_page.Verify that the specific card popup is displayed
    my_cart_page.Verify specific card coupon promotion is displayed on cart page correctly    ${coupon_id_841}    ${${product_sku_list}[5]}[price]    ${qty_product.Q2}    ${percent_discount.ten}
    my_cart_page.Verify credit card on top discount promotion is displayed on cart page correctly    ${total_price_after_deduct}    ${percent_discount.five}
    my_cart_page.Verify that coupon code can be removed and re-applied normally    ${coupon_id_841}    ${${product_sku_list}[5]}[price]    ${qty_product.Q2}    ${percent_discount.five}    ${percent_discount.ten}

Verify promo code with specific credit card on payment page when coupon applied
    [Tags]    promotion    jenkins_group1    regression    promotion_coupon    testrailid:195029    ruleid:841
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${login_username}    ${login_password}    ${login_display_name}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[5]    ${qty_product.Q2} 
    my_cart_page.Apply coupon code     ${coupon_id_841}
    my_cart_page.Verify that the specific card popup is displayed
    my_cart_page.Click Secure Checkout Button
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Verify T1 redeem section method should not visible
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Verify that only credit card KTC is displayed

Verify promo code with specific credit card on payment page when coupon is not applied
    [Tags]    promotion    jenkins_group1    regression    promotion_coupon    testrailid:195030    ruleid:841
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${login_username}    ${login_password}    ${login_display_name}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[5]    ${qty_product.Q2} 
    my_cart_page.Click Secure Checkout Button
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Verify T1 redeem section method should visible
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Verify that other credit cards are displayed

To verify that Subtotal with discount incl tax can be applied with promo coupon
    [Tags]    promotion    jenkins_group1    regression    promotion_coupon    testrailid:C144462    ruleid:826    ruleid:830
    [Setup]    common_cds_web.Login to CDS and remove product in shopping cart    ${login_username}    ${login_password}    ${login_display_name}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[5]    ${qty_product.Q4} 
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[20]    ${qty_product.Q2} 
    my_cart_page.Apply coupon code     ${coupon_id_826}
    my_cart_page.Verify other discount amount is display correct after apply promotion rule    ${fix_discount.THB_50}
    my_cart_page.Verify discount amount is displayed correctly after apply e-coupon    ${coupon_id_826}    ${fix_discount.THB_100}    ${${product_sku_list}[5]}[price]   ${qty_product.Q4} 
    my_cart_page.Verify grand total price is display correctly after deduct discount for multiple items    ${fix_discount.THB_150}    ${product_sku_list}[5]=${qty_product.Q4}    ${product_sku_list}[20]=${qty_product.Q2}
    my_cart_page.Verify button apply coupon is disabled after apply coupon code
    my_cart_page.Remove product from shopping bag    ${product_sku_list}[20]
    my_cart_page.Verify other discount should not display
    Verify grand total price is display correctly after deduct discount    ${fix_discount.THB_100}    ${${product_sku_list}[5]}[price]   ${qty_product.Q4}

