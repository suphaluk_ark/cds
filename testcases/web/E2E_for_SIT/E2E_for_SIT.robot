*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers
Suite Teardown    Close All Browsers

*** Test Cases ***
Order (Retail) product with cart price rule fixed Coupon, paid via Credit card with T1 redeem
    [Documentation]    - *Product source wms*           : CDS11133773 - CDS_SO_10138
    ...                - *Product source pickingtool*   : CDS10219928 - CDS_SO_10116
    ...                - *Auto coupon*                  : No
    ...                - *Apply coupon*                 : ${coupon.five_bath} - CDS: Rule 215 CDS11133773 - RBS: Rule 5087 RBS20265274
    ...                - *Delivery type*                : Standard delivery
    ...                - *Redeem point*                 : Yes
    ...                - *Payment type*                 : Credit card full payment
    ...                - *API*                          : MDC,MCOM,EBS,FMS,PickingTool,WMS
    [Tags]    e2e_flow    e2e_for_sit    testrailid:C995855    e2e_for_sit_01
    Run Keyword And Ignore Error    Remove customer cart and generate new cart    ${e2e_sit_username_01}    ${e2e_sit_password_01}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${e2e_sit_username_01}    ${e2e_sit_password_01}    ${e2e_sit_information_01.firstname}
    common_cds_web.Search product and add product to cart    ${CDS10219928.sku}
    common_cds_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    header_web_keywords.Header Web - Click View Cart Button
    my_cart_page.Verify total price with quantity, summarize multiple product in cart page    ${CDS10219928.sku}   ${cart_price_rule_sku.apply_coupon['5BAHT']}
    my_cart_page.Apply coupon code  ${coupon.five_bath}
    my_cart_page.Verify coupon code display in cart    ${coupon.five_bath}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${coupon.five_bath}    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}   ${5.0}
    my_cart_page.Click Secure Checkout Button
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}    ${t1c_information_4.email}   ${t1c_information_4.password}
    E2E_flow_keywords.Confirm payment by credit card
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${discount_amount}    ${5}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}
    common_cds_web.Retry verify that order create on WMS success    ${increment_id}
    kibana_api.Retry ESB update shipment status to shipped by sku    ${increment_id}    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    kibana_api.Retry ESB update shipment status to shipped by sku    ${increment_id}    ${CDS10219928.sku}    source=pickingtool
    kibana_api.Retry ESB update shipment status to delivered by sku    ${increment_id}    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    kibana_api.Retry ESB update shipment status to delivered by sku    ${increment_id}    ${CDS10219928.sku}    source=pickingtool
    common_cds_web.Verify order status from MCOM should be 'COMPLETE, FULLYSHIPPED'   ${increment_id}
    common_cds_web.Verify order status from MDC should be 'COMPLETE, FULLYSHIPPED'    ${increment_id}
    fms_db.Verify that order number exist in FMS DB    ${increment_id}