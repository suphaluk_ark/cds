*** Settings ***
Test Setup    common_cds_web.Setup - Open browser
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown
...    AND    Close Browser
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Force Tags    Product_PLP
*** Keywords ***
Template - Verify delivery option badge should be display correctly
    [Arguments]    ${pages_sections}
    Maximize Browser Window
    product_page.Go to the pages or sections on plp    ${pages_sections}
    product_page.Verify delivery option badge work correctly on plp    ${pages_sections}

Template - Verify delivery option badge of section should be display correctly
    [Arguments]    ${pages_sections}
    Maximize Browser Window
    product_page.Go to the pages or sections on plp    ${pages_sections}
    product_page.Verify delivery option badge of section display correctly on plp    ${pages_sections}

Template - Verify total number result of filter by delivery options should be display correctly
    [Arguments]    ${pages_sections}    ${option}
    Maximize Browser Window
    Go to the pages or sections on plp    ${pages_sections}
    Wait Until Page Is Completely Loaded
    Click available delivery option
    ${length_sku}    Click checkbox options to filter and get size of delivery method list    ${option}
    Click available delivery option
    Verify total number result displays correctly    ${length_sku}

Template - Verify filter by delivery options should be display and work correctly
    [Arguments]    ${pages_sections}    ${option}
    Maximize Browser Window
    Go to the pages or sections on plp    ${pages_sections}
    Wait Until Page Is Completely Loaded
    Click available delivery option
    ${sku_list}    Click checkbox options to filter and get list of delivery method    ${option}
    Verify The Product Are Filtered Correctly    ${sku_list}    ${option}

Template - Verify filter by delivery options must be mandatory and should be priority of display correctly
    [Arguments]    ${pages_sections}    ${option}
    Maximize Browser Window
    Go to the pages or sections on plp    ${pages_sections}
    Wait Until Page Is Completely Loaded
    Click sort drop down list
    ${qty_sort}    Sort by New Arrival and count the quantity be returned
    ${qty_available_delivery}    Sort by Available Delivery Options and count the quantity be returned    ${option}
    Verify filter by delivery options is priority    ${qty_sort}    ${qty_available_delivery}

*** Test Cases ***
Verify filter by delivery options must be mandatory and should be priority of display correctly
    [Tags]    jenkins_group1    testrailid:C795543
    [Template]    Template - Verify filter by delivery options must be mandatory and should be priority of display correctly
    ${plp_page_section_scope.product_brand}      ${filters.delivery_methods.click_and_collect}

Verify Out Of Stock Product Should Not Display In PLP
    [Tags]    jenkins_group1    product_PLP    testrailid:C282765    testrailid:C282766    testrailid:R13946
    product_page.Go to the pages or sections on plp    ${plp_page_section_scope.oos_product}
    ${product_url}    ${product_name}    common_keywords.Get Product Url And Name By Sku    ${product_oos_sku_list}[0]
    plp_page.Verify Product Name Should Not Display In Search Product    ${product_name}
    Go To Direct Url    ${product_url}
    Wait Until Page Is Completely Loaded
    product_PDP_page.Verify Add To Bag Button Should Not Display    ${product_oos_sku_list}[0]
    product_PDP_page.Verify Out Of Stock Message Should Display

Verify By-Order Product Should Not Display In PLP
    [Tags]    jenkins_group1    product_PLP    testrailid:R13946
    product_page.Go to the pages or sections on plp    ${plp_page_section_scope.byorder_product}
    ${product_url}    ${product_name}    common_keywords.Get Product Url And Name By Sku    ${product_byorder_sku}[0]
    plp_page.Verify Product Name Should Not Display In Search Product    ${product_name}
    Go To Direct Url    ${product_url}
    Wait Until Page Is Completely Loaded
    product_PDP_page.Verify Add To Bag Button Should Not Display    ${product_byorder_sku}[0]
    product_PDP_page.Verify Out Of Stock Message Should Display

Verify Category Total Count In PLP
    [Tags]    jenkins_group1    product_PLP    testrailid:C421381
    ${url}=    Get Category Url Path By Category Id Via Api    ${category_id.level_4}
    Go To Direct Url    ${url}
    Wait Until Page Is Completely Loaded
    Verify Total Found Item Should Equal Total Count Item

Verify Delivery Option Badge Should Be Display Correctly
    [Tags]    jenkins_group1    testrailid:C793994    testrailid:C794000    testrailid:C794001    testrailid:C793995    testrailid:C794003    testrailid:C794004    testrailid:C794013    testrailid:C794049    testrailid:C967095    testrailid:C794051
    ...    testrailid:C794052    testrailid:C794053    testrailid:C794055    testrailid:C795454    testrailid:C795477    testrailid:C878026    testrailid:C878027    testrailid:C795476    Omni_PLP        testrailid:C1014458    testrailid:C1014461
    [Template]    Template - Verify delivery option badge should be display correctly
    ${plp_page_section_scope.catalog_plp}
    ${plp_page_section_scope.search_plp}
    ${plp_page_section_scope.brand_plp}
    ${plp_page_section_scope.product_brand}
    ${plp_page_section_scope.brand_collection}
    ${plp_page_section_scope.simple_product}
    ${plp_page_section_scope.configurable_product}

Verify Delivery Option Badge Should Be Display Correctly For Specific Cases
    [Tags]    jenkins_group1    testrailid:C794059    testrailid:C794058    testrailid:C794057    testrailid:C794056    testrailid:C827563    testrailid:C827566    testrailid:C827567    testrailid:C827568    Omni_PLP
    ...    testrailid:C1010885    testrailid:C1010886    testrailid:C1010887    testrailid:C1010888    testrailid:C1014458    testrailid:C1014461
    [Template]    Template - Verify delivery option badge of section should be display correctly
    ${plp_page_section_scope.hot_deals}
    ${plp_page_section_scope.new_arrival}
    ${plp_page_section_scope.best_sellers}
    ${plp_page_section_scope.flash_deal}
    ${plp_page_section_scope.recently_viewed}
    # ${plp_page_section_scope.you_may_like}
    # ${plp_page_section_scope.similar_product}
    # ${plp_page_section_scope.complete_the_look}

Verify filter by delivery options should be display and work correctly
    [Tags]    jenkins_group1    testrailid:C795526    testrailid:C795528    testrailid:C795529    testrailid:C967097    testrailid:C795533
    ...    testrailid:C795532    testrailid:C795531    testrailid:C795530    testrailid:C795527    testrailid:C967096
    ...    testrailid:C795534    testrailid:C795535    testrailid:C795536    testrailid:C795537    testrailid:C795541    testrailid:C795542    testrailid:C795544    testrailid:C878033    testrailid:C878034    testrailid:C795543    testrailid:C795792    
    ...     Omni_PLP    2020
    [Template]    Template - Verify filter by delivery options should be display and work correctly
    ${plp_page_section_scope.product_brand}      ${filters.delivery_methods.click_and_collect}
    ${plp_page_section_scope.product_brand}      ${filters.delivery_methods.delivery}
    ${plp_page_section_scope.product_brand}      ${filters.delivery_methods.two_hours_pick_up}

Verify total number result of filter by delivery options should be display correctly
    [Tags]    jenkins_group1    testrailid:C795544    Omni_PLP
    [Template]    Template - Verify total number result of filter by delivery options should be display correctly
    ${plp_page_section_scope.product_brand}      ${filters.delivery_methods.click_and_collect}
    ${plp_page_section_scope.product_brand}      ${filters.delivery_methods.delivery}
    ${plp_page_section_scope.product_brand}      ${filters.delivery_methods.two_hours_pick_up}

Verify clear button of filter by delivery options should be work correctly
    [Tags]    jenkins_group1    Omni_PLP
    Maximize Browser Window
    Go to the pages or sections on plp    ${plp_page_section_scope.product_brand}
    Wait Until Page Is Completely Loaded
    ${after_clear_qty}    Click checkbox options to filter and get the quantity of selected checkboxes
    Verify The Quantity Checkbox Displayed Correctly    ${after_clear_qty}