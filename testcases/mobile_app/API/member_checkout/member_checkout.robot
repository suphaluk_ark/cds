*** Settings ***
Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

*** Variables ***
${store}    ${store_view.name.en}
${api_version}=    ${api_ver3}
${installment_plan_id}    21
${store_location_id}    392    #Automate - Test Store Information
${api_non_split_oder_checkout_username}    ${personal_api_app.non_split_order_member_checkout.email}
${api_non_split_oder_checkout_pwd}    ${personal_api_app.non_split_order_member_checkout.pwd}
${encrypted_card_data}    ${credit_card_api.card_4546230000000006.encrypted_card_data}
${card_last4}    ${credit_card_api.card_4546230000000006.card_last4}

*** Test Cases ***
Member Checkout With COD By Standard Shipping
    [Documentation]    Shipping method: Standard Delivery 
    ...                Payment method: COD
    [Tags]    testrailid:C1013435    checkout_api
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${api_non_split_oder_checkout_username}    ${api_non_split_oder_checkout_pwd}    ${CDS10328903.sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.cod}    ${store}
    ${entity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.cod}    ${store}
    ${order_detail_response}=    order_details.Get order details by entity_id    ${entity_id}
    ${increment_id}=    order_details.Get mdc_increment_id by order detail response    ${order_detail_response}
    order_status_api.Retry until MCOM order status should be 'LOGISTICS'    ${increment_id}
    order_status_api.Retry until MCOM order status reason should be 'READYTOSHIP'    ${increment_id}
    order_status_api.Retry until MDC order status should be 'Processing'    ${increment_id}
    order_status_api.Retry until MDC status reason should be 'READYTOSHIP'    ${increment_id}

Member Checkout With Bank Transfer By Standard Shipping
    [Tags]    testrailid:C1013436    checkout_api
    [Documentation]    Shipping method: Standard Delivery
            ...        Payment method: Bank Transfer
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${api_non_split_oder_checkout_username}    ${api_non_split_oder_checkout_pwd}    ${CDS10328903.sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.bank_transfer}    ${store}
    ${entity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.bank_transfer}    ${store}
    ${order_detail_response}=    order_details.Get order details by entity_id    ${entity_id}
    ${increment_id}=    order_details.Get mdc_increment_id by order detail response    ${order_detail_response}
    order_status_api.Retry until MCOM order status should be 'ONHOLD'    ${increment_id}
    order_status_api.Retry until MCOM order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}
    order_status_api.Retry until MDC order status should be 'awaiting_payment'    ${increment_id}
    order_status_api.Retry until MDC status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}

Member Checkout With Installment Via Card Info By Standard Shipping
    [Tags]    testrailid:C1013437    checkout_api
    [Documentation]    Shipping method: Standard Delivery
                ...    Payment method: Installment
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${api_non_split_oder_checkout_username}    ${api_non_split_oder_checkout_pwd}    ${CDS10311165.sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.pay_by_installment}    ${store}
    ${entity_id}=    checkout.Member select installment method without OTP, and create a new order    ${user_token}    ${store}    ${installment_plan_id}    ${encrypted_card_data}    ${card_last4}
    ${customerId}=    customer_api.Get customer id from api response    ${user_token}
    common_api_keywords.Call payment service api order paid full payment no saved card for app    ${entity_id}    ${customerId}    ${encrypted_card_data}
    ${order_detail_response}=    order_details.Get order details by entity_id    ${entity_id}
    ${increment_id}=    order_details.Get mdc_increment_id by order detail response    ${order_detail_response}
    order_status_api.Retry until MCOM order status should be 'LOGISTICS'    ${increment_id}
    order_status_api.Retry until MCOM order status reason should be 'READYTOSHIP'    ${increment_id}
    order_status_api.Retry until MDC order status should be 'Processing'    ${increment_id}
    order_status_api.Retry until MDC status reason should be 'READYTOSHIP'    ${increment_id}

Member Checkout With Full Payment Via Card Info By Standard Shipping
    [Tags]    testrailid:C1013438    checkout_api
    [Documentation]    Shipping method: Standard Delivery
            ...        Payment method: Credit card (Full Payment)
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${api_non_split_oder_checkout_username}    ${api_non_split_oder_checkout_pwd}    ${CDS10328903.sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.full_payment}    ${store}
    ${entity_id}=    checkout.Member select credit card full payment method without OTP, and create a new order    ${user_token}    ${store}    ${encrypted_card_data}    ${card_last4}
    ${customerId}=    customer_api.Get customer id from api response    ${user_token}
    common_api_keywords.Call payment service api order paid full payment no saved card for app    ${entity_id}    ${customerId}    ${encrypted_card_data}
    ${order_detail_response}=    order_details.Get order details by entity_id    ${entity_id}
    ${increment_id}=    order_details.Get mdc_increment_id by order detail response    ${order_detail_response}
    order_status_api.Retry until MCOM order status should be 'LOGISTICS'    ${increment_id}
    order_status_api.Retry until MCOM order status reason should be 'READYTOSHIP'    ${increment_id}
    order_status_api.Retry until MDC order status should be 'Processing'    ${increment_id}
    order_status_api.Retry until MDC status reason should be 'READYTOSHIP'    ${increment_id}

Member Checkout With Pay At Store By Click And Collect
    [Tags]    testrailid:C1013439    checkout_api
    [Documentation]    Shipping method: Click & Collect - Pick at store
            ...        Store: Automate - Test Store Information    (store_location_id: 392)
            ...        Payment method: Pay at store
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${api_non_split_oder_checkout_username}    ${api_non_split_oder_checkout_pwd}    ${CDS10328903.sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${store_location_id}    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.pay_at_store}    ${store}
    ${entity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.pay_at_store}    ${store}
    ${order_detail_response}=    order_details.Get order details by entity_id    ${entity_id}
    ${increment_id}=    order_details.Get mdc_increment_id by order detail response    ${order_detail_response}
    order_status_api.Retry until MCOM order status should be 'LOGISTICS'    ${increment_id}
    order_status_api.Retry until MCOM order status reason should be 'READYTOSHIP'    ${increment_id}
    order_status_api.Retry until MDC order status should be 'Processing'    ${increment_id}
    order_status_api.Retry until MDC status reason should be 'READYTOSHIP'    ${increment_id}

Member Checkout With Full Payment Via Card Info By 2 Hour Pickup
    [Tags]    testrailid:C1013440    checkout_api
    [Documentation]    Shipping method: Click & Collect - 2 Hour pickup
            ...        Store: Automate - Test Store Information    (store_location_id: 392)
            ...        Source stock: CDS_SO_10116
            ...        Payment method: Credit card (Full Payment)
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${api_non_split_oder_checkout_username}    ${api_non_split_oder_checkout_pwd}    ${CDS10328903.sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${store}    ${api_version}
    checkout.Member update shipping information for 2 hour pickup    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${store_location_id}    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.full_payment}    ${store}
    ${entity_id}=    checkout.Member select credit card full payment method without OTP, and create a new order    ${user_token}    ${store}    ${encrypted_card_data}    ${card_last4}
    ${customerId}=    customer_api.Get customer id from api response    ${user_token}
    common_api_keywords.Call payment service api order paid full payment no saved card for app    ${entity_id}    ${customerId}    ${encrypted_card_data}
    ${order_detail_response}=    order_details.Get order details by entity_id    ${entity_id}
    ${increment_id}=    order_details.Get mdc_increment_id by order detail response    ${order_detail_response}
    order_status_api.Retry until MCOM order status should be 'LOGISTICS'    ${increment_id}
    order_status_api.Retry until MCOM order status reason should be 'READYTOSHIP'    ${increment_id}
    order_status_api.Retry until MDC order status should be 'Processing'    ${increment_id}
    order_status_api.Retry until MDC status reason should be 'READYTOSHIP'    ${increment_id}