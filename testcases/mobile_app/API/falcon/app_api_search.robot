*** Settings ***
Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    search_api_app

*** Test Cases ***
Verify search result when searching by keyword
    [Tags]    high    testrailid:C1013450
    Set Test Variable    ${test_keyword}    ${search_by_keyword.app_auto}
    ${response}=    app_search_falcon.Api Search Product By Keyword    ${test_keyword}    ${20}    ${common_language.en}
    ${list_of_product}=    JSONLibrary.Get Value From Json    ${response}    $..products[*]
    Should Not Be Empty    ${list_of_product}

    FOR    ${dict_product}    IN    @{list_of_product}
        ${name}=    Set Variable    ${dict_product}[name]
        ${is_contains_name}=    Run Keyword And Return Status    Should Contain    ${name.upper()}    ${test_keyword.upper()}
        Continue For Loop If    ${is_contains_name}==${True}
        
        ${list_brand_product}=    JSONLibrary.Get Value From Json    ${dict_product}    $..brand.name
        ${is_list_brand_product_lower}=    Run Keyword And Return Status    List Should Contain Value    ${list_brand_product}   ${test_keyword.lower()}
        Continue For Loop If    ${is_list_brand_product_lower}==${True}

        ${sku}=    Set Variable    ${dict_product}[sku]
        ${is_contains_sku}=    Run Keyword And Return Status    Should Contain    ${sku.upper()}    ${test_keyword.upper()}
        Continue For Loop If    ${is_contains_sku}==${True}

        ${list_product_categories}=    JSONLibrary.Get Value From Json    ${dict_product}    $..breadcrumbs[*].title
        ${is_contains_categories_lower}=    Run Keyword And Return Status    List Should Contain Value    ${list_product_categories}   ${test_keyword.lower()}
        Continue For Loop If    ${is_contains_categories_lower}==${True}

        ${is_contains_categories_tittle}=    Run Keyword And Return Status    List Should Contain Value    ${list_product_categories}   ${test_keyword.title()}
        Continue For Loop If    ${is_contains_categories_tittle}==${True}   

        Run Keyword And Continue On Failure    List Should Contain Value    ${list_product_categories}   ${test_keyword}
    END

Verify search result when searching by brand name
    [Tags]    high    testrailid:C1013451
    Set Test Variable    ${test_brand_name}   ${search_by_keyword.adidas}
    app_search_falcon.Api Search Product By Brand Name    ${test_brand_name}    ${20}
    Run Keyword And Continue On Failure    REST.String    $..products[*].brand.name    ${test_brand_name.upper()}

Verify search result when searching by sku
    [Tags]    high    testrailid:C1013452
    Set Test Variable    ${test_keyword}    ${search_by_keyword.sku}
    app_search_falcon.Api Search Product By Keyword   ${test_keyword}    ${1}    ${common_language.en}
    Run Keyword And Continue On Failure    REST.String    $..products[*].sku    ${test_keyword.upper()}