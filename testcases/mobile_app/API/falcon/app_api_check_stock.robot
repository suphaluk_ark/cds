*** Settings ***
Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    app_api_check_stock

*** Test Cases ***
Verify that falcon API returns product quantity - shopping cart page
    [Tags]    high    testrailid:C1028999
    Set Test Variable    ${product_sku}    CDS9366114
    app_shoppinp_cart_falcon.API check stock on cart page    ${product_sku}
    Run Keyword And Continue On Failure    REST.Integer    $..products[*].inventoryStock.quantity
    Run Keyword And Continue On Failure    REST.String    $..products[*].inventoryStock.sku    ${product_sku}

Verify that falcon API returns product quantity - payment page
    [Tags]    high    testrailid:C1030023
    Set Test Variable    ${product_sku}    CDS9366114
    app_payment_page_falcon.API check stock on payment page    ${product_sku}
    Run Keyword And Continue On Failure    REST.Integer    $..v2InventoryStockBySkus[*].quantity
    Run Keyword And Continue On Failure    REST.String    $..v2InventoryStockBySkus[*].sku    ${product_sku}