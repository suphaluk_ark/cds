*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    pdp_wishlist

*** Test Cases ***
Verify that member can add and remove wishlist item on PDP page
    [Tags]    testrailid:C1013343    testrailid:C1013345    medium
    [Setup]    common_mobile_app_keywords.Remove wishlist User Profile  ${personal_ios_app.my_wishlist}
    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership   ${personal_ios_app.my_wishlist}
    wishlist_ios_keywords.Add wishlist item on pdp page
    wishlist_ios_keywords.Remove wishlist item on pdp page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that guest can add and remove wishlist item on PDP page
    [Tags]    testrailid:C1013344    testrailid:C1013346    medium
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest 
    wishlist_ios_keywords.Add wishlist item on pdp page
    wishlist_ios_keywords.Remove wishlist item on pdp page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application