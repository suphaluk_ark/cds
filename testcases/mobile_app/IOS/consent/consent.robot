*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    consent

*** Test Cases ***
Verify that the PDPA consent and marketing consent text should be displayed all the time for Guest registration page
    [Tags]    medium    testrailid:C1027604    testrailid:C1027597
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    consent_ios_keywords.Verify consent marketing and PDPA consent text should be displayed in registration page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that the PDPA consent and marketing text should be displayed all the time for Guest checkout Step 1
    [Tags]    medium    testrailid:C1027598    testrailid:C1027599
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    checkout_ios_keywords.Search product by product sku, add to cart, checkout  ${CDS15972262.sku}
    consent_ios_keywords.Verify consent marketing and PDPA consent text should be displayed in checkout step 1 for guest
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that the PDPA consent and marketing consent text should be displayed on checkout 1 for member when 'consent privacy and marketing status' equal to null
    [Tags]    medium    testrailid:C1027602     testrailid:C1027601
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership   ${personal.membership_ios_null_consent}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout  ${CDS15972262.sku}
    consent_ios_keywords.Verify consent marketing consent text should be displayed    ${personal.membership_ios_null_consent.email}    ${personal.membership_ios_null_consent.pwd}
    consent_ios_keywords.Verify PDPA consent text should be displayed in checkout step 1 for member when 'consent_privacy_status' equal to null    ${personal.membership_ios_null_consent.email}    ${personal.membership_ios_null_consent.pwd}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application
    
Verify that the PDPA consent and marketing consent text should not be displayed on checkout 1 for member when 'consent privacy and marketing status' equal to true
    [Tags]    medium    testrailid:C1027603    testrailid:C1027600
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership   ${personal.membership_ios_true_consent} 
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS15972262.sku}
    consent_ios_keywords.Verify consent marketing consent text should not be displayed    ${personal.membership_ios_true_consent.email}    ${personal.membership_ios_true_consent.pwd}
    consent_ios_keywords.Verify PDPA consent text should not be displayed in checkout step 1 for member when 'consent_privacy_status' equal to true    ${personal.membership_ios_true_consent.email}    ${personal.membership_ios_true_consent.pwd}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application