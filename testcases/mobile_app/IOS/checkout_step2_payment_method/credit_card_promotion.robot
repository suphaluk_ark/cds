*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

*** Test Cases ***
Verify credit card coupon promotion is applied correctly if the default card is matched with the promotion
    [Tags]    testrailid:C1014579    regression    credit_card_promotion    high
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal.coupon_credit_card_ios}
    promotion_ios_keywords.Add product to cart and apply coupon code    ${CDS10395578.sku}    ${promotions.rule_915.coupon_code}
    promotion_ios_keywords.Verify order total on shopping cart page after get discount    ${CDS10395578.price}    ${1}    discount_amount_1=${promotions.rule_915.discount_amount}    discount_amount_2=${promotions.rule_1003.discount_amount}
    promotion_ios_keywords.Member, Go to payment checkout page from shopping cart page with selecting pickup at store
    promotion_ios_keywords.Verify T1 secction should not visible when credit card coupon appied in cart
    promotion_ios_keywords.Verify order total on checkout payment page after get discount    ${CDS10395578.price}    ${1}    discount_amount_1=${promotions.rule_915.discount_amount}    discount_amount_2=${promotions.rule_1003.discount_amount}
    promotion_ios_keywords.Checkout complete with saved credit card
    [Teardown]    Teardown CDS Application

Verify credit card coupon promotion is applied correctly if guest input card is matched with the promotion
    [Tags]    testrailid:C1014580    regression    credit_card_promotion    low
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    promotion_ios_keywords.Add product to cart and apply coupon code    ${CDS10395578.sku}    ${promotions.rule_915.coupon_code}
    promotion_ios_keywords.Verify order total on shopping cart page after get discount    ${CDS10395578.price}    ${1}    discount_amount_1=${promotions.rule_915.discount_amount}    discount_amount_2=${promotions.rule_1003.discount_amount}
    promotion_ios_keywords.Guest, Go to payment checkout page from shopping cart page with selecting pickup at store
    promotion_ios_keywords.Verify T1 secction should not visible when credit card coupon appied in cart
    promotion_ios_keywords.Verify order total on checkout payment page after get discount    ${CDS10395578.price}    ${1}    discount_amount_1=${promotions.rule_915.discount_amount}    discount_amount_2=${promotions.rule_1003.discount_amount}
    promotion_ios_keywords.Checkout complete with entering credit card without OTP
    [Teardown]    Teardown CDS Application