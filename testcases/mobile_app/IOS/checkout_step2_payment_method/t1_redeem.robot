*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

*** Test Cases ***
Verify that customer can select "Full redeem" and apply T1 point redeem successfully
    [Tags]    testrailid:C1014584    regression    T1_redeem    high
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    checkout_ios_keywords.Search product by product sku, add to cart, checkout  ${CDS15972262.sku}
    delivery_ios_keywords.Guest, delivery successfully by standard pickup, self pickup
    payment_ios_keywords.Full Redeem T1 points
    ${order_number}    checkout_completed_common_page.Get non-split order number
    common_mobile_app_keywords.Retry verify that order status from MCOM should be 'LOGISTIC' status    ${order_number}
    common_mobile_app_keywords.Retry verify that order status reason from MCOM should be 'Ready to ship'    ${order_number}
    common_mobile_app_keywords.Retry verify that order status from MDC should be 'LOGISTIC' status    ${order_number}
    common_mobile_app_keywords.Retry verify that order status reason from MDC should be 'PENDING_FIRST_SHIPMENT_REQUEST'    ${order_number} 
    common_mobile_app_keywords.Retry verify that order number exist in FMS DB       ${order_number}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that customer can select "Redeem for discount" and apply T1 point redeem successfully
    [Tags]    testrailid:C1014585    regression    T1_redeem    high
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership   ${personal_ios_app.t1_redeem}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout  ${CDS15972262.sku}
    delivery_ios_keywords.Membership, delivery successfully by standard pickup, self pickup
    ${price_after_redeem}    payment_ios_keywords.Partially Redeem T1 points    ${40}
    payment_ios_keywords.Payment successfully with saved credit card
    ${order_number}    checkout_ios_keywords.Checkout completed with verifying price    ${price_after_redeem}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application