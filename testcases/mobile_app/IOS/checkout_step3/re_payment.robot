*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    repayment

*** Test Cases ***
Verify re-payment successfully with credit card
    [Tags]    high    testrailid:C1027690
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    checkout_ios_keywords.Search product by product sku, add to cart, checkout  ${CDS3684511.sku}
    delivery_ios_keywords.Guest, delivery successfully by shipping to address, standard delivery    ${post_code.bkk}
    payment_ios_keywords.Payment fail via credit card with invalid OTP
    payment_ios_keywords.Repayment successfully via credit card with OTP
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application
