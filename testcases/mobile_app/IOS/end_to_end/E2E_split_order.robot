*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot
Resource    ${CURDIR}/../../../../keywords/web/web_imports.robot

Force Tags    E2E_split_order

*** Test Cases ***
Verify the split order create successfully on Pickingtool, WMS ,and FMS
    [Documentation]    - *Member Type*              : Member
    ...                - *Payment Type*             : Credit card full payment
    ...                - *Delivery Type*            : 1hr pickup + standard shipping
    ...                - *Product from source*      : Pickingtool - CDS12863273 + WMS - CDS9465749
    [Tags]    high    testrailid:C1013369
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.e2e_split_order}
    checkout_ios_keywords.Search product by product sku, add to cart and go back to homepage    ${CDS12863273.sku}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS9465749.sku}
    delivery_ios_keywords.Membership delivery successfully 1hr and standard delivery
    payment_ios_keywords.Payment successfully with saved credit card    process_otp=yes
    checkout_completed_common_page.Verify that 'Your order is successful' should be visible
    ${1hr_pickup_order}=    checkout_completed_ios_page.Get order number that supports '1 Hour Pickup'
    ${std_delivery_order}=    checkout_completed_ios_page.Get order number that supports 'Standard Delivery'
    common_mobile_app_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${1hr_pickup_order}
    common_mobile_app_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${std_delivery_order}
    common_mobile_app_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${1hr_pickup_order}
    common_mobile_app_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${std_delivery_order}
    common_mobile_app_keywords.Retry verify that order number exist in FMS DB    ${1hr_pickup_order}
    common_mobile_app_keywords.Retry verify that order number exist in FMS DB    ${std_delivery_order}
    common_mobile_app_keywords.Verify order is created successfully in Pickingtool via ESB     ${1hr_pickup_order}
    common_mobile_app_keywords.Verify order is created successfully in WMS via ESB    ${std_delivery_order}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application