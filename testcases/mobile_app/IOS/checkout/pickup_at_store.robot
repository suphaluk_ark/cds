*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    pickup_at_store

*** Test Cases ***
Guest checkout successfully by standard pickup + pay at store
    [Documentation]   _*Member Type*_   : Guest
    ...
    ...               _*Payment Type*_  : Pay at store
    ...
    ...               _*Delivery Type*_ : Pickup at store, Standard
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    high    testrailid:C1026932    testrailid:C1027137
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS11140771.sku}
    delivery_ios_keywords.Guest, delivery successfully by standard pickup, self pickup
    payment_ios_keywords.Payment successfully by pay at store
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Member checkout successfully by 1HR pickup + credit card installment
    [Documentation]   _*Member Type*_   : Member
    ...
    ...               _*Payment Type*_  : Installment
    ...
    ...               _*Delivery Type*_ : Pickup at store, 1HR Pickup
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    high    testrailid:C1026933    testrailid:C1027133
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.smoke_installment}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS9946675.sku}
    delivery_ios_keywords.Membership, delivery successfully by 1 hour pickup, self pickup
    payment_ios_keywords.Payment successfully by installment with OTP
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application