*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    home_delivery

*** Test Cases ***
Verify guest checkout successfully by standard delivery + 123 bank transfer
    [Documentation]   _*Member Type*_   : Guest
    ...
    ...               _*Payment Type*_  : Bank transfer
    ...
    ...               _*Delivery Type*_ : Ship to home, standard delivery
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    high    testrailid:C1026930    testrailid:C1027143
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${product_sku_list}[51]
    delivery_ios_keywords.Guest, delivery successfully by shipping to address, standard delivery    ${post_code.cnx}
    payment_ios_keywords.Payment by bank transfer
    checkout_completed_common_page.Verify That 'Your Order Is Successful' Should Be Visible
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify member checkout successfully by express delivery + COD
    [Documentation]   _*Member Type*_   : Member
    ...
    ...               _*Payment Type*_ : COD
    ...
    ...               _*Delivery Type*_ : Next day delivery
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    high    testrailid:C1026931    testrailid:C1027144
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.smoke_cod}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS15187284.sku}
    delivery_ios_keywords.Membership, delivery successfully by shipping to address with the default address, next day delivery
    payment_ios_keywords.Payment successfully by cash on delivery
    checkout_completed_common_page.Verify That 'Your Order Is Successful' Should Be Visible
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application