*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    split_order

*** Test Cases ***
Member checkout successfully with split order by 1HR pickup + standard pickup
    [Documentation]   _*Member Type*_   : Member
    ...
    ...               _*Payment Type*_  : 123 Bank transfer
    ...
    ...               _*Delivery Type*_ : 1HR + Standard pickup
    ...
    ...               _*Split Order*_   : Split order
    [Tags]    high    testrailid:C1026934    testrailid:C1027135
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.smoke_123_bank_transfer.bangkok}
    checkout_ios_keywords.Search product by product sku, add to cart and go back to homepage    ${CDS11133773.sku}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS15187284.sku}
    delivery_ios_keywords.Membership, delivery sucessfully by 1hr and standard pickup
    payment_ios_keywords.Payment by bank transfer
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application
 
Guest checkout successfully with split order by 2HR pickup + standard delivery
    [Documentation]   _*Member Type*_   : Guest
    ...
    ...               _*Payment Type*_  : Credit card full payment
    ...
    ...               _*Delivery Type*_ : 1HR + Standard delivery
    ...
    ...               _*Split Order*_   : Split order
    [Tags]    high    testrailid:C1026935    testrailid:C1027132
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    checkout_ios_keywords.Search product by product sku, add to cart and go back to homepage    ${CDS11133773.sku}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS10232989.sku}
    delivery_ios_keywords.Guest, delivery sucessfully by 1hr and standard delivery    ${post_code.bkk}
    payment_ios_keywords.Payment successfully with credit card without OTP
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application