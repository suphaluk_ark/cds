*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    my_wishlist

*** Test Cases ***
Verify that member can add product to cart on wishlist page
    [Tags]    testrailid:C1013347    high
    [Setup]    common_mobile_app_keywords.Remove wishlist User Profile  ${personal_ios_app.my_wishlist}
    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership   ${personal_ios_app.my_wishlist}
    wishlist_ios_keywords.Add product to cart on wishlist page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that guest can add product to cart on wishlist page
    [Tags]    testrailid:C1013348   high
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest 
    wishlist_ios_keywords.Add product to cart on wishlist page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that member can remove wishlist item on wishlist page
    [Tags]    testrailid:C1013341   meduim
    [Setup]    common_mobile_app_keywords.Remove wishlist User Profile  ${personal_ios_app.my_wishlist}
    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership   ${personal_ios_app.my_wishlist}
    wishlist_ios_keywords.Remove wishlist item on wishlist page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that guest can remove wishlist item on wishlist page
    [Tags]    testrailid:C1013342   meduim
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest 
    wishlist_ios_keywords.Remove wishlist item on wishlist page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application