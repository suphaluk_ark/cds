*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    account_management

*** Test Cases ***
Verify that password change sucessfully
    [Tags]    testrailid:C1013352    medium
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.change_my_password}
    change_password_ios_keywords.Change password sucessfully    ${personal_ios_app.change_my_password.pwd}    ${personal_ios_app.change_my_password.new_pwd}    ${personal_ios_app.change_my_password.confirm_pwd}    ${personal_ios_app.change_my_password.first_name}    ${personal_ios_app.change_my_password.last_name}
    [Teardown]    Run Keywords    Run Keyword If Test Passed    customer_details.Reset Password By API    ${personal_ios_app.change_my_password.email}    ${personal_ios_app.change_my_password.new_pwd}    ${personal_ios_app.change_my_password.pwd}
    ...    AND    common_mobile_app_keywords.Teardown CDS Application

Verify customer profile displayed
    [Tags]    testrailid:C1013351    medium
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.my_profile}
    user_profile_ios_keywords.Verify customer profile displayed
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify credit card saved successfully with setup as default payment
    [Tags]    testrailid:C1013349    testrailid:C1013350    high
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.my_save_cards}
    my_save_cards_ios_keywords.Credit Card Saved Sucessfully With Setup As Default Payment
    my_save_cards_common_page.Delete saved credit card
    [Teardown]    Run Keywords    Run Keyword If Test Passed    my_save_cards_common_page.Delete saved credit card
    ...    AND    common_mobile_app_keywords.Teardown CDS Application 