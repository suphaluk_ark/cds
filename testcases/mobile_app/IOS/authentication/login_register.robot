*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    login_register

*** Test Cases ***
Verify login successfully with facebook
    [Tags]    testrailid:C1013360    high
    [Setup]    common_mobile_app_keywords.Launch CDS Application
    welcome_ios_keywords.Handle initial language, skip tutorial
    login_ios_keywords.Login successfully with facebook    ${personal_ios_app.login.facebook}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify register successfully by using email
    [Tags]    testrailid:C1013364    high
    [Setup]    common_mobile_app_keywords.Launch CDS Application
    register_ios_keywords.Delete registeration account from mdc
    welcome_ios_keywords.Handle initial language, skip tutorial
    register_ios_keywords.Register with all fields successfully
    register_ios_keywords.Delete registeration account from mdc
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application