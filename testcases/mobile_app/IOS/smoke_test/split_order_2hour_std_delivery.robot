*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    split_order_1hr_std_delivery

*** Test Cases ***
Verify guest checkout successfully with split order by 2HR pickup + standard delivery + credit card full payment
    [Documentation]    _*Payment*_ : complete
    ...
    ...               _*Member Type*_ : guest
    ...
    ...               _*Payment Type*_ : credit card
    ...
    ...               _*Delivery Type*_ : 1HR pickup + Standard Delivery
   [Tags]    testrailid:C1021142    smoke_test_app    smoke_credit_card_full_payment
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    checkout_ios_keywords.Search product by product sku, add to cart and go back to homepage    ${CDS11133773.sku}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS10232989.sku}
    delivery_ios_keywords.Guest, delivery sucessfully by 1hr and standard delivery    ${post_code.bkk}
    payment_ios_keywords.Payment successfully with credit card without OTP
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application