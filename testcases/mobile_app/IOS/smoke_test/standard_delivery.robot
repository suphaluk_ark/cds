*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    home_delivery_standard

*** Test Cases ***
Verify guest checkout successfully by standard delivery + 123 bank transfer
    [Documentation]   _*Member Type*_   : Guest
    ...
    ...               _*Payment Type*_  : Bank transfer
    ...
    ...               _*Delivery Type*_ : Ship to home, standard delivery
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    testrailid:C1021125    high
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${product_sku_list}[51]
    delivery_ios_keywords.Guest, delivery successfully by shipping to address, standard delivery    ${post_code.cnx}
    payment_ios_keywords.Payment by bank transfer
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application