*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    pick_at_store_1hr

*** Test Cases ***
Verify member checkout successfully by 1HR pickup + credit card installment
    [Documentation]   _*Member Type*_   : Member
    ...
    ...               _*Payment Type*_  : Installment
    ...
    ...               _*Delivery Type*_ : Pickup at store, 1HR Pickup
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    testrailid:C1021141    high
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.smoke_installment}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS9946675.sku}
    delivery_ios_keywords.Membership, delivery successfully by 1 hour pickup, self pickup
    payment_ios_keywords.Payment successfully by installment with OTP
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application