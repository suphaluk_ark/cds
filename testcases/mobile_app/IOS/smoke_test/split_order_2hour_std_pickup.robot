*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    split_order_1hr_std_pickup

*** Test Cases ***
Verify member checkout successfully with split order by 2HR pickup + standard pickup + 123 bank transfer
    [Documentation]    _*Payment*_ : complete
    ...
    ...               _*Member Type*_ : member
    ...
    ...               _*Payment Type*_ : bank transfer
    ...
    ...               _*Delivery Type*_ : 1HR pickup + Standard Pickup
    [Tags]    testrailid:C1021143    smoke_test_app    smoke_123_bank_transfer
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.smoke_123_bank_transfer.bangkok}
    checkout_ios_keywords.Search product by product sku, add to cart and go back to homepage    ${CDS11133773.sku}
    checkout_ios_keywords.Search product by product sku, add to cart, checkout    ${CDS15187284.sku}
    delivery_ios_keywords.Membership, delivery sucessfully by 1hr and standard pickup
    payment_ios_keywords.Payment by bank transfer
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application