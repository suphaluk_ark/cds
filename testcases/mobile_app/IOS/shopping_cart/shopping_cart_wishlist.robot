*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    shopping_cart_wishlist

*** Test Cases ***
Verify that member can add wishlist item on shopping cart page
    [Tags]    testrailid:C1013339    medium
    [Setup]    common_mobile_app_keywords.Remove wishlist User Profile  ${personal_ios_app.my_wishlist}
    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership   ${personal_ios_app.my_wishlist}
    wishlist_ios_keywords.Add wishlist item on cart page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that guest can add wishlist item on shopping cart page
    [Tags]    testrailid:C1013340    medium
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest 
    wishlist_ios_keywords.Add wishlist item on cart page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application