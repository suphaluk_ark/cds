*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    shopping_cart_promotion

*** Test Cases ***
Verify promotion item display on shopping cart page correctly
    [Tags]    testrailid:C1014612    high
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    product_pdp_common_page.Add product to shopping cart    ${product_sku_list}[54]
    shopping_cart_common_page.Verify promotion item details    ${product_sku_list}[55]
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify if the coupon code is applied to the cart successfully 
    [Tags]    testrailid:C1026927    high
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    product_pdp_common_page.Add product to shopping cart    ${CDS10395578.sku}
    shopping_cart_common_page.Input coupon code  ${promotions.rule_1023.coupon_code}
    shopping_cart_common_page.Click apply button
    shopping_cart_common_page.Verify coupon code displayed correctly    ${promotions.rule_1023.coupon_code}
    ${total_price}=    promotion_calculator.Calculate total price by summary product price with quantity    ${CDS10395578.price}    ${1}
    ${order_total_expected}=    promotion_calculator.Calculate order total with deduct discount    ${total_price}    discount_amount=${promotions.rule_1023.discount_amount}
    shopping_cart_common_page.Verify order total is displayed correctly    ${order_total_expected}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application
