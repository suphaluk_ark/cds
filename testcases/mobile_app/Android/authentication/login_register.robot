*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    login_register

*** Test Cases ***
Verify login successfully with facebook
    [Tags]    testrailid:C1013360    high
    [Setup]    common_mobile_app_keywords.Launch CDS Application
    welcome_android_keywords.Handle initial language, skip tutorial
    login_android_keywords.Login successfully with facebook    ${personal_android_app.login.facebook}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify register successfully by using email
    [Tags]    testrailid:C1013364    high
    [Setup]    common_mobile_app_keywords.Launch CDS Application
    register_android_keywords.Delete registeration account from mdc
    welcome_android_keywords.Handle initial language, skip tutorial
    register_android_keywords.Register with all fields successfully
    register_android_keywords.Delete registeration account from mdc
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application