*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    t1_redeem

*** Test Cases ***
Verify that customer can select "Full redeem" and apply T1 point redeem successfully
    [Tags]    high    testrailid:C1014584
    [Setup]    common_mobile_app_keywords.Open CDS Application
    welcome_android_keywords.Handle initial language, skip tutorial
    checkout_android_keywords.Search product by product sku, add to cart, checkout  ${CDS15972262.sku}
    delivery_android_keywords.Guest, delivery successfully by standard pickup, self pickup
    payment_android_keywords.Full Redeem T1 points
    ${order_number}    checkout_completed_common_page.Get non-split order number
    common_mobile_app_keywords.Retry verify that order status from MCOM should be 'LOGISTIC' status    ${order_number}
    common_mobile_app_keywords.Retry verify that order status reason from MCOM should be 'Ready to ship'    ${order_number}
    common_mobile_app_keywords.Retry verify that order status from MDC should be 'LOGISTIC' status    ${order_number}
    common_mobile_app_keywords.Retry verify that order status reason from MDC should be 'PENDING_FIRST_SHIPMENT_REQUEST'    ${order_number} 
    common_mobile_app_keywords.Retry verify that order number exist in FMS DB       ${order_number}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that customer can select "Partially redeem" and apply T1 point redeem successfully
    [Tags]    high    testrailid:C1014585
    [Setup]    android Setup CDS Application by Membership    ${personal_android_app.t1_redeem}
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${CDS15972262.sku}
    delivery_android_keywords.Membership, delivery successfully by standard pickup, self pickup
    ${price_after_redeem}    payment_android_keywords.Partially Redeem T1 points    ${40}
    payment_android_keywords.Payment successfully with saved credit card
    ${order_number}    checkout_android_keywords.Checkout completed with verifying price    ${price_after_redeem}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application