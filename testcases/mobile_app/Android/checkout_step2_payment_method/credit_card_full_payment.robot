*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

*** Test Cases ***
Verify that system will not allow customer to check out if customer does not complete the payment form (customer has no saved card)
    [Tags]    testrailid:C1014588    regression    credit_card_full_payment    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership   ${personal_android_app.credit_card_full_payment.no_save_credit_card}
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${product_sku_list}[51]
    delivery_android_keywords.Membership, delivery successfully by shipping to address with the default address, standard delivery
    payment_android_keywords.Payment by credit card unsuccessfully
    payment_android_keywords.Verify does not complete the payment form
    [Teardown]    Teardown CDS Application

Verify that system will allow customer to check out if customer complete the payment form (customer has no saved card)
    [Tags]    testrailid:C1014589    regression    credit_card_full_payment    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership   ${personal_android_app.credit_card_full_payment.no_save_credit_card}
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${product_sku_list}[51]
    delivery_android_keywords.Membership, delivery successfully by shipping to address with the default address, standard delivery
    payment_android_keywords.Payment successfully with credit card without OTP
    [Teardown]    Teardown CDS Application

Verify that system will not allow customer to check out if customer does not input CVV (customer has a saved card)
    [Tags]    testrailid:C1014590    regression    credit_card_full_payment    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership   ${personal_android_app.credit_card_full_payment.save_credit_card}
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${product_sku_list}[51]
    delivery_android_keywords.Membership, delivery successfully by shipping to address with the default address, standard delivery
    payment_android_keywords.Payment by credit card unsuccessfully
    payment_android_keywords.Verify does not complete cvv
    [Teardown]    Teardown CDS Application

Verify default saved card and promotion when the default card is expired
    [Tags]    testrailid:C1014581    testrailid:C1014582    regression    credit_card_full_payment    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership   ${personal_android_app.credit_card_full_payment.card_expired}
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${product_sku_list}[51]
    delivery_android_keywords.Membership, delivery successfully by shipping to address with the default address, standard delivery
    payment_android_keywords.Verify text credit card expired is displayed and promotion is inactive
    [Teardown]    Teardown CDS Application