*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    search_filter

*** Test Cases ***
Verify that the results on UI and API are correct when filtering by delivery options
    [Tags]    testrailid:C1014607    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest
    search_filter_android_keywords.Verify result of search API when filtering by delivery options      app_auto    ${shipping_method_api.one_hr}
    search_filter_android_keywords.Verify the total item on UI displays correctly according to API result    ${total_item}    app_auto    ${product_list_page_dto.filter.delivery_method}    ${product_list_page_dto.filter.delivery_options.one_hr}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application