*** Settings ***
Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application
*** Variables ***
${random_chacracter}   xsajrwqadhq5

*** Test Cases ***
Verify if there is no result, system displays "Sorry, we couldn't find anything to match your search"
    [Tags]    testrailid:C1014609    regression    search_product    medium
    [Setup]    common_android_mobile_app_keywords.Android Setup CDS Application By Guest
    welcome_android_keywords.Handle initial language, skip tutorial
    common_page.Input text into search box  ${random_chacracter}
    common_android_mobile_app_keywords.Click Search key on the virtual keyboard
    search_product_common_page.Verify message displays correctly at not found result page
    [Teardown]    Teardown CDS Application