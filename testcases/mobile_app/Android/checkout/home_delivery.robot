*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    home_delivery

*** Test Cases ***
Checkout successfully by standard delivery + 123 bank transfer
    [Documentation]   _*Member Type*_   : Guest
    ...
    ...               _*Payment Type*_  : Bank transfer
    ...
    ...               _*Delivery Type*_ : Ship to home, standard delivery
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    high    testrailid:C1026930    testrailid:C1027143
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest
    checkout_android_keywords.Search product by product sku, add to cart, checkout  ${product_sku_list}[51]
    delivery_android_keywords.Guest, delivery successfully by shipping to address bkk, standard delivery  ${post_code.bkk}
    payment_android_keywords.Payment by bank transfer
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify member checkout successfully by express delivery + COD
    [Documentation]   _*Member Type*_   : Member
    ...
    ...               _*Payment Type*_ : COD
    ...
    ...               _*Delivery Type*_ : Next day delivery
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    high    testrailid:C1026931    testrailid:C1027144
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership    ${personal_android_app.smoke_cod}
    checkout_android_keywords.Search product by product sku, add to cart, checkout  ${CDS15187284.sku}
    delivery_android_keywords.Membership, delivery successfully by shipping to address with the default address, next day delivery
    payment_android_keywords.Payment successfully by cash on delivery
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application