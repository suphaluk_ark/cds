*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    my_wishlist

*** Test Cases ***
Verify that member can add product to cart on wishlist page
    [Tags]    testrailid:C1013347   high
    [Setup]    common_mobile_app_keywords.Remove wishlist User Profile  ${personal_android_app.my_wishlist}
    common_android_mobile_app_keywords.android Setup CDS Application by Membership   ${personal_android_app.my_wishlist}
    wishlist_android_keywords.Add product to cart on wishlist page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that guest can add product to cart on wishlist page
    [Tags]    testrailid:C1013348   high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest 
    wishlist_android_keywords.Add product to cart on wishlist page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that member can remove wishlist item on wishlist page
    [Tags]    testrailid:C1013341   medium
    [Setup]    common_mobile_app_keywords.Remove wishlist User Profile  ${personal_android_app.my_wishlist}
    common_android_mobile_app_keywords.android Setup CDS Application by Membership   ${personal_android_app.my_wishlist}
    wishlist_android_keywords.Remove wishlist item on wishlist page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that guest can remove wishlist item on wishlist page
    [Tags]    testrailid:C1013342   medium
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest 
    wishlist_android_keywords.Remove wishlist item on wishlist page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application