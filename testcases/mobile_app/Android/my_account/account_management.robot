*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    account_management

*** Test Cases ***
Verify that password change sucessfully
    [Tags]    testrailid:C1013352    medium
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership    ${personal_android_app.change_my_password}
    change_password_android_keywords.Change password sucessfully    ${personal_android_app.change_my_password.pwd}    ${personal_android_app.change_my_password.new_pwd}    ${personal_android_app.change_my_password.confirm_pwd}
    [Teardown]    Run Keywords    Run Keyword If Test Passed     customer_details.Reset Password By API    ${personal_android_app.change_my_password.email}    ${personal_android_app.change_my_password.new_pwd}    ${personal_android_app.change_my_password.pwd}
    ...    AND    common_mobile_app_keywords.Teardown CDS Application

Verify customer profile displayed
    [Tags]    testrailid:C1013351    medium
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership    ${personal_android_app.my_profile}
    user_profile_android_keywords.Verify customer profile displayed
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify credit card saved successfully with setup as default payment
    [Tags]    testrailid:C1013349    testrailid:C1013350    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership    ${personal_android_app.my_save_cards}
    my_save_cards_android_keywords.Credit Card Saved Successfully With Setup As Default Payment
    my_save_cards_common_page.Delete saved credit card
    [Teardown]    Run Keywords    Run Keyword If Test Passed    my_save_cards_common_page.Delete saved credit card
    ...    AND    common_mobile_app_keywords.Teardown CDS Application