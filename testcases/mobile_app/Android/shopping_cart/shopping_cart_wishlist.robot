*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    shopping_cart_wishlist

*** Test Cases ***
Verify that member can add wishlist item on shopping cart page
    [Tags]    testrailid:C1013339    medium
    [Setup]    common_mobile_app_keywords.Remove wishlist User Profile  ${personal_android_app.my_wishlist}
    common_android_mobile_app_keywords.android Setup CDS Application by Membership   ${personal_android_app.my_wishlist}
    wishlist_android_keywords.Add wishlist item on cart page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application

Verify that guest can add wishlist item on shopping cart page
    [Tags]    testrailid:C1013340    medium
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest 
    wishlist_android_keywords.Add wishlist item on cart page
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application