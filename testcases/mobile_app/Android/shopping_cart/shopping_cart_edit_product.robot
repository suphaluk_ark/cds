*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    shopping_cart_edit_product

*** Test Cases ***
Verify that user can remove product on shopping cart page Member
    [Tags]    testrailid:C1027522     medium
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership       ${personal_android_app.shopping_cart}
    product_pdp_common_page.Add product to shopping cart    ${product_sku_list}[54]
    shopping_cart_common_page.Click remove item in cart
    shopping_cart_common_page.Click confirm remove
    shopping_cart_common_page.Verify item remove in cart
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application