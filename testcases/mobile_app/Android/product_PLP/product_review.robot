*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    product_review

*** Test Cases ***
Verify product has review and product has no review on PLP
    [Tags]    medium    testrailid:C1014605    testrailid:C1014606
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest  
    product_plp_common_page.Go to PLP of 'Men' category
    product_plp_common_page.Verify product without review rating    ${product_sku_list}[56]
    product_plp_common_page.Verify product with review rating    ${product_sku_list}[57]
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application