*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot
Resource    ${CURDIR}/../../../../keywords/web/web_imports.robot

Force Tags    E2E_standard_order

*** Test Cases ***
Verify the order create successfully on WMS when checkout by standard delivery
    [Documentation]    - *Member Type*              : Guest
    ...                - *Payment Type*             : COD
    ...                - *Delivery Type*            : Standard shipping
    ...                - *Product from source*      : WMS
    [Tags]    high    testrailid:C1019749
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${CDS9465749.sku}
    delivery_android_keywords.Guest, delivery successfully by shipping to address bkk, standard delivery  ${post_code.bkk}
    payment_android_keywords.Payment successfully by cash on delivery
    ${order_number}    checkout_completed_common_page.Get non-split order number
    common_mobile_app_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${order_number}
    common_mobile_app_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${order_number}
    common_mobile_app_keywords.Retry verify that order number exist in FMS DB       ${order_number}
    common_mobile_app_keywords.Verify order is created successfully in WMS via ESB    ${order_number}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application