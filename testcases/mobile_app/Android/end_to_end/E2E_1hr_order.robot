*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot
Resource    ${CURDIR}/../../../../keywords/web/web_imports.robot

Force Tags    E2E_1hr_order

*** Test Cases ***
Verify the order create successfully on Pickingtool when checkout by 2HR pickup
    [Documentation]    - *Member Type*              : Membership
    ...                - *Payment Type*             : Pay at store
    ...                - *Delivery Type*            : 2hr pickup
    ...                - *Product from source*      : Pickingtool
    [Tags]    high    testrailid:C1019748
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership    ${personal_android_app.e2e_2hr_order}
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${CDS12863273.sku}
    delivery_android_keywords.Membership delivery successfully by 1hr pickup
    payment_android_keywords.Payment successfully by pay at store
    ${order_number}    checkout_completed_common_page.Get non-split order number
    common_mobile_app_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${order_number}
    common_mobile_app_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${order_number}}
    common_mobile_app_keywords.Retry verify that order number exist in FMS DB       ${order_number}
    common_mobile_app_keywords.Verify order is created successfully in Pickingtool via ESB     ${order_number}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application