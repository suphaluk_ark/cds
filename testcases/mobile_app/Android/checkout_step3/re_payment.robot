*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    repayment

*** Test Cases ***
Verify re-payment successfully with credit card
    [Tags]    high    testrailid:C1027690
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest
    checkout_android_keywords.Search product by product sku, add to cart, checkout  ${CDS3684511.sku}
    delivery_android_keywords.Guest, delivery successfully by shipping to address bkk, standard delivery    ${post_code.bkk}
    payment_android_keywords.Payment fail via credit card with invalid OTP
    payment_android_keywords.Repayment successfully via credit card with OTP
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application
