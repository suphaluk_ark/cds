*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    split_order_1hr_std_delivery

*** Test Cases ***
Verify guest checkout successfully with split order by 2HR pickup + standard delivery + credit card full payment 
    [Documentation]   _*Member Type*_   : Guest
    ...
    ...               _*Payment Type*_  : Credit card full payment
    ...
    ...               _*Delivery Type*_ : Pickup at store, 2HR + Ship to home, standard delivery
    ...
    ...               _*Split Order*_   : Split order
    [Tags]    testrailid:C1021142    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest
    checkout_android_keywords.Search product by product sku, add to cart and go back to homepage    ${CDS11133773.sku}
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${CDS10232989.sku}
    delivery_android_keywords.Guest, delivery sucessfully by 1hr and standard delivery    ${post_code.bkk}
    payment_android_keywords.Payment successfully with credit card without OTP
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application
