*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    split_order_1hr_std_pickup

*** Test Cases ***
Verify member checkout successfully with split order by 2HR pickup + standard pickup + 123 Bank transfers
    [Documentation]   _*Member Type*_   : Member
    ...
    ...               _*Payment Type*_  : 123 Bank transfer
    ...
    ...               _*Delivery Type*_ : Pickup at store, 2HR + Standard pickup
    ...
    ...               _*Delivery Type*_ : 1hr pickup & standard pickup
    [Tags]    testrailid:C1021143    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership    ${personal_android_app.smoke_123_bank_transfer.bangkok}
    checkout_android_keywords.Search product by product sku, add to cart and go back to homepage    ${CDS11133773.sku}
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${CDS15187284.sku}
    delivery_android_keywords.Membership delivery successfully by 1hr pickup and standard pickup
    payment_android_keywords.Payment by bank transfer
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application