*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    pick_at_store_standard

*** Test Cases ***
Verify guest checkout successfully by standard pickup + pay at store
    [Documentation]   _*Member Type*_   : Guest
    ...
    ...               _*Payment Type*_  : Pay at store
    ...
    ...               _*Delivery Type*_ : Pickup at store, Standard
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    testrailid:C1021140    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest
    checkout_android_keywords.Search product by product sku, add to cart, checkout    ${CDS10765890.sku}
    delivery_android_keywords.Guest, delivery successfully by standard pickup, self pickup
    payment_android_keywords.Payment successfully by pay at store
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application