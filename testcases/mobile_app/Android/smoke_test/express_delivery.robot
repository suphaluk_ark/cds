*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../keywords/mobile_app/mobile_app_imports.robot

Force Tags    express_delivery

*** Test Cases ***
Verify member checkout successfully by express delivery + COD
    [Documentation]   _*Member Type*_   : Member
    ...
    ...               _*Payment Type*_ : COD
    ...
    ...               _*Delivery Type*_ : Next day delivery
    ...
    ...               _*Split Order*_   : Non-split
    [Tags]    testrailid:C1021126    high
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership    ${personal_android_app.smoke_cod}
    checkout_android_keywords.Search product by product sku, add to cart, checkout  ${CDS15187284.sku}
    delivery_android_keywords.Membership, delivery successfully by shipping to address with the default address, next day delivery
    payment_android_keywords.Payment successfully by cash on delivery
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application