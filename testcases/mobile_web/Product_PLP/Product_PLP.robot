*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Keywords ***
Template - Verify delivery option badge of section should be display correctly
    [Arguments]    ${pages_sections}
    Maximize Browser Window
    product_page.Go to the pages or sections on plp    ${pages_sections}
    product_page.Verify delivery option badge of section display correctly on plp    ${pages_sections}

*** Test Cases ***
Verify "home delivery" badge of simple product when enable delivery option toggle should be work correctly
    [Tags]    Omni_PLP    testrailid:C793994
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0]
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[0]

Verify "click & collect" badge of simple product when enable delivery option toggle should be work correctly 
    [Tags]    Omni_PLP    testrailid:C794000
    search_mobile_web_keyword.Search product for go to PLP page for mobile web    ${product_sku_list}[1] 
    PLP_page_mobile_web.Verify click & collect badge enable should be visible on flip card   ${product_sku_list}[1]

Verify "home delivery" badge of simple product when disable delivery option toggle should be work correctly 
    [Tags]    Omni_PLP    testrailid:C793995
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery disable should be visible on flip card    ${product_sku_list}[1]

Verify "click & collect" badge of simple product when disable delivery option toggle should be work correctly
    [Tags]    Omni_PLP    testrailid:C794003
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible on flip card    ${product_sku_list}[0]

Verify "3hour delivery" badge of simple product when enable delivery option toggle should be work correctly
    [Tags]    Omni_PLP    testrailid:C1014458
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]

Verify "3hour delivery" badge of simple product when disable delivery option toggle should be work correctly 
    [Tags]    Omni_PLP    testrailid:C1014461
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[1]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Category PLP - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C794049
    common_cds_mobile_web.Go to beauty category page
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Virtual Category PLP - Verify delivery option badge should be display correctly 
    [Tags]    Omni_PLP    testrailid:C794050    testrailid:C1014458    testrailid:C1014461
    common_cds_mobile_web.Go to virtual category page
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Search PLP - Verify delivery option badge should be display correctly 
    [Tags]    Omni_PLP    testrailid:C794051    testrailid:C1014458    testrailid:C1014461
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0], ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Brand PLP - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C794052    testrailid:C1014458    testrailid:C1014461
    common_cds_web.Go to shop by brand page
    shop_by_brand_page.Click on brand    ${product_brand.garmin}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[26]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible on flip card    ${product_sku_list}[62]
    PLP_page_mobile_web.Verify home delivery disable should be visible on flip card    ${product_sku_list}[62]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible on flip card    ${product_sku_list}[26]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[26]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[62]

Brand/Products - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C794053    testrailid:C1014458    testrailid:C1014461
    search_mobile_web_keyword.Search product for go to PLP page for mobile web    lancome
    PLP_page_mobile_web.Click shop all products    ${product_brand.lancome}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Full Boutique PLP - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C794054    testrailid:C1014458    testrailid:C1014461
    common_cds_web.Go to shop by brand page
    shop_by_brand_page.Click on brand    ${product_brand.sanrio}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery disable should be visible on flip card    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Brand Collection - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C794055    testrailid:C1014458    testrailid:C1014461
    common_cds_web.Go to shop by brand page
    shop_by_brand_page.Click on brand    ${product_brand.aveda}
    PLP_page_mobile_web.Scroll to text section    ${brand_collection_text.hair_care}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible under text section    ${brand_collection_text.hair_care}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible under text section    ${brand_collection_text.hair_care}    ${product_sku_list}[63]
    PLP_page_mobile_web.Verify home delivery badge disable should be visible under text section    ${brand_collection_text.hair_care}    ${product_sku_list}[63]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible under text section    ${brand_collection_text.hair_care}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[63]

PDP_Recently Viewed - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C794056    testrailid:C1014458    testrailid:C1014461
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0]
    PLP_page_mobile_web.Click product flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Scroll to text section    ${product_detail_page.preview_section}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible under text section    ${product_detail_page.preview_section}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible under text section    ${product_detail_page.preview_section}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery badge disable should be visible under text section    ${product_detail_page.preview_section}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible under text section    ${product_detail_page.preview_section}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

PDP_You May Like - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C794057    testrailid:C1014458    testrailid:C1014461
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0]
    PLP_page_mobile_web.Click product flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Scroll to text section    ${product_detail_page.you_may_like}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible under text section    ${product_detail_page.you_may_like}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible under text section    ${product_detail_page.you_may_like}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery badge disable should be visible under text section    ${product_detail_page.you_may_like}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible under text section    ${product_detail_page.you_may_like}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

PDP_Similar Product - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C794058    testrailid:C1014458    testrailid:C1014461
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0]
    PLP_page_mobile_web.Click product flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Scroll to text section    ${product_detail_page.similar_section}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible under text section    ${product_detail_page.similar_section}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible under text section    ${product_detail_page.similar_section}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery badge disable should be visible under text section    ${product_detail_page.similar_section}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible under text section    ${product_detail_page.similar_section}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

PDP_Complete the Look - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C794059    testrailid:C1014458    testrailid:C1014461
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0]
    PLP_page_mobile_web.Click product flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Scroll to text section    ${product_detail_page.complete_the_look}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible under text section    ${product_detail_page.complete_the_look}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible under text section    ${product_detail_page.complete_the_look}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery badge disable should be visible under text section    ${product_detail_page.complete_the_look}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible under text section    ${product_detail_page.complete_the_look}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Hot deal - Verify delivery option badge should be display correctly 
    [Tags]    Omni_PLP    testrailid:C827563    testrailid:C1014458    testrailid:C1014461
    PLP_page_mobile_web.Scroll to text section    ${home_page.section.hotdeal}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible under text section    ${home_page.section.hotdeal}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible under text section    ${home_page.section.hotdeal}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery badge disable should be visible under text section    ${home_page.section.hotdeal}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible under text section    ${home_page.section.hotdeal}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

New Arrival - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C827566    testrailid:C1014458    testrailid:C1014461
    PLP_page_mobile_web.Scroll to text section    ${home_page.section.new_arrival}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible under text section    ${home_page.section.new_arrival}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible under text section    ${home_page.section.new_arrival}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery badge disable should be visible under text section    ${home_page.section.new_arrival}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible under text section    ${home_page.section.new_arrival}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Best Seller - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C827567    testrailid:C1014458    testrailid:C1014461
    PLP_page_mobile_web.Scroll to text section    ${home_page.section.best_seller}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible under text section    ${home_page.section.best_seller}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible under text section    ${home_page.section.best_seller}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery badge disable should be visible under text section    ${home_page.section.best_seller}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible under text section    ${home_page.section.best_seller}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Flashdeal - Verify delivery option badge should be display correctly
    [Tags]    Omni_PLP    testrailid:C827568    testrailid:C1014458    testrailid:C1014461
    PLP_page_mobile_web.Scroll to text section    ${text_name_section.flash_deal}
    PLP_page_mobile_web.Verify home delivery badge enable should be visible under text section    ${text_name_section.flash_deal}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible under text section    ${text_name_section.flash_deal}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify home delivery badge disable should be visible under text section    ${text_name_section.flash_deal}    ${product_sku_list}[1]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible under text section    ${text_name_section.flash_deal}    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify 3hour delivery badge disable should be visible on flip card    ${product_sku_list}[1]

Verify product is not allowed to add to cart from PLP on mobile web
    [Tags]    Omni_PLP    testrailid:C795484
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0]
    PLP_page_mobile_web.Click product flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify detail tab in PDP should be visible

Verify badge of configurable product when set badge of child product all type
    [Tags]    Omni_PLP    testrailid:C795454
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0], ${product_sku_list}[0]
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible on flip card    ${product_sku_list}[0]

Verify badge of configurable product when set badge of child product home delivery type
    [Tags]    Omni_PLP    testrailid:C795477
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0], ${product_sku_list}[0]
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge disable should be visible on flip card    ${product_sku_list}[0]

Verify badge of configurable product when set badge of child product home delivery and click & collect types
    [Tags]    Omni_PLP    testrailid:C795476
    search_mobile_web_keyword.Search product for go to PLP page for mobile web     ${product_sku_list}[0], ${product_sku_list}[0]
    PLP_page_mobile_web.Verify home delivery badge enable should be visible on flip card    ${product_sku_list}[0]
    PLP_page_mobile_web.Verify click & collect badge enable should be visible on flip card    ${product_sku_list}[0]

Verify Out Of Stock Product Should Not Display In PLP
    [Tags]    testrailid:C282765    testrailid:C282766    product_PLP    jenkins_group1    Omni_PLP
    product_page.Go to the pages or sections on plp    ${plp_page_section_scope.oos_product}
    ${product_url}    ${product_name}    common_keywords.Get product url and name by sku    ${product_oos_sku_list}[0]
    plp_page.Verify Product Name Should Not Display In Search Product    ${product_name}
    Go To Direct Url    ${product_url}
    Wait Until Page Is Completely Loaded
    product_PDP_page.Verify Add To Bag Button Should Not Display    ${product_oos_sku_list}[0]
    product_PDP_page.Verify Out Of Stock Message Should Display

Verify Category Total Count In PLP
    [Tags]    testrailid:C421381    product_PLP    jenkins_group1    Omni_PLP
    ${url}=    Get Category Url Path By Category Id Via Api    ${category_id.level_4}
    Go To Direct Url    ${url}
    Wait Until Page Is Completely Loaded
    search_mobile_web_keyword.Verify Total Found Item Should Equal Expected Displayed Item

Verify filter by delivery options should be display and work correctly - Category product PLP
    [Tags]    testrailid:T2657458    PLP_Filter    search_and_filter    Omni_PLP
    common_cds_mobile_web.Go to beauty category - make up page
    product_PLP_mobile_web.Filter Product by delivery methods successfully    ${filters.delivery_methods.click_and_collect}
    
Verify filter by delivery options should be display and work correctly - Search PLP
    [Tags]    testrailid:T2657460    PLP_Filter    search_and_filter    Omni_PLP
    product_page.Go to the pages or sections on plp    ${plp_page_section_scope.search_plp}
    product_PLP_mobile_web.Filter Product by delivery methods successfully    ${filters.delivery_methods.delivery}
    
Verify filter by delivery options should be display and work correctly - Brand PLP
    [Tags]    testrailid:T2657461    PLP_Filter    search_and_filter    Omni_PLP
    product_page.Go to the pages or sections on plp    ${plp_page_section_scope.product_brand}
    product_PLP_mobile_web.Filter Product by delivery methods successfully    ${filters.delivery_methods.two_hours_pick_up}

Verify Delivery Option Badge Should Be Display Correctly For Specific Cases
    [Tags]    jenkins_group1    testrailid:C794059    testrailid:C794058    testrailid:C794057    testrailid:C794056    testrailid:C827563    testrailid:C827566    testrailid:C827567    testrailid:C827568    Omni_PLP
    ...    testrailid:C1010885    testrailid:C1010886    testrailid:C1010887    testrailid:C1010888    testrailid:C1014458    testrailid:C1014461
    [Template]    Template - Verify delivery option badge of section should be display correctly
    ${plp_page_section_scope.hot_deals}
    ${plp_page_section_scope.new_arrival}
    ${plp_page_section_scope.best_sellers}
    ${plp_page_section_scope.flash_deal}
    ${plp_page_section_scope.recently_viewed}
