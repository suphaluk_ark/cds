*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Variables ***
${cust_name}    ${shipping_address_th.firstname}${SPACE}${shipping_address_th.lastname}
${address_line1}    ${shipping_address_th.building}\,${SPACE}${shipping_address_th.address}
${address_line2}    ${shipping_address_th.sub_district}\,${SPACE}${shipping_address_th.district}\,${SPACE}${shipping_address_th.region}${SPACE}${shipping_address_th.zip_code}

${cust_name_update}    ${shipping_address_th.firstname_update}${SPACE}${shipping_address_th.lastname_update}
${address_line1_update}    ${shipping_address_th.building_update}\,${SPACE}${shipping_address_th.address_update}
${address_line2_update}    ${shipping_address_th.sub_district_update}\,${SPACE}${shipping_address_th.district_update}\,${SPACE}${shipping_address_th.region_update}${SPACE}${shipping_address_th.zip_code_update}

${valid_order_number}      ${track_order.track_order_user_1.valid_order_number}
${invalid_order_number}    ${track_order.track_order_user_1.invalid_order_number}
${invalid_phone_number}    123456789

*** Test Cases ***
### Member Profile ###
To verify the member profile form must be displayed correctly
    [Tags]    regression    account_management    member_profile    testrailid:C70764
    Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Profile tab
    my_account_page.Lastname field should be displayed customer data  ${account_management_information_1.lastname}
    my_account_page.Firstname field should be displayed customer data  ${account_management_information_1.firstname}
    my_account_page.Phone field should be displayed customer data  ${account_management_information_1.tel}
    my_account_page.Date of birth field should be displayed customer data  ${account_management_information_1.dob}
    my_account_page.E-mail field should be displayed customer data and disabled  ${account_management_information_1.email}
    my_account_page.Gender radio button should be displayed
    my_account_page.Language radio button should be displayed

To verify that user enters to my profile page and edits the profile
    [Tags]    regression    account_management    member_profile    testrailid:C70765
    Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Profile tab
    my_account_page.Leave Firstname field empty  ${account_management_information_1.firstname}
    my_account_page.Warning message for Firstname text should be displayed
    my_account_page.Leave Lastname field empty  ${account_management_information_1.lastname}
    my_account_page.Warning message for Lastname text should be displayed
    my_account_page.Leave Phone number field empty  ${account_management_information_1.tel}
    my_account_page.Warning message for Phone number text should be displayed
    my_account_page.Edit button should be displayed

To verify the Email field must be displayed correctly
    [Tags]    regression    account_management    member_profile    testrailid:C70766
    Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Profile tab
    my_account_page.E-mail field should be displayed customer data and disabled  ${account_management_information_1.email}

To verify the language must be displayed correctly
    [Tags]    regression    account_management    member_profile    testrailid:C70770
    Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Profile tab
    my_account_page.Language radio button should be displayed

To verify the Gender field must be displayed correctly
    [Tags]    regression    account_management    member_profile    testrailid:C70767
    Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Profile tab
    my_account_page.Gender radio button should be displayed
    my_account_page.Check if only one gender is checked

To verify the phone validation
    [Tags]    regression    account_management    member_profile    testrailid:C425032
    Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Profile tab
    my_account_page.Click edit button on my profile page
    my_account_page.Input text for Phone number field    ${invalid_phone_number}
    my_account_page.Warning Message Phone Should Start With 0 Should Display

### T1C Info ###
To verify the T1C Information must be displayed correctly
    [Tags]  regression    account_management    T1C_Info    testrailid:C83540
    Login Keywords for mobile web - Login Success    ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Overview tab
    my_account_page.Verify connect to The 1 Account button should be displayed
    my_account_page.Click Connect To The 1 Account Button
    my_account_page.Input T1C e-mail account  ${t1c_information_2.email}
    my_account_page.Input T1C password account  ${t1c_information_2.password}
    my_account_page.Click Login to T1C account button on account overview page
    my_account_page.Disconnect button should be displayed
    my_account_page.Customer T1C Information should be displayed correctly  ${t1c_information_2.t1c_number}

To verify CDS account can connect and disconnect success with The 1 account by email
    [Tags]  regression    account_management    T1C_Info    testrailid:C83541    testrailid:C83539    testrailid:C83538
    Login Keywords for mobile web - Login Success    ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Overview tab
    my_account_page.Verify connect to The 1 Account button should be displayed
    my_account_page.Click Connect To The 1 Account Button
    my_account_page.Login to The 1 Account Dialog should be displayed
    my_account_page.Input T1C e-mail account  ${t1c_information.email}
    my_account_page.Input T1C password account  ${t1c_information.password}
    my_account_page.Click Login to T1C account button on account overview page
    my_account_page.Disconnect button should be displayed
    my_account_page.Click Disconnect The 1 Account Button
    my_account_page.Verify connect to The 1 Account button should be displayed

### Address management ###
To verify Add, Edit and Delete new address function, Address information must save and display in address book correctly
    [Tags]  regression    account_management    address_management    testrailid:C49181    testrailid:C49212    testrailid:C49200    testrailid:C49230    testrailid:C49230
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Address Book tab
    my_account_page.Delete all addresses if existed

    #Add a new address and set default shipping or default billing address
    my_account_page.Verify Add New address button should be displayed on address book page
    my_account_page.Click Add New address button on address book page
    my_account_page.Click and input text for first name on address book page  ${shipping_address_th.firstname}
    my_account_page.Click and input text for last name on address book page  ${shipping_address_th.lastname}
    my_account_page.Click and input text for telephone number on address book page  ${shipping_address_th.phone}
    my_account_page.Click and input text for house number and street on address book page  ${shipping_address_th.address}
    my_account_page.Click and input text for postcode on address book page  ${shipping_address_th.zip_code}
    my_account_page.Click and input text for address name on address book page  ${shipping_address_th.address_name}
    my_account_page.Enable add tax invoice
    my_account_page.Click and input text for card id  ${shipping_address_th.vat_id}
    my_account_page.Check default shipping option
    my_account_page.Check default billing option
    my_account_page.Click SAVE CHANGES button on the Address Book page

    #verify the address correctness 
    my_account_page.Verify the lastest address name should be displayed correctly  ${shipping_address_th.address_name_tax}
    my_account_page.Verify the lastest customer name should be displayed correctly  ${cust_name}
    my_account_page.Verify the lastest customer phone should be displayed correctly  ${shipping_address_th.phone}
    my_account_page.Verify the lastest address line 1 should be displayed correctly  ${shipping_address_th.address}
    my_account_page.Verify the lastest address line 2 should be displayed correctly  ${address_line2}
    my_account_page.Verify default shipping address book should be selected  ${account_management_username_1}  ${account_management_password_1}
    my_account_page.Verify default billing address book should be selected  ${account_management_username_1}  ${account_management_password_1}
    my_account_page.Get a random address and click edit on it

    #Edit an address and verify
    ${current_id}=    my_account_page.Get the current address id from url
    my_account_page.Click and input text for telephone number on address book page  ${shipping_address_th.phone_update}
    my_account_page.Click and input text for address name on address book page  ${shipping_address_th.address_name_update}
    my_account_page.Click SAVE CHANGES button on the Address Book page
    my_account_page.Verify address name should be displayed correctly by id   ${current_id}  ${shipping_address_th.address_name_update}
    my_account_page.Verify phone should be displayed correctly by id  ${current_id}  ${shipping_address_th.phone_update}

    #Delete an address 
    ${random_address_id}=  my_account_page.Get a random address id from the address list
    my_account_page.Delete an address by id  ${random_address_id}
    my_account_page.Verify an adress should be not displayed by id  ${random_address_id}

To verify the required fields for address and phone validation
    [Tags]    regression    account_management    address_management    testrailid:C425033    testrailid:C48705    testrailid:C49156
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Address Book tab
    my_account_page.Verify Add New address button should be displayed on address book page
    my_account_page.Click Add New address button on address book page
    my_account_page.Click SAVE CHANGES button on the Address Book page
    my_account_page.Verify system displays warning message under the field
    my_account_page.Click and input text for telephone number on address book page    ${invalid_phone_number}
    my_account_page.Warning Message Shipping Address Phone Should Start With 0 Should Display For Mobile

### Wishlist ###
To verify that Member user is able to access to Wishlist page on My Account page
    [Tags]    regression    account_management    wishlist    testrailid:C70800
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Wishlist page
    wishlist_page.Wishlist title should be displayed

To verify that Member user is able to access to Wishlist page on Home page
    [Tags]    regression    account_management    wishlist    testrailid:C70799
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Go to Wishlist page by click on Wishlist icon
    wishlist_page.Wishlist title should be displayed

##
#To verify the wishlist page after user adds products to the wishlist page
#    [Tags]    regression    account_management    wishlist    testrailid:C48059    defect:4821    defect
#    wishlist.Remove all wishlist items in wishlist group  ${account_management_username_1}  ${account_management_password_1}  ${EMPTY}
#    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
#    #wishlist_api.Add wishlist item by SKU number    ${account_management_username_1}    ${account_management_password_1}  ${product_sku_list}[2]
#    wishlist_api.Add wishlist item by SKU number    ${account_management_username_1}    ${account_management_password_1}  ${product_sku_list}[4]
#    wishlist_api.Add wishlist item by SKU number    ${account_management_username_1}    ${account_management_password_1}  ${product_sku_list}[5]
#    wishlist_api.Add wishlist item by SKU number    ${account_management_username_1}    ${account_management_password_1}  ${product_sku_list}[8]
#    wishlist_api.Add wishlist item by SKU number    ${account_management_username_1}    ${account_management_password_1}  ${product_sku_list}[9]
#    wishlist_api.Add wishlist item by SKU number    ${account_management_username_1}    ${account_management_password_1}  ${product_sku_list}[15]
#    wishlist_api.Add wishlist item by SKU number    ${account_management_username_1}    ${account_management_password_1}  ${product_sku_list}[20]
#    common_cds_mobile_web.Navigate to Wishlist page
#    wishlist_page.Add to bag button should be displayed
#    wishlist_page.Pagination should be displayed
#    wishlist_page.Number of products which are added should be displayed correctly
#    wishlist_page.6 products should be displayed per page
##

To verify that Member user is able to see wishlist items after re-login to their account
    [Tags]    regression    account_management    wishlist    testrailid:C70796     testrailid:C48062
    
    #verify that users can add a product to wishlist
    wishlist.Remove all wishlist items in wishlist group  ${account_management_username_1}  ${account_management_password_1}  ${EMPTY}
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    ${product_url}    ${product_name}    Get product url and name by sku   ${product_sku_list}[4]
    common_keywords.Go To PDP Directly By Product Sku  ${product_sku_list}[4]
    product_page.Click wishlist icon on product detail page  ${product_sku_list}[4]
    common_cds_mobile_web.Navigate to Wishlist page
    login_keywords.Login Keywords for mobile web - Sign Out Success
    Wait Until Page Is Completely Loaded

    #verify that Member user is able to see wishlist items after re-login to their account
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Wishlist page
    wishlist_page.Added product should be displayed in wishlist    ${product_name}
    
    #verify that user deletes the products in wishlist page
    ${product_number}=  wishlist_page.Get the number of products displaying on wishlist
    wishlist_page.Remove the first product
    Wait Until Page Is Completely Loaded
    Sleep     2s    reason=Need to find a proper to wait later
    wishlist_page.Verify product is not displayed after deletion  1

    #verify the wishlist page when there is no product
    wishlist_page.Number of products should be 0
    wishlist_page.Continue shopping button should be displayed
    wishlist_page.Click on Continue shopping button

To verify that Pop up as login will be shown if Guest user need to access to Wishlist page on Home page
    [Tags]    regression    account_management    wishlist    testrailid:C70798    defect:6206    defect
    common_cds_mobile_web.Go to Wishlist page by click on Wishlist icon when user not login
    wishlist_page.Pop up login button should be displayed
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

### Track Order ###
To verify that "Track Your Orders" must display and working correctly
    [Tags]    regression    account_management    track_order    testrailid:C91390
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Track Your Orders
    order_tracking_page.The order number should be 2
    order_tracking_page.Search oder textbox should be displayed
    home_page_mobile_web_keywords.Click on Back button
    home_page_web_keywords.Click on brand
    login_keywords.Login Keywords for mobile web - Sign Out Success
    Wait Until Page Is Completely Loaded
    login_keywords.Login Keywords for mobile web - Login Success   ${no_tracking_order_user}    ${no_tracking_order_password}    ${no_tracking_order_information.firstname}
    common_cds_mobile_web.Navigate to Track Your Orders
    order_tracking_page.Search oder textbox should be displayed
    order_tracking_page.No orders should be displayed

To verify that user clicks at the order numbers which are displayed on "Track Your Orders" section
    [Tags]    regression    account_management    track_order    testrailid:C91391
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Track Your Orders
    order_tracking_page.Click on the first order from menu
    order_tracking_page.Order detail page should be displayed

To verify that if user puts the order number in a textbox and clicks "Track Order" button
    [Tags]    regression    account_management    track_order    testrailid:C91392
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Track Your Orders
    order_tracking_page.Input Order number   ${valid_order_number}
    order_tracking_page.Click Track order button
    order_tracking_page.Order detail page should be displayed
    common_cds_mobile_web.Navigate to Track Your Orders
    order_tracking_page.Input Order number   ${invalid_order_number}
    order_tracking_page.Click Track order button
    order_tracking_page.Error message for valid email and order number combination should be displayed

To verify that if user does not put the order number in a textbox and clicks "Track Order" button
    [Tags]    regression    account_management    track_order    testrailid:C91393
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    common_cds_mobile_web.Navigate to Track Your Orders
    order_tracking_page.Click Track order button
    order_tracking_page.Require order field message should be displayed

To verify Track Your Order menu for Guest
    [Tags]    regression    account_management    track_order    testrailid:C91403
    common_cds_mobile_web.Navigate to Track Your Orders
    Log To Console    ${track_order.guest_user_1.email}
    order_tracking_page.Input email  ${track_order.guest_user_1.email}
    order_tracking_page.Input Order number   ${track_order.guest_user_1.order_number}
    order_tracking_page.Click Track order button
    order_tracking_page.Guest Order detail page should be displayed
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

To verify a valid combination of Email and Order Number
    [Tags]    regression    account_management    track_order    testrailid:C91404
    common_cds_mobile_web.Navigate to Track Your Orders
    order_tracking_page.Input email  ${account_management_username_1}
    order_tracking_page.Input Order number   ${valid_order_number}
    order_tracking_page.Click Track order button
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

To verify an invalid combination of Email and Order Number
    [Tags]    regression    account_management    track_order    testrailid:C91405
    common_cds_mobile_web.Navigate to Track Your Orders
    order_tracking_page.Input Order number   ${invalid_order_number}
    order_tracking_page.Input email  ${account_management_username_1}
    order_tracking_page.Click Track order button
    order_tracking_page.Error message for valid email and order number combination should be displayed
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser