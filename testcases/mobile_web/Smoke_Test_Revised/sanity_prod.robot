*** Setting ***
Documentation   These test cases are following Smoke Test Revised's case, cut off some test step for support env: prod
Resource        ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Teardown   Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Test Cases ***
Verify that the Configurable product display correctly
    [Documentation]    - *Get random product via API*       : configurable
    ...                - *Data Validation points via API*   : falcon
    ...                - *Data Validation points via UI*    : add to cart, qty = 1 (to avoid the product is not enough)
    ...                - *smoke test revised tags*          : pdp, pdp_ui, testrailid:C996398
    [Tags]    sanity_prod
    common_cds_mobile_web.Setup - Open browser for mobile web
    common_cds_web.Retry get random product by category id, search product, go to pdp page via api    ${category_id.woman_clothing_tops_shirt_polos}    configurable

    ${url}=    common_cds_web.Get product urlkey by current url
    ${response}=    pdp_falcon.Api Product PDP By Url Key    ${url}    ${language}    ${BU}

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    ${dict_simple_product}=    product_PDP_keywords.Get configurable's simple product displayed via api   ${response}

    # 1. Data Validation points
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product brand name' should display correctly via api    ${response}
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product name' should display correctly via api    ${response}    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product overlay image' should display correctly via api    ${dict_simple_product}[json_object]
    # 4 Nov 2020 ** GTM is hiding redeem point on prod ** Should be FAIL 
    # Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : redeem point' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag new / promotion' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag only at / online exclusive' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product price' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product market place' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : pay by installment' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product configurable options' should be displayed    ${response}    ${dict_simple_product}[id]
    # ** Mobile web does not display product id ** waiting confirm with BA
    # Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product id' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product customer reviews' should be displayed via api    ${response}
    # 2. UI Validation points
    ${dict_random_product_sku}=    product_PDP_keywords.Select ramdom configurable options via api, return product    ${response}    
    Set Test Variable    ${${dict_random_product_sku}[sku]}    ${dict_random_product_sku}
    common_cds_mobile_web.Add product to cart, product should be displayed in cart    ${dict_random_product_sku}[sku]    ${1}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify that the Simple product display correctly
    [Documentation]    - *Get random product via API*       : simple
    ...                - *Data Validation points via API*   : falcon
    ...                - *Data Validation points via UI*    : add to cart, qty = 1 (to avoid the product is not enough)
    ...                - *smoke test revised tags*          : pdp, pdp_ui, testrailid:C996397
    [Tags]    sanity_prod
    common_cds_mobile_web.Setup - Open browser for mobile web
    common_cds_web.Retry get random product by category id, search product, go to pdp page via api    ${category_id.beauty_skincare_moisturiser}    simple
    ${url}=    common_cds_web.Get product urlkey by current url
    ${response}=    pdp_falcon.Api Product PDP By Url Key    ${url}    ${language}    ${BU}
    ${dict_simple_product}=    product_api.Get simple product data    ${response}

    # 1. Data Validation points
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product brand name' should display correctly via api    ${response}
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product header : product name' should display correctly via api    ${response}    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : image' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product overlay image' should display correctly via api    ${dict_simple_product}[json_object]
    # 4 Nov 2020 ** GTM is hiding redeem point on prod ** Should be FAIL 
    # Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : redeem point' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag new / promotion' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product tag only at / online exclusive' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product price' should display correctly via api    ${dict_simple_product}[json_object]    ${dict_simple_product}[sku]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : pay by installment' should display correctly via api    ${dict_simple_product}[json_object]
    # ** Mobile web does not display product id ** waiting confirm with BA
    # Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product id' should display correctly via api    ${dict_simple_product}[json_object]
    Run Keyword And Continue On Failure    product_PDP_keywords.Verify ui for smoke test - 'product details : product customer reviews' should be displayed via api    ${response}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

    # 2. UI Validation points
    # Social icon
    product_PDP_page.Verify social icon facebook on product PDP page
    product_PDP_page.Verify social icon twitter on product PDP page
    product_PDP_page.Verify social icon email on product PDP page
    product_PDP_page.Verify social icon line on product PDP page

    # Promotion tab ** 16 Nov 2020 no promotion tab on prod
    # product_PDP_page.Click 'promotions' tab
    # product_PDP_page.Verify 'promotions tab details' should be displayed
    
    # Delivery & Return tab
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Verify 'delivery & return tab details' should be displayed

    # Wishlist
    product_PDP_page.Verify 'add wishlist button' should be displayed    ${dict_simple_product}[sku]
    product_PDP_page.Click 'add wishlist' button    ${dict_simple_product}[sku]
    product_PDP_page.Verify 'please login pop up' should be displayed
    product_PDP_page.Click 'OK' please login pop up button

    # Add to cart
    Set Test Variable    ${${dict_simple_product}[sku]}    ${dict_simple_product}
    common_cds_web.Retry add product to cart, product should be displayed in mini cart    ${dict_simple_product}[sku]    ${1}

Verify that a searching by keyword will go to search page and search page UI is displayed with data correctly
    [Documentation]    - *smoke test revised tags*          : search, search_ui, testrailid:C995861
    [Tags]    sanity_prod
    Set Test Variable    ${test_keyword}    ${search_by_keyword.cosmetic}
    Set Test Variable    ${range_of_product}    ${5}

    common_cds_mobile_web.Setup - Open browser for mobile web
    search_keyword.Search product for go to PLP page    ${test_keyword}

    # 1. Get response
    ${response}=    search_falcon.Api Search Product By Keyword    ${test_keyword}    ${language}    ${BU.lower()}    ${50}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]

    # 2. Get display product 
    ${range_of_product}=    Set Variable If     ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}    ${range_of_product}
    ${list_displayed_sku}=    plp_page.Get list display product sku    ${range_of_product}

    # 3. Data Validation points header
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Verify 'search keyword label' should be displayed correctly    ${test_keyword.lower()}
    plp_page.Verify 'product found label' should be displayed correctly    ${result_total_search_product}

    # 4. Data Validation points products, The displayed product should display data from API correctly
    FOR    ${displayed_sku}    IN    @{list_displayed_sku}
        # displayed product
        ${displayed_data-product-sku}=  Set Variable    ${displayed_sku}[data-product-sku]
        ${displayed_data-product-id}=   Set Variable    ${displayed_sku}[data-product-id]
        ${displayed_index}=             Set Variable    ${displayed_sku}[index]

        # API product by displayed sku
        ${dict_display_sku}=    plp_api.Get data-product-sku, data-product-id data by sku    ${response}    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    Should Be Equal As Strings    ${dict_display_sku}[data_index]    ${displayed_index}

        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product brand' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product name' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}
        
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product overlay image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product price' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag new / promotion' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag only at / online exclusive' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify that searching by brand name will go to Normal Brand PLP and Normal Brand PLP UI is displayed with data correctly
    [Documentation]    - *smoke test revised tags*          : search    search_ui    smoke_test_revised    testrailid:C1005735
    [Tags]    sanity_prod
    Set Test Variable    ${test_brand}    ${search_by_keyword.aldo}
    Set Test Variable    ${range_of_product}    ${5}

    common_cds_mobile_web.Setup - Open browser for mobile web
    search_keyword.Search product for go to PLP page    ${test_brand}

    # 1. Get response
    ${response}=    search_falcon.Api Search Product By Brand Name    ${test_brand}    ${language}    ${BU.lower()}    ${50}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]

    # 2. Get display product 
    ${range_of_product}=    Set Variable If     ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}    ${range_of_product}
    ${list_displayed_sku}=    plp_page.Get list display product sku    ${range_of_product}

    # 3. Data Validation points header
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Verify 'product found label' should be displayed correctly    ${result_total_search_product}

    # 4. Data Validation points products, The displayed product should display data from API correctly
    FOR    ${displayed_sku}    IN    @{list_displayed_sku}
        # displayed product
        ${displayed_data-product-sku}=  Set Variable    ${displayed_sku}[data-product-sku]
        ${displayed_data-product-id}=   Set Variable    ${displayed_sku}[data-product-id]
        ${displayed_index}=             Set Variable    ${displayed_sku}[index]

        # API product by displayed sku
        ${dict_display_sku}=    plp_api.Get data-product-sku, data-product-id data by sku    ${response}    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    Should Be Equal As Strings    ${dict_display_sku}[data_index]    ${displayed_index}

        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product brand' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product name' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}
        
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product overlay image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product price' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag new / promotion' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag only at / online exclusive' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify that standard boutique must be displayed and work correctly
    [Documentation]    - *smoke test revised tags*          : search    search_ui    smoke_test_revised    testrailid:C996404
    [Tags]    sanity_prod
    Set Test Variable    ${test_brand}    ${product_brand.aveda}
    Set Test Variable    ${range_of_product}    ${5}

    common_cds_mobile_web.Setup - Open browser for mobile web
    common_cds_web.Go to shop by brand page
    shop_by_brand_page.Click on brand    ${test_brand}
    shop_by_brand_page.Verify product in What's new section is shown correctly    ${test_brand}
    standard_boutique_page.Click on Shop All Product link    ${test_brand}
    standard_boutique_page.Verify correct brand name on PLP    ${test_brand}

    # 1. Get response
    ${response}=    search_falcon.Api Search Product By Brand Name    ${test_brand}    ${language}    ${BU.lower()}    ${50}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]

    # 2. Get display product 
    ${range_of_product}=    Set Variable If     ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}    ${range_of_product}
    ${list_displayed_sku}=    plp_page.Get list display product sku    ${range_of_product}

    # 3. Data Validation points header
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Verify 'product found label' should be displayed correctly    ${result_total_search_product}

    # 4. Data Validation points products, The displayed product should display data from API correctly
    FOR    ${displayed_sku}    IN    @{list_displayed_sku}
        # displayed product
        ${displayed_data-product-sku}=  Set Variable    ${displayed_sku}[data-product-sku]
        ${displayed_data-product-id}=   Set Variable    ${displayed_sku}[data-product-id]
        ${displayed_index}=             Set Variable    ${displayed_sku}[index]

        # API product by displayed sku
        ${dict_display_sku}=    plp_api.Get data-product-sku, data-product-id data by sku    ${response}    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    Should Be Equal As Strings    ${dict_display_sku}[data_index]    ${displayed_index}

        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product brand' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product name' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}
        
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product overlay image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product price' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag new / promotion' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag only at / online exclusive' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait