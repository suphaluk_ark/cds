*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Variables ***
@{sku_list}    ${product_sku_list}[2]  ${product_sku_list}[0]
${dict_2_products}    ${product_sku_list}[0]=1   ${product_sku_list}[2]=1

#buy 2 will be discounted
${discount_product}    ${product_sku_list}[0]
# gify only has qty of 5
${product_not_enough_gift}  ${product_sku_list}[17]
${product_with_free_item}    ${product_sku_list}[17]
${free_gift_sku}    ${product_sku_list}[12]
${coupon}    COUPON10

*** Test Cases ***
Verify the ordered price for free shipping that user sets value in MDC (Standard Delivery)
    [Tags]    regression    shopping_cart    testrailid:C24789
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    &{change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    header_web_keywords.Verify FREE standard delivery message
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify the display of total quantity and total price (Not include promotional discount price)
    [Tags]    regression    shopping_cart    testrailid:C25261
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[0]
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[2]
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[3]
    my_cart_page.Verify total quantity must be displayed correctly
    
Verify the display of grand total
    [Tags]    regression    shopping_cart    testrailid:C25262
    common_cds_mobile_web.Login With Credentials  ${cds_shopping_cart_username}    ${cds_shopping_cart_password}
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[0]
    my_cart_page.Verify price grand total should be caculated on cart page correctly
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

verlify that user clicks "SECURE CHECKOUT" button
    [Tags]    regression    shopping_cart    testrailid:C25263
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[0]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Verify Delivery Details page should be displayed
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

To verify the order summary section and payment summary section must be displayed correctly
    [Tags]    regression    shopping_cart    testrailid:C25859
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[2]    2
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[4]
    checkout_page.Check Out Page Should Be Visible
    checkout_keywords.Input guest infomation at checkout page
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_keywords.Checkout banktranfer for guest member
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify the "Edit Bag" link
    [Tags]    regression    shopping_cart    testrailid:C25861
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    checkout_page_mobile_web.Click view details
    delivery_details_page.Click on edit bag on delivery page
    my_cart_page.Verify shopping bag page should be displayed
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify the total and order total
    [Tags]    regression    shopping_cart    testrailid:C26443
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[2]    2
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[0]    5
    my_cart_page.Verify price grand total should be caculated on cart page correctly
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify the shipping message
    [Tags]    regression    shopping_cart    testrailid:C26423
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[2]    2
    my_cart_page.Verify FREE standard delivery message
    header_web_keywords.Remove all product in shopping cart
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[3]
    my_cart_page.Verify message if add item has total price less than 699 THB
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify that the remaining price must be calculated correctly
    [Tags]    regression    shopping_cart    testrailid:C86804
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[3]
    my_cart_page.Verify message if add item has total price less than 699 THB
    my_cart_page.Verify price grand total should be caculated on cart page correctly
    header_web_keywords.Remove all product in shopping cart
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[2]    2
    my_cart_page.Verify FREE standard delivery message
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify that user changes the quantity
    [Tags]    regression    shopping_cart    testrailid:C49251
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[2]
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[0]
    my_cart_page.Change quantity of product from the drop-down list  ${product_sku_list}[2]  2
    my_cart_page.Change quantity of product from the drop-down list  ${product_sku_list}[0]  3
    my_cart_page.Verify price grand total should be caculated on cart page correctly
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify that user clicks "Remove" button
    [Tags]    regression    shopping_cart    testrailid:C49252
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[0]    5
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[2]
    my_cart_page.Remove product from shopping bag  ${product_sku_list}[2]
    my_cart_page.Verify price grand total should be caculated on cart page correctly

Verify that if products has a gift and there is available gift in a stock
    [Tags]    regression    shopping_cart    testrailid:C79569
    common_cds_mobile_web.Search product and add product to cart  ${product_with_free_item}
    my_cart_page.Free gift message should be displayed in cart page
    my_cart_page.Click on free gift message
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify that a gift is not enough while user buying a product
    [Tags]    regression    shopping_cart    testrailid:C79573
    common_cds_mobile_web.Search product and add product to cart  ${product_not_enough_gift}    6
    my_cart_page.Click on free gift message
    my_cart_page.Verify not enough free gift message should be displayed
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify that freebies item will not count on mini cart if user is guest
    [Tags]    regression    shopping_cart    testrailid:C75012
    common_cds_mobile_web.Search product and add product to cart  ${product_with_free_item}
    my_cart_page.Free gift message should be displayed in cart page
    my_cart_page.Click on free gift message
    my_cart_page.Verify quantity of product should be displayed correctly  ${product_with_free_item}  1
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

Verify that freebies item will not count on mini cart if user is member
    [Tags]    regression    shopping_cart    testrailid:C86714
    common_cds_mobile_web.Login With Credentials    ${cds_shopping_cart_username}    ${cds_shopping_cart_password}
    header_web_keywords.Remove all product in shopping cart
    common_cds_mobile_web.Search product and add product to cart  ${product_with_free_item}
    my_cart_page.Free gift message should be displayed in cart page
    my_cart_page.Click on free gift message
    my_cart_page.Verify quantity of product should be displayed correctly  ${product_with_free_item}  1
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

To validate free gift with this purchase item shows correctly incase added product by guest
    [Tags]    regression    shopping_cart    testrailid:C79544
    common_cds_mobile_web.Search product and add product to cart  ${product_with_free_item}
    my_cart_page.Free gift message should be displayed in cart page
    my_cart_page.Click on free gift message
    my_cart_page.Verify free gift image should be displayed on cart page  ${product_sku_list}[17]
    my_cart_page.Verify free gift product name should be displayed on cart page  ${product_sku_list}[17]
    my_cart_page.Verify free gift brand name should be displayed on cart page  ${product_sku_list}[17]
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

To validate free gift with this purchase item shows correctly incase added product by member
    [Tags]    regression    shopping_cart    testrailid:C86711
    common_cds_mobile_web.Login With Credentials    ${cds_shopping_cart_username}    ${cds_shopping_cart_password}
    header_web_keywords.Remove all product in shopping cart
    common_cds_mobile_web.Search product and add product to cart  ${product_with_free_item}
    my_cart_page.Click on free gift message
    my_cart_page.Verify free gift image should be displayed on cart page  ${free_gift_sku}
    my_cart_page.Verify free gift product name should be displayed on cart page  ${free_gift_sku}
    my_cart_page.Verify free gift brand name should be displayed on cart page  ${free_gift_sku}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser

To validate free gift with this purchase item shows correctly after merging cart guest to member
    [Tags]    regression    shopping_cart    testrailid:C86712
    common_cds_mobile_web.Login With Credentials    ${cds_shopping_cart_username}    ${cds_shopping_cart_password}
    header_web_keywords.Remove all product in shopping cart
    common_cds_mobile_web.Search product and add product to cart  ${product_with_free_item}
    my_cart_page.Click on free gift message
    my_cart_page.Verify free gift image should be displayed on cart page  ${free_gift_sku}
    my_cart_page.Verify free gift product name should be displayed on cart page  ${free_gift_sku}
    my_cart_page.Verify free gift brand name should be displayed on cart page  ${free_gift_sku}
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    header_web_keywords.Remove all product in shopping cart
    ...    AND    Close Browser
