*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close All Browsers

*** Variables ***
${outside_bkk}    77110
${tracking_no}    ROBOT_TRACKING
${product_sku}    ${product_sku_list}[2]

*** Test Cases ***
Verify shipping method for outside Bangkok address guest
    [Tags]    smoke    outside_bkk     testrailid:C985022
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details    postcode=${outside_bkk}
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_transfer}
    checkout_page.Order status from thank you page should be    ${thankyou_page.status_pending}
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]    shipping=${0}
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}

Verify shipping method for outside Bangkok address member
    [Tags]    smoke    outside_bkk     testrailid:C377394
    Login Keywords for mobile web - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_transfer}
    checkout_page.Order status from thank you page should be    ${thankyou_page.status_pending}
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]    shipping=${0}
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}

Verify successfully order products with credit card method and pickup at store guest
    [Tags]    smoke    credit_card    testrailid:C985020
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details    postcode=${outside_bkk}
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    Order successful with credit card
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_credit}
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]

Verify successfully order products with credit card method and pickup at store member
    [Tags]    smoke    credit_card    testrailid:C379493
    Login Keywords for mobile web - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Select Payment Method With Credit Card
    Order successful with credit card
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_credit}
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]

Verify order status from mdc should be 'LOGISTICS' status guest
    [Tags]    smoke    credit_card    testrailid:C379494
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details    postcode=${outside_bkk}
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    Order successful with credit card
    checkout_page.Thank You Page Should Be Visible
    E2E_flow_keywords.Update order status to shipped and verify status in MDC, MCOM is complete fullyshipped    ${increment_id}    ${product_sku}
    E2E_flow_keywords.Update order status to ready to collect and verify status in MDC is fullyshipped    ${increment_id}
    E2E_flow_keywords.Update order status to delivered and verify status in MDC is fullyshipped    ${increment_id}    ${product_sku}

Verify order status from mdc should be 'LOGISTICS' status member
    [Tags]    smoke    credit_card    testrailid:C379494
    Login Keywords for mobile web - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Select Payment Method With Credit Card
    Order successful with credit card
    checkout_page.Thank You Page Should Be Visible
    Navigate to Home page
    E2E_flow_keywords.Update order status to shipped and verify status in MDC, MCOM and order progress bar is complete fullyshipped    ${increment_id}    ${product_sku}
    E2E_flow_keywords.Update order status to ready to collect and verify status in MDC and order progress bar is fullyshipped    ${increment_id}
    E2E_flow_keywords.Update order status to delivered and verify status in MDC and order progress bar is fullyshipped    ${increment_id}    ${product_sku}

Verify successfully order products with payment COD and successfully ship by standard guest
    [Tags]    smoke    cod    testrailid:C985021
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details    postcode=${outside_bkk}
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method COD
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For COD Payment Method
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible
    Set Suite Variable    ${order_id_cod_${member_type}}    ${increment_id}
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_cod}
    checkout_page.Order status from thank you page should be    ${thankyou_page.status_pending}
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]
    common_cds_web.Order status from mdc should be 'pending' status    ${increment_id}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}

Verify successfully order products with payment COD and successfully ship by standard member
    [Tags]    smoke    cod    testrailid:C379522
    Login Keywords for mobile web - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Select Payment Method COD
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For COD Payment Method
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible
    Set Suite Variable    ${order_id_cod_${member_type}}    ${increment_id}
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_cod}
    checkout_page.Order status from thank you page should be    ${thankyou_page.status_pending}
    checkout_page.Verify order total in thank you page    ${${product_sku}}[price]
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}

[Incomplete payment] Verify member checkout order by shipping method is standard delivery and payment method is 123 bank transfer
    [Tags]    smoke    testrailid:C549552
    Login Keywords for mobile web - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible

[Incomplete payment] Verify member checkout order by shipping method is pickup at store and payment method is 123 bank transfer
    [Tags]    smoke    testrailid:C549553    covid-19
    Login Keywords for mobile web - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible

[Incomplete payment] Verify guest checkout order by shipping method is standard delivery and payment method is 123 bank transfer
    [Tags]    smoke    testrailid:C549556
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details    postcode=${outside_bkk}
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    checkout_page.Thank You Page Should Be Visible

[Incomplete payment] Verify guest checkout order by shipping method is pickup at store and payment method is 123 bank transfer
    [Tags]    smoke    testrailid:C549557    covid-19
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_page.Input customer details
    checkout_keywords.Verify delivery options are displayed correctly
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible
   
Verify payment credit card fail and Re-payment guest
    [Tags]    smoke    production    re-payment    testrailid:C379520
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details    postcode=${outside_bkk}
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Order unsuccessful with credit card
    'Sorry your order is not complete' Msg should be displayed
    common_cds_web.Verify order status MCOM 'ONHOLD', order status reason MDC 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}

Verify payment credit card fail and Re-payment member
    [Tags]    smoke    production    re-payment    testrailid:C379520
    Login Keywords for mobile web - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Order unsuccessful with credit card
    'Sorry your order is not complete' Msg should be displayed
    common_cds_web.Verify order status MCOM 'ONHOLD', order status reason MDC 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}

[Incomplete payment] Verify member checkout order by shipping method is standard delivery and payment method is credit card
    [Tags]    smoke    testrailid:C549554
    Login Keywords for mobile web - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Order unsuccessful with credit card
    'Sorry your order is not complete' Msg should be displayed

[Incomplete payment] Verify member checkout order by shipping method is pickup at store and payment method is credit card
    [Tags]    smoke    testrailid:C549555    covid-19
    Login Keywords for mobile web - Login Success    ${outside_bkk_username}    ${outside_bkk_password}    ${outside_bkk_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Order unsuccessful with credit card
    'Sorry your order is not complete' Msg should be displayed

[Incomplete payment] Verify guest checkout order by shipping method is standard delivery and payment method is credit card
    [Tags]    smoke    testrailid:C549558
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details    postcode=${outside_bkk}
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Order unsuccessful with credit card
    'Sorry your order is not complete' Msg should be displayed

[Incomplete payment] Verify guest checkout order by shipping method is pickup at store and payment method is credit card
    [Tags]    smoke    testrailid:C549559    covid-19
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku}    1    ${True}
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details    postcode=${outside_bkk}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Order unsuccessful with credit card
    'Sorry your order is not complete' Msg should be displayed