*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web

Test Teardown    Run Keywords    common_cds_web.Tear down with cancel order    AND    Order_details_page.Click continue button on payment page
Suite Teardown    Close All Browsers

*** Test Cases ***
To Verify that order created on each system when user place order with Click & Collect + Credit card payment
    [Tags]    regression    suite_SIT_1    PK_source    testrailid:C273765
    Login Keywords for mobile web - Login Success    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    checkout_keywords.Select standard pickup and go to payment page    ${pick_at_store.central_bangna}
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

To Click & Collect + T1C full redemption
    [Tags]    regression    suite_SIT_1    PK_source    partial_redeem    testrailid:C273767
    Login Keywords for mobile web - Login Success    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    checkout_keywords.Select standard pickup and go to payment page    ${pick_at_store.central_bangna}
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order at MDC MCOM FMS and WMS with no shipping fee and payment by credit card
    [Tags]    regression    suite_SIT_1    PK_source    testrailid:C273708
    Login Keywords for mobile web - Login Success    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order at MDC MCOM FMS and WMS with shipping fee and payment by credit card
    [Tags]    regression    suite_SIT_1    PK_source    testrailid:C273709
    Login Keywords for mobile web - Login Success    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order at MDC MCOM FMS and WMS with no shipping fee and payment by cod
    [Tags]    regression    suite_SIT_1    PK_source    testrailid:C273712
    Login Keywords for mobile web - Login Success    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    order_details_mobile_web.Successful pay with COD method
    Get order id from checkout page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order creation on MDC MCOM FMS and WMS with no shipping fee and T1C full redemption
    [Tags]    regression    suite_SIT_1    PK_source    Ship_to_address    partial_redeem     testrailid:C273713
    Login Keywords for mobile web - Login Success    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    Get order id from checkout page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database

Verify order creation on MDC MCOM FMS and WMS with with shipping fee and T1C full redemption
    [Tags]    regression    suite_SIT_1    PK_source    Ship_to_address    partial_redeem     testrailid:C273714
    Login Keywords for mobile web - Login Success    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    Get order id from checkout page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database