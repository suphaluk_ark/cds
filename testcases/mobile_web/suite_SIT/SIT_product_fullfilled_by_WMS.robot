*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    common_cds_web.Tear down with cancel order    AND    Close Browser

*** Test Cases ***
To Ship to address (no shipping fee) + credit card payment
    [Tags]    regression    suite_SIT_2    WMS_source    Ship_to_address    testrailid:C273747    testrailid:C273737
    Login Keywords for mobile web - Login Success    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To Verify that order created on each system when user place order with ship to address (no shipping fee) + T1C full redemption
    [Tags]    regression    suite_SIT_2    WMS_source    Ship_to_address    partial_redeem     testrailid:C273752
    Login Keywords for mobile web - Login Success    ${WMS_source_username3}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    E2E_flow_keywords.Verify order status in MDC and MCOM are pending
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To Ship to address (with shipping fee) + credit card payment
    [Tags]    regression    suite_SIT_2    WMS_source    Ship_to_address    testrailid:C273748
    Login Keywords for mobile web - Login Success    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select Same-day shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To verify that order created on each system when user place order with Click & Collect + Credit card payment
    [Tags]    regression    suite_SIT_2    WMS_source    testrailid:C273762
    Login Keywords for mobile web - Login Success    ${WMS_source_username1}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To verify that order created on each system when user place order with Pickup at Store (no shipping fee) + Credit card payment
    [Tags]    regression    suite_SIT_2    WMS_source    partial_redeem    testrailid:C273770
    Login Keywords for mobile web - Login Success    ${WMS_source_username1}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To verify that order created on each system when user place order with Click & Collect + T1C full redemption
    [Tags]    regression    suite_SIT_3    WMS_source    partial_redeem    testrailid:C273764
    Login Keywords for mobile web - Login Success    ${WMS_source_username1}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Select pick at store    ${pick_at_store.central_bangna}
    Wait Until Page Loader Is Not Visible
    Go to payment page
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To verify that order created on each system when user place order with Pickup at store (no shipping fee) + T1C full redemption
    [Tags]    regression    suite_SIT_3    WMS_source    partial_redeem    testrailid:C273774
    Login Keywords for mobile web - Login Success    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]    1    ${True}
    checkout_page.Check Out Page Should Be Visible
    Select pick at store    ${pick_at_store.central_bangna}
    Wait Until Page Loader Is Not Visible
    Go to payment page
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To verify that order created on each system when user place order with Click & Collect + 123 Payment
    [Tags]    regression    suite_SIT_3    WMS_source    testrailid:C273763    testrailid:C273740
    Login Keywords for mobile web - Login Success    ${WMS_source_username1}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[2]    1    ${True}
    Checkout, select pick at store and pay by 1 2 3 bank transfer(check shipping charge)    ${pick_at_store.central_bangna}
    checkout_page.Get order ID from thank you page
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}

To Verify that order created on each system when user place order with ship to address (no shipping fee) + 123 payment
    [Tags]    regression    suite_SIT_3    WMS_source    Ship_to_address    testrailid:C273749
    Login Keywords for mobile web - Login Success    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    E2E_flow_keywords.Confirm payment by banktransfer
    checkout_page.Get order ID from thank you page
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}

To verify that order created on each system when user place order with Pickup at Store (no shipping fee) + 123 Payment
    [Tags]    regression    suite_SIT_3    WMS_source    partial_redeem    testrailid:C273772
    Login Keywords for mobile web - Login Success    ${WMS_source_username2}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[0]
    Select pickup at store option then confim payment by 123 bank transfer    ${pick_at_store.central_bangna}
    checkout_page.Get order ID from thank you page
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}

To Verify that order created on each system when user place order with Ship to address (with shipping fee) + 123 payment
    [Tags]    regression    suite_SIT_3    WMS_source    Ship_to_address    testrailid:C273750
    Login Keywords for mobile web - Login Success    ${WMS_source_username3}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select Next-day shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    E2E_flow_keywords.Confirm payment by banktransfer
    checkout_page.Get order ID from thank you page
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}

To Verify that order created on each system when user place order with ship to address (with shipping fee) + T1C full redemption
    [Tags]    regression    suite_SIT_3    WMS_source    Ship_to_address    partial_redeem     testrailid:C273753
    Login Keywords for mobile web - Login Success    ${WMS_source_username3}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select Next-day shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    # checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card
    checkout_page.Get order ID from thank you page
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database

To Verify that order created on each system when user place order with ship to address (shipping fee) + COD
    [Tags]    regression    suite_SIT_3    WMS_source    Ship_to_address    testrailid:C273751
    Login Keywords for mobile web - Login Success    ${WMS_source_username3}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method COD
    Wait Until Page Is Completely Loaded
    checkout_page.Click Confirm Order For COD Payment Method
    Verify order status in MDC and MCOM are pending 