*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Test Cases ***
To verify that able to customer cancel order before invoice
    [Tags]    regression    suite_SIT_2    WMS_source    testrailid:C273746
    Login Keywords for mobile web - Login Success    ${WMS_source_username}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    E2E_flow_keywords.Fully cancel order from MCOM and verify

To verify that order will be "pending payment" on MCOM when payment incomplete
    [Tags]    regression    suite_SIT_2    WMS_source    testrailid:C273744
    Login Keywords for mobile web - Login Success    ${WMS_source_username}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select standard shipping address and go to payment page
    E2E_flow_keywords.Confirm payment by banktransfer
    checkout_page.Get order ID from thank you page
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}
