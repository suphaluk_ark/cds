*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web

Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser
Suite Teardown    Close All Browsers

*** Test Cases ***
To verify order status on each system when have pick-pack completed and ready to delivery to Click & Collect store (MDC + MCOM + ESB + WMS), source stock 10138
    [Tags]    regression    suite_SIT_1    WMS_source    testrailid:C273723    testrailid:C273724    testrailid:C273725    testrailid:C273778    testrailid:C273780
    Login Keywords for mobile web - Login Success    ${WMS_source_username}    ${WMS_source_password}    ${WMS_source_information.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    Order_details_page.Click continue button on payment page
    common_cds_mobile_web.Navigate to My Order page
    Order_details_page.Select order at my order page    ${increment_id}
    E2E_flow_keywords.Verify order status in MDC, MCOM and order progress bar after create order successfully    ${increment_id}
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database
    E2E_flow_keywords.Update order status to shipped and verify status in MDC, MCOM and order progress bar is complete fullyshipped    ${increment_id}    ${product_sku_list}[2]
    E2E_flow_keywords.Update order status to ready to collect and verify status in MDC and order progress bar is fullyshipped    ${increment_id}
    E2E_flow_keywords.Update order status to delivered and verify status in MDC and order progress bar is fullyshipped    ${increment_id}    ${product_sku_list}[2]

To verify order status on each system when the order in transit, ready to collect to Store and customer received products at store
    [Tags]    regression    suite_SIT_1    PK_source    testrailid:C273779    testrailid:C273778    testrailid:C273780
    Login Keywords for mobile web - Login Success    ${PK_source_username}    ${PK_source_password}    ${PK_source_information.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[2]    1    ${True}
    E2E_flow_keywords.Select delivery pickup at store option and confirm payment by credit card
    Order_details_page.Click continue button on payment page
    common_cds_mobile_web.Navigate to My Order page
    Order_details_page.Select order at my order page    ${increment_id}
    E2E_flow_keywords.Verify order status in MDC, MCOM and order progress bar after create order successfully    ${increment_id}
    E2E_flow_keywords.Verify order is created in FMS database
    E2E_flow_keywords.Update order status to shipped and verify status in MDC, MCOM and order progress bar is complete fullyshipped    ${increment_id}    ${product_sku_list}[2]    source=pickingtool
    E2E_flow_keywords.Update order status to ready to collect and verify status in MDC and order progress bar is fullyshipped    ${increment_id}