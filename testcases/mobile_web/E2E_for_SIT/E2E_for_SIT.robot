*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Test Cases ***
Order (MKP) product paid via Credit card full payment with T1 redeem, Click and Collect
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : No
    ...                - *Delivery type*    : Pickup at store/ Standard Delivery
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Credit card full payment
    [Tags]    e2e_for_sit    testrailid:C995856    e2e_for_sit_02    e2e_flow_2
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_02}  ${e2e_password_02}
    login_keywords.Login Keywords for mobile web - Login Success  ${e2e_username_02}  ${e2e_password_02}    ${e2e_information_02.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${MKP0183257.sku}
    my_cart_page.Click Secure Checkout Button
    # checkout_keywords.Select pick at store    ${central_store.central_bangna}
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_keywords.Redeem point and verify T1C point discount    ${40}   ${5}
    checkout_page.Select Payment Method With Credit Card
    checkout_keywords.Order successful with credit card 
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${MKP0183257.price}    ${0}    ${5}    ${80}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}

Order (Retail) product with cart price rule fixed Coupon, paid via Credit card with T1 redeem
    [Documentation]    - *Product source wms*           : CDS11133773 - CDS_SO_10138
    ...                - *Product source pickingtool*   : CDS10219928 - CDS_SO_10116
    ...                - *Auto coupon*                  : No
    ...                - *Apply coupon*                 : ${coupon.five_bath} - CDS: Rule 215 CDS11133773 - RBS: Rule 5087 RBS20265274
    ...                - *Delivery type*                : Standard delivery
    ...                - *Redeem point*                 : Yes
    ...                - *Payment type*                 : Credit card full payment
    ...                - *API*                          : MDC,MCOM,EBS,FMS,PickingTool,WMS
    [Tags]    e2e_for_sit    testrailid:C995855    e2e_for_sit_02    e2e_flow_2
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_02}  ${e2e_password_02}
    login_keywords.Login Keywords for mobile web - Login Success  ${e2e_username_02}  ${e2e_password_02}    ${e2e_information_02.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${CDS10219928.sku}
    common_cds_mobile_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    my_cart_page_mobile_web.Verify total price with quantity, summarize multiple product in cart page    ${CDS10219928.sku}   ${cart_price_rule_sku.apply_coupon['5BAHT']}
    my_cart_page.Apply coupon code  ${coupon.five_bath}
    my_cart_page.Verify coupon code display in cart    ${coupon.five_bath}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${coupon.five_bath}    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}   ${5.0}
    my_cart_page.Click Secure Checkout Button
    E2E_flow_keywords.Select standard shipping address and go to payment page
    checkout_keywords.Redeem point and verify T1C point discount    ${40}    ${5}    ${t1c_information_4.email}   ${t1c_information_4.password}
    E2E_flow_keywords.Confirm payment by credit card
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${discount_amount}    ${5}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}
    common_cds_web.Retry verify that order create on WMS success    ${increment_id}
    kibana_api.Retry ESB update shipment status to shipped by sku    ${increment_id}    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    kibana_api.Retry ESB update shipment status to shipped by sku    ${increment_id}    ${CDS10219928.sku}    source=pickingtool
    kibana_api.Retry ESB update shipment status to delivered by sku    ${increment_id}    ${cart_price_rule_sku.apply_coupon['5BAHT']}
    kibana_api.Retry ESB update shipment status to delivered by sku    ${increment_id}    ${CDS10219928.sku}    source=pickingtool
    common_cds_web.Verify order status from MCOM should be 'COMPLETE, FULLYSHIPPED'   ${increment_id}
    common_cds_web.Verify order status from MDC should be 'COMPLETE, FULLYSHIPPED'    ${increment_id}
    fms_db.Verify that order number exist in FMS DB    ${increment_id}

Order product with mix product (mkp and retail) + product with back order+cart price rule, paid via credit card
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : ${coupon.five_bath} - CDS: Rule 215 CDS11133773 - RBS: Rule 5087 RBS20265274
    ...                - *Delivery type*    : shipping to address
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Credit card full payment
    [Tags]    e2e_for_sit    testrailid:C996112    e2e_for_sit_02    e2e_flow_2
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${e2e_username_02}  ${e2e_password_02}
    login_keywords.Login Keywords for mobile web - Login Success  ${e2e_username_02}  ${e2e_password_02}    ${e2e_information_02.firstname}
    common_cds_mobile_web.Search product and add product to cart    ${cart_price_rule_sku.apply_coupon['5BAHT']}    # Retail Product
    common_cds_mobile_web.Search product and add product to cart    ${MKP0183257.sku}    # Marketplace Product
    my_cart_page_mobile_web.Verify total price with quantity, summarize multiple product in cart page    ${cart_price_rule_sku.apply_coupon['5BAHT']}   ${MKP0183257.sku}
    my_cart_page.Apply coupon code  ${coupon.five_bath}
    my_cart_page.Verify coupon code display in cart    ${coupon.five_bath}
    my_cart_page.Verify e-coupon discount in cart page(Baht)    ${coupon.five_bath}    ${${cart_price_rule_sku.apply_coupon['5BAHT']}.price}   ${5.0}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    # checkout_keywords.Redeem point and verify T1C point discount    ${80}   ${10}
    checkout_page.Select Payment Method With Credit Card
    checkout_keywords.Order successful with credit card
    checkout_page.Thank You Page Should Be Visible
    checkout_page.Verify order total in thank you page    ${total_price_with_quantity}    ${discount_amount}    ${0}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}