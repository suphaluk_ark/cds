*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Variables ***
${store}=    ${store_view.name.en}
@{lst_skus_standard_pickup_sku_and_standard_delivery_sku}   ${omni_sku.sku_have_only_standard_pickup}    ${omni_sku.sku_have_only_standard_delivery}
@{quantities}    1    1

*** Test Cases ***
Guest checkout flow: Log in from Register page
    [Tags]    checkout1    regression    checkout_process    testrailid:C192699
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    common_cds_mobile_web.Navigate to Login page
    login_page.Input username  ${cds_checkout_process_username}
    login_page.Input password  ${cds_checkout_process_password}
    login_page.Click login button
    checkout_page.Verify home page for checkout process
    Wait until page is completely loaded
    checkout_page.Verify cart merged for checkout process    ${1}

Guest checkout flow: Log in with facebook from Register page
    [Tags]    checkout1    regression    checkout_process   testrailid:C192700
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    common_cds_mobile_web.Navigate to Login page
    registration_page.Header Web - Click Facebook login Button
    common_cds_web.Switch to new window
    registration_page.Input Email With Facebook  ${facebook_email}[facebook_email_test_1]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    common_cds_web.Switch to main window
    registration_page.Redirect home page
    checkout_page.Verify home page for checkout process
    Wait until page is completely loaded
    checkout_page.Verify cart merged for checkout process    ${1}

Guest checkout flow: Log in from Login bubble
    [Tags]    checkout1    regression    checkout_process   testrailid:C192701
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    common_cds_mobile_web.Navigate to Login page
    checkout_page.Input member email    ${cds_checkout_process_username}
    checkout_page.Input member password    ${cds_checkout_process_password}
    checkout_page.Click member login
    checkout_page.Verify home page for checkout process
    Wait until page is completely loaded
    checkout_page.Verify cart merged for checkout process    ${1}

Guest checkout flow: Log in with facebook from Login bubble
    [Tags]    checkout1    regression    checkout_process   testrailid:C192702
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    common_cds_mobile_web.Navigate to Login page
    registration_page.Header Web - Click Facebook login Button
    common_cds_web.Switch to new window
    registration_page.Input Email With Facebook  ${facebook_email}[facebook_email_test_1]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button

Guest checkout flow: Clicini Cart when cart is empty
    [Tags]    checkout1    regression    checkout_process   testrailid:C192703
    Wait Until Page Is Completely Loaded
    checkout_page.click mini cart icon
    checkout_page.Verify member login botton

Guest checkout flow: Click on Mini Cart when cart is not empty
    [Tags]    checkout1    regression    checkout_process   testrailid:C192704
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    checkout_page.click mini cart icon

Guest checkout flow: On Shopping Bag page, click on SECURE CHECKOUT button
    [Tags]    checkout1    regression    checkout_process   testrailid:C192705
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Verify log in box text displayed on delivery page

Guest checkout flow: Guest-Login page, click on CHECKOUT AS GUEST button
    [Tags]    checkout1    regression    checkout_process   testrailid:C192706
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    checkout_page.Check Out Page Should Be Visible
    thankyou_page.Click guest login checkout Thank you page
    guest_login_page.Click login as guest
    
Guest checkout flow: Guest-Login page, log in as member
    [Tags]    checkout1    regression    checkout_process   testrailid:C192707
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    checkout_page.Check Out Page Should Be Visible
    thankyou_page.Click guest login checkout Thank you page
    guest_login_page.Input username, password and click login button at guest login page    ${cds_checkout_process_username}     ${cds_checkout_process_password}
    checkout_page.Verify Delivery Details page should be displayed

Guest checkout flow: Guest-Login page, log in with facebook
    [Tags]    checkout1    regression    checkout_process   testrailid:C192708
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[3]
    Wait until page is completely loaded
    checkout_page.Check Out Page Should Be Visible
    thankyou_page.Click guest login checkout Thank you page
    registration_page.Header Web - Click Facebook login Button
    common_cds_web.Switch to new window
    registration_page.Input Email With Facebook  ${facebook_email}[facebook_email_test_1]
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    common_cds_web.Switch to main window
    registration_page.Redirect home page

To verify that Out-of-stock SKUs (free items) should not be displayed on the Checkout
    [Tags]    testrailid:C1014500    testrailid:C1012970    Omni_checkout_process
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with standard pickup    ${e2e_username_10}    ${e2e_password_10}
    ...    ${lst_skus_standard_pickup_sku_and_standard_delivery_sku}    ${quantities}    ${shipping_address_en.store_location_id}    ${store}
    ${free_item_skus}    product_api.Get free item sku in cart    ${user_token}
    checkout_page.Select pick at store option with omni design 
    ${store_data}=    delivery_details_keywords.Click standard pickup active store and return store data    ${response}    ${api_ver3}   
    Wait Until Page Loader Is Not Visible
    checkout_page.Verify product view is not visible in order summary    ${free_item_skus}    