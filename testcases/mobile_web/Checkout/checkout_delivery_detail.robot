*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    CommonWebKeywords.Test Teardown
Suite Teardown    Close All Browsers

*** Variables ***
${login_username}    ${personal_checkout_delivery.username}
${login_password}    ${personal_checkout_delivery.password}
${login_display_name}    ${personal_checkout_delivery.firstname}
${sku_with_2hr}    CDS11473459
${sku_without_2hr}    CDS14488986
${sku_quantity}    2
@{lst_split_skus}    ${sku_list_with_2hr}[0]    ${sku_list_without_2hr}[0]
@{lst_qtys}    2    2
${store}=    ${store_view.name.en}
${api_version}=    ${api_ver3}

*** Keywords ***
Go to checkout page and click continue to payment button
    E2E_flow_keywords.Go to checkout page
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_page.Click Continue Payment Button

Test setup - verify warning messge
    common_cds_mobile_web.Setup - Open browser for mobile web
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    delivery_details_page.Click request tax invoice

Template - verify warning message for ID card of tax invoice
    [Arguments]    ${test_case_name}    ${invalid_id_card}    ${warning_message}
    delivery_details_page.Input ID card for tax invoice info    ${invalid_id_card}
    delivery_details_page.Verify warning message is displayed if input invalid personal ID card for tax invoice    ${warning_message}

Template - verify warning message for tax ID of tax invoice
    [Arguments]    ${test_case_name}    ${invalid_tax_id}    ${warning_message}
    delivery_details_page.Click billing invoice option
    delivery_details_page.Input tax ID for tax invoice info    ${invalid_tax_id}
    delivery_details_page.Verify warning message is displayed if input invalid company tax ID for tax invoice    ${warning_message}

Test setup - register new account
    common_cds_mobile_web.Setup - Open browser for mobile web
    common_cds_mobile_web.Navigate To Register Page
    registration_page.Input Firstname    ${delivery_registration_info.firstname}
    registration_page.Input Lastname    ${delivery_registration_info.lastname}
    registration_page.Input Email    ${delivery_registration_info.username}
    registration_page.Input Password For Register Page    ${delivery_registration_info.password}
    registration_page.Click Submit Button
    registration_keywords.verify registration success message after register

Test teardown - remove registration account
    customer_api.Delete registeration account from mdc    ${delivery_registration_info.username}    ${delivery_registration_info.password}
    CommonWebKeywords.Test Teardown

Test teardown - Remove address from address book
    go To Direct Url    account/address
    my_account_page.Delete address except the default address

*** Test Cases ***
To verify profile account, shipping address, shipping option and payment summary on delivery page should be display correctly
    [Tags]    regression    checkout_delivery    sameday_delivery    testrailid:C290458    testrailid:C290492
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_keywords.Verify delivery shipping option are displayed correctly
    checkout_keywords.Verify default shipping address is displayed correctly    ${login_username}    ${login_password}
    checkout_keywords.Verify standard delivery shipping price by relate to subtotal
    checkout_keywords.Verify same day delivery shipping price are display correctly
    checkout_keywords.Verify next day delivery shipping price are display correctly
    Click edit bag link to go to shopping bag page and verify the page is displayed
    Go to checkout page and click continue to payment button
    checkout_page.Payment Page Should Be Displayed

To verify that user clicks "Edit" in each address
    [Tags]    regression    checkout_delivery    testrailid:C290480
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_page.Click change shipping address link
    change_shipping_address_popup.Verify change shipping address popup should be visible
    change_shipping_address_popup.Click edit shipping address by address id    ${member_profile}[address_id]
    Update new shipping address info    ${change_shipping_address}
    change_shipping_address_popup.Click save address button
    change_shipping_address_keywords.Verify shipping address has been updated correctly    ${login_username}    ${login_password}
    change_shipping_address_popup.Click use selected address button
    checkout_keywords.Verify default shipping address is displayed correctly    ${login_username}    ${login_password}

To verify that admin is able to enable multiple delivery methods
    [Tags]    regression    checkout_delivery    testrailid:C290486
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[20]
    checkout_page.Select shipping to address option
    checkout_keywords.Verify delivery shipping option are displayed correctly
    checkout_page.Next-day Delivery Should Not Be Visible
    checkout_page.Verify pickup at store delivery option is displayed in delivery details page
    #checkout_page.Verify pickup at skybox delivery option is displayed in delivery details page

To verify shipping cost when shipping method have shipping rules applied can calculated and displayed correctly
    [Tags]    regression    checkout_delivery    testrailid:C290494
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_keywords.Verify standard delivery shipping price by relate to subtotal
    Click edit bag link to go to shopping bag page and verify the page is displayed
    my_cart_page.Remove product from shopping bag    ${product_sku_list}[2]
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[0]
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_keywords.Verify standard delivery shipping price by relate to subtotal

To verify that Cut-Off Time configuration works correctly
    [Tags]    regression    checkout_delivery    testrailid:C290511
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_keywords.Verify delivery shipping option are displayed correctly

To verify the delivery Options section
    [Tags]    regression    checkout_delivery    testrailid:C290498
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    checkout_keywords.Verify customer profile display information correctly
    checkout_keywords.Verify delivery options are displayed correctly
    checkout_keywords.Verify there is no any delivery option is selected
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_keywords.Verify delivery shipping option are displayed correctly
    checkout_page.Standard shipping address is default Selected

To verify that delivery method will not be available when there is no available common delivery method in the basket
    [Tags]    regression    checkout_delivery    testrailid:C290490
    Login, Add multiple product to Cart and Go to Checkout page    ${login_username}    ${login_password}    ${login_display_name}    ${product_sku_list}[11]    ${product_sku_list}[38]
    checkout_page.Standard delivery should not be visible
    checkout_page.Same-day delivery should not be visible
    checkout_page.Next-day delivery should not be visible
    checkout_page.Pickup at store delivery option should not be visible
    checkout_page.Pickup at skybox and family mart delivery option should not be visible
    checkout_page.Delivery option not available message should be displayed

To verify Member checkout without full tax invoice
    [Tags]    regression    checkout_delivery    testrailid:C306632
    Login, Add product to Cart and Go to Checkout page    ${personal_no_taxinvoice.username}    ${personal_no_taxinvoice.password}    ${personal_no_taxinvoice.firstname}    ${product_sku_list}[2]
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Verify there is no default tax invoice display if customer has no tax invoice information setting    ${personal_no_taxinvoice.username}    ${personal_no_taxinvoice.password}

To verify Member checkout with a default personal full tax invoice address
    [Tags]    regression    checkout_delivery    testrailid:C306633
    Login, Add product to Cart and Go to Checkout page    ${personal_taxinvoice_personal.username}    ${personal_taxinvoice_personal.password}    ${personal_taxinvoice_personal.firstname}    ${product_sku_list}[2]
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Verify default tax invoice is displayed consistency with MDC    ${personal_taxinvoice_personal.username}    ${personal_taxinvoice_personal.password}    ${tax_type_personal}

To verify Member checkout with a default company full tax invoice address
    [Tags]    regression    checkout_delivery     testrailid:C306634
    Login, Add product to Cart and Go to Checkout page    ${personal_taxinvoice_company.username}    ${personal_taxinvoice_company.password}    ${personal_taxinvoice_company.firstname}    ${product_sku_list}[2]
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Verify default tax invoice is displayed consistency with MDC    ${personal_taxinvoice_company.username}    ${personal_taxinvoice_company.password}    ${tax_type_company}

To verify Guest checkout without full tax invoice
    [Tags]    regression    checkout_delivery    branch9    testrailid:C306629
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    delivery_details_keywords.Verify guest is no contact info display
    checkout_page.Select shipping to address option
    delivery_details_keywords.Verify guest is no shipping address info display
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Verify guest is no tax invoice address info display

To verify Guest checkout with personal full tax invoice address
    [Tags]    regression    checkout_delivery    branch9    testrailid:C306630
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    delivery_details_keywords.Input cutomer contact information
    checkout_page.Select shipping to address option
    delivery_details_keywords.Input shipping address information
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Input tax invoice address information    ${tax_type_personal}
    checkout_page.Click on standard delivery button
    delivery_details_page.Click continue to payment button
    payment_page.Click on view details on mobile
    payment_keywords.Verify shipping address is display on shipping summary correctly
    payment_keywords.Verify tax invoice address is display on shipping summary correctly
    payment_page.Close view deatails on mobile
    checkout_page.Payment Page Should Be Displayed
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    Verify shipping address in thank you page is dispayed consistency with MDC
    Verify billing address in thank you page is dispayed consistency with MDC    ${tax_type_personal}
    thankyou_keywords.Verify shipping address in thank you page is displayed correctly    ${thankyou_page}[standard_delivery]
    thankyou_keywords.Verify billing address in thank you page is displayed correctly for personal

To verify the personal ID card for require Tax Invoice
    [Tags]    regression    checkout_delivery    branch9    testrailid:C306637
    [Setup]    Test setup - verify warning messge
    [Template]    Template - verify warning message for ID card of tax invoice
    To verify ID card is less than 13 digits    133162215438    ${delivery_details_page.invalid_id_card}
    To verify ID card start with zero    0331622154385    ${delivery_details_page.invalid_id_card}
    To verify ID card has white space    ${SPACE}133162215438    ${delivery_details_page.invalid_id_card}
    To verify ID card is invalid format   1234567890123    ${delivery_details_page.invalid_id_card}
    To verify ID card is require    ${EMPTY}    ${delivery_details_page.require_field}

To verify the company ID for require Tax Invoice
    [Tags]    regression    checkout_delivery    branch9    testrailid:C306638
    [Setup]    Test setup - verify warning messge
    [Template]    Template - verify warning message for tax ID of tax invoice
    To verify tax ID is less than 10 digits    123456789    ${delivery_details_page.invalid_tax_id}
    To verify tax ID start with zero    ${SPACE}12345678    ${delivery_details_page.invalid_tax_id}
    To verify tax ID is require    ${EMPTY}    ${delivery_details_page.require_field}

To verify tax invoice and shipping address is not whitespace (Incase Address is exist) for member
    [Tags]    regression    checkout_delivery    branch9    testrailid:C355873    testrailid:C355874    testrailid:C355875
    Login, Add product to Cart and Go to Checkout page    ${white_space_info.username}    ${white_space_info.password}    ${white_space_info.firstname}    ${product_sku_list}[2]
    delivery_details_page.Click request tax invoice
    delivery_details_page.Click change tax invoice address link
    change_shipping_address_popup.Verify change shipping address popup should be visible
    change_shipping_address_popup.Verify address is not allowed to selected if there is white space    ${white_space_info.username}    ${white_space_info.password}    ${address_type_billing}

To verify that house no of shipping address is not whitespace (Incase Address is exist) for member
    [Tags]    regression    checkout_delivery    branch9    testrailid:C355845
    Login, Add product to Cart and Go to Checkout page    ${white_space_info.username}    ${white_space_info.password}    ${white_space_info.firstname}    ${product_sku_list}[2]
    checkout_page.Select shipping to address option
    delivery_details_page.Click select shipping address link
    change_shipping_address_popup.Verify change shipping address popup should be visible
    change_shipping_address_popup.Verify address is not allowed to selected if there is white space    ${white_space_info.username}    ${white_space_info.password}    ${address_type_shipping}

To verify Guest checkout with company full tax invoice address
    [Tags]    regression    checkout_delivery    branch9    testrailid:C306631
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    delivery_details_keywords.Input cutomer contact information
    checkout_page.Select shipping to address option
    delivery_details_keywords.Input shipping address information
    delivery_details_page.Click request tax invoice
    delivery_details_page.Click billing invoice option
    delivery_details_keywords.Input tax invoice address information    ${tax_type_company}
    checkout_page.Click on standard delivery button
    delivery_details_page.Click continue to payment button
    payment_page.Click on view details on mobile
    payment_keywords.Verify shipping address is display on shipping summary correctly
    payment_keywords.Verify tax invoice address is display on shipping summary correctly
    Close view deatails on mobile
    checkout_page.Payment Page Should Be Displayed
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer
    thankyou_keywords.Verify shipping address in thank you page is dispayed consistency with MDC
    thankyou_keywords.Verify billing address in thank you page is dispayed consistency with MDC    ${tax_type_company}
    thankyou_keywords.Verify shipping address in thank you page is displayed correctly    ${thankyou_page}[standard_delivery]
    thankyou_keywords.Verify billing address in thank you page is displayed correctly for company

To verify that admin is able to enable single delivery method
    [Tags]    regression    checkout_delivery    sameday_delivery    testrailid:C290485    testrailid:C290487    testrailid:C290488
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    checkout_keywords.Verify delivery options are displayed correctly
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_keywords.Verify delivery shipping option are displayed correctly
    checkout_keywords.Verify default shipping address is displayed correctly    ${login_username}    ${login_password}
    checkout_keywords.Verify standard delivery shipping price by relate to subtotal
    checkout_keywords.Verify same day delivery shipping price are display correctly
    checkout_keywords.Verify next day delivery shipping price are display correctly
    checkout_page.Verify pickup at store delivery option is displayed in delivery details page
    # checkout_page.Verify pickup at skybox delivery option is displayed in delivery details page

To verify that only delivery methods that available in each basket will available at checkout page
    [Tags]    regression    checkout_delivery    testrailid:C290489
    Login, Add multiple product to Cart and Go to Checkout page    ${login_username}    ${login_password}    ${login_display_name}    ${product_sku_list}[0]    ${product_sku_list}[20]
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_keywords.Verify Same Day shipping option is not displayed in disable time setting
    checkout_page.Next-day delivery should not be visible
    checkout_page.Verify pickup at store delivery option is displayed in delivery details page
    checkout_page.Pickup at skybox and family mart delivery option should not be visible

To verify that text is displayed in shipping method correctly
    [Tags]    regression    checkout_delivery    branch9    testrailid:C954979    testrailid:C954980
    Login Keywords for mobile web - Login Success  ${white_space_info.username}    ${white_space_info.password}    ${white_space_info.firstname}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[4]
    checkout_keywords.Verify delivery options are displayed correctly
    delivery_details_page.Verify pickup at store text is displayed correctly

To verify Member checkout with a new selecting company full tax invoice address
    [Tags]    regression    checkout_delivery    branch9    testrailid:C306636    testrailid:C243975
    Test setup - register new account
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Verify there is no default tax invoice display if customer has no tax invoice information setting    ${delivery_registration_info.username}    ${delivery_registration_info.password}
    delivery_details_page.Click change tax invoice address link
    Update new shipping address info    ${tax_invoice_address}
    change_shipping_address_popup.Click request tax invoice option
    change_shipping_address_popup.Select tax invoice for company
    change_shipping_address_popup.Input company name  ${tax_invoice_address}[company_name]
    change_shipping_address_popup.Input tax ID  ${tax_invoice_address}[tax_id]
    change_shipping_address_popup.Input branch ID  ${tax_invoice_address}[branch_id]
    change_shipping_address_popup.Select default billing address option
    change_shipping_address_popup.Click save address button
    change_shipping_address_popup.Select address to be a default
    change_shipping_address_popup.Click use selected address button
    delivery_details_keywords.Verify default tax invoice is displayed consistency with MDC    ${delivery_registration_info.username}    ${delivery_registration_info.password}    ${tax_type_company}
    [Teardown]    Test teardown - remove registration account

To verify Member checkout with a new selecting personal full tax invoice address
    [Tags]    regression    checkout_delivery    branch9    testrailid:C306635
    Test setup - register new account
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    delivery_details_page.Click request tax invoice
    delivery_details_keywords.Verify there is no default tax invoice display if customer has no tax invoice information setting    ${delivery_registration_info.username}    ${delivery_registration_info.password}
    delivery_details_page.Click change tax invoice address link
    Update new shipping address info    ${tax_invoice_address}
    change_shipping_address_popup.Click request tax invoice option
    change_shipping_address_popup.Select tax invoice for personal
    change_shipping_address_popup.Input ID card  ${tax_invoice_address}[id_card]
    change_shipping_address_popup.Select default billing address option
    change_shipping_address_popup.Click save address button
    change_shipping_address_popup.Select address to be a default
    change_shipping_address_popup.Click use selected address button
    delivery_details_keywords.Verify default tax invoice is displayed consistency with MDC    ${delivery_registration_info.username}    ${delivery_registration_info.password}    ${tax_type_personal}
    [Teardown]    Test teardown - remove registration account

To verify that the delivery fee and the gift wrapping fee must be calculated in order total
    [Tags]    regression    checkout_delivery    testrailid:C290497
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[0]    1    ${True}
    checkout_page.Click gift wrapping checkbox
    E2E_flow_keywords.Go to checkout page
    checkout_page.Select shipping to address option
    Click on view details on mobile
    checkout_keywords.Verify subtotal in order summary is calculated correctly  ${product_sku_list}[0]
    checkout_keywords.Verify grand total in order summary is calculated correctly without any discount  ${delivery_type}[shipping_standard]

To Verify The Checkout Delivery With Click And Collect Show 2HR Pickup Flow
    [Tags]    checkout    Omni_checkout_delivery    testrailid:C963390    testrailid:C963391    testrailid:C963392    testrailid:C963393
    ...    testrailid:C963395    testrailid:C963396    testrailid:C963397    testrailid:C963398    testrailid:C1010754    testrailid:C1010757    testrailid:C1010758
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with 2hr pickup    ${e2e_username_10}    ${e2e_password_10}
    ...    ${sku_list_with_2hr}[1]    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.Verify the store data display correctly with show 2hr pickup flow    ${store_data}    ${sku_quantity}

To Verify The Checkout Delivery With Click And Collect With Out 2HR Pickup Flow
    [Tags]    Omni_checkout_delivery    testrailid:C984848    testrailid:C984849    testrailid:C984850    testrailid:C984851
    ...    testrailid:C984852    testrailid:C984855    testrailid:C984856    testrailid:C984857    testrailid:C984858
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with standard pickup    ${e2e_username_10}    ${e2e_password_10}
    ...    ${sku_list_without_2hr}[0]    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.Verify the store data display correctly without 2hr pickup flow    ${store_data}    ${sku_quantity}

To Verify The Checkout Delivery With Split Order - Select Store Having 2 Hours
    [Tags]      2hrpickup    Omni_checkout_delivery
    ...    testrailid:C963399    testrailid:C963400    testrailid:C963401    testrailid:C963402
    ...    testrailid:C963403    testrailid:C963404    testrailid:C963405    testrailid:C963406
    ...    testrailid:C963407    testrailid:C963409    testrailid:C963410    testrailid:C963411
    ...    testrailid:C963223    testrailid:C963224    testrailid:C963225    testrailid:C963226
    ...    testrailid:C963228    testrailid:C963229    testrailid:C963230    testrailid:C963231
    ...    testrailid:C963232    testrailid:C963235    testrailid:C963236    testrailid:C963237
    ...    testrailid:C963238    testrailid:C963239    testrailid:C963240    testrailid:C963241
    ...    testrailid:C963242    testrailid:C963243    testrailid:C963244    testrailid:C963247
    ...    testrailid:C963248    testrailid:C963250    testrailid:C963251    testrailid:C963252
    ...    testrailid:C963253    testrailid:C963285    testrailid:C963319    testrailid:C963320
    ...    testrailid:C1010759    testrailid:C1010760    testrailid:C1010761    testrailid:C1010895
    ...    testrailid:C1014441    testrailid:C1014443    testrailid:C1014444    testrailid:C1014445
    ...    testrailid:C1010899    testrailid:C1010901       testrailid:C1029216
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.Verify the store data display correctly with split order    ${store_data}    ${lst_qtys}

To Verify The Checkout Delivery With Split Order - Select Store Not Having 2 Hours
    [Tags]    Omni_checkout_delivery    testrailid:C994832
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_11}    ${e2e_password_11}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click standard pickup active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.Verify the store data display correctly with standard pickup of split order   ${store_data}    ${lst_qtys}

To Verify The Checkout Delivery Select Pickup Store Without Phone Number
    [Tags]    Omni_checkout_delivery    testrailid:C980606
    ...     testrailid:C986515    testrailid:C986516    testrailid:C986517    testrailid:C986528    testrailid:C980587    testrailid:C980595    testrailid:C980591
    ...    testrailid:C980588    testrailid:C980589    testrailid:C980590    testrailid:C991482    testrailid:C1010746    testrailid:C1010748    testrailid:C1010749    testrailid:C1010750
    ...    testrailid:C990899    testrailid:C990908    testrailid:C990912    testrailid:C990913    testrailid:C990914    testrailid:C1010894
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with standard pickup    ${e2e_username_11}    ${e2e_password_11}
    ...    ${sku_list_without_2hr}[0]    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    delivery_details_keywords.Verify the store phone number, user location and postcode search
    delivery_details_keywords.Verify cannot select store if dont enter the phone number
    delivery_details_keywords.Verify cannot select store if enter the phone number incorrectly    ${e2e_information_10.tel}
    delivery_details_keywords.Verify select store properly if enter the phone number correctly    ${e2e_information_10.tel}
    delivery_details_page.Verify the continue payment button disable
    ${store_data}=    delivery_details_keywords.Click active store and return store data    ${response}    ${api_ver3}
    delivery_details_page.Verify the continue payment button enable

To Verify Shipping Address - Member Check Out With Split Order
    [Tags]    Omni_checkout_delivery    testrailid:C980573    testrailid:C980602    testrailid:C980603
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.Click standard delivery option
    delivery_details_keywords.Verify default address is display correctly
    checkout_page.Click change shipping address link
    delivery_details_keywords.Click edit shipping address
    delivery_details_keywords.Verify shipping address field are display correctly
    delivery_details_keywords.Click back to adrress list button
    delivery_details_page.Click add shipping address
    delivery_details_keywords.Verify shipping address field are display correctly

To Verify Shipping Address - Guest Check Out With Split Order
    [Tags]    Omni_checkout_delivery    testrailid:C980604    testrailid:C980605
    ${quote_id}=    common_keywords.Get Cookie Value From Browser    guest
    cart.Guest add items to cart   ${sku_list_with_2hr}[0]    ${lst_qtys}[0]    ${quote_id}    ${store}
    cart.Guest add items to cart    ${sku_list_without_2hr}[0]    ${lst_qtys}[1]    ${quote_id}    ${store}
    common_keywords.Go To Specific Page By Guest    checkout
    checkout_page.Select pick at store option with omni design
    delivery_details_page.Click random the 2hr pickup store and return name
    delivery_details_keywords.Click standard delivery option
    delivery_details_keywords.Verify shipping address field are display correctly with guest

To Verify The Free Item Display With Split Order
    [Tags]    Omni_checkout_delivery    testrailid:C1010878
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with 2hr pickup    ${e2e_username_10}    ${e2e_password_10}
    ...    ${sku_list_with_2hr}[2]    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.Verify the store data display correctly with show 2hr pickup flow    ${store_data}    ${sku_quantity}

To Verify Three Hour Delivery Option With Member - Default Shipping Method
    [Tags]    Omni_checkout_delivery    3hrdelivery    Omni_smoke    testrailid:C1032037    testrailid:C1032038    testrailid:C1032298    testrailid:C1032299    testrailid:C1032301    testrailid:C1032302    testrailid:C1032306    testrailid:C1032308
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_09}    ${e2e_password_09}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Click on home delivery option
    delivery_details_keywords.Verify home delivery shipping option is not selected
    delivery_details_keywords.Member verify pin location section is not active correctly
    delivery_details_keywords.Member create new shipping address
    delivery_details_keywords.Member select shipping address
    delivery_details_page.Verify the continue payment button disable
    ${customer_detail}    Get customer details    ${user_token}
    ${latitude}    ${longitude}    delivery_details_page.Member get latitude and longitude    ${customer_detail}    ${postcode_storage}
    ${flag}    cart.Member validate shipping address by pin location    ${user_token}    ${store}    ${latitude}    ${longitude}
    delivery_details_keywords.Verify 3 hour delivery option display correctly with postcode    ${flag}
    checkout_keywords.Verify 3 hour delivery shipping price are display correctly
    delivery_details_keywords.Member pin location address display correctly after pin location
    delivery_details_page.Verify the continue payment button enable
    delivery_details_keywords.Member verify location is not within the selected district    ${post_code_list}[3]
    [Teardown]    Test teardown - Remove address from address book

To Verify Three Hour Delivery Option With Guest
    [Tags]    Omni_checkout_delivery    3hrdelivery    Omni_smoke
    ${quote_id}    delivery_details_keywords.Add product to cart and go to checkout delivery page with guest    ${lst_split_skus}    ${lst_qtys}    ${store}
    delivery_details_keywords.Input cutomer contact information
    checkout_page.Click on home delivery option
    delivery_details_page.Guest verify pin location is not active correctly
    delivery_details_keywords.Guest input shipping address information
    delivery_details_keywords.Verify home delivery shipping option is not selected
    delivery_details_page.Guest get shipping address information
    delivery_details_page.Guest verify pin location is active correctly
    delivery_details_page.Guest click pin location button
    delivery_details_page.Verify pin your location popup display correctly
    delivery_details_page.Click pin location confirm button
    ${latitude}    ${longitude}    delivery_details_page.Guest get latitude and longitude
    ${flag}    cart.Guest validate shipping address by pin location    ${quote_id}    ${store}    ${latitude}    ${longitude}
    delivery_details_keywords.Verify 3 hour delivery option display correctly with postcode    ${flag}
    checkout_keywords.Verify 3 hour delivery shipping price are display correctly
    delivery_details_page.Verify the continue payment button disable
    delivery_details_keywords.Guest pin location address display correctly after pin location
    delivery_details_page.Verify the continue payment button enable