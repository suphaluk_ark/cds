*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Variables ***
${login_username}    ${cds_checkout_thank_you_username}
${login_password}    ${cds_checkout_thank_you_password}
${login_display_name}    ${cds_checkout_thank_you_information.firstname}
${product_type_sku}=    ${product_sku_list}[0]
${sku_quantity}    1
@{lst_split_skus}    ${sku_list_with_2hr}[1]    ${sku_list_without_2hr}[0]
@{lst_split_skus_std}    ${product_sku_list}[12]    ${sku_list_with_2hr}[0]
@{lst_split_full_redeem_skus}    ${sku_for_reddem}[0]    ${sku_for_reddem}[1]
@{list_split_with_mkp_sku}    ${sku_for_reddem}[0]    ${sku_list_mkp}[0]
@{lst_qtys}    1    1
${store}=    ${store_view.name.en}
${api_version}=    ${api_ver3}
${invalid_pass_no_number}     Robotabcd@
${invalid_pass_lowercase}     robotabcd@
${invalid_pass_uppercase}     ROBOT@12345
${invalid_pass_less_8_char}     Ro@1
${valid_pass}    Ro@123456

*** Keywords ***
Template - Go To Thank You Page And Check Input Password Field
    [Arguments]    ${password}    ${err_msg}    ${store}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    Go To Direct Url    checkout/completed/${increment_id}[0]
    common_keywords.Wait Until Page Is Completely Loaded
    thankyou_page.Input password personal details for the registration    ${password}
    thankyou_page.Verify password input field should display error message    ${err_msg}

Template - To verify 2Hr Pickup Omnichannel checkout flow on UI
    [Arguments]    ${payment_method}
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${sku_list_with_2hr}[1] 
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup   ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_method}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_method}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    Verify each section on Thank you page displays correctly

Template - To verify Home Delivery on thank you page
    [Arguments]    ${shipping_method}    ${shipping_carrier}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${sku_list_with_2hr}[0]
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_carrier}    ${shipping_method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_method}    ${shipping_carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    Verify each section on Thank you page displays correctly

*** Test Cases ***
Verify that complete page by guest show section correctly
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80263
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    &{change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    checkout_keywords.Checkout banktranfers for guest member

To validate CREATE YOUR CENTRAL ACCOUNT NOW section show detail correctly
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80264
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    &{change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfers for guest member

Verify that values display on REGISTER section
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80265
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member

Verify that complete page by member show section correctly
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83467
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    thankyou_page.Click guest login checkout Thank you page
    thankyou_page.Login User for checkout Thank you page   ${login_username}    ${login_password}
    checkout_page.Select shipping to address option
    checkout_page.Click on Same-day delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify Three Icon Status On Thank You Page
    thankyou_page.Verify ordered successfully messenger
    thankyou_page.Verify order information form thank you page

Verify that Full Tax Invoice Details show correctly as guest input information on checkout page
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83106
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    delivery_details_keywords.Input cutomer contact information
    checkout_page.Select shipping to address option
    delivery_details_keywords.Input shipping address information
    delivery_details_page.Click request tax invoice
    delivery_details_page.Click billing invoice option
    delivery_details_keywords.Input tax invoice address information    ${tax_type_company}
    checkout_page.Click on standard delivery button
    delivery_details_page.Click continue to payment button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Click connect to T1C link
    checkout_page.Login to T1C account    ${t1c_information_2}[email]    ${t1c_information_2}[password]
    checkout_page.Click apply point after login T1C
    checkout_page.Input T1C point    ${200}
    checkout_page.Click apply T1C point
    checkout_keywords.Checkout banktranfer for guest member

To validate order information show information correctly incase pay by full Redeem point
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80233    full_redeem
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Click apply point after login T1C
    checkout_page.Input T1C point    ${200}
    checkout_page.Click apply T1C point
    payment_keywords.Thank you page payment with credit card

To validate order information show information correctly incase pay by Redeem point and credit card
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83145
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Click apply point after login T1C
    checkout_page.Input T1C point    ${50}
    checkout_page.Click apply T1C point
    payment_keywords.Thank you page payment with credit card

To validate order information show information correctly incase pay by Redeem point and Bank Transfer
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83153
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Click apply point after login T1C
    checkout_page.Input T1C point    ${25}
    checkout_page.Click apply T1C point
    checkout_keywords.Checkout banktranfer for guest member

To validate order information show information correctly incase pay by Credit Card
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83162
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_keywords.Thank you page payment with credit card
    thankyou_page.Verify order information form thank you page

To validate order information show information correctly incase pay by Bank Transfer
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83163
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify that Payment summary show information correctly if guest user promo code, gift option, T1C redemption and pickup at store
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83389
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Input T1C point    ${t1c_information_2.point}
    checkout_page.Click apply T1C point
    payment_keywords.Thank you page payment with credit card

Verify that Promo code field is hided if customer have no promo code
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83419
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify that Gift Options field is hided if customer doesn't check on gift wrapping
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83420
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select shipping to address option
    change_shipping_address_popup.Input building to update shipping address    ${shipping_address_Guest_1}[building]
    change_shipping_address_popup.Input address no to update shipping address    ${shipping_address_Guest_1}[address]
    change_shipping_address_popup.Input postcode to update shipping address    ${shipping_address_Guest_1}[zip_code]
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify that T1C Redemption field is hided if customer doesn't pay with T1C redeem points
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83421
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify standard Delivery method show correctly based on customer selected
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C83422
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    checkout_page.Check Out Page Should Be Visible
    checkout_keywords.Input guest infomation at checkout page
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    payment_keywords.Thank you page payment with credit card

Verify Next-day Delivery method show correctly based on customer selected
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C359600
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[5]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_keywords.Select Same day or Next day delivery shipping method
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify Same-day Delivery method show correctly based on customer selected
    [Tags]    regression    checkout1   sameday_delivery    checkout_thankyou    testrailid:C359603
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_keywords.Add shipping address with guest
    checkout_page.Click on Same-day delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify Pickup at Store method show correctly based on customer selected
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C359604
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_keywords.Thank you page payment with credit card

Verify that information of pick up at store show on complete page correctly
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80273
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
     checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify that Collector info show correctly and match with information of guest
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80272
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store.central_bangna}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_keywords.Thank you page payment with credit card

Verify that Pickup Location section show information correctly based on guest select from checkout page
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80274
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${guest_information_1.email}
    registration_page.Input guest telephone    ${guest_information_1}[tel]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_keywords.Thank you page payment with credit card

Verify that purchased items show correctly
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80266
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Input T1C point     ${200}
    checkout_page.Click apply T1C point
    payment_keywords.Thank you page payment with credit card

Verify that purchased items show correctly if have gift wrapping
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80267
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[0]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    registration_page.Input T1C number    ${t1c_information_2.t1c_number}
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Connect The 1 Card Account
    checkout_page.Login to T1C account    ${t1c_information_2.email}    ${t1c_information_2.password}
    checkout_page.Input T1C point     ${200}
    checkout_page.Click apply T1C point
    payment_keywords.Thank you page payment with credit card

Verify that purchased items show correctly if have free gift with this purchase
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80268    testrailid:C80271
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[21]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
     my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify that purchased items show correctly if have free gift with this purchase and free item price is zero
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80269
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[21]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
     my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify that purchased items show correctly if have free item for whole cart
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80270
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[21]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
     my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

Verify that purchased items show correctly if have free item for whole cart and free item price is zero
    [Tags]    regression    checkout1   checkout_thankyou    testrailid:C80271
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[21]
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]
    checkout_page.Select pick at store option
    checkout_page.Select store    ${pick_at_store_2.central_chidlom_ploenchit}
    checkout_page.Click on select this store button
    checkout_page.Click Continue Payment Button
     my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Checkout banktranfer for guest member
    thankyou_page.Verify order information form thank you page

To Verify That Use Same Validation For Email And Password That We Use On Current Product Website.
    [Tags]    checkout_thankyou    Omni_checkout_delivery    testrailid:C980381    testrailid:C980393
    [Template]    Template - Go To Thank You Page And Check Input Password Field
    ${invalid_pass_no_number}     ${error_message.guest_login.invalid_password}    ${store}
    ${invalid_pass_lowercase}     ${error_message.guest_login.invalid_password}    ${store}
    ${invalid_pass_uppercase}     ${error_message.guest_login.invalid_password}    ${store}
    ${invalid_pass_less_8_char}   ${error_message.guest_login.invalid_password}    ${store}

To Verify That Register Fail When Email Has Already Register
    [Tags]    checkout_thankyou    Omni_checkout_delivery    testrailid:C980382    testrailid:C980387    testrailid:C980385    testrailid:C980387    testrailid:C980392    testrailid:C980383
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Verify create account section are display correctly
    thankyou_page.Input password personal details for the registration    ${valid_pass}
    thankyou_page.Click button register for guest
    thankyou_page.Verify the duplicate email message should be visible    ${error_message.account_registration.email_existed}
    thankyou_page.Click on continue shopping button
    home_page_web_keywords.Verify homepage page should be displayed

To Verify Create Account Successfully
    [Tags]    checkout_thankyou    Omni_checkout_delivery    testrailid:C980384    testrailid:C980386    testrailid:C980389    testrailid:C980390    testrailid:C980396
    ...    testrailid:C980395    testrailid:C980397
    ${checkout_delivery_username}=    FakerLibrary.Email
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${checkout_delivery_username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${checkout_delivery_username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id} 
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Verify the order is completed
    thankyou_page.Verify the each section are display correctly    ${thankyou_page.status_awaiting_payment}    ${thankyou_page.payment_transfer}
    thankyou_page.Input password personal details for the registration    ${valid_pass}
    thankyou_page.Click button register for guest
    thankyou_page.Verify account create successfully   ${success_message.thank_you.create_account_success}

To Verify Payment Failed Page By Credit Cart
    [Tags]    checkout_thankyou    Omni_checkout_delivery   Omni_smoke    testrailid:C990808    testrailid:C990807    testrailid:C980473    testrailid:C990811
    ...    testrailid:C1010927
    ${response}    ${user_token}    Login, add product to cart, return and go to checkout delivery page with 2hr pickup    ${login_username}    ${login_password}
    ...    ${product_type_sku}    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Click on cancel payment button
    checkout_page.Verify section on payment failed page after click cancel otp
    checkout_page.Click pay now button at payment failed page
    checkout_page.Verify Repayment page is displayed
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    thankyou_page.Verify the each section are display correctly    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order By Credit Cart
    [Tags]    checkout_thankyou    Omni_checkout_delivery   Omni_smoke    testrailid:C1010873
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    checkout_page.Verify order total in thank you page with split order    ${lst_split_skus}    ${lst_qtys}    discount=${50}
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order By Bank Transfer
    [Tags]    checkout_thankyou    Omni_checkout_delivery
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method 1 2 3 bank transfer
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${personal_information.tel}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    bank_transfer.Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button
    Wait Until Page Loader Is Not Visible
    checkout_page.Verify order total in thank you page with split order    ${lst_split_skus}    ${lst_qtys}    discount=${50}
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_awaiting_payment}    ${payment_page.payment_banktransfer}

To Verify Payment Successfully With Split Order - T1C Partial Redemption
    [Tags]    checkout_thankyou    Omni_checkout_delivery   Omni_smoke    testrailid:C1016305
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    ${response_redeem}=    checkout.Log in with The1 account   ${user_token}    ${t1c_information_4.email}    ${t1c_information_4.password}
    ${total_point}    ${point}    ${baht}    checkout_page.Get redeem point from account T1C    ${response_redeem}
    ${discount_redeem}=    E2E_flow_keywords.Get Partial redeem point and verify T1C discount rule and pay by full payment    ${total_point}    ${point}    ${baht}    ${40}    tc1_email=${t1c_information_4.email}    tc1_pwd=${t1c_information_4.password}
    checkout_page.Verify order total in thank you page with split order   ${lst_split_skus}    ${lst_qtys}    discount=${50}    redeem=${discount_redeem}
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order - T1C Full Redemption
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke    testrailid:C1016302
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${lst_split_full_redeem_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_keywords.Full redeem point and verify T1C point discount    ${t1c_information_4.email}    ${t1c_information_4.password}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click continue payment for full redeemtion
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible   
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_redeem_point}
    checkout_page.Verify order total in thank you page    ${0}

To Verify Payment Successfully With 2 Hour Pickup - T1C Partial Redemption
    [Tags]    checkout_thankyou    Omni_checkout_delivery   Omni_smoke    testrailid:C1016300
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with 2hr pickup    ${e2e_username_10}    ${e2e_password_10}
    ...    ${sku_list_with_2hr}[1]    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    ${response_redeem}=    checkout.Log in with The1 account   ${user_token}    ${t1c_information_4.email}    ${t1c_information_4.password}
    ${total_point}    ${point}    ${baht}    checkout_page.Get redeem point from account T1C    ${response_redeem}
    ${discount_redeem}=    E2E_flow_keywords.Get Partial redeem point and verify T1C discount rule and pay by full payment    ${total_point}    ${point}    ${baht}    ${80}    tc1_email=${t1c_information_4.email}    tc1_pwd=${t1c_information_4.password}
    checkout_page.Verify order total in thank you page with new design   ${${sku_list_with_2hr}[1].price}    redeem=${discount_redeem}

To Verify Payment Successfully With 2 Hour Pickup - T1C Full Redemption
    [Tags]    checkout_thankyou    Omni_thankyou    smoke_test_revised      Omni_smoke    testrailid:C1016297
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with 2hr pickup    ${e2e_username_10}    ${e2e_password_10}
    ...    ${sku_for_reddem}[0]    ${sku_quantity}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_keywords.Full redeem point and verify T1C point discount    ${t1c_information_4.email}    ${t1c_information_4.password}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click continue payment for full redeemtion
    Wait Until Page Loader Is Not Visible
    checkout_page.Thank You Page Should Be Visible   
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_redeem_point}
    checkout_page.Verify order total in thank you page    ${0}

To Verify Split Order With MKP Product
    [Tags]    checkout_thankyou    Omni_thankyou    testrailid:C1016301    Omni_smoke
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order    ${e2e_username_10}    ${e2e_password_10}    ${list_split_with_mkp_sku}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order - 2 Hour Pickup & Standard Delivery
    [Tags]    checkout_thankyou    Omni_checkout_delivery   Omni_smoke
    ...    testrailid:C994712    testrailid:C1010872
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with split order     ${e2e_username_11}    ${e2e_password_11}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.click Standard delivery sub header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    checkout_page.Verify order total in thank you page with split order   ${lst_split_skus}    ${lst_qtys}
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}

To Verify Payment Successfully With Split Order - Apply Coupon Code
    [Tags]    checkout_thankyou    Omni_checkout_delivery    testrailid:C994717    testrailid:C994715
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to cart page with split order     ${e2e_username_11}    ${e2e_password_11}    ${lst_split_skus}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    my_cart_page.Apply coupon code    ${coupon.automation10}
    ${discount_price}=    checkout_page.Get discount price by apply coupon    ${coupon.automation10}
    checkout_page.Click gift wrapping checkbox
    my_cart_page.Click Secure Checkout Button
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click 2hr active store and return store data    ${response}    ${api_ver3}
    delivery_details_keywords.click 2 hour pickup header
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    thankyou_page.Verify gift wrapping details
    checkout_page.Verify order total in thank you page with split order   ${lst_split_skus}    ${lst_qtys}    discount=${discount_price}
    thankyou_page.Verify coupon code display correctly    ${coupon.automation10}

To verify Pickup Instruction at the Thank You page
    [Tags]    regression    checkout_thankyou    Omni_checkout_delivery
    ...    testrailid:C989971    testrailid:C991259    testrailid:C991265    testrailid:C991266    testrailid:C991276    testrailid:C991274
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Get order ID after completely order
    thankyou_page.Verify Stock Availability Notice Should Be Visible
    thankyou_page.Verify How To PickUp Title Should Be Visible
    thankyou_page.Verify Pickup Instruction Should Be Visible

To Guest CheckOut: Disable Email Input
    [Tags]    regression    Omni_checkout_delivery     testrailid:C984479
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    checkout_page.Thank You Page Should Be Visible
    thankyou_page.verify Email Input Field Disable

To verify 2Hr Pickup Omnichannel checkout flow on UI
    [Tags]    testrailid:C986505    testrailid:C986504    testrailid:C986503    testrailid:C986502    testrailid:C986500    testrailid:C1010884
    ...    testrailid:C986499    testrailid:C986498    testrailid:C986501    testrailid:C986497
    ...    Omni_checkout_delivery
    [Template]    Template - To verify 2Hr Pickup Omnichannel checkout flow on UI
    ${payment_methods.pay_at_store}
    ${payment_methods.bank_transfer}
    ${payment_methods.full_payment}

To verify that the Thank you page for Guest Registration show correctly
    [Tags]    regression    checkout_thankyou   testrailid:C963192    testrailid:C963189    testrailid:C963188    testrailid:C963190    testrailid:C973981    testrailid:C973985    testrailid:C973986    Omni_checkout_delivery
    ${checkout_delivery_username}=    FakerLibrary.Email
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    checkout.Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.cod}    ${checkout_delivery_username}    ${store}
    ${enity_id}=    checkout.Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.cod}    ${checkout_delivery_username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_keywords.Verify registration section for guest checkout at thank you page    ${checkout_delivery_username} 
    thankyou_keywords.Verify account registration for guest user successful    ${valid_pass}
    [Teardown]    common_cds_web.Tear down with cancel order

To verify Click and Collect on thank you page
    [Tags]    regression    checkout_thankyou    testrailid:C963288    testrailid:C963295    testrailid:C963301    testrailid:C963298
    ...    testrailid:C963299    testrailid:C963300    testrailid:C963359    testrailid:C963360    testrailid:C963361    testrailid:C963362    testrailid:C963363    Omni_checkout_delivery
    Set Test Variable    ${storage_email_checkout_page}    ${e2e_username_10}
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${sku_list_with_2hr}[0] 
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content   ${quote_id}    ${shipping_methods.click_n_collect.method}    ${shipping_methods.click_n_collect.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.pay_at_store}    ${e2e_username_10}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.pay_at_store}    ${e2e_username_10}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    Verify Click and Collect section at thank you page
    [Teardown]    common_cds_web.Tear down with cancel order

To verify Home Delivery on thank you page
    [Tags]    testrailid:C980439    testrailid:C980440    testrailid:C980443    testrailid:C980449    testrailid:C980450
    ...    testrailid:C980452    testrailid:C980451    testrailid:C980453    testrailid:C980441    testrailid:C980442    testrailid:C980444    
    ...    testrailid:C980445    testrailid:C980446    testrailid:C980447    testrailid:C980448    testrailid:C980454    Omni_checkout_delivery
    [Template]    Template - To verify Home Delivery on thank you page
    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}

To Verify Payment Successfully With Split Order - Standard Pickup & Standard Delivery
    [Tags]    checkout_thankyou    Omni_checkout_delivery   Omni_smoke    testrailid:C1010874
    ${response}    ${user_token}    delivery_details_keywords.Login, add product to cart, return and go to checkout delivery page with standard pickup     ${e2e_username_11}    ${e2e_password_11}    ${lst_split_skus_std}    ${lst_qtys}    ${shipping_address_en.store_location_id}    ${store}
    checkout_page.Select pick at store option with omni design
    ${store_data}=    delivery_details_keywords.Click standard pickup active store and return store data   ${response}    ${api_ver3}
    delivery_details_page.Verify the continue payment button enable
    delivery_details_keywords.Click Continue Payment Button
    checkout_page.Select Payment Method With Credit Card
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    2c2p.Proceed OTP
    Wait Until Page Loader Is Not Visible
    thankyou_page.Verify the each section are display correctly with split order    ${thankyou_page.status_processing}    ${payment_page.credit_card}