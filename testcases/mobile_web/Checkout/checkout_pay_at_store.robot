*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    CommonWebKeywords.Test Teardown
Suite Teardown    Close All Browsers

*** Variables ***
${login_username}    ${pay_at_store_info.username}
${login_password}    ${pay_at_store_info.password}
${login_display_name}    ${pay_at_store_info.firstname}

*** Test Cases ***
To verify that T1C redemption is not allowed to pay by pay at store
    [Tags]    regression    checkout2       checkout_pay_at_store    testrailid:C678450    defect
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[8]
    checkout_keywords.Select standard pickup and go to payment page    ${pick_at_store.central_bangna}
    payment_keywords.Verify payment method name    ${payment_page.payment_pay_at_store}
    checkout_keywords.Redeem point and verify T1C point discount    40    5
    payment_page.Verify payment method pay at store should not Visible

To verify that pay at store will be displayed if there is 1 item in cart
    [Tags]    regression    checkout2       checkout_pay_at_store    testrailid:C682074
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[5]
    checkout_keywords.Select standard pickup and go to payment page    ${pick_at_store.central_bangna}
    payment_page.Verify payment method pay at store should Visible

To verify that pay at store must not be displayed if shipping method is selected
    [Tags]    regression    checkout2       checkout_pay_at_store    testrailid:C678451
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[5]
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    payment_page.Verify payment method pay at store should not Visible

To verify that pay at store will be hid if any product (which is not allowed pay at store) is in a cart
    [Tags]    regression    checkout2       checkout_pay_at_store    testrailid:C678421
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[2]
    checkout_keywords.Select standard pickup and go to payment page    ${pick_at_store.central_bangna}
    payment_page.Verify payment method pay at store should not Visible

To verify that pay at store will be displayed on a payment method if mkp or dropship product (type = 3pl) are added to cart
    [Tags]    regression    checkout2       checkout_pay_at_store    testrailid:C678420
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[5]
    checkout_keywords.Select standard pickup and go to payment page    ${pick_at_store.central_bangna}
    payment_page.Verify payment method pay at store should Visible

To verify that pay at store will be hid if there are more than 1 item in cart
    [Tags]    regression    checkout2       checkout_pay_at_store    testrailid:C682076
    Login, Add multiple product to Cart and Go to Checkout page    ${login_username}    ${login_password}    ${login_display_name}
    checkout_keywords.Select standard pickup and go to payment page    ${pick_at_store.central_bangna}
    payment_page.Verify payment method pay at store should not Visible

To verify that Member paid "Pay at Store" method is displayed details correctly
    [Tags]    regression    checkout2       checkout_pay_at_store    testrailid:C678487
    Login Keywords for mobile web - Login Success  ${login_username}    ${login_password}    ${login_display_name}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[5]
    checkout_keywords.Verify delivery options are displayed correctly
    checkout_keywords.Select standard pickup and go to payment page    ${pick_at_store.central_bangna}
    payment_keywords.Verify payment method name    ${payment_page.payment_pay_at_store}
    payment_page.Select pay at store option
    payment_keywords.Verify pay at store warning message
    payment_page.Click on confirm payment button for pay at store
    payment_keywords.Verify order status and status reason from MDC and MCOM after pay by pay at store
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database
    payment_keywords.Verify payment type and payment status in Thank you page after pay by pay at store
    [Teardown]    common_cds_web.Tear down with cancel order

To verify that Guest paid "Pay at Store" method is displayed details correctly
    [Tags]    regression    checkout2       checkout_pay_at_store    testrailid:C682045
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[8]
    checkout_keywords.Verify delivery options are displayed correctly
    delivery_details_keywords.Input cutomer contact information
    checkout_keywords.Select standard pickup and go to payment page    ${pick_at_store.central_bangna}
    payment_keywords.Verify payment method name    ${payment_page.payment_pay_at_store}
    payment_page.Select pay at store option
    payment_keywords.Verify pay at store warning message
    payment_page.Click on confirm payment button for pay at store
    payment_keywords.Verify payment type and payment status in Thank you page after pay by pay at store
    payment_keywords.Verify order status and status reason from MDC and MCOM after pay by pay at store
    E2E_flow_keywords.Verify order is created in WMS via ESB
    E2E_flow_keywords.Verify order is created in FMS database
    [Teardown]    common_cds_web.Tear down with cancel order