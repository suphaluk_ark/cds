*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser
*** Keywords ***
Template - Verify Available Delivery Options With Product Type On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Postcode field and apply button should be visible
    product_PDP_page.Verify available delivery options by default lead time label    ${sku}
    product_PDP_page.Input value into postcode textbox    ${post_code_list}[0]
    product_PDP_page.Click postcode apply button
    product_PDP_page.Verify available delivery options by postcode value    ${sku}    ${post_code_list}[0]

Template - Verify Available Delivery Options Be Hidden When Postcode Entry Is Not Supported
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Postcode field and apply button should be visible
    product_PDP_page.Input value into postcode textbox    ${post_code_list}[4]
    product_PDP_page.Click postcode apply button
    product_PDP_page.Verify available delivery options by postcode value    ${sku}    ${post_code_list}[4]
    product_PDP_page.The invalid postcode message should be visible

Template - Verify Format Time (UTC+7) in Find pickup Locations On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Verify view pickup location link display correctly
    product_PDP_page.Verify view more pickup location button display correctly    ${sku}
    product_PDP_page.Verify format lead time for each store    ${sku}

Template - Verify Check stock at store On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Verify Check stock at store should be visible
    product_PDP_page.Click Check stock at store
    product_PDP_page.Verify search field should be visible
    product_PDP_page.Verify Use my current location in check stock at store should be visible
    product_PDP_page.Verify list store in check stock at store should be visible    ${sku}

Template - Verify search on check stock at store On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Verify Check stock at store should be visible
    product_PDP_page.Click Check stock at store
    product_PDP_page.Input store name into postcode textbox    ${pick_at_store_name.central_changwattana1}
    product_PDP_page.Verify store are searched by store name    ${pick_at_store_name.central_changwattana1}
    product_PDP_page.Input postcode into postcode textbox    ${post_code_list}[6]
    product_PDP_page.Click postcode apply button
    product_PDP_page.Verify store are searched by postcode value    ${sku}    ${post_code_list}[6]
    product_PDP_page.Input postcode into postcode textbox    ${post_code_list}[4]
    product_PDP_page.Click postcode apply button
    product_PDP_page.The invalid postcode message should be visible on check stock at store

Template - Verify stock status on Check stock at store On PDP
    [Arguments]    ${product_type}
    ${sku}    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Verify Check stock at store should be visible
    product_PDP_page.Click Check stock at store
    product_PDP_page.Verify stock status on Check stock at store    ${sku}

*** Test Cases ***
Verify the product ID section
    [Tags]    regression    product_detail_page    testrailid:C280422    jenkins_group1
    home_page_web_keywords.Search product by product sku    ${product_sku_list}[5]
    product_PDP_keywords.Verify product id display

Verify the Recently Viewed section
    [Tags]    regression    product_detail_page    testrailid:C280436    jenkins_group1
    home_page_web_keywords.Search product by product sku    ${product_sku_list}[5]
    product_PDP_keywords.Verify image product
    product_PDP_keywords.Verify button add to bag    ${product_sku_list}[5]
    product_PDP_keywords.Verify social icon on PDP page
    product_PDP_keywords.Verify product id display

Verify that the Configurable product display correctly
    [Tags]    regression    product_detail_page    testrailid:C355678    jenkins_group1
    product_PDP_keywords.Search product and show on product PDP page    ${CDS11133773.sku}
    #TODO: unable to compare text between data returned from API and data in UI DOM (Need more investigation and action to fix)
    #product_PDP_keywords.Verify product details is display information in PDP page correct by sku    ${CDS11133773.sku}
    product_PDP_page.Verify product name on product PDP page    ${CDS11133773.sku}
    ${product_info}=    product_PDP_page.Get product details to verify in PDP pageby sku    ${CDS11133773.sku}
    product_PDP_page.Verify redeem point text display on product PDP page    ${CDS11133773.sku}    ${product_info.redeem_point}
    product_PDP_keywords.Verify social icon on PDP page
    product_PDP_page.Verify add to bag button on product PDP page    ${CDS11133773.sku}

Verify that if product has a discount price, a discount price must be displayed on a product preview correctly
    [Tags]    regression    product_detail_page    testrailid:C413133    jenkins_group1
    home_page_web_keywords.Search product by product sku    ${product_sku_list}[15]
    product_PDP_page.Verify product name on product PDP page    ${product_sku_list}[15]
    product_PDP_keywords.Verify promotion of SKU    ${order_PDP_${product_sku_list}[15]}

Verify the product slider in the complete the look section if products are less than 2 items or equal to 2 items 
    [Tags]    regression    product_detail_page    testrailid:C415729    jenkins_group1
    home_page_web_keywords.Search product by product sku    ${product_sku_list}[5]
    product_PDP_keywords.Verify relate 5 product product with SKU

Verify that the product slider in the complete the look section if product more than 2 items
    [Tags]    regression    product_detail_page    testrailid:C415765    jenkins_group1
    home_page_web_keywords.Search product by product sku    ${product_sku_list}[9]
    product_PDP_keywords.Verify product relate if moren than 5 items with SKU

Verify Available Delivery Options With Product Type On PDP
    [Tags]    testrailid:C1014519    testrailid:C1014510    testrailid:C1014511    testrailid:C1014520    testrailid:C1014521    testrailid:C1014522    testrailid:C1014523
    [Template]    Template - Verify Available Delivery Options With Product Type On PDP
    ${plp_page_section_scope.simple_product}
    ${plp_page_section_scope.configurable_product}

Verify Available Delivery Options Be Hidden When Postcode Entry Is Not Supported
    [Tags]    testrailid:C1014524
    [Template]    Template - Verify Available Delivery Options Be Hidden When Postcode Entry Is Not Supported
    ${plp_page_section_scope.simple_product}
    ${plp_page_section_scope.configurable_product}

Verify Pickup Location On PDP Page
    [Tags]    Omni_PDP    testrailid:C982111    testrailid:C982113    testrailid:C982114    testrailid:C982117    testrailid:C982119    testrailid:C982120    testrailid:C982121    
    ...     testrailid:C1029198     testrailid:C1029199     testrailid:C1029202     testrailid:C1029203
    ${response_method}=    Get delivery methods response by sku and postcode    ${store_view.name.en}    ${sku_list_with_2hr}[2]    0
    product_PDP_keywords.Verify sku contain click and collect method    ${response_method}
    ${response_store}=    Get Store pickup location    ${sku_list_with_2hr}[2]
    common_keywords.Go To PDP Directly By Product Sku    ${sku_list_with_2hr}[2]
    product_PDP_page.Click 'delivery & return' tab
    product_PDP_page.Verify view pickup location link display correctly
    product_PDP_page.Verify close button display correctly
    product_PDP_keywords.Verify store name display correctly with filter    ${response_store}    ${product_detail_page.filter_option.central}
    product_PDP_keywords.Verify store name display correctly with filter    ${response_store}    ${product_detail_page.filter_option.robinson}
    product_PDP_keywords.Verify store name display correctly with postcode search    ${response_store}    ${post_code_list}[5]

Verify Format Time (UTC+7) in Find pickup Locations On PDP
    [Tags]    testrailid:C1026319    Omni_PDP
    [Template]    Template - Verify Format Time (UTC+7) in Find pickup Locations On PDP
    ${plp_page_section_scope.simple_product}

Verify Check stock at store on PDP
    [Tags]    testrailid:C1014362    Omni_PDP
    [Template]    Template - Verify Check stock at store On PDP
    ${plp_page_section_scope.simple_product}

Verify search on Check stock at store
    [Tags]    testrailid:C1014363    Omni_PDP
    [Template]    Template - Verify search on check stock at store On PDP
    ${plp_page_section_scope.simple_product}

Verify stock status on Check stock at store page
    [Tags]    testrailid:C1014366    Omni_PDP
    [Template]    Template - Verify stock status on Check stock at store On PDP
    ${plp_page_section_scope.simple_product}