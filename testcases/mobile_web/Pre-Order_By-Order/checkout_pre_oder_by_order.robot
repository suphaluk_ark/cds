*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Variables ***
${login_username}    ${cds_checkout_pre_order_username}
${login_password}    ${cds_checkout_pre_order_password}
${login_display_name}    ${cds_checkout_pre_order_information.firstname}

*** Keywords ***
Search pre-order product by product
    [Arguments]    ${product_sku}    ${quantity}=${1}
    Wait Until Page Loader Is Not Visible
    home_page_web_keywords.Search product by product sku    &{${product_sku}}[sku]
    product_page.Click on add to cart button    ${product_sku}
    common_cds_web.Click view cart button should not be visble

*** Test Cases ***
Verify that COD payment method is suppressed when there's a Pre-order item in cart
    [Tags]    regression    Pre-order/By-order    order    testrailid:C178074
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_mobile_web.Login With Credentials    ${login_username}    ${login_password}
    Search pre-order product by product    ${product_sku_list}[36]
    product_page.Click OK to close pre-oder popup
    Header Web - Click Mini Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.COD payment method should not be visible

Verify that COD payment method is suppressed when there's a By order item in cart
    [Tags]    regression    Pre-order/By-order    order    testrailid:C178075
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_mobile_web.Login With Credentials    ${login_username}    ${login_password}
    Search pre-order product by product    ${product_sku_list}[36]
    product_page.Click OK to close pre-oder popup
    Header Web - Click Mini Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.COD payment method should not be visible

Verify that COD payment method is back available when Pre-order item has been removed from cart
    [Tags]    regression    Pre-order/By-order    order    testrailid:C178076
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_mobile_web.Login With Credentials    ${login_username}    ${login_password}
    Search pre-order product by product    ${product_sku_list}[36]
    product_page.Click OK to close pre-oder popup
    Header Web - Click Mini Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    checkout_page.COD payment method should not be visible
    payment_page.Click on view details on mobile
    payment_page.Click Edit Bag in Order Summary
    my_cart_page.Remove product from shopping bag  ${product_sku_list}[36]

Verify that COD payment method is back available when By order item has been removed from cart
    [Tags]    regression    Pre-order/By-order    order    testrailid:C178077
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_mobile_web.Login With Credentials    ${login_username}    ${login_password}
    Search pre-order product by product    ${product_sku_list}[37]
    product_page.Click OK to close pre-oder popup
    Search pre-order product by product    ${product_sku_list}[7]
    Header Web - Click Mini Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed
    payment_page.Click Edit Bag in Order Summary
    my_cart_page.Remove product from shopping bag  ${product_sku_list}[37]
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed

Verify that only common payment methods for all items in cart are available when the cart has Pre-order and/or By order item
    [Tags]    regression    Pre-order/By-order    order    testrailid:C178078
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_mobile_web.Login With Credentials    ${login_username}    ${login_password}
    Search pre-order product by product    ${product_sku_list}[37]
    product_page.Click OK to close pre-oder popup
    Search pre-order product by product    ${product_sku_list}[11]
    Header Web - Click Mini Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed

Verify that Pre-Order shipping information on PDP displays as product setup for Pre-Order item
    [Tags]    regression    Pre-order/By-order    order    testrailid:C183077    testrailid:C183078    testrailid:C183080    testrailid:C183081
    [Setup]    Test Setup - Remove all product in shopping cart    ${login_username}    ${login_password}
    common_cds_mobile_web.Setup - Open browser for mobile web
    Search pre-order product by product    ${product_sku_list}[34]
    product_page.Click OK to close pre-oder popup

Verify that Pre-Order shipping information on PDP is suppressed if the purchase date surpass Pre-Order shipping date
    [Tags]    regression    Pre-order/By-order    order    testrailid:C183079    testrailid:C183082
    common_cds_mobile_web.Setup - Open browser for mobile web
    Search pre-order product by product    ${product_sku_list}[35]
    product_page.Verify Pre-order is suppressed    ${preorder_details.Pre_order}


