*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    CommonWebKeywords.Test Teardown
Suite Teardown    Close All Browsers

*** Variables ***
${login_username}    ${personal_suite_sit.username}
${login_password}    ${personal_suite_sit.password}
${login_display_name}    ${personal_suite_sit.firstname}

*** Test Cases ***
To verify that PDPA checkbox must be unticked by default for register page
    [Tags]    regression    pdpa    register    testrailid:C877868    jenkins_group1
    common_cds_mobile_web.Navigate To Register Page
    registration_page.Verify PDPA Checkbox is unselected as a defaut
    registration_page.Verify PDPA Checkbox is able to clickable
    registration_page.Verify pada text message is dislay correctly

To verify that PDPA checkbox must be unticked by default for delivery detail page
    [Tags]    regression    pdpa    delivery_detail    testrailid:C877866    jenkins_group1
    Login, Add product to Cart and Go to Checkout page    ${login_username}    ${login_password}    ${login_display_name}    ${product_sku_list}[4]
    registration_page.Verify PDPA Checkbox is unselected as a defaut
    registration_page.Verify PDPA Checkbox is able to clickable
    registration_page.Verify pada text message is dislay correctly

To verify that PDPA checkbox must be unticked by default for guest in delivery detail page
    [Tags]    regression    pdpa    delivery_detail    testrailid:C877867    jenkins_group1
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[4]
    registration_page.Verify PDPA Checkbox is unselected as a defaut
    registration_page.Verify PDPA Checkbox is able to clickable
    registration_page.Verify pada text message is dislay correctly