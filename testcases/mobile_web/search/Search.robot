*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Keywords ***
Template - verify elastic search result is displayed on PLP page
    [Arguments]    ${search_keyword}    ${result_keyword}
    search_keyword.Search product for go to PLP page    ${search_keyword}
    common_keywords.Wait Until Page Is Completely Loaded
    plp_page.Verify elastic search result is displayed product that contain search keyword    ${result_keyword}

*** Test Cases ***
Verify elastic search result is displayed on PLP page
    [Tags]    search_and_filter    regression    search_product    elastic_search    testrailid:C697580
    [Template]    Template - verify elastic search result is displayed on PLP page
    ${search_by_keyword.sanria}    ${search_by_keyword.sanrio}
    ${search_by_keyword.casia}    ${search_by_keyword.casio}
    ${search_by_keyword.adidos}    ${search_by_keyword.adidas}
    ${search_by_keyword.diox}    ${search_by_keyword.dior}

To verify page navigation on PLP page can display follow result and working correctly
    [Tags]    search_and_filter    regression     search_product    pagination    testrailid:C25284
    search_keyword.Search product for go to PLP page    ${search.product_exist}
    search_keyword.Verify pagination working correctly on PLP page    ${search.num_page}

To verify page navigation on PLP page when no result navigation of page not visible
    [Tags]    search_and_filter    regression     search_product    pagination    testrailid:C25285
    search_keyword.Search product for go to PLP page    ${search.product_not_exist}
    plp_page.Verify pagination should not be displayed in PLP

Verify the filter category when select Thai or English language
    [Tags]    search_and_filter    regression    filter_product    testrailid:C72417
    search_keyword.Search product for go to PLP page  ${search_by_keyword.beauty_the_beast}
    common_cds_mobile_web.Switch language    ${mobile_web_common.switch_language.english}
    plp_page.Verify product name should be displayed correctly in search product  ${product_name.disney_beauty_and_the_beast_cushion_pillow.en}
    common_cds_mobile_web.Switch language    ${mobile_web_common.switch_language.thai}
    plp_page.Verify product name should be displayed correctly in search product  ${product_name.disney_beauty_and_the_beast_cushion_pillow.th}

Verify that user clicks at filter category
    [Tags]    search_and_filter    regression    filter_product    testrailid:C72418
    common_cds_mobile_web.Go to beauty category - make up page
    plp_page.Verify that color of Filter should be red
    PLP_page_mobile_web.Click filter button
    plp_page.Click on brand name arrow-down button
    plp_page.Click on a brand name  ${filters.brand_name.mac}
    plp_page.Verify that color of Brand name should be red
    plp_page.Click on brand name arrow-down button
    plp_page.Click on a brand name  ${filters.brand_name.mac}
    plp_page.Verify that color of Brand name should be not red

Verify that user clicks at sub categories
    [Tags]    search_and_filter    regression    filter_product    testrailid:C72419
    common_cds_mobile_web.Go to beauty category - make up page
    PLP_page_mobile_web.Click filter button
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.beauty.makeup.face}
    plp_page.Verify sub category should be not displayed from the category list  ${product_category.beauty.makeup.sub_eyes.eyeshadow}
    plp_page.Click on a category  ${product_category.beauty.makeup.eyes}
    plp_page.Verify sub category should be displayed from the category list  ${product_category.beauty.makeup.sub_eyes.eyeshadow}

Verify that user clicks at the last sub categories
    [Tags]    search_and_filter    regression    filter_product    testrailid:C72421
    common_cds_mobile_web.Go to beauty category - make up page
    PLP_page_mobile_web.Click filter button
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.beauty.makeup.face}
    plp_page.Verify red check mark should be displayed  ${product_category.beauty.makeup.face}
    plp_page.Click on a category  ${product_category.beauty.makeup.eyes}
    plp_page.Click on a category  ${product_category.beauty.makeup.sub_eyes.eyeshadow}
    product_category_page.Verify last sub category direct to correct page  ${product_category.beauty.makeup.sub_eyes.eyeshadow}

Verify that user clicks a check mark to select the sub category
    [Tags]    search_and_filter    regression    filter_product    testrailid:C72422
    common_cds_mobile_web.Go to beauty category - make up page
    PLP_page_mobile_web.Click filter button
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.beauty.makeup.face}
    plp_page.Verify red check mark should be displayed  ${product_category.beauty.makeup.face}
    plp_page.Click on a category  ${product_category.beauty.makeup.face}
    plp_page.Verify red check mark should be not displayed  ${product_category.beauty.makeup.face}

Verify that check mark must be displayed correctly
    [Tags]    search_and_filter    regression    filter_product    testrailid:C72423
    common_cds_mobile_web.Go to beauty category - make up page
    PLP_page_mobile_web.Click filter button
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.beauty.makeup.face}
    plp_page.Verify red check mark should be displayed  ${product_category.beauty.makeup.face}
    PLP_page_mobile_web.Click on category arrow-up button
    plp_page.Click on brand name arrow-down button
    PLP_page_mobile_web.Click on brand name arrow-up button
    plp_page.Click on category arrow-down button
    plp_page.Verify red check mark should be displayed  ${product_category.beauty.makeup.face}
    plp_page.Click on a category  ${product_category.beauty.makeup.eyes}
    plp_page.Verify red check mark should be displayed  ${product_category.beauty.makeup.eyes}
    PLP_page_mobile_web.Click on category arrow-up button
    plp_page.Click on brand name arrow-down button
    PLP_page_mobile_web.Click on brand name arrow-up button
    plp_page.Click on category arrow-down button
    plp_page.Verify red check mark should be displayed   ${product_category.beauty.makeup.eyes}

Verify the sub categories that there is no product in that sub categories
    [Tags]    search_and_filter    regression    filter_product    testrailid:C72424
    common_cds_mobile_web.Go to beauty category - make up page
    PLP_page_mobile_web.Click filter button
    plp_page.Click on category arrow-down button
    plp_page.Click on a category  ${product_category.beauty.makeup.lips}
    plp_page.Verify sub category should be not displayed from the category list   ${product_category.beauty.makeup.sub_lips.lips_primer}

Verify that if any option in filter is seleted on PLP
    [Tags]    search_and_filter    regression    filter_product    testrailid:C117750
    search_keyword.Search product for go to PLP page  ${search_by_keyword.beauty}
    plp_page.Click on sorter arrow-down button
    plp_page.Select New arrivals from sorter
    plp_page.Verify that color of Filter should be red
    PLP_page_mobile_web.Click filter button
    plp_page.Click on price range arrow-down button
    PLP_page_mobile_web.Click on price range arrow-up button
    plp_page.Verify that color of price range should not be red
    plp_page.Click on brand name arrow-down button
    plp_page.Click on a brand name  ${filters.brand_name.mac}
    plp_page.Verify that color of Brand name should be red
    plp_page.Click on color arrow-down button
    plp_page.Click on a color  ${filters.color.pink}
    plp_page.Verify that color of Color should be red

Verify that a "view by" function on a search result page can work correctly
    [Tags]    search_and_filter    regression    search_product    catalog_service    testrailid:C335748
    search_keyword.Search product for go to PLP page    ${search_by_keyword.pillow}
    # *BUG* CDS-7218 
    plp_page.Click on arrow-down on view by section
    plp_page.Click 50 from view by section
    plp_page.Verify the total of products should be displayed correctly    ${search_result_number.total_50}
    plp_page.Click on arrow-down on view by section
    plp_page.Click 100 from view by section
    plp_page.Verify the total of products should be displayed correctly    ${search_result_number.total_100}
    plp_page.Click on arrow-down on view by section
    plp_page.Click 200 from view by section

Verify that product count must be displayed on a search result page correctly
    [Tags]    search_and_filter    regression    search_product    catalog_service    testrailid:C360138
    search_keyword.Search product for go to PLP page    ${search_by_keyword.beauty_the_beast}
    common_keywords.Wait Until Page Is Completely Loaded
    product_PLP_mobile_web.Verify the total of products should be displayed correctly    ${search_result_number.total_50}

Verify that Trending Search is hidden from Website
    [Tags]    search_and_filter    regression    search_product    search_result    testrailid:C143079
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${search_by_keyword.beauty}
    plp_page.Verify trending search should be not displayed

To verify that result search when user type < 3 characters, system should not show result search
    [Tags]    search_and_filter    regression    search_product    search_popup    testrailid:C23947
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${search.two_characters}
    plp_page.Verify product image should be not displayed in popup

To verify that result search when user type => 3 character, system should display result search
    [Tags]    search_and_filter    regression    search_product    search_popup    testrailid:C23948
    search_keyword.Verify product preview should be displayed in popup by search keyword via API    ${search_by_keyword.pillow}
    plp_page.Verify search keyword should be displayed in pop-up
    plp_page.Verify categories should be displayed in pop-up

To verify that result search when user type => 3 character but not math result, system show only recent search
    [Tags]    search_and_filter    regression    search_product    search_popup    testrailid:C23949
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${search.random_characters}
    home_page_web_keywords.Press Enter key to search a product
    home_page_web_keywords.Click close button in search box
    plp_page.Verify recent search should be displayed in pop-up

To verify that CSS style of result of product when result less than 6 or more than 6
    [Tags]    search_and_filter    regression    search_product    search_popup    testrailid:C23950
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${search.three_characters}
    plp_page.Verify the product images should be six

Verify the search keyword section must display result from serch team's search keyword suggestion api
    [Tags]    search_and_filter    regression    search_product    brand_blacklist    testrailid:C130480
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${search.four_characters}
    plp_page.Verify a recommendation keyword should be displayed in pop-up  ${recommendation_keyword.beauty}
    plp_page.Verify a recommendation keyword should be displayed in pop-up  ${recommendation_keyword.beauty_cosmetic}

Verify that search result will be redirected to URL which is return from search team api
    [Tags]    search_and_filter    regression    search_product    brand_blacklist    testrailid:C130555
    SeleniumLibrary.Go To    ${url_search_keyword.nike.api}
    plp_page.Verify api should return valid keyword  ${search_by_keyword.nike}
    SeleniumLibrary.Go To    ${url_search_keyword.nike.web}
    plp_page.Verify product name should be displayed correctly in search product  ${product_name.dp_air_jordan_ow_chicaco.th}
