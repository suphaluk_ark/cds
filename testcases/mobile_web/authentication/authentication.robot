*** Settings ***
Resource    ${CURDIR}/../../../keywords/mobile_web/mobile_web_imports.robot
Test Setup    common_cds_mobile_web.Setup - Open browser for mobile web
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Keywords ***
Template - Verify register error message is displayed correctly when input invalid value
    [Arguments]    ${firstname}    ${lastname}    ${email}   ${password}    ${err_msg}
    common_cds_mobile_web.Navigate To Register Page
    registration_page.Input Firstname    ${firstname}
    registration_page.Input Lastname     ${lastname}
    registration_page.Input Email    ${email}
    registration_page.Input Password For Register Page    ${password}
    registration_page.Click Submit Button
    registration_page.Verify registration page should display error message    ${err_msg}

Templete - Verify login error message is displayed correctly when invalid login
    [Arguments]    ${username}    ${password}
    common_cds_mobile_web.Navigate to Login page
    login_keywords.Login Keywords - Login with Email    ${username}    ${password}
    login_keywords.Login Keyword - Verify Error Message When Login Failed    ${error_message.login_email.login_failed}
    home_page_mobile_web_keywords.Click on Back button 
    home_page_mobile_web_keywords.Click x button

Templete - Verify forgot password error message is displayed correctly
    [Arguments]    ${email}    ${error_msg}
    common_cds_mobile_web.Navigate to Login page
    forgot_password_mobile_web_keywords.Forgot Password With Email    ${email}
    forgot_password_page.Forgot Password - E-mail Field Display Error Message    ${error_msg}

*** Test Cases ***
To verify that Web or Mobile user should be automatically logged-in to the system after wait for 3 seconds
    [Tags]    regression    authentication    testrailid:C71505    testrailid:C26434
    common_cds_mobile_web.Navigate To Register Page
    registration_page.Input Firstname    ${register_information.firstname}
    registration_page.Input Lastname    ${register_information.lastname}
    registration_page.Input Email    ${register_email}
    registration_page.Input Password For Register Page    ${register_password}
    registration_page.Click Submit Button
    registration_keywords.verify registration success message after register
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    customer_api.Delete registeration account from mdc    ${register_email}    ${register_password}
    ...    AND    Close Browser

To verify warning message of registration page when missing important information
    [Tags]    regression    authentication    testrailid:C26436
    common_cds_mobile_web.Navigate To Register Page
    registration_page.Input Lastname    ${cds_authentication_information_1.lastname}
    registration_page.Input Password For Register Page    ${cds_authentication_password_1}
    registration_page.Click Submit Button
    registration_page.Verify registeration page with error message in firstname field
    registration_page.Verify registeration page with error message in email field
    registration_page.Verify registeration fail and should not contain welcome message

To verify system automatically logged-in after click continue
    [Tags]    regression    authentication    testrailid:C71506
    common_cds_mobile_web.Navigate To Register Page
    registration_page.Input Firstname    ${register_information.firstname}
    registration_page.Input Lastname    ${register_information.lastname}
    registration_page.Input Email    ${register_email}
    registration_page.Input Password For Register Page    ${register_password}
    registration_keywords.Submit And Verify Registeration Success
    [Teardown]    Run Keywords    CommonWebKeywords.Test Teardown
    ...    AND    customer_api.Delete registeration account from mdc    ${register_email}    ${register_password}
    ...    AND    Close Browser

To verify register fail when user input password without number, lowercase , uppercase and less than 8 character
    [Tags]    regression    authentication    testrailid:C26435    testrailid:C26437    testrailid:C26438
    [Template]    Template - Verify register error message is displayed correctly when input invalid value
    ${authentication.firstname}    ${authentication.lastname}   ${authentication.existed_email}    ${register_password}    ${error_message.account_registration.email_existed}
    ${authentication.firstname}    ${authentication.lastname}   ${authentication.non_existed_email}    ${authentication.invalid_pass_no_number}    ${error_message.account_registration.invalid_password}
    ${authentication.firstname}    ${authentication.lastname}   ${authentication.non_existed_email}    ${authentication.invalid_pass_lowercase}    ${error_message.account_registration.invalid_password}
    ${authentication.firstname}    ${authentication.lastname}   ${authentication.non_existed_email}    ${authentication.invalid_pass_uppercase}    ${error_message.account_registration.invalid_password}
    ${authentication.firstname}    ${authentication.lastname}   ${authentication.non_existed_email}    ${authentication.invalid_pass_less_8_char}    ${error_message.account_registration.invalid_password}
    ${authentication.firstname}    ${authentication.lastname}   ${authentication.wrong_format}    ${register_password}    ${error_message.account_registration.invalid_email}

To verify when doesn't input any data then click log in button
    [Tags]    regression    authentication    testrailid:C22892
    common_cds_mobile_web.Navigate to Login page
    Login Keywords - Login with Email    ${EMPTY}    ${EMPTY}
    Login Keyword - Email field Should Be Visible Error    ${error_message.login_email.required_field}
    Login Keyword - Password field Should Be Visible Error    ${error_message.login_email.required_field}

To verify when input invalid email format
    [Tags]    regression    authentication    testrailid:C22893
    common_cds_mobile_web.Navigate to Login page
    header_web_keywords.Header Web - Input Email on Header  ${authentication.wrong_email_format}
    header_web_keywords.Header Web - Click Submit Button on Header
    header_web_keywords.Hearder Web - Email should be displayed error message  ${error_message.account_registration.invalid_email}

To verify when username or password are incorrect
    [Tags]    regression    authentication    testrailid:C22895
    [Template]    Templete - Verify login error message is displayed correctly when invalid login
    wrong_${authentication.username}    ${authentication.password}
    ${authentication.username}    wrong_${authentication.password}

To verify that able to login via email and password successfuly
    [Tags]    regression    authentication    testrailid:C22896
    common_cds_mobile_web.Navigate to Login page
    header_web_keywords.Header Web - Input Email on Header  ${cds_authentication_username_1}
    header_web_keywords.Header Web - Input Password on Header  ${cds_authentication_password_1}
    header_web_keywords.Header Web - Click Submit Button on Header
    home_page_mobile_web_keywords.Click on hamburger menu
    header_web_keywords.Verify display name with welcome message should be displayed

To verify that guest click view cart button on mini cart
    [Tags]    regression    authentication    testrailid:C85170
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[2] 
    my_cart_page.Verify shopping bag page should be displayed

To validate LOG IN OR CONTINUE AS GUEST page show information correctly
    [Tags]    regression    authentication    testrailid:C85171
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]  1    ${True}
    checkout_page.Click guest login button
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.title}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.registered_customer}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.email}
    guest_login_page.Verify email place holder display correctly
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.password}
    guest_login_page.Verify password place holder display correctly
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.forgot_password}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.login}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.login_facebook}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.new_to_central}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.new_acc_description}
    guest_login_page.Verify page displays correctly require fields  ${guest_login_page.checkout_guest}

To verify that guest can login with facebook at checkout step1 (Delivery Detail)
    [Tags]    regression    authentication    testrailid:C85235
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${facebook_email.facebook_email_test_5}  ${facebook_password}
    common_cds_mobile_web.Search product and add product to cart  ${product_sku_list}[2]    1    ${True}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[3]
    checkout_page.Click guest login button
    guest_login_page.Click login with facebook
    common_cds_web.Switch to new window
    registration_page.Input Email With Facebook    ${facebook_email.facebook_email_test_5}
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    common_cds_web.Switch to main window
    checkout_page_mobile_web.Click view details
    checkout_page.Verify that product and quantity should be displayed correctly  ${product_sku_list}[2]
    checkout_page.Verify that product and quantity should be displayed correctly  ${product_sku_list}[3]
    checkout_page.Verify total of products should be displayed correctly at order summary on Delivery Details page  ${2}

To verify that page will redirect to Delivery page after login successful on checkout page when no item in member cart
    [Tags]    regression    authentication    testrailid:C85239    testrailid:C85234    testrailid:C85211
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${product_sku_list}[2]    1    ${True}
    checkout_page.Click guest login button
    guest_login_page.Input username, password and click login button at guest login page  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    checkout_page.Verify Delivery Details page should be displayed
    checkout_page_mobile_web.Click view details
    checkout_page.Verify that product and quantity should be displayed correctly  ${product_sku_list}[2]
    checkout_page.Verify total of products should be displayed correctly at order summary on Delivery Details page  ${1}

To verify that error message is shown correctly when guest have wrong username and then log in on checkout page successfully
    [Tags]    regression    authentication    testrailid:C85238    
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${cds_authentication_username_1}  ${cds_authentication_password_1}
    common_cds_mobile_web.Search product, add product to cart, go to checkout page    ${product_sku_list}[3]
    checkout_page.Click guest login button
    guest_login_page.Input text and verify email error message should be displayed    ${authentication.wrong_email_format1}
    guest_login_page.Input text and verify email error message should be displayed    ${authentication.wrong_email_format2}
    guest_login_page.Input text and verify email error message should be displayed    ${authentication.wrong_email_format3}
    guest_login_page.Input text and verify email error message should be displayed    ${authentication.wrong_email_format4}
    guest_login_page.Input text and verify email error message should be displayed    ${authentication.wrong_email_format5}
    guest_login_page.Input username, password and click login button at guest login page    ${authentication.existed_email}    ${authentication.invalid_pass_no_number}
    guest_login_page.Verify incorrect account error message should be displayed

To verify that email address field on forgot password page, the field should be display error message
    [Tags]    regression    authentication    testrailid:C23699    testrailid:C23700
    [Template]    Templete - Verify forgot password error message is displayed correctly
    ${authentication.wrong_format}    ${error_message.forgot_password.invalid_format}
    ${authentication.wrong_email}    ${error_message.forgot_password.invalid_email}

To verify retry sent email for forgot password can working correctly
    [Tags]    regression    authentication    testrailid:C23703
    common_cds_mobile_web.Navigate to Login page
    forgot_password_mobile_web_keywords.Forgot Password With Email    ${authentication.cust_email}
    Send E-mail Success Message Should Be Visible    ${success_message.forgot_password.sent_email_success}

To verify that system can merge cart after guest login correctly
    [Tags]    regression    authentication    testrailid:C85236
    common_cds_web.Test Setup - Remove all product in shopping cart   ${cds_authentication_username_1}    ${cds_authentication_password_1}
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    header_web_keywords.Remove all product in shopping cart

    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[0]
    login_keywords.Login Keywords for mobile web - Sign Out Success
    common_keywords.Wait Until Page Is Completely Loaded
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[2]
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[3]
    login_keywords.Login Keywords for mobile web - Login Success  ${account_management_username_1}  ${account_management_password_1}  ${account_management_information_1.firstname}
    header_web_keywords.Header Web - Click Mini Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page_mobile_web.Click view details
    checkout_page.Verify total of products should be displayed correctly at order summary on Delivery Details page  ${3}

To verify that system can merge cart after guest login with facebook correctly
    [Tags]    regression    authentication    testrailid:C85237    testrailid:C22631
    Run Keyword And Ignore Error  common_cds_web.Test Setup - Remove all product in shopping cart  ${facebook_email.facebook_email_test_5}  ${facebook_password}

    #verify registeration message success with welcome message
    login_keywords.Login Keywords for mobile web - Login Success Via Facebook    ${facebook_email.facebook_email_test_5}    ${facebook_password}
    header_web_keywords.Remove all product in shopping cart
    home_page_mobile_web_keywords.Click on hamburger menu
    registration_page.Verify registeration message success with welcome message

    #verify that system can merge cart after guest login with facebook correctly
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[4]
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[5]
    login_keywords.Login Keywords for mobile web - Sign Out Success
    SeleniumLibrary.Close All Browsers
    common_cds_mobile_web.Setup - Open browser for mobile web
    common_keywords.Wait Until Page Is Completely Loaded
    common_cds_mobile_web.Search product and add product to cart    ${product_sku_list}[0]
    login_keywords.Login Keywords for mobile web - Login Success Via Facebook    ${facebook_email.facebook_email_test_5}    ${facebook_password}
    header_web_keywords.Header Web - Click Mini Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page_mobile_web.Click view details
    checkout_page.Verify total of products should be displayed correctly at order summary on Delivery Details page  ${3}
    
To validation connecting not success with facebook account, new account don't be created for individual type
    [Tags]    regression    authentication    testrailid:C22634
    common_cds_mobile_web.Navigate to Login page
    header_web_keywords.Header Web - Click Login on Header
    registration_page.Header Web - Click Facebook login Button
    common_cds_web.Switch to new window
    registration_page.Input Email With Facebook    ${facebook_email.facebook_email_test_3}
    registration_page.Input Password With Facebook    ${facebook_password}
    registration_page.Click Submit Facebook Login Button
    registration_page.Verify registeration Error box facebook email

To validation connecting success with facebook email, MDC information
    # *** BLOCK -- api MDC got '401 Unauthorize by email and pwd from facebook' -- 21 July 2020 ***
    [Tags]    regression    authentication    testrailid:C22632    testrailid:C22633  testrailid:C991758
    Run Keyword And Ignore Error    customer_api.Delete registeration account from mdc    ${facebook_email.facebook_email_test_8.email}    ${facebook_password}
    common_cds_web.Verify user on mdc authentication should be fail    ${facebook_email.facebook_email_test_8.email}    ${facebook_password}
    login_keywords.Login Keywords for mobile web - Login Success Via Facebook    ${facebook_email.facebook_email_test_8.email}    ${facebook_password}
    common_cds_web.Customer details 'email' from mdc should be equal 'login email'    ${facebook_email.facebook_email_test_8.email}    ${facebook_password}
    common_cds_web.Customer details 'first name and lastname' from mdc should be equal   ${facebook_email.facebook_email_test_8.email}    ${facebook_password}    ${facebook_email.facebook_email_test_8.first_name}    ${facebook_email.facebook_email_test_8.last_name}

