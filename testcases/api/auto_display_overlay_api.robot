*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot

*** Keywords ***
Template - Verify When Invalid Parameter On Auto Display Overlay Base On Cart Price Rule
    [Arguments]    ${store}    ${product_ids}
    ${response}=    auto_display_overlay.Get Overlay Base On Cart Price Rule    ${store}    productIds    ${product_ids}
    @{empty_list}=    Create List
    Should Be Equal    ${response}    ${empty_list}


Template - Verify When Missing Parameter On Auto Display Overla Base On Cart Price Rule
    [Arguments]    ${store}    ${param_product_ids}    ${product_ids}
    ${resonse_code}=    Set Variable    400
    ${response}=    auto_display_overlay.Get Overlay Base On Cart Price Rule    ${store}    ${param_product_ids}    ${product_ids}    ${resonse_code}
    ${message}=    Get Value From Json    ${response}    $.message
    Should Be Equal    ${message}[0]    \"%fieldName\" is required. Enter and try again.
    ${field_name}=    Get Value From Json    ${response}    $..fieldName
    Should Be Equal    ${field_name}[0]    productIds

Template - Verify that a product overlay can be set on a cart price rule and displayed on website successfully if a product overlay is set as "Enable" and cart price rule is set as "Active"
    [Arguments]    ${store}    ${product_id}    ${rule_id}    ${overlay_image}    ${display_priority}
    ${param_product_ids}=    Set Variable    productIds
    ${resonse_code}=    Set Variable    200
    ${expected}=    Create Dictionary    rule_id=${rule_id}    product_id=${product_id}    overlay_image=${overlay_image}    display_priority=${display_priority}
    ${response}=    auto_display_overlay.Get Overlay Base On Cart Price Rule    ${store}    ${param_product_ids}    ${product_id}    ${resonse_code}
    auto_display_overlay.Verify Single Item Response Overlay Base On Cart Price Rule    ${response}[0]    ${expected}

*** Test Cases ***
Verify that a product overlay can be set on a cart price rule and displayed on website successfully if a product overlay is set as "Enable" and cart price rule is set as "Active"
    [Template]    Template - Verify that a product overlay can be set on a cart price rule and displayed on website successfully if a product overlay is set as "Enable" and cart price rule is set as "Active"
    [Tags]    overlay_cart_price_rule_api    testrailid:C988959    rule_id=1038
    cds_th    188768    1038    overlay/overlay-1594009605.jpg    1
    cds_en    188768    1038    overlay/overlay-1566897694.png    1

Verify When Missing Parameter On Auto Display Overla Base On Cart Price Rule
    [Template]    Template - Verify When Missing Parameter On Auto Display Overla Base On Cart Price Rule
    [Tags]    overlay_cart_price_rule_api    testrailid:C989654
    cds_th    ${Empty}       188768
    cds_en    ${Empty}       18876
    cds_th    productId       188768
    cds_en    productId       18876
    cds_th    หกดกห       หกดกห
    cds_en    หกดกห       หกดกห

Verify When Invalid Parameter On Auto Display Overlay Base On Cart Price Rule
    [Template]    Template - Verify When Invalid Parameter On Auto Display Overlay Base On Cart Price Rule
    [Tags]    overlay_cart_price_rule_api    testrailid:C989058
    cds_th    -427169
    cds_en    -427169
    cds_th    test427169
    cds_en    test427169
    cds_th    """~!@#$%^&*()_+`-="""
    cds_en    """~!@#$%^&*()_+`-="""
    cds_th    กดกดห
    cds_en    กดกดห
    cds_th    9876543210987654321098765432109876543210
    cds_en    9876543210987654321098765432109876543210