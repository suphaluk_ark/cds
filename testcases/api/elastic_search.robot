*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot
Test Template    Template - Verify GraphQL Search Keyword

*** Keywords ***
Template - Verify GraphQL Search Keyword
    [Documentation]    This template is checking elastic search api should return result contain with correct product
    ...    ${search_type}    'brand' or 'keyword'
    ...    brand - compare result from brand with result from search keyword
    ...    keyword - compare result from search correct keyword with result from search handle keyword
    [Arguments]    ${correct_keyword}    ${handle_keyword}    ${search_type}=brand
    Run Keyword If    '${search_type}'=='brand'    Test - Verify Brand Keyword With Search Keyword    ${correct_keyword}    ${handle_keyword}
    ...    ELSE    Test - Verify Search Correct Keyword With Search Handle Keyword    ${correct_keyword}    ${handle_keyword}

Test - Verify Brand Keyword With Search Keyword
    [Arguments]    ${brand_name}    ${search_keyword}
    ${skus_brand}=    Get Product Sku By Brand Name Via Api    ${brand_name}
    ${skus_keyword}=    Get Product Sku By Keyword Via Api    ${search_keyword}    ${500}
    FOR    ${item}    IN    @{skus_brand}
        Collections.List Should Contain Value    ${skus_keyword}    ${item}
    END

Test - Verify Search Correct Keyword With Search Handle Keyword
    [Arguments]    ${correct_keyword}    ${handle_keyword}
    ${skus_correct}=    Get Product Sku By Keyword Via Api    ${correct_keyword}
    ${skus_handle}=    Get Product Sku By Keyword Via Api    ${handle_keyword}    ${100}
    FOR    ${item}    IN    @{skus_correct}
        Collections.List Should Contain Value    ${skus_handle}    ${item}
    END

*** Test Cases ***
Verify GraphQL Search Keyword With Keyword Data
    [Tags]    elastic_search    testrailid:C655681    testrailid:C806135
    sanrio    sanria
    casio    casia
    adidas    adidos
    dior    diox
    american tourister    americ tourist
    biotherm    bioterm
    lancome    lancom
    xiaomi    xiaomii
    sulwhasoo    sulwhasoooo
    dyson    dysonn
    browne & co.    browne co
    g-shock    gshock
    coach    coaches
    burt's bees    burt bee
    era-won    era won
    era-won    erawon
    teddy house    teddyhouse
    LANCÔME    lancome
    women tshirt    woman tshirt    keyword
    brush n16    brush n 16    keyword
    360swim    360 swim    keyword
    power drills    power drill    keyword
    drill    drills    keyword
    bosch drill    bosch drills    keyword
    office chair    office chairs    keyword
    chair    chairs    keyword
    air fryer    air fryers    keyword
    books    book    keyword