*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot

*** Variables ***
${product_type_sku}=    ${product_sku_list}[0]
${store}=    ${store_view.name.en}
${api_version}=    ${api_ver3}

*** Test Cases ***
Member Checkout With COD By Standard Shipping
    [Tags]
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.cod}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.cod}    ${store}
    ${order_id}=    order_details.Get order details by entity_id    ${enity_id}

Member Checkout With Bank Transfer By Standard Shipping
    [Tags]
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.bank_transfer}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.bank_transfer}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}[0]

Member Checkout With Installment Via Card Info By Standard Shipping
    [Tags]
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.pay_by_installment}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.pay_by_installment}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}[0]

Member Checkout With Full Payment Via Card Info By Standard Shipping
    [Tags]
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.full_payment}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.full_payment}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}[0]

Member Checkout With Pay At Store By Click And Collect
    [Tags]
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.pay_at_store}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.pay_at_store}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}[0]

Member Checkout With Bank Transfer By 2 Hour Pickup
    [Tags]
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${store}    ${api_version}
    checkout.Member update shipping information for 2 hour pickup    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    9    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.bank_transfer}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.bank_transfer}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}[0]

###---Guest---###
Guest Checkout With COD By Standard Shipping
    [Tags]
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    checkout.Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.cod}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    checkout.Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.cod}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id

Guest Checkout With Bank Transfer By Standard Shipping
    [Tags]
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}[0]

Guest Checkout With Installment Via Card Info By Standard Shipping
    [Tags]
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.pay_by_installment}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.pay_by_installment}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}[0]

Guest Checkout With Full Payment Via Card Info By Standard Shipping
    [Tags]
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.full_payment}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.full_payment}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}[0]

Guest Checkout With Pay At Store By Click And Collect
    [Tags]
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.click_n_collect.method}    ${shipping_methods.click_n_collect.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.pay_at_store}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.pay_at_store}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}[0]

Guest Checkout With Bank Transfer By 2 Hour Pickup
    [Tags]
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    Verify order status and status reason from MDC and MCOM after pay by transfer    ${increment_id}[0]