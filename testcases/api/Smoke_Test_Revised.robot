*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot

*** Keywords ***    
Verify field should not be null
    [Arguments]    ${json_path}
    ${is_field_null}    Run Keyword And Return Status    REST.Null    ${json_path}
    Should Not Be True    ${is_field_null}

Verify list product categories should contain keyword
    [Arguments]    ${list_product_categories}    ${keyword}
    FOR    ${category}    IN    @{list_product_categories}
        ${is_contain}=    Run Keyword And Return Status    Should Contain    ${category.upper()}    ${keyword.upper()}
        Exit For Loop If    ${is_contain}
    END
    Should Be True    ${is_contain}

*** Test Cases ***
Verify that searching keyword by API return search result correctly - Brand name
    [Tags]    search    search_api    smoke_test_revised    sanity_prod    testrailid:C1004730
    Set Test Variable    ${test_brand_name}    ${search_by_keyword.clinique}
    ${response}=    search_falcon.Api Search Product By Brand Name    ${test_brand_name}    ${language}    ${BU.lower()}    ${50}
    ${list_of_product}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*]
    Should Not Be Empty    ${list_of_product}

    Run Keyword And Continue On Failure    REST.String    $..brand_name    ${test_brand_name.upper()}
    Run Keyword And Continue On Failure    Verify field should not be null    $..sku
    Run Keyword And Continue On Failure    Verify field should not be null    $..price

Verify that searching keyword by API return search result correctly - Keyword
    [Tags]    search    search_api    smoke_test_revised    sanity_prod    testrailid:C1005683
    Set Test Variable    ${test_keyword}    ${search_by_keyword.pillow}
    ${response}=    search_falcon.Api Search Product By Keyword    ${test_keyword}    ${common_language.en}    ${BU.lower()}    ${50}
    ${list_of_product}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*]
    Should Not Be Empty    ${list_of_product}

    FOR    ${dict_product}    IN    @{list_of_product}
        ${name}=    Set Variable    ${dict_product}[name]
        ${is_contains_name}=    Run Keyword And Return Status    Should Contain    ${name.upper()}    ${test_keyword.upper()}
        Continue For Loop If    ${is_contains_name}==${True}
        
        ${brand_name}=    Set Variable    ${dict_product}[brand_name]
        ${is_contains_brand_name}=    Run Keyword And Return Status    Should Contain    ${brand_name.upper()}    ${test_keyword.upper()}
        Continue For Loop If    ${is_contains_brand_name}==${True}

        ${sku}=    Set Variable    ${dict_product}[sku]
        ${is_contains_sku}=    Run Keyword And Return Status    Should Contain    ${sku.upper()}    ${test_keyword.upper()}
        Continue For Loop If    ${is_contains_sku}==${True}

        ${list_product_categories}=    JSONLibrary.Get Value From Json    ${dict_product}    $..categories[*].name
        Run Keyword And Continue On Failure   Verify list product categories should contain keyword    ${list_product_categories}    ${test_keyword}
    END

Verify the "Price Range" filter must be displayed correctly - Brand Name
    [Tags]    filter    filter_api    smoke_test_revised    testrailid:C1003513
    Set Test Variable    ${test_brand_name}    ${search_by_keyword.clinique}
    ${min_price}    ${max_price}    plp_api.Get min, max of filter price range by product brand name    ${test_brand_name}
    ${test_min_price}    ${test_max_price}    plp_api.Retry get random price range    ${min_price}    ${max_price}    ${2000}

    ${response}=    search_falcon.Api Search Product By Brand Name And Filter Price    ${test_brand_name}    ${test_min_price}    ${test_max_price}    ${language}    ${BU.lower()}    ${50}
    ${list_of_product}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*]
    Should Not Be Empty    ${list_of_product}
    
    FOR    ${dict_product}    IN    @{list_of_product}
        ${product_sell_price}    product_api.Get product sell price by json object product    ${dict_product}
        Run Keyword And Continue On Failure    Should Be True    ${test_min_price} <= ${product_sell_price} <= ${test_max_price}
    END

Verify that products are sorted by discount (high-low) - Brand Name
    [Tags]    filter    filter_api    smoke_test_revised    testrailid:C1003569
    Set Test Variable    ${test_brand_name}    ${search_by_keyword.clinique}
    ${list_of_discount}=    plp_api.Get list percent discount (high-low) by product brand name    ${test_brand_name}    ${50}
    
    ${list_of_discount_ordered_high_to_low}=    Copy List    ${list_of_discount}
    Sort List    ${list_of_discount_ordered_high_to_low}
    Reverse List    ${list_of_discount_ordered_high_to_low}
    Lists Should Be Equal    ${list_of_discount}    ${list_of_discount_ordered_high_to_low}   

Verify that products are sorted by price (low-high) - Keyword
    [Tags]    filter    filter_api    smoke_test_revised    testrailid:C1003568
    Set Test Variable    ${test_keyword}    ${search_by_keyword.cosmetic}
    ${list_of_price}=    plp_api.Get list price (low-high) by product keyword    ${test_keyword}    ${50}

    ${list_of_price_ordered_low_to_high}=    Copy List    ${list_of_price}
    Sort List    ${list_of_price_ordered_low_to_high}
    Lists Should Be Equal    ${list_of_price}    ${list_of_price_ordered_low_to_high}   

Verify the "Brand" filter must be displayed correctly - Keyword
    [Tags]    filter    filter_api    smoke_test_revised    testrailid:C1003574
    Set Test Variable    ${test_keyword}    ${search_by_keyword.pillow}
    ${test_brand_name_filter}=    Get random brand name of filter brand name by keyword    ${test_keyword}
    
    ${response}=    search_falcon.Api Search Product By Keyword And Filter Brand Name    ${test_keyword}    ${test_brand_name_filter}    ${language}    ${BU.lower()}    ${50}
    ${list_of_product}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*]
    Should Not Be Empty    ${list_of_product}

    ${list_brand_name}=    JSONLibrary.Get Value From Json    ${response}    $..brand_name
    FOR    ${brand_name}    IN    @{list_brand_name}
        Run Keyword And Continue On Failure    Should Be Equal As Strings    ${brand_name.upper()}    ${test_brand_name_filter.upper()}
    END

    ${list_brand_name_filter}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='brand_name')].buckets[*].key
    Run Keyword And Continue On Failure    List Should Contain Value    ${list_brand_name_filter}    ${test_brand_name_filter.lower()}
    ${list_filter_brand_name_product_number}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='brand_name')].buckets[*].doc_count
    Run Keyword And Continue On Failure    List Should Not Contain Value    ${list_filter_brand_name_product_number}    ${0}

Verify the "Category" filter must be displayed correctly - Category
    [Tags]    filter    filter_api    smoke_test_revised    testrailid:C1003496
    Set Test Variable    ${test_category_id}    ${category_id.woman_clothing_tops_shirt}
    ${response}=    search_falcon.Api Search Product By Category Id    ${test_category_id}    ${language}    ${BU.lower()}    ${50}

    ${list_of_product_categories}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*].categories
    Should Not Be Empty    ${list_of_product_categories}

    FOR    ${dict_categories}    IN    @{list_of_product_categories}
        ${list_categories_id}=    JSONLibrary.Get Value From Json    ${dict_categories}    $..id
        Run Keyword And Continue On Failure    List Should Contain Value    ${list_categories_id}    ${test_category_id}
    END

Verify the "Color" filter must be displayed correctly - Category
    [Tags]    filter    filter_api    smoke_test_revised    testrailid:C1003573
    Set Test Variable    ${test_category_id}    ${category_id.beauty_makeup_lips}
    ${test_color}=    plp_api.Get random color of filter color by category id    ${test_category_id}
    ${response}=    search_falcon.Api Search Product By Category Id And Filter Color    ${test_category_id}    ${test_color}    ${language}    ${BU.lower()}    ${50}
    ${list_of_product}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*]
    Should Not Be Empty    ${list_of_product}
    
    FOR    ${dict_product}    IN    @{list_of_product}
        ${list_of_color}=    Run Keyword If    '${dict_product}[type_id]'=='simple'    Set Variable   ${dict_product}[color_group_name]
        ...    ELSE IF    '${dict_product}[type_id]'=='configurable'    plp_api.Get list color of filter color by configurable product    ${dict_product}
        ...    ELSE    Fail    Incorrect 'type_id'

        ${is_contains_color_title}=    Run Keyword And Return Status    List Should Contain Value    ${list_of_color}   ${test_color.title()}
        Continue For Loop If    ${is_contains_color_title}==${True}

        ${is_contains_color_upper}=    Run Keyword And Return Status    List Should Contain Value    ${list_of_color}   ${test_color.upper()}
        Continue For Loop If    ${is_contains_color_upper}==${True}
         
        ${is_contains_color_lower}=    Run Keyword And Return Status    List Should Contain Value    ${list_of_color}   ${test_color.lower()}
        Continue For Loop If    ${is_contains_color_lower}==${True}
        
        Run Keyword And Continue On Failure    List Should Contain Value    ${list_of_color}    ${test_color.capitalize()}
    END

Verify that PDP API return simple product results correctly
    [Tags]    pdp    pdp_api    smoke_test_revised    testrailid:C1014088
    ${response}=    pdp_falcon.Api Product PDP By Url Key    ${urlkey.simple.avenda}    ${language}    ${BU.lower()}

    Run Keyword And Continue On Failure    REST.String    $..product.type_id    simple
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.sku
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.name
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.price
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.image
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.brand.name
    Run Keyword And Continue On Failure    REST.Array    $..product.extension_attributes.t1c_redeemable_points    maxItems=1
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.rating
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.total_vote
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.five_star
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.four_star
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.three_star
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.two_star
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.one_star
    Run Keyword And Continue On Failure    REST.Array    $..product.extension_attributes.reviews
    Run Keyword And Continue On Failure    REST.Null    $..product.extension_attributes.configurable_product_options
    Run Keyword And Continue On Failure    REST.Array    $..product.configurable_product_items    maxItems=0

Verify that PDP API return configurable product results correctly
    [Tags]    pdp    pdp_api    smoke_test_revised    testrailid:C1014300
    ${response}=    pdp_falcon.Api Product PDP By Url Key    ${urlkey.configurable.defry}    ${language}    ${BU.lower()}

    Run Keyword And Continue On Failure    REST.String    $..product.type_id    configurable
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.sku
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.name
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.price
    Run Keyword And Continue On Failure    REST.Null    $..product.image
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.brand.name
    Run Keyword And Continue On Failure    REST.Array    $..product.extension_attributes.t1c_redeemable_points    minItems=1
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.rating
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.total_vote
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.five_star
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.four_star
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.three_star
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.two_star
    Run Keyword And Continue On Failure    Verify field should not be null    $..product.extension_attributes.overall_rating.one_star
    Run Keyword And Continue On Failure    REST.Array    $..product.extension_attributes.reviews
    Run Keyword And Continue On Failure    REST.Array    $..product.extension_attributes.configurable_product_options    minItems=1
    Run Keyword And Continue On Failure    REST.Array    $..product.configurable_product_items    minItems=1
    
    # configurable options should be existing
    ${list_of_product_simple}=    JSONLibrary.Get Value From Json    ${response}    $..configurable_product_items..id
    FOR    ${product_simple_id}    IN    @{list_of_product_simple}
        ${dict_product_configurable_options}=    product_api.Get configurable option by product id    ${response}    ${product_simple_id}
        Should Not Be Empty    ${dict_product_configurable_options}
    END

Verify that products are sorted by new arrival products
    [Tags]    filter    filter_api    smoke_test_revised    testrailid:C1003566
    ${response}=    search_falcon.Api Search Product By Keyword And Filter Sort 'New Arrivals'    ${search_by_keyword.pillow}    ${language}    ${BU.lower()}    ${50}
    ${list_of_news_from_date}=    JSONLibrary.Get Value From Json    ${response}    $..news_from_date
    Should Not Be Empty    ${list_of_news_from_date}
    
    ${compare_date}=    Set Variable    ${list_of_news_from_date}[0]
    FOR    ${news_from_date}    IN    @{list_of_news_from_date}
        ${is_compare_date_null}    Run Keyword And Return Status    Should Be Equal    ${compare_date}    ${None}
        ${is_news_from_date_null}    Run Keyword And Return Status    Should Be Equal    ${news_from_date}    ${None}

        Run Keyword If   ${is_compare_date_null}   Run Keyword And Continue On Failure   Should Be Equal   ${news_from_date}    ${None}
        ...   ELSE IF   ${is_news_from_date_null}   Run Keyword And Continue On Failure    Should Not Be Equal    ${compare_date}    ${None}
        ...   ELSE    Run Keyword And Continue On Failure    Should Be True   '${compare_date}' >= '${news_from_date}'

        ${compare_date}=    Set Variable    ${news_from_date}
    END

Fully cancel order, paid via Credit Card
    [Tags]    e2e    e2e_api    smoke_test_revised    testrailid:C1002656    support_sit
    [Documentation]    - *Auto coupon*      : No
    ...                - *Apply coupon*     : No
    ...                - *Delivery type*    : Standard Delivery
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Credit card
    Set Test Variable    ${store}    ${store_view.name.th}
    Set Test Variable    ${encrypted_card_data}    ${credit_card_api.card_4546230000000006.encrypted_card_data}
    Set Test Variable    ${card_last4}    ${credit_card_api.card_4546230000000006.card_last4}
    # 1. create order 
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${CDS11133773.sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_ver3}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.full_payment}    ${store}
    ${entity_id}=    checkout.Member select credit card full payment method without OTP, and create a new order    ${user_token}    ${store}    ${encrypted_card_data}    ${card_last4}
    ${customerId}=    customer_api.Get customer id from api response    ${user_token}
    common_api_keywords.Call payment service api order paid full payment no saved card for app    ${entity_id}    ${customerId}    ${encrypted_card_data}
    ${order_detail_response}=    order_details.Get order details by entity_id    ${entity_id}
    ${increment_id}=    order_details.Get mdc_increment_id by order detail response    ${order_detail_response}
    order_status_api.Retry until MCOM order status should be 'LOGISTICS'    ${increment_id}
    order_status_api.Retry until MCOM order status reason should be 'READYTOSHIP'    ${increment_id}
    order_status_api.Retry until MDC order status should be 'Processing'    ${increment_id}
    order_status_api.Retry until MDC status reason should be 'READYTOSHIP'    ${increment_id}
    # 2. fully cancel
    force_shipment.Fully force cancel via MCOM by order number    ${increment_id}
    order_status_api.Retry until MCOM order status should be 'COMPLETE'    ${increment_id}
    order_status_api.Retry until MCOM order status reason should be 'FULLYCANCELLED'    ${increment_id}
    order_status_api.Retry until MDC order status should be 'MCOM_COMPLETE'    ${increment_id}
    order_status_api.Retry until MDC order status reason should be 'FULLYCANCELLED'    ${increment_id}
