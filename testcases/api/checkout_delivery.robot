*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot

*** Variables ***
${store}=    ${store_view.name.en}
${api_version}=    ${api_ver3}
@{lst_split_skus}    ${omni_sku.sku_have_2_hours_pick_up}   ${omni_sku.sku_have_standard_and_express_delivery}
@{lst_split_skus_have_at_least_sku_without_multi_shipping}    ${omni_sku.sku_have_2_hours_pick_up}    ${omni_sku.sku_have_standard_delivery_without_multi_shipping}
@{lst_split_skus_have_all_skus_without_multi_shipping}    ${omni_sku.sku_have_2_hours_pick_up_without_multi_shipping}    ${omni_sku.sku_have_standard_delivery_without_multi_shipping}
@{lst_split_skus_have_sku_with_free_item_with_multi_shipping}    ${omni_sku.sku_have_2_hours_pick_up_with_free_item_with_multi_shipping}    ${omni_sku.sku_have_standard_and_express_delivery}
@{lst_split_skus_have_sku_with_free_item_without_multi_shipping}    ${omni_sku.sku_have_2_hours_pick_up_with_free_item_without_multi_shipping}    ${omni_sku.sku_have_standard_and_express_delivery}
@{quantities}    1    1

*** Keywords ***
Get Is Allow Split Order field by estimate shipping methods with member
    [Arguments]    ${lst_skus}    ${quantities}    ${carrier_code}    ${shipping_code}
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${lst_skus}    ${quantities}    ${store}
    ${response}    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${carrier_code}    ${shipping_code}    ${store}    ${api_version}
    ${is_allow_split_order}=    Get Value From Json    ${response}[0]    $.is_allow_split_order
    [Return]    ${user_token}    ${is_allow_split_order}[0]

Verify cart should support non-split order by estimate shipping methods with member
    [Arguments]    ${lst_skus}    ${quantities}    ${carrier_code}    ${shipping_code}
    ${user_token}    ${is_allow_split_order}    Get Is Allow Split Order field by estimate shipping methods with member    ${lst_skus}    ${quantities}    ${carrier_code}    ${shipping_code}
    Should Not Be True    ${is_allow_split_order}
    ${flag}=    Run Keyword And Return Status    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    
    ...    ${shipping_methods.two_hour_pickup.method}    ${store}    ${api_version}
    Should Not Be True      ${flag}

*** Test Cases ***
Verify cart with all skus include Multi Shipping option should support split order
    [Tags]    testrailid:C1025884    Omni_api_checkout_delivery
    ${user_token}    ${is_allow_split_order}    Get Is Allow Split Order field by estimate shipping methods with member    ${lst_split_skus}    ${quantities}    
    ...    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method} 
    Should Be True    ${is_allow_split_order}

Verify cart with at least sku not include Multi Shipping option should support non-split order
    [Tags]    testrailid:C1025888    Omni_api_checkout_delivery
    Verify cart should support non-split order by estimate shipping methods with member    ${lst_split_skus_have_at_least_sku_without_multi_shipping}    ${quantities}    
    ...    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}
   
Verify cart with all skus not include Multi Shipping option should support non-split order
    [Tags]    testrailid:C1025888    Omni_api_checkout_delivery
    Verify cart should support non-split order by estimate shipping methods with member    ${lst_split_skus_have_all_skus_without_multi_shipping}    ${quantities}    
    ...    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    

Verify all skus in cart that have Multi Shipping with free item has Multi Shipping should support split order
    [Tags]    testrailid:C1025891    Omni_api_checkout_delivery
    ${user_token}    ${is_allow_split_order}    Get Is Allow Split Order field by estimate shipping methods with member    ${lst_split_skus_have_sku_with_free_item_with_multi_shipping}    ${quantities}
    ...    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method} 
    Should Be True    ${is_allow_split_order}

Verify all skus in cart that have Multi Shipping with free item has Multi Shipping should support non-split order
    [Tags]    testrailid:C1025892    Omni_api_checkout_delivery
    Verify cart should support non-split order by estimate shipping methods with member    ${lst_split_skus_have_sku_with_free_item_without_multi_shipping}    ${quantities}    
    ...    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}                        