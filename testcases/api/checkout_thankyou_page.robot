*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot
Test Teardown    Run Keywords    CommonWebKeywords.Test Teardown    AND    Close Browser

*** Variables ***
${product_type_sku}=    CDS11473459
${store}=    ${store_view.name.en}
${api_version}=    ${api_ver3}

*** Test Cases ***
Member Checkout With COD By Standard Shipping
    [Tags]    Checkout_api    Omni_api      Member
    ...    testrailid:C990793    testrailid:C990794    testrailid:C990795    testrailid:C990796    testrailid:C990797    testrailid:C990798
    ...     testrailid:C1010231     testrailid:C1010709  
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.cod}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.cod}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Member    ${increment_id}[0]      ${e2e_username_01}     ${e2e_password_01}
    thankyou_page.Get order ID after completely order

Member Checkout With Bank Transfer By Standard Shipping
    [Tags]    Checkout_api    Omni_api      Member
    ...    testrailid:C990799    testrailid:C990800    testrailid:C990801    testrailid:C990802    testrailid:C990803
    ...     testrailid:C1010232     testrailid:C1010710
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.bank_transfer}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.bank_transfer}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Member    ${increment_id}[0]      ${e2e_username_01}     ${e2e_password_01}
    thankyou_page.Get order ID after completely order

Member Checkout With Installment Via Card Info By Standard Shipping
    [Tags]    Checkout_api    Omni_api      Member
    ...     testrailid:C1010233     testrailid:C1010711
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.pay_by_installment}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.pay_by_installment}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Member    ${increment_id}[0]      ${e2e_username_01}     ${e2e_password_01}
    thankyou_page.Get order ID after completely order

Member Checkout With Full Payment Via Card Info By Standard Shipping
    [Tags]    Checkout_api    Omni_api      Member
    ...     testrailid:C1010234     testrailid:C1010712
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.full_payment}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.full_payment}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Member    ${increment_id}[0]      ${e2e_username_01}     ${e2e_password_01}
    thankyou_page.Get order ID after completely order

Member Checkout With Pay At Store By Click And Collect
    [Tags]    Checkout_api    Omni_api      Member
    ...    testrailid:C990778    testrailid:C990779    testrailid:C990780    testrailid:C990781    testrailid:C990782    testrailid:C990783    testrailid:C990786    testrailid:C990787
    ...     testrailid:C1010235     testrailid:C1010713
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${store}    ${api_version}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${shipping_address_en.store_location_id}    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.pay_at_store}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.pay_at_store}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Member    ${increment_id}[0]      ${e2e_username_01}     ${e2e_password_01}
    thankyou_page.Get order ID after completely order

Member Checkout With Full Payment Via Card Info By 2 Hour Pickup
    [Tags]    Checkout_api    Omni_api      Omni_smoke      2hrpickup      Member    smoke_test_revised
    ...    testrailid:C990759    testrailid:C990760    testrailid:C990761    testrailid:C990765    testrailid:C990767    testrailid:C990768
    ...     testrailid:C1010236     testrailid:C1010714
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${e2e_username_01}    ${e2e_password_01}    ${product_type_sku}    ${1}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${store}    ${api_version}
    checkout.Member update shipping information for 2 hour pickup    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${shipping_address_en.store_location_id}    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.full_payment}    ${store}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.full_payment}    ${store}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Member    ${increment_id}[0]      ${e2e_username_01}     ${e2e_password_01}
    thankyou_page.Get order ID after completely order

###---Guest---###
Guest Checkout With COD By Standard Shipping
    [Tags]   Checkout_api    Omni_api   Guest
    ...    testrailid:C990742    testrailid:C990744    testrailid:C990745    testrailid:C990747    testrailid:C990748
    ...    testrailid:C990750    testrailid:C990751    testrailid:C990752
    ...      testrailid:C1010237        testrailid:C1010715
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    checkout.Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.cod}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    checkout.Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.cod}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Get order ID after completely order

Guest Checkout With Bank Transfer By Standard Shipping
    [Tags]    Checkout_api    Omni_api  Guest
    ...      testrailid:C1010238        testrailid:C1010716
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Get order ID after completely order

Guest Checkout With Installment Via Card Info By Standard Shipping
    [Tags]    Checkout_api    Omni_api  Guest
    ...      testrailid:C1010239        testrailid:C1010717
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.pay_by_installment}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.pay_by_installment}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Get order ID after completely order

Guest Checkout With Full Payment Via Card Info By Standard Shipping
    [Tags]    Checkout_api    Omni_api  Guest
    ...      testrailid:C1010240        testrailid:C1010718
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.standard.method}    ${shipping_methods.standard.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.full_payment}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.full_payment}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Get order ID after completely order

Guest Checkout With Pay At Store By Click And Collect
    [Tags]    Checkout_api    Omni_api  Guest
    ...      testrailid:C1010241        testrailid:C1010719
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    checkout.Guest Update Shipping Method Content    ${quote_id}    ${shipping_methods.click_n_collect.method}    ${shipping_methods.click_n_collect.carrier}     ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.pay_at_store}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.pay_at_store}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Get order ID after completely order

Guest Checkout With Bank Transfer By 2 Hour Pickup
    [Tags]    Checkout_api    Omni_api      Omni_smoke      2hrpickup     Guest    smoke_test_revised
    ...      testrailid:C1010242        testrailid:C1010720
    ${quote_id}=    Genarate cart guest token    ${store}
    Guest Add Product To Cart    ${quote_id}    ${store}    ${product_type_sku}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    Guest Update Shipping Method Content For 2 Hour Pickup    ${quote_id}    ${shipping_methods.two_hour_pickup.method}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_address_en.region_id}    ${shipping_address_en.store_location_id}    ${store}
    ${enity_id}=    Guest Select A Specific PayMent Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${store}
    ${enity_id}=    Guest Create A New Order By Payment Method By API    ${quote_id}    ${payment_methods.bank_transfer}    ${personal_checkout_delivery.username}    ${BU}    ${shipping_address_en.region_id}    ${store}
    ${order_id}=    order_details.Get order id by entity_id    ${enity_id}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    common_keywords.Go To Thank You Page With Order Id By Guest    ${increment_id}[0]
    thankyou_page.Get order ID after completely order