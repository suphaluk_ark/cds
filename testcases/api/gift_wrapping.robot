*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot
Test Template    Template - Verify Gift Wrapping

*** Keywords ***
Template - Verify Gift Wrapping
    [Arguments]    ${order_id}    ${expect_id}
    ${response}=    order_details.Get order details integration by order id    ${order_id}
    ${gw_id}=    Get Value From Json    ${response}    $..gw_id
    ${count}=    Get Length    ${gw_id}
    ${gw_id}=    Run Keyword If    ${count}>0    Set Variable    ${gw_id}[0]
    ...    ELSE    Set Variable    ${None}
    Should Be Equal    ${gw_id}    ${expect_id}

*** Test Cases ***
Verify Gift Wrapping Selected And Gift Message
    [Tags]    testrailid:C125856
    STGCO1901040001350    1
    STGCO1901040001365    1

Verify Gift Wrapping Selected And No Gift Message
    [Tags]    testrailid:C125857
    STGCO1901040001359    1
    STGCO1901040001366    1

Verify No Gift Wrapping Selected
    [Tags]    testrailid:C125858
    STGCO1901040001362    ${None}
    STGCO1901040001369    ${None}