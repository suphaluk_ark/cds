*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile/mobile_imports.robot

*** Variables ***
${brand_name}    ${brand_1.name}
${keyword}    ${brand_1.category_4}
${minimum_product_name}    ${brand_3.product_1.name}
${minimum_product_price}    ${brand_3.product_1.price}
${minimum_product_price_tbh}    ${brand_3.product_1.price_thb}
${maximum_product_name}    ${brand_3.product_2.name}
${maximum_product_price}    ${brand_3.product_2.price}
${maximum_product_price_thb}    ${brand_3.product_2.price_thb}

*** Test Cases ***
To verify Main Category (Non Beauty)
    [Tags]     mobile_app    android    product_category    testrailid:T397830
    [Setup]    common_keywords.Setup Open application
    home_page.Click on kid brand
    main_category_page.Verify Category button should be displayed
    main_category_page.Verify Brands button should be displayed
    main_category_page.Verify New Arrivals should be displayed
    main_category_page.Verify Central Inspirer should be displayed
    [Teardown]    Close Application

To verify Main Category - Sub Category Drop Downlist
    [Tags]     mobile_app    android    product_category    testrailid:T397831
    [Setup]    common_keywords.Setup Open application
    home_page.Click on kid brand
    main_category_page.Click Category button
    #Will verify Close button should be displayed - this button is not available now.
    main_category_page.Click New Arrivals at Central Pick section
    main_category_page.Verify New Arrivals page should be displayed
    main_category_page.Click back button
    main_category_page.Click Category button
    main_category_page.Click Online Exclusive at Central Pick section
    main_category_page.Verify Online Exclusive page should be displayed
    main_category_page.Click back button
    main_category_page.Click Category button
    Click on All kids category
    main_category_page.Verify sub-category should be displayed  ${all_kid_sub_category.maternity}
    main_category_page.Verify sub-category should be displayed  ${all_kid_sub_category.baby_essentials}
    #[EX3] No menu “View All xxx” on Level 2 and display “All xxx” after level 2 - 5
    [Teardown]    Close Application

To verify Main Category - Brands Drop Downlist
    [Tags]     mobile_app    android    product_category    testrailid:T397832
    [Setup]    common_keywords.Setup Open application
    #alphabet on the right side - cannot get the locator in appium
    home_page.Click on kid brand
    main_category_page.Click Brands button
    main_category_page.Search a brand  ${brand_4.name}
    main_category_page.Verify search close button should be displayed
    #[EX3] Close button will hide “View By Brands” pop-up => "view by brands" does not hide in UI
    [Teardown]    Close Application

To verify Main Category - Hero Banner
    [Tags]     mobile_app    android    product_category    testrailid:T397833
    [Setup]    common_keywords.Setup Open application
    home_page.Click on kid brand
    main_category_page.Verify banner page should be displayed
    #wait for dev to set hero banner name [EX1] Hero banners should display correctly and navigate to the Deeplink / Webview which config on MDC
    main_category_page.Verify banner indicator should be displayed
    [Teardown]    Close Application

To verify Main Category - New Arrivals and Best Deals
    [Tags]     mobile_app    android    product_category    testrailid:T397834
    [Setup]    common_keywords.Setup Open application
    home_page.Click on beauty brand
    main_category_page.Verify New Arrivals should be displayed
    main_category_page.Verify Best Deal should be displayed
    #use API to verify New Arrivals & Best Deals display maximum 20 items in 2 rows 
    [Teardown]    Close Application

To verify Main Category - Central Inspirer
    [Tags]     mobile_app    android    product_category    testrailid:T397835
    [Setup]    common_keywords.Setup Open application
    home_page.Click on kid brand
    main_category_page.Click on a card under Central Inspirer section  0
    main_category_page.Verify URL of the page should be correct
    [Teardown]    Close Application

To verify Main Category - Gift with purchase
    [Tags]     mobile_app    android    product_category    testrailid:T397836
    [Setup]    common_keywords.Setup Open application
    home_page.Click on beauty brand
    main_category_page.Verify Gift with purchase section should be displayed
    main_category_page.Verify a gift should be displayed in gift with purchase section  ${gift_with_purchase_1}
    [Teardown]    Close Application

*** Keywords ***
Click on All kids category 
    main_category_page.Click on a category   ${mobile_main_category_page.all_kids}