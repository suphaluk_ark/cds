*** Setting ***
Resource    ${CURDIR}/../../keywords/mobile/mobile_imports.robot

*** Variables ***
${cust_name}    ${personal_information.firstname} ${personal_information.lastname}

*** Test Cases ***
Login to CDS application success
    [Tags]     login
    [Setup]     common_keywords.Setup Open application
    Run Keyword And Ignore Error    welcome_page.Click on TH language button
    Login to CDS with personal account    ${personal_username}    ${personal_password}     ${cust_name}
    [Teardown]    Remove Application     ${${mobileDevice}}[pagkageName]
