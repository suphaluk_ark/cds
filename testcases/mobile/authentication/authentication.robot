*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile/mobile_imports.robot

*** Variables ***
${empty_email}  ${EMPTY}
${empty_password}  ${EMPTY}
${valid_password}    1QAZxsw2
${valid_email}  ${personal_username_16}
${wrong_email_format}     tester@gg
${wrong_email_format1}     test
${wrong_email_format2}     tester@
${wrong_email_format3}     test@g
${wrong_email_format4}     @gmil.com
${wrong_email_format5}     test#gmail.com

${register_user1}  mobile_user123@mail.com
${un_register_user}  not_mobile_user123@mail.com
${register_password1}  Cto1234!

*** Test Cases ***
To verify incorrect email & password will trigger a correct message
    [Tags]     mobile_app    android    authentication    testrailid:T713617
    [Setup]    common_keywords.Setup Open application
    Go to login page
    login_page.Input username    ${personal_username_16}
    login_page.Input password    wrong_${personal_password_16}
    login_page.Click login button
    #This verification point is failed due to the error message not correct. This bug has been raised on test rail.
    login_page.Verify incorrect account message should be displayed
    [Teardown]    Close Application

To verify account doesn't exist in system will trigger a correct message
    [Tags]     mobile_app    android    authentication    testrailid:T713618
    [Setup]    common_keywords.Setup Open application
    Go to login page
    login_page.Input username    un_existed_${personal_username_16}
    login_page.Input password    wrong_${personal_password_16}
    login_page.Click login button
    #This verification point is failed due to the error message not correct. This bug has been raised on test rail.
    login_page.Verify incorrect account message should be displayed
    [Teardown]    Close Application

To verify Login - Facebook successfully
    [Tags]     mobile_app    android    authentication    testrailid:T808265
    [Setup]    common_keywords.Setup Open application
    #unable to implement because the login with Facebook page is not available
    Go to login page
    login_page.Click login with Facebook
    
    [Teardown]    Close Application

To verify empty email will trigger a correct message
    [Tags]     mobile_app    android    authentication    testrailid:T713614
    [Setup]    common_keywords.Setup Open application
    Go to login page
    login_page.Input username    something
    login_page.Input username    ${empty_email}
    login_page.Input password    ${valid_password}
    login_page.Verify error message should be displayed  ${mobile_login_page.msg_empty_email}
    login_page.Verify login button should be disabled
    [Teardown]    Close Application

To verify empty password will trigger a correct message
    [Tags]     mobile_app    android    authentication    testrailid:T713615
    [Setup]    common_keywords.Setup Open application
    Go to login page
    login_page.Input username    ${valid_email}
    login_page.Input password    something
    login_page.Input password    ${empty_password}
    login_page.Verify error message should be displayed  ${mobile_login_page.msg_empty_password}
    login_page.Verify login button should be disabled
    [Teardown]    Close Application

To verify invalid email format will trigger a correct message
    [Tags]     mobile_app    android    authentication    testrailid:T713616
    [Setup]    Run Keywords  common_keywords.Setup Open application    AND    Go to login page
    [Template]    Verify error message will display correctly with wrong email format
    ${wrong_email_format}     ${mobile_login_page.msg_wrong_email_format}
    ${wrong_email_format1}     ${mobile_login_page.msg_wrong_email_format}
    ${wrong_email_format2}     ${mobile_login_page.msg_wrong_email_format}
    ${wrong_email_format3}     ${mobile_login_page.msg_wrong_email_format}
    ${wrong_email_format4}     ${mobile_login_page.msg_wrong_email_format}
    ${wrong_email_format5}     ${mobile_login_page.msg_wrong_email_format}
    [Teardown]    Close Application

To verify Login - Email Success
    [Tags]     mobile_app    android    authentication    testrailid:T742122
    [Setup]    common_keywords.Setup Open application
    Go to login page
    login_page.Input username    ${personal_username_16}
    login_page.Input password    ${personal_password_16}
    login_page.Click on show password icon
    login_page.Verify show password should be checked
    login_page.Verify password should be visible
    login_page.Click login button
    myaccount_page.Customer firstname and lastname should be displayed correctly    ${personal_information_16.firstname} ${personal_information_16.lastname} 
    [Teardown]  Run Keywords   myaccount_page.Click logout button    AND    Close Application

To verify register form displays as mockup
    [Tags]     mobile_app    android    authentication    testrailid:T808266
    [Setup]    common_keywords.Setup Open application
    Go to login page
    login_page.Click register button
    register_page.Click login section
    login_page.Click register now
    register_page.Verify first name should be displayed
    register_page.Verify last name should be displayed
    register_page.Verify username should be displayed
    register_page.Verify password should be displayed
    [Teardown]    Close Application

To verify forget password successfully
    [Tags]     mobile_app    android    authentication    testrailid:T808267
    [Setup]    common_keywords.Setup Open application
    Go to login page
    login_page.Click on forgot password button
    forget_account_page.Input registered email  ${valid_email}
    forget_account_page.Click submit button
    forget_account_page.Verify sent reset password link message should be displayed
    forget_account_page.Verify resend it button should be displayed
    forget_account_page.Click go to login button
    login_page.Verify login button should be disabled
    [Teardown]    Close Application

To verify register successfully and account is created on MDC
    [Tags]     mobile_app    android    authentication    testrailid:T870507
    [Setup]    common_keywords.Setup Open application
    Run Keyword And Ignore Error    Run Keywords    Delete the registered user 1
    Go to login page
    login_page.Click register button
    register_page.Input first name  first
    register_page.Input last name  last
    register_page.Input username  ${register_user1}
    register_page.Input password  ${register_password1}
    register_page.Click register button
    register_page.Verify register succesfully message should be displayed  ${mobile_login_page.msg_success_register}
    register_page.Click continue shopping button
    [Teardown]    Run Keywords    Delete the registered user 1    AND    Close Application

To verify forget password failed
    [Tags]     mobile_app    android    authentication    testrailid:T872903
    [Setup]    common_keywords.Setup Open application
    Go to login page
    login_page.Click on forgot password button
    forget_account_page.Input registered email  ${un_register_user}
    forget_account_page.Click submit button
    forget_account_page.Verify not registered email error message should be displayed
    [Teardown]    Close Application

*** Keywords ***
Go to login page
    home_page.Click on view profile icon
    login_page.Click login button

Verify error message will display correctly with wrong email format
    [Arguments]  ${email}  ${error_msg}
    login_page.Input username    ${email}
    login_page.Verify error message should be displayed  ${error_msg}

Delete the registered user 1
    ${customer_id}=    customer_details.Get customer ID  ${register_user1}  ${register_password1}
    customer_details.Delete customer by customer ID  ${customer_id}  200