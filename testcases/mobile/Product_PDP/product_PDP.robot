*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile/mobile_imports.robot

*** Test Cases ***
To verify PDP Page - Product ID (Verify The 1 Point)
    [Tags]     mobile_app    android    product_association    testrailid:T763609
    [Setup]    common_keywords.Setup Open application
    common_keywords.Search and go to a product detail  ${mobile_product_1.name}
    product_detail_page.Verify the 1 should be displayed  ${mobile_product_1.the_1}
    [Teardown]    Close Application

To verify PDP Page - Product ID (Delivery Option)
    [Tags]     mobile_app    android    product_association    testrailid:T763610
    [Setup]    common_keywords.Setup Open application
    common_keywords.Search and go to a product detail  ${mobile_product_1.name}
    product_detail_page.Verify product id should be displayed  ${mobile_product_1.id}
    product_detail_page.Verify sold by label should be displayed
    product_detail_page.Verify product seller should be displayed  ${mobile_product_1.seller}
    product_detail_page.Verify click and collect should be displayed
    product_detail_page.Verify express delivery should be displayed
    product_detail_page.Verify return exchange should be displayed
    product_detail_page.Verify gift wrapping should be displayed
    product_detail_page.Verify pay by installment should be displayed
    product_detail_page.Verify pay on delivery should be displayed
    [Teardown]    Close Application
