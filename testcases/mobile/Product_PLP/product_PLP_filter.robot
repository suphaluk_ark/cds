*** Setting ***
Resource    ${CURDIR}/../../../keywords/mobile/mobile_imports.robot

*** Variables ***
${brand_name}    ${brand_1.name}
${keyword}    ${brand_1.category_4}
${minimum_product_name}    ${brand_3.product_1.name}
${minimum_product_price}    ${brand_3.product_1.price}
${minimum_product_price_tbh}    ${brand_3.product_1.price_thb}
${maximum_product_name}    ${brand_3.product_2.name}
${maximum_product_price}    ${brand_3.product_2.price}
${maximum_product_price_thb}    ${brand_3.product_2.price_thb}

*** Test Cases ***
To verify Brand works correctly
    [Tags]     mobile_app    android    product_PLP_filter    testrailid:T763688
    [Setup]    common_keywords.Setup Open application
    home_page.Click on Brand icon
    product_filter_page.Click on a brand    ${brand_name}
    product_filter_page.Click on filter button
    product_filter_page.Click on filter by category
    product_filter_page.Input text for Filter by category  ${keyword}
    product_filter_page.Verify category should be displayed after search  ${keyword}
    product_filter_page.Click search close button
    product_filter_page.Click on apply filters button
    product_filter_page.Click on filter button
    product_filter_page.Click on filter by category
    Select category 1 on the list
    Select category 2 on the list
    Select category 3 on the list
    product_filter_page.Click on apply filters button
    product_filter_page.Click on filter button
    product_filter_page.Click on filter by category
    product_filter_page.Click on reset button
    Select category 1 on the list
    Select category 2 on the list
    Select category 3 on the list
    product_filter_page.Click on apply filters button
    product_filter_page.Click on filter button
    product_filter_page.Click on filter by category
    product_filter_page.Click on back button
    [Teardown]    Close Application

To verify Price works correctly
    [Tags]     mobile_app    android    product_PLP_filter    testrailid:T763689
    [Setup]    common_keywords.Setup Open application
    home_page.Click on Brand icon
    product_filter_page.Click on a brand    ${brand_3.name}
    product_filter_page.Verify the minimum price should be displayed on the product list  ${minimum_product_name}  ${minimum_product_price}
    product_filter_page.Verify the maximum price should be displayed on the product list  ${maximum_product_name}  ${maximum_product_price}
    product_filter_page.Click on filter button
    product_filter_page.Verify the minimum price should be displayed on price range bar  ${minimum_product_price_tbh}
    product_filter_page.Verify the maximum price should be displayed on price range bar  ${maximum_product_price_thb}
    # Write script to verify that Price range can slide left&right to decrease & increase the price and able to collapse
    [Teardown]    Close Application

To verify Color works correctly
    [Tags]     mobile_app    android    product_PLP_filter    testrailid:T763690
    [Setup]    common_keywords.Setup Open application
    home_page.Click on Brand icon
    product_filter_page.Click on a brand    ${brand_2.name}
    product_filter_page.Click on filter button
    product_filter_page.Click on color button
    product_filter_page.Verify color and quantity should be displayed correctly  ${brand_2.color_1.name}  ${brand_2.color_1.quantity}
    product_filter_page.Verify color and quantity should be displayed correctly  ${brand_2.color_2.name}  ${brand_2.color_2.quantity}
    product_filter_page.Click a color on the list  ${brand_2.color_1.name}
    product_filter_page.Click a color on the list  ${brand_2.color_2.name}
    product_filter_page.Verify color stack should be displayed correctly  ${brand_2.color_1.name}, ${brand_2.color_2.name}
    [Teardown]    Close Application

*** Keywords ***
Select category 1 on the list
    product_filter_page.Select a category on the list  ${brand_1.category_1}

Select category 2 on the list
    product_filter_page.Select a category on the list  ${brand_1.category_2}

Select category 3 on the list
    product_filter_page.Select a category on the list  ${brand_1.category_3}