*** Keywords ***
Calculate Multiple Discounts
    [Arguments]    ${total}    &{optionals_kwargs}
    ${result}    Convert To Number    ${total}
    FOR    ${key}    IN    @{optionals_kwargs.keys()}
        ${result}    Run Keyword If    'percent' in '${key}'    Calculate Discount Percent    ${result}    ${optionals_kwargs}[${key}]
        ...    ELSE IF    'amount' in '${key}'    Calculate Discount Amount    ${result}    ${optionals_kwargs}[${key}]
        ...    ELSE    Set Variable    ${result}
    END
    [Return]    ${result}

Calculate Discount Percent
    [Arguments]    ${total}    ${discount_percent}
    ${result}    Evaluate    ${total} - ( ${total} * ${discount_percent} ) / 100
    [Return]    ${result}

Calculate Discount Amount
    [Arguments]    ${total}    ${discount_amount}
    ${result}    Evaluate    ${total} - ${discount_amount}
    [Return]    ${result}

Calculate total price by summary product price with quantity
    [Arguments]    ${price}    ${qty}
    ${total_price}=    Evaluate    round(${price}*${qty}, 2)
    [Return]    ${total_price}

Calculate order total with deduct discount
    [Arguments]    ${total_price}    &{discounts}
    ${actual_price}=    promotion_calculator.Calculate Multiple Discounts    ${total_price}    &{discounts}
    [Return]    ${actual_price}