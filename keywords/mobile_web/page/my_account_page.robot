*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/my_account_page.robot

*** Keywords ***
Click on Edit Address button
    CommonWebKeywords.Scroll And Click Element    ${dictMyAccountPage}[btn_edit_address]

Click on Edit Profile button
    CommonWebKeywords.Scroll And Click Element    ${dictMyAccountPage}[btn_edit_profile]