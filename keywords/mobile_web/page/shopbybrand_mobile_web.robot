*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot

*** Variables ***
&{brandlist}
...    brand_name=xpath=//a[@to='/{$brand_name}']

*** Keywords ***
Click on brand
    [Arguments]    ${brand_name}
    ${elem}=    CommonKeywords.Format Text    ${brandlist}[brand_name]    $brand_name=${brand_name}
    CommonWebKeywords.Scroll And Click Element    ${elem}
