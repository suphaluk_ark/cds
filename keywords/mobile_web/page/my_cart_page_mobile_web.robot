*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/my_cart_page.robot

*** Keywords ***
Verify total price with quantity, summarize multiple product in cart page
    [Arguments]    @{product_sku}
    ${summary_price}=    Create List
    FOR    ${product}    IN    @{product_sku}
        ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[ddl_quantity]    $skunumber=${product}
        ${quantity}=    SeleniumLibrary.Get Selected List Label    ${elem}
        Set Test Variable    ${quantity_${product}}    ${quantity}
        ${product_price}=    Convert To Number    ${${product}}[price]
        calculation.Summarize price with quantity    ${quantity_${product}}    ${product_price}
        Collections.Append To List    ${summary_price}    ${price_with_quantity}
    END
    ${total_price_with_quantity}=    Evaluate    sum($summary_price)
    ${total_price_with_quantity_format}=    Run Keyword If    ${total_price_with_quantity}.is_integer()    Convert To Integer    ${total_price_with_quantity}
    ...    ELSE IF    CommonKeywords.Convert price to total amount format    ${total_price_with_quantity}
    Set Test Variable    ${total_price_with_quantity}    ${total_price_with_quantity_format}

Change quantity of product from the drop-down list
    [Arguments]    ${skunumber}    ${quantity}
    Set Test Variable    ${product_quantity}    ${quantity}
    ${elem}=  CommonKeywords.Format Text    ${dictMyCartPage}[ddl_quantity]    $skunumber=${sku_number}
    ${quantity}    BuiltIn.Convert To String    ${quantity}
    SeleniumLibrary.Select From List By Value    ${elem}    ${quantity}
