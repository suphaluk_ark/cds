*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot

*** Variables ***
&{homepagelist}
...    search_box_mobile=xpath=(//input[@id='txt-searchProductOnSearchBar'])[2]
...    hamburger_menu=xpath=//a[@data-testid='mnu-viewMenu-hamburger']
...    sale_cate=xpath=//div[@data-testid='mnu-viewMenuOnmobile-sale']
...    brand_cate=xpath=//div[@data-testid='mnu-viewMenuOnmobile-brand']
...    beauty_cate=xpath=//div[@data-testid='mnu-viewMenuOnmobile-beauty']
...    makeup_sub_cate=xpath=//div[@data-testid='mnu-viewMenuOnmobile-makeup']
...    view_all=xpath=//a[@data-testid='mnu-viewMenuOnmobile-viewAll']
...    women_cate=xpath=//*[contains(@data-testid, 'mnu-viewMenuOnmobile-women')]
...    login_register_lbl=xpath=//label[contains(text(),'${mobile_web_common}[login_register]')]
...    lbl_hello_first_name=xpath=//*[starts-with(@data-testid, 'mnu-viewMenu')][1]/../preceding-sibling::*/div
...    lbl_my_account=xpath=//div[contains(text(),'${mobile_web_common}[my_account]')]
...    lbl_my_order=xpath=//div[contains(text(),'${mobile_web_common}[my_order]')]
...    lbl_sign_out=xpath=//div[contains(text(),'${mobile_web_common}[sign_out]')]
...    lbl_wishlist=xpath=//div[contains(text(),'${wishlist_page.title}')]
...    img_wishlist_icon=xpath=(//a[@to='/account/wishlist'])[2]
...    img_central_icon=xpath=(//*[@to='/']//*[local-name()='svg'])[2]
...    img_wishlist_icon_not_loggin=xpath=//a[@to='/wishlist']
...    popup=css=#btn-popup-close
...    lbl_track_orders=xpath=//div[contains(text(),'${web_common.track_your_orders}')]
...    btn_back=xpath=//span[text()='${web_common.back}']
...    select_language=xpath=//img[contains(@src,'languages')]/following-sibling::select
...    btn_x=xpath=(//*[@id='layout']/div)[4]/*[@viewBox]
       

*** Keywords ***
Click on search box for mobile web
   CommonWebKeywords.Click Element     ${homepagelist}[search_box_mobile]

Input text into search box for mobile web
    [Arguments]    ${text}
    CommonWebKeywords.Click Element    ${homepagelist}[search_box_mobile]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${homepagelist}[search_box_mobile]    ${text}

Press Enter key to search a product
    SeleniumLibrary.Press Keys    None    RETURN

Click on hamburger menu
    CommonWebKeywords.Click Element     ${homepagelist}[hamburger_menu]

Click on beauty category
    CommonWebKeywords.Click Element     ${homepagelist}[beauty_cate]

Click on makeup sub category
    CommonWebKeywords.Click Element     ${homepagelist}[makeup_sub_cate]

Click on sale category
    CommonWebKeywords.Click Element     ${homepagelist}[sale_cate]

Click view all sub category
    CommonWebKeywords.Click Element     ${homepagelist}[view_all]

Click on women category
    CommonWebKeywords.Click Element     ${homepagelist}[women_cate] 

Click on Login/Register lable
    CommonWebKeywords.Click Element     ${homepagelist}[login_register_lbl]

Verify customer Firstname is displayed correctly
    [Arguments]    ${first_name}
    home_page_mobile_web_keywords.Click on hamburger menu
    CommonWebKeywords.Verify Web Element Text Should Contain  ${homepagelist}[lbl_hello_first_name]    ${first_name}
    home_page_web_keywords.Click on brand

Click on Hi First Name lable
    CommonWebKeywords.Click Element     ${homepagelist}[lbl_hello_first_name]

Click on My Order
    CommonWebKeywords.Click Element     ${homepagelist}[lbl_my_order]

Click on My Account
    CommonWebKeywords.Click Element     ${homepagelist}[lbl_my_account]

Close Popup
    SeleniumLibrary.Wait Until Page Contains Element   ${homepagelist}[popup]
    SeleniumLibrary.Click Element    ${homepagelist}[popup]

Click on Wishlist
    CommonWebKeywords.Click Element     ${homepagelist}[lbl_wishlist]

Click on Wishlist icon
    CommonWebKeywords.Click Element    ${homepagelist}[img_wishlist_icon]

Click on Central icon
    CommonWebKeywords.Click Element    ${homepagelist}[img_central_icon]

Click on Wishlist icon when user not login
    CommonWebKeywords.Click Element  ${homepagelist}[img_wishlist_icon_not_loggin]

Click on Sign Out
    CommonWebKeywords.Click Element    ${homepagelist}[lbl_sign_out]

Click on Track Your Orders
    CommonWebKeywords.Scroll To Element    ${homepagelist}[lbl_track_orders]
    CommonWebKeywords.Click Element     ${homepagelist}[lbl_track_orders]

Click on Back button
    CommonWebKeywords.Click Element    ${homepagelist}[btn_back]

Get current language
    CommonWebKeywords.Scroll To Element    ${homepagelist}[select_language]
    ${lang}=    SeleniumLibrary.Get Selected List Label    ${homepagelist}[select_language]
    [Return]    ${lang}

Select language by label
    [Arguments]    ${label}
    CommonWebKeywords.Scroll To Element    ${homepagelist}[select_language]
    SeleniumLibrary.Select From List By Label    ${homepagelist}[select_language]    ${label}

Click x button
    CommonWebKeywords.Click Element    ${homepagelist}[btn_x]

