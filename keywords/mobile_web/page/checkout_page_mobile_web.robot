*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/checkout_page.robot

*** Keywords ***
Click view details 
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_view_details]

Click view detail
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_view_detail]

Verify redeem point display correctly
    [Arguments]    ${point}    ${discount}
    CommonWebKeywords.Click Element Using Javascript    ${dictCheckoutPage}[btn_view_details]
    ${elem1}=    Format Text    ${dictCheckoutPage}[txt_redeem_point_summary]    $point=${point}
    ${elem2}=    Format Text    ${dictCheckoutPage}[txt_discount_redeem]    $discount=${discount}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem1}    ${elem2}
    CommonWebKeywords.Click Element Using Javascript    ${dictCheckoutPage}[btn_hide_detail]