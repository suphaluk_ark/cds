*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/category_PLP.robot

*** Keywords ***
Click subcategory clothing
    SeleniumLibrary.Wait Until Page Contains Element    ${dictCategoryPLP}[lbl_clothing]
    CommonWebKeywords.Click Element    ${dictCategoryPLP}[lbl_clothing]
    