*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/registration_page.robot

*** Keywords ***
Open Login form from register page
    Run Keyword And Ignore Error    home_page_mobile_web_keywords.Close Popup
    CommonWebKeywords.click Element Using Javascript    ${dictRegistrationPage}[btn_open_login_form]