*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot

*** Variables ***
&{dictProductListPage}
...    badge_homedelivery_enable=xpath=//div[@data-testid='icon-delivery-badge-delivery-enable-{$sku_number}']
...    badge_clickncollect_enable=xpath=//div[@data-testid='icon-delivery-badge-click-collect-enable-{$sku_number}']
...    badge_homedelivery_disable=xpath=//div[@data-testid='icon-delivery-badge-delivery-disable-{$sku_number}']
...    badge_clickncollect_disable=xpath=//div[@data-testid='icon-delivery-badge-click-collect-disable-{$sku_number}']
...    badge_3hourdelivery_enable=xpath=//div[@data-testid='icon-delivery-badge-3hour-delivery-enable-{$sku_number}']
...    badge_3hourdelivery_disable=xpath=//div[@data-testid='icon-delivery-badge-3hour-delivery-disable-{$sku_number}']
...    product_img=xpath=//img[@data-product-id='{$sku_number}']
...    color_swatch=xpath=(//img[@data-product-id='{$sku_number}']//following::div[@id='color-swatch-slider']//..//p)
...    shop_all_product=xpath=//a[@href='/{$product_name}/products']

...    text_section=xpath=//*[text()='{$text_section}']
...    text_section_homedelivery_enable=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-delivery-enable-{$sku_number}']
...    text_section_clickncollect_enable=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-click-collect-enable-{$sku_number}']
...    text_section_homedelivery_disable=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-delivery-disable-{$sku_number}']
...    text_section_clickncollect_disable=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-click-collect-disable-{$sku_number}']

...    btn_filter=xpath=//*[text()='${product_list_page.filters}']
...    btn_delivery_methods=xpath=//*[@data-testid="sel-filterPLPOnmobile-shipping_delivery_methods.delivery_method"]
...    btn_x=xpath=//*[text()='${product_list_page.filters}']/parent::*/*[@viewBox]
...    btn_category_arrow_up=xpath=(//*[@id='sel-formFilter-category-2'])[2]//img[contains(@src,'arrow-up')]
...    btn_brand_name_arrow_up=xpath=(//*[@id='brand_name'])[2]//img[contains(@src,'arrow-up')]
...    btn_price_range_arrow_up=xpath=(//*[@data-testid='price_range'])[2]//img[contains(@src,'arrow-up')]
...    a_count_product=xpath=//*[@data-product-sku][@data-index]

*** Keywords ***
Verify home delivery badge enable should be visible on flip card
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[badge_homedelivery_enable]    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify home delivery disable should be visible on flip card
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[badge_homedelivery_disable]    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify click & collect badge enable should be visible on flip card
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[badge_clickncollect_enable]    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify click & collect badge disable should be visible on flip card
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[badge_clickncollect_disable]    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify product image should visible on flip card
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[product_img]    $sku_number=${product_sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${elem}

Verify color swatch should visible on flip card
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[color_swatch]    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click shop all products
    [Arguments]    ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[shop_all_product]    $product_name=${product_name}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Click product flip card
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[product_img]    $sku_number=${product_sku}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Scroll to text section
    [Arguments]    ${text_section}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[text_section]    $text_section=${text_section}
    CommonWebKeywords.Scroll to bottom page
    CommonWebKeywords.Scroll To Element    ${elem}

Verify home delivery badge enable should be visible under text section
    [Arguments]    ${text_section}    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[text_section_homedelivery_enable]    $text_section=${text_section}    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify home delivery badge disable should be visible under text section
    [Arguments]    ${text_section}    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[text_section_homedelivery_disable]    $text_section=${text_section}    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify click & collect badge enable should be visible under text section
    [Arguments]    ${text_section}    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[text_section_clickncollect_enable]    $text_section=${text_section}    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify click & collect badge disable should be visible under text section
    [Arguments]    ${text_section}    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[text_section_clickncollect_disable]    $text_section=${text_section}    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify detail tab in PDP should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictProductListPage}[detail_product]

Click filter button
    CommonWebKeywords.Scroll And Click Element    ${dictProductListPage}[btn_filter]

Click delivery methods button
    CommonWebKeywords.Scroll And Click Element    ${dictProductListPage}[btn_delivery_methods]

Click X button 
    CommonWebKeywords.Scroll And Click Element    ${dictProductListPage}[btn_x]

Click on category arrow-up button
    CommonWebKeywords.Click Element    ${dictProductListPage}[btn_category_arrow_up]

Click on brand name arrow-up button
    CommonWebKeywords.Click Element    ${dictProductListPage}[btn_brand_name_arrow_up]

Click on price range arrow-up button
    CommonWebKeywords.Click Element    ${dictProductListPage}[btn_price_range_arrow_up]

Count all products in search result page
    ${count}=    SeleniumLibrary.Get Element Count  ${dictProductListPage}[a_count_product]
    [Return]    ${count}

Verify that category PLP is displayed correctly
    SeleniumLibrary.Wait Until Page Contains Element    ${dictCategoryPLP}[txt_clothing]
    ${text} =    SeleniumLibrary.Get Text    ${dictCategoryPLP}[txt_clothing]
    BuiltIn.Should Be Equal As Strings    ${text}    ${category_plp.subcategory.subcategory_name_clothing}    msg= expected ${category_plp.subcategory.subcategory_name_clothing} but actual result is ${text}

Verify 3hour delivery badge enable should be visible on flip card
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[badge_3hourdelivery_enable]    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 3hour delivery badge disable should be visible on flip card
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictProductListPage}[badge_3hourdelivery_disable]    $sku_number=${product_sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}