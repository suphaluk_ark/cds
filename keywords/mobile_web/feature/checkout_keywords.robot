*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot

*** Keywords ***
Checkout banktranfers for guest member
    checkout_page.Select Payment Method 1 2 3 bank transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    &{change_shipping_address}[telephone]
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible