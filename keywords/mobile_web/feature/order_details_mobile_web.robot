*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot


*** Keywords ***
Login, Add product to Cart and Go to Checkout page
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}    ${sku1}=${non_cart_price_rule_sku.credit_card['item_1']}
    login_keywords.Login Keywords for mobile web - Login Success  ${login_username}  ${login_password}  ${login_display_name}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Remove all product in shopping cart
    common_cds_mobile_web.Search product, add product to cart, go to checkout page  ${sku1}

Login, Add multiple product to Cart and Go to Checkout page
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}    ${sku1}=${non_cart_price_rule_sku.credit_card['item_1']}    ${sku2}=${non_cart_price_rule_sku.credit_card['item_2']}
    login_keywords.Login Keywords for mobile web - Login Success  ${login_username}  ${login_password}  ${login_display_name}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Remove all product in shopping cart
    common_keywords.Go To PDP Directly By Product Sku  ${sku1}
    product_page.Click on add to cart button    ${sku1}
    common_keywords.Go To PDP Directly By Product Sku  ${sku2}
    product_page.Click on add to cart button    ${sku2}
    Header Web - Click Mini Cart Button
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible

Successful pay with Credit Card
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card

Successful pay with COD method
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method COD
    Wait Until Page Is Completely Loaded
    checkout_page.Click Confirm Order For COD Payment Method

Create order for verify order details with cod
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    Login, Add product to Cart and Go to Checkout page  ${login_username}  ${login_password}  ${login_display_name}
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    Successful pay with COD method

Create order for verify order details with credit card
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    Login, Add product to Cart and Go to Checkout page  ${login_username}  ${login_password}  ${login_display_name}
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Successful pay with Credit Card

Create order for verify order details with pickup at store
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    Login, Add product to Cart and Go to Checkout page  ${login_username}  ${login_password}  ${login_display_name}
    Select pick at store    ${pick_at_store.central_bangna}
    Successful pay with Credit Card

Create multiple orders for verify order details with credit card
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    Login, Add multiple product to Cart and Go to Checkout page  ${login_username}  ${login_password}  ${login_display_name}
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Successful pay with Credit Card

Create order for verify order details with credit card incomplete
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    Login, Add product to Cart and Go to Checkout page  ${login_username}  ${login_password}  ${login_display_name}
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Cancel payment credit card
    Wait Until Page Is Completely Loaded

Create multiple orders for verify order details with cod
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    Login, Add multiple product to Cart and Go to Checkout page  ${login_username}  ${login_password}  ${login_display_name}
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    Successful pay with COD method