*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot

*** Keywords ***
Login Keywords for mobile web - Login Success
    [Arguments]    ${username}    ${password}    ${display_name}
    common_cds_mobile_web.Navigate to Login page
    login_page.Input username      ${username}
    login_page.Input password      ${password}
    login_page.Click login button
    common_keywords.Wait Until Page Is Completely Loaded
    Run Keyword And Ignore Error    home_page_mobile_web_keywords.Close Popup
    home_page_mobile_web_keywords.Verify customer Firstname is displayed correctly  ${display_name}
    Get customer info with current shipping address from MDC api by customer member    ${username}    ${password}

Login Keywords for mobile web - Sign Out Success
    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_mobile_web_keywords.Click on Hi First Name lable
    home_page_mobile_web_keywords.Click on Sign Out
    home_page_web_keywords.Verify homepage page should be displayed

Login Keywords for mobile web - Login Success Via Facebook
    [Arguments]    ${email_facebook}    ${password}    
    common_cds_mobile_web.Navigate to Login page
    registration_page.Header Web - Click Facebook login Button
    common_cds_web.Switch to new window
    registration_page.Input Email With Facebook    ${email_facebook}
    registration_page.Input Password With Facebook    ${password}
    registration_page.Click Submit Facebook Login Button
    Run Keyword And Ignore Error    registration_page.Click Confirm Facebook New Account Button
    common_cds_web.Switch to main window