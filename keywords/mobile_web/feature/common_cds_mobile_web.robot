*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot

*** Variables ***
${devicesName}    ${device_name.default}
${mdc_access_token}    %{MDC_ACCESS_TOKEN}
${key_mdc}    %{MDC_ACCESS_TOKEN}

*** Keywords ***
Setup - Open browser for mobile web
    Run Keyword If    '${BU.lower()}'=='${bu_type.rbs}' and '${ENV.lower()}'=='${env_type.sit}'    CommonWebKeywords.Open Chrome Browser to page on devices    ${rbs_url}   ${devicesName}    ${seleniumSpeed}    headless=${FALSE}    extension_full_path=${CURDIR}/../../../resources/cds_extension/${ENV.lower()}    sleep_loading_extension=2
    ...    ELSE    CommonWebKeywords.Open Chrome Browser to page on devices    ${central_url}   ${devicesName}    ${seleniumSpeed}    headless=${FALSE}    extension_full_path=${CURDIR}/../../../resources/cds_extension/${ENV.lower()}    sleep_loading_extension=2
    SeleniumLibrary.Set Window Size    ${width_mobile_web}    ${height_mobile_web}
    Run Keyword And Ignore Error    home_page_mobile_web_keywords.Close Popup
    Run Keyword And Ignore Error    common_keywords.Add Cookie For Disable Capcha
    common_keywords.Wait Until Page Is Completely Loaded
    Run Keyword If    '${BU.lower()}'=='${bu_type.rbs}'    common_keywords.SignIn Web Authentication

Login With Credentials
    [Arguments]    ${username}    ${password}
    Get customer info with current shipping address from MDC api by customer member    ${username}    ${password}
    common_cds_mobile_web.Setup - Open browser for mobile web
    common_cds_mobile_web.Navigate to Login page
    login_page.Input username  ${cds_checkout_process_username}
    login_page.Input password  ${cds_checkout_process_password}
    login_page.Click login button
    common_keywords.Wait Until Page Is Completely Loaded

Go to beauty category page
    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_mobile_web_keywords.Click on beauty category
    home_page_mobile_web_keywords.Click view all sub category

Go to virtual category page
    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_mobile_web_keywords.Click on sale category
    home_page_mobile_web_keywords.Click view all sub category

Go to beauty category - make up page
    product_page.Navigate to page url on plp    ${catagory_url.beauty_makeup}

Navigate to Home page
    home_page_mobile_web_keywords.Click on Central icon

Navigate to Login page
    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_mobile_web_keywords.Click on Login/Register lable

Navigate to Overview tab
    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_mobile_web_keywords.Click on Hi First Name lable
    home_page_mobile_web_keywords.Click on My Account

Navigate to My Order page
    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_mobile_web_keywords.Click on Hi First Name lable
    home_page_mobile_web_keywords.Click on My Order

Navigate to Order Detail page
    [Arguments]    ${order_id}
    Navigate to Home page
    Navigate to My Order page
    Order_details_page.Select order at my order page    ${order_id}

Navigate to Profile tab
    Navigate to Overview tab
    my_account_page.Click on Edit Profile button

Navigate to Address Book tab
    Navigate to Overview tab
    my_account_page.Click on Edit Address button

Navigate To Register Page
    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_mobile_web_keywords.Click on Login/Register lable
    login_page.Click Register Link

Navigate to Wishlist page
    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_mobile_web_keywords.Click on Hi First Name lable
    home_page_mobile_web_keywords.Click on Wishlist

Go to Wishlist page by click on Wishlist icon
    home_page_mobile_web_keywords.Click on Wishlist icon

Go to Wishlist page by click on Wishlist icon when user not login
    home_page_mobile_web_keywords.Click on Wishlist icon when user not login 

Navigate to Track Your Orders
    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_mobile_web_keywords.Click on Track Your Orders

Click Back button to go back to main menu
    home_page_mobile_web_keywords.Click on Back button

Switch language
    [Arguments]    ${language}
    home_page_mobile_web_keywords.Click on hamburger menu
    ${current_language}=    home_page_mobile_web_keywords.Get current language
    Run Keyword And Return If    '${current_language}' == '${language}'    home_page_mobile_web_keywords.Click x button
    home_page_mobile_web_keywords.Select language by label    ${language}
    common_keywords.Wait Until Page Is Completely Loaded
    Run Keyword And Ignore Error    home_page_mobile_web_keywords.Close Popup
    ${current_language}=    home_page_mobile_web_keywords.Get current language
    Should Be Equal As Strings    ${current_language}    ${language}
    
Search product, add product to cart, go to checkout page
    [Arguments]    ${product_sku}    ${quantity}=${1}    ${clear_cart}=${False}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    Run Keyword If    ${clear_cart}==${True}    header_web_keywords.Remove all product in shopping cart
    Wait Until Page Loader Is Not Visible
    # common_keywords.Go To PDP Directly By Product Sku   ${product_sku}
    Retry search product by product sku    ${product_sku}
    Retry add product to cart, product should be displayed in mini cart    ${product_sku}    ${quantity}
    Wait Until Page Loader Is Not Visible
    Run Keyword If    ${quantity}>${1}    my_cart_page_mobile_web.Change quantity of product from the drop-down list    ${product_sku}    ${quantity}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Search product and add product to cart
    [Arguments]    ${product_sku}    ${quantity}=${1}    ${clear_cart}=${False}
    Run Keyword If    ${clear_cart}==${True}    header_web_keywords.Remove all product in shopping cart
    Wait Until Page Loader Is Not Visible
    common_keywords.Go To PDP Directly By Product Sku   ${product_sku}
    product_page.Click on add to cart button  ${product_sku}
    Header Web - Click Mini Cart Button
    Wait Until Page Loader Is Not Visible
    Run Keyword If    ${quantity}>${1}    my_cart_page_mobile_web.Change quantity of product from the drop-down list    ${product_sku}    ${quantity}

Add product to cart, product should be displayed in cart
    [Arguments]    ${product_sku}    ${quantity}=${1}
    product_page.Click on add to cart button    ${product_sku}
    my_cart_page.Wait Until Page Loader Is Not Visible
    Header Web - Click Mini Cart Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    Run Keyword If    ${quantity}>${1}    my_cart_page_mobile_web.Change quantity of product from the drop-down list    ${product_sku}    ${quantity}
    header_web_keywords.Product should be displayed in mini cart    ${${product_sku}}[sku]    ${${product_sku}}[name]
