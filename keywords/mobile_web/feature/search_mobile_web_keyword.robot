*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot

*** Keywords ***
Search product for go to PLP page for mobile web
    [Arguments]    ${text}
    home_page_mobile_web_keywords.Click on search box for mobile web
    home_page_mobile_web_keywords.Input text into search box for mobile web      ${text}
    home_page_Mobile_web_keywords.Press Enter key to search a product

Verify Total Found Item Should Equal Expected Displayed Item
    [Arguments]    ${max_default_displayed_item}=10
    ${total_found_text}=    plp_page.Get Text Total Found Item
    ${total_item_count}=   plp_page.Get Total Product Item In PLP
    Run Keyword And Return If    ${total_found_text} > ${max_default_displayed_item}    Should Be Equal As Numbers    ${total_item_count}    ${max_default_displayed_item}
    Should Be Equal As Numbers    ${total_found_text}    ${total_item_count}