*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot

*** Keywords ***
Filter Product by delivery methods successfully
    [Arguments]    ${delivery_method}
    PLP_page_mobile_web.Click filter button
    common_keywords_mobile_web.Scroll to top screen
    PLP_page_mobile_web.Click delivery methods button 
    ${sku_list}    product_page.Click checkbox options to filter and get list of delivery method    ${delivery_method}
    PLP_page_mobile_web.Click X button 
    product_page.Verify the product are filtered correctly    ${sku_list}    ${delivery_method}

Verify the total of products should be displayed correctly
    [Arguments]    ${number}
    ${count}=    PLP_page_mobile_web.Count all products in search result page
    Should be Equal As Integers    ${count}  ${number}

Click filter price range
    PLP_page_mobile_web.Click filter button
    plp_page.Verify 'filter price range' should be displayed
    plp_page.Click on price range arrow-down button

Click filter brand name
    PLP_page_mobile_web.Click filter button
    plp_page.Verify 'filter brand name' should be displayed
    plp_page.Click on brand name arrow-down button

Select filter color option
    [Arguments]    ${color}
    PLP_page_mobile_web.Click filter button
    plp_page.Click on color arrow-down button
    plp_page.Click on filter color 'clear' button
    plp_page.Click on a color    ${color}
    PLP_page_mobile_web.Click X button 

Click filter color 'clear' button
    PLP_page_mobile_web.Click filter button
    plp_page.Click on color arrow-down button
    plp_page.Click on filter color 'clear' button
