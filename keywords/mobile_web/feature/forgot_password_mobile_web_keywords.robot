*** Setting ***
Resource    ${CURDIR}/../mobile_web_imports.robot

*** Keywords ***
Forgot Password With Email
    [Arguments]    ${email}
    header_web_keywords.Header Web - Click Forgot Password on Header
    forgot_password_page.Input E-mail    ${email}
    forgot_password_page.Click Send E-mail Button

