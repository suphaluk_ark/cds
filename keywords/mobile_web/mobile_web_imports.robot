*** Setting ***
Library    ${CURDIR}/../common-keywords/HTML.py
Library     ${CURDIR}/../common-keywords/AwsKeywords.py
Resource    ${CURDIR}/../../resources/imports.robot
Variables    ${CURDIR}/../../resources/configs/mobile_web_config.yaml

Resource    ${CURDIR}/common/common_keywords_mobile_web.robot

Resource    ${CURDIR}/../web/common/common_keywords.robot
Resource    ${CURDIR}/../web/calculation.robot

Resource    ${CURDIR}/../common-keywords/CommonKeywords.robot
Resource    ${CURDIR}/../common-keywords/CommonWebKeywords.robot
Resource    ${CURDIR}/../common-keywords/commonResponsiveKeywords.robot

Resource    ${CURDIR}/../common-keywords/api/kibana/sms_details.robot
Resource    ${CURDIR}/../common-keywords/fms/fms_db.robot

Resource    ${CURDIR}/../web/page/checkout_page.robot
Resource    ${CURDIR}/../web/page/forgot_password_page.robot
Resource    ${CURDIR}/../web/page/header_web_keywords.robot
Resource    ${CURDIR}/../web/page/home_page_web_keywords.robot
Resource    ${CURDIR}/../web/page/login_page.robot
Resource    ${CURDIR}/../web/page/guest_login_page.robot
Resource    ${CURDIR}/../web/page/my_cart_page.robot
Resource    ${CURDIR}/../web/page/registration_page.robot
Resource    ${CURDIR}/../web/page/resend_email_page.robot
Resource    ${CURDIR}/../web/page/product_page.robot
Resource    ${CURDIR}/../web/page/2c2p.robot
Resource    ${CURDIR}/../web/page/my_account_page.robot
Resource    ${CURDIR}/../web/page/bank_transfer.robot
Resource    ${CURDIR}/../web/page/wishlist_page.robot
Resource    ${CURDIR}/../web/page/change_shipping_address_popup.robot
Resource    ${CURDIR}/../web/page/delivery_details_page.robot
Resource    ${CURDIR}/../web/page/order_history.robot
Resource    ${CURDIR}/../web/page/product_category_page.robot
Resource    ${CURDIR}/../web/page/order_tracking_page.robot
Resource    ${CURDIR}/../web/page/thankyou_page.robot
Resource    ${CURDIR}/../web/page/payment_page.robot
Resource    ${CURDIR}/../web/page/standard_boutique_page.robot
Resource    ${CURDIR}/../web/page/plp_page.robot
Resource    ${CURDIR}/../web/page/order_details_page.robot
Resource    ${CURDIR}/../web/page/product_PDP_page.robot

Resource    ${CURDIR}/../web/feature/common_cds_web.robot
Resource    ${CURDIR}/../web/feature/checkout_keywords.robot
Resource    ${CURDIR}/../web/feature/forgot_password_keywords.robot
Resource    ${CURDIR}/../web/feature/login_keywords.robot
Resource    ${CURDIR}/../web/feature/registration_keywords.robot
Resource    ${CURDIR}/../web/feature/change_shipping_address_keywords.robot
Resource    ${CURDIR}/../web/feature/delivery_details_keywords.robot
Resource    ${CURDIR}/../web/feature/thankyou_keywords.robot
Resource    ${CURDIR}/../web/feature/payment_keywords.robot
Resource    ${CURDIR}/../web/feature/order_details.robot
Resource    ${CURDIR}/../web/feature/E2E_flow_keywords.robot
Resource    ${CURDIR}/../web/feature/product_PDP_keywords.robot
Resource    ${CURDIR}/../web/feature/search_keyword.robot

Resource    ${CURDIR}/feature/search_mobile_web_keyword.robot
Resource    ${CURDIR}/feature/common_cds_mobile_web.robot
Resource    ${CURDIR}/feature/login_keywords.robot
Resource    ${CURDIR}/feature/product_PLP_mobile_web.robot
Resource    ${CURDIR}/feature/order_details_mobile_web.robot
Resource    ${CURDIR}/feature/checkout_keywords.robot
Resource    ${CURDIR}/feature/forgot_password_mobile_web_keywords.robot

Resource    ${CURDIR}/page/login_page.robot
Resource    ${CURDIR}/page/home_page_mobile_web_keywords.robot
Resource    ${CURDIR}/page/PLP_page_mobile_web.robot
Resource    ${CURDIR}/page/my_account_page.robot
Resource    ${CURDIR}/page/checkout_page_mobile_web.robot
Resource    ${CURDIR}/page/my_cart_page_mobile_web.robot
Resource    ${CURDIR}/page/registration_page_mobile_web.robot
Resource    ${CURDIR}/page/category_landing_page_mobile_web.robot

Resource    ${CURDIR}/../api/kibana_api.robot
Resource    ${CURDIR}/../api/customer_api.robot
Resource    ${CURDIR}/../api/wishlist_api.robot
Resource    ${CURDIR}/../api/cart_api.robot
Resource    ${CURDIR}/../api/homepage_api.robot
Resource    ${CURDIR}/../api/search_api.robot
Resource    ${CURDIR}/../api/product_api.robot
Resource    ${CURDIR}/../api/plp_api.robot

Resource    ${CURDIR}/../magento/etc/set_product_file.robot
Resource    ${CURDIR}/../magento/api/mcom/search_sales_order.robot
Resource    ${CURDIR}/../magento/api/mcom/force_shipment.robot
Resource    ${CURDIR}/../magento/api/mcom/payment_validated.robot

Resource    ${CURDIR}/../magento/api/mdc/wishlist.robot
Resource    ${CURDIR}/../magento/api/mdc/get_order_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/address_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/authentication.robot
Resource    ${CURDIR}/../magento/api/mdc/banner_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/cart.robot
Resource    ${CURDIR}/../magento/api/mdc/categories_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/checkout.robot
Resource    ${CURDIR}/../magento/api/mdc/cmsblock_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/consent.robot
Resource    ${CURDIR}/../magento/api/mdc/customer_details.robot
Resource    ${CURDIR}/../magento/api/mdc/customers.robot
Resource    ${CURDIR}/../magento/api/mdc/product.robot
Resource    ${CURDIR}/../magento/api/mdc/promotion_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/search.robot
Resource    ${CURDIR}/../magento/api/mdc/store_location.robot
Resource    ${CURDIR}/../magento/api/mdc/auto_display_overlay.robot

Resource    ${CURDIR}/../falcon/api/homepage/homepage_falcon.robot
Resource    ${CURDIR}/../falcon/api/plp/search_falcon.robot
Resource    ${CURDIR}/../falcon/api/pdp/pdp_falcon.robot

Variables    ${CURDIR}/../../resources/testdata/common${/}translation_${language.lower()}.yaml

