*** Keywords ***
Get Recommend Product Name By Api
    ${response}=    homepage_falcon.Api Get Homepage Recommend Product
    ${name}=    Get Value From Json    ${response}    $..name
    [Return]    ${name}[0]

Get data-product-sku, data-product-id data by sku
    [Documentation]     Return dictionary of product.
    ...                 - *Configuralbe product*
    ...                 - data-product-sku : _configurable's sku_, data-product-id : _simple's sku_
    ...                 - *Simple product*
    ...                 - data-product-sku = _simple's sku_, data-product-id = _simple's sku_
    [Arguments]    ${response}    ${data-product-sku}    ${data-product-id}
    ${list_products}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*]
    ${json_object_by_data-product-sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.sku=='${data-product-sku}')]
    ${type_id_data-product-sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.sku=='${data-product-sku}')].type_id
    ${type_id_data-product-sku}=    Set Variable    ${type_id_data-product-sku}[0]
    ${json_object_by_data-product-id}=    Run Keyword If    '${type_id_data-product-sku}'=='simple'    Set Variable    ${json_object_by_data-product-sku}
    ...    ELSE IF    '${type_id_data-product-sku}' == 'configurable'    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.sku=='${data-product-sku}')].configurable_products[?(@.sku=='${data-product-id}')]
    ...    ELSE        Fail    Incorrect 'type_id'
    ${json_object_by_data-product-sku}=   Set Variable    ${json_object_by_data-product-sku}[0]
    ${data_index}=    Get Index From List    ${list_products}    ${json_object_by_data-product-sku}
    ${data_index}=    Evaluate    ${data_index}+1
    ${json_object_by_data-product-id}=    Set Variable    ${json_object_by_data-product-id}[0]
    ${dict_result}=    BuiltIn.Create Dictionary   
    ...    data_index=${data_index}
    ...    type_id=${json_object_by_data-product-id}
    ...    json_object_data-product-sku=${json_object_by_data-product-sku}
    ...    json_object_data-product-id=${json_object_by_data-product-id}
    [Return]    ${dict_result}