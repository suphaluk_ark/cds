*** Keywords ***
Get configurable option by product id
    [Documentation]    Return dictionary of configurable product
    ...                - *key*      : string, label configurable option, ex. shade, size
    ...                - *value*    : data dictionary, value matched with product id, ex. label, value_index
    [Arguments]    ${json_object}    ${product_id}
    ${product_id}=   Convert To Integer    ${product_id}
    ${list_label}=    JSONLibrary.Get Value From Json    ${json_object}    $..configurable_product_options[?(@.label)].label
    
    ${dict_product_configurable_options}=    Create Dictionary
    FOR   ${label}    IN    @{list_label}
        ${list_configurable_options_values}    JSONLibrary.Get Value From Json    ${json_object}    $..configurable_product_options[?(@.label=='${label}')].values[*]
        ${dict_configurable_options_values_matched_product_id}    Get configurable option values by product id    ${list_configurable_options_values}    ${product_id}

        ${result_label}=    Set Variable    ${dict_configurable_options_values_matched_product_id}[extension_attributes][label]
        ${result_value_index}=    Set Variable    ${dict_configurable_options_values_matched_product_id}[value_index]
        
        ${temp_dict}=    BuiltIn.Create Dictionary    label=${result_label}    value_index=${result_value_index}
        Set To Dictionary    ${dict_product_configurable_options}   ${label}    ${temp_dict}
    END
    [Return]    ${dict_product_configurable_options}
    
Get configurable option values by product id
    [Arguments]    ${list_configurable_options_values}    ${product_id}
    FOR    ${dict_configurable_options_values}    IN    @{list_configurable_options_values}
        ${list_products}=    Set Variable    ${dict_configurable_options_values}[extension_attributes][products]
        ${is_contains_product_id}=    Run Keyword And Return Status    List Should Contain Value    ${list_products}    ${product_id}
        Exit For Loop If    ${is_contains_product_id}==${True}
    END
    Should Be True    ${is_contains_product_id}    Product id : ${product_id}, did not match any value of configurable_product_options
    [Return]    ${dict_configurable_options_values}

Get product price data by product sku
    [Documentation]    Return dictionary of product price
    ...                - *is_product_no_discount*     : ${True}, product has no discount
    ...                - *price*     : number, Product price
    ...                - *special_price*     : number, Product special price
    ...                - *evaluate_save_price*     : number, price - special price
    ...                - *evaluate_percent_discount*     : number, calculation 'evaluate_save_price' with percent
    [Arguments]    ${json_object_by_product_sku}
    ${price}=    Set Variable    ${json_object_by_product_sku}[price]
    ${special_price}=    Set Variable    ${json_object_by_product_sku}[special_price]

    ${is_product_no_discount}=    Set Variable If
    ...    ${special_price}==${None}    ${True}
    ...    ${special_price}==0    ${True}
    ...    ${price}<=${special_price}    ${True}    ${False}    # 21 DEC 2020 Added logic for one project - price service
    
    ${evaluate_save_price}=    Run Keyword If    ${is_product_no_discount}    Set Variable    ${None}
    ...   ELSE    calculation.Evaluate save price    ${price}    ${special_price}
    ${evaluate_percent_discount}=    Run Keyword If    ${is_product_no_discount}    Set Variable    ${None}
    ...   ELSE    calculation.Evaluate percent discount    ${price}    ${special_price}
    
    ${dict_result}=    Create Dictionary
    Set To Dictionary    ${dict_result}    is_product_no_discount    ${is_product_no_discount}
    Set To Dictionary    ${dict_result}    price    ${price}
    Set To Dictionary    ${dict_result}    special_price    ${special_price}
    Set To Dictionary    ${dict_result}    evaluate_save_price    ${evaluate_save_price}
    Set To Dictionary    ${dict_result}    evaluate_percent_discount    ${evaluate_percent_discount}
    [Return]    ${dict_result}

Get product sell price by json object product
    [Documentation]    Return sell price if
    ...                - *product type_id - simple*     : number, product sell price
    ...                - *product type_id - configurable*     : number, product simple lowest sell price
    [Arguments]    ${json_object}
    ${price}=    Run Keyword If    '${json_object}[type_id]'=='simple'    Get product price   ${json_object}
    ...    ELSE IF    '${json_object}[type_id]'=='configurable'    Get configurable product price   ${json_object} 
    ...    ELSE       Fail    Incorrect 'type_id'
    [Return]    ${price}

Get product price
    [Arguments]    ${dict_product}
    ${result_price}=    Set Variable If    ${dict_product}[special_price]==${None}    ${dict_product}[price]
    ...    ${dict_product}[special_price] > ${dict_product}[price]    ${dict_product}[price]    # 21 DEC 2020 Added logic for one project - price service
    ...    ${dict_product}[special_price]
    [Return]    ${result_price}

Get configurable product price
    [Arguments]    ${dict_product}
    ${list_of_configurable_product}=    JSONLibrary.Get Value From Json    ${dict_product}    $..configurable_products[*]
    ${lowest_price}=    Get product price    ${list_of_configurable_product}[0]
    FOR    ${dict_configurable_product}    IN    @{list_of_configurable_product}
        ${price}=    Get product price    ${dict_configurable_product}
        ${lowest_price}=    Set Variable If    ${lowest_price} < ${price}    ${lowest_price}    ${price}
    END 
    [Return]    ${lowest_price}

Get display product overlays by product sku
    [Arguments]    ${json_object_by_product_sku}
    [Documentation]    Return dictionary of product overlays 
    ...                - *If product has* _*overlays*_ *and* _*cart price rule overlays*_   :   return cart_price_rule_overlays
    ...                - *If product* _*overlay*_ *has* _*one more priority*_               :   return first display priority
    ...                - *key:*  is_display         : boolean
    ...                - *key:*  overlay_type       : string, product_overlay / cart_price_rule_overlay
    ...                - *key:*  overlay_image      : string, img
    ...                - *key:*  priority           : integer
    # In case FALCON API return 'only 1 overlay' for product

    # cart_price_rule_overlays
    ${list_cart_price_rule_overlays}=    JSONLibrary.Get Value From Json    ${json_object_by_product_sku}    $..cart_price_rule_overlays
    ${list_cart_price_rule_overlays}=    Set Variable    ${list_cart_price_rule_overlays}[0]
    ${is_list_cart_price_rule_overlays_empty}    Run Keyword And Return Status    Should Be Empty    ${list_cart_price_rule_overlays}
    
    ${is_cart_price_rule_image_null}    Run Keyword If    not ${is_list_cart_price_rule_overlays_empty}    Run Keyword And Return Status    Should Be True    ${list_cart_price_rule_overlays}[0][overlay_image] == ${None}

    ${dict_result}=    Run Keyword And Return If    not ${is_list_cart_price_rule_overlays_empty} and not ${is_cart_price_rule_image_null}   Create dictionary
    ...   is_display=${True}
    ...   overlay_type=cart_price_rule_overlays
    ...   overlay_image=${list_cart_price_rule_overlays}[0][overlay_image]
    ...   priority=${list_cart_price_rule_overlays}[0][display_priority]

    # product overlays
    ${list_overlays_img}=    JSONLibrary.Get Value From Json    ${json_object_by_product_sku}    $..overlays[?(@.overlay_image)]
    ${is_img_none}=    Run Keyword And Return Status    Should Be True    ${list_overlays_img}[0][overlay_image] == ${None}
    
    ${dict_result}=    Run Keyword And Return If    not ${is_img_none}    Create dictionary
    ...   is_display=${True}
    ...   overlay_type=product_overlay
    ...   overlay_image=${list_overlays_img}[0][overlay_image]
    ...   priority=${list_overlays_img}[0][overlay_status]
    
    ${dict_result}=    BuiltIn.Create Dictionary    is_display=${False}    overlay_type=${None}    overlay_image=${None}    priority=${None}
    [Return]    ${dict_result}

Get first display priority product cart price rule overlay
    [Arguments]    ${list_cart_price_rule_overlays}
    ${list_display_priority}=    JSONLibrary.Get Value From Json    ${list_cart_price_rule_overlays}    $..display_priority[*]
    ${first_priority}=    Set Variable    ${list_display_priority}[0]

    FOR    ${cart_price_rule_overlay}   IN    @{list_cart_price_rule_overlays}
        ${is_priority}=    Run Keyword And Return Status    Should Be True    ${cart_price_rule_overlay}[display_priority] == ${first_priority}
        Exit For Loop If    ${is_priority}
    END
    Should Be True    ${is_priority}    msg=Incorrect product cart price rule overlays

    ${dict_result}=    BuiltIn.Create Dictionary    is_display=${True}
    ...   overlay_type=cart_price_rule_overlays
    ...   overlay_image=${cart_price_rule_overlay}[overlay_image]
    ...   priority=${cart_price_rule_overlay}[display_priority]
    [Return]    ${dict_result}

Get first overlay status product overlay
    [Arguments]    ${list_product_overlays}
    ${list_overlay_status}=    JSONLibrary.Get Value From Json    ${list_product_overlays}    $..overlay_status[*]
    ${first_priority}=    Set Variable    ${list_overlay_status}[0]

    FOR    ${product_overlay}   IN    @{list_product_overlays}
        ${is_priority}=    Run Keyword And Return Status    Should Be True    ${product_overlay}[overlay_status] == ${first_priority}
        Exit For Loop If    ${is_priority}
    END
    Should Be True    ${is_priority}    msg=Incorrect product overlays

    ${dict_result}=    BuiltIn.Create Dictionary    is_display=${True}
    ...   overlay_type=product_overlay
    ...   overlay_image=${product_overlay}[overlay_image]
    ...   priority=${product_overlay}[overlay_status]
    [Return]    ${dict_result}

Get configurable's simple product data by sku
    [Documentation]    Return dictionary of configurable's simple product
    [Arguments]    ${response}    ${configurable_simple_product_sku}
    ${json_object_simple_product}=    JSONLibrary.Get Value From Json    ${response}    $..configurable_product_items[?(@.sku=='${configurable_simple_product_sku}')]
    ${json_object_simple_product}=    Set Variable    ${json_object_simple_product}[0]
    ${simple_product_id}=    Set Variable    ${json_object_simple_product}[id]
    ${simple_product_name}=    Set Variable    ${json_object_simple_product}[name]

    ${dict_result}=    BuiltIn.Create Dictionary 
    ...    json_object=${json_object_simple_product}
    ...    sku=${configurable_simple_product_sku}
    ...    id=${simple_product_id}
    ...    name=${simple_product_name}
    [Return]    ${dict_result}

Get simple product data
    [Documentation]    Return dictionary of simple product
    [Arguments]    ${response}
    ${json_object_simple_product}=    JSONLibrary.Get Value From Json    ${response}    $..product
    ${json_object_simple_product}=    Set Variable    ${json_object_simple_product}[0]
    ${simple_product_id}=    Set Variable    ${json_object_simple_product}[id]
    ${simple_product_sku}=    Set Variable    ${json_object_simple_product}[sku]
    ${simple_product_name}=    Set Variable    ${json_object_simple_product}[name]
    
    ${dict_result}=    BuiltIn.Create Dictionary 
    ...    json_object=${json_object_simple_product}
    ...    sku=${simple_product_sku}
    ...    id=${simple_product_id}
    ...    name=${simple_product_name}
    [Return]    ${dict_result}

Get free item sku in cart
    [Arguments]    ${user_token}    
    ${response}=    cart.Verify cart have freebie member    ${user_token}
    ${response}=    Replace String    ${response}    '    "
    ${response}=    Replace String    ${response}    None    "None"
    ${response}=    Catenate    [    ${response}    ]  
    ${response_json}=     Evaluate    json.loads('${response}')    json
    ${free_item_skus}=    Get Value From Json     ${response_json}    $..sku
    ${flag}=    Evaluate     isinstance($free_item_skus,list) and len($free_item_skus)>0
    Should Be True     ${flag}    
    [Return]    ${free_item_skus}   