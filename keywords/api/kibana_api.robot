*** Keywords ***
Compare quantity by line item on kibana CDS create order with MDC
    [Arguments]     ${order_no}     ${product_sku}
    ${response}=     Get Kibana log filter by condition    ${order_no} AND requestRouteId:create-order
    ${response}=    Get Value From Json    ${response}    $.._source..integrationRequests[*]
    ${len}=     Get Length    ${response}
    ${order_details}=   Set Variable     ${response}[2]
    ${response}=     Evaluate    json.loads('${order_details}')    json
    ${items_response}=    Get Value From Json    ${response}      $..sales_order_item.*
    ${kibana_qty}=    Get Value From Json    ${items_response}      $[?(@.sku=='${product_sku}')]..quantity
    ${sum_kibana_qty}=     Evaluate    sum($kibana_qty)
    ${status}   ${mdc_response}=    get_order_keywords.Get order by order id    ${order_no}
    ${mdc_qty}=    Get Value From Json    ${mdc_response}    $.items[*].items[?(@.sku='${product_sku}')].qty_ordered
    Should Be Equal As Strings    ${sum_kibana_qty}    ${mdc_qty}[0]

Compare quantity for whole cart on kibana CDS create order with MDC
    [Arguments]     ${order_no}
    ${response}=     Get Kibana log filter by condition    ${order_no} AND requestRouteId:create-order
    ${response}=    Get Value From Json    ${response}    $.._source..integrationRequests[*]
    ${order_details}=   Set Variable     ${response}[2]
    ${response}=     Evaluate    json.loads('${order_details}')    json
    ${items_response}=    Get Value From Json    ${response}      $..sales_order_item.*
    ${kibana_qty}=    Get Value From Json    ${items_response}      $..quantity
    ${sum_kibana_qty}=     Evaluate    sum($kibana_qty)
    ${status}   ${mdc_response}=    get_order_keywords.Get order by order id    ${order_no}
    ${mdc_qty}=    Get Value From Json    ${mdc_response}    $.items[*].total_qty_ordered
    Should Be Equal As Strings    ${sum_kibana_qty}    ${mdc_qty}[0]

Compare total amount for whole cart on kibana CDS create order with MDC
    [Arguments]     ${order_no}
    ${response}=     Get Kibana log filter by condition    ${order_no} AND requestRouteId:create-order
    ${response}=    Get Value From Json    ${response}    $.._source..integrationRequests[*]
    ${order_details}=   Set Variable     ${response}[1]
    ${response}=     Evaluate    json.loads('${order_details}')    json
    ${kibana_total_amount}=    Get Value From Json    ${response}      $.order.netAmt
    ${status}   ${mdc_response}=    get_order_keywords.Get order by order id    ${order_no}
    ${mdc_total_amount}=    Get Value From Json    ${mdc_response}    $.items[*].grand_total
    ${mdc_total_amount}=    Set Variable     ${mdc_total_amount}[0]
    Should Be Equal As Strings    ${kibana_total_amount}[0]    ${mdc_total_amount}

Compare total discount amount for whole cart on kibana CDS create order with MDC
    [Arguments]     ${order_no}
    ${response}=     Get Kibana log filter by condition    ${order_no} AND requestRouteId:create-order
    ${response}=    Get Value From Json    ${response}    $.._source..integrationRequests[*]
    ${order_details}=   Set Variable     ${response}[1]
    ${response}=     Evaluate    json.loads('${order_details}')    json
    ${kibana_total_discount}=    Get Value From Json    ${response}       $.order.itemDiscountAmt
    ${status}   ${mdc_response}=    get_order_keywords.Get order by order id    ${order_no}
    ${mdc_total_discount}=    Get Value From Json    ${mdc_response}    $.items[*].base_discount_amount
    ${mdc_total_discount}=    Set Variable     ${mdc_total_discount}[0]
    Should Be Equal As Strings    ${kibana_total_discount}[0]    ${mdc_total_discount}

Compare total tax amount for whole cart on kibana CDS create order with MDC
    [Arguments]     ${order_no}
    ${response}=     Get Kibana log filter by condition    ${order_no} AND requestRouteId:create-order
    ${response}=    Get Value From Json    ${response}    $.._source..integrationRequests[*]
    ${order_details}=   Set Variable     ${response}[1]
    ${response}=     Evaluate    json.loads('${order_details}')    json
    ${kibana_total_tax}=    Get Value From Json    ${response}       $.order.vatAmt
    ${status}   ${mdc_response}=    get_order_keywords.Get order by order id    ${order_no}
    ${mdc_total_tax}=    Get Value From Json    ${mdc_response}    $.items[*].base_tax_amount
    ${mdc_total_tax}=    Set Variable     ${mdc_total_tax}[0]
    Should Be Equal As Strings    ${kibana_total_tax}[0]    ${mdc_total_tax}

### picking tool ####
Get CDS create order response body for picking tool source
    [Arguments]     ${order_no}
    ${response}=     Get Kibana log filter by condition    ${order_no} AND requestRouteId:"CDS-create-order"
    ${response}=    Get Value From Json    ${response}    $.._source..responseBody[*]
    ${len}=     Get Length    ${response}
    FOR     ${index}    IN RANGE     ${len}
        ${pk_response}=    Set Variable     ${response}[${index}]
        ${pk_response}=     Convert String To Json    ${pk_response}
        ${pk_status}=     Get Value From Json    ${pk_response}      $..successfully..sourceId
        ${status}    Run Keyword And Return Status     Should Be Equal As Strings    ${pk_status}[0]    CDS_SO_10116
        Run Keyword If    '${status}'=='${TRUE}'     Run Keywords     Set Test Variable     ${cds_response}     ${pk_response}
        ...    AND    Exit For Loop
    END
    Should Be True    ${status}    All status verify 'sourceId' are not equal 'CDS_SO_10116'
    [Return]    ${cds_response}

#####################
Get CDS create order response body for wms source
    [Arguments]     ${order_no}
    ${response}=     Get Kibana log filter by condition    ${order_no} AND requestRouteId:"CDS-create-order-sqs"
    ${response}=    Get Value From Json    ${response}    $.._source..responseBody[*]
    ${len}=     Get Length    ${response}
    FOR     ${index}    IN RANGE     ${len}
        ${pk_response}=    Set Variable     ${response}[${index}]
        ${pk_response}=     Convert String To Json    ${pk_response}
        ${pk_status}=     Get Value From Json    ${pk_response}      $..successfully..sourceId
        ${status}    Run Keyword And Return Status     Should Be Equal As Strings    ${pk_status}[0]    CDS_SO_10138
        Run Keyword If    '${status}'=='${TRUE}'     Run Keywords     Set Test Variable     ${cds_response}     ${pk_response}
        ...    AND    Exit For Loop
    END
    Should Be True    ${status}    All status verify 'sourceId' are not equal 'CDS_SO_10138'
    [Return]    ${cds_response}

Get CDS create order request body for wms source
    [Arguments]    ${order_no}
    ${request}=    Get Kibana log filter by condition    ${order_no} AND requestRouteId:"CDS-create-order"
    ${request}=    Get Value From Json    ${request}    $.._source..requestBody[*]
    ${len}=     Get Length    ${request}
    FOR     ${index}    IN RANGE     ${len}
        ${pk_request}=    Set Variable     ${request}[${index}]
        ${pk_request}=     Convert String To Json    ${pk_request}
        ${pk_status}=     Get Value From Json    ${pk_request}      $..source_id
        ${status}    Run Keyword And Return Status     Should Be Equal As Strings    ${pk_status}[0]    CDS_SO_10138
        Run Keyword If    '${status}'=='${TRUE}'     Run Keywords     Set Test Variable     ${cds_request}     ${pk_request}
        ...    AND    Exit For Loop
    END
    Should Be True    ${status}    All status verify 'source_id' are not equal 'CDS_SO_10138'
    [Return]    ${cds_request}

Get CDS create order request body for picking tool source
    [Arguments]    ${order_no}
    ${request}=    Get Kibana log filter by condition    ${order_no} AND requestRouteId:"CDS-create-order"
    ${request}=    Get Value From Json    ${request}    $.._source..requestBody[*]
    ${len}=     Get Length    ${request}
    FOR     ${index}    IN RANGE     ${len}
        ${pk_request}=    Set Variable     ${request}[${index}]
        ${pk_request}=     Convert String To Json    ${pk_request}
        ${pk_status}=     Get Value From Json    ${pk_request}      $..source_id
        ${status}    Run Keyword And Return Status     Should Be Equal As Strings    ${pk_status}[0]    CDS_SO_10116
        Run Keyword If    '${status}'=='${TRUE}'     Run Keywords     Set Test Variable     ${cds_request}     ${pk_request}
        ...    AND    Exit For Loop
    END
    Should Be True    ${status}    All status verify 'source_id' are not equal 'CDS_SO_10116'
    [Return]    ${cds_request}

Verify that order create on WMS success
    [Arguments]     ${order_no}
    ${wms_status_response}=     Get status response from wms source     ${order_no}
    ${wms_status}=    Get Value From Json    ${wms_status_response}      $.message
    Should Be Equal As Strings    ${wms_status_response}[0]    success

Verify that order create on WMS successfully by destination system
    [Arguments]     ${order_no}
    ${wms_status_response}=     Get status response from wms by destinationSystem     ${order_no}
    ${wms_status_response}=     Convert String To Json    ${wms_status_response}
    ${wms_status}=     Get Value From Json    ${wms_status_response}    $..message
    ${len}=    Get Length    ${wms_status}
    ${wms_status}=    Set Variable If  ${len} > 0    ${wms_status}[0]
    Should Be Equal As Strings    ${wms_status}    success

Verify that order create on pickingtool successfully by destination system
    [Arguments]     ${order_no}
    ${pickingtool_status_response}=     Get status response from pickingtool by destinationSystem     ${order_no}
    ${pickingtool_status_response}=     Convert String To Json    ${pickingtool_status_response}
    ${pickingtool_status}=    Get Value From Json    ${pickingtool_status_response}    $..statusReason
    ${len}=    Get Length    ${pickingtool_status}
    ${pickingtool_status}=    Set Variable If  ${len} > 0    ${pickingtool_status}[0]
    Should Be Equal As Strings    ${pickingtool_status}    Success

#### POST to ESB ####
Get shiprequestID by order number
    [Arguments]     ${order_no}     ${source}=wms
    ${order_response}=     Run Keyword If   '${source}'=='wms'     Get CDS create order response body for wms source     ${order_no}
    ...     ELSE IF     '${source}'=='pickingtool'    Get CDS create order response body for picking tool source     ${order_no}    
    ...     ELSE    Fail    Incorrect source
    ${shiprequestID}=    Get Value From Json    ${order_response}      $..successfully.shipRequestId
    [Return]    ${shiprequestID}

Get line number by sku
    [Arguments]     ${order_no}     ${sku}     ${source}=wms
    ${order_response}=     Run Keyword If   '${source}'=='wms'     Get CDS create order response body for wms source     ${order_no}
    ...     ELSE IF     '${source}'=='pickingtool'    Get CDS create order response body for picking tool source     ${order_no}
    ...     ELSE    Fail    Incorrect source
    ${lineNumber}=    Get Value From Json    ${order_response}      $..successfully.requestShipmentLineItems[?(@.sku='${sku}')].lineNumber
    Set Test variable    @{lineNumber}    @{lineNumber}
    [return]    @{lineNumber}

Generate tracking number
    ${random_value}=    kibana_api.Get Unique Value From Datetime
    ${tracking_no}    Set Variable    TRACK${random_value}${SUFFIX_EMAIL}
    [Return]    ${tracking_no}

Get Unique Value From Datetime
    ${current_datetime}=    Get Time
    ${current_datetime}=    Remove String   ${current_datetime}     :   -   ${SPACE}
    ${milliseconds}=    Evaluate    int(round(time.time() * 10000))     time
    ${milliseconds}=    Convert To String   ${milliseconds}
    ${current_datetime}=    Set Variable    ${current_datetime}${milliseconds[10:14]}
    Log to console  Random value is : ${current_datetime}
    [Return]    ${current_datetime}

Get line id by SKU
    [Arguments]    ${order_no}    ${sku}    ${source}=wms
    ${line_id}=    Run Keyword If    '${source}'=='wms'    Get order line id by SKU from WMS source    ${order_no}    ${sku}
    ...    ELSE IF    '${source}'=='pickingtool'    Get order line id by SKU from picking tool source    ${order_no}    ${sku}
    ...    ELSE    Fail    Incorrect source
    [Return]    ${line_id}

Get order line id by SKU from WMS source
    [Arguments]    ${order_no}    ${sku}
    ${order_request}=    Get CDS create order request body for wms source    ${order_no}
    ${order_line_id}=    Get Value From Json    ${order_request}     $..items[?(@.sku=="${sku}")].order_line_id
    Should Not Be Empty    ${order_line_id}   
    [Return]    ${order_line_id}[0]

Get order line id by SKU from picking tool source
    [Arguments]    ${order_no}    ${sku}
    ${order_request}=    Get CDS create order request body for picking tool source    ${order_no}
    ${order_line_id}=    Get Value From Json    ${order_request}     $..items[?(@.sku=="${sku}")].order_line_id
    Should Not Be Empty    ${order_line_id}   
    [Return]    ${order_line_id}[0]

ESB update shipment status to shipped
    [Arguments]     ${sku}
    ${tracking_no}=     Generate tracking number
    &{headers}=    Create Dictionary
    ...    x-api-key=${ris_api_key}    content-type=application/json    accept-encoding=gzip, deflate    host=${apigw_url}
    ${body}=        Set Variable    [{ "request_shipment_id": "${shiprequestID}", "line_number": ${lineNumber_item}, "sku": "${sku}", "tracking_number": "${tracking_no}", "shipment_provider": "bdc", "tracking_url": "AUTOMATE_ROBOT", "package_id": "", "source": "pickingtools", "line_status": "Ship" }]
    ${response}=     AwsKeywords.Send Request With AWS Authen     PUT    ${apigw_url}
    ...    ${apigw_endpoint_shipped}    ${body}     ${headers}
    ...    execute-api
    ...    ap-southeast-1
    Should Be Equal As Strings     ${response.status_code}    200
    [Return]    ${response}

ESB update shipment status to delivered
    [Arguments]     ${sku}
    ${tracking_no}=     Generate tracking number
    &{headers}=    Create Dictionary
    ...    x-api-key=${ris_api_key}    content-type=application/json    accept-encoding=gzip, deflate    host=${apigw_url}
    ${body}=        Set Variable    [{ "request_shipment_id": "${shiprequestID}", "line_number": ${lineNumber_item}, "line_id": "${line_id}", "line_status": "Delivered", "sku": "${sku}", "shipment_provider": "bdc", "tracking_url": "AUTOMATE_ROBOT", "package_id": "", "source": "pickingtools" }]
    ${response}=     AwsKeywords.Send Request With AWS Authen     PUT    ${apigw_url}
    ...    ${apigw_endpoint_delivered}    ${body}     ${headers}
    ...    execute-api
    ...    ap-southeast-1
    Should Be Equal As Strings     ${response.status_code}    200
    [Return]    ${response}

ESB update shipment status to shipped by sku
    [Documentation]     Please set os environment variable for ${AWS_ACCESS_KEY_ID} and ${AWS_SECRET_ACCESS_KEY}
    [Arguments]     ${order_no}     ${sku}    ${qty}=1     ${source}=wms
    ${shiprequestID}=     Get shiprequestID by order number     ${order_no}    ${source}
    Set Test variable    ${shiprequestID}    ${shiprequestID}[0]
    ${lineNumber}=    Get line number by sku    ${order_no}     ${sku}    ${source}
    ${len}=     Get Length    ${lineNumber}
    Return From Keyword If    ${qty} > ${len}
    ${count_qty}=    Set variable    0
    FOR     ${index}    IN RANGE    ${len}
        Set Test variable     ${lineNumber_item}     ${lineNumber}[${index}]
        ${status}=    Run keyword and return status     ESB update shipment status to shipped     ${sku}
        ${count_qty}=    Run Keyword If    '${status}'=='${True}'      Set variable    ${${count_qty}+1}
        ...     ELSE    Set variable    ${count_qty}
        Set Test variable    ${count_qty}    ${count_qty}
        Run Keyword If    '${count_qty}'!='${qty}' and '${${index}+1}'=='${len}'     Run Keywords     Log to console     can update shipment status to shipped ${count_qty} items
        ...     AND     Fail
        Return From Keyword If     '${count_qty}'=='${qty}'
    END

Retry ESB update shipment status to shipped by sku
    [Arguments]     ${order_no}     ${sku}    ${qty}=1     ${source}=wms
    Wait Until Keyword Succeeds    10 x    5 sec    ESB update shipment status to shipped by sku    ${order_no}     ${sku}    ${qty}     ${source}

ESB update shipment status to delivered by sku
    [Documentation]     Please set os environment variable for ${AWS_ACCESS_KEY_ID} and ${AWS_SECRET_ACCESS_KEY}
    [Arguments]     ${order_no}     ${sku}    ${qty}=1     ${source}=wms
    ${shiprequestID}=     Get shiprequestID by order number     ${order_no}     ${source}
    Set Test variable    ${shiprequestID}    ${shiprequestID}
    ${lineNumber}=    Get line number by sku    ${order_no}     ${sku}     ${source}
    ${line_id}=     Get line id by SKU    ${order_no}     ${sku}     ${source}
    Set Test variable    ${line_id}    ${line_id}
    ${len}=     Get Length    ${lineNumber}
    Return From Keyword If    ${qty} > ${len}
    ${count_qty}=    Set variable    0
    FOR     ${index}    IN RANGE    ${len}
        Set Test variable     ${lineNumber_item}     ${lineNumber}[${index}]
        ${status}=    Run keyword and return status     ESB update shipment status to delivered     ${sku}
        ${count_qty}=    Run Keyword If    '${status}'=='${True}'      Set variable    ${${count_qty}+1}
        ...     ELSE    Set variable    ${count_qty}
        Set Test variable    ${count_qty}    ${count_qty}
        Run Keyword If    '${count_qty}'!='${qty}' and '${${index}+1}'=='${len}'     Run Keywords     Log to console     can update shipment status to delivered ${count_qty} items
        ...     AND     Fail
        Return From Keyword If     '${count_qty}'=='${qty}'
    END

Retry ESB update shipment status to delivered by sku
    [Arguments]     ${order_no}     ${sku}    ${qty}=1     ${source}=wms
    Wait Until Keyword Succeeds    10 x    5 sec    ESB update shipment status to delivered by sku    ${order_no}     ${sku}    ${qty}     ${source}

ESB update shipment status ready to collect by order number
    [Arguments]     ${order_no}
    &{headers}=    Create Dictionary
    ...    x-api-key=${ris_api_key}    content-type=application/json    accept-encoding=gzip, deflate    host=${apigw_url}
    ${body}=        Set Variable    { "status" : "ready_to_collect", "pickupCode" : "123-456-789"}
    ${apigw_endpoint_readytocollect}=    CommonKeywords.Format Text    ${apigw_endpoint_readytocollect}    $order_number=${order_no}
    ${response}=     AwsKeywords.Send Request With AWS Authen     POST    ${apigw_url}
    ...    ${apigw_endpoint_readytocollect}    ${body}     ${headers}
    ...    execute-api
    ...    ap-southeast-1
    Should Be Equal As Strings     ${response.status_code}    200
    [Return]    ${response}

Compare tax amount by line item on kibana log with FMS db
    [Arguments]     ${order_no}     ${sku}     ${source}=wms
    ${order_response}=     Run Keyword If   '${source}'=='wms'     Get CDS create order response body for wms source     ${order_no}
    ...     ELSE IF     '${source}'=='pickingtool'    Get CDS create order response body for picking tool source     ${order_no}
    ${lineNumber}=    Get line number by sku    ${order_no}     ${sku}
    ${len}=     Get Length    ${lineNumber}
    FOR    ${index}    IN RANGE    ${len}
        Set Test variable     ${lineNumber_item}     ${lineNumber}[${index}]
        ${fms_tax_amount}=    Select tax amount by line item from sale order details on FMS db    ${order_no}     ${sku}    ${lineNumber_item}
        ${cds_tax_amount}=    Get Value From Json    ${order_response}      $..items[?(@.sku='${sku}')].extension_attributes.unit_vat_amount_incl_t1c
        Should Be Equal As Numbers    ${cds_tax_amount}[0]    ${fms_tax_amount}
    END

Compare total amount by line item on kibana log with FMS db
    [Arguments]     ${order_no}     ${sku}     ${source}=wms
    ${order_response}=     Run Keyword If   '${source}'=='wms'     Get CDS create order response body for wms source     ${order_no}
    ...     ELSE IF     '${source}'=='pickingtool'    Get CDS create order response body for picking tool source     ${order_no}
    ${lineNumber}=    Get line number by sku    ${order_no}     ${sku}
    ${len}=     Get Length    ${lineNumber}
    FOR    ${index}    IN RANGE    ${len}
        Set Test variable     ${lineNumber_item}     ${lineNumber}[${index}]
        ${fms_total_amount}=    Select total amount by line item from sale order details on FMS db    ${order_no}     ${sku}    ${lineNumber_item}
        ${cds_total_amount}=    Get Value From Json    ${order_response}      $..items[?(@.sku='${sku}')].base_price_incl_tax
        Should Be Equal As Numbers    ${cds_total_amount}[0]    ${fms_total_amount}
    END

Compare discount amount by line item on kibana log with FMS db
    [Arguments]     ${order_no}     ${sku}     ${source}=wms
    ${order_response}=     Run Keyword If   '${source}'=='wms'     Get CDS create order response body for wms source     ${order_no}
    ...     ELSE IF     '${source}'=='pickingtool'    Get CDS create order response body for picking tool source     ${order_no}
    ${lineNumber}=    Get line number by sku    ${order_no}     ${sku}
    ${len}=     Get Length    ${lineNumber}
    FOR    ${index}    IN RANGE    ${len}
        Set Test variable     ${lineNumber_item}     ${lineNumber}[${index}]
        ${fms_discount_amount}=    Select discount amount by line item from sale order details on FMS db    ${order_no}     ${sku}    ${lineNumber_item}
        ${cds_discount_amount}=    Get Value From Json    ${order_response}       $..items[?(@.sku='${sku}')].extension_attributes.unit_discount_without_t1c
        Should Be Equal As Numbers    ${cds_discount_amount}[0]    ${fms_discount_amount}
    END

Compare tax, discount, total amount by line item on kibana log with FMS db
    [Arguments]     ${order_no}     ${sku}     ${source}=wms
    Compare tax amount by line item on kibana log with FMS db     ${order_no}     ${sku}         ${source}
    Compare total amount by line item on kibana log with FMS db     ${order_no}     ${sku}         ${source}
    Compare discount amount by line item on kibana log with FMS db     ${order_no}     ${sku}        ${source}

Compare tax, discount, total amount by line item for multiple sku on kibana log with FMS db
    [Arguments]     ${order_no}     @{sku}
    FOR    ${item_sku}   IN    @{sku}
        Compare tax, discount, total amount by line item on kibana log with FMS db      ${order_no}    ${item_sku}   ${source}=wms
    END

Compare tax amount for whole cart on kibana log with FMS db
    [Arguments]     ${order_no}     ${source}=wms
    ${order_response}=     Run Keyword If   '${source}'=='wms'     Get CDS create order response body for wms source     ${order_no}
    ...     ELSE IF     '${source}'=='pickingtool'    Get CDS create order response body for picking tool source     ${order_no}
    ${cds_tax_amount}=    Get Value From Json    ${order_response}      $..orderDetails
    ${cds_tax_amount}=    Get Value From Json    ${cds_tax_amount}[0]      $..vatAmt
    ${fms_tax_amount_origin}=    Select tax amount for whole cart from sale order details on FMS db     ${order_no}
    ${fms_tax_amount}=    Convert To Two Digit Number    ${fms_tax_amount_origin}
    Should Be Equal As Numbers    ${cds_tax_amount}[0]    ${fms_tax_amount}

Compare total amount for whole on kibana log with FMS db
    [Arguments]     ${order_no}     ${source}=wms
    ${order_response}=     Run Keyword If   '${source}'=='wms'     Get CDS create order response body for wms source     ${order_no}
    ...     ELSE IF     '${source}'=='pickingtool'    Get CDS create order response body for picking tool source     ${order_no}
    ${cds_total_amount}=    Get Value From Json    ${order_response}      $..netAmt
    ${fms_total_amount}=    Select total amount for whole cart from sale order details on FMS db     ${order_no}
    Should Be Equal As Numbers    ${cds_total_amount}[0]    ${fms_total_amount}

Compare discount amount for whole cart on kibana log with FMS db
    [Arguments]     ${order_no}     ${source}=wms
    ${order_response}=     Run Keyword If   '${source}'=='wms'     Get CDS create order response body for wms source     ${order_no}
    ...     ELSE IF     '${source}'=='pickingtool'    Get CDS create order response body for picking tool source     ${order_no}
    ${cds_discount_amount}=    Get Value From Json    ${order_response}      $..itemDiscountAmt
    ${fms_discount_amount}=    Select discount amount for whole cart from sale order details on FMS db     ${order_no}
    ${fms_t1c_discount}=    Select t1c redeem amount for whole cart from sale order details on FMS db     ${order_no}
    ${fms_discount_order}=      Evaluate     ${fms_discount_amount}+${fms_t1c_discount}
    Should Be Equal As Numbers    ${cds_discount_amount}[0]    ${fms_discount_order}

Compare tax, discount, total amount for whole cart on kibana log with FMS db
    [Arguments]     ${order_no}     ${source}=wms
    Compare tax amount for whole cart on kibana log with FMS db     ${order_no}     ${source}
    Compare total amount for whole on kibana log with FMS db     ${order_no}     ${source}
    Compare discount amount for whole cart on kibana log with FMS db     ${order_no}     ${source}

Compare t1c discount amount for whole cart on kibana log with FMS db test
    [Arguments]     ${order_no}     ${source}=wms
    ${order_response}=     Run Keyword If   '${source}'=='wms'     Get CDS create order response body for wms source     ${order_no}
    ...     ELSE IF     '${source}'=='pickingtool'    Get CDS create order response body for picking tool source     ${order_no}
    ${cds_t1c_amount}=    Get Value From Json    ${order_response}      $..extension_attributes.t1c_info.discount_amount
    ${fms_t1c_amount}=    Select t1c redeem amount for whole cart from sale order details on FMS db     ${order_no}
    Should Be Equal As Numbers    ${cds_t1c_amount}[0]    ${fms_t1c_amount}
    
Get status response from wms source
    [Arguments]     ${order_no}
    ${response}=     Get Kibana log filter by condition    ${order_no} AND requestRouteId:"CDS-create-order-sqs"
    ${response}=    Get Value From Json    ${response}    $.._source..responseBody[*]
    log    ${response}
    ${len}=     Get Length    ${response}
    FOR     ${index}    IN RANGE     ${len}
        ${wms_status_response}=    Set Variable     ${response}[${index}]
        ${wms_status_response}=     Convert String To Json    ${wms_status_response}
        ${wms_status}=     Get Value From Json    ${wms_status_response}      $..AIL..message..message
        ${status}    Run Keyword And Return Status     Should Be Equal As Strings    ${wms_status}[0]    success
        Run Keyword If    '${status}'=='${TRUE}'    Set Test Variable    ${cds_response}    ${wms_status}
    END
    [Return]    ${cds_response}

Get status response from wms by destinationSystem
    [Arguments]    ${order_no}
    ${response}=    Get Kibana log filter by condition    ${order_no} AND destinationSystem:"ail"
    ${response}=    Get Value From Json    ${response}    $.._source..response.body
    ${len}=    Get Length    ${response}
    ${response}=    Set Variable If  ${len} > 0    ${response}[0]
    [Return]    ${response}

Get status response from pickingtool by destinationSystem
    [Arguments]    ${order_no}
    ${response}=    Get Kibana log filter by condition    ${order_no} AND destinationSystem:"pickingToolCreateOrder-CDS"
    ${response}=    Get Value From Json    ${response}    $.._source..response.body
    ${len}=    Get Length    ${response}
    ${response}=    Set Variable If  ${len} > 0    ${response}[0]
    [Return]    ${response}