*** Keywords ***
Get random color of filter color by product keyword
    [Arguments]    ${keyword}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${1}
    ${list_of_color}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='color_group_name')]..key
    Should Not Be Empty    ${list_of_color}
    ${color}=    Evaluate    random.choice(${list_of_color})    random
    [Return]    ${color}

Get random color of filter color by category id
    [Arguments]    ${category_id}
    ${response}=    search_falcon.Api Search Product By Category Id    ${category_id}    ${language}    ${BU.lower()}    ${1}
    ${list_of_color}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='color_group_name')]..key
    Should Not Be Empty    ${list_of_color}
    ${color}=    Evaluate    random.choice(${list_of_color})    random
    [Return]    ${color}

Get min, max of filter price range by product brand name
    [Arguments]    ${brand_name}
    ${response}=    search_falcon.Api Search Product By Brand Name    ${test_brand_name}    ${language}    ${BU.lower()}    ${1}
    ${min_price}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='min_price')].value
    ${max_price}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='max_price')].value
    [Return]    ${min_price}[0]    ${max_price}[0]

Get min, max of filter price range by product keyword
    [Arguments]    ${keyword}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${1}
    ${min_price}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='min_price')].value
    ${max_price}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='max_price')].value
    [Return]    ${min_price}[0]    ${max_price}[0]

Get list brand name of filter brand name by product keyword
    [Arguments]    ${keyword}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${1}
    ${list_of_brand_name}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='brand_name')]..key
    Should Not Be Empty    ${list_of_brand_name}
    [Return]    ${list_of_brand_name}

Get list percent discount (high-low) by product brand name
    [Arguments]    ${brand_name}    ${page_size}=${50}
    ${response}=    search_falcon.Api Search Product By Brand Name And Filter Sort 'Discount High-Low'    ${test_brand_name}    ${language}    ${BU.lower()}    ${page_size}
    ${list_of_product}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*]
    Should Not Be Empty    ${list_of_product}

    ${list_of_evaluate_discount}    Create List
    FOR    ${dict_product}    IN    @{list_of_product}
        ${percent_discount}=    Run Keyword If    '${dict_product}[type_id]'=='simple'    Evaluate percent discount    ${dict_product}[price]    ${dict_product}[special_price]
        ...    ELSE IF    '${dict_product}[type_id]'=='configurable'    Get percent discount configurable product    ${dict_product} 
        ...    ELSE       Fail    Incorrect 'type_id'
        Append To List    ${list_of_evaluate_discount}    ${percent_discount}
    END
    [Return]    ${list_of_evaluate_discount}

Get list price (low-high) by product keyword
    [Arguments]    ${keyword}    ${page_size}=${50}
    ${response}=    search_falcon.Api Search Product By Keyword And Filter Sort 'Price Low-High'    ${test_keyword}    ${language}    ${BU.lower()}    ${page_size}
    ${list_of_product}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*]
    Should Not Be Empty    ${list_of_product}

    ${list_of_price}    Create List
    FOR    ${dict_product}    IN    @{list_of_product}
        ${price}=    product_api.Get product sell price by json object product    ${dict_product}
        Append To List    ${list_of_price}    ${price}
    END
    [Return]    ${list_of_price}

Get list color of filter color by configurable product
    [Documentation]    Return list color by simple products of configurable product
    [Arguments]    ${dict_configurable_product}
    ${list_color}=    JSONLibrary.Get Value From Json    ${dict_configurable_product}    $..configurable_products..color_group_name
    ${result}=    Create List
    FOR    ${item}    IN    @{list_color}
        Continue For Loop If    ${item}==${None}
        ${result}=    Combine Lists    ${result}    ${item}
    END
    ${result}=    Remove Duplicates    ${result}
    Log List    ${result}
    [Return]    ${result}

Retry get random price range
    [Documentation]    Return 'min price' and 'max price' between the price range with custom 'different price'.
    [Arguments]    ${min}    ${max}    ${diff}
    ${min}=    Evaluate    math.floor(${min})    math
    ${max}=    Evaluate    math.floor(${max})    math
    ${result_min}    ${result_max}      Wait Until Keyword Succeeds    0.5s    0.001s      Get random price range    ${min}    ${max}    ${diff}
    [Return]    ${result_min}    ${result_max}

Get random price range
    [Arguments]    ${min}    ${max}    ${diff}
    ${list_random}    Evaluate    random.sample(range(${min}, ${max}), 2)    random
    Collections.Sort List    ${list_random}

    ${evaluate_diff}=    Evaluate    ${list_random}[1]-${list_random}[0]
    Return From Keyword If   ${evaluate_diff} >= ${diff}    ${list_random}[0]    ${list_random}[1]
    Fail   Could not find the range price from  ${min}, to ${max}, diff ${diff}

Get percent discount configurable product 
    [Arguments]    ${dict_product}
    ${list_of_configurable_product}=    JSONLibrary.Get Value From Json    ${dict_product}    $..configurable_products
    ${list_of_configurable_product}=    Set Variable    ${list_of_configurable_product}[0]
    ${percent_discount}=    Set Variable    0
    ${percent_discount}=    Convert To Integer    ${percent_discount}
    FOR    ${dict_configurable_product}    IN    @{list_of_configurable_product}
        ${evaluate_percent_discount}=    calculation.Evaluate percent discount    ${dict_configurable_product}[price]    ${dict_configurable_product}[special_price]
        ${percent_discount}=    Set Variable If    ${evaluate_percent_discount} > ${percent_discount}    ${evaluate_percent_discount}    ${percent_discount}
    END
    [Return]    ${percent_discount}

Get random product by category id
    [Documentation]     *Arguments*
    ...                 - category_id
    ...                 - product_type : simple, configurable
    ...                 *Return:* 
    ...                 - data dictionary of first configurable product data by category id
    ...                 - _*fail*_ if has no configurable product data by category id.
    [Arguments]    ${category_id}    ${product_type}
    ${response}=    search_falcon.Api Search Product By Category Id    ${category_id}    ${language}    ${BU}     ${50}
    ${list_product}=    Run Keyword If    '${product_type}' == 'configurable'    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.type_id=='configurable')]
    ...    ELSE IF    '${product_type}' == 'simple'    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.type_id=='simple')]
    ...    ELSE    Fail    Incorrect product_type

    BuiltIn.Should Not Be Empty    ${list_product}    msg=Could not find any ${product_type} product data by category id : ${category_id}

    ${list_product}=    Evaluate    random.choice(${list_product})    random
    ${result}=    BuiltIn.Create Dictionary
    Collections.Set To Dictionary    ${result}    name    ${list_product}[name]
    Collections.Set To Dictionary    ${result}    sku    ${list_product}[sku]
    Collections.Set To Dictionary    ${result}    url_key    ${list_product}[url_key]
    [Return]    ${result}
    
Get random brand name of filter brand name by keyword
    [Arguments]    ${keyword}    ${language}=${common_language.en}
    ${response}=    search_falcon.Api Search Product By Keyword    ${test_keyword}    ${language}    ${BU}    ${1}
    ${list_of_brand_name}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='brand_name')]..key
    Should Not Be Empty    ${list_of_brand_name}
    ${brand_name}=    Evaluate    random.choice(${list_of_brand_name})    random
    [Return]    ${brand_name}

Get display product overlays by product sku
    [Arguments]    ${json_object_by_product_sku}
    [Documentation]    Return dictionary of product overlays 
    ...                - *If product has* _*overlays*_ *and* _*cart price rule overlays*_   :   return cart_price_rule_overlays
    ...                - *key:*  is_display         : boolean
    ...                - *key:*  overlay_type       : string, product_overlay / cart_price_rule_overlay
    ...                - *key:*  overlay_image      : string, img
    ...                - *key:*  priority           : integer
    # cart_price_rule_overlays
    ${list_cart_price_rule_overlays_img}=    JSONLibrary.Get Value From Json    ${json_object_by_product_sku}    $..cart_price_rule_overlays[?(@.overlay_image)]
    ${len_list_cart_price_rule_overlays_img}=    Get Length    ${list_cart_price_rule_overlays_img}
    ${dict_result}=    Run Keyword And Return If    ${len_list_cart_price_rule_overlays_img} > ${0}    Get first display priority product cart price rule overlay    ${list_cart_price_rule_overlays_img}

    # product overlay 
    ${dict_overlay}=    JSONLibrary.Get Value From Json    ${json_object_by_product_sku}    $..overlay
    ${dict_overlay}=    Set Variable   ${dict_overlay}[0]
    ${is_dict_overlay_none}=    Run Keyword And Return Status    Should Be Equal As Strings    ${dict_overlay}    ${None}

    ${dict_result}=    Run Keyword If    not ${is_dict_overlay_none}    BuiltIn.Create Dictionary    is_display=${True}    overlay_type=product_overlay    overlay_image=${dict_overlay}[image]    priority=${dict_overlay}[status]
    ...    ELSE    BuiltIn.Create Dictionary    is_display=${False}    overlay_type=${None}    overlay_image=${None}    priority=${None}
    [Return]    ${dict_result}

Get data-product-sku, data-product-id data by sku
    [Documentation]     Return dictionary of product.
    ...                 - *Configuralbe product*
    ...                 - data-product-sku : _configurable's sku_, data-product-id : _simple's sku_
    ...                 - *Simple product*
    ...                 - data-product-sku = _simple's sku_, data-product-id = _simple's sku_
    [Arguments]    ${response}    ${data-product-sku}    ${data-product-id}
    ${list_products}=    JSONLibrary.Get Value From Json    ${response}    $..products[*]
    ${json_object_by_data-product-sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.sku=='${data-product-sku}')]
    ${type_id_data-product-sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.sku=='${data-product-sku}')].type_id
    ${type_id_data-product-sku}=    Set Variable    ${type_id_data-product-sku}[0]

    ${json_object_by_data-product-id}=    Run Keyword If    '${type_id_data-product-sku}'=='simple'    Set Variable    ${json_object_by_data-product-sku}
    ...    ELSE IF    '${type_id_data-product-sku}' == 'configurable'    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.sku=='${data-product-sku}')].configurable_products[?(@.sku=='${data-product-id}')]
    ...    ELSE        Fail    Incorrect 'type_id'

    ${json_object_by_data-product-sku}=   Set Variable    ${json_object_by_data-product-sku}[0]
    ${data_index}=    Get Index From List    ${list_products}    ${json_object_by_data-product-sku}
    ${json_object_by_data-product-id}=    Set Variable    ${json_object_by_data-product-id}[0]

    ${dict_result}=    BuiltIn.Create Dictionary   
    ...    data_index=${data_index}
    ...    type_id=${json_object_by_data-product-id}
    ...    json_object_data-product-sku=${json_object_by_data-product-sku}
    ...    json_object_data-product-id=${json_object_by_data-product-id}
    [Return]    ${dict_result}