*** Keywords ***
Get MDC order status from order detail response
    [Arguments]    ${increment_id}
    ${response}=    order_details.Get order details by increment_id    ${increment_id}
    ${status}=    REST.Output    $.items[?(@.increment_id == '${increment_id}')].extension_attributes.order_status
    [Return]    ${status} 

Get status from order detail response
    [Arguments]    ${increment_id}
    ${response}=    order_details.Get order details by increment_id    ${increment_id}
    ${status}=    REST.Output    $.items[?(@.increment_id == '${increment_id}')].status
    [Return]    ${status}

Get status reason from order detail response
    [Arguments]    ${increment_id}
    ${response}=    order_details.Get order details by increment_id    ${increment_id}
    ${status}=    REST.Output    $.items[?(@.increment_id == '${increment_id}')].extension_attributes.mom_status_reason
    [Return]    ${status}

### Verify status on MDC ####
Verify MDC order status should be 'processing'
    [Arguments]    ${increment_id}
    ${status}=    order_status_api.Get MDC order status from order detail response    ${increment_id}
    BuiltIn.Should Be Equal As Strings    ${status}    processing

Verify MDC order status reason should be 'READYTOSHIP'
    [Arguments]    ${increment_id}
    ${status_reason}=    order_status_api.Get status reason from order detail response    ${increment_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    READYTOSHIP

Verify MDC order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${increment_id}
    ${status_reason}=    order_status_api.Get status reason from order detail response    ${increment_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    PENDING_VALIDATE_PAYMENT_REPLY

Verify MDC order status should be 'awaiting_payment'
    [Arguments]    ${increment_id}
    ${status}=    order_status_api.Get MDC order status from order detail response    ${increment_id}
    BuiltIn.Should Be Equal As Strings    ${status}    awaiting_payment

Verify MDC order status should be 'MCOM_COMPLETE'
    [Arguments]    ${increment_id}
    ${status}=    order_status_api.Get status from order detail response    ${increment_id}
    BuiltIn.Should Be Equal As Strings    ${status}    MCOM_COMPLETE

Verify MDC order status reason should be 'FULLYCANCELLED'
    [Arguments]    ${increment_id}
    ${status_reason}=    order_status_api.Get status reason from order detail response    ${increment_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    FULLYCANCELLED

### Verify status in MCOM ###
Verify MCOM order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${order_number}
    ${status_reason}=    search_sales_order.Get order status reason from MCOM    ${order_number}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    PENDING_VALIDATE_PAYMENT_REPLY

Verify MCOM order status should be 'ONHOLD'
    [Arguments]    ${increment_id}
    ${status}=    search_sales_order.Get order status from MCOM    ${increment_id}
    BuiltIn.Should Be Equal As Strings    ${status}    ONHOLD

Verify MCOM order status should be 'LOGISTICS'
    [Arguments]    ${increment_id}
    ${status}=    search_sales_order.Get order status from MCOM    ${increment_id}
    BuiltIn.Should Be Equal As Strings    ${status}    LOGISTICS

Verify MCOM order status reason should be 'READYTOSHIP'
    [Arguments]    ${order_number}
    ${status_reason}=    search_sales_order.Get order status reason from MCOM    ${order_number}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    READYTOSHIP

Verify MCOM order status should be 'COMPLETE'
    [Arguments]    ${order_number}
    ${status_reason}=    search_sales_order.Get order status from MCOM    ${order_number}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    COMPLETE

Verify MCOM order status reason should be 'FULLYCANCELLED'
    [Arguments]    ${order_number}
    ${status_reason}=    search_sales_order.Get order status reason from MCOM    ${order_number}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    FULLYCANCELLED

### Retry for MDC ###
Retry until MDC order status should be 'Processing'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MDC order status should be 'processing'    ${order_number}

Retry until MDC order status should be 'awaiting_payment'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MDC order status should be 'awaiting_payment'    ${order_number}

Retry until MDC status reason should be 'READYTOSHIP'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MDC order status reason should be 'READYTOSHIP'    ${order_number}

Retry until MDC status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MDC order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${order_number}

Retry until MDC order status should be 'MCOM_COMPLETE'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MDC order status should be 'MCOM_COMPLETE'    ${order_number}

Retry until MDC order status reason should be 'FULLYCANCELLED'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MCOM order status reason should be 'FULLYCANCELLED'    ${order_number}

### Retry for MCOM ###
Retry until MCOM order status should be 'ONHOLD'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MCOM order status should be 'ONHOLD'    ${order_number}

Retry until MCOM order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MCOM order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${order_number}

Retry until MCOM order status should be 'LOGISTICS'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MCOM order status should be 'LOGISTICS'    ${order_number}

Retry until MCOM order status reason should be 'READYTOSHIP'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MCOM order status reason should be 'READYTOSHIP'    ${order_number}

Retry until MCOM order status should be 'COMPLETE'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MCOM order status should be 'COMPLETE'    ${order_number}

Retry until MCOM order status reason should be 'FULLYCANCELLED'
    [Arguments]    ${order_number}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MCOM order status reason should be 'FULLYCANCELLED'    ${order_number}