*** Keywords ***
Add wishlist item by SKU number
    [Arguments]    ${username}    ${password}    ${sku_number}    ${store_code}=cds_${language}    ${store}=${EMPTY}
    ${customer_token}=    Get login token    ${username}    ${password}
    ${customer_id}=    customer_api.Get customer id from api response    ${customer_token}
    ${wishlist_id}=    Get wishlist id from api response    ${customer_id}    ${store}
    ${product_id}=    product.Get product via product V2 API by SKU    ${sku_number}    ${store}
    ${product_id}=    REST.Output    $.id
    ${store_id}=    Get customer store by store code    ${store_code}    ${mdc_access_token}    ${store}
    ${store_id}=    REST.Output    $..id
    ${response}=    Add wishlist item to wishlist    ${wishlist_id}    ${product_id}    ${store_id}    ${mdc_access_token}    ${store}
    [Return]    ${response}