*** Keywords ***
Get Product Sku By Brand Name Via Api
    [Arguments]    ${brand_name}    ${page_size}=${20}
    ${response}=    search_falcon.Api Search Product By Brand Name    ${brand_name}    ${language}    ${BU.lower()}    ${page_size}
    ${skus}=    Get Value From Json    ${response}    $..sku
    [Return]    ${skus}

Get Product Sku Without Special Price By Brand Name Via Api
    [Arguments]    ${brand_name}    ${page_size}=${20}
    ${response}=    search_falcon.Api Search Product By Brand Name    ${brand_name}    ${language}    ${BU.lower()}    ${page_size}
    ${products}=    Get Value From Json    ${response}    $..search.products[*]
    ${skus}=    Create List
    FOR    ${product}    IN     @{products}
        Run Keyword If    ${product}[special_price] == ${None}    Append To List    ${skus}    ${product}[sku]
    END
    [Return]    ${skus}

Get Configurable Product Sku By Brand Name Via Api
    [Arguments]    ${brand_name}    ${page_size}=${20}
    ${response}=    search_falcon.Api Search Product By Brand Name    ${brand_name}    ${language}    ${BU.lower()}    ${page_size}
    ${skus}=    Get Value From Json    ${response}    $..configurable_products..sku
    [Return]    ${skus}

Get Product Sku By Keyword Via Api
    [Arguments]    ${keyword}    ${page_size}=${20}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${page_size}
    ${skus}=    Get Value From Json    ${response}    $..sku
    [Return]    ${skus}

Get Category Url Path By Category Id Via Api
    [Arguments]    ${category_id}    ${page_size}=${1}
    ${response}=    search_falcon.Api Search Product By Category Id    ${category_id}    ${language}    ${BU.lower()}    ${page_size}
    ${url_path}=    Get Value From Json    ${response}    $..category_aggregations[?(@.id=='${category_id}')].url_path
    [Return]    ${url_path}[0]

Get Dictionary Of Discount Product From Catalog Service
    [Arguments]    ${category_id}    ${page_size}=5
    ${response}=    Search Product From Catalog Service Api    cds_${language}    ${page_size}
    ...    searchCriteria[filter_groups][0][filters][0][field]=category_id
    ...    searchCriteria[filter_groups][0][filters][0][value]=${category_id}
    ...    searchCriteria[filter_groups][0][filters][0][condition_type]=eq
    ...    searchCriteria[filter_groups][1][filters][0][field]=visibility
    ...    searchCriteria[filter_groups][1][filters][0][value]=4
    REST.Integer    response status    200
    ${products_list}=    Get Value From Json    ${response}    $..products[?(@.special_price > 0)]
    ${product}=    Collections.Get From List    ${products_list}    0
    ${name}=    Get Value From Json    ${product}    $.name
    ${sku}=    Get Value From Json    ${product}    $.sku
    ${saving_price}=    Get Value From Json    ${product}    $.price_saving
    ${saving_price}=    Convert Price To Us Format    ${saving_price}[0]
    &{product_dict}=    Create Dictionary
    ...    product_name=${name}[0]
    ...    product_sku=${sku}[0]
    ...    product_saving_price=${saving_price}
    [Return]    ${product_dict}

Get List Of Product Name By Keyword Via API
    [Arguments]    ${keyword}    ${page_size}=${20}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${page_size}
    ${list_product_name}=    Get Value From Json    ${response}    $..name
    [Return]    ${list_product_name}