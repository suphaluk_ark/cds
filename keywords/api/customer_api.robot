*** Keywords ***
Delete All Customer Addresses By Api
    [Arguments]     ${username}     ${password}
    ${user_token}=    authentication.Get login token    ${username}    ${password}
    ${response}=    customer_details.Get customer details    ${user_token}
    ${address_list}=    Get Value From Json    ${response}    $..addresses..id
    FOR    ${address}    IN   @{address_list}
        address_keywords.Delete Address by API    ${address}
    END

Get customer id from api response
    [Arguments]    ${customer_token}
    ${response}=    Get customer details    ${customer_token}
    ${customer_id}=    REST.Output    $.id
    [Return]    ${customer_id}

Delete registeration account from mdc
    [Arguments]    ${register_email}    ${regresstion_password_1}
    ${customer_ID}=    Get customer ID    ${register_email}    ${regresstion_password_1}
    customer_details.Delete customer by customer ID    ${customer_ID}