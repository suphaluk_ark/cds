*** Keywords ***
Remove All Products From Cart Using API
    [Arguments]    ${email}    ${password}
    ${user_token}=    Get login token    ${email}  ${password}
    Run Keyword And Ignore Error    Delete cart    ${user_token}

Add Product To Cart Using API
    [Arguments]    ${email}    ${password}    ${sku}    ${quantity}=1    ${store}=${EMPTY}
    ${user_token}=    Get login token    ${email}    ${password}
    ${is_available}=    Run Keyword And Return Status    Get cart id member    ${user_token}    ${store}
    Run Keyword If    not ${is_available}    Generate customer cart    ${user_token}    store=${store}
    ${response}=    Add product to cart    ${user_token}    ${sku}    ${quantity}   ${store}