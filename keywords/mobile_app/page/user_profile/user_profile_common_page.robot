*** Keywords ***
Click login button
    CommonMobileKeywords.Click Element    ${dictUserProfilePage}[btn_login]

Click register button
    CommonMobileKeywords.Click Element    ${dictUserProfilePage}[btn_register]

Click x button
    Wait Until Keyword Succeeds    3 x    1 sec    Run Keywords    CommonMobileKeywords.Click Element    ${dictUserProfilePage}[btn_x]    AND    AppiumLibrary.Wait Until Page Contains Element    ${dictHomePage}[img_user_profile]

Click on customer name
    CommonMobileKeywords.Click Element    ${dictUserProfilePage}[lbl_first_name_last_name]    timeout=${GLOBALTIMEOUT}

Scroll to save button
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictUserProfilePage}[view_scrollable_edit_profile]    ${dictUserProfilePage}[btn_save]

Click change my password tab
    CommonMobileKeywords.Click Element    ${dictUserProfilePage}[lbl_change_my_password]

Click menu wishlist
    CommonMobileKeywords.Click Element    ${dictUserProfilePage}[txt_menu_wishlist]    timeout=${GLOBALTIMEOUT}