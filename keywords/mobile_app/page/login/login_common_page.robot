*** Keywords ***
Input text password
    [Arguments]    ${pwd}
    AppiumLibrary.Wait Until Element Is Visible    ${dictLoginPage}[txt_password]
    AppiumLibrary.Clear Text    ${dictLoginPage}[txt_password]
    # AppiumLibrary.Input Text    ${dictLoginPage}[txt_password]    ${pwd}
    AppiumLibrary.Input Password    ${dictLoginPage}[txt_password]    ${pwd}

Input text email
    [Arguments]    ${user}
    AppiumLibrary.Wait Until Element Is Visible    ${dictLoginPage}[txt_email]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Clear Text    ${dictLoginPage}[txt_email]
    AppiumLibrary.Input Text    ${dictLoginPage}[txt_email]    ${user}

Click login button
    CommonMobileKeywords.Click Element    ${dictLoginPage}[btn_login]

Click lbl email
    CommonMobileKeywords.Click Element    ${dictLoginPage}[lbl_email]

Click login with facebook button
    CommonMobileKeywords.Click Element    ${dictLoginPage}[btn_login_with_facebook]

Click english language
    AppiumLibrary.Wait Until Element Is Visible    ${dictLoginPage}[btn_english_language]    timeout=${GLOBALTIMEOUT}
    CommonMobileKeywords.Click Element    ${dictLoginPage}[btn_english_language]

Input text email facebook
    [Arguments]    ${user}
    AppiumLibrary.Wait Until Element Is Visible    ${dictLoginPage}[txt_email_facebook]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Input Text    ${dictLoginPage}[txt_email_facebook]    ${user}

Input text password facebook
    [Arguments]    ${pwd}
    AppiumLibrary.Wait Until Element Is Visible    ${dictLoginPage}[txt_password_facebook]
    AppiumLibrary.Input Password    ${dictLoginPage}[txt_password_facebook]    ${pwd}

Click submit login facebook button
    CommonMobileKeywords.Click Element    ${dictLoginPage}[btn_submit_login_facebook]