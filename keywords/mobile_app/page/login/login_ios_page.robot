*** Variables ***
&{dictLoginPage}
...    btn_login=chain=**/XCUIElementTypeOther/XCUIElementTypeButton[`name=="login_button"`]/XCUIElementTypeStaticText[`name=="${login_dto.btn_login_with_space}"`]
...    txt_email=chain=**/*[`type=="XCUIElementTypeOther"`][$type == "XCUIElementTypeStaticText" AND name BEGINSWITH "${login_dto.lbl_email}"$]/XCUIElementTypeTextField
...    txt_password=chain=**/*[`type=="XCUIElementTypeOther"`][$type == "XCUIElementTypeStaticText" AND name BEGINSWITH "${login_dto.lbl_pwd}"$]/XCUIElementTypeSecureTextField
...    lbl_email=chain=**/*[`type=="XCUIElementTypeStaticText"`][`name BEGINSWITH "${login_dto.lbl_email}"`]
...    btn_login_with_facebook=id=login_login_with_facebook
...    btn_english_language=id=English (UK)
...    txt_email_facebook=chain=**/XCUIElementTypeOther[`name=="main"`]/XCUIElementTypeTextField
...    txt_password_facebook=chain=**/XCUIElementTypeOther[`name=="main"`]/XCUIElementTypeSecureTextField
...    btn_submit_login_facebook=id=Log In