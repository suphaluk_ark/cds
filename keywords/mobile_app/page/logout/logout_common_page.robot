*** Keywords ***
Scroll to logout button
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictUserProfilePage}[view_scrollable_profile]    ${dictLogoutPage}[btn_logout]     ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}

Click logout button
    CommonMobileKeywords.Click Element    ${dictLogoutPage}[btn_logout]