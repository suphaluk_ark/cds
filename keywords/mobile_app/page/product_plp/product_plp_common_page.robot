*** Keywords ***
Click 'Men' category icon
    CommonMobileKeywords.Click Element  ${dictProductListPage}[icon_men]

Click category button
    CommonMobileKeywords.Click Element  ${dictProductListPage}[btn_category]

Scroll from 'All Men' to 'Automate Test'
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictProductListPage}[btn_all_men]    ${dictProductListPage}[btn_automate_test]

Click 'Automate Test' button
    CommonMobileKeywords.Click Element  ${dictProductListPage}[btn_automate_test]

Go to PLP of 'Men' category
    product_plp_common_page.Click 'Men' category icon
    product_plp_common_page.Click category button
    product_plp_common_page.Scroll from 'All Men' to 'Automate Test'
    product_plp_common_page.Click 'Automate Test' button

Product name without review rating should be displayed on shopping cart
    [Arguments]    ${product_name}
    ${locator}    String.Format String    ${dictProductListPage}[txt_without_review_product_name]    product_name=${product_name}
    AppiumLibrary.Wait Until Page Contains Element   ${locator}

Product name with review rating should be displayed on shopping cart
    [Arguments]    ${product_name}
    ${locator}    String.Format String    ${dictProductListPage}[txt_with_review_product_name]    product_name=${product_name}
    AppiumLibrary.Wait Until Page Contains Element   ${locator}

Verify product without review rating
    [Arguments]    ${product_sku}
    product_plp_common_page.Product name without review rating should be displayed on shopping cart    ${${product_sku}}[name]
    AppiumLibrary.Wait Until Element Is Visible    ${dictProductListPage}[txt_review]    
    common_mobile_app_keywords.Element Text Should Be    ${dictProductListPage}[txt_review]    ${product_list_page_dto.txt_review}

Verify product with review rating
    [Arguments]    ${product_sku}
    product_plp_common_page.Product name with review rating should be displayed on shopping cart    ${${product_sku}}[name]
    AppiumLibrary.Wait Until Element Is Visible    ${dictProductListPage}[icon_star]

Verify the total items label is visible
    AppiumLibrary.Wait Until Element Is Visible     ${dictProductListPage}[lbl_total_items]    timeout=${GLOBALTIMEOUT}

Verify the total items displayed
    [Arguments]    ${total}
    ${locator}    String.Format String    ${dictProductListPage}[lbl_total]    total=${total}
    AppiumLibrary.Wait Until Page Contains Element     ${locator}    ${GLOBALTIMEOUT}

Get text total items
    ${total}=    AppiumLibrary.Get Text     ${dictProductListPage}[lbl_total_items]
    [Return]    ${total}

Click on filter icon
    CommonMobileKeywords.Click Element     ${dictProductListPage}[icon_filter]

Select filter type
    [Arguments]    ${filter_type}
    ${locator}    String.Format String    ${dictProductListPage}[lbl_filter_type]    filter_type=${filter_type}
    CommonMobileKeywords.Click Element    ${locator}

Select filter option
    [Arguments]    ${selected_filter_option}
    ${locator}    String.Format String    ${dictProductListPage}[filter_option]    filter_option=${selected_filter_option}
    CommonMobileKeywords.Click Element    ${locator}

Click apply filter button
    Wait Until Keyword Succeeds    3 x    1 sec     CommonMobileKeywords.Click Element    ${dictProductListPage}[btn_apply_filter]

Click on the product name
    [Arguments]    ${product_name}
    ${locator}    String.Format String    ${dictProductListPage}[lbl_product_name]    product_name=${product_name}
    CommonMobileKeywords.Click Element    ${locator}