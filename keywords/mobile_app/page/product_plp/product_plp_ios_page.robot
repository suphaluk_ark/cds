*** Variables ***
&{dictProductListPage}
...    icon_men=chain=**/XCUIElementTypeStaticText[`label == "${product_list_page_dto.txt_men_category}"`]
...    btn_category=chain=**/XCUIElementTypeStaticText[`name == "${product_list_page_dto.btn_category}"`]
...    btn_all_men=chain=**/*[`value == "All Men"`]
...    btn_automate_test=chain=**/*[`value == "Automate Test"`]
...    txt_without_review_product_name=chain=**/*[`value == "{product_name}"`]
...    txt_with_review_product_name=chain=**/*[`value == "{product_name}"`]
...    txt_review=chain=**/*[`value == "${product_list_page_dto.txt_review}"`]
...    icon_star=chain=**/*[`name == "productCellRatingView1"`]
...    icon_wishlist_active=chain=**/XCUIElementTypeButton[`name == 'productDetailMainAddToWishlistButton' and value == '1'`]
...    lbl_total_items=accessibility_id=plpTotalLabel
...    lbl_total=chain=**/XCUIElementTypeStaticText[`value == "{total} ${product_list_page_dto.lbl_items}"`]
...    icon_filter=chain=**/XCUIElementTypeOther[$name =="ic_filter" and type =="XCUIElementTypeImage"$][11]
...    lbl_filter_type=chain=**/XCUIElementTypeCell[$type =="XCUIElementTypeStaticText" and value =="{filter_type}"$]
...    filter_option=chain=**/XCUIElementTypeCell/XCUIElementTypeStaticText[`value == "{filter_option}"`]
...    btn_apply_filter=accessibility_id=plpFilterApplyButton
...    lbl_product_name=chain=**/XCUIElementTypeCell[$type == "XCUIElementTypeStaticText" and value == "{product_name}"$]