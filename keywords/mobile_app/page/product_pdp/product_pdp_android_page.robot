*** Variables ***
&{dictProductDetailPage}
...    btn_add_cart=android=UiSelector().resourceIdMatches(".*id/buttonAddToCart$") 
...    icon_cart=android=UiSelector().resourceIdMatches(".*id/imageButton$")
...    icon_cart_with_product=android=UiSelector().resourceIdMatches(".*id/bgBadge$")
...    icon_wishlist=android=UiSelector().resourceIdMatches(".*id/buttonWishList$")
...    lbl_added=android=UiSelector().resourceIdMatches(".*id/buttonAddToCart$").text("${product_detail_page.lbl_added}")
...    lbl_add_to_bag=android=UiSelector().resourceIdMatches(".*id/buttonAddToCart$").text("${product_detail_page.lbl_add_to_bag}")

*** Keywords ***
Click wishlist button
    ${is_selected}=   AppiumLibrary.Get Element Attribute    ${dictProductDetailPage}[icon_wishlist]    selected
    ${is_selected}    Convert To Boolean    ${is_selected}
    BuiltIn.Run Keyword If    ${is_selected} == ${False}    CommonMobileKeywords.Click Element  ${dictProductDetailPage}[icon_wishlist]

Click heart icon to remove wishlist item
    ${is_selected}=   AppiumLibrary.Get Element Attribute    ${dictProductDetailPage}[icon_wishlist]    selected
    ${is_selected}    Convert To Boolean    ${is_selected}
    BuiltIn.Run Keyword If    ${is_selected} == ${True}    CommonMobileKeywords.Click Element  ${dictProductDetailPage}[icon_wishlist]