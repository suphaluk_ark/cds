*** Variables ***
&{dictRegisterPage}    
...    txt_first_name=android=UiSelector().resourceIdMatches(".*id/editTextFirstName$")
...    txt_last_name=android=UiSelector().resourceIdMatches(".*id/editTextLastName$")
...    txt_email=android=UiSelector().resourceIdMatches(".*id/editTextEmail$")
...    txt_pwd=android=UiSelector().resourceIdMatches(".*id/editTextPassword$")
...    chk_privacy_policy=android=UiSelector().resourceIdMatches(".*id/checkboxConsent$")
...    btn_register=android=UiSelector().resourceIdMatches(".*id/buttonRegister$")
...    lbl_successfully_registered=android=UiSelector().resourceIdMatches(".*id/textViewTitle$")
...    btn_continue_shopping=android=UiSelector().resourceIdMatches(".*id/buttonContinueShopping$")
...    lbl_register=android=UiSelector().resourceIdMatches(".*id/textViewRegister$")
...    lbl_marketing_consent_description=android=UiSelector().resourceIdMatches(".*id/textViewConsent$")
...    lbl_marketing_consent_description_on_checkbox=android=UiSelector().resourceIdMatches(".*id/textViewCheckbox$")
...    txt_PDPA_consent=android=UiSelector().resourceIdMatches(".*id/textViewNote$")