*** Variables ***
&{dictRegisterPage}    
...    txt_first_name=chain=**/XCUIElementTypeTextField[`value == "${register_dto.txt_first_name_placeholder}"`]
...    txt_last_name=chain=**/XCUIElementTypeTextField[`value == "${register_dto.txt_last_name_placeholder}"`]
...    txt_email=chain=**/XCUIElementTypeTextField[`value == "${register_dto.txt_email_placeholder}"`]
...    txt_pwd=chain=**/XCUIElementTypeSecureTextField[`value == "${register_dto.txt_pwd_placeholder}"`]
...    chk_privacy_policy=chain=**/XCUIElementTypeButton[`name == "ic uncheck"`]
...    btn_register=chain=**/XCUIElementTypeButton[`label == "register_button"`]
...    lbl_successfully_registered=chain=**/XCUIElementTypeStaticText[`label == "register_success_label"`]
...    btn_continue_shopping=chain=**/XCUIElementTypeButton[`label == "continue_shopping"`]
...    lbl_register=chain=**/XCUIElementTypeOther[$name=="Overlay controller"$]/**/XCUIElementTypeStaticText[`name == "${register_dto.lbl_register}"`][1]
...    lbl_marketing_consent_description=chain=**/XCUIElementTypeStaticText[`label == "${register_dto.lbl_marketing_consent_description}"`]
...    lbl_marketing_consent_description_on_checkbox=chain=**/XCUIElementTypeButton[`label == "${register_dto.lbl_marketing_consent_description_on_checkbox}"`]
...    txt_PDPA_consent=chain=**/XCUIElementTypeStaticText[`label == "${register_dto.lbl_PDPA_consent}"`][2]