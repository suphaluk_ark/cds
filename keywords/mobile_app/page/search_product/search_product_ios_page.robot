*** Variables ***
&{dictSearchProductPage}
...    icon_delete=chain=**/XCUIElementTypeButton[`name == "Clear text"`]
...    msg_not_found_anything=chain=**/XCUIElementTypeStaticText[`name == "${no_result_found_page_dto.msg_not_find_anything}"`]
...    msg_chat_with_assistant=chain=**/XCUIElementTypeStaticText[`name CONTAINS[cd] "${no_result_found_page_dto.msg_chat_with_assistant}"`]
...    btn_chat_now=chain=**/XCUIElementTypeButton[`name CONTAINS[cd] "${no_result_found_page_dto.btn_chat_now}"`]