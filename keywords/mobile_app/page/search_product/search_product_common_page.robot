*** Keywords ***
Verify the delete icon should be displayed on search box
    AppiumLibrary.Wait Until Element Is Visible    ${dictSearchProductPage}[icon_delete]

Verify message displays correctly at not found result page
    CommonMobileKeywords.Verify Elements Are Visible    ${dictSearchProductPage}[msg_not_found_anything]    ${dictSearchProductPage}[msg_chat_with_assistant]    ${dictSearchProductPage}[btn_chat_now]

Click icon delete clear text
    CommonMobileKeywords.Click Element    ${dictSearchProductPage}[icon_delete]