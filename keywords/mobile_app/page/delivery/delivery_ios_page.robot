*** Variables ***
&{dictCheckoutDeliveryPage}
...    view_scrollable_locator=chain=**/XCUIElementTypeTable
...    txt_firstname=chain=**/XCUIElementTypeTextField[`value == '${checkout_delivery_page_dto.txt_enter_first_name_placeholder}'`]
...    lbl_customer_info=chain=**/XCUIElementTypeStaticText[` name == '${checkout_delivery_page_dto.lbl_customer_info}'`]
...    lbl_phoneno=chain=**/XCUIElementTypeStaticText[` name == '${checkout_delivery_page_dto.lbl_phoneno}'`]
...    lbl_delivery_option=chain=**/XCUIElementTypeStaticText[`name == '${checkout_delivery_page_dto.lbl_delivery_option}'`]
...    txt_lastname=chain=**/XCUIElementTypeTextField[`value == '${checkout_delivery_page_dto.txt_enter_last_name_placeholder}'`]
...    txt_email=chain=**/XCUIElementTypeTextField[`value == '${checkout_delivery_page_dto.txt_enter_email_placeholder}'`]
...    txt_phoneno=chain=**/XCUIElementTypeTextField[`value == '${checkout_delivery_page_dto.txt_enter_phone_no_placeholder}'`]
...    rdo_home_delivery=chain=**/XCUIElementTypeStaticText[`value CONTAINS[cd] '${checkout_delivery_page_dto.rdo_home_delivery}'`]
...    rdo_standard_delivery=chain=**/XCUIElementTypeStaticText[`name == '${checkout_delivery_page_dto.txt_standard_delivery}'`]
...    rdo_standard_pickup=chain=**/XCUIElementTypeStaticText[`value == "${checkout_delivery_page_dto.rdo_standard_pickup}"`][2]
...    lbl_store_name=chain=**/XCUIElementTypeStaticText[$ name == 'address_name_label' AND value == '{store_name}'$]
...    btn_select_store=chain=**/XCUIElementTypeCell[$ name == 'address_name_label' AND value == '{store_name}'$]/XCUIElementTypeButton[$name == '${checkout_delivery_page_dto.lbl_select_store}'$]
...    btn_continue_payment=chain=**/XCUIElementTypeButton[`name == '${checkout_delivery_page_dto.btn_continue_to_payment}'`]
...    swt_self_pickup=chain=**/XCUIElementTypeSwitch[`name == '${checkout_delivery_page_dto.lbl_self_pickup}'`]
...    lbl_default_address=chain=**/XCUIElementTypeCell[$ name == 'address_cell'$]/XCUIElementTypeButton[`name == 'bt bg enable white'`]
...    txt_place_building=chain=**/XCUIElementTypeTextField[`value == "${checkout_delivery_page_dto.txt_building_place}"`]
...    txt_house_no_street=chain=**/XCUIElementTypeTextField[`value == "${checkout_delivery_page_dto.txt_house_number_street}"`]
...    txt_postcode=chain=**/XCUIElementTypeTextField[`value == "${checkout_delivery_page_dto.txt_postcode}"`]
...    ddl_district=chain=**/XCUIElementTypeTextField[`value == "${checkout_delivery_page_dto.ddl_select_district}"`]
...    ddl_subdistrict=chain=**/XCUIElementTypeTextField[`value == "${checkout_delivery_page_dto.ddl_select_subdistrict}"`]
...    lbl_title_secure_checkout=chain=**/XCUIElementTypeOther[$name =="ic back" and type =="XCUIElementTypeButton"$]/XCUIElementTypeStaticText[`label = "${checkout_delivery_page_dto.lbl_title_secure_checkout}"`]
...    lbl_PDPA_consent_text=chain=**/XCUIElementTypeStaticText[`label == "${checkout_delivery_page_dto.lbl_PDPA_consent}"`]
...    lbl_marketing_consent_description=chain=**/XCUIElementTypeStaticText[`label == "${checkout_delivery_page_dto.lbl_marketing_consent_description}"`]
...    lbl_marketing_consent_description_on_checkbox=chain=**/XCUIElementTypeButton[`label == "${checkout_delivery_page_dto.lbl_marketing_consent_description_on_checkbox}"`]
...    lbl_secure_checkout=chain=**/XCUIElementTypeButton[`label == "${checkout_delivery_page_dto.btn_continue_to_payment}"`]
...    txt_search_store=chain=**/XCUIElementTypeTextField[`name == "searchTextField3"`]
...    rdo_click_and_collect=chain=**/XCUIElementTypeStaticText[`value CONTAINS[cd] '${checkout_delivery_page_dto.rdo_click_collect}'`]
...    rdo_1hour_pickup=chain=**/XCUIElementTypeStaticText[`value CONTAINS[cd] "${checkout_delivery_page_dto.rdo_1hour_pickup}"`][-1]
...    rdo_next_day_pickup=chain=**/XCUIElementTypeStaticText[`value CONTAINS[cd] "${checkout_delivery_page_dto.rdo_next_day_pickup}"`][-1]
...    rdo_next_day_delivery=chain=**/XCUIElementTypeCell[$type == 'XCUIElementTypeStaticText' and label == '${checkout_delivery_page_dto.lbl_lead_time_next_day_delivery}'$]/XCUIElementTypeStaticText[`label == '${checkout_delivery_page_dto.rdo_next_day_delivery}'`]



&{dictCheckoutDeliveryPage_ios}
...    ddl_district_list_1st_index=chain=**/XCUIElementTypeOther[$ name == "drop_down" $]/**/XCUIElementTypeCell[1]
...    ddl_subdistrict_list_1st_index=chain= **/XCUIElementTypeOther[$ name == "drop_down" $]/**/XCUIElementTypeCell[1]

*** Keywords ***
Search for a store name
    [Arguments]    ${store_name}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[txt_search_store]
    AppiumLibrary.Input Text    ${dictCheckoutDeliveryPage}[txt_search_store]    ${store_name}
    common_ios_mobile_app_keywords.Click Search key on the virtual keyboard
    ${locator}=    CommonKeywords.Format Text    ${dictCheckoutDeliveryPage}[lbl_store_name]    store_name=${store_name}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${locator}

Click Select Store
    [Arguments]    ${store_name}
    ${locator}=    CommonKeywords.Format Text    ${dictCheckoutDeliveryPage}[btn_select_store]    store_name=${store_name}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${locator}    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element  ${locator}

Select 'Automate - Test Store Information'
    [Documentation]    Store name: Automate - Test Store Information
    Search for a store name    ${checkout_delivery_page_dto.lbl_automate_test_store}
    Click Select Store    ${checkout_delivery_page_dto.lbl_automate_test_store}

Select district by 1st index
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[ddl_district]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[ddl_district]
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage_ios}[ddl_district_list_1st_index]

Select subdistrict by 1st index
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[ddl_subdistrict]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[ddl_subdistrict]
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage_ios}[ddl_subdistrict_list_1st_index]
