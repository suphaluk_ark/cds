*** Keywords ***
Click icon cart
    CommonMobileKeywords.Click Element    ${dictCart}[icon_cart]

Input text into search box
    [Arguments]    ${sku}
    CommonMobileKeywords.Click Element    ${dictHeader}[txt_search]
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictHeader}[txt_search_after_click]    ${sku}