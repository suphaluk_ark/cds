*** Keywords ***
Input current password
    [Arguments]    ${pwd}
    AppiumLibrary.Wait Until Element Is Visible    ${dictChangePasswordPage}[txt_current_password]
    AppiumLibrary.Clear Text    ${dictChangePasswordPage}[txt_current_password]
    AppiumLibrary.Input Password    ${dictChangePasswordPage}[txt_current_password]    ${pwd}

Input new password
    [Arguments]    ${pwd}
    AppiumLibrary.Wait Until Element Is Visible    ${dictChangePasswordPage}[txt_new_password]
    AppiumLibrary.Clear Text    ${dictChangePasswordPage}[txt_new_password]
    AppiumLibrary.Input Password    ${dictChangePasswordPage}[txt_new_password]    ${pwd}

Input confirm new password
    [Arguments]    ${pwd}
    AppiumLibrary.Wait Until Element Is Visible    ${dictChangePasswordPage}[txt_confirm_new_password]
    AppiumLibrary.Clear Text    ${dictChangePasswordPage}[txt_confirm_new_password]
    AppiumLibrary.Input Password    ${dictChangePasswordPage}[txt_confirm_new_password]    ${pwd}

Click confirm change button
    CommonMobileKeywords.Click Element    ${dictChangePasswordPage}[btn_confirm_change]

Click current password label
     CommonMobileKeywords.Click Element    ${dictChangePasswordPage}[lbl_current_password]