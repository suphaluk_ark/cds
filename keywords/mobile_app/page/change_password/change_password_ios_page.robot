*** Variables ***
&{dictChangePasswordPage}
...    txt_current_password=chain=**/XCUIElementTypeSecureTextField[1]
...    txt_new_password=chain=**/XCUIElementTypeSecureTextField[2]
...    txt_confirm_new_password=chain=**/XCUIElementTypeSecureTextField[3]
...    btn_confirm_change=chain=**/XCUIElementTypeButton[`name=="${my_account_page_dto.btn_confirm_change}"`]
...    lbl_current_password=id=${my_account_page_dto.lbl_current_password} *