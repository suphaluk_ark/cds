*** Variables ***
&{dictChangePasswordPage}
...    txt_current_password=android=UiSelector().resourceIdMatches(".*id/editTextCurrentPassword$")
...    txt_new_password=android=UiSelector().resourceIdMatches(".*id/editTextNewPassword$")
...    txt_confirm_new_password=android=UiSelector().resourceIdMatches(".*id/editTextConfirmNewPassword$")
...    btn_confirm_change=android=UiSelector().resourceIdMatches(".*id/buttonConfirm$")