*** Variables ***
&{dictSaveCardPage}
...    lbl_my_credit_cards=android=UiSelector().text("${payment_page_dto.lbl_my_credit_cards}")
...    txt_credit_card_no=android=UiSelector().resourceIdMatches(".*id/editTextCardNumber$")
...    txt_expired_date=android=UiSelector().resourceIdMatches(".*id/editTextExpiredDate$")
...    txt_cvv=android=UiSelector().resourceIdMatches(".*id/editTextCVV$")
...    txt_name=android=UiSelector().resourceIdMatches(".*id/editTextName$")
...    btn_option=android=UiSelector().resourceIdMatches(".*id/buttonOption$")
...    btn_delete=android=UiSelector().resourceIdMatches(".*id/textViewLabel$").textContains("${payment_page_dto.lbl_delete}")
...    btn_confirm_delete=android=UiSelector().resourceIdMatches(".*id/buttonPositive$").textContains("${payment_page_dto.lbl_delete}")
...    btn_save_card=android=UiSelector().resourceIdMatches(".*id/buttonSaveCard$")
...    txt_hide_credit_card_no=android=UiSelector().resourceIdMatches(".*id/textViewNumber$")
...    swt_default_payment=android=UiSelector().resourceIdMatches(".*id/switchDefault$").textContains("${payment_page_dto.swt_default_payment}")