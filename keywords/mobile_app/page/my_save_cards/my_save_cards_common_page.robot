*** Keywords ***
Delete saved credit card
    CommonMobileKeywords.Click Element    ${dictSaveCardPage}[btn_option]
    CommonMobileKeywords.Click Element    ${dictSaveCardPage}[btn_delete]
    CommonMobileKeywords.Click Element    ${dictSaveCardPage}[btn_confirm_delete]

Click my credit cards
    Wait Until Keyword Succeeds    3 x    1 sec    Run Keywords
    ...    CommonMobileKeywords.Click Element    ${dictSaveCardPage}[lbl_my_credit_cards]    AND
    ...    AppiumLibrary.Wait Until Element Is Visible    ${dictSaveCardPage}[txt_credit_card_no]    ${GLOBALTIMEOUT}

Click save card button
    CommonMobileKeywords.Click Element    ${dictSaveCardPage}[btn_save_card]

Verify save credit card should be visible
    [Arguments]    ${hide_card_number}
    Wait Until Keyword Succeeds    3 x    1 sec    common_mobile_app_keywords.Element Text Should Contains    ${dictSaveCardPage}[txt_hide_credit_card_no]    ${hide_card_number}
    AppiumLibrary.Wait Until Element Is Visible    ${dictSaveCardPage}[swt_default_payment]

Input credit card no
    [Arguments]    ${card_no}
    AppiumLibrary.Input Text    ${dictSaveCardPage}[txt_credit_card_no]    ${card_no}

Input credit card no 2 times
    [Arguments]    @{list_card_no}
    :FOR     ${card_no}    IN    @{list_card_no}
        AppiumLibrary.Input Text    ${dictSaveCardPage}[txt_credit_card_no]    ${card_no}
    END

Input expired date
    [Arguments]    ${expired_date}
    AppiumLibrary.Input Text    ${dictSaveCardPage}[txt_expired_date]    ${expired_date}

Input expired date with slash
    [Arguments]    ${expired_date_with_slash}
    AppiumLibrary.Input Text    ${dictSaveCardPage}[txt_expired_date]    ${expired_date_with_slash}

Input cvv
    [Arguments]    ${cvv}
    AppiumLibrary.Input Text    ${dictSaveCardPage}[txt_cvv]    ${cvv}

Input name customer
    [Arguments]    ${name}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictSaveCardPage}[txt_name]    ${name}

Input cvv saved card
    [Arguments]    ${cvv}
    AppiumLibrary.Input Text     ${dictSaveCardPage}[txt_cvv_saved_card]    ${cvv}
