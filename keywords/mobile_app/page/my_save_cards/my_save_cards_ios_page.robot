*** Variables ***
&{dictSaveCardPage}
...    lbl_my_credit_cards=chain=**/XCUIElementTypeStaticText[`name=="${payment_page_dto.lbl_my_credit_cards}"`]
...    txt_credit_card_no=chain=**/XCUIElementTypeOther[`name=="cardNumberTextView"`]/**XCUIElementTypeTextField
...    txt_expired_date=id=expiredDateTextView
...    txt_cvv=id=cvvTextView
...    txt_name=id=fullNameTextView
...    swt_default_payment=chain=**/XCUIElementTypeSwitch[`name=="setDefaultPaymentSwitch0" AND value=="1"`]
...    btn_save_card=id=saveCardButton
...    txt_hide_credit_card_no=chain=**/XCUIElementTypeStaticText[`value == "${payment.credit_card.hide_card_number}"`]
...    btn_option=chain=**/XCUIElementTypeButton[`name=="ic options"`]
...    btn_delete=id=delete_address_label
...    btn_confirm_delete=id=confirm_delete_address_button
