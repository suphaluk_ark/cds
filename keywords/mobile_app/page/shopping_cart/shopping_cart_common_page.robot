*** Keywords ***
Verify the title "SHOPPING BAG" should be displayed
    AppiumLibrary.Wait Until Element Is Visible    ${dictShoppingCartPage}[lbl_shopping_bag]    timeout=${GLOBALTIMEOUT}

Click checkout product button
    CommonMobileKeywords.Click Element   ${dictShoppingCartPage}[btn_checkout]    ${GLOBALTIMEOUT}

Retry click checkout product button
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    BuiltIn.Run Keywords
    ...    Click checkout product button
    ...    AND    delivery_common_page.Verify the title page is visible

Verify promotion item details
    [Arguments]    ${product_sku}
    AppiumLibrary.Wait Until Element Is Visible    ${dictShoppingCartPage}[lbl_promo_header]    timeout=${GLOBALTIMEOUT}
    shopping_cart_common_page.Promotion item name should be displayed on shopping cart    ${${product_sku}}[name]   
    Promotion item status should be displayed on shopping cart

Promotion item name should be displayed on shopping cart
    [Arguments]    ${product_name}
    ${locator}    String.Format String    ${dictShoppingCartPage}[lbl_product_name]    product_name=${product_name}
    AppiumLibrary.Wait Until Page Contains Element   ${locator}

Promotion item status should be displayed on shopping cart
    AppiumLibrary.Wait Until Element Is Visible    ${dictShoppingCartPage}[lbl_promotion_item_status]

Click Add to wishlist
    CommonMobileKeywords.Click Element   ${dictShoppingCartPage}[lbl_add_wishlist]    timeout=${GLOBALTIMEOUT}

Click icon back page
    CommonMobileKeywords.Click Element   ${dictCommonPage}[icon_back_page]    timeout=${GLOBALTIMEOUT}

Click remove item in cart
    CommonMobileKeywords.Click Element   ${dictShoppingCartPage}[lbl_remove]

Click confirm remove
    CommonMobileKeywords.Click Element   ${dictShoppingCartPage}[lbl_confirm_remove]

Verify item remove in cart
    AppiumLibrary.Wait Until Element Is Visible    ${dictShoppingCartPage}[btn_continue_shopping]

Input coupon code
    [Arguments]    ${coupon_code}
    AppiumLibrary.Wait Until Element Is Visible    ${dictShoppingCartPage}[txt_coupon_code]
    AppiumLibrary.Input Text    ${dictShoppingCartPage}[txt_coupon_code]    ${coupon_code}

Click apply button
    CommonMobileKeywords.Click Element    ${dictShoppingCartPage}[btn_apply]

Verify coupon code displayed correctly
    [Arguments]    ${coupon_code}
    ${locator}    String.Format String    ${dictShoppingCartPage}[lbl_coupon_code]    coupon_code=${coupon_code}
    AppiumLibrary.Wait Until Page Contains Element    ${locator}    ${GLOBALTIMEOUT}

Verify order total is displayed correctly
    [Arguments]    ${actual_price}
    ${total_price}=    shopping_cart_common_page.Get total price
    BuiltIn.Should Be Equal As Integers    ${actual_price}    ${total_price}

Get total price    
    ${text}=    AppiumLibrary.Get Text    ${dictShoppingCartPage}[lbl_total]
    ${total_price}=    CommonKeywords.Convert price to number format    ${text}
    [Return]    ${total_price}