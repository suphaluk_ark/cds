*** Variables ***
&{dictWelcome}
...    btn_english=android=UiSelector().resourceIdMatches(".*id/buttonLangEnglish$")
...    btn_thai=android=UiSelector().resourceIdMatches(".*id/buttonLangThai$")
...    btn_skip=android=UiSelector().resourceIdMatches(".*id/buttonSkip$")
...    btn_turn_on=android=UiSelector().resourceIdMatches(".*id/buttonTurnOn$")

*** Keywords ***
Select preferred language and skip button
    [Arguments]    ${language}
    Run Keyword And Ignore Error    CommonMobileKeywords.Click Element    ${language}
    Run Keyword And Ignore Error    CommonMobileKeywords.Click Element    ${dictWelcome}[btn_skip]
    Run Keyword And Ignore Error    CommonMobileKeywords.Click Element    ${dictWelcome}[btn_turn_on]
    home_common_page.Verify that home screen is shown