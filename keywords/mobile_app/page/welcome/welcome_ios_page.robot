*** Variables ***
&{dictWelcome}
...    btn_english=id=onboardingLanguageEnglishButton
...    btn_thai=id=onboardingLanguageThaiButton
...    btn_skip=id=onboardingSkipButton
...    allow_once=chain=**/XCUIElementTypeButton[1]


*** Keywords ***
Click Allow Once On iOS 14
    [Documentation]    TODO: 20201109 - workaround accept alert on ios 14 and waiting appiun.io fix it
    Run Keyword If  str('${${mobileDevice}}[platformName]').lower() == 'ios' and '14' in '${${mobileDevice}}[platformVersion]'    CommonMobileKeywords.Click Element    ${dictWelcome}[allow_once]