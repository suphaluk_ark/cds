*** Keywords ***
Verify product name is visible
    AppiumLibrary.Wait Until Element Is Visible    ${dictWishlistPage}[txt_product]    timeout=${GLOBALTIMEOUT}

Click heart icon
    CommonMobileKeywords.Click Element    ${dictWishlistPage}[icon_heart]
    
Click icon back page
    CommonMobileKeywords.Click Element  ${dictCommonPage}[icon_back_page]    timeout=${GLOBALTIMEOUT}

Verify product name is not visible
    AppiumLibrary.Wait Until Element Is Visible    ${dictWishlistPage}[txt_empty]    timeout=${GLOBALTIMEOUT}

Click add to bag
    CommonMobileKeywords.Click Element    ${dictWishlistPage}[icon_add_to_bag]

Verify heart icon
    AppiumLibrary.Wait Until Element Is Visible    ${dictWishlistPage}[icon_heart_active]    timeout=${GLOBALTIMEOUT}