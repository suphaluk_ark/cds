*** Variables ***
&{dictWishlistPage}    
...    txt_product=chain=**/XCUIElementTypeButton[`name == 'wishlist_product_name'`]
...    icon_heart=chain=**/XCUIElementTypeButton[`name == 'wishlist_icon'`]
...    icon_add_to_bag=chain=**/XCUIElementTypeButton[`name == 'wishlist_add_to_cart'`]
...    icon_cart=chain=**/XCUIElementTypeButton[`name == 'ic cart'`]
...    txt_empty=chain=**/XCUIElementTypeStaticText[`name == "wishlist_empty_label"`]