*** Keywords ***
Verify that 'Your order is successful' should be visible
    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutCompletedPage}[lbl_ordered_successfully]    timeout=${GLOBALTIMEOUT}

Verify that 'Sorry, payment failed!' should be displayed
    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutCompletedPage}[lbl_ordered_failed]    timeout=${GLOBALTIMEOUT}

Get non-split order number
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutCompletedPage}[lbl_ordered_number]
    ${order_number_lbl}=    AppiumLibrary.Get Text    ${dictCheckoutCompletedPage}[lbl_ordered_number]
    ${list_of_order_number}=    String.Get Regexp Matches    ${order_number_lbl}    STGCO\\d+    #get the order ID starts with STGCO
    BuiltIn.Should Not Be Empty    ${list_of_order_number}
    ${order_number}=    BuiltIn.Set Variable    ${list_of_order_number}[0]
    [Return]    ${order_number}

Click continue shopping
    CommonMobileKeywords.Click Element    ${dictCheckoutCompletedPage}[btn_continue_shopping]

Verify order total is displayed correctly
    [Arguments]    ${actual_price}
    ${total_price}=    checkout_completed_common_page.Get total price
    BuiltIn.Should Be Equal As Integers    ${actual_price}    ${total_price}

Get total price
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutCompletedPage}[lbl_total]
    CommonMobileKeywords.Verify Elements Are Visible    ${dictCheckoutCompletedPage}[lbl_total]
    ${text}=    AppiumLibrary.Get Text    ${dictCheckoutCompletedPage}[lbl_total]
    ${total_price}=    CommonKeywords.Convert price to number format    ${text}
    [Return]    ${total_price}