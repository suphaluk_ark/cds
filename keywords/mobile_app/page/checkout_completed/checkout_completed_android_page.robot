*** Variables ***
&{dictCheckoutCompletedPage}
...    view_scrollable_locator=android=UiSelector().resourceIdMatches(".*id/recyclerView$")
...    lbl_ordered_successfully=android=UiSelector().text("${thank_you_page_dto.lbl_ordered_successfully}")
...    lbl_ordered_failed=android=UiSelector().text("${thank_you_page_dto.lbl_ordered_failed}")
...    lbl_ordered_number=android=UiSelector().resourceIdMatches(".*id/textViewReferenceNumber$")
...    lbl_ordered_number2=android=UiSelector().resourceIdMatches(".*id/textViewReferenceNumber$").textContains("_2")
...    btn_continue_shopping=android=UiSelector().resourceIdMatches(".*id/buttonContinueShopping$")
...    lbl_total=android=UiSelector().resourceIdMatches(".*id/textViewTotal$")
...    txt_order_number=android=UiSelector().textContains("${thank_you_page_dto.lbl_order_number}")
...    lbl_shipping_method=android=UiSelector().textContains("${thank_you_page_dto.lbl_order_number}")


*** Keywords ***
Get all orders and shipping methods for split order
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutCompletedPage}[view_scrollable_locator]    ${dictCheckoutCompletedPage}[lbl_ordered_number2]
    ${shipping_method_list}=    AppiumLibrary.Get Webelements    ${dictCheckoutCompletedPage}[txt_order_number]
    ${order_id_list}=    AppiumLibrary.Get Webelements    ${dictCheckoutCompletedPage}[lbl_ordered_number]
    ${shipping_method_1}=    AppiumLibrary.Get Text    ${shipping_method_list}[0]
    ${shipping_method_2}=    AppiumLibrary.Get Text    ${shipping_method_list}[1]
    ${order_id_1}=    AppiumLibrary.Get Text    ${order_id_list}[0]
    ${order_id_2}    AppiumLibrary.Get Text    ${order_id_list}[1]
    ${orderDict}    BuiltIn.Create Dictionary    ${shipping_method_1}=${order_id_1}    ${shipping_method_2}=${order_id_2}
    [Return]    ${orderDict}

Get order number through shipping method
    [Arguments]    ${shipping_method}
    ${oderDict}=    Get all orders and shipping methods for split order
    ${order_number}=    Collections.Pop From Dictionary    ${oderDict}    ${shipping_method}
    [Return]    ${order_number}

Get order number that supports 'Standard Pickup'
    [Documentation]    Split order only
    ${order_number}=    Get order number through shipping method    ${thank_you_page_dto.lbl_standard_pickup}
    [Return]    ${order_number}

Get order number that supports 'Standard Delivery'
    [Documentation]    Split order only
    ${order_number}=    Get order number through shipping method    ${thank_you_page_dto.lbl_standard_delivery}
    [Return]    ${order_number}

Get order number that supports '1 Hour Pickup'
    [Documentation]    Split order only
    ${order_number}=    Get order number through shipping method    ${thank_you_page_dto.lbl_1_hr_pickup}
    [Return]    ${order_number}
