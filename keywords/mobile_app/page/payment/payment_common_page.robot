*** Keywords ***
Click payment bank
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[rdo_bank]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[rdo_bank]

Click payment credit card
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[rdo_credit_card]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[rdo_credit_card]

Input credit card no
    [Arguments]    ${card_no}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_credit_card_no]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}    
    Run Keyword If    '${platform}'=='android'    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_credit_card_no]    ${card_no}
    ...    ELSE IF    '${platform}'=='ios'    payment_common_page.Input credit card no 2 times    ${card_no}

Input credit card no for repayment
    [Arguments]    ${card_no}
    Run Keyword If    '${platform}'=='android'    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_credit_card_no]    ${card_no}
    ...    ELSE IF    '${platform}'=='ios'    payment_common_page.Input credit card no 2 times for repayment    ${card_no}

Input credit card no 2 times
    [Documentation]    For iOS workaround only, don't use this keyword
    [Arguments]    ${card_no}
    ${length}=    BuiltIn.Get Length    ${card_no}
    ${first_6_chars}=    String.Get Substring    ${card_no}    0    6
    ${the_rest}=    String.Get Substring    ${card_no}    6    ${length}
    ${full_card}=    Create List    ${first_6_chars}    ${the_rest}
    FOR     ${split_number}    IN    @{full_card}
        AppiumLibrary.Input Text    ${dictPaymentPage}[txt_credit_card_no]    ${split_number}
    END

Input credit card no 2 times for repayment
    [Documentation]    For iOS workaround only, don't use this keyword
    [Arguments]    ${card_no}
    ${length}=    BuiltIn.Get Length    ${card_no}
    ${first_6_chars}=    String.Get Substring    ${card_no}    0    6
    ${the_rest}=    String.Get Substring    ${card_no}    6    ${length}
    ${full_card}=    Create List    ${first_6_chars}    ${the_rest}
    FOR     ${split_number}    IN    @{full_card}
        AppiumLibrary.Input Text    ${dictPaymentPage}[txt_repay_card_no]    ${split_number}
    END

Input expired date
    [Arguments]    ${expired_date}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_expired_date]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_expired_date]    ${expired_date}

Input expired date with slash
    [Arguments]    ${expired_date_with_slash}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_expired_date]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_expired_date]    ${expired_date_with_slash}

Input cvv
    [Arguments]    ${cvv}
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_cvv]    ${cvv}

Input name customer
    [Arguments]    ${name}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_name]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictPaymentPage}[txt_name]    ${name}



Input cvv saved card
    [Arguments]    ${cvv}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_cvv_saved_card]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    AppiumLibrary.Input Text     ${dictPaymentPage}[txt_cvv_saved_card]    ${cvv}

Click button pay now
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[btn_paynow]

Click pay on delivery
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[rdo_pay_on_delivery]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[rdo_pay_on_delivery]
    
Verify that 'saved card' should be visible
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[btn_saved_card]    ${global_scroll_times}
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[btn_saved_card]    timeout=${GLOBALTIMEOUT}

Verify that 'QRCODE' should be visible
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[img_qr_code]    timeout=${GLOBALTIMEOUT}

Click continue button
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[btn_continue]

Click pay at store
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[rdo_pay_at_store]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[rdo_pay_at_store]

Verify error textfield card number
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[lbl_error_cardnumber]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Verify Elements Are Visible  ${dictPaymentPage}[lbl_error_cardnumber]

Verify error textfield expired date
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[lbl_error_expired_date]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Verify Elements Are Visible  ${dictPaymentPage}[lbl_error_expired_date]

Verify error textfield cvv
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[lbl_error_cvv]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Verify Elements Are Visible  ${dictPaymentPage}[lbl_error_cvv]

Verify error textfield fullname on card
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[lbl_error_fullname_card]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Verify Elements Are Visible  ${dictPaymentPage}[lbl_error_fullname_card]

Verify the connect T1 button is not visible
    AppiumLibrary.Page Should Not Contain Element    ${dictPaymentPage}[lnk_login_T1]

Connect T1 link
    CommonMobileKeywords.Click Element  ${dictPaymentPage}[lnk_login_T1]

Input email T1C account
    [Arguments]    ${email}
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_email]    ${email} 

Input password T1C account
    [Arguments]    ${password}
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_password]    ${password} 

Click login to T1C account button
    CommonMobileKeywords.Click Element    ${dictPaymentPage}[btn_login_t1c]

Login to T1C account
    [Arguments]    ${email}    ${password}
    Input email T1C account    ${email}
    Input password T1C account    ${password}
    Click login to T1C account button

Verify order total is displayed correctly
    [Arguments]    ${actual_price}
    ${total_price}=    payment_common_page.Get total price
    BuiltIn.Should Be Equal As Integers    ${actual_price}    ${total_price}

Get total price
    CommonMobileKeywords.Verify Elements Are Visible    ${dictPaymentPage}[lbl_price]
    ${text}=    AppiumLibrary.Get Text    ${dictPaymentPage}[lbl_price]
    ${total_price}=    CommonKeywords.Convert price to number format    ${text}
    [Return]    ${total_price}

Click payment Installment
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[rdo_installment]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[rdo_installment]

Click Bank Installment
    [Arguments]    ${bank_name}
    ${locator}=    String.Format String    ${dictPaymentPage}[bank_installment]    bank_name=${bank_name}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${locator}
    CommonMobileKeywords.Click Element  ${locator}

Input credit card no installment 2 times
    [Arguments]    @{list_card_no}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_credit_card_no_installment]    ${global_scroll_times}*2    ${global_scale_min}    ${global_scale_medium}
    FOR     ${card_no}    IN    @{list_card_no}
        AppiumLibrary.Input Text    ${dictPaymentPage}[txt_credit_card_no_installment]    ${card_no}
    END

Input expired date installment
    [Arguments]    ${expired_date}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_expired_date_installment]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_expired_date_installment]    ${expired_date}

Input cvv installment
    [Arguments]    ${cvv}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_cvv_installment]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_cvv_installment]    ${cvv}

Input name customer installment
    [Arguments]    ${name}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_name_installment]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictPaymentPage}[txt_name_installment]    ${name}

Input OTP 2c2p
    [Arguments]    ${otp}
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[txt_otp]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_otp]    ${otp}

Click Proceed button
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[btn_Proceed]    timeout=${GLOBALTIMEOUT}

Click Cancel button
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[btn_Cancel]    timeout=${GLOBALTIMEOUT}

Verify 'Invalid OTP code. Please recheck the OTP and try again' should be displayed
    CommonMobileKeywords.Verify Elements Are Visible    ${dictPaymentPage}[msg_error]

Verify 'REPAY WITH NEW CARD' should be displayed
    CommonMobileKeywords.Verify Elements Are Visible    ${dictPaymentPage}[lbl_repay_with_new_card]