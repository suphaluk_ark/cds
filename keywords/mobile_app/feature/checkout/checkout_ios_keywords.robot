*** Keywords ***
Search product by product sku, add to cart, checkout
    [Arguments]    ${product_sku}
    checkout_ios_keywords.Retry search product by product sku    ${product_sku}
    product_pdp_common_page.Wait until click add to cart button with product successfully
    shopping_cart_common_page.Click checkout product button

Search product by product sku, add to cart and go back to homepage
    [Arguments]    ${product_sku}
    checkout_ios_keywords.Retry search product by product sku    ${product_sku}
    product_pdp_common_page.Wait until click add to cart button with product successfully
    common_ios_mobile_app_keywords.Click icon back page
    common_ios_mobile_app_keywords.Click icon back page
    product_plp_common_page.Verify the total items label is visible
    common_ios_mobile_app_keywords.Click icon back page
    home_ios_page.Click Cancel button on search box
    home_common_page.Verify that home screen is shown

Retry search product by product sku
    [Arguments]    ${product_sku}
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    BuiltIn.Run Keywords
    ...    common_page.Input text into search box    ${${product_sku}}[sku]
    ...    AND    common_ios_mobile_app_keywords.Click Search key on the virtual keyboard
    ...    AND    product_plp_common_page.Verify the total items label is visible
    ...    AND    product_plp_common_page.Click on the product name    ${${product_sku}}[name]

Search product by product sku
    [Arguments]    ${product_sku}
    checkout_ios_keywords.Retry search product by product sku    ${product_sku}

Checkout completed with verifying price
    [Arguments]    ${expected_price}
    ${order_number}=    checkout_completed_common_page.Get non-split order number
    ${price_on_thankyou_page}=    checkout_completed_common_page.Get total price
    Should Be Equal As Numbers    ${price_on_thankyou_page}    ${expected_price}    msg=Price is not discounted correctly. Expected price = ฿${expected_price}
    checkout_completed_common_page.Click continue shopping
    [Return]    ${order_number}