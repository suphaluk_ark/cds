*** Keywords ***
Change password sucessfully
    [Arguments]    ${pwd}    ${new_pwd}    ${confirm_pwd}    ${first_name}    ${last_name}
    home_common_page.Click user img
    Wait Until Keyword Succeeds    3 x    1 sec    Run Keywords    user_profile_common_page.Click change my password tab    AND    change_password_common_page.Input current password    ${pwd}
    change_password_common_page.Input new password    ${new_pwd}
    change_password_common_page.Input confirm new password    ${confirm_pwd}
    change_password_common_page.Click current password label
    change_password_common_page.Click confirm change button
    user_profile_ios_page.Verify 'firstname and lastname' displays correctly    ${first_name}    ${last_name}