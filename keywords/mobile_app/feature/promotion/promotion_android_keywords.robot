*** Keywords ***
Add product to cart and apply coupon code
    [Arguments]    ${product_sku}    ${coupon_code}
    product_pdp_common_page.Add product to shopping cart    ${product_sku}
    shopping_cart_common_page.Input coupon code    ${coupon_code}
    shopping_cart_common_page.Click apply button
    shopping_cart_common_page.Verify coupon code displayed correctly    ${coupon_code}

Verify order total on shopping cart page after get discount
    [Arguments]    ${product_price}    ${qty}    &{discounts}
    ${total_price}=    promotion_calculator.Calculate total price by summary product price with quantity    ${product_price}    ${qty}
    ${actual_price}=    promotion_calculator.Calculate order total with deduct discount    ${total_price}    &{discounts}
    shopping_cart_common_page.Verify order total is displayed correctly    ${actual_price}

Member, Go to payment checkout page from shopping cart page with selecting pickup at store
    shopping_cart_common_page.Click checkout product button
    delivery_android_keywords.Membership, delivery successfully by standard pickup, self pickup

Verify T1 secction should not visible when credit card coupon appied in cart
    payment_common_page.Verify the connect T1 button is not visible

Verify order total on checkout payment page after get discount
    [Arguments]    ${product_price}    ${qty}    &{discounts}
    ${total_price}=    promotion_calculator.Calculate total price by summary product price with quantity    ${product_price}    ${qty}
    ${actual_price}=    promotion_calculator.Calculate order total with deduct discount    ${total_price}    &{discounts}
    payment_common_page.Verify order total is displayed correctly    ${actual_price}

Checkout complete with saved credit card
    [Documentation]    To process OTP at 2C2P, set process_otp=yes
    [Arguments]    ${process_otp}=no
    payment_android_keywords.Payment successfully with saved credit card    ${process_otp}

Guest, Go to payment checkout page from shopping cart page with selecting pickup at store
    shopping_cart_common_page.Click checkout product button
    delivery_android_keywords.Guest, delivery successfully by standard pickup, self pickup

Checkout complete with entering credit card without OTP
    payment_android_keywords.Payment successfully with credit card without OTP
