*** Keywords ***
Add wishlist pdp page
    product_pdp_ios_page.Click wishlist button

Remove wishlist pdp page
    product_pdp_ios_page.Click heart icon to remove wishlist item

Add wishlist cart page
    product_pdp_common_page.Wait until click add to cart button with product successfully
    shopping_cart_common_page.Click Add to wishlist
    shopping_cart_common_page.Click icon back page

Go to back page
    product_pdp_common_page.Click icon back page
    home_ios_page.Click Cancel button on search box

Go to homePage
    wishlist_common_page.Click icon back page
    user_profile_common_page.Click x button

Verify wishlist is visible
    home_common_page.Click user img
    user_profile_common_page.Click menu wishlist
    wishlist_common_page.Verify product name is visible

Verify wishlist is not visible
    home_common_page.Click user img
    user_profile_common_page.Click menu wishlist
    wishlist_common_page.Verify product name is not visible

Remove wishlist item on wishlist page
    checkout_ios_keywords.Search product by product sku    ${product_sku_list}[3]
    wishlist_ios_keywords.Add wishlist pdp page
    wishlist_ios_keywords.Go to back page
    wishlist_ios_keywords.Verify wishlist is visible
    wishlist_common_page.Click heart icon
    wishlist_common_page.Verify product name is not visible

Add wishlist item on pdp page
    checkout_ios_keywords.Search product by product sku    ${product_sku_list}[3]
    wishlist_ios_keywords.Add wishlist pdp page
    wishlist_ios_keywords.Go to back page
    wishlist_ios_keywords.Verify wishlist is visible

Add wishlist item on cart page
    checkout_ios_keywords.Search product by product sku    ${product_sku_list}[3]
    wishlist_ios_keywords.Add wishlist cart page
    wishlist_ios_keywords.Go to back page
    wishlist_ios_keywords.Verify wishlist is visible

Remove wishlist item on pdp page
    wishlist_ios_keywords.Go to homePage
    checkout_ios_keywords.Search product by product sku    ${product_sku_list}[3]
    wishlist_ios_keywords.Remove wishlist pdp page
    wishlist_ios_keywords.Go to back page
    wishlist_ios_keywords.Verify wishlist is not visible

Add product to cart on wishlist page
    checkout_ios_keywords.Search product by product sku    ${product_sku_list}[3]
    wishlist_ios_keywords.Add wishlist pdp page
    wishlist_ios_keywords.Go to back page
    wishlist_ios_keywords.Verify wishlist is visible
    wishlist_common_page.Click add to bag
    common_page.Click icon cart