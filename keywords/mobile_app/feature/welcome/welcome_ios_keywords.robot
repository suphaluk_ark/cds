*** Keywords ***
Handle initial language, skip tutorial
    # TODO: 20201109 - workaround accept alert on ios 14 and waiting appiun.io fix it
    welcome_ios_page.Click Allow Once On iOS 14
    Run Keyword    welcome_ios_keywords.Retry Select preferred language ${language} and skip button

Retry Select preferred language en and skip button
    Wait Until Keyword Succeeds    3x    1s    welcome_common_page.Select preferred language and skip button    ${dictWelcome}[btn_english]

Retry Select preferred language th and skip button
    Wait Until Keyword Succeeds    3x    1s    welcome_common_page.Select preferred language and skip button    ${dictWelcome}[btn_thai]