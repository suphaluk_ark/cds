*** Keywords ***
Handle initial language, skip tutorial
    Run Keyword    welcome_android_keywords.Retry Select preferred language ${language} and skip button

Retry Select preferred language en and skip button
    Wait Until Keyword Succeeds    3x    1s    welcome_android_page.Select preferred language and skip button    ${dictWelcome}[btn_english]

Retry Select preferred language th and skip button
    CommonMobileKeywords.Click Element    ${dictWelcome}[btn_thai]
    Wait Until Keyword Succeeds    3x    1s    welcome_android_page.Select preferred language and skip button    ${dictWelcome}[btn_thai]