*** Keywords ***
Register with all fields successfully
    home_common_page.Click user img
    user_profile_common_page.Click register button
    register_common_page.Input first name    ${personal_ios_app.register.first_name}
    register_common_page.Input last name    ${personal_ios_app.register.last_name}
    register_common_page.Input email    ${personal_ios_app.register.email}
    register_common_page.Input password    ${personal_ios_app.register.pwd}
    # click lbl register for hide keyboard for ios
    register_common_page.Click lbl register
    register_common_page.Click privacy policy
    register_common_page.Click register button
    register_common_page.Verify that 'Successfully Registered' is visible
    register_common_page.Click continue shopping

Delete registeration account from mdc
    Run Keyword And Ignore Error    common_mobile_app_keywords.Delete registeration account from mdc    ${personal_ios_app.register.email}    ${personal_ios_app.register.pwd}    
    
Open register page
    home_common_page.Click user img
    user_profile_common_page.Click register button
