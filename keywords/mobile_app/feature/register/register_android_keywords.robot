*** Keywords ***
Register with all fields successfully
    home_common_page.Click user img
    user_profile_common_page.Click register button
    register_common_page.Input first name    ${personal_android_app.register.first_name}
    register_common_page.Input last name    ${personal_android_app.register.last_name}
    register_common_page.Input email    ${personal_android_app.register.email}
    register_common_page.Input password    ${personal_android_app.register.pwd}
    register_common_page.Click privacy policy
    register_common_page.Click register button
    register_common_page.Verify that 'Successfully Registered' is visible
    register_common_page.Click continue shopping

Open register page
    home_common_page.Click user img
    user_profile_common_page.Click register button

Delete registeration account from mdc
    Run Keyword And Ignore Error    common_mobile_app_keywords.Delete registeration account from mdc    ${personal_android_app.register.email}    ${personal_android_app.register.pwd}