*** Keywords ***
Redeem full point after login T1C
    CommonMobileKeywords.Click Element        ${dictPaymentPage}[btn_redeem_t1c]
    CommonMobileKeywords.Click Element        ${dictPaymentPage}[ddl_editTextOption_t1c]
    CommonMobileKeywords.Click Element        ${dictPaymentPage}[rdo_full_redeem_t1c]
    CommonMobileKeywords.Click Element        ${dictPaymentPage}[btn_apply_t1c_option]
    payment_android_page.Verify that 'Your The 1 redemption has been applied' should be visible
    Click button pay now

Partially redeem point after login T1C
    [Arguments]    ${thb_discount}    ${number_of_points}
    ${thb_discount}=    Convert To Number    ${thb_discount}
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[lbl_price]    timeout=${GLOBALTIMEOUT}
    ${price_before_redeem}=    AppiumLibrary.Get Text    ${dictPaymentPage}[lbl_price]
    ${price_before_redeem}=    Remove String    ${price_before_redeem}    ฿
    ${price_before_redeem}=    Convert To Number    ${price_before_redeem}
    CommonMobileKeywords.Click Element        ${dictPaymentPage}[btn_redeem_t1c]
    CommonMobileKeywords.Click Element        ${dictPaymentPage}[ddl_editTextOption_t1c]
    CommonMobileKeywords.Click Element        ${dictPaymentPage}[lbl_partially_redeem_t1c]
    payment_android_page.Input points for partial redeeming    ${number_of_points}
    CommonMobileKeywords.Click Element        ${dictPaymentPage}[btn_apply_t1c_option]
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[lbl_price]    timeout=${GLOBALTIMEOUT}
    payment_android_page.Verify that 'Your The 1 redemption has been applied' should be visible
    ${price_after_redeem}=    AppiumLibrary.Get Text    ${dictPaymentPage}[lbl_price]
    ${price_after_redeem}=    CommonKeywords.Convert price to number format    ${price_after_redeem}
    #compare that price_before_redeem-price_after_redeem=Discount
    ${diff_price}=    Evaluate    ${price_before_redeem}-${price_after_redeem}
    ${expected_price_after_discount}=    Evaluate    ${price_before_redeem}-${thb_discount}
    Should Be Equal As Numbers    ${diff_price}    ${thb_discount}    msg=Price is not discounted correct value. Expected discount = ฿${thb_discount}, Expected price after having discount = ฿${expected_price_after_discount}
    [Return]    ${price_after_redeem}

Calculate T1 point to THB
    [Arguments]    ${number_of_points}
    ${number_of_points}=    Convert To Number    ${number_of_points}
    ${thb_discount}=    Evaluate    ${number_of_points}/${T1_rate.normal_rate}
    [Return]   ${thb_discount}