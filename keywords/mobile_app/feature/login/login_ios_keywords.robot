*** Keywords ***
Login successfully
    [Arguments]    ${dictMembership}
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    home_common_page.Click user img
    user_profile_common_page.Click login button
    # login_ios_keywords.Retry login and Verify    ${dictMembership}
    login_common_page.Input text email    ${dictMembership.email}
    common_ios_mobile_app_keywords.Click keyboard done
    login_common_page.Input text password    ${dictMembership.pwd}
    common_ios_mobile_app_keywords.Click keyboard done
    login_common_page.Click lbl email
    login_common_page.Click login button
    user_profile_ios_page.Verify 'firstname and lastname' displays correctly    ${dictMembership.first_name}    ${dictMembership.last_name}
    # click x btn
    user_profile_common_page.Click x button

Retry login and Verify
    [Arguments]    ${dictMembership}
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    BuiltIn.Run Keywords    
    ...    login_common_page.Input text email    ${dictMembership.email}
    ...    AND    login_common_page.Input text password    ${dictMembership.pwd}
    # click lbl email for hide keyboard for ios
    ...    AND    login_common_page.Click lbl email
    ...    AND    login_common_page.Click login button
    ...    AND    user_profile_ios_page.Verify 'firstname and lastname' displays correctly    ${dictMembership.first_name}    ${dictMembership.last_name}

Login successfully with facebook
    [Arguments]    ${dictMembership}
    home_common_page.Click user img
    user_profile_common_page.Click login button
    login_common_page.Click login with facebook button
    login_common_page.Click english language
    login_common_page.Input text email facebook    ${dictMembership.email}
    login_common_page.Input text password facebook    ${dictMembership.pwd}
    login_common_page.Click submit login facebook button
    user_profile_ios_page.Verify 'firstname and lastname' displays correctly    ${dictMembership.first_name}    ${dictMembership.last_name}
    user_profile_common_page.Click x button