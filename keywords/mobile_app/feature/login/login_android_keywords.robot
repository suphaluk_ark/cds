*** Keywords ***
Login successfully
    [Arguments]    ${dictMembership}
    home_common_page.Click user img
    user_profile_common_page.Click login button
    login_common_page.Input text email    ${dictMembership.email}
    login_common_page.Input text password    ${dictMembership.pwd}
    login_common_page.Click login button
    user_profile_android_page.Verify 'firstname and lastname' displays correctly    ${dictMembership.first_name}    ${dictMembership.last_name}
    # click x btn
    user_profile_common_page.Click x button

Login successfully with facebook
    [Arguments]    ${dictMembership}
    home_common_page.Click user img
    user_profile_common_page.Click login button
    login_common_page.Click login with facebook button
    login_common_page.Input text email facebook    ${dictMembership.email}
    login_common_page.Input text password facebook    ${dictMembership.pwd}
    login_common_page.Click submit login facebook button
    user_profile_android_page.Verify 'firstname and lastname' displays correctly    ${dictMembership.first_name}    ${dictMembership.last_name}
    user_profile_common_page.Click x button