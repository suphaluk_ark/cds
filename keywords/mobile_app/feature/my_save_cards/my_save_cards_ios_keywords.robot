*** Keywords ***
Save credit card
    home_common_page.Click user img
    my_save_cards_common_page.Click my credit cards
    my_save_cards_common_page.Input credit card no 2 times    ${payment.credit_card.card_number_without_OTP0}    ${payment.credit_card.card_number_without_OTP1}
    my_save_cards_common_page.Input expired date with slash    ${payment.credit_card.expired_date_with_slash}
    my_save_cards_common_page.Input cvv    ${payment.credit_card.cvv}
    my_save_cards_common_page.Input name customer    ${payment.credit_card.full_name_on_card}
    common_ios_mobile_app_keywords.Click keyboard done
    my_save_cards_common_page.Click save card button

Verify default saved credit card
    my_save_cards_common_page.Verify save credit card should be visible    ${payment.credit_card.hide_card_number}

Credit Card Saved Sucessfully With Setup As Default Payment
    my_save_cards_ios_keywords.Save credit card
    my_save_cards_ios_keywords.Verify default saved credit card
