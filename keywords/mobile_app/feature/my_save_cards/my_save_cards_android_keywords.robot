*** Keywords ***
Click my credit cards
    Wait Until Keyword Succeeds    3 x    1 sec    Run Keywords 
    CommonMobileKeywords.Click Element    ${dictSaveCardPage}[lbl_my_credit_cards]

Save credit card
    home_common_page.Click user img
    my_save_cards_common_page.Click my credit cards
    my_save_cards_common_page.Input credit card no    ${payment.credit_card.card_number_without_OTP}
    my_save_cards_common_page.Input expired date with slash    ${payment.credit_card.expired_date_with_slash}
    my_save_cards_common_page.Input cvv    ${payment.credit_card.cvv}
    my_save_cards_common_page.Input name customer    ${payment.credit_card.full_name_on_card}
    my_save_cards_common_page.Click save card button

Verify default saved credit card
    my_save_cards_common_page.Verify save credit card should be visible    ${payment.credit_card.hide_card_number}

Credit Card Saved Successfully With Setup As Default Payment
    my_save_cards_android_keywords.Save credit card
    my_save_cards_android_keywords.Click my credit cards
    my_save_cards_android_keywords.Verify default saved credit card