*** Keywords ***
Verify customer profile displayed
    home_common_page.Click user img
    user_profile_android_keywords.Go to my profile page    ${personal_android_app.my_profile.first_name}
    user_profile_android_keywords.Verify that first name is visible    ${personal_android_app.my_profile.first_name}
    user_profile_android_keywords.Verify that last name is visible    ${personal_android_app.my_profile.last_name}
    user_profile_common_page.Scroll to save button
    user_profile_android_keywords.Verify that email is visible    ${personal_android_app.my_profile.email}
    user_profile_android_keywords.Verify that phone number is visible    ${personal_android_app.my_profile.phone}
    user_profile_android_keywords.Verify that date of birth is visible    ${personal_android_app.my_profile.date_of_birth}
    user_profile_android_keywords.Verify that gender is male

Go to my profile page
    [Arguments]    ${customer_first_name}
    Wait Until Keyword Succeeds    3 x    1 sec    Run Keywords    user_profile_android_page.Click on customer name    AND    user_profile_android_keywords.Verify that first name is visible    ${customer_first_name}

Verify that first name is visible
    [Arguments]    ${customer_first_name}
    ${locator}    String.Format String    ${dictUserProfilePage}[txt_customer_first_name]    first_name=${customer_first_name}
    AppiumLibrary.Wait Until Element Is Visible    ${locator}
    common_mobile_app_keywords.Element Text Should Be    ${locator}    ${customer_first_name}

Verify that last name is visible
    [Arguments]    ${customer_last_name}
    ${locator}    String.Format String    ${dictUserProfilePage}[txt_customer_last_name]    last_name=${customer_last_name}
    AppiumLibrary.Wait Until Element Is Visible    ${locator}
    common_mobile_app_keywords.Element Text Should Be    ${locator}    ${customer_last_name}

Verify that email is visible
    [Arguments]    ${customer_email}
    ${locator}    String.Format String    ${dictUserProfilePage}[txt_customer_email]    email=${customer_email}
    AppiumLibrary.Wait Until Element Is Visible    ${locator}
    common_mobile_app_keywords.Element Text Should Be    ${locator}    ${customer_email}

Verify that phone number is visible
    [Arguments]    ${customer_phone}
    ${locator}    String.Format String    ${dictUserProfilePage}[txt_customer_phone]    phone=${customer_phone}
    AppiumLibrary.Wait Until Element Is Visible    ${locator}
    common_mobile_app_keywords.Element Text Should Be    ${locator}    ${customer_phone}

Verify that date of birth is visible
    [Arguments]    ${customer_birthday}
    ${locator}    String.Format String    ${dictUserProfilePage}[txt_customer_birthday]    date_of_birth=${customer_birthday}
    AppiumLibrary.Wait Until Element Is Visible    ${locator}
    common_mobile_app_keywords.Element Text Should Be    ${locator}    ${customer_birthday}

Verify that gender is male
    AppiumLibrary.Wait Until Element Is Visible    ${dictUserProfilePage}[txt_customer_gender]    timeout=${GLOBALTIMEOUT}
    common_mobile_app_keywords.Element Text Should Be    ${dictUserProfilePage}[txt_customer_gender]    ${my_account_page_dto.btn_male}