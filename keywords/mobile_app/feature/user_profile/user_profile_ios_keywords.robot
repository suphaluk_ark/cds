*** Keywords ***
Verify customer profile displayed
    home_common_page.Click user img
    user_profile_ios_keywords.Go to my profile page    ${personal_ios_app.my_profile.first_name}    ${personal_ios_app.my_profile.last_name}
    user_profile_ios_keywords.Verify that first name is visible    ${personal_ios_app.my_profile.first_name}
    user_profile_ios_keywords.Verify that last name is visible    ${personal_ios_app.my_profile.last_name}
    user_profile_common_page.Scroll to save button
    user_profile_ios_keywords.Verify that email is visible    ${personal_ios_app.my_profile.email}
    user_profile_ios_keywords.Verify that phone number is visible    ${personal_ios_app.my_profile.phone}
    user_profile_ios_keywords.Verify that date of birth is visible    ${personal_ios_app.my_profile.date_of_birth}
    user_profile_ios_keywords.Verify that gender is male

Go to my profile page
    [Arguments]    ${customer_first_name}    ${customer_last_name}
    Wait Until Keyword Succeeds    3 x    1 sec    Run Keywords    user_profile_ios_page.Click on customer name    ${customer_first_name}    ${customer_last_name}    AND    user_profile_ios_keywords.Verify that first name is visible    ${customer_first_name}

Verify that first name is visible
    [Arguments]    ${customer_first_name}
    AppiumLibrary.Wait Until Element Is Visible    ${dictUserProfilePage}[txt_customer_first_name]
    AppiumLibrary.Element Value Should Be    ${dictUserProfilePage}[txt_customer_first_name]    ${customer_first_name}

Verify that last name is visible
    [Arguments]    ${customer_last_name}
    AppiumLibrary.Wait Until Element Is Visible    ${dictUserProfilePage}[txt_customer_last_name]
    AppiumLibrary.Element Value Should Be    ${dictUserProfilePage}[txt_customer_last_name]    ${customer_last_name}

Verify that email is visible
    [Arguments]    ${customer_email}
    AppiumLibrary.Wait Until Element Is Visible    ${dictUserProfilePage}[txt_customer_email]
    AppiumLibrary.Element Value Should Be    ${dictUserProfilePage}[txt_customer_email]    ${customer_email}

Verify that phone number is visible
    [Arguments]    ${customer_phone}
    AppiumLibrary.Wait Until Element Is Visible    ${dictUserProfilePage}[txt_customer_phone]
    AppiumLibrary.Element Value Should Be    ${dictUserProfilePage}[txt_customer_phone]    ${customer_phone}

Verify that date of birth is visible
    [Arguments]    ${customer_birthday}
    AppiumLibrary.Wait Until Element Is Visible    ${dictUserProfilePage}[txt_customer_birthday]
    AppiumLibrary.Element Value Should Be    ${dictUserProfilePage}[txt_customer_birthday]    ${customer_birthday}

Verify that gender is male
    AppiumLibrary.Wait Until Element Is Visible    ${dictUserProfilePage}[txt_customer_gender]    timeout=${GLOBALTIMEOUT}