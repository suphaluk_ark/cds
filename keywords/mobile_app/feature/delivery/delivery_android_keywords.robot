*** Keywords ***
Guest, delivery successfully by standard pickup, self pickup
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Input first name    ${guest.guest_1.first_name}
    delivery_common_page.Input last name    ${guest.guest_1.last_name}
    delivery_common_page.Input email address    ${guest.guest_1.email}
    delivery_common_page.Input phone no    ${guest.guest_1.phone}
    delivery_common_page.Click Click & Collect
    # self pickup -- BUG * SHOULD BE 'ON' 20200625
    # delivery_common_page.Click self pickup switch
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click continue to payment button

Guest, delivery successfully by shipping to address bkk, standard delivery
    [Arguments]    ${post_code}
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Input first name    ${guest.guest_1.first_name}
    delivery_common_page.Input last name    ${guest.guest_1.last_name}
    delivery_common_page.Input email address    ${guest.guest_1.email}
    delivery_common_page.Input phone no    ${guest.guest_1.phone}
    delivery_common_page.Click Home Delivery
    delivery_common_page.Input place building    ${guest.guest_1.place_buidling}
    delivery_common_page.Input house no street    ${guest.guest_1.house_no_street}
    delivery_common_page.Input postcode    ${post_code}
    delivery_android_page.Select bkk district by 1st index
    delivery_android_page.Select bkk subdistrict by 1st index
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Click continue to payment button

Guest, delivery successfully by shipping to address outside bkk, standard delivery
    [Arguments]    ${post_code}  
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Input first name    ${guest.guest_1.first_name}
    delivery_common_page.Input last name    ${guest.guest_1.last_name}
    delivery_common_page.Input email address    ${guest.guest_1.email}
    delivery_common_page.Input phone no    ${guest.guest_1.phone}
    delivery_common_page.Click Home Delivery
    delivery_common_page.Input place building    ${guest.guest_1.place_buidling}
    delivery_common_page.Input house no street    ${guest.guest_1.house_no_street}
    delivery_common_page.Input postcode    ${post_code}
    delivery_android_page.Select outside bkk district by 1st index
    delivery_android_page.Select outside bkk subdistrict by 1st index
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Click continue to payment button

Guest, delivery sucessfully by 1hr and standard delivery
    [Arguments]    ${post_code}  
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Input first name    ${guest.guest_1.first_name}
    delivery_common_page.Input last name    ${guest.guest_1.last_name}
    delivery_common_page.Input email address    ${guest.guest_1.email}
    delivery_common_page.Input phone no    ${guest.guest_1.phone}
    delivery_common_page.Click Click & Collect
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Input place building    ${guest.guest_1.place_buidling}
    delivery_common_page.Input house no street    ${guest.guest_1.house_no_street}
    delivery_common_page.Input postcode    ${post_code}
    delivery_android_page.Select bkk district by 1st index
    delivery_android_page.Select bkk subdistrict by 1st index
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by shipping to address with the default address, standard delivery
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Click Home Delivery
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by standard pickup, self pickup
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Click Click & Collect
    # delivery_common_page.Click self pickup switch
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click continue to payment button

Membership delivery successfully 1hr and standard delivery
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Click Click & Collect
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Click continue to payment button

Membership delivery successfully by 1hr pickup
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Click Click & Collect
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click continue to payment button

Membership delivery successfully by 1hr pickup and standard pickup
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Click Click & Collect
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by shipping to address with the default address, next day delivery
    common_android_mobile_app_keywords.Wait until the loading icon disappears
    delivery_common_page.Click Home Delivery
    delivery_common_page.Select Next Day Delivery
    delivery_common_page.Click continue to payment button