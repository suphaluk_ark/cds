*** Setting ***
# main resource
Resource          ${CURDIR}/../../resources/imports.robot

# common
Resource          ${CURDIR}/../common-keywords/CommonKeywords.robot
Resource          ${CURDIR}/../common-keywords/CommonMobileKeywords.robot
Resource          ${CURDIR}/../common-keywords/pCloudy.robot
Resource          ${CURDIR}/common/common_mobile_app_keywords.robot
Resource          ${CURDIR}/common/common_${platform}_mobile_app_keywords.robot
Resource          ${CURDIR}/../common/promotion_calculator.robot
Resource          ${CURDIR}/../falcon/api/plp/app_search_falcon.robot
Resource          ${CURDIR}/../falcon/api/shopping_cart_page/app_shoppinp_cart_falcon.robot
Resource          ${CURDIR}/../falcon/api/payment_page/app_payment_page_falcon.robot
Resource          ${CURDIR}/../common-keywords/api/kibana/sms_details.robot
Resource          ${CURDIR}/../../keywords/api/cart_api.robot
Resource          ${CURDIR}/../../keywords/api/customer_api.robot
Resource          ${CURDIR}/../../keywords/api/order_status_api.robot
Resource          ${CURDIR}/../../keywords/api/kibana_api.robot
Library           ${CURDIR}/../common-keywords/GetAMXkey.py

# mdc
Resource          ${CURDIR}/../magento/imports.robot

# mcom
Resource          ${CURDIR}/../magento/api/mcom/search_sales_order.robot

# fms 
Resource          ${CURDIR}/../common-keywords/fms/fms_db.robot

# page
Resource    ${CURDIR}/page/checkout_completed/checkout_completed_common_page.robot
Resource    ${CURDIR}/page/checkout_completed/checkout_completed_${platform}_page.robot
Resource    ${CURDIR}/page/delivery/delivery_common_page.robot
Resource    ${CURDIR}/page/delivery/delivery_${platform}_page.robot
Resource    ${CURDIR}/page/home/home_common_page.robot
Resource    ${CURDIR}/page/home/home_${platform}_page.robot
Resource    ${CURDIR}/page/login/login_common_page.robot
Resource    ${CURDIR}/page/login/login_${platform}_page.robot
Resource    ${CURDIR}/page/payment/payment_common_page.robot
Resource    ${CURDIR}/page/payment/payment_${platform}_page.robot
Resource    ${CURDIR}/page/product_pdp/product_pdp_common_page.robot
Resource    ${CURDIR}/page/product_pdp/product_pdp_${platform}_page.robot
Resource    ${CURDIR}/page/register/register_common_page.robot
Resource    ${CURDIR}/page/register/register_${platform}_page.robot
Resource    ${CURDIR}/page/shopping_cart/shopping_cart_common_page.robot
Resource    ${CURDIR}/page/shopping_cart/shopping_cart_${platform}_page.robot
Resource    ${CURDIR}/page/user_profile/user_profile_common_page.robot
Resource    ${CURDIR}/page/user_profile/user_profile_${platform}_page.robot
Resource    ${CURDIR}/page/welcome/welcome_common_page.robot
Resource    ${CURDIR}/page/welcome/welcome_${platform}_page.robot
Resource    ${CURDIR}/page/change_password/change_password_common_page.robot
Resource    ${CURDIR}/page/change_password/change_password_${platform}_page.robot
Resource    ${CURDIR}/page/logout/logout_common_page.robot
Resource    ${CURDIR}/page/logout/logout_${platform}_page.robot
Resource    ${CURDIR}/page/product_plp/product_plp_common_page.robot
Resource    ${CURDIR}/page/product_plp/product_plp_${platform}_page.robot
Resource    ${CURDIR}/page/wishlist/wishlist_common_page.robot
Resource    ${CURDIR}/page/wishlist/wishlist_${platform}_page.robot
Resource    ${CURDIR}/page/my_save_cards/my_save_cards_common_page.robot
Resource    ${CURDIR}/page/my_save_cards/my_save_cards_${platform}_page.robot
Resource    ${CURDIR}/page/common_page/common_page.robot
Resource    ${CURDIR}/page/common_page/common_${platform}_page.robot
Resource    ${CURDIR}/page/search_product/search_product_common_page.robot
Resource    ${CURDIR}/page/search_product/search_product_${platform}_page.robot

# feature
Resource    ${CURDIR}/feature/checkout/checkout_${platform}_keywords.robot
Resource    ${CURDIR}/feature/delivery/delivery_${platform}_keywords.robot
Resource    ${CURDIR}/feature/login/login_${platform}_keywords.robot
Resource    ${CURDIR}/feature/payment/payment_${platform}_keywords.robot
Resource    ${CURDIR}/feature/register/register_${platform}_keywords.robot
Resource    ${CURDIR}/feature/welcome/welcome_${platform}_keywords.robot
Resource    ${CURDIR}/feature/logout/logout_${platform}_keywords.robot
Resource    ${CURDIR}/feature/user_profile/user_profile_${platform}_keywords.robot
Resource    ${CURDIR}/feature/change_password/change_password_${platform}_keywords.robot
Resource    ${CURDIR}/feature/wishlist/wishlist_${platform}_keywords.robot
Resource    ${CURDIR}/feature/my_save_cards/my_save_cards_${platform}_keywords.robot
Resource    ${CURDIR}/feature/T1/T1_${platform}_keywords.robot
Resource    ${CURDIR}/feature/consent/consent_${platform}_keywords.robot
Resource    ${CURDIR}/feature/promotion/promotion_${platform}_keywords.robot
Resource    ${CURDIR}/feature/search_filter/search_filter_${platform}_keywords.robot

# variables
Variables    ${CURDIR}/mobile_devices.yaml
Variables    ${CURDIR}/../../resources/testdata/cds/cds_translation/translation_${language}_app.yaml    
Variables    ${CURDIR}/../../resources/configs/cds/staging/common_variables_app.yaml

Variables    ${CURDIR}/../../resources/testdata/cds/staging/test_data_app.yaml
Variables    ${CURDIR}/../../resources/testdata/cds/staging/product_data.yaml

Variables    ${CURDIR}/../../resources/configs/cds/staging/env_config.yaml