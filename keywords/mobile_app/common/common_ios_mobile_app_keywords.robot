*** Variables ***
&{dictVirtualKeyboardPage}
...    btn_key=chain=**/XCUIElementTypeButton[`label == "{$key}"`]
...    btn_keyboard_done=chain=**/XCUIElementTypeButton[`name == 'Done'`]
...    btn_keyboard_search=chain=**/XCUIElementTypeKeyboard/**/XCUIElementTypeButton[-1]

&{dictCommonPage}
...    icon_back_page=chain=**/XCUIElementTypeButton[`name == 'ic back'`]

*** Keywords ***
ios Setup CDS Application by Membership 
    [Arguments]    ${dictMembership}
    common_mobile_app_keywords.Test Setup - Remove all product in shopping cart    ${dictMembership.email}    ${dictMembership.pwd}
    common_mobile_app_keywords.Launch CDS Application
    welcome_ios_keywords.Handle initial language, skip tutorial
    login_ios_keywords.Login successfully    ${dictMembership}

ios Setup CDS Application by Guest 
    common_mobile_app_keywords.Launch CDS Application
    welcome_ios_keywords.Handle initial language, skip tutorial

Click icon back page
    CommonMobileKeywords.Click Element  ${dictCommonPage}[icon_back_page]    timeout=${GLOBALTIMEOUT}

Click a key on keyboard
    [Documentation]  For iOS only
    ...     Input only one key in keyboard such as a, b, c, 1, 2, 3 [,], @, $
    [Arguments]    ${key}
    ${key_locator}=    CommonKeywords.Format Text    ${dictVirtualKeyboardPage}[btn_key]    $key=${key}
    CommonMobileKeywords.Click Element    ${key_locator}

Click keyboard done
    CommonMobileKeywords.Click Element   ${dictVirtualKeyboardPage}[btn_keyboard_done]

Click Search key on the virtual keyboard
    CommonMobileKeywords.Click Element   ${dictVirtualKeyboardPage}[btn_keyboard_search]

Click Delete key on the virtual keyboard
    common_ios_mobile_app_keywords.Click a key on keyboard    delete

Click Space key on the virtual keyboard
    common_ios_mobile_app_keywords.Click a key on keyboard    space

Click ABC key on the virtual keyboard
    common_ios_mobile_app_keywords.Click a key on keyboard    letters

Click 123 key on the virtual keyboard
    common_ios_mobile_app_keywords.Click a key on keyboard    shift

Click Next key on the virtual keyboard
    common_ios_mobile_app_keywords.Click a key on keyboard    next
