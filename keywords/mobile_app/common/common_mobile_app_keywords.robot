*** Variables ***
${mdc_access_token}    %{MDC_ACCESS_TOKEN}
${key_mdc}    %{MDC_ACCESS_TOKEN}

*** Keywords ***
Element Text Should Be
    [Arguments]    ${locator}    ${expected_text}
    AppiumLibrary.Wait Until Element Is Visible    ${locator}    ${GLOBALTIMEOUT}
    ${actual_text}    AppiumLibrary.Get Text    ${locator}
    Should Be Equal As Strings    ${expected_text}    ${actual_text}

Open CDS Application
    [Arguments]    &{additional_caps}
    BuiltIn.Run Keyword If    ${${mobileDevice}}[is_pcloudy]    CommonMobileKeywords.Prepare devices, app and Setup Open application with pCloudy    &{additional_caps}
    ...    ELSE    CommonMobileKeywords.Open application on local    &{additional_caps}

Teardown CDS Application
    CommonMobileKeywords.Test Teardown
    Quit Application

Test Setup - Remove all product in shopping cart
    [Arguments]    ${email}    ${pwd}
    Run Keyword And Ignore Error     customer_details.Remove customer cart and generate new cart    ${email}    ${pwd}
    common_mobile_app_keywords.Set api root URL for CDS

Set api root URL for CDS
    Set Global Variable    ${api_root}    ${original_api_root}

Get api root URL for CDS
    Set Global Variable    ${original_api_root}    ${api_root}
    Set Global Variable    ${api_root}    ${api_root_copy}

Retry verify that order status from MCOM should be 'LOGISTIC' status
    [Arguments]    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec    common_mobile_app_keywords.Verify that order status from MCOM should be 'LOGISTIC' status    ${order_number}

Retry verify that order status reason from MCOM should be 'Ready to ship' 
    [Arguments]    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec    common_mobile_app_keywords.Verify that order status reason from MCOM should be 'Ready to ship'    ${order_number}

Retry verify that order status from MDC should be 'LOGISTIC' status
    [Arguments]    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec    common_mobile_app_keywords.Verify that order status from MDC should be 'LOGISTIC' status    ${order_number}

Retry verify that order status reason from MDC should be 'PENDING_FIRST_SHIPMENT_REQUEST'
    [Arguments]    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec    common_mobile_app_keywords.Verify that order status reason from MDC should be 'PENDING_FIRST_SHIPMENT_REQUEST'    ${order_number} 

Retry verify that order number exist in FMS DB     
    [Arguments]    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec    common_mobile_app_keywords.Verify that order number exist in FMS DB       ${order_number}

Retry verify that order status reason from MDC should be 'PENDING_VALIDATE_PAYMENT_REPLY' status
    [Arguments]    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec    common_mobile_app_keywords.Verify that order status reason from MDC should be 'PENDING_VALIDATE_PAYMENT_REPLY' status       ${order_number}

Retry verify that order status from MDC should be 'awaiting payment'
    [Arguments]    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec    common_mobile_app_keywords.Verify that order status from MDC should be 'awaiting payment'      ${order_number}

Verify that order status from MDC should be 'awaiting payment'
    [Arguments]    ${order_number}
    ${status}=    Get order status from order detail response    ${order_number}
    Log To Console    ${status}
    Should Be Equal As Strings    ${status}    awaiting_payment

Get order status from order detail response
    [Arguments]    ${increment_id}
    ${response}=    order_details.Get order details by increment_id    ${increment_id}
    ${status}=    REST.Output    ${path_root}extension_attributes.order_status
    [Return]    ${status}

Verify that order status from MCOM should be 'LOGISTIC' status
    [Arguments]    ${order_number}
    ${status}=    Get order status from MCOM    ${order_number}
    Log To Console    ${status}
    Should Be Equal As Strings    ${status}    LOGISTICS

Verify that order status reason from MCOM should be 'Ready to ship'
    [Arguments]    ${order_number}
    ${status_reason}=    Get order status reason from MCOM    ${order_number}
     Log To Console    ${status_reason}
    Should Be Equal As Strings    ${status_reason}    READYTOSHIP

Verify that order status from MDC should be 'LOGISTIC' status
    [Arguments]    ${order_number}
    ${status}=    order_status_api.Get status from order detail response    ${order_number}
    Log To Console    ${status}
    Should Be Equal As Strings    ${status}    MCOM_LOGISTICS

Verify that order status reason from MDC should be 'PENDING_FIRST_SHIPMENT_REQUEST'
    [Arguments]    ${order_number}
    ${status_reason}=    order_status_api.Get status reason from order detail response    ${order_number}
    Log To Console    ${status_reason}
    Should Be True    '${status_reason}'=='PENDING_FIRST_SHIPMENT_REQUEST' or '${status_reason}'=='READYTOSHIP'

Verify that order number exist in FMS DB
    [Arguments]    ${order_number}
    fms_db.Verify that order number exist in FMS DB    ${order_number}

Verify that order status reason from MDC should be 'PENDING_VALIDATE_PAYMENT_REPLY' status
    [Arguments]    ${increment_id}
    ${status_reason}=    order_status_api.Get status reason from order detail response    ${increment_id}
    Log To Console    ${status_reason}
    Should Be Equal As Strings    ${status_reason}    PENDING_VALIDATE_PAYMENT_REPLY

Delete registeration account from mdc
    [Arguments]    ${register_email}    ${regresstion_password}
    ${customer_ID}=    customer_details.Get customer ID    ${register_email}    ${regresstion_password}
    customer_details.Delete customer by customer ID    ${customer_ID}

Close CDS Application
    Run Keyword And Ignore Error    AppiumLibrary.Close Application
    
Element Text Should Contains
    [Arguments]    ${locator}    ${expected_text}
    AppiumLibrary.Wait Until Page Contains Element    ${locator}    ${GLOBALTIMEOUT}
    ${actual_text}    AppiumLibrary.Get Text    ${locator}
    Should Be Equal As Strings    ${expected_text}    ${actual_text}

Launch CDS Application
    AppiumLibrary.Launch Application

Quit Application
    Run Keyword And Ignore Error    AppiumLibrary.Quit Application

Prepare additional caps
    ${additional_caps}=    Create Dictionary
    Run Keyword And Ignore Error    Set To Dictionary    ${additional_caps}    pCloudy_ApplicationName=${appVersion}
    [Return]    ${additional_caps}
    
Open CDS Application with additional caps
    ${additional_caps}=    Prepare additional caps
    Open CDS Application    &{additional_caps}

Get consent info from check consent privacy status info response
    [Arguments]    ${customer_email}    ${customer_password}
    ${customer_id}    customer_details.Get customer ID    ${customer_email}    ${customer_password}
    ${response}=    consent.Check Consent Information    ${customer_email}    ${customer_id}    CDS    ${EMPTY}
    ${consent_privacy_status}=    Output    $.consent_privacy_status
    [Return]    ${consent_privacy_status}

Get consent info from check consent marketing status info response
    [Arguments]    ${customer_email}    ${customer_password}
    ${customer_id}    customer_details.Get customer ID    ${customer_email}    ${customer_password}
    ${response}=    consent.Check Consent Information    ${customer_email}    ${customer_id}    CDS    ${EMPTY}
    ${consent_marketing_status}=    Output    $.consent_marketing_status
    [Return]    ${consent_marketing_status}

Remove wishlist User Profile
    [Arguments]    ${dictMembership}
    ${response}=    wishlist.Remove all wishlist items in wishlist group   ${dictMembership.email}    ${dictMembership.pwd}    ${EMPTY}

Verify order is created successfully in WMS via ESB
    [Arguments]    ${order_id}
    Wait Until Keyword Succeeds    30 x    10 sec    kibana_api.Verify that order create on WMS successfully by destination system    ${order_id}

Verify order is created successfully in Pickingtool via ESB
    [Arguments]    ${order_id}
    Wait Until Keyword Succeeds    30 x    10 sec    kibana_api.Verify that order create on pickingtool successfully by destination system    ${order_id}

Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM
    [Arguments]    ${order_number}
    order_status_api.Retry until MCOM order status should be 'LOGISTICS'    ${order_number}
    order_status_api.Retry until MCOM order status reason should be 'READYTOSHIP'    ${order_number}

Verify order status should be "Processing/ READYTOSHIP" on MDC
    [Arguments]    ${order_number}
    order_status_api.Retry until MDC order status should be 'Processing'    ${order_number}
    order_status_api.Retry until MDC status reason should be 'READYTOSHIP'    ${order_number}
