*** Keywords ***
Input registered email
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible  ${forgetAccountPage}[txt_username]
    AppiumLibrary.Input Text     ${forgetAccountPage}[txt_username]     ${value}

Click submit button
    CommonMobileKeywords.Click Element  ${forgetAccountPage}[btn_submit]

Verify message should be displayed
    [Arguments]    ${message}
    ${message_locator}=    CommonKeywords.Format Text    ${forgetAccountPage}[msg_forgot]     $message=${message}
    CommonMobileKeywords.Verify Elements Are Visible  ${message_locator}

Verify not registered email error message should be displayed
    forget_account_page.Verify message should be displayed  ${forgot_account_page.msg_not_has_been_registered}

Verify sent reset password link message should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${forgetAccountPage}[msg_sent_sucessfully]

Verify resend it button should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${forgetAccountPage}[btn_resend_it]

Click resend it button
    CommonMobileKeywords.Click Element  ${forgetAccountPage}[btn_resend_it]

Click go to login button
    CommonMobileKeywords.Click Element  ${forgetAccountPage}[btn_go_to_login]