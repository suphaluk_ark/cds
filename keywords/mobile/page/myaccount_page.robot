*** Keywords ***
Click on login menu
    CommonMobileKeywords.Click Element  ${accountPage}[btn_login]

Customer firstname and lastname should be displayed correctly
    [Arguments]     ${cust_name}
    ${cust_name_locator}=    CommonKeywords.Format Text    ${accountPage}[txt_display_name]     $cust_name=${cust_name}
    CommonMobileKeywords.Verify Elements Are Visible    ${cust_name_locator}

Click logout button
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${accountPage}[view_scroll]  ${accountPage}[btn_logout]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}
    CommonMobileKeywords.Click Element  ${accountPage}[btn_logout]
