*** Keywords ***
Input username
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible  ${loginPage}[txt_username]
    AppiumLibrary.Input Text     ${loginPage}[txt_username]     ${value}

Input password
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible  ${loginPage}[txt_password]
    AppiumLibrary.Input Text     ${loginPage}[txt_password]     ${value}

Click login button
    CommonMobileKeywords.Click Element  ${loginPage}[btn_login]

Click login with Facebook
    CommonMobileKeywords.Click Element      ${loginPage}[btn_login_with_facebook]

Click on show password icon
    CommonMobileKeywords.Click Element      ${loginPage}[ico_show_password]

Click on forgot password button
    CommonMobileKeywords.Click Element  ${loginPage}[btn_forgot_password]

Click register button
    CommonMobileKeywords.Click Element  ${loginPage}[btn_register]

Click register now
    CommonMobileKeywords.Click Element  ${loginPage}[lbl_register_now]

Verify error message should be displayed
    [Arguments]    ${message}
    ${error_msg_locator}=    CommonKeywords.Format Text    ${loginPage}[msg_error]     $message=${message}
    CommonMobileKeywords.Verify Elements Are Visible  ${error_msg_locator}

Verify incorrect account message should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${loginPage}[msg_incorrect_account]

Verify login button should be disabled
    ${enabled_att}=    CommonMobileKeywords.Get Element Attribute  ${loginPage}[btn_login]  enabled
    Should Be Equal  ${enabled_att}  false

Verify show password should be checked
    ${show_password}=    CommonMobileKeywords.Get Element Attribute  ${loginPage}[ico_show_password]  checked
    Should Be Equal  ${show_password}  true

Verify password should be visible
    ${password}=    CommonMobileKeywords.Get Element Attribute  ${loginPage}[txt_password]  password
    Should Be Equal  ${password}  false
