*** Keywords ***
Scroll down and click product on search page
    [Arguments]  ${product_name}
    ${product_locator}=    CommonKeywords.Format Text    ${searchProduct}[lbl_product_name]     $product_name=${product_name}
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${searchProduct}[view_search_product_scroll]  ${product_locator}  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}
    CommonMobileKeywords.Click Element  ${product_locator}