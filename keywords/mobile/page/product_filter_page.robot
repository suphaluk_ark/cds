*** Keywords ***
Click on a brand 
    [Arguments]  ${brand}
    ${brand_locator}=    CommonKeywords.Format Text    ${filterPage}[lbl_brand]     $brand=${brand}
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${filterPage}[view_brand_scroll]  ${brand_locator}  60  ${global_scale_min}  ${global_scale_max}
    CommonMobileKeywords.Click Element  ${brand_locator}

Click on filter button
    CommonMobileKeywords.Click Element    ${filterPage}[btn_filter]

Click on filter by category
    CommonMobileKeywords.Click Element    ${filterPage}[btn_filter_by_category]

Click on filter by price range
    CommonMobileKeywords.Click Element    ${filterPage}[btn_filter_price_range]

Click on apply filters button
    CommonMobileKeywords.Click Element    ${filterPage}[btn_apply]

Click on cancel button
    CommonMobileKeywords.Click Element    ${filterPage}[btn_cancel]

Input text for Filter by category
    [Arguments]    ${keyword}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element   ${filterPage}[input_search]    ${keyword}

Select a category on the list
    [Arguments]    ${category}
    ${category_locator}=    CommonKeywords.Format Text    ${filterPage}[lbl_category]     $category=${category}
    CommonMobileKeywords.Click Element    ${category_locator}

Click on reset button
    CommonMobileKeywords.Click Element    ${filterPage}[btn_reset]

Click search close button
    CommonMobileKeywords.Click Element    ${filterPage}[btn_search_close]

Click on back button
    CommonMobileKeywords.Click Element    ${filterPage}[btn_back]

Click on color button
    CommonMobileKeywords.Click Element    ${filterPage}[btn_color]

Verify category should be displayed after search
    [Arguments]    ${category}
    ${category_locator}=    CommonKeywords.Format Text    ${filterPage}[lbl_category]     $category=${category}
    CommonMobileKeywords.Verify Elements Are Visible  ${category_locator}

Get total of item
    ${text}=  AppiumLibrary.Get Text   ${filterPage}[lbl_item_count]
    ${total}=  Get regexp matches  ${text}   \\d+
    [Return]  ${total}[0]

Verify color and quantity should be displayed correctly
    [Arguments]  ${color}  ${quantity}
    ${color_locator}=    CommonKeywords.Format Text    ${filterPage}[txt_item_color]     $color=${color}
    ${quantity_locator}=    CommonKeywords.Format Text    ${filterPage}[txt_item_quantity]     $quantity=${quantity}
    CommonMobileKeywords.Verify Elements Are Visible  ${color_locator}  ${quantity_locator}

Click a color on the list
    [Arguments]  ${color}
    ${color_locator}=    CommonKeywords.Format Text    ${filterPage}[txt_item_color]     $color=${color}
    CommonMobileKeywords.Click Element    ${color_locator}

Verify color stack should be displayed correctly
    [Arguments]  ${color_stack}
    AppiumLibrary.Element Text Should Be  ${filterPage}[txt_color_stack]  ${color_stack}

Verify the minimum price should be displayed on the product list
    [Arguments]    ${product_name}  ${price}
    ${name_locator}=    CommonKeywords.Format Text    ${filterPage}[lbl_product_name]     $name=${product_name}
    ${price_locator}=    CommonKeywords.Format Text    ${filterPage}[lbl_product_price]     $price=${price}
    CommonMobileKeywords.Verify Elements Are Visible  ${name_locator}  ${price_locator}

Verify the maximum price should be displayed on the product list
    [Arguments]    ${product_name}  ${price}
    ${name_locator}=    CommonKeywords.Format Text    ${filterPage}[lbl_product_name]     $name=${product_name}
    ${price_locator}=    CommonKeywords.Format Text    ${filterPage}[lbl_product_price]     $price=${price}
    CommonMobileKeywords.Verify Elements Are Visible  ${name_locator}  ${price_locator}

Verify the minimum price should be displayed on price range bar
    [Arguments]    ${price}
    ${price_locator}=    CommonKeywords.Format Text    ${filterPage}[lbl_min_price]     $price=${price}
    CommonMobileKeywords.Verify Elements Are Visible    ${price_locator}

Verify the maximum price should be displayed on price range bar
    [Arguments]    ${price}
    ${price_locator}=    CommonKeywords.Format Text    ${filterPage}[lbl_max_price]     $price=${price}
    CommonMobileKeywords.Verify Elements Are Visible    ${price_locator}