*** Keywords ***
Verify Category button should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${mainCategoryPage}[btn_category]

Verify Brands button should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${mainCategoryPage}[btn_brand]

Click Category button
    CommonMobileKeywords.Click Element  ${mainCategoryPage}[btn_category]  timeout=${GLOBAL_TIMEOUT}

Click Brands button
    CommonMobileKeywords.Click Element  ${mainCategoryPage}[btn_brand]

Verify New Arrivals should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${mainCategoryPage}[view_category_scroll]  ${mainCategoryPage}[lbl_new_arrivals]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}

Verify Central Inspirer should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${mainCategoryPage}[view_category_scroll]  ${mainCategoryPage}[lbl_central_inspirer]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}

Verify Best Deal should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${mainCategoryPage}[view_category_scroll]  ${mainCategoryPage}[lbl_best_deal]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}

Verify Gift with purchase section should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${mainCategoryPage}[view_category_scroll]  ${mainCategoryPage}[lbl_gift_with_purchase]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}

Verify brand name list should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${mainCategoryPage}[lbl_brand_name_list]

Verify a gift should be displayed in gift with purchase section
    [Arguments]  ${gift_name}
    ${gift_locator}=    CommonKeywords.Format Text    ${mainCategoryPage}[lbl_gift]     $gift_name=${gift_name}
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${mainCategoryPage}[view_category_scroll]  ${gift_locator}  3  ${global_scale_min}  ${global_scale_max}
    CommonMobileKeywords.Verify Elements Are Visible  ${gift_locator}

Search a brand
    [Arguments]  ${brand_name}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element  ${mainCategoryPage}[tbx_search_brand]  ${brand_name}

Verify search close button should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${mainCategoryPage}[tbx_search_close]

Verify View by brand should be not displayed
    AppiumLibrary.Element Should Be Disabled    ${mainCategoryPage}[lbl_view_by_brand]

Click back button
    CommonMobileKeywords.Click Element  ${mainCategoryPage}[btn_back]

Click New Arrivals at Central Pick section
   CommonMobileKeywords.Click Element  ${mainCategoryPage}[btn_central_pick_new_arrival]

Click Online Exclusive at Central Pick section
   CommonMobileKeywords.Click Element  ${mainCategoryPage}[btn_central_pick_online_exclusive]

Verify New Arrivals page should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${mainCategoryPage}[title_new_arrival]

Verify Online Exclusive page should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${mainCategoryPage}[title_online_exclusive]

Click on a category 
    [Arguments]  ${category}
    ${category_locator}=    CommonKeywords.Format Text    ${mainCategoryPage}[btn_specific_category]     $category=${category}
    CommonMobileKeywords.Click Element  ${category_locator}

Verify sub-category should be displayed
    [Arguments]  ${sub_category}
    ${sub_category_locator}=    CommonKeywords.Format Text    ${mainCategoryPage}[btn_sub_category]     $sub_category=${sub_category}
    CommonMobileKeywords.Verify Elements Are Visible  ${sub_category_locator}

Click on a card under Central Inspirer section
    [Arguments]  ${index}
    ${card_locator}=    CommonKeywords.Format Text    ${mainCategoryPage}[img_central_inspirer_card]     $index=${index}
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${mainCategoryPage}[view_category_scroll]  ${mainCategoryPage}[img_central_inspirer_card]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}
    CommonMobileKeywords.Click Element  ${card_locator}

Verify URL of the page should be correct
    CommonMobileKeywords.Verify Elements Are Visible  ${mainCategoryPage}[url_centralinspirer]
    AppiumLibrary.Element Text Should Be    ${mainCategoryPage}[url_centralinspirer]    ${central_inspirer_url}

Verify banner page should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${mainCategoryPage}[img_hero_banner_indicator]

Verify banner indicator should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${mainCategoryPage}[view_pager_banner]

