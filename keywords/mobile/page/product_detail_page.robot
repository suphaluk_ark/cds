*** Keywords ***
Verify product id should be displayed
    [Arguments]  ${product_id}
    ${product_id_locator}=    CommonKeywords.Format Text    ${productDetailPage}[lbl_product_id]     $product_id=${product_id}
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${product_id_locator}  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}

Verify sold by label should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${productDetailPage}[lbl_sold_by]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}

Verify product seller should be displayed
    [Arguments]  ${product_seller}
    ${product_seller_locator}=    CommonKeywords.Format Text    ${productDetailPage}[lbl_product_seller]     $product_seller=${product_seller}
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${product_seller_locator}  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}

Verify the 1 should be displayed
    [Arguments]  ${the_1_text}
    ${the_1_locator}=    CommonKeywords.Format Text    ${productDetailPage}[lbl_the_1]     $the_1_text=${the_1_text}
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${the_1_locator}  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}

Verify click and collect should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${productDetailPage}[lbl_click_collect]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}


Verify gift wrapping should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${productDetailPage}[lbl_gift_wrapping]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}


Verify pay on delivery should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${productDetailPage}[lbl_pay_on_delivery]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}


Verify express delivery should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${productDetailPage}[lbl_express_delivery]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}


Verify return exchange should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${productDetailPage}[lbl_return_exchange]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}


Verify pay by installment should be displayed
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${productDetailPage}[view_scroll]  ${productDetailPage}[lbl_pay_by_installment]  ${global_scroll_times}  ${global_scale_min}  ${global_scale_max}
