*** Keywords ***
Input username
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible  ${registerPage}[txt_username]
    AppiumLibrary.Input Text     ${registerPage}[txt_username]     ${value}

Verify username should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${registerPage}[txt_username]

Input password
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible  ${registerPage}[txt_password]
    AppiumLibrary.Input Text     ${registerPage}[txt_password]     ${value}

Verify password should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${registerPage}[txt_password]

Input first name
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible  ${registerPage}[txt_first_name]
    AppiumLibrary.Input Text     ${registerPage}[txt_first_name]     ${value}

Verify first name should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${registerPage}[txt_first_name]

Input last name
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible    ${registerPage}[txt_last_name]
    AppiumLibrary.Input Text     ${registerPage}[txt_last_name]     ${value}

Verify last name should be displayed
    CommonMobileKeywords.Verify Elements Are Visible  ${registerPage}[txt_last_name]

Click register button
    CommonMobileKeywords.Click Element  ${registerPage}[btn_register]

Click change language button
    CommonMobileKeywords.Click Element  ${registerPage}[btn_language]

Click login section
    CommonMobileKeywords.Click Element  ${registerPage}[lbl_login_section]

Click continue shopping button
    CommonMobileKeywords.Click Element  ${registerPage}[btn_continue_shopping]

Verify register succesfully message should be displayed
    [Arguments]    ${message}
    ${register_sucessfully_locator}=    CommonKeywords.Format Text    ${registerPage}[msg_register_sucessfully]     $message=${message}
    CommonMobileKeywords.Verify Elements Are Visible  ${register_sucessfully_locator}


