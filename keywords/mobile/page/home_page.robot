*** Keywords ***
Click on view profile icon
    CommonMobileKeywords.Click Element  ${homePage}[btn_viewProfile]

Click on brand icon
    CommonMobileKeywords.Click Element  ${homePage}[ico_brand]  timeout=${GLOBAL_TIMEOUT}

Click on kid brand
    CommonMobileKeywords.Click Element  ${homePage}[ico_kids]  timeout=${GLOBAL_TIMEOUT}

Click on tech brand
    CommonMobileKeywords.Click Element  ${homePage}[ico_tech]  timeout=${GLOBAL_TIMEOUT}

Click on sports brand
    CommonMobileKeywords.Click Element  ${homePage}[ico_sports]  timeout=${GLOBAL_TIMEOUT}

Click on super sports brand
    CommonMobileKeywords.Click Element  ${homePage}[ico_supersports]  timeout=${GLOBAL_TIMEOUT}

Click on beauty brand
    CommonMobileKeywords.Click Element  ${homePage}[ico_beauty]  timeout=${GLOBAL_TIMEOUT}

Input product name into search box
    [Arguments]  ${product_name}
    CommonMobileKeywords.Click Element  ${homePage}[input_search]  timeout=${GLOBAL_TIMEOUT}
    CommonMobileKeywords.Verify Elements Are Visible    ${homePage}[tbx_search]
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element  ${homePage}[tbx_search]  ${product_name}





