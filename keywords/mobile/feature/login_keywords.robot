*** Keywords ***
Login to CDS with personal account
    [Arguments]     ${username}     ${password}     ${fname_lname}
    home_page.Click on view profile icon
    myaccount_page.Click on login menu
    login_page.Input username      ${username}
    login_page.Input password      ${password}
    login_page.Click login button
    myaccount_page.Customer firstname and lastname should be displayed correctly    ${fname_lname}
