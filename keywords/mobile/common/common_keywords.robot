*** Settings ***
Resource    ${CURDIR}/../mobile_imports.robot

*** Keywords ***
Setup Open application
    [Arguments]     ${no_reset}=${True}
    CommonMobileKeywords.Open Application On    ${${mobileDevice}}[platform]
    ...     ${${mobileDevice}}[deviceName]
    ...     ${${mobileDevice}}[appLocation]
    ...     no_reset=${no_reset}
    ...     skipServerInstallation=${True}
    ...     noSign=${True}

Search and go to a product detail
    [Arguments]  ${product_name}
    home_page.Input product name into search box  ${product_name}
    search_product_page.Scroll down and click product on search page  ${product_name}
