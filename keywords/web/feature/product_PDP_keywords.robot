*** Setting ***
Resource    ${CURDIR}/../web_imports.robot


*** Keywords ***
Search product and show on product PDP page
    [Arguments]    ${product_sku}    ${quantity}=${1}
    home_page_web_keywords.Search product by product sku    ${${product_sku}}[sku]
    Add product quantity    ${product_sku}    ${quantity}

Verify social icon on PDP page
    product_PDP_page.Verify social icon facebook on product PDP page
    product_PDP_page.Verify social icon twitter on product PDP page
    product_PDP_page.Verify social icon email on product PDP page
    product_PDP_page.Verify social icon line on product PDP page

Verify complete the look section when product is less than order equal to zero
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[txt_product_id]
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[product_active_section]
    product_PDP_page.Verify complete the look default tab is visible
    product_PDP_page.Verify previous button of complete the look tab is not visible
    product_PDP_page.Verify next button of complete the look tab is not visible

Verify product relate if moren than 5 items with SKU on product slider
    product_PDP_page.Verify product relate display on slider    ${order_PDP_CDS10232989.relate_product.no_1}
    product_PDP_page.Verify product relate display on slider    ${order_PDP_CDS11082736.relate_product.no_2}
    product_PDP_page.Verify product relate display on slider    ${order_PDP_CDS11082736.relate_product.no_3}
    product_PDP_page.Verify product relate display on slider    ${order_PDP_CDS11082736.relate_product.no_4}
    product_PDP_page.Verify product relate display on slider    ${order_PDP_CDS11082736.relate_product.no_5}
    product_PDP_page.Verify product relate secound page display on slider   ${order_PDP_CDS10232989.relate_product.no_2} 

Verify promotion of SKU
    [Arguments]    ${order_pdp}
    product_PDP_page.Verify promotion text display on product PDP page    ${order_pdp.sku}
    product_PDP_page.Verify full price on product PDP page   ${order_pdp.sku}    ${order_pdp.full_price}
    product_PDP_page.Verify discount price on product PDP page   ${order_pdp.sku}    ${order_pdp.discount_price}
    product_PDP_page.Verify redeem point on product PDP page    ${order_pdp.sku}    ${order_pdp.redeem_point}

Verify relate 5 product product with SKU
    #TODO: Should fix script to support dryrun
    [Tags]    robot:no-dry-run
    BuiltIn.Run Keyword If   '${TEST_PLATFORM}' == 'web'    Verify relate 5 product product with SKU on product slider    ELSE    Verify relate 2 product product with SKU on product slider

Verify relate 2 product product with SKU on product slider
    product_PDP_page.Verify product relate display on slider    ${order_PDP_CDS11082736.relate_product.no_1}
    product_PDP_page.Verify product relate display on slider    ${order_PDP_CDS11082736.relate_product.no_2}
    
Verify product relate if moren than 5 items with SKU
    BuiltIn.Run Keyword If   '${TEST_PLATFORM}' == 'web'    Verify product relate if moren than 5 items with SKU on product slider    ELSE    Verify product relate if more than 2 items with SKU on product slider

Verify product relate if more than 2 items with SKU on product slider
    product_PDP_page.Verify product relate display on slider    ${order_PDP_CDS10232989.relate_product.no_1}
    product_PDP_page.Verify product relate display on slider    ${order_PDP_CDS11082736.relate_product.no_2}

Verify product id display
    product_PDP_page.Verify product id display on product PDP page

Verify image product
    product_PDP_page.Verify slide image product

Verify button add to bag
    [Arguments]    ${product_sku}
    product_PDP_page.Verify add to bag button on product PDP page        ${${product_sku}}[sku]    

Click button add to bag
    [Arguments]    ${product_sku}
    product_PDP_page.Verify add to bag button on product PDP page        ${${product_sku}}[sku]
    product_PDP_page.Click add to bag button on product PDP page        ${${product_sku}}[sku]
    
Click icon cart
    product_PDP_page.Click Cart Add Item

Verify product details is display information in PDP page correct by sku
    [Arguments]    ${product_sku}
    ${product_info}=    product_PDP_page.Get product details to verify in PDP pageby sku    ${product_sku}
    #TODO: unable to compare text between data returned from API and data in UI DOM (Need more investigation and action to fix)
    #product_PDP_page.Verify product details on product PDP page    ${product_info.description}
    product_PDP_page.Verify product name on product PDP page    ${product_sku}
    product_PDP_page.Verify redeem point text display on product PDP page     ${product_sku}    ${product_info.redeem_point}
    product_PDP_page.Verify brand name on product PDP page     ${product_info.brand_name}
    product_PDP_page.Verify product picture display on product PDP page    ${product_sku}

Get configurable's simple product displayed via api
    [Arguments]    ${response}
    ${simple_product_sku}=   product_PDP_keywords.Get configurable's simple product displayed sku   ${response}
    ${dict_result}=    product_api.Get configurable's simple product data by sku    ${response}    ${simple_product_sku}
    [Return]    ${dict_result}

Get configurable's simple product displayed sku
    [Arguments]    ${response}
    ${list_img_src}=    JSONLibrary.Get Value From Json    ${response}    $..configurable_product_items..image
    Run Keyword And Ignore Error    product_PDP_page.Verify 'img product src' display on product PDP page    ${list_img_src}[0]    5s

    FOR    ${img_src}    IN    @{list_img_src}
        ${is_img_displayed}=    Run Keyword And Return Status    product_PDP_page.Verify 'img product src' display on product PDP page    ${img_src}    0.30s
        Exit For Loop If    ${is_img_displayed}==${True}
    END
    Should Be True    ${is_img_displayed}    img product by image src is not displayed
    ${simple_product_sku}=    JSONLibrary.Get Value From Json    ${response}    $..configurable_product_items[?(@.image=='${img_src}')].sku
    ${simple_product_sku}=    Set Variable    ${simple_product_sku}[0]
    [Return]    ${simple_product_sku}

Verify ui for smoke test - 'product header : product brand name' should display correctly via api
    [Arguments]    ${response}
    ${brand_name}=    JSONLibrary.Get Value From Json    ${response}    $..custom_attributes.brand_name
    product_PDP_page.Verify brand name on product PDP page    ${brand_name}[0]

Verify ui for smoke test - 'product header : product name' should display correctly via api
    [Arguments]    ${response}    ${product_sku}
    ${product_name}=    JSONLibrary.Get Value From Json    ${response}    $..product.name
    ${product_name}=    String.Strip String    ${SPACE}${product_name}[0]${SPACE}
    ${product_name}=    String.Replace String Using Regexp    ${product_name}    ${SPACE}+    ${SPACE}

    product_PDP_page.Verify 'product name label' should be displayed correctly    ${product_sku}   ${product_name}

Verify ui for smoke test - 'product details : image' should display correctly via api
    [Arguments]    ${json_object_by_sku}
    ${image}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..image
    ${is_main_image_display}=    Run Keyword And Return Status    product_PDP_page.Verify 'img product src' display on product PDP page    ${image}[0]
    Return From Keyword If    ${is_main_image_display}

    ${list_media_gallery_entries_file}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..media_gallery_entries[*].file
    FOR    ${media_gallery_entries_file}    IN    @{list_media_gallery_entries_file}
        ${is_media_gallery_entries_file_display}=    Run Keyword And Return Status    product_PDP_page.Verify 'img product src' display on product PDP page    ${media_gallery_entries_file}
        Exit For Loop If    ${is_media_gallery_entries_file_display}
    END
    Should Be True    ${is_media_gallery_entries_file_display}    msg=Image not displayed

Verify ui for smoke test - 'product details : redeem point' should display correctly via api
    [Arguments]    ${json_object_by_sku}    ${product_sku}
    ${t1c_redeemable_points}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..extension_attributes.t1c_redeemable_points[*]
    product_PDP_page.Verify redeem point text display on product PDP page    ${product_sku}    ${t1c_redeemable_points}[0]

Verify ui for smoke test - 'product details : product price' should display correctly via api
    [Arguments]    ${json_object_by_sku}    ${product_sku}
    ${dict_product_price}=    product_api.Get product price data by product sku    ${json_object_by_sku}
    Run Keyword If    ${dict_product_price}[is_product_no_discount]    product_PDP_keywords.Verify ui for smoke test - 'product price no discount' should be displayed correctly    ${product_sku}    ${dict_product_price}
    ...    ELSE    product_PDP_keywords.Verify ui for smoke test - 'product price with discount' should be displayed correctly    ${product_sku}    ${dict_product_price}

Verify ui for smoke test - 'product details : product image' should display correctly via api
    [Arguments]    ${json_object_by_sku}    ${product_sku}
    ${img_src}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..image
    ${len}=    BuiltIn.Get Length    ${img_src}
    Should Be Equal As Integers    ${len}    ${1}
    product_PDP_page.Verify 'img product src' display on product PDP page    ${img_src}[0]

Verify ui for smoke test - 'product details : product overlay image' should display correctly via api
    [Arguments]    ${json_object_by_sku}
    ${dict_product_overlays}=     product_api.Get display product overlays by product sku    ${json_object_by_sku}
    Run Keyword If    ${dict_product_overlays}[is_display]    product_PDP_page.Verify 'img product overlay src' display on product PDP page    ${dict_product_overlays}[overlay_image]

Verify ui for smoke test - 'product details : product tag new / promotion' should display correctly via api
    [Arguments]    ${json_object_by_sku}    ${product_sku}
    ${new_tag}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..new
    ${new_tag}=    Set Variable    ${new_tag}[0]
    ${promo_name_tag}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..promo_name_tag
    ${promo_name_tag}=    Set Variable    ${promo_name_tag}[0]
    
    ${promo_name_tag}=    Run Keyword If    '${promo_name_tag}' == '${None}'    Set Variable    ${promo_name_tag}
    ...    ELSE    String.Strip String    ${promo_name_tag}
    
    Run Keyword If    '${promo_name_tag}' != '${None}' and '${promo_name_tag}' != '${EMPTY}'   Run Keywords    product_PDP_page.Verify 'product promo tag' should be displayed correctly    ${product_sku}    ${promo_name_tag}   
    ...    AND    product_PDP_page.Verify 'product new tag' should not be displayed    ${product_sku}
    ...   ELSE IF    ${new_tag} == ${1}   product_PDP_page.Verify 'product new tag' should be displayed    ${product_sku}
    ...   ELSE     product_PDP_page.Verify 'product new tag' should not be displayed    ${product_sku}

Verify ui for smoke test - 'product details : product tag only at / online exclusive' should display correctly via api
    [Arguments]    ${json_object_by_sku}    ${product_sku}
    ${online_exclusive_tag}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..online_exclusive_tag
    ${online_exclusive_tag}=    Set Variable    ${online_exclusive_tag}[0]
    ${only_central_tag}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..only_central_tag
    ${only_central_tag}=    Set Variable    ${only_central_tag}[0]

    Run Keyword If    ${only_central_tag} == ${1}   Run Keywords   product_PDP_page.Verify 'product only at tag' should be displayed    ${product_sku}
    ...   AND    product_PDP_page.Verify 'product online exclusive tag' should not be displayed    ${product_sku}
    ...   ELSE IF   ${online_exclusive_tag} == ${1}   Run Keywords    product_PDP_page.Verify 'product online exclusive tag' should be displayed    ${product_sku}
    ...   AND    product_PDP_page.Verify 'product only at tag' should not be displayed    ${product_sku}
    ...   ELSE    Run Keywords    product_PDP_page.Verify 'product online exclusive tag' should not be displayed    ${product_sku}
    ...   AND    product_PDP_page.Verify 'product only at tag' should not be displayed    ${product_sku}

Verify ui for smoke test - 'product details : product market place' should display correctly via api
    [Arguments]    ${json_object_by_sku}    ${product_sku}
    ${marketplace_product_type}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..custom_attributes_option.marketplace_product_type
    ${marketplace_product_type}=    Set Variable    ${marketplace_product_type}[0]
    ${marketplace_seller}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..custom_attributes_option.marketplace_seller
    ${marketplace_seller}=    Set Variable    ${marketplace_seller}[0]

    Run Keyword If    '${marketplace_product_type}' == '${None}'    product_PDP_page.Verify 'product marketplace sold by' should not be displayed
    ...    ELSE    product_PDP_page.Verify 'product marketplace sold by' should be displayed correctly    ${marketplace_seller}    

Verify ui for smoke test - 'product details : pay by installment' should display correctly via api
    [Arguments]    ${json_object_by_sku}
    ${list_installment_plans}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..custom_attributes_option.installment_plans
    ${list_installment_plans}=    Set Variable    ${list_installment_plans}[0]

    Run Keyword If    "${list_installment_plans}" == "${None}"    product_PDP_page.Verify 'pay by installment' should not be displayed
    ...   ELSE    product_PDP_page.Verify 'pay by installment' should be displayed

Verify ui for smoke test - 'product price no discount' should be displayed correctly
    [Arguments]    ${product_sku}    ${dict_product_price}
    product_PDP_page.Verify discount price on product PDP page    ${product_sku}    ${dict_product_price}[price]
    product_PDP_page.Verify full price on product PDP page should not be displayed    ${product_sku}
    product_PDP_page.Verify save price percent on product PDP page should not be displayed

Verify ui for smoke test - 'product price with discount' should be displayed correctly
    [Arguments]    ${product_sku}    ${dict_product_price}
    product_PDP_page.Verify discount price on product PDP page    ${product_sku}    ${dict_product_price}[special_price]
    product_PDP_page.Verify full price on product PDP page    ${product_sku}    ${dict_product_price}[price]
    product_PDP_page.Verify save price percent on product PDP page    ${dict_product_price}[evaluate_percent_discount]
    
Verify ui for smoke test - 'product details : product configurable options' should be displayed
    [Arguments]    ${response}    ${product_id}
    ${dict_product_configurable_options}=    product_api.Get configurable option by product id    ${response}    ${product_id}
    FOR    ${product_details_attr}    IN    @{dict_product_configurable_options}
            ${label}=    Set Variable    ${dict_product_configurable_options}[${product_details_attr}][label]
            Run Keyword If    '${product_details_attr}' == 'Shade'              product_PDP_page.Verify product details attr 'Shade' should be displayed correctly    ${label}
            ...    ELSE IF    '${product_details_attr}' == 'Size'               product_PDP_page.Verify product details attr 'Size' should be displayed correctly    ${label.strip()}
            ...    ELSE IF    '${product_details_attr}' == 'Color Template'     product_PDP_page.Verify product details attr 'Color Template' should be displayed correctly    ${label.strip()}
            ...    ELSE    Fail    Incorrect configurable option
    END

Verify ui for smoke test - 'product details : product id' should display correctly via api
    [Arguments]    ${json_object_by_sku}
    ${product_sku}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..sku
    ${product_sku}=    Set Variable    ${product_sku}[0]
    product_PDP_page.Verify 'product id' should be displayed correctly    ${product_sku}

Verify ui for smoke test - 'product details : product customer reviews' should be displayed via api
    [Arguments]    ${response}
    ${total_vote}=    JSONLibrary.Get Value From Json    ${response}    $..overall_rating.total_vote
    ${total_vote}=    Set Variable     ${total_vote}[0]
    
    Run Keyword And Return If    ${total_vote}==0    product_PDP_keywords.Verify ui for smoke test - 'overall rating' should not be displayed
    product_PDP_keywords.Verify ui for smoke test - 'overall rating' should be displayed correctly    ${response}
    product_PDP_keywords.Verify ui for smoke test - 'all reviews' should be displayed correctly    ${response}

Verify ui for smoke test - 'overall rating' should not be displayed
    product_PDP_page.Verify 'no review label' should be displayed
    product_PDP_page.Verify 'add a review link' should be displayed

Verify ui for smoke test - 'overall rating' should be displayed correctly
    [Arguments]    ${response}
    ${dict_overall_rating}=    JSONLibrary.Get Value From Json    ${response}    $..overall_rating
    ${dict_overall_rating}=    Set Variable    ${dict_overall_rating}[0]

    product_PDP_page.Verify 'no review label' should not be displayed
    product_PDP_page.Verify 'rating label' should be displayed correctly    ${dict_overall_rating}[rating]
    product_PDP_page.Verify 'total vote label' should be displayed correctly    ${dict_overall_rating}[total_vote]
    product_PDP_page.Verify 'all label' should be displayed correctly    ${dict_overall_rating}[total_vote]
    product_PDP_page.Verify 'five star label' should be displayed correctly    ${dict_overall_rating}[five_star]
    product_PDP_page.Verify 'four star label' should be displayed correctly    ${dict_overall_rating}[four_star]
    product_PDP_page.Verify 'three star label' should be displayed correctly    ${dict_overall_rating}[three_star]
    product_PDP_page.Verify 'two star label' should be displayed correctly    ${dict_overall_rating}[two_star]
    product_PDP_page.Verify 'one star label' should be displayed correctly    ${dict_overall_rating}[one_star]

Verify ui for smoke test - 'all reviews' should be displayed correctly
    [Arguments]    ${response}    ${range}=${5}
    ${list_reviews}=    JSONLibrary.Get Value From Json    ${response}    $..reviews[*]
    ${total_reviews}=    Get Length    ${list_reviews}
    ${range}    Set Variable If    ${total_reviews} < ${range}    ${total_reviews}    ${range}

    FOR    ${index}    IN RANGE   0    ${range}
        ${index_locator}=    Evaluate    ${index} + 1
        product_PDP_page.Verify 'customer reviews details' should be displayed correctly    ${index_locator}    ${list_reviews}[${index}][nickname]
        product_PDP_page.Verify 'customer reviews details' should be displayed correctly    ${index_locator}    ${list_reviews}[${index}][title]
        product_PDP_page.Verify 'customer reviews details' should be displayed correctly    ${index_locator}    ${list_reviews}[${index}][rating_items][rating]
    END

Verify store name display correctly with postcode search
    [Arguments]    ${response}    ${postcode}
    ${api_stores}=    Get list active store    ${response}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictproductPDP}[txt_search_postcode]    ${postcode}
    ${ui_stores}=    common_keywords.Get List Text Elements    ${dictproductPDP}[lbl_store_name]
    ${status}=    Evaluate    all(elem in ${api_stores} for elem in ${ui_stores})
    Should Be True    ${status}

Verify store name display correctly with filter
    [Arguments]    ${response}    ${filter_option}
    @{api_stores}=    Get list active store    ${response}
    CommonWebKeywords.Click Element    ${dictproductPDP}[lnk_filter_by]
    ${filter}=    Format Text    ${dictproductPDP}[lnk_filter_option]    $filter=${filter_option}
    CommonWebKeywords.Click Element    ${filter}
    @{ui_stores}=    common_keywords.Get List Text Elements    ${dictproductPDP}[lbl_store_name]
    ${status}=    Evaluate    all(elem in ${api_stores} for elem in ${ui_stores})
    Should Be True    ${status}

Get list active store
    [Arguments]    ${response}
    ${json_expression}=    Set Variable    $[?(@.is_active==true)].name
    ${store_name}=    JSONLibrary.Get Value From Json    ${response}    ${json_expression}
    ${api_stores}=    Convert To List    ${store_name}
    [Return]    ${api_stores}

Get random sku from list
    ${product_type_sku}=    Get list random sku of plp page
    Run Keyword And Return If    len(${product_type_sku})==${0}    Fail    msg=Please check product skus on PLP again
    ${product_type_sku}=    Evaluate    [i for i in ${product_type_sku} if i.startswith('CDS')]
    ${sku}=    Evaluate    random.choice(${product_type_sku})    random
    [Return]    ${sku}

Verify sku contain click and collect method
    [Arguments]    ${response_method}
    @{delivery_methods}=    Get Value From Json    ${response_method}    $..delivery_methods..delivery_method
    ${status}=    Run Keyword And Return Status    Should Contain Match    ${delivery_methods}    ${filters.delivery_methods.click_and_collect}
    Run Keyword And Return If    '${status}' == '${FALSE}'    Fail    msg=Please check on sku with click & collect method again

Select ramdom configurable options via api, return product
    [Arguments]    ${response}
    ${list_product_id_mathed_configurable_options}=    JSONLibrary.Get Value From Json    ${response}     $..configurable_product_options..products[*]
    ${random_product_id}=    Evaluate    random.choice(${list_product_id_mathed_configurable_options})    random
    
    ${dict_configurable_options}=    product_api.Get configurable option by product id    ${response}    ${random_product_id}
    FOR    ${configurable_options_label}    IN    @{dict_configurable_options}
        Run Keyword If    '${configurable_options_label}' == 'Shade'    product_PDP_page.Select configurable option Shade    ${dict_configurable_options}[${configurable_options_label}][value_index]
        ...    ELSE IF    '${configurable_options_label}' == 'Size'    product_PDP_page.Select configurable option Size    ${dict_configurable_options}[${configurable_options_label}][label]
        ...    ELSE    Fail    Incorrect configurable option
    END

    ${sku}=    JSONLibrary.Get Value From Json    ${response}    $..configurable_product_items[?(@.id=='${random_product_id}')].sku
    ${name}=    JSONLibrary.Get Value From Json    ${response}    $..configurable_product_items[?(@.id=='${random_product_id}')].name
    
    ${dict_result}    BuiltIn.Create Dictionary    sku=${sku}[0]    name=${name}[0]
    [Return]    ${dict_result}