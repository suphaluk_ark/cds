*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Keywords ***
Verify free items with order purchase should be display correctly
    [Arguments]    @{list_free_items_sku}
    my_cart_page.Click 'free items with order purchase' link
    FOR   ${free_item_sku}   IN    @{list_free_items_sku}
        ${sku}=    Set Variable    ${${free_item_sku}}[sku]
        ${name}=    Set Variable    ${${free_item_sku}}[name]
        my_cart_page.Verify 'free items with order purchase' should be display correctly   ${sku}    ${name}
    END

Verify free gift with this item purchase should be display correctly
    [Arguments]    ${purchase_item_sku}    @{list_free_items_sku}
    my_cart_page.Click 'free gift with this item purchase' link    ${purchase_item_sku}
    FOR   ${free_item_sku}   IN    @{list_free_items_sku}
        ${free_gift_product_name}=    Set Variable    ${${free_item_sku}}[name]
        my_cart_page.Verify 'free gift with this item purchase' should be display correctly    ${purchase_item_sku}    ${free_gift_product_name}
    END