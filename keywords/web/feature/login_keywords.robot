*** Setting ***
Resource    ${CURDIR}/../web_imports.robot


*** Keywords ***
Login Keywords - Login Success
    [Arguments]    ${email}    ${password}    ${display_name}
    Header Web - Click Login on Header
    Header Web - Input Email on Header    ${email}
    Header Web - Input Password on Header    ${password}
    Header Web - Click Submit Button on Header
    Wait Until Page Is Completely Loaded
    Header Web - Verify Display Name Is Visible    ${display_name}

Login Keywords - Login With Email
    [Arguments]    ${email}=${email}    ${password}=${password}
    Header Web - Click Login on Header
    Header Web - Input Email on Header    ${email}
    Header Web - Input Password on Header    ${password}
    Header Web - Click Submit Button on Header

Login Keywords - Logut Success
    my_account_page.Click on the user icon
    Header Web - Click Logout on Header

Login Keyword - Email field Should Be Visible Error
    [Arguments]    ${expect_message}
    Header Web - Email Field Display Error Message    ${expect_message}

Login Keyword - Password field Should Be Visible Error
    [Arguments]    ${expect_message}
    Header Web - Password Field Display Error Message    ${expect_message}

Login Keyword - Verify Error Message When Login Failed
    [Arguments]    ${expect_message}
    Header Web - Verify Error Message Afer Login Failed    ${expect_message}

Login Keywords - Sign Out Success
    Header Web - Click Accont Button on Header
    Header Web - Click Logout on Header
    home_page_web_keywords.Verify homepage page should be displayed