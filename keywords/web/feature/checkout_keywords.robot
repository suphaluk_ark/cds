*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Keywords ***
Add multiple product to cart
    [Arguments]    @{product}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    FOR    ${product}    IN    @{product}
        Add product to cart by SKU    ${product}
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Add product to cart by SKU
    [Arguments]    ${product_sku}    ${quantity}=${1}
    home_page_web_keywords.Search product by product sku    ${${product_sku}}[sku]
    Add product quantity    ${product_sku}    ${quantity}
    product_page.Click on add to cart button    ${product_sku}
    Wait Until Keyword Succeeds    3 x    5 sec    header_web_keywords.Click view cart button should not be visible
    Header Web - Click Mini Cart Button
    header_web_keywords.Product should be displayed in mini cart    ${${product_sku}}[sku]    ${${product_sku}}[name]
    Header Web - Click View Cart Button
    # Run Keyword If    '${member_type}'=='guest_member' and '${ENV.lower()}'=='prod'    login_page.Click checkout as guest button
    my_cart_page.Product should be displayed in my cart page    ${${product_sku}}[sku]    ${${product_sku}}[name]

Check Out Product Until Payment With COD Method
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    ## for guest input address
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method COD
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For COD Payment Method
    Wait Until Page Loader Is Not Visible

Check Out Product With T1 Redeem Until Payment With COD Method
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    ## for guest input address
    checkout_page.Select shipping to address option
    checkout_page.Select default address
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    Redeem point and verify T1C point discount    ${40}    ${5}    ${t1c_information_4.email}   ${t1c_information_4.password}
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method COD
    Wait Until Page Loader Is Not Visible
    #SeleniumLibrary.Wait Until Element Is Visible    ${locator}
    checkout_page.Click Confirm Order For COD Payment Method
    Wait Until Page Loader Is Not Visible

Checkout Product With T1 Redeem Until Payment With Credit Card Method
    [Arguments]    ${point}    ${baht}    ${tc1_email}=${t1c_information_4.email}    ${tc1_pwd}=${t1c_information_4.password}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    ## for guest input address
    checkout_page.Select shipping to address option
    checkout_page.Select default address
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    Redeem point and verify T1C point discount    ${point}    ${baht}    ${tc1_email}    ${tc1_pwd}
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    # Wait Until Page Loader Is Not Visible
    # Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Order successful with credit card

Check Out Product Until Payment With COD Method And Request Full Tax Invoice(Personal)
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    delivery_details_page.Click request tax invoice
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method COD
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For COD Payment Method
    Wait Until Page Loader Is Not Visible


Check Out Product Until Payment With COD Method For Guest Member
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Input customer details
    Select shipping to address option
    Input shipping address information
    Wait Until Page Loader Is Not Visible
    checkout_page.Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method COD
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For COD Payment Method
    Wait Until Page Loader Is Not Visible

Checkout, select pick at store and pay by credit card
    [Arguments]    ${store_name}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Is Completely Loaded
    Select pick at store    ${store_name}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Click Pay now Button
    Wait until page is completely loaded
    Order successful with credit card

Checkout, select pick at store and pay by credit card For Guest Member
    [Arguments]    ${store_name}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Is Completely Loaded
    Input customer details
    Select pick at store    ${store_name}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Order successful with credit card

Order successful with credit card
    Return From Keyword If    '${ENV.lower()}'=='prod'
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    2c2p.Proceed OTP

Order unsuccessful with credit card
    Return From Keyword If    '${ENV.lower()}'=='prod'
    checkout_page.Input credit card
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    2c2p.Click on cancel payment button

Order successful with credit card for repayment
    # [Arguments]    ${amount}
    2c2p.Display product details in 2c2p page
    ${order_number}=    Get order number from 2C2P page
    Set Test Variable    ${order_number}    ${order_number}
    Return From Keyword If    '${ENV.lower()}'=='prod'
    # 2c2p.Display product amount in 2c2p page    ${amount}
    2c2p.Submit credit card
    2c2p.Proceed OTP

Cancel payment credit card
    2c2p.Click on cancel payment button

Checkout, select pick at store and pay by 1 2 3 bank transfer(check shipping charge)
    [Arguments]    ${store_name}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Select pick at store    ${store_name}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method 1 2 3 bank transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${personal_information.tel}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button

Checkout, select pick at store and pay by 1 2 3 bank transfer For Guest Member(check shipping charge)
    [Arguments]    ${store_name}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Input customer details
    Select pick at store    ${store_name}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method 1 2 3 bank transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${personal_information.tel}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button

Checkout, select pick at watson and pay by 1 2 3 bank transfer(check shipping charge)
    [Arguments]    ${store_name}   ${store_address}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Select pick at skybox and family mart    ${store_name}
    Wait Until Page Loader Is Not Visible
    checkout_page.shipping charge for pick up at family mart should be displayed    ${store_name}
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible
    checkout_page.shipping charge for pick up at family mart should be displayed    ${store_address}
    checkout_page.Select Payment Method 1 2 3 bank transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.shipping charge for pick up at family mart should be displayed    ${store_address}
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${personal_information.tel}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button

Checkout, select pick at watson and pay by 1 2 3 bank transfer For Guest Member(check shipping charge)
    [Arguments]    ${store_name}   ${store_address}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Input customer details
    Select pick at skybox and family mart    ${store_name}
    Wait Until Page Loader Is Not Visible
    checkout_page.shipping charge for pick up at family mart should be displayed    ${store_name}
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible
    checkout_page.shipping charge for pick up at family mart should be displayed    ${store_address}
    checkout_page.Select Payment Method 1 2 3 bank transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.shipping charge for pick up at family mart should be displayed    ${store_address}
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${personal_information.tel}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button

Checkout, select pick at watson and pay by 1 2 3 bank transfer
    [Arguments]    ${store_name}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Select pick at skybox and family mart    ${store_name}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method 1 2 3 bank transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${personal_information.tel}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button

Checkout, select pick at watson and pay by 1 2 3 bank transfer For Guest Member
    [Arguments]    ${store_name}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Input customer details
    Select pick at skybox and family mart    ${store_name}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method 1 2 3 bank transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${personal_information.tel}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button

Checkout banktranfer for guest member
    checkout_page.Select Payment Method 1 2 3 bank transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${change_shipping_address}[telephone]
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button

Select pick at store
    [Arguments]    ${store_name}
    checkout_page.Select pick at store option
    Wait Until Page Loader Is Not Visible
    checkout_page.Select store    ${store_name}
    Click on select this store button

Pickup at store, standard pickup, go to payment page
    [Arguments]    ${store_name}
    checkout_page.Select pick at store option
    Wait Until Page Loader Is Not Visible
    checkout_page.Input name or postcode then select the store    ${store_name}
    checkout_page.Click on select store button    ${store_name}
    checkout_page.Click on standard pickup option after select store
    checkout_page.Click Continue Payment Button

Select pick at skybox and family mart
    [Arguments]    ${store_name}
    checkout_page.Select pick at skybox and family mart option
    Wait Until Page Loader Is Not Visible
    Continue Payment Button Should Be Visible
    checkout_page.Select store    ${store_name}
    Click on select this store button

Pay by 1 2 3 kbank ATM transfer
    checkout_page.Select Payment Method 1 2 3 bank transfer
    Wait Until Page Loader Is Not Visible
    checkout_page.Select bank or service counter for 1 2 3 bank transfer    ${bank_name.kbank}
    checkout_page.Select payment channel    ${payment_channel.atm}
    checkout_page.Input phone number for 1 2 3 bank transfer    ${personal_information.tel}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Payment Code Should Display In Payment Slip
    bank_transfer.Click return to merchant button

Re-payment by credit card
    checkout_page.Click re-payment now button
    Order successful with credit card for repayment

Verify customer profile display information correctly
    ${actual_customer_name}=    checkout_page.Get customer firstname and lastname
    ${actual_email}=    checkout_page.Get customer email
    ${actual_telephone}=    checkout_page.Get customer telephone

    ${expected_customer_name}=    Set Variable    ${member_profile}[firstname]${SPACE}${member_profile}[lastname]
    ${expected_email}=    Set Variable    ${member_profile}[email]
    ${expected_ftelephone}=    Set Variable    ${member_profile}[telephone]

    Should Be Equal    ${actual_customer_name}    ${expected_customer_name}
    Should Be Equal    ${actual_email}    ${expected_email}
    Should Be Equal    ${actual_telephone}    ${expected_ftelephone}

Verify delivery options are displayed correctly
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[shipping_to_address]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[pick_at_store]
    #CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[pick_at_skybox]

Verify delivery shipping option are displayed correctly
    checkout_page.Standard delivery should be displayed
    Run Keyword If    "${member_type}"=="personal_member"    Verify shipping option for member    ELSE    Verify shipping option for guest

Verify shipping option for member
    Verify Same Day shipping option is not displayed in disable time setting
    Standard delivery should be displayed

Verify shipping option for guest
    checkout_page.Same-day delivery should not be visible
    checkout_page.Next-day delivery should not be visible

Verify standard delivery shipping price by relate to subtotal
    ${shipping_price}=    checkout_page.Get standard delivery shipping price
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    payment_page.Click on view details on mobile
    ${subtotal}=    checkout_page.Get subtotal from order summary
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    payment_page.Close view deatails on mobile

    Run Keyword If    '${BU.lower()}'=='${bu_type.rbs}'    Should Be Equal    ${shipping_price}    ${standard_delivery_price_default}
    ...    ELSE IF    ${subtotal} > ${subtotal_for_free_shipping}    Should Be Equal    ${shipping_price}    ${standard_delivery_free_shipping}
    ...    ELSE    Should Be Equal    ${shipping_price}    ${standard_delivery_price_default}

Verify same day delivery shipping price are display correctly
    ${status}=    Verify Same Day shipping option is not displayed in disable time setting

    Return From Keyword If    ${status}==${TRUE}
    ${shipping_price}=    checkout_page.Get same day delivery shipping price
    Should Be True    ${shipping_price}==${same_day_delivery_price}

Verify next day delivery shipping price are display correctly
    ${shipping_price}=    checkout_page.Get next day delivery shipping price
    Should Be True    ${shipping_price}==${next_day_delivery_price}

Verify 3 hour delivery shipping price are display correctly
    ${shipping_price}=    checkout_page.Get 3 hour delivery shipping price
    Should Be True    ${shipping_price}==${3_hour_delivery_price}

Select Same day or Next day delivery shipping method
    ${status}=    Verify Same Day shipping option is not displayed in disable time setting
    Run Keyword If    ${status}==${TRUE}    checkout_page.Click on Next-day delivery button
    ...    ELSE    checkout_page.Click on Same-day delivery button

Verify Same Day shipping option is not displayed in disable time setting
    ${current_date_time}=    Get Current Date     result_format='%Y-%m-%d %H:%M:%S'
    ${time_disable_start}=    Get Current Date     result_format='%Y-%m-%d ${same_day_disable_time_start}'
    ${time_disable_end}=    Get Current Date     result_format='%Y-%m-%d ${same_day_disable_time_end}'
    ${status}=    Run Keyword And Return Status    Should Be True    ${time_disable_start}<=${current_date_time}<=${time_disable_end}

    Return From Keyword If    '${BU.lower()}'=='${bu_type.rbs}'
    Run Keyword If    ${status}==${TRUE}    Same-day delivery should not be visible
    ...    ELSE    Same-day delivery should be displayed

    [Return]    ${status}

Get subtotal by product sku
    [Arguments]    @{sku_list}
    ${summary_subtotal}=    Set Variable    ${0}
    FOR    ${product_sku}    IN    @{sku_list}
        ${product_qty}=    checkout_page.Get product quantiy from order summary    ${product_sku}
        ${product_price}=    Get Product Price From Product Data    ${product_sku}
        ${subtotal}=    Evaluate    ${product_qty}*${product_price}
        ${summary_subtotal}=    Evaluate    ${summary_subtotal}+${subtotal}
    END
    [Return]    ${summary_subtotal}

Verify subtotal in order summary is calculated correctly
    [Arguments]    ${sku_list}
    ${actual_subtotal}=    checkout_page.Get subtotal from order summary
    ${expected_subtotal}=    Get subtotal by product sku    ${sku_list}
    Should Be Equal     ${expected_subtotal}    ${actual_subtotal}

Verify grand total in order summary is calculated correctly without any discount
    [Arguments]    ${delivery_option_selected}
    ${actual_grand_total}=    checkout_page.Get grand total from order summary
    ${subtotal}=    checkout_page.Get subtotal from order summary
    ${shipping_fee}=    Get shipping fee by delivery option    ${delivery_option_selected}
    ${expected_grand_total}=    Evaluate    ${subtotal}+${shipping_fee}
    Should Be Equal     ${expected_grand_total}    ${actual_grand_total}
    

Get shipping fee by delivery option
    [Arguments]    ${delivery_option_selected}

    ${shipping_fee}=    Set Variable If    
    ...    "${delivery_option_selected}"=="${delivery_type}[shipping_standard]"    ${standard_delivery_price_default}
    ...    "${delivery_option_selected}"=="${delivery_type}[shipping_same_day]"    ${same_day_delivery_price}
    ...    "${delivery_option_selected}"=="${delivery_type}[shipping_next_day]"    ${next_day_delivery_price}
    ...    "${delivery_option_selected}"=="${delivery_type}[pickup_skybox_family_mart]"    ${standard_delivery_price_default}
    ...    ${standard_delivery_free_shipping}
    
    ${shipping_free_flag}=    Set Variable If
    ...    "${delivery_option_selected}"=="${delivery_type}[shipping_standard]"    ${TRUE}
    ...    "${delivery_option_selected}"=="${delivery_type}[pickup_skybox_family_mart]"    ${TRUE}
    ...    ${FALSE}

    Return From Keyword If    ${shipping_free_flag}==${FALSE}    ${shipping_fee}
    ${subtotal}=    checkout_page.Get subtotal from order summary
    ${shipping_fee}=    Set Variable If    '${BU.lower()}'=='${bu_type.rbs}'    ${shipping_fee}
    ...    ${subtotal}>${subtotal_for_free_shipping}    ${standard_delivery_free_shipping}
    ...    ${shipping_fee}

    [Return]    ${shipping_fee}

Verify there is no any delivery option is selected
    checkout_page.Delivery shipping option is not Selected
    checkout_page.Delivery pick up at store option is not Selected
    #checkout_page.Delivery pick up at skybox family mart option is not Selected

Add shipping address with guest
    checkout_page.Select shipping to address option
    change_shipping_address_popup.Input building to update shipping address    ${change_shipping_address}[building]
    change_shipping_address_popup.Input address no to update shipping address    ${change_shipping_address}[address_no]
    change_shipping_address_popup.Input postcode to update shipping address    ${billing_address_th}[zip_code]
    Wait Until Page Is Completely Loaded
    checkout_page.Select region    ${billing_address_th}[region]
    checkout_page.Select district    ${billing_address_th}[district]
    checkout_page.Select Subdistrict    ${billing_address_th}[sub_district]
    
Verify default shipping address is displayed correctly
    [Arguments]    ${username}    ${password}
    common_cds_web.Get customer info with current shipping address from MDC api by customer member    ${username}    ${password}
    
    ${expected_shipping_customer_name}=    Set Variable    ${member_profile}[shipping_firstname]${SPACE}${member_profile}[shipping_lastname]
    ${expected_shipping_telephone}=    Set Variable    ${member_profile}[telephone]
    
    ${expected_shipping_address_name}=    Set Variable    ${member_profile}[address_name]
    ${expected_shipping_building}=    Set Variable    ${member_profile}[building]
    ${expected_shipping_address_no}=    Set Variable    ${member_profile}[address_no]
    ${expected_shipping_subdistrict}=    Set Variable    ${member_profile}[subdistrict_name]
    ${expected_shipping_district}=    Set Variable    ${member_profile}[district_name]
    ${expected_shipping_province}=    Set Variable    ${member_profile}[province]
    ${expected_shipping_postcode}=    Set Variable    ${member_profile}[postcode]
    
    ${actual_shipping_customer_name}=    checkout_page.Get default shipping customer firstname and lastname
    ${actual_shipping_telephone}=    checkout_page.Get default shipping telephone
    ${actual_shipping_address_name}=    checkout_page.Get default shipping address name
    ${actual_shipping_address_no_building}=    checkout_page.Get default shipping address number
    ${actual_shipping_address}=    checkout_page.Get default shipping full address

    Should Be Equal    ${expected_shipping_customer_name}    ${actual_shipping_customer_name}
    Should Be Equal    ${expected_shipping_telephone}    ${actual_shipping_telephone}
    Should Be Equal    ${expected_shipping_address_name}    ${actual_shipping_address_name}
    Should Contain    ${actual_shipping_address_no_building}    ${expected_shipping_building}
    Should Contain    ${actual_shipping_address_no_building}    ${expected_shipping_address_no}
    Should Contain    ${actual_shipping_address}    ${expected_shipping_subdistrict}
    Should Contain    ${actual_shipping_address}    ${expected_shipping_district}
    Should Contain    ${actual_shipping_address}    ${expected_shipping_province}
    Should Contain    ${actual_shipping_address}    ${expected_shipping_postcode}

Input guest infomation at checkout page
    my_account_page.Click and input text for first name on address book page    ${regresstion_information_1.firstname}
    my_account_page.Click and input text for last name on address book page    ${regresstion_information_1.lastname}
    registration_page.Input guest email    ${register_email}
    registration_page.Input guest telephone    ${change_shipping_address}[telephone]

Add product to cart and go to checkout page
    [Arguments]    ${product_sku}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible

Select standard pickup and go to payment page
    [Arguments]    ${store_pickup}
    Select pick at store    ${store_pickup}
    Select standard pickup option
    E2E_flow_keywords.Go to payment page

Select 2 hour pickup and go to payment page
    [Arguments]    ${store_pickup}
    Select pick at store    ${store_pickup}
    Select 2 hour pickup option
    E2E_flow_keywords.Go to payment page

Redeem point and verify T1C point discount
    [Arguments]    ${point}    ${baht}    ${tc1_email}=${t1c_automation_1.email}    ${tc1_pwd}=${t1c_automation_1.password}
    checkout_page.Click connect to T1C link
    checkout_page.Login to T1C account    ${tc1_email}    ${tc1_pwd}
    checkout_page.Click apply point after login T1C
    checkout_page.Input T1C point    ${point}
    checkout_page.Click apply T1C point
    Wait Until T1C Loader Is Not Visible
    Wait Until Page Loader Is Not Visible
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    Click on Redeem button
    # TODO: fix redeem point validate
    # checkout_page.Total redeem point and discount for the 1 redemption should be    ${point}    ${baht}

Add multiple product to cart and go to checkout page
    [Arguments]    ${multiple_products}
    common_cds_web.Search product and add multiple products to cart    ${multiple_products}
    Header Web - Click View Cart Button
    common_keywords.Wait Until Page Is Completely Loaded
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible

Full redeem point and verify T1C point discount
    [Arguments]    ${tc1_email}    ${tc1_pwd}
    checkout_page.Click connect to T1C link
    checkout_page.Login to T1C account    ${tc1_email}    ${tc1_pwd}
    checkout_page.Click apply point after login T1C
    checkout_page.Select on full redeem point
    checkout_page.Click apply T1C point
    my_cart_page.Wait Until Page Loader Is Not Visible

Order successful with credit card installment
    [Arguments]    ${bank}    
    ...    ${installment_month_plan}     
    ...    ${card_number}=${credit_card}[number]
    ...    ${card_holder}=${credit_card}[holder]
    ...    ${card_cvv}=${credit_card}[cvv]
    ...    ${expiry_month}=${credit_card}[month]
    ...    ${expiry_year}=${credit_card}[year]
    Return From Keyword If    '${ENV.lower()}'=='prod'
    checkout_page.Select payment method 2c2p Installment
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select bank pay by installment    ${bank}
    checkout_page.Select installment month plan    ${installment_month_plan}
    SeleniumLibrary.Wait Until Page Contains Element    ${dictCheckoutPage}[iframe_credit_card_installment]
    SeleniumLibrary.Select Frame    ${dictCheckoutPage}[iframe_credit_card_installment]
    checkout_page.Input credit card holder   ${card_holder}
    checkout_page.Input credit card number   ${card_number}
    checkout_page.Input credit card cvv   ${card_cvv}
    checkout_page.Input credit card expiry date    ${expiry_month}${expiry_year}
    SeleniumLibrary.Unselect Frame
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    2c2p.Proceed OTP

Proceed to check out and verify with Credit Card for 2 Hour pickup
    Select credit card option and input valid card information    ${credit_card.holder}    ${credit_card.number}    ${credit_card.month_year}    ${credit_card.cvv}
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    2c2p.Proceed OTP
    Verify each section on payment slip page displays correctly
    Verify each section on Thank you page displays correctly

Select credit card option and input valid card information
    [Arguments]    ${card_name}    ${card_number}    ${card_expiry_date}    ${card_cvv}
    Wait Until Page Is Completely Loaded
    Select option credit card and full payment
    checkout_keywords.Input valid card information    ${card_name}    ${card_number}    ${card_expiry_date}    ${card_cvv}

Input valid card information
    [Arguments]    ${card_name}    ${card_number}    ${card_expiry_date}    ${card_cvv}
    SeleniumLibrary.Wait Until Page Contains Element    ${dictCheckoutPage}[iframe_credit_card]
    SeleniumLibrary.Select Frame    ${dictCheckoutPage}[iframe_credit_card]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_card_name]    ${card_name}
    SeleniumLibrary.Input Text    ${dictCheckoutPage}[txt_card_number]    ${card_number}
    SeleniumLibrary.Input Text    ${dictCheckoutPage}[txt_card_expiry_date]    ${card_expiry_date}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_card_cvv]    ${card_cvv}
    SeleniumLibrary.Unselect Frame

Proceed to check out and verify with Pay At Store for 2 Hour pickup
    Select Payment Method Pay at Store
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For Pay At Store Payment Method
    Wait Until Page Loader Is Not Visible
    Verify each section on Thank you page displays correctly

Proceed to check out and verify with Bank Transfer for 2 Hour pickup
    Select bank transfer option and input bank information
    Click Pay now Button
    Verify each section on payment slip page displays correctly
    Verify each section on Thank you page displays correctly

Input postcode then click arrow button
    [Arguments]    ${postcode}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_StoreNameOrLocation]    ${postcode}
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_postcode_arrow]
    Wait Until Page Is Completely Loaded

Verify same-day delivery option
    Run Keyword If    '${BU.lower()}'=='${bu_type.rbs}'    checkout_page.Verify same-day option should not display in shipping
    ...    ELSE    Verify same day delivery shipping price are display correctly

Verify next-day delivery option
    Run Keyword If    '${BU.lower()}'=='${bu_type.rbs}'    checkout_page.Verify Next-day option should not display in shipping
    ...    ELSE    Verify next day delivery shipping price are display correctly

Place pay at store order by api and login by ui
    [Arguments]    ${email}    ${password}    ${sku}    ${store_id}    ${coupon}=${EMPTY}
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${email}    ${password}    ${sku}    ${1}    ${store_view.name.en}    ${coupon}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${store_view.name.en}    ${api_ver3}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${store_id}    ${store_view.name.en}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.pay_at_store}    ${store_view.name.en}
    ${enity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.pay_at_store}    ${store_view.name.en}
    ${response}=    order_details.Get order details by entity_id    ${enity_id}
    ${increment_id}=    Get Value From Json    ${response}    $.increment_id
    ${is_open}=    common_keywords.Is Browser Openning
    Run Keyword If    ${is_open}==False    common_cds_web.Setup - Open browser
    Wait Until Page Is Completely Loaded
    Go To Direct Url    register
    Wait Until Page Is Completely Loaded
    registration_keywords.Login Account At Register Page    ${email}    ${password}
    Set Test Variable    ${oder_id}    ${increment_id}[0]
    Go To Direct Url    checkout/completed/${increment_id}[0]
    Wait Until Page Is Completely Loaded