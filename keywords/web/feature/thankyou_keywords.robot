*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Keywords ***
Verify each section on payment slip page displays correctly
    thankyou_page.Verify top message
    thankyou_page.Verify payment code
    thankyou_page.Verify money amount
    thankyou_page.Verify pay before
    thankyou_page.Verify bar code
    thankyou_page.Verify qr code
    Click continue button

Verify shipping address in thank you page is displayed correctly
    [Arguments]    ${shipping_type}
    thankyou_page.Verify element text is displayed correctly for shipping address    ${shipping_type}
    thankyou_page.Verify element text is displayed correctly for shipping address    ${guest_contact_info.firstname}    2
    thankyou_page.Verify element text is displayed correctly for shipping address    ${guest_contact_info.lastname}    2
    thankyou_page.Verify element text is displayed correctly for shipping address    ${shipping_address}    2
    thankyou_page.Verify element text is displayed correctly for shipping address    ${guest_contact_info.telephone}

Verify billing address in thank you page is displayed correctly for personal
    thankyou_page.Verify element text is displayed correctly for billing address    ${guest_contact_info.firstname}    3
    thankyou_page.Verify element text is displayed correctly for billing address    ${guest_contact_info.lastname}    3
    thankyou_page.Verify element text is displayed correctly for billing address    ${billing_address}
    thankyou_page.Verify element text is displayed correctly for billing address    ${guest_contact_info.id_card}

Verify billing address in thank you page is displayed correctly for company
    thankyou_page.Verify element text is displayed correctly for billing address    ${guest_contact_info.company_name}
    thankyou_page.Verify element text is displayed correctly for billing address    ${billing_address}
    thankyou_page.Verify element text is displayed correctly for billing address    ${guest_contact_info.tax_id}
    thankyou_page.Verify element text is displayed correctly for billing address    ${guest_contact_info.branch_id}

Verify shipping address in thank you page is dispayed consistency with MDC
    ${order_id}=    thankyou_page.Get order ID after completely order
    Get shipping info from order integration for guest    ${order_id}
    
    ${expected_customer_name}=    Set Variable    ${order_shipping_address.firstname}${SPACE}${order_shipping_address.lastname}
    ${expected_shipping_address}=    Set Variable    ${order_shipping_address.building},${SPACE}${order_shipping_address.address_no}
    ${expected_shipping_address}=    Set Variable    ${expected_shipping_address},${SPACE}${order_shipping_address.subdistrict},${SPACE}${order_shipping_address.district}
    ${expected_shipping_address}=    Set Variable    ${expected_shipping_address},${SPACE}${order_shipping_address.province}${SPACE}${order_shipping_address.postcode}

    Should Be Equal    ${order_shipping_address.address_type}    ${address_type_shipping}
    Should Be Equal    ${expected_customer_name}    ${guest_contact_info.firstname}${SPACE}${guest_contact_info.lastname}
    Should Be Equal    ${expected_shipping_address}    ${shipping_address}
    Should Be Equal    ${order_shipping_address.telephone}    ${guest_contact_info.telephone}

Verify billing address in thank you page is dispayed consistency with MDC
    [Arguments]     ${tax_type}
    ${order_id}=    thankyou_page.Get order ID after completely order
    Get billing info from order integration for guest    ${order_id}

    ${expected_customer_name}=    Set Variable    ${order_billing_address.firstname}${SPACE}${order_billing_address.lastname}
    ${expected_billing_address}=    Set Variable    ${order_billing_address.building},${SPACE}${order_billing_address.address_no}
    ${expected_billing_address}=    Set Variable    ${expected_billing_address},${SPACE}${order_billing_address.subdistrict},${SPACE}${order_billing_address.district}
    ${expected_billing_address}=    Set Variable    ${expected_billing_address},${SPACE}${order_billing_address.province}${SPACE}${order_billing_address.postcode}

    Should Be Equal    ${order_billing_address.address_type}    ${address_type_billing}
    Should Be Equal    ${expected_customer_name}    ${guest_contact_info.firstname}${SPACE}${guest_contact_info.lastname}
    Should Be Equal    ${expected_billing_address}    ${billing_address}

    Run Keyword If    "${tax_type}"=="${tax_type_personal}"    Should Be Equal    ${order_billing_address.vat_id}    ${guest_contact_info.id_card}
    Return From Keyword If    "${tax_type}"=="${tax_type_personal}"

    Should Be Equal    ${order_billing_address.company}    ${guest_contact_info.company_name}
    Should Be Equal    ${order_billing_address.vat_id}    ${guest_contact_info.tax_id}
    Should Be Equal    ${order_billing_address.branch_id}    ${guest_contact_info.branch_id}

Verify payment type is displayed consistency with selected
    [Arguments]    ${expected_type}
    ${actual_type}=    thankyou_page.Get payment type in Thank you page
    Should Be Equal    ${expected_type}    ${actual_type}

Verify payment status is displayed consistency with selected
    [Arguments]    ${expected_status}
    ${actual_status}=    thankyou_page.Get payment status in Thank you page
    Should Be Equal    ${expected_status}    ${actual_status}

Verify payment type is displayed consistency with new design 
    [Arguments]    ${expected_type}
    ${actual_type}=    thankyou_page.Get payment type in Thank you page with new design
    Should Be Equal    ${expected_type}    ${actual_type}

Verify payment status is displayed consistency with new design
    [Arguments]    ${expected_status}
    ${actual_status}=    thankyou_page.Get payment status in Thank you page with new design
    Should Be Equal    ${expected_status}    ${actual_status}

Verify registration section for guest checkout at thank you page
    [Arguments]    ${email}
    thankyou_page.Verify email personal details   ${email}
    thankyou_page.Verify password personal details
    thankyou_page.Verify button register for guest

Verify account registration for guest user successful
    [Arguments]    ${password}
    thankyou_page.Input password personal details for the registration    ${password}
    thankyou_page.Click button register for guest
    thankyou_page.Verify the succecssfull registration message

Verify Click and Collect section at thank you page
    Verify Click and Collect section
    Verify Click and Collect location

Verify each section on Thank you page displays correctly
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.web}'    Verify three icon status on thank you page
    Verify order information form thank you page
    Verify order date form thank you page
    Verify payment type

Add product to cart and go to the check out page
    [Arguments]    ${product_sku}
    product_page.Click on add to cart button    ${product_sku}
    Header Web - Click Mini Cart Button
    Header Web - Click View Cart Button
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible