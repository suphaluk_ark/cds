*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Keywords ***
Update new shipping address info
    [Arguments]    ${address_info}
    change_shipping_address_popup.Input address name to update shipping address    ${address_info}[address_name]
    change_shipping_address_popup.Input firstname to update shipping address    ${address_info}[firstname]
    change_shipping_address_popup.Input lastname to update shipping address    ${address_info}[lastname]
    change_shipping_address_popup.Input telephone to update shipping address    ${address_info}[telephone]
    change_shipping_address_popup.Input building to update shipping address    ${address_info}[building]
    change_shipping_address_popup.Input address no to update shipping address    ${address_info}[address_no]
    change_shipping_address_popup.Input postcode to update shipping address    ${address_info}[postcode]

Update shipping address to be the original one
    checkout_page.Click change shipping address link
    change_shipping_address_popup.Verify change shipping address popup should be visible
    change_shipping_address_popup.Click edit shipping address by address id    ${member_profile}[address_id]
    change_shipping_address_popup.Input address name to update shipping address    ${current_shipping_address}[address_name]
    change_shipping_address_popup.Input firstname to update shipping address    ${current_shipping_address}[firstname]
    change_shipping_address_popup.Input lastname to update shipping address    ${current_shipping_address}[lastname]
    change_shipping_address_popup.Input telephone to update shipping address    ${current_shipping_address}[telephone]
    change_shipping_address_popup.Input building to update shipping address    ${current_shipping_address}[building]
    change_shipping_address_popup.Input address no to update shipping address    ${current_shipping_address}[address_no]
    change_shipping_address_popup.Input postcode to update shipping address    ${current_shipping_address}[postcode]
    change_shipping_address_popup.Click save address button

Verify shipping address has been updated correctly
    [Arguments]    ${username}    ${password}
    common_cds_web.Get customer info with current shipping address from MDC api by customer member    ${username}    ${password}

    ${expected_shipping_customer_name}=    Set Variable    ${member_profile}[shipping_firstname]${SPACE}${member_profile}[shipping_lastname]
    ${expected_shipping_telephone}=    Set Variable    ${member_profile}[telephone]
    
    ${expected_shipping_address_name}=    Set Variable    ${member_profile}[address_name]
    ${expected_shipping_building}=    Set Variable    ${member_profile}[building]
    ${expected_shipping_address_no}=    Set Variable    ${member_profile}[address_no]
    ${expected_shipping_subdistrict}=    Set Variable    ${member_profile}[subdistrict_name]
    ${expected_shipping_district}=    Set Variable    ${member_profile}[district_name]
    ${expected_shipping_province}=    Set Variable    ${member_profile}[province]
    ${expected_shipping_postcode}=    Set Variable    ${member_profile}[postcode]
    
    ${actual_shipping_customer_name}=    change_shipping_address_popup.Get current shipping customer name
    ${actual_shipping_telephone}=    change_shipping_address_popup.Get current shipping telephone 
    ${actual_shipping_address_name}=    change_shipping_address_popup.Get current shipping address name
    ${actual_shipping_address_no_building}=    change_shipping_address_popup.Get current shipping building and address no 
    ${actual_shipping_address}=    change_shipping_address_popup.Get current shipping address 

    Should Be Equal    ${expected_shipping_customer_name}    ${actual_shipping_customer_name}
    Should Be Equal    ${expected_shipping_telephone}    ${actual_shipping_telephone}
    Should Be Equal    ${expected_shipping_address_name}    ${actual_shipping_address_name}
    Should Contain    ${actual_shipping_address_no_building}    ${expected_shipping_building}
    Should Contain    ${actual_shipping_address_no_building}    ${expected_shipping_address_no}
    Should Contain    ${actual_shipping_address}    ${expected_shipping_subdistrict}
    Should Contain    ${actual_shipping_address}    ${expected_shipping_district}
    Should Contain    ${actual_shipping_address}    ${expected_shipping_province}
    Should Contain    ${actual_shipping_address}    ${expected_shipping_postcode}


