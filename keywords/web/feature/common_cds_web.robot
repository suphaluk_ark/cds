*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Variables ***
${URL_CDS}    http://central:72KCm+Gk@staging.central.co.th
${personal_member}    personal_member
${guest_member}    guest_member
${status_in_transit}    in_transit
${status_delivered}    delivered
${tracking_no}    ROBOT_TRACKING

${member_profile}
${mdc_access_token}    %{MDC_ACCESS_TOKEN}
${key_mdc}    %{MDC_ACCESS_TOKEN}
*** Keywords ***
Setup - Open browser
    Run Keyword If    '${BU.lower()}'=='${bu_type.rbs}' and '${ENV.lower()}'=='${env_type.sit}'    CommonWebKeywords.Open Chrome Browser to page    ${rbs_url}    ${seleniumSpeed}    headless=${FALSE}    extension_full_path=${CURDIR}/../../../resources/rbs_extension/${ENV.lower()}    sleep_loading_extension=2
    ...    ELSE    CommonWebKeywords.Open Chrome Browser to page    ${central_url}    ${seleniumSpeed}    headless=${FALSE}    extension_full_path=${CURDIR}/../../../resources/cds_extension/${ENV.lower()}    sleep_loading_extension=2
    Run Keyword And Ignore Error    home_page_web_keywords.Close Popup
    Run Keyword And Ignore Error    common_keywords.Add Cookie For Disable Capcha
    Wait Until Page Is Completely Loaded
    Run Keyword If    '${BU.lower()}'=='${bu_type.rbs}'    common_keywords.SignIn Web Authentication

Scroll down to fetch data until element is contained in page
    [Documentation]    Use For Loop to scroll with height until element is fetched and contained in page, this is to support lazy load
    ...    pageHeight-(scrollLength*index)
    [Arguments]    ${locator}
    ${section}=    Set Variable    ${10}
    ${page_height}=    SeleniumLibrary.Get Element Size    ${dictHomepage}[lbl_homepage]
    ${page_height}=    Set Variable    ${page_height}[1]
    ${scroll_length}=    Evaluate    '${page_height}/${section}'
    FOR    ${index}    IN RANGE    ${section}      
        SeleniumLibrary.Execute Javascript    window.scrollTo(0, ${${page_height}-${${scroll_length}*${index}}})
        ${status}=    Run Keyword And Return Status    SeleniumLibrary.Wait Until Page Contains Element    ${locator}    timeout=10
        Return From Keyword If  '${status}' == '${true}'
    END
    Fail    This element cannot be found in this page.

Order status from mdc should be 'pending' status
    [Arguments]    ${increment_id}
    ${status}=    order_status_api.Get status from order detail response    ${increment_id}
    Log To Console    ${status}
    Should Be True    '${status}'=='pending' or '${status}'=='payment_authorized'

Order status from mdc should be 'pending payment' status
    [Arguments]    ${increment_id}
    ${status}=    order_status_api.Get status from order detail response    ${increment_id}
    Log To Console    ${status}
    Should Be Equal As Strings    ${status}    pending_payment

Verify order status and status reason from MDC and MCOM after pay by credit card
    [Arguments]    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    Run Keywords    common_cds_web.Order status from MCOM should be 'LOGISTIC' status    ${increment_id}
    ...    AND    common_cds_web.Order status reason from MCOM should be 'Ready to ship'    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    Run Keywords    common_cds_web.Order status from mdc should be 'LOGISTIC' status    ${increment_id}
    ...    AND    common_cds_web.Order status reason should be 'PENDING_FIRST_SHIPMENT_REQUEST'    ${increment_id}

Order status from mdc should be 'LOGISTIC' status
    [Arguments]    ${increment_id}
    ${status}=    order_status_api.Get status from order detail response    ${increment_id}
    Log To Console    ${status}
    Should Be Equal As Strings    ${status}    MCOM_LOGISTICS

Order status reason should be 'PENDING_FIRST_SHIPMENT_REQUEST'
    [Arguments]    ${increment_id}
    ${status_reason}=    order_status_api.Get status reason from order detail response    ${increment_id}
    Log To Console    ${status_reason}
    Should Be True    '${status_reason}'=='PENDING_FIRST_SHIPMENT_REQUEST' or '${status_reason}'=='READYTOSHIP'

Order status from MCOM should be 'LOGISTIC' status
    [Arguments]    ${increment_id}
    ${status}=    Get order status from MCOM    ${increment_id}
    Log To Console    ${status}
    Should Be Equal As Strings    ${status}    LOGISTICS

Order status reason from MCOM should be 'Ready to ship'
    [Arguments]    ${increment_id}
    ${status_reason}=    Get order status reason from MCOM    ${increment_id}
     Log To Console    ${status_reason}
    Should Be Equal As Strings    ${status_reason}    READYTOSHIP

Wait Until Order status from mdc change to 'LOGISTIC'
    [Arguments]    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status from mdc should be 'LOGISTIC' status    ${increment_id}

Click view cart button should not be visble
    Wait Until Keyword Succeeds    3 x    5 sec    header_web_keywords.Click view cart button should not be visible

Login to CDS and remove product in shopping cart
    [Arguments]    ${username}    ${password}    ${display_name}
    common_cds_web.Test Setup - Remove all product in shopping cart    ${username}    ${password}
    common_cds_web.Setup - Open browser
    login_keywords.Login Keywords - Login Success    ${username}    ${password}    ${display_name}
    Wait Until Page Is Completely Loaded

Order status from thank you page should be processing
    Wait Until Keyword Succeeds    30 x    10 sec    Run Keywords    Reload Page
    ...    AND    checkout_page.Order status from thank you page should be    ${thankyou_page.status_processing}

Search product, add product to cart, go to checkout page
    [Arguments]    ${product_sku}    ${quantity}=${1}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    Wait Until Page Loader Is Not Visible
    common_cds_web.Retry search product by product sku    ${product_sku}
    Retry add product to cart, product should be displayed in mini cart    ${product_sku}    ${quantity}
    Header Web - Click View Cart Button
    Run Keyword If    '${member_type}'=='guest_member' and '${ENV.lower()}'=='prod'    login_page.Click checkout as guest button
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Search product and add product to cart
    [Arguments]    ${product_sku}    ${quantity}=${1}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    Wait Until Page Loader Is Not Visible
    common_cds_web.Retry search product by product sku    ${product_sku}
    Retry add product to cart, product should be displayed in mini cart    ${product_sku}    ${quantity}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Retry search product by product sku
    [Arguments]    ${product_sku}
    Wait Until Keyword Succeeds    3x    1s    home_page_web_keywords.Search product by product sku    ${${product_sku}}[sku]

Retry add product to cart, product should be displayed in mini cart
    [Arguments]    ${product_sku}    ${quantity}=${1}
    Wait Until Keyword Succeeds    3x    1s    Add product to cart, product should be displayed in mini cart   ${product_sku}    ${quantity}

Add product to cart, product should be displayed in mini cart
    [Arguments]    ${product_sku}    ${quantity}=${1}
    product_page.Add product quantity    ${product_sku}    ${quantity}
    product_page.Click on add to cart button    ${product_sku}
    common_cds_web.Click view cart button should not be visble
    Header Web - Click Mini Cart Button
    header_web_keywords.Product should be displayed in mini cart    ${${product_sku}}[sku]    ${${product_sku}}[name]

Search and add product to wishlist
    [Arguments]  ${product_sku}
    home_page_web_keywords.Search product by product sku    ${${product_sku}}[sku]
    Wait Until Page Is Completely Loaded
    product_page.Click wishlist icon on product detail page  ${product_sku}


Check Out Product Until Payment With COD Method by Member Type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    checkout_keywords.Check Out Product Until Payment With COD Method
    ...    ELSE IF    '${member_type}' == 'guest_member'    checkout_keywords.Check Out Product Until Payment With COD Method For Guest Member

Proceed checkout until go to payment page (COD should not be visible)
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Input customer details
    checkout_page.Select shipping to address option
    Input shipping details
    Click on standard delivery button
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    checkout_page.COD payment method should not be visible

Checkout, select pick at store and pay by credit card by Member Type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    Checkout, select pick at store and pay by credit card    ${pick_at_store.central_bangna}
    ...    ELSE IF    '${member_type}' == 'guest_member'    Checkout, select pick at store and pay by credit card For Guest Member    ${pick_at_store.central_bangna}

Checkout, select pick at store and pay by 1 2 3 bank transfer by Member Type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    checkout_keywords.Checkout, select pick at store and pay by 1 2 3 bank transfer(check shipping charge)    ${pick_at_store.central_bangna}
    ...    ELSE IF    '${member_type}' == 'guest_member'    checkout_keywords.Checkout, select pick at store and pay by 1 2 3 bank transfer For Guest Member(check shipping charge)    ${pick_at_store.central_bangna}

Checkout, select pick at watson and pay by 1 2 3 bank transfer by Member Type
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    checkout_keywords.Checkout, select pick at watson and pay by 1 2 3 bank transfer(check shipping charge)    ${pick_at_watson.family_rama}    ${pick_at_watson.family_address}
    ...    ELSE IF    '${member_type}' == 'guest_member'    checkout_keywords.Checkout, select pick at watson and pay by 1 2 3 bank transfer For Guest Member(check shipping charge)    ${pick_at_watson.family_rama}    ${pick_at_watson.family_address}

Checkout, select pick at watson and pay by 1 2 3 bank transfer by Member Type(no-shipping charge)
    [Arguments]    ${member_type}
    Run Keyword If    '${member_type}' == 'personal_member'    checkout_keywords.Checkout, select pick at watson and pay by 1 2 3 bank transfer    ${pick_at_watson.family_rama}
    ...    ELSE IF    '${member_type}' == 'guest_member'    checkout_keywords.Checkout, select pick at watson and pay by 1 2 3 bank transfer For Guest Member    ${pick_at_watson.family_rama}

Checkout, select pick at watson
    [Arguments]    ${store_name}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Select pick at skybox and family mart    ${store_name}
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible

Checkout and go to payment page by member type
    [Arguments]    ${member_type}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Run Keyword If    '${member_type}' == 'guest_member'    Input customer details
    checkout_page.Select shipping to address option
    Run Keyword If    '${member_type}' == 'guest_member'    Input shipping details
    Run Keyword If    '${member_type}' == 'guest_member'    Click on standard delivery button
    Run Keyword If    '${member_type}' == 'personal_member'    checkout_page.Select default address
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed


Select ship to standard and go to payment page
    [Arguments]    ${member_type}
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Run Keyword If    '${member_type}' == 'guest_member'    Input customer details
    checkout_page.Select shipping to address option
    Run Keyword If    '${member_type}' == 'guest_member'    Input shipping details
    Run Keyword If    '${member_type}' == 'guest_member'    Click on standard delivery button
    Run Keyword If    '${member_type}' == 'personal_member'    checkout_page.Select default address
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed

Order status from MCOM should be 'ONHOLD' status
    [Arguments]    ${increment_id}
    ${status}=    Get order status from MCOM    ${increment_id}
    Log To Console    ${status}
    Should Be Equal As Strings    ${status}    ONHOLD

Order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${increment_id}
    ${status_reason}=    order_status_api.Get status reason from order detail response    ${increment_id}
    Log To Console    ${status_reason}
    Should Be Equal As Strings    ${status_reason}    PENDING_VALIDATE_PAYMENT_REPLY

Create shipment by order id
    [Arguments]    ${order_id}
    ${entity_id}=    Get enity_id by order id    ${order_id}
    ${items_id}=    Get items id by order id    ${order_id}
    Set Test Variable    ${items_id}    ${items_id}
    Set Test Variable    ${entity_id}    ${entity_id}
    FOR    ${item_id}    IN    @{items_id}
        Create shipment by enity id    ${entity_id}    ${item_id}
    END


Update order delivery status to in transit
    [Arguments]    ${order_no}    ${product_sku}
    FOR    ${item_id}    IN    @{items_id}
        Update order delivery status    ${order_no}    ${item_id}    ${product_sku}    ${status_in_transit}    ${tracking_no}
    END

Update order delivery status to delivered
    [Arguments]    ${order_no}    ${product_sku}
    FOR    ${item_id}    IN    @{items_id}
        Update order delivery status    ${order_no}    ${item_id}    ${product_sku}    ${status_delivered}    ${tracking_no}
    END

Order status on progress bar should be pending payment status
    Reload Page
    Wait Until Page Loader Is Not Visible
    Order status on progress bar should be displayed correctly    ${order_history}[pending_payment]

Order status on progress bar should be processing status
    Reload Page
    Wait Until Page Loader Is Not Visible
    Order status on progress bar should be displayed correctly    ${order_history}[status_processing]

Order status on progress bar should be shipping status
    Reload Page
    Wait Until Page Loader Is Not Visible
    Order status on progress bar should be displayed correctly    ${order_history}[status_shipping]

Order status on progress bar should be complete status
    Reload Page
    Wait Until Page Loader Is Not Visible
    Order status on progress bar should be displayed correctly    ${order_history}[status_complete]

Guest member search order number
    [Arguments]    ${email}    ${order_no}
    Wait Until Page Is Completely Loaded
    Click order tracking button
    Input E-mail into tracking pop-up    ${email}
    Input order number into tracking pop-up    ${order_no}
    order_tracking_page.Click Track order button

Setup - Login successfully, create product data file and get customer profile
     [Arguments]    ${username}    ${password}    ${display_name}
     Get customer info with current shipping address from MDC api by customer member    ${username}    ${password}
     Run Keyword And Ignore Error     customer_details.Remove customer cart and generate new cart    ${username}    ${password}
     common_cds_web.Setup - Open browser
     login_keywords.Login Keywords - Login Success    ${username}    ${password}    ${display_name}
     common_keywords.Wait Until Page Is Completely Loaded

Setup - For guest, create product data file and get customer profile
    common_cds_web.Setup - Open browser
    Wait Until Page Is Completely Loaded

Suite Setup - Open browser and load product data file
    common_cds_web.Setup - Open browser

Test Setup - Remove all product in shopping cart
    [Arguments]    ${username}    ${password}
    Run Keyword And Ignore Error     customer_details.Remove customer cart and generate new cart    ${username}    ${password}
    Set api root URL for CDS

Get customer info with current shipping address from MDC api by customer member
    [Arguments]    ${username}    ${password}
    ${user_token}=    authentication.Get login token    ${username}    ${password}
    ${response}=    customer_details.Get customer details    ${user_token}

    ${firstname}=    REST.Output    $.firstname
    ${lastname}=    REST.Output    $.lastname
    ${email}=    REST.Output    $.email
    ${telephone}=    Run Keyword And Ignore Error    REST.Output    $..addresses[?(@.default_shipping == true)].telephone
    ${telephone}=    Set Variable If    'FAIL' == '${telephone[0]}'    ${no_data}    ${telephone[1]}


    ${shipping_firstname}=    Run Keyword And Ignore Error    REST.Output    $..addresses[?(@.default_shipping == true)].firstname
    Return From Keyword If    'FAIL' == '${shipping_firstname[0]}'
    ${shipping_firstname}=    Set Variable    ${shipping_firstname[1]}

    ${shipping_lastname}=    REST.Output    $..addresses[?(@.default_shipping == true)].lastname

    ${address_name}=    Run Keyword And Ignore Error    REST.Output    $..addresses[?(@.default_shipping == true)].custom_attributes[?(@.attribute_code=='address_name')].value
    ${address_name}=    Set Variable If    'FAIL' == '${address_name[0]}'    ${EMPTY}    ${address_name[1]}

    ${address_id}=    REST.Output    $..addresses[?(@.default_shipping == true)].id
    ${address_no}=    Run Keyword And Ignore Error    REST.Output    $..addresses[?(@.default_shipping == true)].custom_attributes[?(@.attribute_code=='address_line')].value
    ${address_no}=    Set Variable If    'FAIL' == '${address_no[0]}'    ${EMPTY}    ${address_no[1]}

    ${subdistrict_id}=    REST.Output    $..addresses[?(@.default_shipping == true)].custom_attributes[?(@.attribute_code=='subdistrict_id')].value
    ${subdistrict_name}=    Set Variable    ${subdistrict}[subdistrict_id_${subdistrict_id}]

    ${district_id}=    REST.Output    $..addresses[?(@.default_shipping == true)].custom_attributes[?(@.attribute_code=='district_id')].value
    ${district_name}=    Set Variable    ${district}[district_id_${district_id}]

    ${region_code}=    REST.Output    $..addresses[?(@.default_shipping == true)].region.region_code
    ${province}=    Set Variable    ${region}[${region_code}]

    ${postcode}=    REST.Output    $..addresses[?(@.default_shipping == true)].postcode

    ${building}=    Run Keyword And Ignore Error    REST.Output    $..addresses[?(@.default_shipping == true)].custom_attributes[?(@.attribute_code=='building')].value
    ${building}=    Set Variable If    'FAIL' == '${building[0]}'    ${EMPTY}    ${building[1]}

    ${shipping_address}=    Set Variable    ${subdistrict_name},${SPACE}${district_name},${SPACE}${province}${SPACE}${postcode}

    ${customer_info}=    Create Dictionary    firstname=${firstname}    lastname=${lastname}
    ...    email=${email}    telephone=${telephone}    shipping_firstname=${shipping_firstname}
    ...    shipping_lastname=${shipping_lastname}    address_name=${address_name}
    ...    address_id=${address_id}    address_no=${address_no}    shipping_address=${shipping_address}
    ...    building=${building}    subdistrict_name=${subdistrict_name}    district_name=${district_name}
    ...    province=${province}    postcode=${postcode}

    Set Global Variable    ${member_profile}    ${customer_info}

Convert price to string format
    [Arguments]    ${price}
    ${price}=     Evaluate    math.trunc(${price})    modules=math
    ${price}=     Evaluate    "{:,}".format(${price})
    ${price}=     Convert To String    ${price}
    [Return]    ${price}

Get customer tax invoice address from MDC
    [Arguments]    ${username}    ${password}
    ${user_token}=    authentication.Get login token    ${username}    ${password}
    ${response}=    customer_details.Get customer details    ${user_token}

    ${firstname}=    Run Keyword And Ignore Error    REST.Output    $..addresses[?(@.default_billing == true)].firstname
    Return From Keyword If    'FAIL' == '${firstname[0]}'    ${checkout_page}[no_tax_invoice_address]
    ${firstname}=    Set Variable    ${firstname[1]}

    ${lastname}=    REST.Output    $..addresses[?(@.default_billing == true)].lastname

    ${address_name}=    Run Keyword And Ignore Error    REST.Output    $..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code=='address_name')].value
    ${address_name}=    Set Variable If    'FAIL' == '${address_name[0]}'    ${EMPTY}    ${address_name[1]}

    ${address_id}=    REST.Output    $..addresses[?(@.default_billing == true)].id
    ${address_no}=    REST.Output    $..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code=='address_line')].value

    ${subdistrict_id}=    REST.Output    $..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code=='subdistrict_id')].value
    ${subdistrict_name}=    Set Variable    ${subdistrict}[subdistrict_id_${subdistrict_id}]

    ${district_id}=    REST.Output    $..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code=='district_id')].value
    ${district_name}=    Set Variable    ${district}[district_id_${district_id}]

    ${region_code}=    REST.Output    $..addresses[?(@.default_billing == true)].region.region_code
    ${province}=    Set Variable    ${region}[${region_code}]

    ${postcode}=    REST.Output    $..addresses[?(@.default_billing == true)].postcode

    ${building}=    REST.Output    $..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code=='building')].value

    ${telephone}=    REST.Output    $..addresses[?(@.default_billing == true)].telephone

    ${full_address}=    Set Variable    ${subdistrict_name},${SPACE}${district_name},${SPACE}${province}${SPACE}${postcode}

    ${full_tax_type}=    REST.Output     $..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code=='full_tax_type')].value

    ${vat_id}=    Run Keyword And Ignore Error    REST.Output    $..addresses[?(@.default_billing == true)].vat_id
    ${vat_id}=    Set Variable If    'FAIL' == '${vat_id[0]}'    ${EMPTY}    ${vat_id[1]}

    ${invoice_info}=    Create Dictionary    firstname=${firstname}    lastname=${lastname}
    ...    telephone=${telephone}    firstname=${firstname}    lastname=${lastname}    address_name=${address_name}
    ...    address_id=${address_id}    address_no=${address_no}    full_address=${full_address}
    ...    building=${building}    subdistrict_name=${subdistrict_name}    district_name=${district_name}
    ...    province=${province}    postcode=${postcode}    full_tax_type=${full_tax_type}    vat_id=${vat_id}
    Set Global Variable    ${tax_invoice_info}    ${invoice_info}

    Return From Keyword If    "${full_tax_type}"=="${tax_type_personal}"

    ${branch_id}=    Run Keyword And Ignore Error    REST.Output     $..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code=='branch_id')].value
    ${branch_id}=    Set Variable If    'FAIL' == '${branch_id[0]}'    ${EMPTY}    ${branch_id[1]}
    ${company_name}=    REST.Output    $..addresses[?(@.default_billing == true)].company

    Set To Dictionary    ${invoice_info}    branch_id=${branch_id}    company_name=${company_name}
    Set Global Variable    ${tax_invoice_info}    ${invoice_info}

Verify element is displayed wihtout any default value
    [Arguments]    ${locator}
    CommonWebKeywords.Verify Web Elements Are Visible    ${locator}
    ${value}=    Get Value    ${locator}
    Should Be Equal    ${value}    ${EMPTY}

Get label text by element
    [Arguments]    ${locator}
    CommonWebKeywords.Verify Web Elements Are Visible    ${locator}
    ${value}=    SeleniumLibrary.Get Text    ${locator}
    [Return]    ${value}

Get shipping info from order integration for guest
    [Arguments]    ${order_id}
    Get api root URL for CDS
    ${response}=    order_details.Get order details integration by order id    ${order_id}

    ${address_type}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='address_type')].value

    ${firstname}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='firstname')].value
    ${lastname}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='lastname')].value
    ${telephone}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='telephone')].value
    ${building}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='building')].value
    ${address_no}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='address_line')].value
    ${subdistrict}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='subdistrict')].value
    ${district}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='district')].value
    ${province}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='region')].value
    ${postcode}=    REST.Output    $..shipping.address.extension_attributes.custom_attributes[?(@.attribute_code=='postcode')].value

    ${order_shipping_address}=    Create Dictionary    address_type=${address_type}
    ...    firstname=${firstname}    lastname=${lastname}    telephone=${telephone}    building=${building}
    ...    address_no=${address_no}    subdistrict=${subdistrict}    district=${district}    province=${province}
    ...    postcode=${postcode}

    Set Global Variable    ${order_shipping_address}    ${order_shipping_address}
    Set api root URL for CDS


Get billing info from order integration for guest
    [Arguments]    ${order_id}
    Get api root URL for CDS
    ${response}=    order_details.Get order details integration by order id    ${order_id}

    ${tax_type}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='full_tax_type')].value
    ${address_type}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='address_type')].value

    ${firstname}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='firstname')].value
    ${lastname}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='lastname')].value
    ${telephone}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='telephone')].value
    ${building}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='building')].value
    ${address_no}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='address_line')].value
    ${subdistrict}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='subdistrict')].value
    ${district}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='district')].value
    ${province}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='region')].value
    ${postcode}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='postcode')].value

    ${vat_id}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='vat_id')].value
    ${company}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='company')].value
    ${branch_id}=    REST.Output    $..billing_address.extension_attributes.custom_attributes[?(@.attribute_code=='branch_id')].value

    ${billing_address}=    Create Dictionary    address_type=${address_type}
    ...    firstname=${firstname}    lastname=${lastname}    telephone=${telephone}    building=${building}
    ...    address_no=${address_no}    subdistrict=${subdistrict}    district=${district}    province=${province}
    ...    postcode=${postcode}    vat_id=${vat_id}    company=${company}    branch_id=${branch_id}

    Set Global Variable    ${order_billing_address}    ${billing_address}
    Set api root URL for CDS

Get customer address id list from MDC
    [Arguments]    ${username}    ${password}
    ${user_token}=    authentication.Get login token    ${username}    ${password}
    ${response}=    customer_details.Get customer details    ${user_token}

    ${id_list}=    REST.Output    $..id
    ${id_length}=    Get Length    ${id_list}

    @{address_id_list}=    Create List
    FOR   ${index}   IN RANGE   ${id_length}
        Continue For Loop If    ${index}==0
        ${address_id}=    REST.Output    $..addresses[?(@.id == ${id_list[${index}]})].id
        ${address_type}=    REST.Output    $..addresses[?(@.id == ${id_list[${index}]})].custom_attributes[?(@.attribute_code=='customer_address_type')].value
        ${address_dict}=    Create Dictionary    address_id=${address_id}    address_type=${address_type}
        Append To List    ${address_id_list}    ${address_dict}
    END
    [Return]    ${address_id_list}

Search product and add multiple products to cart
    [Arguments]    ${dictProducts}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    FOR  ${product_sku}  IN  @{dictProducts}
        home_page_web_keywords.Search product by product sku    ${${product_sku}}[sku]
        Add product quantity  ${product_sku}    ${dictProducts['${product_sku}']}
        product_page.Click on add to cart button    ${product_sku}
        common_cds_web.Click view cart button should not be visble
        Header Web - Click Mini Cart Button
        header_web_keywords.Product should be displayed in mini cart    ${${product_sku}}[sku]    ${${product_sku}}[name]
    END
    Wait Until Page Is Completely Loaded
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Get api root URL for CDS
    Set Global Variable    ${original_api_root}    ${api_root}
    Set Global Variable    ${api_root}    ${api_root_copy}

Set api root URL for CDS
    Set Global Variable    ${api_root}    ${original_api_root}

Verify order status from MCOM should be 'COMPLETE, FULLYCANCELLED'
    [Arguments]     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status from MCOM should be 'COMPLETE' status     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status reason from MCOM should be 'FULLYCANCELLED'     ${order_no}

Verify order status from MDC should be 'COMPLETE, FULLYCANCELLED'
    [Arguments]     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status from mdc should be 'COMPLETE' status     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status reason from mdc should be 'FULLYCANCELLED'     ${order_no}

Verify order status from MCOM should be 'COMPLETE, FULLYSHIPPED'
    [Arguments]     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status from MCOM should be 'COMPLETE' status     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status reason from MCOM should be 'FULLYSHIPPED'     ${order_no}

Verify order status from MDC should be 'COMPLETE, FULLYSHIPPED'
    [Arguments]     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status from mdc should be 'COMPLETE' status     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status reason from mdc should be 'FULLYSHIPPED'     ${order_no}

Verify order status from MCOM should be 'COMPLETE, PARTIALLYSHIPPED'
    [Arguments]     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status from MCOM should be 'COMPLETE' status     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status reason from MCOM should be 'PARTIALLYSHIPPED'     ${order_no}

Verify order status from MDC should be 'COMPLETE, PARTIALLYSHIPPED'
    [Arguments]     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status from mdc should be 'COMPLETE' status     ${order_no}
    Wait Until Keyword Succeeds     30 x    10 sec    Order status reason from mdc should be 'PARTIALLYSHIPPED'     ${order_no}

Order status from MCOM should be 'COMPLETE' status
    [Arguments]     ${increment_id}
    ${status}=  Get order status from MCOM  ${increment_id}
    Log To Console  ${status}
    Should Be Equal As Strings  ${status}   COMPLETE

Order status reason from MCOM should be 'FULLYCANCELLED'
    [Arguments]     ${increment_id}
    ${status_reason}=   Get order status reason from MCOM   ${increment_id}
     Log To Console     ${status_reason}
    Should Be Equal As Strings  ${status_reason}    FULLYCANCELLED

Order status reason from MCOM should be 'FULLYSHIPPED'
    [Arguments]     ${increment_id}
    ${status_reason}=   Get order status reason from MCOM   ${increment_id}
     Log To Console     ${status_reason}
    Should Be Equal As Strings  ${status_reason}    FULLYSHIPPED

Order status reason from MCOM should be 'PARTIALLYSHIPPED'
    [Arguments]     ${increment_id}
    ${status_reason}=   Get order status reason from MCOM   ${increment_id}
     Log To Console     ${status_reason}
    Should Be Equal As Strings  ${status_reason}    PARTIALLYSHIPPED

Order status from mdc should be 'COMPLETE' status
    [Arguments]     ${increment_id}
    ${status}=  order_status_api.Get status from order detail response   ${increment_id}
    Should Be True    '${status}'=='MCOM_COMPLETE' or '${status}'=='complete'

Order status reason from mdc should be 'FULLYCANCELLED'
    [Arguments]     ${increment_id}
    ${status_reason}=   order_status_api.Get status reason from order detail response    ${increment_id}
    Log To Console  ${status_reason}
    Should Be Equal As Strings  ${status_reason}    FULLYCANCELLED

Order status reason from mdc should be 'FULLYSHIPPED'
    [Arguments]     ${increment_id}
    ${status_reason}=   order_status_api.Get status reason from order detail response    ${increment_id}
    Log To Console  ${status_reason}
    Should Be Equal As Strings  ${status_reason}    FULLYSHIPPED

Order status reason from mdc should be 'PARTIALLYSHIPPED'
    [Arguments]     ${increment_id}
    ${status_reason}=   order_status_api.Get status reason from order detail response    ${increment_id}
    Log To Console  ${status_reason}
    Should Be Equal As Strings  ${status_reason}    PARTIALLYSHIPPED

Tear down with cancel order
    CommonWebKeywords.Test Teardown
    Run Keyword And Ignore Error    common_keywords.Force cancel order via MCOM directly

Remove text value
    ${search_value}=    Get Value    ${search_box}
    ${length}=    Get Length    ${search_value}
    Run Keyword If    ${length}!=0    CommonWebKeywords.Click Element    ${btn_x_on_search_box}

Switch to new window
    SeleniumLibrary.Switch Window    NEW

Switch to main window
    SeleniumLibrary.Switch Window    MAIN
    
Get customer details 'first name'
    [Arguments]    ${email}    ${pwd}
    ${user_token}=    authentication.Get login token    ${email}    ${pwd}
    ${response}=    customer_details.Get customer details    ${user_token}
    ${customer_first_name}=    Output    $.firstname
    [Return]    ${customer_first_name}

Get customer details 'last name'
    [Arguments]    ${email}    ${pwd}
    ${user_token}=    authentication.Get login token    ${email}    ${pwd}
    ${response}=    customer_details.Get customer details    ${user_token}
    ${customer_last_name}=    Output    $.lastname
    [Return]    ${customer_last_name}

Get customer details 'email'
    [Arguments]    ${email}    ${pwd}
    ${user_token}=    authentication.Get login token    ${email}    ${pwd}
    ${response}=    customer_details.Get customer details    ${user_token}
    ${customer_email}=    Output    $.email
    [Return]    ${customer_email}

Customer details 'email' from mdc should be equal 'login email'
    [Arguments]    ${email}    ${pwd}
    ${customer_email}=    Get customer details 'email'    ${email}    ${pwd}
    Should Be Equal As Strings    ${customer_email}    ${email}

Customer details 'first name and lastname' from mdc should be equal
    [Arguments]    ${email}    ${pwd}    ${expected_first_name}    ${expected_last_name}
    ${customer_first_name}=    Get customer details 'first name'    ${email}    ${pwd}
    Should Be Equal As Strings    ${customer_first_name}    ${expected_first_name}
    ${customer_last_name}=    Get customer details 'last name'    ${email}    ${pwd}
    Should Be Equal As Strings    ${customer_last_name}    ${expected_last_name}

Verify user on mdc authentication should be fail
    [Arguments]    ${email}    ${pwd}
    authentication.Get login token    ${email}    ${pwd}   401

Verify order status MCOM 'ONHOLD', order status reason MDC 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status from MCOM should be 'ONHOLD' status    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}

Verify order status and status reason from MDC and MCOM after pay by transfer
    [Arguments]     ${increment_id}
    Wait Until Keyword Succeeds     30 x    10 sec    common_cds_web.Order status from MCOM should be 'ONHOLD' status    ${increment_id}
    Wait Until Keyword Succeeds     30 x    10 sec    common_cds_web.Order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'     ${increment_id}

Send payment validated request to mcom
    [Documentation]     ${order_number} is increment id after perform bank transfer payment method
    [Arguments]     ${order_number}
    ${token_len}=    Run Keyword And Return Status    Get Length    ${mcom_api_key}
    ${access_token}=    Run Keyword If    not ${token_len}    McomAuthentication.Get Oauth Token    ${mcom_client_secret}
    ...    ELSE    Set Variable    ${mcom_api_key}
    &{header}=  Create Dictionary
    ...    Authorization=Bearer ${access_token}
    ...    Content-Type=${api_contentType_json}
    ${validate_bank_transfer}=     Get payment validated from MCOM    ${order_number}
    ${validate_bank_transfer}=    Remove String        ${validate_bank_transfer}    \n
    ${body}=  Set Variable    { "jsonrpc": "2.0", "id": 1, "method": "magento.payments.payments_management.notify_validated", "params": ${validate_bank_transfer} }
    &{response}=    REST.Post    ${mcom_api_url}/centralgroup/events
    ...    headers=${header}
    ...    body=${body}
    Integer  response status    200

Verify order status MCOM 'COMPLETE, PARTIALLYSHIPPED', MDC 'COMPLETE, PARTIALLYSHIPPED'
    [Arguments]    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    Verify order status from MCOM should be 'COMPLETE, PARTIALLYSHIPPED'   ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    Verify order status from MDC should be 'COMPLETE, PARTIALLYSHIPPED'    ${increment_id}

Verify order status MCOM 'COMPLETE, FULLYCANCELLED', MDC 'COMPLETE, FULLYCANCELLED'
    [Arguments]    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    Verify order status from MCOM should be 'COMPLETE, FULLYCANCELLED'    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    Verify order status from MDC should be 'COMPLETE, FULLYCANCELLED'    ${increment_id}

Verify order create WMS, shipped, delivered, FMS for order payment completed by list of product
    [Arguments]    ${increment_id}    @{list_product_sku}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card     ${increment_id}
    Retry verify that order create on WMS success    ${increment_id}
    FOR    ${product_sku}    IN    @{list_product_sku}
       ESB update shipment status to shipped by sku    ${increment_id}    ${product_sku}
    END
    FOR    ${product_sku}    IN    @{list_product_sku}
       Wait Until Keyword Succeeds    10 x    5 sec    ESB update shipment status to delivered by sku    ${increment_id}    ${product_sku}
    END
    Verify order status from MCOM should be 'COMPLETE, FULLYSHIPPED'   ${increment_id}
    Verify order status from MDC should be 'COMPLETE, FULLYSHIPPED'    ${increment_id}
    Verify that order number exist in FMS DB    ${increment_id}

Retry verify that order create on WMS success
    [Arguments]    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    kibana_api.Verify that order create on WMS successfully by destination system    ${increment_id}

Get product urlkey by current url
    ${current_url}=    SeleniumLibrary.Get Location
    ${urlkey}    Get product urlkey    ${current_url}
    [Return]    ${urlkey}

Get product urlkey
    [Arguments]    ${url}
    ${urlkey}=    Evaluate    urllib.parse.urlparse('${url}')    urllib
    ${urlkey}=   String.Remove String    ${urlkey.path}    /${language}/
    [Return]    ${urlkey}

Go to Shop By Brand page
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    home_page_mobile_web_keywords.Click on hamburger menu
    home_page_web_keywords.Click on brand

Retry get random product by category id, search product, go to pdp page via api
    [Arguments]    ${category_id}    ${type_id}
    Wait Until Keyword Succeeds    3x    1s    Get random product by category id, search product, go to pdp page via api    ${category_id}    ${type_id}

Get random product by category id, search product, go to pdp page via api
    [Arguments]    ${category_id}    ${type_id}
    ${product}=    plp_api.Get random product by category id    ${category_id}    ${type_id}
    search_keyword.Search product for go to PDP page by product name    ${product}[name]

Search products, add products to cart, go to checkout page
    [Arguments]    ${order_total_type}    ${product_skus}    ${quantities}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    Wait Until Page Loader Is Not Visible
    Run Keyword If    isinstance($product_skus,list) and isinstance($quantities,list)    common_cds_web.Search and add multiple products to cart    ${product_skus}    ${quantities}
    ...    ELSE    common_cds_web.Search product and add product to cart    ${product_skus}    ${quantities}
    Verify total price of items for Pay at Store Payment Method    ${order_total_type}  
    Header Web - Click View Cart Button
    Run Keyword If    '${member_type}'=='guest_member' and '${ENV.lower()}'=='prod'    login_page.Click checkout as guest button
    Run Keyword And Ignore Error    Enable Testability Automatic Wait
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible  

Verify grand total for Pay at Store Payment Method
    [Arguments]    ${order_total_type}
    Run Keyword If    "${order_total_type}" == "${order_total.lower_minimum}"    checkout_page.Verify grand total is lower than minimum total for Pay at Store Payment Method    
    ...    ELSE IF    "${order_total_type}" == "${order_total.higher_maximum}"    checkout_page.Verify grand total is higher than maximum total for Pay at Store Payment Method  
    ...    ELSE    checkout_page.Verify grand total is valid total for Pay at Store Payment Method         