*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Keywords ***
Thank you page payment with credit card
    checkout_page.Select Payment Method With Credit Card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card

Verify shipping address is display on shipping summary correctly
    ${actual_customer_name}=    payment_page.Get shipping customer name
    ${actual_shipping_address}=    payment_page.Get shipping address
    ${actual_telephone}=    payment_page.Get shipping telephone

    ${expected_customer_name}=    Set Variable    ${guest_contact_info.firstname}${SPACE}${guest_contact_info.lastname}

    Should Be Equal    ${actual_customer_name}    ${expected_customer_name}
    Should Contain    ${actual_shipping_address}    ${guest_contact_info.shipping_building}
    Should Contain    ${actual_shipping_address}    ${guest_contact_info.shipping_address_no}
    Should Contain    ${actual_shipping_address}    ${shipping_subdistrict}
    Should Contain    ${actual_shipping_address}    ${shipping_district}
    Should Contain    ${actual_shipping_address}    ${shipping_province}
    Should Contain    ${actual_shipping_address}    ${guest_contact_info.shipping_postcode}
    Should Be Equal    ${actual_telephone}    ${guest_contact_info.telephone}

    ${full_shipping_address}=    Set Variable    ${guest_contact_info.shipping_building},${SPACE}${guest_contact_info.shipping_address_no}
    ${full_shipping_address}=    Set Variable    ${full_shipping_address},${SPACE}${shipping_subdistrict},${SPACE}${shipping_district}
    ${full_shipping_address}=    Set Variable    ${full_shipping_address},${SPACE}${shipping_province}${SPACE}${guest_contact_info.shipping_postcode}

    # Set Global Variable   ${shipping_customer_name}    ${expected_customer_name}
    Set Global Variable   ${shipping_address}    ${full_shipping_address}
    # Set Global Variable   ${shipping_telephone}    ${guest_contact_info.telephone}


Verify tax invoice address is display on shipping summary correctly
    ${actual_customer_name}=    payment_page.Get tax invoice customer name
    ${actual_billing_address}=    payment_page.Get tax invoice address
    ${actual_telephone}=    payment_page.Get tax invoice telephone

    ${expected_customer_name}=    Set Variable    ${guest_contact_info.firstname}${SPACE}${guest_contact_info.lastname}

    Should Be Equal    ${actual_customer_name}    ${expected_customer_name}
    Should Contain    ${actual_billing_address}    ${guest_contact_info.tax_invoice_building}
    Should Contain    ${actual_billing_address}    ${guest_contact_info.tax_invoice_address_no}
    Should Contain    ${actual_billing_address}    ${tax_invoice_subdistrict}
    Should Contain    ${actual_billing_address}    ${tax_invoice_district}
    Should Contain    ${actual_billing_address}    ${tax_invoice_province}
    Should Contain    ${actual_billing_address}    ${guest_contact_info.tax_invoice_postcode}
    Should Be Equal    ${actual_telephone}    ${guest_contact_info.telephone}

    ${full_billing_address}=    Set Variable    ${guest_contact_info.tax_invoice_building},${SPACE}${guest_contact_info.tax_invoice_address_no}
    ${full_billing_address}=    Set Variable    ${full_billing_address},${SPACE}${tax_invoice_subdistrict},${SPACE}${tax_invoice_district}
    ${full_billing_address}=    Set Variable    ${full_billing_address},${SPACE}${tax_invoice_province}${SPACE}${guest_contact_info.tax_invoice_postcode}

    # Set Global Variable   ${billing_customer_name}    ${expected_customer_name}
    Set Global Variable   ${billing_address}    ${full_billing_address}
    # Set Global Variable   ${billing_id_card}    ${guest_contact_info.id_card}

Confirm payment by pay at store
    payment_page.Select pay at store option
    payment_page.Click on confirm payment button for pay at store

Verify payment type and payment status in Thank you page after pay by pay at store
    thankyou_keywords.Verify payment type is displayed consistency with selected    ${thankyou_page.payment_pay_at_store}
    thankyou_keywords.Verify payment status is displayed consistency with selected    ${thankyou_page.status_pending}

Verify payment type and payment status in Thank you page after pay by pay at store with new design
    thankyou_keywords.Verify payment type is displayed consistency with new design     ${thankyou_page.payment_pay_at_store}
    thankyou_keywords.Verify payment status is displayed consistency with new design    ${thankyou_page.status_awaiting_payment}

Store payment status in Thank you page
    ${payment_status}=    thankyou_page.Get payment status in Thank you page with new design
    Set Test Variable    ${order_status}    ${payment_status}

Verify order status and status reason from MDC and MCOM after pay by pay at store
    Get order id from checkout page
    Verify order status and status reason from MDC and MCOM after pay by credit card    ${increment_id}

Verify payment method name
    [Arguments]    ${expected_name}
    ${actual_name}=    payment_page.Get payment method name
    Should Be Equal    ${expected_name}    ${actual_name}

Verify pay at store warning message
    ${actual_name}=    payment_page.Get pay at store warning message
    Should Be Equal    ${payment_page.pay_at_store_warning_msg}    ${actual_name}

Click edit bag link to go to shopping bag page and verify the page is displayed
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    payment_page.Click on view details on mobile
    checkout_page.Click edit bag link
    Wait Until Page Loader Is Not Visible

Verify product total after guest and member is merged cart after login successfully
    [Arguments]    ${product_total}
    payment_page.Verify input text T1C earn point is Visible
    checkout_page.Verify total of products should be displayed correctly at order summary on Delivery Details page    ${product_total}

