*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Keywords ***
Forgot Password Success
    [Arguments]    ${email}
    Header Web - Click Login on Header
    Header Web - Click Forgot Password on Header
    Input E-mail    ${email}
    Click Send E-mail Button
    # Send E-mail Success Message Should Be Visible

Forgot Password With Email
    [Arguments]    ${email}
    Header Web - Click Login on Header
    Header Web - Click Forgot Password on Header
    Input E-mail    ${email}
    Click Send E-mail Button
