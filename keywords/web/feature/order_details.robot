*** Setting ***
Resource    ${CURDIR}/../web_imports.robot


*** Keywords ***
Create order for verify order details with cod
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    login_keywords.Login Keywords - Login Success  ${login_username}  ${login_password}  ${login_display_name}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product, add product to cart, go to checkout page    ${non_cart_price_rule_sku.credit_card['item_1']}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method COD
    Wait Until Page Is Completely Loaded
    checkout_page.Click Confirm Order For COD Payment Method

Create order for verify order details with credit card
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    login_keywords.Login Keywords - Login Success  ${login_username}  ${login_password}  ${login_display_name}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product, add product to cart, go to checkout page    ${non_cart_price_rule_sku.credit_card['item_1']}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card

Create order for verify order details with pickup at store
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    login_keywords.Login Keywords - Login Success  ${login_username}  ${login_password}  ${login_display_name}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product, add product to cart, go to checkout page    ${non_cart_price_rule_sku.credit_card['item_1']}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    Select pick at store    ${pick_at_store.central_bangna}
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card

Create order for verify order details with credit card incomplete
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    login_keywords.Login Keywords - Login Success  ${login_username}  ${login_password}  ${login_display_name}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product, add product to cart, go to checkout page    ${non_cart_price_rule_sku.credit_card['item_1']}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    my_cart_page.Wait Until Page Loader Is Not Visible
    Input credit card
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Cancel payment credit card
    Wait Until Page Is Completely Loaded

Create multiple orders for verify order details with cod
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    login_keywords.Login Keywords - Login Success  ${login_username}  ${login_password}  ${login_display_name}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product, add product to cart, go to checkout page    ${non_cart_price_rule_sku.credit_card['item_1']}
    common_cds_web.Search product, add product to cart, go to checkout page    ${non_cart_price_rule_sku.credit_card['item_2']}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method COD
    Wait Until Page Is Completely Loaded
    checkout_page.Click Confirm Order For COD Payment Method

Create multiple orders for verify order details with credit card
    [Arguments]    ${login_username}    ${login_password}    ${login_display_name}
    login_keywords.Login Keywords - Login Success  ${login_username}  ${login_password}  ${login_display_name}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Remove all product in shopping cart
    common_cds_web.Search product, add product to cart, go to checkout page    ${non_cart_price_rule_sku.credit_card['item_1']}
    common_cds_web.Search product, add product to cart, go to checkout page    ${non_cart_price_rule_sku.credit_card['item_2']}
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select shipping to address option
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Click Pay now Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_keywords.Order successful with credit card

Verify tracking number and tracking message display in order details page
    Order_details_page.Verify tracking message in order details page display
    Order_details_page.Verify tracking number in order details page

Verify all of tracking number display in order details page
    Order_details_page.Verify tracking number 1 should display in order details page
    Order_details_page.Verify tracking number 2 should display in order details page

Verify partial of tracking number display in order details page
    Order_details_page.Verify tracking number 1 should display in order details page
    Order_details_page.Verify tracking number 2 should not display in order details page

Checkout order and pay by credit card for guest member
    my_cart_page.Click Secure Checkout Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Input customer details
    checkout_page.Select shipping to address option
    checkout_page.Input shipping details
    Wait Until Page Loader Is Not Visible 
    checkout_page.Click on standard delivery button
    Wait Until Page Is Completely Loaded
    checkout_page.Click Continue Payment Button
    my_cart_page.Wait Until Page Loader Is Not Visible
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Order successful with credit card
    
Guest verify tracking_id
    [Arguments]    ${order_id}
    order_tracking_page.Click on Tracking your orders
    Wait Until Page Is Completely Loaded
    order_tracking_page.Input email    ${guest_information}[email]
    order_tracking_page.Input Order number     ${order_id}
    Wait Until Page Is Completely Loaded
    header_web_keywords.Click track order button

Go to order detail page and verify order status
    [Arguments]    ${order_id}    ${order_status}
    Go To Direct Url    account/orders/${order_id}
    Wait Until Page Is Completely Loaded
    order_details_page.Verify order status at specific order detail page    ${order_status}