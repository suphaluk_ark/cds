*** Setting ***
Resource    ${CURDIR}/../web_imports.robot


*** Keywords ***
Verify there is no default tax invoice display if customer has no tax invoice information setting
    [Arguments]    ${username}    ${password}
    ${tax_invoice_status}=    Get customer tax invoice address from MDC    ${username}    ${password}
    ${actual_tax_invoice}=    delivery_details_page.Get default tax invoice address
    Should Be Equal    ${tax_invoice_status}    ${actual_tax_invoice}

Verify default tax invoice is displayed consistency with MDC
    [Arguments]    ${username}    ${password}    ${tax_type}
    common_cds_web.Get customer tax invoice address from MDC    ${username}    ${password}

    ${expected_invoice_customer_name}=    Set Variable    ${tax_invoice_info}[firstname]${SPACE}${tax_invoice_info}[lastname]
    ${expected_invoice_telephone}=    Set Variable    ${tax_invoice_info}[telephone]
    
    ${expected_invoice_address_name}=    Set Variable    ${tax_invoice_info}[address_name]
    ${expected_invoice_building}=    Set Variable    ${tax_invoice_info}[building]
    ${expected_invoice_address_no}=    Set Variable    ${tax_invoice_info}[address_no]
    ${expected_invoice_subdistrict}=    Set Variable    ${tax_invoice_info}[subdistrict_name]
    ${expected_invoice_district}=    Set Variable    ${tax_invoice_info}[district_name]
    ${expected_invoice_province}=    Set Variable    ${tax_invoice_info}[province]
    ${expected_invoice_postcode}=    Set Variable    ${tax_invoice_info}[postcode]
    ${expected_tax_type}=    Set Variable    ${tax_invoice_info}[full_tax_type]

    ${actual_invoice_address_name}=    delivery_details_page.Get tax invoice address name
    ${actual_invoice_customer_name}=    delivery_details_page.Get tax invoice customer name
    ${actual_invoice_telephone}=    delivery_details_page.Get tax invoice telephone
    ${actual_invoice_address_no_building}=    delivery_details_page.Get tax invoice address no and building
    ${actual_invoice_full_address}=    delivery_details_page.Get tax invoice full address

    Should Be Equal    ${expected_tax_type}    ${tax_type}
    Should Be Equal    ${expected_invoice_customer_name}    ${actual_invoice_customer_name}
    Should Be Equal    ${expected_invoice_telephone}    ${actual_invoice_telephone}
    Should Contain    ${actual_invoice_address_name}    ${expected_invoice_address_name}
    Should Contain    ${actual_invoice_address_no_building}    ${expected_invoice_building}
    Should Contain    ${actual_invoice_address_no_building}    ${expected_invoice_address_no}
    Should Contain    ${actual_invoice_full_address}    ${expected_invoice_subdistrict}
    Should Contain    ${actual_invoice_full_address}    ${expected_invoice_district}
    Should Contain    ${actual_invoice_full_address}    ${expected_invoice_province}
    Should Contain    ${actual_invoice_full_address}    ${expected_invoice_postcode}

    Run Keyword If    "${expected_tax_type}"=="${tax_type_personal}"    Verify tax invoice personal info is displayed consistency with MDC
    ...    ELSE    Verify tax invoice company info is displayed consistency with MDC

Verify tax invoice personal info is displayed consistency with MDC
    delivery_details_page.Click change tax invoice address link
    change_shipping_address_popup.Click edit shipping address by address id    ${tax_invoice_info}[address_id]
    change_shipping_address_popup.Verify tax invoice personal type is selected

    ${vat_id}=    change_shipping_address_popup.Get ID card for personal tax invoice
    Should Be Equal    ${vat_id}    ${tax_invoice_info}[vat_id]

Verify tax invoice company info is displayed consistency with MDC
    delivery_details_page.Click change tax invoice address link
    change_shipping_address_popup.Click edit shipping address by address id    ${tax_invoice_info}[address_id]
    change_shipping_address_popup.Verify tax invoice company type is selected

    ${company_name}=    change_shipping_address_popup.Get tax invoice company name
    ${vat_id}=    change_shipping_address_popup.Get tax ID for company tax invoice
    ${branch_id}=    change_shipping_address_popup.Get tax invoice branch id

    Should Be Equal    ${company_name}    ${tax_invoice_info}[company_name]
    Should Be Equal    ${vat_id}    ${tax_invoice_info}[vat_id]
    Should Be Equal    ${branch_id}    ${tax_invoice_info}[branch_id]

Verify guest is no contact info display
    delivery_details_page.Firstname textbox is visible wihtout any default value
    delivery_details_page.Lastname textbox is visible wihtout any default value
    delivery_details_page.Email textbox is visible wihtout any default value
    delivery_details_page.Telephone textbox is visible wihtout any default value
    delivery_details_page.The1no textbox is visible wihtout any default value

Verify guest is no shipping address info display
    delivery_details_page.Shipping building textbox is visible wihtout any default value
    delivery_details_page.Shipping address no textbox is visible wihtout any default value
    delivery_details_page.Shipping postcode textbox is visible wihtout any default value

Verify guest is no tax invoice address info display
    delivery_details_page.Tax ID card textbox is visible wihtout any default value
    delivery_details_page.Tax building textbox is visible wihtout any default value
    delivery_details_page.Tax address no textbox is visible wihtout any default value
    delivery_details_page.Tax postcode textbox is visible wihtout any default value

Input cutomer contact information
    delivery_details_page.Input firstname for customer info    ${guest_contact_info}[firstname]
    delivery_details_page.Input lastname for customer info    ${guest_contact_info}[lastname]
    delivery_details_page.Input email for customer info    ${guest_contact_info}[email]
    delivery_details_page.Input telephone for customer info    ${guest_contact_info}[telephone]

Input shipping address information
    delivery_details_page.Input building for shipping info    ${guest_contact_info}[shipping_building]
    delivery_details_page.Input address no for shipping info    ${guest_contact_info}[shipping_address_no]
    delivery_details_page.Input postcode for shipping info    ${guest_contact_info}[shipping_postcode]

    delivery_details_page.Get shipping district selected value
    delivery_details_page.Get shipping subdistrict selected value
    delivery_details_page.Get shipping province selected value


Input tax invoice address information
    [Arguments]    ${tax_type}
    delivery_details_page.Input building for tax invoice info    ${guest_contact_info}[tax_invoice_building]
    delivery_details_page.Input address no for tax invoice info    ${guest_contact_info}[tax_invoice_address_no]
    delivery_details_page.Input postcode for tax invoice info    ${guest_contact_info}[tax_invoice_postcode]

    delivery_details_page.Get tax invoice subdistrict selected value
    delivery_details_page.Get tax invoice district selected value
    delivery_details_page.Get tax invoice province selected value

    Run Keyword If    "${tax_type}"=="${tax_type_personal}"    delivery_details_page.Input ID card for tax invoice info  ${guest_contact_info}[id_card]
    Return From Keyword If    "${tax_type}"=="${tax_type_personal}"
    
    delivery_details_page.Input company name for tax invoice info    ${guest_contact_info}[company_name]
    delivery_details_page.Input tax ID for tax invoice info    ${guest_contact_info}[tax_id]
    delivery_details_page.Input branch ID for tax invoice info    ${guest_contact_info}[branch_id]

Update tax invoice address information
    [Arguments]    ${tax_type}
    delivery_details_page.Input building for tax invoice info    ${guest_contact_info_update}[tax_invoice_building]
    delivery_details_page.Input address no for tax invoice info    ${guest_contact_info_update}[tax_invoice_address_no]
    delivery_details_page.Input postcode for tax invoice info    ${guest_contact_info_update}[tax_invoice_postcode]

    delivery_details_page.Get tax invoice subdistrict selected value
    delivery_details_page.Get tax invoice district selected value
    delivery_details_page.Get tax invoice province selected value

    Run Keyword If    "${tax_type}"=="${tax_type_personal}"    delivery_details_page.Input ID card for tax invoice info  ${guest_contact_info}[id_card]
    Return From Keyword If    "${tax_type}"=="${tax_type_personal}"
    
    delivery_details_page.Input company name for tax invoice info    ${guest_contact_info}[company_name]
    delivery_details_page.Input tax ID for tax invoice info    ${guest_contact_info}[tax_id]
    delivery_details_page.Input branch ID for tax invoice info    ${guest_contact_info}[branch_id]

Verify the active stores displayed
    [Arguments]    ${estimate_response}    ${api_version}
    ${json_expression}=    Set Variable If    '${api_version}'=='${api_ver3}'    $..extension_attributes.pickup_stores_location[?(@.is_active==true)].name    $..pickup_locations[?(@.id)].name
    ${names}=    JSONLibrary.Get Value From Json    ${estimate_response}    ${json_expression}
    ${api_names}=    Convert To List    ${names}
    ${ui_names}=    Get list active stores
    ${len_ui}=    Get Length    ${ui_names}
    ${len_api}=    Get Length    ${api_names}
    Should Be Equal As Integers    ${len_ui}    ${len_api}

Click active store and return store data
    [Arguments]    ${estimate_response}    ${api_version}
    ${store_name}=    delivery_details_page.Click random the active store and return name
    Should Not Be True    '${store_name}'=='${None}'    msg=Please re-check the store list on UI
    ${json_expression}=    Set Variable If    '${api_version}'=='${api_ver3}'    $..extension_attributes.pickup_stores_location[?(@.name=='${store_name}')].extension_attributes    $..pickup_locations[?(@.name=='${store_name}')].opening_hours
    ${store_json_data}=    JSONLibrary.Get Value From Json    ${estimate_response}    ${json_expression}
    [Return]    ${store_json_data}

Click 2hr active store and return store data
    [Arguments]    ${estimate_response}    ${api_version}
    ${store_name}=    delivery_details_page.Click random the 2hr pickup store and return name
    Should Not Be True    '${store_name}'=='${None}'    msg=Please re-check the store list on UI
    ${json_expression}=    Set Variable If    '${api_version}'=='${api_ver3}'    $..extension_attributes.pickup_stores_location[?(@.name=='${store_name}')].extension_attributes    $..pickup_locations[?(@.name=='${store_name}')].opening_hours
    ${store_json_data}=    JSONLibrary.Get Value From Json    ${estimate_response}    ${json_expression}
    [Return]    ${store_json_data}

Click standard pickup active store and return store data
    [Arguments]    ${estimate_response}    ${api_version}
    ${store_name}=    delivery_details_page.Click random the standard pickup store and return name
    Should Not Be True    '${store_name}'=='${None}'    msg=Please re-check the store list on UI
    ${json_expression}=    Set Variable If    '${api_version}'=='${api_ver3}'    $..extension_attributes.pickup_stores_location[?(@.name=='${store_name}')].extension_attributes    $..pickup_locations[?(@.name=='${store_name}')].opening_hours
    ${store_json_data}=    JSONLibrary.Get Value From Json    ${estimate_response}    ${json_expression}
    [Return]    ${store_json_data}

Verify the store data display correctly with show 2hr pickup flow
    [Arguments]    ${store_json_data}    ${quantity}
    Verify having two package delivery methods displayed
    Verify the 2 hour or next day pickup at the top and standard pickup at the bottom for split order only
    Verify label free text display correctly with 2 hour pickup or next day pickup
    Verify label free text display correctly with standard pickup    ${FALSE}
    Verify the product quantity displayed correctly on 2hr pickup    ${quantity}
    Verify the product quantity displayed correctly on standard pickup    ${quantity}    ${FALSE}
    Verify the uptime of current store    ${store_json_data}
    Verify next day or 2 hour pickup title displayed correctly    ${store_json_data}
    Verify should be clickable on both with show 2hr pickup flow

Verify the store data display correctly without 2hr pickup flow
    [Arguments]    ${store_json_data}    ${quantity}
    Verify having only one package delivery methods displayed
    Verify the 2 hour or next day pickup is disable
    Verify the product quantity displayed correctly on standard pickup    ${quantity}    ${FALSE}
    Verify the uptime of current store    ${store_json_data}
    Verify standard pickup title displayed correctly
    Verify should be clickable on standard pickup and 2hr pickup is not visible

Verify the store data display correctly with standard pickup of split order
    [Arguments]    ${store_json_data}    ${quantity}
    Verify having only one package delivery methods displayed
    Verify the 2 hour or next day pickup is disable
    Verify label free text display correctly with standard pickup    ${FALSE}
    Verify the product quantity displayed correctly on standard pickup of split order   ${quantity}    ${FALSE}
    Verify the uptime of current store    ${store_json_data}
    Verify standard pickup title displayed correctly
    Verify should be clickable on standard pickup and 2hr pickup is not visible

Verify the store data display correctly with sub package of split order flow
    [Arguments]    ${store_json_data}    ${quantity}
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    CommonWebKeywords.Scroll to bottom page
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lbl_2_hour_header]
    Wait Until Page Is Completely Loaded
    Verify having two sub package delivery methods displayed
    Verify the standard pickup at the top and standard delivery at the bottom for split order only
    Verify label free text display correctly with standard pickup    ${TRUE}
    Verify label free text display correctly with home delivery
    Verify the product quantity displayed correctly on standard pickup    ${quantity}    ${TRUE}
    Verify the product quantity displayed correctly on standard delivery    ${quantity}    ${TRUE}
    Verify the sub standard pickup title displayed correctly
    Verify the sub standard delivery title displayed correctly
    Verify should be clickable on both sub package of split order

Login, add product to cart, return and go to checkout delivery page with 2hr pickup
    [Arguments]    ${email}    ${password}    ${skus}    ${quantities}    ${pickup_id}    ${store}
    ${user_token}    ${quote_id}    common_keywords.Add skus to cart and return user token    ${email}    ${password}    ${store}    ${quantities}    ${skus}
    ${response}=    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${store}    ${api_ver3}
    common_keywords.Go To Specific Page By Member    checkout    ${email}    ${password}
    [Return]    ${response}    ${user_token}

Login, add product to cart, return and go to checkout delivery page with standard pickup
    [Arguments]    ${email}    ${password}    ${skus}    ${quantities}    ${pickup_id}    ${store}
    ${user_token}    ${quote_id}    common_keywords.Add skus to cart and return user token    ${email}    ${password}    ${store}    ${quantities}    ${skus}
    ${response}=    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${store}    ${api_ver3}
    common_keywords.Go To Specific Page By Member    checkout    ${email}    ${password}
    [Return]    ${response}    ${user_token}

Login, add product to cart, return and go to checkout delivery page with split order
    [Arguments]    ${email}    ${password}    ${skus}    ${quantities}    ${pickup_id}    ${store}
    ${user_token}    ${quote_id}    common_keywords.Add skus to cart and return user token    ${email}    ${password}    ${store}    ${quantities}    ${skus}
    ${response}=    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${store}    ${api_ver3}
    common_keywords.Go To Specific Page By Member    checkout    ${email}    ${password}
    [Return]    ${response}    ${user_token}

Login, add product to cart, return and go to cart page with split order
    [Arguments]    ${email}    ${password}    ${skus}    ${quantities}    ${pickup_id}    ${store}
    ${user_token}    ${quote_id}    common_keywords.Add skus to cart and return user token    ${email}    ${password}    ${store}    ${quantities}    ${skus}
    ${response}=    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${store}    ${api_ver3}
    common_keywords.Go To Specific Page By Member    cart    ${email}    ${password}
    [Return]    ${response}    ${user_token}

Add product to cart and go to checkout delivery page with guest
    [Arguments]    ${skus}    ${quantities}    ${store}
    ${quote_id}=    common_keywords.Get Cookie Value From Browser    guest
    Guest Add Multi Skus To Cart    ${skus}    ${quantities}    ${quote_id}    ${store}
    checkout.Guest validate carriers and shippings by estimate shipping methods    ${quote_id}    ${shipping_methods.two_hour_pickup.carrier}    ${shipping_methods.two_hour_pickup.method}    ${BU}    ${shipping_address_en.region_id}    ${shipping_address_en.zip_code}    ${api_version}
    common_keywords.Go to Specific Page By Guest    checkout
    [Return]    ${quote_id}

Login, add product to cart and go to checkout delivery page without shipping method
    [Arguments]    ${email}    ${password}    ${skus}    ${quantities}    ${pickup_id}    ${store}
    ${user_token}    ${quote_id}    common_keywords.Add skus to cart and return user token    ${email}    ${password}    ${store}    ${quantities}    ${skus}
    common_keywords.Go To Specific Page By Member    checkout    ${email}    ${password}

Verify the store data display correctly with split order
    [Arguments]    ${store_json_data}    ${quantities}
    ${total_qty}=    Evaluate    sum([int(i) for i in $quantities])
    Verify the store data display correctly with show 2hr pickup flow    ${store_json_data}    ${total_qty}
    Verify the store data display correctly with sub package of split order flow    ${store_json_data}    ${total_qty}

Verify the store phone number, user location and postcode search
    delivery_details_page.Verify the phone number field display after select standard pickup
    delivery_details_page.Verify the store name or postcode text field
    delivery_details_page.Verify the user location section

Verify cannot select store if dont enter the phone number
    SeleniumLibrary.Input Text     ${dictCheckoutPage}[txt_phone_delivery_option]    ${EMPTY}
    SeleniumLibrary.Press Key    ${dictCheckoutPage}[txt_phone_delivery_option]    RETURN
    Verify the Select store buttons disable

Verify cannot select store if enter the phone number incorrectly
    [Arguments]    ${telephone}
    ${phone_number}=    Evaluate    '${telephone}'\[:len('${telephone}')-${3}]
    SeleniumLibrary.Input Text     ${dictCheckoutPage}[txt_phone_delivery_option]    ${phone_number}
    Verify the Select store buttons disable

Verify select store properly if enter the phone number correctly
    [Arguments]    ${phone_number}
    SeleniumLibrary.Input Text     ${dictCheckoutPage}[txt_phone_delivery_option]    ${phone_number}
    Verify the Select store buttons enable

Click standard delivery option
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lbl_2_hour_header]
    CommonWebKeywords.Scroll And Click Element    ${dictDeliveryDetailPage}[lbl_standard_delivery_header]

Click edit shipping address
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[btn_edit_shipping_address]

Verify shipping address field are display correctly
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_edit_address_name]    ${dictChangeShippingAddressPopup}[txt_edit_firstname] 
    ...    ${dictChangeShippingAddressPopup}[txt_edit_lastname]    ${dictChangeShippingAddressPopup}[txt_edit_postcode]    ${dictChangeShippingAddressPopup}[btn_save_address]

Verify default address is display correctly
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[default_address]

Click back to adrress list button
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[btn_address_list]

Verify shipping address field are display correctly with guest
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_edit_building]    ${dictChangeShippingAddressPopup}[txt_edit_address_no]
    ...    ${dictChangeShippingAddressPopup}[txt_edit_postcode]    ${dictChangeShippingAddressPopup}[lst_edit_region_id]
    ...    ${dictChangeShippingAddressPopup}[lst_edit_district_id]    ${dictChangeShippingAddressPopup}[lst_edit_subdistrict_id]

Click 2 hour pickup header
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lbl_2_hour_header]

Click Continue Payment Button
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[btn_continue_payment]

Click Standard delivery sub header
    CommonWebKeywords.Click Element Using Javascript    ${dictDeliveryDetailPage}[lbl_sub_standard_delivery_header]

Login, add product to cart, return and go to checkout delivery page with standard delivery
    [Arguments]    ${email}    ${password}    ${skus}    ${quantities}    ${pickup_id}    ${store}    ${order_total_type}
    ${user_token}    ${quote_id}    common_keywords.Add skus to cart and return user token    ${email}    ${password}    ${store}    ${quantities}    ${skus}
    Run Keyword If    "${order_total_type}" == "${order_total.lower_minimum}"    checkout_page.Verify cart total of member is lower than minimum total for Pay at Store Payment Method     ${user_token}
    ...    ELSE IF    "${order_total_type}" == "${order_total.higher_maximum}"    checkout_page.Verify cart total of member is higher than maximum total for Pay at Store Payment Method     ${user_token}
    ...    ELSE    checkout_page.Verify cart total of member have valid total for Pay at Store Payment Method    ${user_token}     
    ${response}=    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_ver3}
    common_keywords.Go To Specific Page By Member    checkout    ${email}    ${password}
    [Return]    ${response}    ${user_token}

Login, add product to cart, verify cart total, return and go to checkout delivery page with standard pickup
    [Arguments]    ${email}    ${password}    ${skus}    ${quantities}    ${pickup_id}    ${store}
    ${user_token}    ${quote_id}    common_keywords.Add skus to cart and return user token    ${email}    ${password}    ${store}    ${quantities}    ${skus}
    ${response}=    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.click_n_collect.carrier}    ${shipping_methods.click_n_collect.method}    ${store}    ${api_ver3}
    common_keywords.Go To Specific Page By Member    checkout    ${email}    ${password}
    [Return]    ${response}    ${user_token}    

Member create new shipping address
    delivery_details_page.Click address book button
    delivery_details_page.Click add shipping address
    delivery_details_page.Verify pin your location can not be selected if dont enter postcode
    delivery_details_page.Member input contact information
    delivery_details_page.Member input shipping address information
    delivery_details_page.Verify pin your location can be selected if enter postcode
    delivery_details_page.Member get shipping address information
    delivery_details_page.Click pin location button
    delivery_details_page.Verify pin your location popup display correctly
    delivery_details_page.Click pin location confirm button
    change_shipping_address_popup.Click save address button

Member verify location is not within the selected district
    [Arguments]    ${member_postcode}
    delivery_details_page.Click address book button
    delivery_details_page.Click add shipping address
    delivery_details_page.Input postcode for member info    ${member_postcode}
    delivery_details_page.Click pin location button
    delivery_details_page.Verify location button message display correctly

Guest input shipping address information
    delivery_details_page.Input address for guest info    ${guest_contact_info}[shipping_building]
    delivery_details_page.Input postcode for member info    ${guest_contact_info}[shipping_postcode]

Member select shipping address
    delivery_details_page.Select shipping address from address book
    change_shipping_address_popup.Click use selected address button
    Wait Until Page Loader Is Not Visible

Member verify pin location section is not active correctly
    delivery_details_page.Verify pin location is not active with default address
    delivery_details_page.Verify 3 hour delivery option display correctly
    delivery_details_page.Verify pin location require for 3 hour delivery

Member pin location address display correctly after pin location
    delivery_details_page.Verify member pin location address is display correctly
    delivery_details_page.Verify member pin location address is display correctly when selected 3 hour delivery option

Guest pin location address display correctly after pin location
    delivery_details_page.Verify guest pin location address is display correctly
    delivery_details_page.Verify guest pin location address is display correctly when selected 3 hour delivery option

Verify home delivery shipping option is not selected
    ${status}=    Run Keyword And Return Status    SeleniumLibrary.Radio Button Should Not Be Selected    shippingMethod
    Should Be True    ${status}

Verify 3 hour delivery option display correctly with postcode
    [Arguments]    ${flag}
    Run Keyword If    '${flag}'=='${TRUE}'    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_3_hour_delivery]
    ...    ELSE    Fail    The address location is not within pin location range. Please check inputted postcode again.