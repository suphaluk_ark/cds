*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../mobile_web/mobile_web_imports.robot

*** Keywords ***
Go to checkout page
    my_cart_page.Click Secure Checkout Button
    checkout_page.Check Out Page Should Be Visible

Select standard shipping address and go to payment page
    checkout_page.Select shipping to address option
    checkout_page.Standard delivery should be displayed
    checkout_page.Click on standard delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed

Select Same-day shipping address and go to payment page
    checkout_page.Select shipping to address option
    checkout_page.Same-day delivery should be displayed
    checkout_page.Click on Same-day delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed

Select Next-day shipping address and go to payment page
    checkout_page.Select shipping to address option
    checkout_page.Next-day delivery should be displayed
    checkout_page.Click on Next-day delivery button
    checkout_page.Click Continue Payment Button
    checkout_page.Payment Page Should Be Displayed

Confirm payment by banktransfer
    checkout_keywords.Pay by 1 2 3 kbank ATM transfer

Verify order status in MDC and MCOM are pending payment
    checkout_page.Get order ID from thank you page
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status from mdc should be 'pending payment' status    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status from MCOM should be 'ONHOLD' status    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${increment_id}

Select delivery pickup at store option and confirm payment by credit card
    checkout_keywords.Checkout, select pick at store and pay by credit card    ${pick_at_store.central_bangna}
    Get order ID from thank you page

Select Click & Collect pick at store
    [Arguments]    ${pick_at_store}
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    Select pick at store    ${pick_at_store}
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed

Fully cancel order from MCOM and verify
    Wait Until Keyword Succeeds    30 x    10 sec    force_shipment.Fully force cancel via MCOM by order number    ${increment_id}

Verify order is created in WMS via ESB
    Wait Until Keyword Succeeds    30 x    10 sec    kibana_api.Verify that order create on WMS success    ${increment_id}

Verify order is created in FMS database
    Wait Until Keyword Succeeds    30 x    10 sec    fms_db.Verify that order number exist in FMS DB      ${increment_id}

Partial redeem point and verify T1C point discount and pay other by credit card
    [Arguments]    ${point}    ${baht}    ${tc1_email}=${t1c_information_3.email}    ${tc1_pwd}=${t1c_information_3.password}
    #paitail redeem
    Click connect to T1C link
    Login to T1C account    ${tc1_email}    ${tc1_pwd}
    Click apply point after login T1C
    Input T1C point    ${point}
    Click apply T1C point
    Wait Until Page Loader Is Not Visible
    Total redeem point and discount for the 1 redemption should be    ${point}    ${baht}
    Confirm payment by credit card
    #get order ID
    Get order ID from thank you page

Get Partial redeem point and verify T1C discount rule and pay by full payment
    [Arguments]    ${total_point_api}    ${point_api}    ${baht_api}    ${point}    ${tc1_email}=${t1c_information_3.email}    ${tc1_pwd}=${t1c_information_3.password}
    Run Keyword And Return If    ${point}>${total_point_api}    Fail    msg=Not enough point for redeem. Please check on T1C account again
    Click connect to T1C link
    Login to T1C account    ${tc1_email}    ${tc1_pwd}
    Total redeem point and discount for the 1 redemption should be    ${point_api}    ${baht_api}
    Click apply point after login T1C
    Input T1C point    ${point}
    Click apply T1C point
    Wait Until Page Loader Is Not Visible
    ${discount}=    Get discount and verify redeem point display correctly     ${point_api}    ${baht_api}    ${point}
    Confirm payment by credit card
    Get order ID from thank you page
    [Return]    ${discount}

Get discount and verify redeem point display correctly 
    [Arguments]    ${point_api}    ${baht_api}    ${point}
    ${discount}=    Evaluate    (${point}*${baht_api})/${point_api}
    ${discount}=    Convert price to total amount format    ${discount}
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    checkout_page_mobile_web.Verify redeem point display correctly    ${point}    ${discount}
    ...    ELSE    checkout_page.Verify redeem point display correctly    ${point}    ${discount}
    [Return]    ${discount}

Select pickup at skybox and family mart option then confim payment by credit card
    Select picktup at skybox and family mart option
    Go to payment page
    Confirm payment by credit card
    Get order ID from thank you page

Go to payment page
    checkout_page.Click Continue Payment Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Payment Page Should Be Displayed
    Wait Until Page Loader Is Not Visible

Confirm payment by credit card
    checkout_page.Select Payment Method With Credit Card
    Wait Until Page Loader Is Not Visible
    Click Pay now Button
    Wait Until Page Loader Is Not Visible
    Order successful with credit card

Select picktup at skybox and family mart option
    my_cart_page.Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    checkout_page.Check Out Page Should Be Visible
    checkout_page.Select pick at skybox and family mart option
    Wait Until Page Loader Is Not Visible
    checkout_page.Select pick at skybox and family mart branches
    Wait Until Page Loader Is Not Visible

Select pickup at store option then confim payment by 123 bank transfer
    [Arguments]    ${store_name}
    Checkout, select pick at store and pay by 1 2 3 bank transfer(check shipping charge)    ${store_name}
    Get order ID from thank you page

Select pickup at store then confim payment by T1C redeem
    [Arguments]    ${store_name}    ${redeemp_point}    ${discount}
    Select Click & Collect pick at store    ${store_name}
    Partial redeem point and verify T1C point discount and pay other by credit card    ${redeemp_point}    ${discount}
    Get order ID from thank you page

Verify order status in MDC and MCOM are pending 
    Get order id from checkout page
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status from mdc should be 'pending' status    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status from MCOM should be 'LOGISTIC' status    ${increment_id}
    Wait Until Keyword Succeeds    30 x    10 sec    common_cds_web.Order status reason should be 'PENDING_FIRST_SHIPMENT_REQUEST'    ${increment_id}

Search and create order for verify order details with cod
    [Arguments]    ${product_sku_list}
    common_cds_web.Search product, add product to cart, go to checkout page    ${product_sku_list}
    checkout_keywords.Check Out Product Until Payment With COD Method

Go to order detail page from thank you page by order number
    [Arguments]    ${order_number}
    thankyou_page.Click continue shopping button
    order_tracking_page.Click on Tracking your orders
    header_web_keywords.Input order number into tracking pop-up    ${order_number}
    header_web_keywords.Click track order button

Refresh page
    Reload Page
    Wait Until Page Loader Is Not Visible

Verify order status in MDC, MCOM and order progress bar after create order successfully
    [Arguments]    ${order_number}
    common_cds_web.Verify order status and status reason from MDC and MCOM after pay by credit card    ${order_number}
    order_details_page.Verify order status progress bar is processing

Update order status to shipped and verify status in MDC, MCOM and order progress bar is complete fullyshipped
    [Arguments]    ${order_number}    ${sku}    ${source}=wms
    Update order status to shipped and verify status in MDC, MCOM is complete fullyshipped    ${order_number}    ${sku}    ${source}
    E2E_flow_keywords.Refresh page
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    common_cds_mobile_web.Navigate to Order Detail page    ${order_number}
    order_details_page.Verify order status progress bar is ready to ship

Update order status to shipped and verify status in MDC, MCOM is complete fullyshipped
    [Arguments]    ${order_number}    ${sku}    ${source}=wms
    Wait Until Keyword Succeeds    30 x    10 sec   kibana_api.ESB update shipment status to shipped by sku    ${order_number}    ${sku}    source=${source}
    Wait Until Keyword Succeeds    30 x    10 sec   common_cds_web.Order status from mdc should be 'COMPLETE' status    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec   common_cds_web.Verify order status from MCOM should be 'COMPLETE, FULLYSHIPPED'    ${order_number}

Update order status to ready to collect and verify status in MDC and order progress bar is fullyshipped
    [Arguments]    ${order_number}
    Update order status to ready to collect and verify status in MDC is fullyshipped    ${order_number}
    E2E_flow_keywords.Refresh page
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    common_cds_mobile_web.Navigate to Order Detail page    ${order_number}
    order_details_page.Verify order status progress bar is ready to collect

Update order status to ready to collect and verify status in MDC is fullyshipped
    [Arguments]    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec   kibana_api.ESB update shipment status ready to collect by order number    ${order_number}
    Wait Until Keyword Succeeds    30 x    10 sec   common_cds_web.Order status reason from mdc should be 'FULLYSHIPPED'    ${order_number}

Update order status to delivered and verify status in MDC and order progress bar is fullyshipped
    [Arguments]    ${order_number}    ${sku}
    Update order status to delivered and verify status in MDC is fullyshipped    ${order_number}    ${sku}
    E2E_flow_keywords.Refresh page
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    Navigate to Order Detail page    ${order_number}
    order_details_page.Verify order status progress bar is deliver
    Wait Until Page Loader Is Not Visible

Update order status to delivered and verify status in MDC is fullyshipped
    [Arguments]    ${order_number}    ${sku}
    Wait Until Keyword Succeeds    30 x    10 sec   kibana_api.ESB update shipment status to delivered by sku    ${order_number}    ${sku}
    Wait Until Keyword Succeeds    30 x    10 sec   common_cds_web.Order status reason from mdc should be 'FULLYSHIPPED'    ${order_number}
