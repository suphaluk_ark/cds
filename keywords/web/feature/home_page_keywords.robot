*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Keywords ***
Verify ui for smoke test - 'flash deal product' should be displayed correctly via api
    home_page.Verify flashdeal section should display
    ${response}=    homepage_falcon.Api Search Product In Flash Deal    ${language}    ${BU.lower()}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    Set Test Variable    ${range_of_product}    ${5}
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${range_of_product}=    Set Variable If     ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}    ${range_of_product}
    ${list_displayed_sku}=    home_page.Get list display product sku    ${range_of_product}

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    #TODO: convert keyword from PLP to flash deal for home page
    #plp_page.Verify 'search keyword label' should be displayed correctly    ${test_keyword.lower()}
    #plp_page.Verify 'product found label' should be displayed correctly    ${result_total_search_product}

    FOR    ${displayed_sku}    IN    @{list_displayed_sku}
        # displayed product
        ${displayed_data-product-sku}=  Set Variable    ${displayed_sku}[data-product-sku]
        ${displayed_data-product-id}=   Set Variable    ${displayed_sku}[data-product-id]
        ${displayed_index}=             Set Variable    ${displayed_sku}[index]

        # API product by displayed sku
        ${dict_display_sku}=    homepage_api.Get data-product-sku, data-product-id data by sku    ${response}    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    Should Be Equal As Strings    ${dict_display_sku}[data_index]    ${displayed_index}
        #TODO: convert keyword from PLP to flash deal for home page
        #Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        #Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product brand' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}
        #Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product name' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-sku]    ${displayed_data-product-sku}
        #Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product overlay image' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        #Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product price' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        #Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag new / promotion' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        #Run Keyword And Continue On Failure    search_keyword.Verify ui for smoke test - 'product tag only at / online exclusive' should be displayed correctly via api    ${dict_display_sku}[json_object_data-product-id]    ${displayed_data-product-sku}    ${displayed_data-product-id}
        Run Keyword And Continue On Failure    Verify ui for smoke test - 'Flash deal price' should be equal to cheapest price via api    ${displayed_data-product-sku}    ${dict_display_sku}[json_object_data-product-sku]
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify ui for smoke test - 'Flash deal price' should be equal to cheapest price via api
   [Arguments]    ${displayed_data-product-sku}    ${json_object_data-product-sku}
    ${expected_cheapest_price}=    product_api.Get product sell price by json object product    ${json_object_data-product-sku}
    ${elem}=    CommonKeywords.Format Text    ${dictHomePage}[lbl_flash_deal_price]    $sku=${displayed_data-product-sku}
    ${actual_displayed_price}=    SeleniumLibrary.Get Text    ${elem}
    ${actual_displayed_price}=    Convert price to number format    ${actual_displayed_price}
    Should Be Equal As Numbers    ${expected_cheapest_price}    ${actual_displayed_price}    msg="For SKU ${displayed_data-product-sku}, expected flash deal price is ${expected_cheapest_price} but get ${actual_displayed_price}. Please check with flash deal API response result"
