*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Keywords ***
Create New Account With Random Email
    [Arguments]    ${firstname}    ${lastname}    ${password}
    ${email}=    registration_page.Generate Random Email
    Set Suite Variable    ${register_email}    ${email}
    registration_page.Input Email    ${email}
    registration_page.Input Firstname    ${firstname}
    registration_page.Input Lastname    ${lastname}
    registration_page.Input Password For Register Page    ${password}
    registration_page.Click Submit Button

Verify registration success message after register
    registration_page.Verify registeration message success with welcome message

Submit And Verify Registeration Success
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    registration_page.Click Submit Button
    registration_page.Click continue shopping button after registeration suuccess
    registration_page.Verify registeration message success with welcome message
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Login Account At Register Page
    [Arguments]    ${email}    ${password}
    registration_page.Input email and password to login    ${email}    ${password}