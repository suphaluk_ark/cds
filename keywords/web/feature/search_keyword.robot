*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Keywords ***
Search product for go to PLP page
    [Arguments]    ${text}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    home_page_web_keywords.Click on search box
    common_cds_web.Remove text value
    home_page_web_keywords.Input text into search box      ${text}
    home_page_web_keywords.Press Enter key to search a product
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Search product for go to PDP page by product name
    [Arguments]    ${product_name}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    home_page_web_keywords.Search product by product name    ${product_name}
    product_PDP_page.Verify product details on product PDP page    ${product_name}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify pagination working correctly on PLP page
    [Arguments]    ${number_page}
    plp_page.Verify previous arrow is disabled
    plp_page.Click pagination    ${number_page}
    plp_page.Verify pagination should be displayed correctly after click page number     ${number_page}
    plp_page.Verify previous arrow is enabled
    plp_page.Verfy next arrow is enabled
    plp_page.Click pagination last page
    plp_page.Verfy next arrow is disabled

Verify Total Found Item Should Equal Total Count Item
    ${total_found_text}=    Get Text Total Found Item
    ${total_item_count}=   Get Total Product Item In PLP
    Should Be Equal As Numbers    ${total_found_text}    ${total_item_count}

Verify product preview should be displayed in popup by search keyword via API
    [Arguments]    ${keyword}
    home_page_web_keywords.Click on search box
    home_page_web_keywords.Input text into search box      ${keyword}
    ${list_product_name_popup}=    plp_page.Get list of product name by keyword in popup 
    ${list_product_name_api}=    search_api.Get List Of Product Name By Keyword Via API      ${keyword}
    FOR     ${product_name}     IN     @{list_product_name_popup}
        Collections.List Should Contain Value    ${list_product_name_api}    ${product_name}  
    END

Select sort discount high-low discount
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Click on sorter arrow-down button
    plp_page.Select discount high-low from sorter
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify ui for smoke test - 'category product' should be displayed correctly via api
    [Arguments]    ${test_category_id}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Category Id    ${test_category_id}    ${language}    ${BU.lower()}    ${range_of_product}
    ${products}=    JSONLibrary.Get Value From Json    ${response}    $..search.products[*]
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku
    ${len}=    Get Length    ${products}
    FOR    ${index}    IN RANGE    0    ${len}
        ${product}=    Set Variable    ${products}[${index}]
        ${configurable_product}=    Set Variable    ${product['configurable_products']}[0]
        Verify product name should be displayed correctly in search product    ${configurable_product['name']}
        search_keyword.Verify ui for smoke test - 'product tag' should be displayed correctly   ${response}   ${result_list_of_sku}[${index}]
    END

Verify ui for smoke test - 'product keyword' should be displayed correctly via api
    [Arguments]    ${keyword}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${range_of_product}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${result_list_of_product_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].name
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Verify 'search keyword label' should be displayed correctly    ${keyword.lower()}
    plp_page.Verify 'product found label' should be displayed correctly    ${result_total_search_product}
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        plp_page.Verify 'product img' should be displayed correctly    ${result_list_of_sku}[${index}]
        plp_page.Verify 'product name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_name}[${index}]
        search_keyword.Verify ui for smoke test - 'product price label' should be displayed   ${response}   ${result_list_of_sku}[${index}]
        search_keyword.Verify ui for smoke test - 'product tag' should be displayed correctly   ${response}   ${result_list_of_sku}[${index}]
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify ui for smoke test - 'product brand name' should be displayed correctly via api
    [Arguments]    ${brand_name}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Brand Name    ${brand_name}    ${language}    ${BU.lower()}    ${range_of_product}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${result_list_of_product_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].name
    ${result_list_of_product_brand_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].brand_name
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Verify 'product found label' should be displayed correctly    ${result_total_search_product}
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        plp_page.Verify 'product img' should be displayed correctly    ${result_list_of_sku}[${index}]
        plp_page.Verify 'product brand name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_brand_name}[${index}]
        plp_page.Verify 'product name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_name}[${index}]
        search_keyword.Verify ui for smoke test - 'product price label' should be displayed   ${response}    ${result_list_of_sku}[${index}]
        search_keyword.Verify ui for smoke test - 'product tag' should be displayed correctly   ${response}   ${result_list_of_sku}[${index}]
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify ui for smoke test - 'product brand name sort by discount high-low' should be displayed with discount correctly via api
    [Arguments]    ${brand_name}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Brand Name And Filter Sort 'Discount High-Low'    ${brand_name}    ${language}    ${BU.lower()}    ${range_of_product}
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        plp_page.Verify 'product with discount' should be displayed    ${result_list_of_sku}[${index}]
        search_keyword.Verify ui for smoke test - 'product price label' should be displayed   ${response}   ${result_list_of_sku}[${index}]
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify ui for smoke test - 'product price label' should be displayed 
    [Arguments]    ${response}    ${product_sku}
    ${type_id}=        JSONLibrary.Get Value From Json    ${response}     $..products[?(@.sku=='${product_sku}')].type_id
    ${type_id}=    Set Variable    ${type_id}[0]
    ${product_sku_get_price}=    Run Keyword If    '${type_id}'=='simple'    Set Variable   ${product_sku}
    ...     ELSE IF    '${type_id}'=='configurable'    plp_page.Get display configurable's simple product by configurable product sku    ${product_sku}
    ...     ELSE        Fail    Incorrect 'type_id'

    ${dict_product_sku_get_price}=    JSONLibrary.Get Value From Json    ${response}     $..*[?(@.sku=='${product_sku_get_price}')]
    ${dict_product_price}=    product_api.Get product price data by product sku        ${dict_product_sku_get_price}[0]

    Run Keyword If    ${dict_product_price}[is_product_no_discount]    search_keyword.Verify ui for smoke test - 'product price no discount' should be displayed correctly    ${product_sku}    ${dict_product_price}
    ...    ELSE    search_keyword.Verify ui for smoke test - 'product price with discount' should be displayed correctly    ${product_sku}    ${dict_product_price}

Verify ui for smoke test - 'product price no discount' should be displayed correctly
    [Arguments]    ${product_sku}    ${dict_product_price}
    plp_page.Verify 'product sell price' should be displayed correctly    ${product_sku}    ${dict_product_price}[price]
    plp_page.Verify 'product save price' should not be displayed    ${product_sku} 
    plp_page.Verify 'product save price percent' should not be displayed    ${product_sku} 

Verify ui for smoke test - 'product price with discount' should be displayed correctly
    [Arguments]    ${product_sku}    ${dict_product_price}
    plp_page.Verify 'product sell price' should be displayed correctly    ${product_sku}    ${dict_product_price}[special_price]
    Run Keyword If    '${TEST_PLATFORM}' == 'web'    plp_page.Verify 'product before discount price' should be displayed correctly    ${product_sku}    ${dict_product_price}[price]
    plp_page.Verify 'product save price' should be displayed correctly    ${product_sku}    ${dict_product_price}[evaluate_save_price]
    plp_page.Verify 'product save price percent' should be displayed    ${product_sku}    ${dict_product_price}[evaluate_percent_discount]

Verify ui for smoke test - 'product tag' should be displayed correctly
    [Arguments]    ${response}    ${product_sku}
    ${type_id}=        JSONLibrary.Get Value From Json    ${response}     $..products[?(@.sku=='${product_sku}')].type_id
    ${type_id}=    Set Variable    ${type_id}[0]
    ${product_sku_get_tag}=    Run Keyword If    '${type_id}'=='simple'    Set Variable   ${product_sku}
    ...     ELSE IF    '${type_id}'=='configurable'    plp_page.Get display configurable's simple product by configurable product sku    ${product_sku}
    ...     ELSE        Fail    Incorrect 'type_id'

    ${dict_product}=    JSONLibrary.Get Value From Json    ${response}     $..*[?(@.sku=='${product_sku_get_tag}')]
    ${dict_product}=    Set Variable    ${dict_product}[0]

    ${promo_tag}=    Set Variable    ${dict_product}[promo_tag]
    ${new_tag}=    Set Variable    ${dict_product}[new_tag]
    Run Keyword If    '${promo_tag}'!='${None}' and '${promo_tag}'!='${SPACE}'    Run Keywords    plp_page.Verify 'product promo tag' should be displayed correctly    ${product_sku}    ${promo_tag}   
    ...    AND    plp_page.Verify 'product new tag' should not be displayed    ${product_sku}
    ...   ELSE IF    ${new_tag} == ${1}   plp_page.Verify 'product new tag' should be displayed    ${product_sku}
    ...   ELSE     plp_page.Verify 'product new tag' should not be displayed    ${product_sku}

    ${only_at_tag}=    Set Variable    ${dict_product}[only_at_tag]
    ${online_exclusive_tag}=    Set Variable    ${dict_product}[online_exclusive_tag]
    Run Keyword If    ${only_at_tag} == ${1}   Run Keywords   plp_page.Verify 'product only at tag' should be displayed    ${product_sku}
    ...   AND    plp_page.Verify 'product online exclusive tag' should not be displayed    ${product_sku_get_tag}
    ...   ELSE IF   ${online_exclusive_tag} == ${1}   Run Keywords    plp_page.Verify 'product online exclusive tag' should be displayed    ${product_sku}
    ...   AND    plp_page.Verify 'product only at tag' should not be displayed    ${product_sku}
    ...   ELSE    Run Keywords    plp_page.Verify 'product online exclusive tag' should not be displayed    ${product_sku}
    ...   AND    plp_page.Verify 'product only at tag' should not be displayed    ${product_sku}

Verify ui for smoke test - 'product keyword with filter color' should be displayed correctly via api
    [Arguments]    ${keyword}    ${color}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Keyword And Filter Color    ${keyword}    ${color}    ${language}    ${BU.lower()}    ${range_of_product}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${result_list_of_product_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].name
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Verify 'search keyword label' should be displayed correctly    ${keyword.lower()}
    plp_page.Verify 'product found label' should be displayed correctly    ${result_total_search_product}
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        plp_page.Verify 'product img' should be displayed correctly    ${result_list_of_sku}[${index}]
        plp_page.Verify 'product name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_name}[${index}]
        search_keyword.Verify ui for smoke test - 'product price label' should be displayed   ${response}   ${result_list_of_sku}[${index}]
        search_keyword.Verify ui for smoke test - 'product tag' should be displayed correctly   ${response}   ${result_list_of_sku}[${index}]
    END

Select filter color option
    [Arguments]    ${color}
    plp_page.Verify 'filter color' should be displayed
    plp_page.Click on color arrow-down button
    plp_page.Click on a color    ${color}

Click filter color 'clear' button 
    plp_page.Click on color arrow-down button
    plp_page.Click on filter color 'clear' button

Click filter price range
    plp_page.Verify 'filter price range' should be displayed
    plp_page.Click on price range arrow-down button

Click filter brand name
    plp_page.Verify 'filter brand name' should be displayed
    plp_page.Click on brand name arrow-down button

Verify 'filter brand name value' should be displayed correctly by list of brand name
    [Arguments]    ${list_of_brand_name}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    FOR    ${brand_name}    IN    @{list_of_brand_name}
        plp_page.Verify 'filter brand name value' should be displayed correctly    ${brand_name}
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Select sort price low-high
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Click on sorter arrow-down button
    plp_page.Select price low-high from sorter
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify ui for smoke test - 'product brand name and sorting price low to high' should be displayed correctly via api
    [Arguments]    ${brand_name}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Brand Name And Filter Sort 'Price Low-High'    ${brand_name}    ${language}    ${BU}    ${range_of_product}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${result_list_of_product_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].name
    ${result_list_of_product_brand_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].brand_name
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    plp_page.Verify 'product found label' should be displayed correctly    ${result_total_search_product}
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        plp_page.Verify 'product img' should be displayed correctly    ${result_list_of_sku}[${index}]
        plp_page.Verify 'product brand name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_brand_name}[${index}]
        plp_page.Verify 'product name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_name}[${index}]
        search_keyword.Verify ui for smoke test - 'product price label' should be displayed   ${response}    ${result_list_of_sku}[${index}]
        search_keyword.Verify ui for smoke test - 'product tag' should be displayed correctly   ${response}   ${result_list_of_sku}[${index}]
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify ui for smoke test - 'product image' should be displayed correctly via api
    [Documentation]     - *Configuralbe product*
    ...                 - data-product-sku : _configurable's sku_, data-product-id : _simple's sku_
    ...                 - *Simple product*
    ...                 - data-product-sku : _simple's sku_, data-product-id : _simple's sku_
    ...                 - *Expected*
    ...                 - simple's _*image*_
    [Arguments]    ${json_object_by_sku}    ${data-product-sku}    ${data-product-id}
    ${type_id}=    Set Variable    ${json_object_by_sku}[type_id]

    ${image}=    Run Keyword If    '${type_id}' == 'simple'    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $.image
    ...    ELSE IF    '${type_id}' == 'configurable'    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..configurable_products[?(@.sku == '${data-product-id}')].image
    ...    ELSE    Fail    msg=Incorrect type_id

    ${image}=    Set Variable    ${image}[0]
    plp_page.Verify 'product image by data-product-sku, data-product-id' should be displayed    ${data-product-sku}    ${data-product-id}    ${image}

Verify ui for smoke test - 'product brand' should be displayed correctly via api
    [Documentation]     - *Configuralbe product*
    ...                 - data-product-sku : _configurable's sku_
    ...                 - *Simple product*
    ...                 - data-product-sku : _simple's sku_
    ...                 - *Expected*
    ...                 - data-product-sku's _*brand_name*_
    [Arguments]    ${json_object_by_sku}    ${data-product-sku}
    ${brand_name}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $.brand_name
    ${brand_name}=    Set Variable    ${brand_name}[0]

    Should Not Be Equal As Strings    ${brand_name}    ${None}    msg=Data Issue, this product brand name is None
    plp_page.Verify 'product brand name label' should be displayed correctly    ${data-product-sku}    ${brand_name}

Verify ui for smoke test - 'product name' should be displayed correctly via api
    [Documentation]     - *Configuralbe product*
    ...                 - data-product-sku : _configurable's sku_
    ...                 - *Simple product*
    ...                 - data-product-sku : _simple's sku_
    ...                 - *Expected*
    ...                 - data-product-sku's _*name*_
    [Arguments]    ${json_object_by_sku}    ${data-product-sku}
    ${name}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $.name
    ${name}=    Set Variable    ${name}[0]

    plp_page.Verify 'product name label' should be displayed correctly    ${data-product-sku}    ${name}

Verify ui for smoke test - 'product price' should be displayed correctly via api
    [Documentation]     - *Configuralbe product*
    ...                 - data-product-sku : _configurable's sku_, data-product-id : _simple's sku_
    ...                 - *Simple product*
    ...                 - data-product-sku : _simple's sku_, data-product-id : _simple's sku_
    ...                 - *Expected*
    ...                 - simple's _*before discount price, sell price*, save price, save price percent*_ 
    [Arguments]    ${json_object_by_sku}    ${data-product-sku}    ${data-product-id}
    ${dict_product_price}=    product_api.Get product price data by product sku    ${json_object_by_sku}

    Run Keyword If    ${dict_product_price}[is_product_no_discount]    search_keyword.Verify ui for smoke test - 'product price no discount' should be displayed correctly    ${data-product-sku}    ${dict_product_price}
    ...    ELSE    search_keyword.Verify ui for smoke test - 'product price with discount' should be displayed correctly    ${data-product-sku}    ${dict_product_price}

Verify ui for smoke test - 'product tag new / promotion' should be displayed correctly via api
    [Documentation]     - *Configuralbe product*
    ...                 - data-product-sku : _configurable's sku_, data-product-id : _simple's sku_
    ...                 - *Simple product*
    ...                 - data-product-sku : _simple's sku_, data-product-id : _simple's sku_
    ...                 - *Expected*
    ...                 - simple's _*tags*_ 
    [Arguments]    ${json_object_by_sku}    ${data-product-sku}    ${data-product-id}
    ${new_tag}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..new_tag
    ${new_tag}=    Set Variable    ${new_tag}[0]
    ${promo_name_tag}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..promo_tag
    ${promo_name_tag}=    Set Variable    ${promo_name_tag}[0]
    
    ${promo_name_tag}=    Run Keyword If    '${promo_name_tag}' == '${None}'    Set Variable    ${promo_name_tag}
    ...    ELSE    String.Strip String    ${promo_name_tag}
    
    Run Keyword If    '${promo_name_tag}' != '${None}' and '${promo_name_tag}' != '${EMPTY}'   Run Keywords    plp_page.Verify 'product promo tag' should be displayed correctly    ${data-product-sku}    ${promo_name_tag}   
    ...    AND    plp_page.Verify 'product new tag' should not be displayed    ${data-product-sku}
    ...   ELSE IF    ${new_tag} == ${1}   plp_page.Verify 'product new tag' should be displayed    ${data-product-sku}
    ...   ELSE     plp_page.Verify 'product new tag' should not be displayed    ${data-product-sku}

Verify ui for smoke test - 'product tag only at / online exclusive' should be displayed correctly via api
    [Documentation]     - *Configuralbe product*
    ...                 - data-product-sku : _configurable's sku_, data-product-id : _simple's sku_
    ...                 - *Simple product*
    ...                 - data-product-sku : _simple's sku_, data-product-id : _simple's sku_
    ...                 - *Expected*
    ...                 - simple's _*tags*_ 
    [Arguments]    ${json_object_by_sku}    ${data-product-sku}    ${data-product-id}
    ${online_exclusive_tag}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..online_exclusive_tag
    ${online_exclusive_tag}=    Set Variable    ${online_exclusive_tag}[0]
    ${only_central_tag}=    JSONLibrary.Get Value From Json    ${json_object_by_sku}    $..only_at_tag
    ${only_central_tag}=    Set Variable    ${only_central_tag}[0]

    Run Keyword If    ${only_central_tag} == ${1}   Run Keywords   plp_page.Verify 'product only at tag' should be displayed    ${data-product-sku}
    ...   AND    plp_page.Verify 'product online exclusive tag' should not be displayed    ${data-product-sku}
    ...   ELSE IF   ${online_exclusive_tag} == ${1}   Run Keywords    plp_page.Verify 'product online exclusive tag' should be displayed    ${data-product-sku}
    ...   AND    plp_page.Verify 'product only at tag' should not be displayed    ${data-product-sku}
    ...   ELSE    Run Keywords    plp_page.Verify 'product online exclusive tag' should not be displayed    ${data-product-sku}
    ...   AND    plp_page.Verify 'product only at tag' should not be displayed    ${data-product-sku}

Verify ui for smoke test - 'product overlay image' should be displayed correctly via api
    [Documentation]     - *Configuralbe product*
    ...                 - data-product-sku : _configurable's sku_, data-product-id : _simple's sku_
    ...                 - *Simple product*
    ...                 - data-product-sku : _simple's sku_, data-product-id : _simple's sku_
    ...                 - *Expected*
    ...                 - simple's _*product's overlays*_ 
    [Arguments]    ${json_object_by_sku}    ${data-product-sku}    ${data-product-id}
    ${dict_product_overlays}=     plp_api.Get display product overlays by product sku    ${json_object_by_sku}
    Run Keyword If    ${dict_product_overlays}[is_display]    plp_page.Verify 'product image by data-product-sku' should be displayed    ${data-product-sku}    ${dict_product_overlays}[overlay_image]