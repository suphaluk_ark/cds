*** Setting ***
Library     ${CURDIR}/../common-keywords/HTML.py
Library     ${CURDIR}/../common-keywords/AwsKeywords.py
Library     ${CURDIR}/../common-keywords/GetAMXkey.py

Resource    ${CURDIR}/../../resources/imports.robot
Variables   ${CURDIR}/../../resources/configs/web_config.yaml

Resource    ${CURDIR}/../common-keywords/CommonKeywords.robot
Resource    ${CURDIR}/../common-keywords/CommonWebKeywords.robot

Resource    ${CURDIR}/../common-keywords/fms/fms_db.robot
Resource    ${CURDIR}/../common-keywords/api/kibana/sms_details.robot

Resource    ${CURDIR}/common/common_keywords.robot
Resource    ${CURDIR}/calculation.robot

Resource    ${CURDIR}/page/checkout_page.robot
Resource    ${CURDIR}/page/forgot_password_page.robot
Resource    ${CURDIR}/page/header_web_keywords.robot
Resource    ${CURDIR}/page/home_page_web_keywords.robot
Resource    ${CURDIR}/page/login_page.robot
Resource    ${CURDIR}/page/guest_login_page.robot
Resource    ${CURDIR}/page/my_cart_page.robot
Resource    ${CURDIR}/page/registration_page.robot
Resource    ${CURDIR}/page/resend_email_page.robot
Resource    ${CURDIR}/page/product_page.robot
Resource    ${CURDIR}/page/2c2p.robot
Resource    ${CURDIR}/page/my_account_page.robot
Resource    ${CURDIR}/page/bank_transfer.robot
Resource    ${CURDIR}/page/wishlist_page.robot
Resource    ${CURDIR}/page/change_shipping_address_popup.robot
Resource    ${CURDIR}/page/delivery_details_page.robot
Resource    ${CURDIR}/page/home_page.robot
Resource    ${CURDIR}/page/order_history.robot
Resource    ${CURDIR}/page/product_category_page.robot
Resource    ${CURDIR}/page/order_tracking_page.robot
Resource    ${CURDIR}/page/thankyou_page.robot
Resource    ${CURDIR}/page/payment_page.robot
Resource    ${CURDIR}/page/standard_boutique_page.robot
Resource    ${CURDIR}/page/shop_by_brand_page.robot
Resource    ${CURDIR}/page/plp_page.robot
Resource    ${CURDIR}/page/order_details_page.robot
Resource    ${CURDIR}/page/product_PDP_page.robot
Resource    ${CURDIR}/page/category_landing_page.robot
Resource    ${CURDIR}/page/full_boutique_page.robot

Resource    ${CURDIR}/feature/common_cds_web.robot
Resource    ${CURDIR}/feature/checkout_keywords.robot
Resource    ${CURDIR}/feature/forgot_password_keywords.robot
Resource    ${CURDIR}/feature/login_keywords.robot
Resource    ${CURDIR}/feature/registration_keywords.robot
Resource    ${CURDIR}/feature/change_shipping_address_keywords.robot
Resource    ${CURDIR}/feature/delivery_details_keywords.robot
Resource    ${CURDIR}/feature/home_page_keywords.robot
Resource    ${CURDIR}/feature/thankyou_keywords.robot
Resource    ${CURDIR}/feature/payment_keywords.robot
Resource    ${CURDIR}/feature/order_details.robot
Resource    ${CURDIR}/feature/shopping_bag_keywords.robot
Resource    ${CURDIR}/feature/E2E_flow_keywords.robot
Resource    ${CURDIR}/feature/product_PDP_keywords.robot
Resource    ${CURDIR}/feature/search_keyword.robot

Resource    ${CURDIR}/../api/kibana_api.robot
Resource    ${CURDIR}/../api/customer_api.robot
Resource    ${CURDIR}/../api/wishlist_api.robot
Resource    ${CURDIR}/../api/cart_api.robot
Resource    ${CURDIR}/../api/homepage_api.robot
Resource    ${CURDIR}/../api/search_api.robot
Resource    ${CURDIR}/../api/product_api.robot
Resource    ${CURDIR}/../api/plp_api.robot
Resource    ${CURDIR}/../api/order_status_api.robot

Resource    ${CURDIR}/../magento/api/mdc/order_details.robot
Resource    ${CURDIR}/../magento/api/mcom/search_sales_order.robot
Resource    ${CURDIR}/../magento/api/mdc/get_order_keywords.robot
Resource    ${CURDIR}/../magento/etc/set_product_file.robot
Resource    ${CURDIR}/../magento/api/mcom/force_shipment.robot
Resource    ${CURDIR}/../magento/api/mcom/payment_validated.robot
Resource    ${CURDIR}/../magento/api/mdc/address_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/authentication.robot
Resource    ${CURDIR}/../magento/api/mdc/banner_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/cart.robot
Resource    ${CURDIR}/../magento/api/mdc/categories_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/checkout.robot
Resource    ${CURDIR}/../magento/api/mdc/cmsblock_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/consent.robot
Resource    ${CURDIR}/../magento/api/mdc/customer_details.robot
Resource    ${CURDIR}/../magento/api/mdc/customers.robot
Resource    ${CURDIR}/../magento/api/mdc/product.robot
Resource    ${CURDIR}/../magento/api/mdc/promotion_keywords.robot
Resource    ${CURDIR}/../magento/api/mdc/search.robot
Resource    ${CURDIR}/../magento/api/mdc/store_location.robot
Resource    ${CURDIR}/../magento/api/mdc/wishlist.robot
Resource    ${CURDIR}/../magento/api/mdc/auto_display_overlay.robot

Resource    ${CURDIR}/../falcon/api/homepage/homepage_falcon.robot
Resource    ${CURDIR}/../falcon/api/plp/search_falcon.robot
Resource    ${CURDIR}/../falcon/api/pdp/pdp_falcon.robot

Variables    ${CURDIR}/../../resources/testdata/common${/}common_variable.yaml
Variables    ${CURDIR}/../../resources/testdata/common${/}translation_${language.lower()}.yaml
Variables    ${CURDIR}/../../resources/testdata/${BU.lower()}/${ENV.lower()}${/}test_data.yaml
Variables    ${CURDIR}/../../resources/testdata/${BU.lower()}/${BU.lower()}_translation/${BU.lower()}_translation_${language.lower()}.yaml
Variables    ${CURDIR}/../../resources/testdata/${BU.lower()}/${env}/test_one_project.yaml

*** Variables ***
${testdata_filepath}    ${CURDIR}/../../resources/testdata/${BU.lower()}/

*** Keywords ***
Global init
    Import Variables    ${testdata_filepath}${ENV.lower()}${/}test_data.yaml
    Set Global Variable    ${language}    ${language.lower()}