*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Variables ***
&{dictGuestLoginPage}
...    lbl_required_field=xpath=//*[text()='{$value}']
...    lbl_email_placeholder=xpath=//input[@placeholder='${guest_login_page.email_place_holder}']
...    lbl_pass_placeholder=xpath=//input[@placeholder='${guest_login_page.password_place_holder}']
...    input_username=xpath=//*[@name='email']
...    input_password=xpath=//*[@name='password']
...    btn_submit_login=xpath=//button[@type='submit']
...    msg_email_error=xpath=//div[text()='${error_message.guest_login.invalid_email}']
...    msg_password_error=xpath=//div[text()='${error_message.guest_login.invalid_password}']
...    msg_incorrect_account_error=xpath=//*[text()='${error_message.guest_login.incorrect_account}']
...    btn_login_facebook=xpath=//*[contains(text(),"${guest_login_page.login_facebook}")]
...    btn_checkout_as_guest=xpath=//a[@to='/checkout']
${timeout}    10s

*** Keywords ***
Verify page displays correctly require fields
    [Arguments]    ${field_name}
    ${text}=    CommonKeywords.Format Text    ${dictGuestLoginPage}[lbl_required_field]    $value=${field_name}
    CommonWebKeywords.Verify Web Elements Are Visible  ${text}

Verify email place holder display correctly
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictGuestLoginPage}[lbl_email_placeholder]

Verify password place holder display correctly
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictGuestLoginPage}[lbl_pass_placeholder]

Input text for username
    [Arguments]    ${username}
    SeleniumLibrary.Wait Until Element Is Visible    ${dictGuestLoginPage}[input_username]    ${timeout}
    SeleniumLibrary.Input Text    ${dictGuestLoginPage}[input_username]    ${username}

Input text for password
    [Arguments]    ${password}
    SeleniumLibrary.Wait Until Element Is Visible    ${dictGuestLoginPage}[input_password]    ${timeout}
    SeleniumLibrary.Input Text    ${dictGuestLoginPage}[input_password]    ${password}

Click login submit button
    CommonWebKeywords.Click Element  ${dictGuestLoginPage}[btn_submit_login]

Input username, password and click login button at guest login page
    [Arguments]  ${username}  ${password}
    Input text for username  ${username}
    Input text for password  ${password}
    Click login submit button

Click login with facebook
    CommonWebKeywords.Click Element  ${dictGuestLoginPage}[btn_login_facebook]

Click login as guest
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictGuestLoginPage}[btn_checkout_as_guest]
    CommonWebKeywords.Click Element  ${dictGuestLoginPage}[btn_checkout_as_guest]

Input text and verify email error message should be displayed
    [Arguments]  ${username}
    Input text for username  ${username}
    Click login submit button
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictGuestLoginPage}[msg_email_error]

Verify password error message should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictGuestLoginPage}[msg_password_error]

Verify incorrect account error message should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictGuestLoginPage}[msg_incorrect_account_error]


