*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/product_page.robot

*** Variables ***
${product_type_sku}=    ${product_sku_list}[0]

*** Keywords ***
Click available delivery option
    CommonWebKeywords.Click Element    ${productPage}[ddl_filter]

Click sort drop down list
    CommonWebKeywords.Click Element    ${productPage}[ddl_sort]

Click checkbox options to filter and get size of delivery method list
    [Arguments]    ${option}
    Run Keyword If    '${option}'=='${filters.delivery_methods.click_and_collect}'    CommonWebKeywords.Click Element    ${productPage}[chk_clickAndCollect]
    ...    ELSE IF    '${option}'=='${filters.delivery_methods.delivery}'    CommonWebKeywords.Click Element    ${productPage}[chk_delivery]
    ...    ELSE IF    '${option}'=='${filters.delivery_methods.two_hours_pick_up}'    CommonWebKeywords.Click Element    ${productPage}[chk_hourPickup]
    ...    ELSE    BuiltIn.Fail    Incorrect option
    SeleniumLibrary.Wait Until Element Is Visible    ${productPage}[fil_products]
    @{elements}    SeleniumLibrary.Get WebElements    ${productPage}[fil_products]
    @{sku_list}    Create List
    FOR    ${element}    IN    @{elements}
        ${element_text}    SeleniumLibrary.Get Element Attribute    ${element}    attribute=data-product-id
        Append To List    ${sku_list}    ${element_text}
    END
    ${length_sku}=    Get Length       ${sku_list}
    [Return]    ${length_sku}

Verify total number result displays correctly
    [Arguments]    ${length_sku}
    ${length_ui}=    SeleniumLibrary.Get Text    ${productPage}[chk_clickAndCollect]
    ${length_ui}=    Replace String    ${length_ui}    (    ${EMPTY}    count=1
    ${length_ui}=    Replace String    ${length_ui}    )    ${EMPTY}    count=1
    Should Be Equal As Numbers    ${length_sku}    ${length_ui}

Sort by New Arrival and count the quantity be returned
    CommonWebKeywords.Click Element    ${productPage}[new_arrivals]
    SeleniumLibrary.Wait Until Element Is Visible    ${productPage}[fil_products]
    ${qty_sort}=    Get Element Count    ${productPage}[fil_products]
    [Return]    ${qty_sort}

Sort by Available Delivery Options and count the quantity be returned
    [Arguments]    ${option}
    Click available delivery option
    Run Keyword If    '${option}'=='${filters.delivery_methods.click_and_collect}'    CommonWebKeywords.Click Element    ${productPage}[chk_clickAndCollect]
    ...    ELSE IF    '${option}'=='${filters.delivery_methods.delivery}'    CommonWebKeywords.Click Element    ${productPage}[chk_delivery]
    ...    ELSE IF    '${option}'=='${filters.delivery_methods.two_hours_pick_up}'    CommonWebKeywords.Click Element    ${productPage}[chk_hourPickup]
    ...    ELSE    BuiltIn.Fail    Incorrect option
    SeleniumLibrary.Wait Until Element Is Visible    ${productPage}[fil_products]
    ${qty_available_delivery}=    Get Element Count    ${productPage}[fil_products]
    [Return]    ${qty_available_delivery}

Verify filter by delivery options is priority
    [Arguments]    ${qty_sort}    ${qty_available_delivery}
    Should Not Be Equal As Integers    ${qty_sort}    ${qty_available_delivery}

Click checkbox options to filter and get list of delivery method
    [Arguments]    ${option}
    Run Keyword If    '${option}'=='${filters.delivery_methods.click_and_collect}'    CommonWebKeywords.Click Element    ${productPage}[chk_clickAndCollect]
    ...    ELSE IF    '${option}'=='${filters.delivery_methods.delivery}'    CommonWebKeywords.Click Element    ${productPage}[chk_delivery]
    ...    ELSE IF    '${option}'=='${filters.delivery_methods.two_hours_pick_up}'    CommonWebKeywords.Click Element    ${productPage}[chk_hourPickup]
    ...    ELSE    BuiltIn.Fail    Incorrect option
    SeleniumLibrary.Wait Until Element Is Visible    ${productPage}[fil_products]
    @{elements}    SeleniumLibrary.Get WebElements    ${productPage}[fil_products]
    @{sku_list}    Create List
    FOR    ${element}    IN    @{elements}
        ${element_text}    SeleniumLibrary.Get Element Attribute    ${element}    attribute=data-product-id
        Append To List    ${sku_list}    ${element_text}
    END
    [Return]    ${sku_list}

Verify the product are filtered correctly
    [Arguments]    ${sku_list}    ${option}
    FOR    ${sku}    IN    @{sku_list}
        ${response}=    Get delivery methods response by sku and postcode    cds_en    ${sku}    0
        ${shipping_methods}=    Get Value From Json    ${response}    $..delivery_method
        Run Keyword If    '${option}'=='${filters.delivery_methods.click_and_collect}'    Run Keyword And Continue On Failure    Should Contain    ${shipping_methods}    click_and_collect    ignore_case=False
        ...    ELSE IF    '${option}'=='${filters.delivery_methods.delivery}'    Run Keyword And Continue On Failure    Should Contain    ${shipping_methods}    delivery    ignore_case=False
        ...    ELSE IF    '${option}'=='${filters.delivery_methods.two_hours_pick_up}'    Run Keyword And Continue On Failure    Should Contain    ${shipping_methods}    hour_pickup    ignore_case=False
    END

Verify message line for out of stock or preorder or byorder was displayed
    [Arguments]    ${msgLine}
    SeleniumLibrary.Element Should Be Visible    ${msgLine}

Verify available delivery options section was not display
    SeleniumLibrary.Element Should Not Be Visible    ${dictproductPDP}[lnk_get_estimated_dates]
    SeleniumLibrary.Element Should Not Be Visible    ${dictproductPDP}[txt_postcode]
    SeleniumLibrary.Element Should Not Be Visible    ${dictproductPDP}[btn_postcode_apply]

Click checkbox options to filter and get the quantity of selected checkboxes
    Click available delivery option
    CommonWebKeywords.Click Element    ${productPage}[chk_clickAndCollect]
    Wait Until Page Is Completely Loaded
    Click available delivery option
    ${when_selected_qty}=    SeleniumLibrary.Get Text    ${productPage}[txt_number_selected]
    Click clear button
    Click available delivery option
    ${after_clear_qty}=    SeleniumLibrary.Get Text    ${productPage}[txt_number_selected]
    [Return]    ${after_clear_qty}

Click clear button
    SeleniumLibrary.Wait Until Element Is Visible    ${productPage}[btn_clear]
    CommonWebKeywords.Click Element    ${productPage}[btn_clear]
    Wait Until Page Is Completely Loaded

Verify the quantity checkbox displayed correctly
    [Arguments]    ${after_clear_qty}
    Should Contain    ${after_clear_qty}    0

Click add product to wishlist button
    CommonWebKeywords.Click Element    ${productPage}[btn_add_to_wishlist]

Add product quantity
    [Arguments]    ${skunumber}    ${quantity}=${1}
    Set Test Variable    ${product_quantity}    ${quantity}
    Return From Keyword If    ${quantity}==1    ${True}
    ${elem}=    CommonKeywords.Format Text    ${productPage}[ddl_quantity]    $skunumber=${sku_number}
    CommonWebKeywords.Scroll And Click Element    ${elem}
    ${elem}=    CommonKeywords.Format Text    ${productPage}[list_qty]    $skunumber=${sku_number}    $number=${quantity}
    Scroll To Element   ${elem}
    Wait Until Keyword Succeeds    3 x    2s    CommonWebKeywords.Click Element    ${elem}
    common_keywords.Wait Until Page Is Completely Loaded

Click on add to cart button
    [Arguments]    ${sku_number}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait
    ${elem}=    CommonKeywords.Format Text    ${productPage}[btn_addToCart]    $skunumber=${sku_number}
    Wait Until Keyword Succeeds    3x    1s    CommonWebKeywords.Scroll And Click Element    ${elem}    
    ${elem}=    CommonKeywords.Format Text    ${productPage}[btn_loading]    $skunumber=${sku_number}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${elem}=    CommonKeywords.Format Text    ${productPage}[btn_add_success]    $skunumber=${sku_number}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    common_keywords.Wait Until Page Is Completely Loaded

Click wishlist icon on product detail page
    [Arguments]  ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${productPage}[icon_product_wishlist]    $product_sku=${product_sku}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Verify Pre-order popup
    [Arguments]  ${Preorder_sku}
    ${elem}=    CommonKeywords.Format Text    ${productPage}[txt_pre_order_popup]    $txt_pre_order_popup=${Preorder_sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click OK to close pre-oder popup
    CommonWebKeywords.Click Element  ${productPage}[btn_popup_ok]

Verify Pre-order is suppressed
    [Arguments]  ${text}
    ${elem}=    CommonKeywords.Format Text    ${productPage}[txt_pre_order_onpage]    $txt_pre_order=${text}
    SeleniumLibrary.Wait Until Element Is Not Visible    ${elem}

Get product sku by product type via api
    [Documentation]    ${product_type}=    simple, configurable, marketplace.
    [Arguments]    ${product_type}    ${is_delivery_option}=${FALSE}
    ${type}=    Run Keyword If    '${product_type}'=='marketplace'    Set Variable    simple
    ...    ELSE    Set Variable    ${product_type}
    ${response}=    Get products by type id    ${type}    ${store_view.name.en}
    @{skus}=    Get Value From Json    ${response}    $..sku
    ${skus}=    Convert To List    ${skus}
    @{product_skus}=    Set Variable If    '${product_type}'=='simple'    ${skus}
    FOR    ${sku}    IN    @{skus}
        Exit For Loop If    '${product_type}'=='simple'
        Run Keyword If    'GR' in '${sku}' or 'MKP' in '${sku}'    Append To List    ${product_skus}    ${sku}
    END
    FOR    ${inx}    IN RANGE    len(${product_skus})
        Exit For Loop If    ${is_delivery_option}==${FALSE}
        ${response}=    product.Get delivery methods response by sku and postcode    ${store_view.name.en}    ${product_skus}[${inx}]    0
        @{shipping_methods}=    Get Value From Json    ${response}    $..delivery_method
        ${len_methods}=    Get Length    ${shipping_methods}
        Return From Keyword If    ${len_methods}>=2    ${product_skus}[${inx}]
    END
    ${index}=    Evaluate    random.randint(0, ${${len}-1})    modules=random
    [Return]    ${product_skus}[${index}]

Navigate to page url on plp
    [Arguments]    ${url}
    Go To Direct Url    ${url}
    Wait Until Page Is Completely Loaded
    SeleniumLibrary.Wait Until Page Contains Element    ${productPage}[img_product_images]    ${GLOBALTIMEOUT}

Navigate to page url with specific product type
    [Arguments]    ${product_type}
    ${product_sku}=    Get product sku by product type via api    ${product_type}
    Set Test Variable    ${product_type_sku}    ${product_sku}
    Navigate to page url on plp    search/${product_type_sku}

Go to the section on plp
    [Arguments]    ${url}    ${section}    ${sku}=${EMPTY}
    Go To Direct Url    ${url}
    Wait Until Page Is Completely Loaded
    Scroll to text section on PLP    ${section}
    ${section}=    Run Keyword If    '${section}'=='${brand_collection_text.skin_care}'    Set Variable    ${sku.lower()}
    ...    ELSE    Set Variable    ${section}
    ${elem}=    CommonKeywords.Format Text    ${productPage}[img_product_images_into_section]    $section=${section}
    SeleniumLibrary.Wait Until Page Contains Element    ${elem}

Scroll to text section on PLP
    [Arguments]    ${section}
    ${text_section}=    Run Keyword If    '${section}'=='${plp_page_section_scope.hot_deals}'    Set Variable    ${home_page.section.hotdeal}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.new_arrival}'    Set Variable    ${home_page.section.new_arrival}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.best_sellers}'    Set Variable    ${home_page.section.best_seller}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.flash_deal}'    Set Variable    ${text_name_section.flash_deal}
    ...    ELSE IF    '${section}'=='${brand_collection_text.skin_care}'    Set Variable    ${brand_collection_text.skin_care}
    common_keywords.Scroll To Text Section    ${text_section}

Verify delivery option badge should be visible on plp
    [Arguments]    ${product_sku}    ${text_section}=${EMPTY}
    SeleniumLibrary.Wait Until Page Contains Element    ${productPage}[img_product_images]
    ${response}=    product.Get delivery methods response by sku and postcode    ${store_view.name.en}    ${product_sku}    0
    @{delivery_methods}=    Get Value From Json    ${response}    $..delivery_method
    ${is_home}=    Evaluate    '${filters.delivery_methods.delivery}' in ${delivery_methods}
    ${is_click_n_collect}=    Evaluate    '${filters.delivery_methods.click_and_collect}' in ${delivery_methods}
    ${is_3hours}=    Evaluate    '${filters.delivery_methods.three_hours_delivery}' in ${delivery_methods}

    Check 2 hour pickup badge should be visible    ${product_sku}    ${text_section}
    Run Keyword If    ${is_home}==${TRUE}    Check home delivery badge should be visible    ${product_sku}    ${flagged.enable}    ${text_section}
    ...    ELSE IF    ${is_home}==${FALSE}    Check home delivery badge should be visible    ${product_sku}    ${flagged.disable}    ${text_section}
    Run Keyword If    ${is_click_n_collect}==${TRUE}    Check click n collect delivery badge should be visible    ${product_sku}    ${flagged.enable}    ${text_section}
    ...    ELSE IF    ${is_click_n_collect}==${FALSE}    Check click n collect delivery badge should be visible    ${product_sku}    ${flagged.disable}    ${text_section}
    Run Keyword If    ${is_3hours}==${TRUE}    Check 3 hour delivery badge should be visible    ${product_sku}    ${flagged.enable}    ${text_section}
    ...    ELSE IF    ${is_3hours}==${FALSE}    Check 3 hour delivery badge should be visible    ${product_sku}    ${flagged.disable}    ${text_section}

Check 2 hour pickup badge should be visible
    [Arguments]    ${product_sku}    ${text_section}=${EMPTY}
    ${loc_hour_pickup}=    Set Variable If    '${text_section}'=='${EMPTY}'    ${productPage}[lbl_badge_2hours]
    ...    '${text_section}'!='${EMPTY}'    ${productPage}[txt_section_2hours]
    ${response}=    Get product via product V2 API by SKU    ${product_sku}
    @{shipping_methods}=    Get Value From Json    ${response}[0]    $.custom_attributes_option[?(@.attribute_code=='shipping_methods')].value
    ${len}=    Get Length    ${shipping_methods}
    Run Keyword If    ${len}==0    Run Keyword And Return    Check delivery option badge should be visible    ${loc_hour_pickup}    ${product_sku}    ${flagged.disable}    ${text_section}
    ${shipping_methods}=    Convert To List    ${shipping_methods}[0]
    ${shipping_methods}=    Evaluate    [x.replace(" ", "") for x in ${shipping_methods}]
    ${is_hour}=    Evaluate    any('2hourpickup' in item.lower() for item in ${shipping_methods})
    ${hour_flag}=    Run Keyword If    ${is_hour}==${TRUE}    Set Variable    ${flagged.enable}
    ...    ELSE    Set Variable    ${flagged.disable}
    Check delivery option badge should be visible    ${loc_hour_pickup}    ${product_sku}    ${hour_flag}    ${text_section}

Check home delivery badge should be visible
    [Arguments]    ${product_sku}    ${home_flag}    ${text_section}=${EMPTY}
    ${loc_home_delivery}=    Set Variable If    '${text_section}'=='${EMPTY}'    ${productPage}[lbl_badge_homedelivery]
    ...    '${text_section}'!='${EMPTY}'    ${productPage}[txt_section_homedelivery]
    Check delivery option badge should be visible    ${loc_home_delivery}    ${product_sku}    ${home_flag}    ${text_section}

Check click n collect delivery badge should be visible
    [Arguments]    ${product_sku}    ${click_n_collect_flag}    ${text_section}=${EMPTY}
    ${loc_click_n_collect}=    Set Variable If    '${text_section}'=='${EMPTY}'    ${productPage}[lbl_badge_clickncollect]
    ...    '${text_section}'!='${EMPTY}'    ${productPage}[txt_section_clickncollect]
    Check delivery option badge should be visible    ${loc_click_n_collect}    ${product_sku}    ${click_n_collect_flag}    ${text_section}

Check 3 hour delivery badge should be visible
    [Arguments]    ${product_sku}    ${3hour_delivery_flag}    ${text_section}=${EMPTY}
    ${loc_3hour_delivery}=    Set Variable If    '${text_section}'=='${EMPTY}'    ${productPage}[lbl_badge_3hours]
    ...    '${text_section}'!='${EMPTY}'    ${productPage}[txt_section_3hours]
    Check delivery option badge should be visible    ${loc_3hour_delivery}    ${product_sku}    ${3hour_delivery_flag}    ${text_section}

Check delivery option badge should be visible
    [Arguments]    ${locator}    ${product_sku}    ${flag}    ${text_section}
    ${elem}=    Run Keyword If    '${text_section}'!='${EMPTY}'
    ...    CommonKeywords.Format Text    ${locator}    $text_section=${text_section}    $sku_number=${product_sku}    $flag=${flag}
    ...    ELSE
    ...    CommonKeywords.Format Text    ${locator}    $sku_number=${product_sku}    $flag=${flag}
    CommonWebKeywords.Scroll to bottom page
    CommonWebKeywords.Scroll To Element    ${elem}
    SeleniumLibrary.Wait Until Page Contains Element    ${elem}

Go to the pages or sections on plp
    [Arguments]    ${section}
    ${product_type_sku}=    Get list random sku of plp page
    ${len}=    Get Length    ${product_type_sku}
    Run Keyword And Return If    ${len}==${0}    Fail    msg=Please check product skus on PLP again
    Run Keyword If    '${section}'=='${plp_page_section_scope.catalog_plp}'    Navigate to page url on plp    ${home_category.home_appliances}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.search_plp}'    Navigate to page url on plp    search/${product_type_sku},${product_sku_list}[1]
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.brand_plp}'    Navigate to page url on plp    ${product_brand.aveda}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.product_brand}'    Navigate to page url on plp    ${product_brand.adidas}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.brand_collection}'    Go to the section on plp    ${product_brand.aveda}    ${brand_collection_text.skin_care}    ${product_type_sku}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.hot_deals}'    Go to the section on plp    ${EMPTY}    ${plp_page_section_scope.hot_deals}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.new_arrival}'    Go to the section on plp    ${EMPTY}    ${plp_page_section_scope.new_arrival}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.best_sellers}'    Go to the section on plp    ${EMPTY}    ${plp_page_section_scope.best_sellers}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.flash_deal}'    Go to the section on plp    ${EMPTY}    ${plp_page_section_scope.flash_deal}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.recently_viewed}'    product_PDP_page.Go to section on pdp    ${product_type_sku}    ${plp_page_section_scope.recently_viewed}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.you_may_like}'    product_PDP_page.Go to section on pdp    ${product_type_sku}    ${plp_page_section_scope.you_may_like}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.similar_product}'    product_PDP_page.Go to section on pdp    ${product_sku_list}[45]    ${plp_page_section_scope.similar_product}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.complete_the_look}'    product_PDP_page.Go to section on pdp    ${product_sku_list}[45]    ${plp_page_section_scope.complete_the_look}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.simple_product}'    Navigate to page url with specific product type    simple
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.configurable_product}'    Navigate to page url with specific product type    configurable
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.market_place_product}'    Navigate to page url with specific product type    marketplace
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.preorder_product}'    Navigate to page url on plp    search/${product_preorder_sku}[0]
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.byorder_product}'    Navigate to page url on plp    search/${product_byorder_sku}[0]
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.oos_product}'    Navigate to page url on plp    search/${product_oos_sku_list}[0]

Verify delivery option badge work correctly on plp
    [Arguments]    ${section}
    Run Keyword If    '${section}'=='${plp_page_section_scope.catalog_plp}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[44]
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.search_plp}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[1]
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.brand_plp}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[40]
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.product_brand}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[41]
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.brand_collection}'
    ...    Verify delivery option badge should be visible on plp    ${product_type_sku}    ${brand_collection_text.skin_care}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.hot_deals}'
    ...    Verify delivery option badge should be visible on plp    ${product_type_sku}    ${home_page.section.hotdeal}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.new_arrival}'
    ...    Verify delivery option badge should be visible on plp    ${product_type_sku}    ${home_page.section.new_arrival}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.best_sellers}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[45]    ${home_page.section.best_seller}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.flash_deal}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[46]    ${text_name_section.flash_deal}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.recently_viewed}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[1]    ${product_detail_page.preview_section}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.you_may_like}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[47]    ${product_detail_page.you_may_like}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.similar_product}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[48]    ${product_detail_page.similar_section}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.complete_the_look}'
    ...    Verify delivery option badge should be visible on plp    ${product_sku_list}[49]    ${product_detail_page.complete_the_look}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.simple_product}'
    ...    Verify delivery option badge should be visible on plp    ${product_type_sku}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.configurable_product}'
    ...    Verify delivery option badge should be visible on plp    ${product_type_sku}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.market_place_product}'
    ...    Verify delivery option badge should be visible on plp    ${product_type_sku}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.preorder_product}'
    ...    Verify delivery option badge should be visible on plp    ${product_preorder_sku}[0]

Get list random sku of plp page
    ${list}    Create List
    @{elem}=    SeleniumLibrary.Get WebElements    ${productPage}[list_product_plp]
    ${len}=    Get Length    ${elem}
    FOR    ${idx}    IN RANGE    5
        Exit For Loop If    ${len}==${0}
        ${index}=    Evaluate    random.randint(0, ${${len}-1})    random
        ${elem_id}=    SeleniumLibrary.Get Element Attribute    ${elem}[${index}]    id
        ${sku}=    Split String From Right    ${elem_id}    separator=-
        ${sku}=    Set Variable    ${sku}[2]
        Append To List    ${list}    ${sku}
    END
    [Return]    ${list}

Verify delivery option badge of section display correctly on plp
    [Arguments]    ${section}
    ${list_elem}=    CommonKeywords.Format Text    ${productPage}[list_product_section_plp]    $section=${section}
    @{elem}=    SeleniumLibrary.Get WebElements    ${list_elem}
    ${len}=    Get Length    ${elem}
    Run Keyword And Return If    ${len}==${0}    Fail    msg=Delivery Badges don't display. Please check on UI or the cookie again
    ${index}=    Evaluate    random.randint(0, ${${len}-1})    random
    ${sku}=    SeleniumLibrary.Get Element Attribute    ${elem}[${index}]    id
    ${sku}=    Split String From Right    ${sku}    separator=-
    ${sku}=    Set Variable    ${sku}[2]
    ${response}=    product.Get delivery methods response by sku and postcode    ${store_view.name.en}    ${sku}    0
    Verify delivery option badge should be display correctly    ${response}    ${sku}

Verify delivery option badge should be display correctly
    [Arguments]    ${response}    ${sku}
    @{delivery_method_label}=    Get Value From Json    ${response}    $..delivery_methods..delivery_method_label
    Verify standard delivery badge display correctly    ${delivery_method_label}    ${sku}
    Verify standard pickup badge display correctly    ${delivery_method_label}    ${sku}
    Verify 2 hour pickup badge display correctly    ${delivery_method_label}    ${sku}
    Verify 3 hour delivery badge display correctly    ${delivery_method_label}    ${sku}

Verify standard delivery badge display correctly
    [Arguments]    ${delivery_method_label}    ${sku}
    ${status}=    Run Keyword And Return Status    Should Contain Match    ${delivery_method_label}    ${shipping_method_label.home_delivery.label}
    ${flag}=    Set Variable If    '${status}'=='${TRUE}'    ${flagged.enable}    ${flagged.disable}
    ${elem}=    Format Text    ${productPage}[lbl_badge_homedelivery]    $flag=${flag}    $sku_number=${sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify standard pickup badge display correctly
    [Arguments]    ${delivery_method_label}    ${sku}
    ${status}=    Run Keyword And Return Status    Should Contain Match    ${delivery_method_label}    ${shipping_method_label.click_n_collect.label}
    ${flag}=    Set Variable If    '${status}'=='${TRUE}'    ${flagged.enable}    ${flagged.disable}
    ${elem}=    Format Text    ${productPage}[lbl_badge_clickncollect]    $flag=${flag}    $sku_number=${sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 2 hour pickup badge display correctly
    [Arguments]    ${delivery_method_label}    ${sku}
    ${status}=    Run Keyword And Return Status    Should Contain Match    ${delivery_method_label}    ${shipping_method_label.one_hour_pickup.today}
    ${flag}=    Set Variable If    '${status}'=='${TRUE}'    ${flagged.enable}    ${flagged.disable}
    ${elem}=    Format Text    ${productPage}[lbl_badge_2hours]    $flag=${flag}    $sku_number=${sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 3 hour delivery badge display correctly
    [Arguments]    ${delivery_method_label}    ${sku}
    ${status}=    Run Keyword And Return Status    Should Contain Match    ${delivery_method_label}    ${shipping_method_label.three_hour_delivery.label}
    ${flag}=    Set Variable If    '${status}'=='${TRUE}'    ${flagged.enable}    ${flagged.disable}
    ${elem}=    Format Text    ${productPage}[lbl_badge_3hours]    $flag=${flag}    $sku_number=${sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}