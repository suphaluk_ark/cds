*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/wishlist_page.robot

*** Variables ***
${timeout}    10s

*** Keywords ***
Product should be displayed on wishlist page
    [Arguments]     ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${dictWishlistpage}[txt_product_name]    $product_name=${product_name}
    CommonWebKeywords.Verify Web Elements Are Visible      ${elem}

Product should not be visible on wishlist page
    [Arguments]     ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${dictWishlistpage}[txt_product_name]    $product_name=${product_name}
    Seleniumlibrary.Wait Until Element Is Not Visible      ${elem}

Remove wishlist product
    CommonWebKeywords.Click Element     ${dictWishlistpage}[btn_remove]

Clicking add to cart button on wishlist page
    [Arguments]     ${sku_number}
    ${elem}=    CommonKeywords.Format Text  ${dictWishlistpage}[btn_addToCart]   $skunumber=${sku_number}
    CommonWebKeywords.Click Element     ${elem}

    ${elem}=    CommonKeywords.Format Text  ${dictWishlistpage}[btn_loading]     $skunumber=${sku_number}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}

    ${elem}=    CommonKeywords.Format Text  ${dictWishlistpage}[btn_add_success]     $skunumber=${sku_number}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}

Go to wishlist page 
    CommonWebKeywords.Click Element  ${dictWishlistpage}[img_wishlist_icon]
    common_keywords.Wait Until Page Is Completely Loaded

Click on heart icon when user not login 
    CommonWebKeywords.Click Element  ${dictWishlistpage}[img_wishlist_icon_not_loggin]

Wishlist title should be displayed 
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictWishlistpage}[lbl_wishlist_title]

Get pagination number
    SeleniumLibrary.Wait Until Element Is Visible  ${dictWishlistpage}[btn_last_page]
    ${elem}=    SeleniumLibrary.Get Text    ${dictWishlistpage}[btn_last_page]
    [Return]    ${elem}

Scroll to social icon
    Scroll Element Into View  ${dictWishlistpage}[img_social_icon]

6 products should be displayed per page
    ${pages}=    Get pagination number
    FOR    ${index}    IN RANGE  1    ${pages}
           ${size}=    Get Element Count    ${dictWishlistpage}[div_products]
           Should Be True  ${size}<=6
           Scroll To Element  ${dictWishlistpage}[lnk_last_product]
           Click Element  ${dictWishlistpage}[btn_next]
    END

Click on the last page
    Scroll to social icon
    CommonWebKeywords.Click Element    ${dictWishlistpage}[btn_last_page]

Get the number of un-wishlisted products displaying on the product page
    SeleniumLibrary.Wait Until Page Contains Element    ${dictWishlistpage}[btn_wishlist_list]    ${timeout}
    ${length}=    Get Element Count  ${dictWishlistpage}[btn_wishlist_list]
    [Return]    ${length}

Pop up login button should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictWishlistpage}[msg_login_require]

Click OK to close the pop up
    CommonWebKeywords.Click Element  ${dictWishlistpage}[btn_OK]

Added product should be displayed in wishlist
    [Arguments]  ${product_title}
    ${elem}=    CommonKeywords.Format Text    ${dictWishlistpage}[lbl_added_product_title]    $title=${product_title}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Click on a random wishlist button
    ${length}=    Get the number of un-wishlisted products displaying on the product page
    ${random}=    Evaluate    random.randint(1,${length})  modules=random
    ${img_product}=    CommonKeywords.Format Text    ${dictWishlistpage}[img_product_wishlist]    $random=${random}
    ${product_sku}=    SeleniumLibrary.Get Element Attribute    ${img_product}  data-product-sku
    ${wishlist_btn}=    CommonKeywords.Format Text    ${dictWishlistpage}[btn_wishlist_random]    $sku_number=${product_sku}
    
    CommonWebKeywords.Scroll To Element    ${img_product}
    Mouse Over    ${img_product}
    CommonWebKeywords.Scroll To Element    ${wishlist_btn}
    Mouse Over  ${img_product}
    CommonWebKeywords.Click Element    ${wishlist_btn}

Click wishlist button on multiple products
    [Arguments]  ${times}
    FOR  ${i}  IN RANGE  1  ${times}
         Click on a random wishlist button
         Scroll Page To Location  0  0
    END

Click on a random wishlist button and get product name
    ${length}=    Get the number of un-wishlisted products displaying on the product page
    ${random}=    Evaluate    random.randint(1, ${length})  modules=random
    ${img_product}=    CommonKeywords.Format Text    ${dictWishlistpage}[img_product_wishlist]    $random=${random}
    ${product}=    CommonKeywords.Format Text    ${dictWishlistpage}[img_product_wishlist_info]    $random=${random}

    ${product_name}=    SeleniumLibrary.Get Element Attribute    ${product}  data-product-name
    ${product_sku}=    SeleniumLibrary.Get Element Attribute    ${img_product}  data-product-sku
    ${wishlist_btn}=    CommonKeywords.Format Text    ${dictWishlistpage}[btn_wishlist_random]    $sku_number=${product_sku}

    CommonWebKeywords.Scroll To Element    ${img_product}
    Mouse Over  ${img_product}
    CommonWebKeywords.Scroll To Element    ${wishlist_btn}
    Mouse Over  ${img_product}

    CommonWebKeywords.Click Element  ${wishlist_btn}
    Set Test Variable  ${product_name}
    Set Test Variable  ${product_sku}

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute JavaScript    window.scrollTo(${x_location},${y_location})

Pagination should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictWishlistpage}[btn_next]

Pagination should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible  ${dictWishlistpage}[btn_next]


Add to bag button should be displayed
    CommonWebKeywords.Scroll To Center Page
    CommonWebKeywords.Scroll To Element    ${dictWishlistpage}[btn_add_to_bag]
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictWishlistpage}[btn_add_to_bag]

Get the number of products displaying on wishlist
    SeleniumLibrary.Wait Until Element Is Visible  ${dictWishlistpage}[lbl_product_number]
    ${text}=  SeleniumLibrary.Get Text  ${dictWishlistpage}[lbl_product_number]
    ${num}=  Get regexp matches  ${text}   \\d+
    [Return]  ${num}[0]

Number of products which are added should be displayed correctly
    ${num}=    Get the number of products displaying on wishlist
    Should Be True  ${num}>=0

Number of products should be 0
    ${num}=    Get the number of products displaying on wishlist
    Should Be True  ${num}==0

Continue shopping button should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictWishlistpage}[btn_continue_shopping]

Click on Continue shopping button 
    CommonWebKeywords.Click Element  ${dictWishlistpage}[btn_continue_shopping]

Verify product is not displayed after deletion
    [Arguments]  ${old_number}
    ${current_num}=  Get the number of products displaying on wishlist
    ${new_num}=  Evaluate  ${old_number}-1
    Should Be True  ${current_num}==${new_num}

Remove the first product
    CommonWebKeywords.Click Element  ${dictWishlistpage}[btn_remove_first_product]
    Wait Until Page Is Completely Loaded

Remove all products on the list 
    ${num}=  Get the number of products displaying on wishlist
    FOR    ${INDEX}    IN RANGE    ${num}
        Remove the first product
    END

