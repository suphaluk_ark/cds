*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/plp_page.robot

*** Keywords ***
Verify product name should be displayed correctly in search product
    [Arguments]  ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_name]    $product_name=${product_name}
    BuiltIn.Run Keyword And Ignore Error    Disable Testability Automatic Wait
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}
    BuiltIn.Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify Product Name Should Not Display In Search Product
    [Arguments]  ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_name]    $product_name=${product_name}
    SeleniumLibrary.Wait Until Page Does Not Contain Element    ${elem}

Click pagination
    [Arguments]    ${number_page}
    ${elem}=    CommonKeywords.Format Text    ${Pagination}[btn_number_page]    $number_page=${number_page}
    CommonWebKeywords.Scroll to bottom page
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}
    CommonWebKeywords.Click Element   ${elem}

Click pagination last page
    CommonWebKeywords.Scroll To Element    ${Pagination}[btn_last_page]
    CommonWebKeywords.Click Element    ${Pagination}[btn_last_page]

Verify pagination should be displayed correctly after click page number 
    [Arguments]    ${number_page}
    ${elem}=    CommonKeywords.Format Text    ${Pagination}[btn_number_page_active]    $number_page=${number_page}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Verify previous arrow is disabled
    CommonWebKeywords.Verify Web Elements Are Visible  ${Pagination}[btn_previousArrow_disable]

Verify previous arrow is enabled
    CommonWebKeywords.Verify Web Elements Are Visible  ${Pagination}[btn_previousArrow_enable]

Verfy next arrow is disabled
    CommonWebKeywords.Verify Web Elements Are Visible  ${Pagination}[btn_nextArrow_disable]

Verfy next arrow is enabled
    CommonWebKeywords.Verify Web Elements Are Visible  ${Pagination}[btn_nextArrow_enable]

Verify pagination should not be displayed in PLP
    CommonWebKeywords.Verify Web Elements Are Not Visible   ${Pagination}[btn_pagination]

Click on arrow-down on view by section
    CommonWebKeywords.Click Element  ${SearchProductDict}[btn_viewBy_arrow_down]

Click on a number from view by section
    [Arguments]    ${number}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_viewBy_number]    $number=${number}
    CommonWebKeywords.Click Element    ${elem}

Click 50 from view by section
    plp_page.Click on a number from view by section  50

Click 100 from view by section
    plp_page.Click on a number from view by section  100

Click 200 from view by section
    plp_page.Click on a number from view by section  200

Count all products in search result page
    ${count}=    SeleniumLibrary.Get Element Count  ${SearchProductDict}[a_count_product]
    ${count}=    Convert To Integer  ${count}
    [Return]    ${count}

Verify the total of products should be displayed correctly
    [Arguments]    ${number}
    ${count}=    plp_page.Count all products in search result page
    Should be Equal As Numbers    ${count}    ${number}

Click on sorter arrow-down button
    CommonWebKeywords.Click Element    ${SearchProductDict}[btn_filter_arrow_down]

Click on price range arrow-down button
    CommonWebKeywords.Click Element    ${SearchProductDict}[btn_price_range_arrow_down]

Click on brand name arrow-down button
    CommonWebKeywords.Click Element    ${SearchProductDict}[btn_brand_name_arrow_down]

Click on size arrow-down button
    CommonWebKeywords.Click Element    ${SearchProductDict}[btn_size_arrow_down]

Click on color arrow-down button
    CommonWebKeywords.Scroll And Click Element    ${SearchProductDict}[btn_color_arrow_down]

Click on category arrow-down button
    CommonWebKeywords.Click Element    ${SearchProductDict}[btn_category_arrow_down]

Select an option from sorter
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[opt_sorter]    $text=${text}
    CommonWebKeywords.Click Element    ${elem}

Select New arrivals from sorter
    plp_page.Select an option from sorter  ${search_result_page.sorter.new_arrival}

Select discount high-low from sorter
    plp_page.Select an option from sorter  ${search_result_page.sorter.discount_high_low}

Select price low-high from sorter
    plp_page.Select an option from sorter  ${search_result_page.sorter.price_low_high}

Click on a brand name
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[opt_brand_name]    $text=${text}
    CommonWebKeywords.Click Element    ${elem}
    common_keywords.Wait Until Page Is Completely Loaded

Click on a size
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[opt_size]    $text=${text}
    CommonWebKeywords.Click Element    ${elem}
    common_keywords.Wait Until Page Is Completely Loaded

Click on a color
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[opt_color]    $text=${text}
    CommonWebKeywords.Click Element    ${elem}
    common_keywords.Wait Until Page Is Completely Loaded

Click on a category
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[opt_category]    $text=${text}
    CommonWebKeywords.Click Element    ${elem}
    common_keywords.Wait Until Page Is Completely Loaded

Verify sub category should be displayed from the category list
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[opt_category]    $text=${text}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Verify sub category should be not displayed from the category list
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[opt_category]    $text=${text}
    CommonWebKeywords.Verify Web Elements Are Not Visible  ${elem}

Verify red check mark should be displayed
    [Arguments]    ${category}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[img_check_mark_category]    $text=${category}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Verify red check mark should be not displayed
    [Arguments]    ${category}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[img_check_mark_category]    $text=${category}
    CommonWebKeywords.Verify Web Elements Are Not Visible  ${elem}

Verify that color of Filter should be red
    ${color}=    CommonWebKeywords.Get Web Element CSS Property Value    ${SearchProductDict}[check_color_filter]  border-bottom
    Should Be Equal  ${color}  ${under_line}

Verify that color of Brand name should be red
    ${color}=    CommonWebKeywords.Get Web Element CSS Property Value    ${SearchProductDict}[check_color_brand_name]  border-bottom
    Should Be Equal  ${color}  ${under_line}

Verify that color of Brand name should be not red
    ${color}=    CommonWebKeywords.Get Web Element CSS Property Value    ${SearchProductDict}[check_color_brand_name]  border-bottom
    Should Not Be Equal  ${color}  ${under_line}

Verify that color of Size should be red
    ${color}=    CommonWebKeywords.Get Web Element CSS Property Value    ${SearchProductDict}[check_color_size]  border-bottom
    Should Be Equal  ${color}  ${under_line}

Verify that color of Color should be red
    ${color}=    CommonWebKeywords.Get Web Element CSS Property Value    ${SearchProductDict}[check_color]  border-bottom
    Should Be Equal  ${color}  ${under_line}

Verify trending search should be not displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible  ${SearchProductDict}[lbl_trending_search]

Verify product image should be not displayed in popup
    SeleniumLibrary.Element Should Not Be Visible    ${SearchProductDict}[product_search_popup]

Verify product image should be displayed in popup
    [Arguments]    ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[img_product]    $product_name=${product_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Count all products in search pop-up
    ${count}=    SeleniumLibrary.Get Element Count  ${SearchProductDict}[img_pop_up_product]
    ${count}=    Convert To Integer  ${count}
    [Return]    ${count}

Verify the product images should be six
    [Arguments]    ${number}=6
    ${count}=    plp_page.Count all products in search pop-up
    Should be Equal As Numbers    ${count}  ${number}

Verify search keyword should be displayed in pop-up
    CommonWebKeywords.Verify Web Elements Are Visible    ${SearchProductDict}[lbl_search_keyword]

Verify categories should be displayed in pop-up
    CommonWebKeywords.Verify Web Elements Are Visible    ${SearchProductDict}[lbl_categories]

Verify recent search should be displayed in pop-up
    CommonWebKeywords.Verify Web Elements Are Visible    ${SearchProductDict}[lbl_recent_search]

Verify a recommendation keyword should be displayed in pop-up
    [Arguments]    ${keyword}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[a_search_keyword]    $keyword=${keyword}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify api should return valid keyword
    [Arguments]    ${keyword}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[pre_keyword_response]    $keyword=${keyword}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click Product Name
    [Arguments]    ${sku}
    ${locator}=    Format Text    ${SearchProductDict}[lnk_view_product]    product_sku=${sku}
    CommonWebKeywords.Click Element    ${locator}    ${timeout}

Get Text Total Found Item
    SeleniumLibrary.Wait Until Element Is Visible    ${SearchProductDict}[product_item]
    SeleniumLibrary.Wait Until Element Is Visible    ${SearchProductDict}[lbl_total_found]
    ${text}=    SeleniumLibrary.Get Text    ${SearchProductDict}[lbl_total_found]
    ${total}=    Split String    ${text}     ${SPACE}
    ${total}=    Convert To Integer    ${total}[0]
    [Return]    ${total}

Get Total Product Item In PLP
    SeleniumLibrary.Wait Until Element Is Visible    ${SearchProductDict}[product_item]
    ${count}=    SeleniumLibrary.Get Element Count    ${SearchProductDict}[product_item]
    [Return]    ${count}

Verify elastic search result is displayed product that contain search keyword
    [Arguments]    ${search_keyword}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[product_flip_card]    $search_keyword=${search_keyword}
    Wait Until Keyword Succeeds    3 x    1s     SeleniumLibrary.Wait Until Page Contains Element    ${elem}
    SeleniumLibrary.Page Should Contain Element      ${elem}

Verify that color of price range should not be red
    ${color}=    CommonWebKeywords.Get Web Element CSS Property Value    ${SearchProductDict}[check_color_price_range]  border-bottom
    Should Not Be Equal  ${color}  ${under_line}

Get list of product name by keyword in popup
    SeleniumLibrary.Wait Until Element Is Visible    ${SearchProductDict}[product_search_popup]
    ${count}=    SeleniumLibrary.Get Element Count    ${SearchProductDict}[product_search_popup]
    ${list_product_popup}=    BuiltIn.Create List
    FOR     ${index}     IN RANGE     1    ${count}+1
        ${product_name}    SeleniumLibrary.Get Text    (${SearchProductDict}[product_search_popup])[${index}]
        Collections.Append To List    ${list_product_popup}    ${product_name}
    END
    [Return]    ${list_product_popup}

Get display configurable's simple product by configurable product sku
    [Arguments]    ${product_configurable_sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[img_product_with_sku_via_api]    $sku=${product_configurable_sku}
    ${product_displayed_sku}=    SeleniumLibrary.Get Element Attribute    ${elem}    data-product-id
    [Return]    ${product_displayed_sku}

Get list display product sku
    [Arguments]    ${range_of_products}=${5}
    ${list_sku}=    Create List
    FOR    ${index}    IN RANGE    ${0}   ${range_of_products}
        ${dict_product_sku}    plp_page.Get display product sku by index    ${index}
        Append To List    ${list_sku}    ${dict_product_sku}
    END
    [Return]    ${list_sku}

Get display product sku by index
    [Arguments]   ${index}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[img_data-product-sku_by_index]    $index=${index}
    CommonWebKeywords.Scroll To Element    ${elem}
    ${product_displayed_sku}=    SeleniumLibrary.Get Element Attribute    ${elem}    data-product-sku
    
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[img_data-product-id_by_index]    $index=${index}
    ${product_displayed_id}=    SeleniumLibrary.Get Element Attribute    ${elem}    data-product-id
    Run Keyword And Ignore Error    Enable Testability Automatic Wait
    
    ${dict_result}    BuiltIn.Create Dictionary
    ...    data-product-sku    ${product_displayed_sku}
    ...    data-product-id    ${product_displayed_id}
    ...    index    ${index}
    [Return]    ${dict_result}

Verify 'search keyword label' should be displayed correctly
    [Arguments]    ${keyword}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_search_result]    $keyword=${keyword}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product found label' should be displayed correctly
    [Arguments]    ${total_product_found}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_total_product_found]    $styles_found=${total_product_found} ${search_result_page.total_found_item}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product img' should be displayed correctly
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[img_product_with_sku_via_api]    $sku=${sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product brand name label' should be displayed correctly
    [Arguments]    ${sku}   ${product_brand_name}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_brand_with_sku_via_api]    $sku=${sku}    $brand_name=${product_brand_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product name label' should be displayed correctly
    [Arguments]    ${sku}   ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_name_with_sku_via_api]    $sku=${sku}    $product_name=${product_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product before discount price' should be displayed correctly
    [Arguments]    ${sku}    ${price}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_before_discount_price_with_sku_via_api]    $sku=${sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${elem}
    ${before_discount_price_ui}    SeleniumLibrary.Get Text    ${elem}
    ${before_discount_price_ui}    CommonKeywords.Convert price to number format    ${before_discount_price_ui}
    BuiltIn.Should Be Equal As Numbers    ${before_discount_price_ui}    ${price}

Verify 'product save price' should be displayed correctly
    [Arguments]    ${sku}    ${evaluate_save_price}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_save_price_with_sku_via_api]    $sku=${sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${elem}
    ${save_discount_price_ui}    SeleniumLibrary.Get Text    ${elem}
    ${save_discount_price_ui}    CommonKeywords.Convert price to number format    ${save_discount_price_ui}
    BuiltIn.Should Be Equal As Numbers    ${save_discount_price_ui}    ${evaluate_save_price}

Verify 'product save price' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_save_price_with_sku_via_api]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify 'product save price percent' should be displayed
    [Arguments]    ${sku}    ${evaluate_percent_discount}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_save_price_percent_with_sku_via_api]    $sku=${sku}
    ${percent_discount_ui}=    SeleniumLibrary.Get Text    ${elem}
    BuiltIn.Should Be Equal As Strings    ${percent_discount_ui}    ${evaluate_percent_discount}%

Verify 'product save price percent' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_save_price_percent_with_sku_via_api]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify 'product sell price' should be displayed correctly
    [Arguments]    ${sku}    ${price}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_price_with_sku_via_api]    $sku=${sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${elem}
    ${price_ui}    SeleniumLibrary.Get Text    ${elem}
    ${price_ui}    CommonKeywords.Convert price to number format    ${price_ui}
    BuiltIn.Should Be Equal As Numbers    ${price_ui}    ${price}

Verify 'product with discount' should be displayed
    [Arguments]    ${sku}
    ${lbl_product_save_price_percent_with_sku}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_save_price_percent_with_sku_via_api]    $sku=${sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${lbl_product_save_price_percent_with_sku}    timeout=${GLOBALTIMEOUT}    error=Data issue : no product with discount displayed.

Verify that category PLP is displayed correctly
    [Arguments]    ${expected_category}
    SeleniumLibrary.Wait Until Page Contains Element    ${dictCategoryPLP}[lbl_clothing]
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictCategoryPLP}[lbl_clothing]    ${expected_category}

Verify 'product promo tag' should be displayed correctly
    [Arguments]    ${sku}    ${promo}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_promo_tag]    $sku=${sku}    $promo=${promo}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product new tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_new_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product only at tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_only_at_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product online exclusive tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_online_exclusive_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product new tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_new_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify 'product only at tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_only_at_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify 'product online exclusive tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[lbl_product_online_exclusive_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify 'filter price range' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${SearchProductDict}[btn_price_range_arrow_down]

Verify 'filter price range value' should be displayed correctly
    [Arguments]    ${min}    ${max}
    ${min}=    Evaluate    int(${min})
    ${max}=    Evaluate    int(${max})

    ${min}=    Convert price to total amount int format    ${min}
    ${max}=    Convert price to total amount int format    ${max}

    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${SearchProductDict}[lbl_price_rage_min]    ฿${min}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${SearchProductDict}[lbl_price_rage_max]    ฿${max}

Verify 'filter brand name' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${SearchProductDict}[btn_brand_name_arrow_down]

Verify 'filter brand name value' should be displayed correctly
    [Arguments]    ${value}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[opt_brand_name]    $text=${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'filter color' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${SearchProductDict}[btn_color_arrow_down]

Click on filter color 'clear' button 
    CommonWebKeywords.Click Element    ${SearchProductDict}[btn_clear_filter_color]

Verify 'product image by data-product-sku, data-product-id' should be displayed
    [Arguments]    ${data-product-sku}    ${data-product-id}    ${image_src}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[img_product_by_data_product_sku_data_product_id]    $data-product-sku=${data-product-sku}    $data-product-id=${data-product-id}    $image_src=${image_src}
    Wait Until Keyword Succeeds    3x    1s    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product image by data-product-sku' should be displayed
    [Arguments]    ${data-product-sku}    ${image_src}
    ${elem}=    CommonKeywords.Format Text    ${SearchProductDict}[img_product_by_data_product_sku]    $data-product-sku=${data-product-sku}    $image_src=${image_src}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click 1st product
    CommonWebKeywords.Click Element    ${SearchProductDict}[lnk_1st_product]
