*** Setting ***
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/2c2p.robot

*** Keywords ***
Click return to merchant now
    Run Keyword And Ignore Error    CommonWebKeywords.Click Element    ${2c2p}[return_to_page]

Get order number from 2C2P page
    CommonWebKeywords.Verify Web Elements Are Visible    ${2c2p}[lbl_order_number]
    ${order_number}=    SeleniumLibrary.Get Text    ${2c2p}[lbl_order_number]
    [return]    ${order_number}

Click on cancel payment button
    Run Keyword If    '${ENV.lower()}'=='staging' or '${ENV.lower()}'=='sit'     CommonWebKeywords.Click Element    ${2c2p}[cancel_btn]
    ...    ELSE IF    '${ENV.lower()}'=='prod'    CommonWebKeywords.Click Element    ${2c2p}[cancel_btn_prod]

Verify 2c2p page displayed
    CommonWebKeywords.Verify Web Elements Are Visible   ${2c2p}[txt_credit_card]    ${2c2p}[txt_cardholder]

Input credit card in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${2c2p}[txt_credit_card]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${2c2p}[txt_credit_card]    ${value}

Input card holder in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${2c2p}[txt_cardholder]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${2c2p}[txt_cardholder]    ${value}

Input CVV in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${2c2p}[txt_cvv]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${2c2p}[txt_cvv]    ${value}

Select expiry year in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${2c2p}[sel_expiry_year]    ${value}

Select on continue payment button
    CommonWebKeywords.Click Element    ${2c2p}[btn_submit]

Submit credit card
    [Arguments]    ${card_number}=${credit_card}[number]
    ...    ${card_holder}=${credit_card}[holder]
    ...    ${card_cvv}=${credit_card}[cvv]
    ...    ${expiry_month}=${credit_card}[month]
    ...    ${expiry_year}=${credit_card}[year]
    Input credit card in 2c2p page    ${card_number}
    Input card holder in 2c2p page    ${card_holder}
    Input CVV in 2c2p page    ${card_cvv}
    Select expiry year in 2c2p page    ${expiry_month}${expiry_year}
    Select on continue payment button

Get OTP from 2c2p page
    CommonWebKeywords.Verify Web Elements Are Visible    ${2c2p}[lal_otp]
    ${otp}=    SeleniumLibrary.Get Text    ${2c2p}[lal_otp]
    ${otp}=    Get Regexp Matches    ${otp}    =\\s([0-9]+)    1
    [Return]    ${otp}[0]

Input OTP in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${2c2p}[txt_otp]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${2c2p}[txt_otp]    ${value}

Select on proceed button in 2c2p page
    CommonWebKeywords.Click Element    ${2c2p}[btn_proceed]

Proceed OTP
    ${otp}=    Get OTP from 2c2p page
    Input OTP in 2c2p page    ${otp}
    Select on proceed button in 2c2p page
    Click return to merchant now

Display product details in 2c2p page
    CommonWebKeywords.Click Element    ${2c2p}[btn_arrow]
    ${elem}=    CommonKeywords.Format Text    ${2c2p}[lbl_product_details]    label=${web_common.product_details}
    CommonWebKeywords.Verify Web Elements Are Visible     ${elem}

Display product amount in 2c2p page
    [Arguments]    ${amount}
    ${elem}=    CommonKeywords.Format Text    ${2c2p}[lbl_product_amount]    label=${web_common.amount}:    amount=${amount}
    CommonWebKeywords.Verify Web Elements Are Visible     ${elem}

Select bangkok bank for installment
    CommonWebKeywords.Click Element    ${installmentpayment}[icon_bank_bbl]

Select citi bank for installment
    CommonWebKeywords.Click Element    ${installmentpayment}[icon_bank_citi]

BBL Select 4 month with 0.8% interest
    CommonWebKeywords.Click Element    ${installmentpayment}[bbl_rdo_4month]

BBL Select 6 month with 0.8% interest
    CommonWebKeywords.Click Element    ${installmentpayment}[bbl_rdo_6month]

BBL Select 10 month with 0.8% interest
    CommonWebKeywords.Click Element    ${installmentpayment}[bbl_rdo_10month]

CITI Select 4 month with 0.8% interest
    CommonWebKeywords.Click Element    ${installmentpayment}[citi_rdo_4month]

CITI Select 6 month with 0.8% interest
    CommonWebKeywords.Click Element    ${installmentpayment}[citi_rdo_6month]

CITI Select 10 month with 0.8% interest
    CommonWebKeywords.Click Element    ${installmentpayment}[citi_rdo_10month]

Click on cancel payment button from installment payment 2c2p page
    CommonWebKeywords.Click Element    ${installmentpayment}[btn_cancelpayment_installment]

Submit credit card in installment payment 2c2p page
    [Arguments]    ${card_number}=${credit_card_kbank}[kbank_number]
    ...    ${card_holder}=${credit_card_kbank}[holder]
    ...    ${card_cvv}=${credit_card_kbank}[cvv]
    ...    ${expiry_month}=${credit_card_kbank}[month]
    ...    ${expiry_year}=${credit_card_kbank}[year]
    Input credit card number for installment payment in 2c2p page    ${card_number}
    Input card holder for installment payment in 2c2p page    ${card_holder}
    Input CVV for installment payment in 2c2p page    ${card_cvv}
    Select expiry month for installment payment in 2c2p page    ${expiry_month}
    Select expiry year for installment payment in 2c2p page    ${expiry_year}
    Select on continue payment button for installment payment in 2c2p page

Click return to merchant button from installment payment 2c2p page
    CommonWebKeywords.Click Element    ${installmentpayment}[btn_return_to_merchant]

Select first installment option
    CommonWebKeywords.Click Element    ${installmentpayment}[rdo_option_kbank]

Accept term and condition for installment payment in 2c2p page
    CommonWebKeywords.Click Element    ${installmentpayment}[chk_term_and_condition]

Select bank for installment payment in 2c2p page
    [Arguments]    ${bankname}
    CommonWebKeywords.Click Element    ${installmentpayment}[icon_bank_${bankname}]
    CommonWebKeywords.Click Element    ${installmentpayment}[rdo_option_${bankname}]

Input credit card number for installment payment in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${installmentpayment}[txt_cardnumber_installment]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${installmentpayment}[txt_cardnumber_installment]    ${value}

Input card holder for installment payment in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${installmentpayment}[txt_cardholder_installment]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${installmentpayment}[txt_cardholder_installment]    ${value}

Select expiry month for installment payment in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${installmentpayment}[ddl_expiry_month_installment]
    Select dropdownlist by label    ${installmentpayment}[ddl_expiry_month_installment]    ${value}

Select expiry year for installment payment in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${installmentpayment}[ddl_expiry_year_installment]
    Select dropdownlist by label    ${installmentpayment}[ddl_expiry_year_installment]    ${value}

Input CVV for installment payment in 2c2p page
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${installmentpayment}[txt_cvv_installment]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${installmentpayment}[txt_cvv_installment]    ${value}

Select on continue payment button for installment payment in 2c2p page
    CommonWebKeywords.Click Element    ${installmentpayment}[btn_continuepayment_installment]

Process pay via 2c2p installment plan with CITI BANK 4 month
    2c2p.Select citi bank for installment
    2c2p.CITI Select 4 month with 0.8% interest
    2c2p.Accept term and condition for installment payment in 2c2p page
    2c2p.Submit credit card in installment payment 2c2p page
    2c2p.Click return to merchant button from installment payment 2c2p page
