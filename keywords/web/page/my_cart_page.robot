*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/my_cart_page.robot

*** Variables ***
${cart_page_locator}    ${web_common.windows_locator_cart_title_cds}
${timeout}    10s

*** Keywords ***
Click view promo code button
    CommonWebKeywords.Click Element    ${dictMyCartPage}[btn_view_coupon]

Coupon code should be displayed in view coupon code popup
    [Arguments]    ${title}    ${coupon}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_view_coupon]    $title=${title}    $coupon=${coupon}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}
    CommonWebKeywords.Click Element    ${dictMyCartPage}[btn_close_popup]

Coupon code should be removed
    [Arguments]    ${title}    ${coupon}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_view_coupon]    $title=${title}    $coupon=${coupon}
    SeleniumLibrary.Wait Until Element Is Not Visible    ${elem}
    CommonWebKeywords.Click Element    ${dictMyCartPage}[btn_close_popup]

Verify that coupon code should be displayed correctly
    [Arguments]    ${title}    ${coupon}
    Click view promo code button
    Coupon code should be displayed in view coupon code popup    ${title}    ${coupon}

Verify that coupon code should be removed
    [Arguments]    ${title}    ${coupon}
    Click view promo code button
    Coupon code should be removed    ${title}    ${coupon}

Verify total price with quantity, summarize multiple product in cart page
    [Arguments]    @{product_sku}
    ${summary_price}=    Create List
    FOR    ${product}    IN    @{product_sku}
        ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_quantity]    $product_sku=${product}
        SeleniumLibrary.Wait Until Element Is Visible    ${elem}    ${GLOBALTIMEOUT}
        ${quantity}=    SeleniumLibrary.Get Text    ${elem}
        Set Test Variable    ${quantity_${product}}    ${quantity}
        ${product_price}=    Convert To Number    ${${product}}[price]
        Summarize price with quantity    ${quantity_${product}}    ${product_price}
        Append to List    ${summary_price}    ${price_with_quantity}
    END
    ${total_price_with_quantity}=    Evaluate    sum($summary_price)
    ${total_price_with_quantity_format}=    Run Keyword If    ${total_price_with_quantity}.is_integer()    Convert To Integer    ${total_price_with_quantity}
    ...    ELSE IF    Convert price to total amount format    ${total_price_with_quantity}
    Set Test Variable    ${total_price_with_quantity}    ${total_price_with_quantity_format}

Product should be displayed in my cart page
    [Arguments]    ${sku_number}   ${product_name}
    common_keywords.Wait Until Page Is Completely Loaded
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_product_name]    $product_sku=${sku_number}    $product_name=${product_name}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}

Verify total price with quantity in cart page
    [Arguments]    ${product_price}    ${product_quantity}
    Summarize price with quantity    ${product_quantity}    ${product_price}
    SeleniumLibrary.Element Text Should Be    ${dictMyCartPage}[lbl_items]    ฿${price_with_quantity_format}

Verify e-coupon discount in cart page(%)
    [Arguments]    ${coupon}    ${total_price}    ${discount}
    Calculate discount(%) from original total price    ${total_price}    ${discount}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_coupon_code]    $coupon=${coupon}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}
    SeleniumLibrary.Element Text Should Be    ${elem}    -฿${discount_amount_with_format}

Verify e-coupon discount in cart page(Baht)
    [Arguments]    ${coupon}    ${total_price}    ${discount}
    Calculate discount(Bath) from original total price    ${total_price}    ${discount}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_coupon_code]    $coupon=${coupon}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}
    SeleniumLibrary.Element Text Should Be    ${elem}    -฿${discount_amount_with_format}

Verify other discount in cart page(%)
    [Arguments]    ${total_price}    ${discount}
    Calculate discount(%) from original total price    ${total_price}    ${discount}
    Verify other discount in cart page(Baht)    ${discount_amount_with_format}

Verify other discount in cart page(Baht)
    [Arguments]    ${discount}
    ${discount_amount}=    Convert Price To Us Format    ${discount}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyCartPage}[other_discount]
    ${text}=    SeleniumLibrary.Get Text    ${dictMyCartPage}[other_discount]
    ${actual_discount}=    CommonKeywords.Convert price to number format    ${text}
    Verify expected and actual discount amount is the same value    ${actual_discount}    ${discount}

Click Secure Checkout Button
    Wait Until Page Loader Is Not Visible
    CommonWebKeywords.Click Element Using Javascript    ${dictMyCartPage}[btn_checkout]
    Wait Until Page Is Completely Loaded

Wait Until Page Loader Is Not Visible
    Seleniumlibrary.Wait Until Page Does Not Contain Element    ${dictMyCartPage}[page_load]    timeout=${GLOBALTIMEOUT}
    Wait Until Page Is Completely Loaded

Click on free gift message
    CommonWebKeywords.Scroll And Click Element    ${dictMyCartPage}[txt_free_gift]

Free gift message should be displayed in cart page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyCartPage}[txt_free_gift]

Free gift product should be displayed in cart page
    [Arguments]    ${product}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_free_gift_product]    $product=${product}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Apply coupon code
    [Arguments]    ${coupon_code}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyCartPage}[txt_apply_coupon]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictMyCartPage}[txt_apply_coupon]    ${coupon_code}
    CommonWebKeywords.Scroll And Click Element    ${dictMyCartPage}[btn_apply_coupon]
    my_cart_page.Wait Until Page Loader Is Not Visible

Coupon Error Message Should Display
    SeleniumLibrary.Wait Until Element Is Visible    ${dictMyCartPage}[lbl_coupon_error_msg]

####### for calculate price ########
Display cart page
    [Arguments]    @{productList}
    Display Shopping cart section    @{productList}

Display shopping cart section
    [Arguments]    @{productList}
    ${elem}=    CommonKeywords.Format Text    ${cart}[label]    label=${web_common}[shopping_cart]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    Display product item in shopping cart page    @{productList}

Display product item in shopping cart page
    [Documentation]    This keyword receive SKU number as a list that looping verify each product picture,product description and product amount
    ...    After calling this keyword ‘test variable’ sum_per_product_price would be assigned
    [Arguments]    @{productList}
    ${price_list}    Create List
    FOR    ${product}    IN    @{productList}
        ${elem}=    CommonKeywords.Format Text    ${cart}[img_productItem]    skuNumber=${product}
        CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
        ${elem}=    CommonKeywords.Format Text    ${cart}[lal_product_description]    skuNumber=${product}
        CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
        ${elem}=    SeleniumLibrary.Get WebElement    ${elem}
        ${elem_text}=    SeleniumLibrary.Get Text    ${elem}
        ${elem}=    CommonKeywords.Format Text    ${cart}[lal_quantity]    skuNumber=${product}
        ${quantity}=    SeleniumLibrary.Get Value    ${elem}
        Display product description in shopping cart page    ${elem_text}    ${product}    ${quantity}
        Append To List    ${price_list}    ${price_with_quantity}
    END
    Set Test Variable    ${sum_per_product_price}    ${price_list}

Display product description in shopping cart page
    [Documentation]    This keyword receive text from web element and use Test Variable from calculation.robot
    [Arguments]    ${elem_text}    ${skuNumber}    ${quantity}
    ${product}=    Set Variable    ${${skuNumber}}
    Subtract total price from vat    ${product}[price]
    calculation.Summarize price with VAT    ${total_without_vat}
    calculation.Summarize price with quantity    ${quantity}
    FOR    ${text}    IN    ${product}[name]
        ...    ${web_common}[product_code]: ${skuNumber}
        ...    ฿ ${round_total_without_vat_format}
        ...    ฿ ${price_with_vat_format}
        ...    ${web_common}[total] VAT
        ...    ฿ ${price_with_quantity_format}
        Should be True    $text in $elem_text
    END

Select on delete button in cart page
    [Arguments]    ${sku_number}
    ${elem}=    CommonKeywords.Format Text    ${cart}[btn_delete]    skuNumber=${sku_number}
    CommonWebKeywords.Click Element    ${elem}

Display product amount in cart page
    [Arguments]    @{product_list}
    ${len}=    Get Length    ${product_list}
    CommonWebKeywords.Verify Web Elements Are Visible    ${cart}[img_products]
    ${elems}=    SeleniumLibrary.Get WebElements    ${cart}[img_products]
    ${actual_amount}=    Get Length    ${elems}
    Should be True    ${len} == ${actual_amount}

Remove and display product amount in cart page
    [Arguments]    ${sku_number}    @{product_list}
    Select on delete button in cart page    ${sku_number}
    Select on remove in modal cart
    Display product amount in cart page    @{product_list}

Display delete in modal cart
    CommonWebKeywords.Verify Web Elements Are Visible    ${cart}[modal_delete]
    ${text}=    SeleniumLibrary.Get Text    ${cart}[modal_delete]
    Should be True    $delete_item.message1 in $text
    Should be True    $delete_item.message2 in $text

Select on remove in modal cart
    Display delete in modal cart
    ${elem}=    CommonKeywords.Format Text    ${cart}[lal_delete_modal]    label=${web_common.remove.upper()}
    CommonWebKeywords.Click Element    ${elem}

Remove product quantity in cart page
    [Arguments]    ${sku_number}
    ${elem_qty}=    CommonKeywords.Format Text    ${cart}[lal_quantity]    skuNumber=${sku_number}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem_qty}
    ${befor_remove}=    SeleniumLibrary.Get Value    ${elem_qty}
    ${elem}=    CommonKeywords.Format Text    ${cart}[btn_remove_qty]    label=${sku_number}
    CommonWebKeywords.Click Element    ${elem}
    ${after_remove}=    SeleniumLibrary.Get Value    ${elem_qty}
    Should Be True    ${${befor_remove}-1}==${after_remove}

Remove product from shopping bag
    [Arguments]    ${sku_number}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lnk_remove_product]    $sku_number=${sku_number}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}
    CommonWebKeywords.Click Element  ${elem}
    CommonWebKeywords.Verify Web Elements Are Not Visible   ${elem}

Verify shopping bag page should be displayed
    common_keywords.Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictMyCartPage}[lbl_cart_page_title]

Verify brand of product should be displayed on cart page
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictMyCartPage}[lbl_product_brands]

Verify product name should be displayed on cart page
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictMyCartPage}[lbl_product_name]

Verify quantity of product should be displayed
    @{sku_list}=  Get sku list on cart page
    FOR    ${sku}    IN    @{sku_list}
         ${text}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_sub_quantity]    $product_sku=${sku}
         CommonWebKeywords.Verify Web Elements Are Visible  ${text}
    END

Verify quantity of product should be displayed correctly
    [Arguments]    ${product_sku}    ${quantity}
    ${number}=    Get item quantity by product sku    ${product_sku}
    Should Be Equal As Strings    ${number}    ${quantity}

Verify coupon of product should be displayed after apply
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyCartPage}[lbl_coupon]

Verify FREE standard delivery message
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyCartPage}[msg_free_deliver]
    ${msg}=    SeleniumLibrary.Get Text    ${dictMyCartPage}[msg_free_deliver]
    Should Be Equal As Strings    ${cart_page.delivery_message.free_shipping}    ${msg}

Get grand price total
    SeleniumLibrary.Wait Until Page Contains Element    ${dictMyCartPage}[lbl_grand_total]
    ${text}=    SeleniumLibrary.Get Text    ${dictMyCartPage}[lbl_grand_total]
    ${price}=    CommonKeywords.Convert price to number format  ${text}
    [Return]    ${price}

Verify grand total displayed correctly
    [Arguments]    ${price}
    ${actual_price}=    Get grand price total
    Should Be Equal As Numbers    ${actual_price}    ${price}

Get item quantity by product sku
    [Arguments]    ${product_sku}
    ${quantity_elem}=  CommonKeywords.Format Text    ${dictMyCartPage}[ddl_quantity]    $skunumber=${product_sku}
    ${quantity}=  SeleniumLibrary.Get Selected List Value  ${quantity_elem}
    [Return]  ${quantity}

Get item sub-total by product sku
    [Arguments]    ${product_sku}
    ${text}=  CommonKeywords.Format Text    ${dictMyCartPage}[lbl_sub_total]    $product_sku=${product_sku}
    ${sub_total}=  SeleniumLibrary.Get Text  ${text}
    ${sub_total}=  CommonKeywords.Convert price to number format  ${sub_total}
    [Return]  ${sub_total}

Get price item total by product sku
    [Arguments]    ${product_sku}
    ${text}=  CommonKeywords.Format Text    ${dictMyCartPage}[lbl_price]    $product_sku=${product_sku}
    ${price_total}=  SeleniumLibrary.Get Text  ${text}
    ${price_total}=  CommonKeywords.Convert price to number format  ${price_total}
    [Return]  ${price_total}

Get discount on cart page
    ${elem}=  SeleniumLibrary.Get Text  ${dictMyCartPage}[other_discount]
    ${discount}=  CommonKeywords.Convert price to number format  ${elem}
    [Return]  ${discount}

Get price grand total on cart page
    SeleniumLibrary.Wait Until Element Is Visible    ${dictMyCartPage}[lbl_grand_total]    ${timeout}
    ${elem}=    SeleniumLibrary.Get Text    ${dictMyCartPage}[lbl_grand_total]
    ${grand_total}=    CommonKeywords.Convert price to number format    ${elem}
    [Return]    ${grand_total}

Get price sub-total on cart page
    SeleniumLibrary.Wait Until Element Is Visible    ${dictMyCartPage}[lbl_price_total]    ${timeout}
    ${elem}=  SeleniumLibrary.Get Text  ${dictMyCartPage}[lbl_price_total]
    ${grand_total}=  CommonKeywords.Convert price to number format  ${elem}
    [Return]  ${grand_total}

Get sku list on cart page
    SeleniumLibrary.Wait Until Page Contains Element    ${dictMyCartPage}[lbl_remove_all]    ${timeout}
    @{list}=  SeleniumLibrary.Get WebElements  ${dictMyCartPage}[lbl_remove_all]
    @{sku_list}=  Create List
    FOR    ${elem}    IN   @{list}
         ${id}=  SeleniumLibrary.Get Element Attribute  ${elem}  id
         ${sku}=  Remove String  ${id}  btn-removeProduct-
         Append To List  ${sku_list}  ${sku}
    END
    [Return]  ${sku_list}

Get sum total quantity of all products displaying on cart page
    @{sku_list}=  Get sku list on cart page
    ${total}=  Set Variable  0
    FOR    ${sku}    IN    @{sku_list}
         ${text}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_sub_quantity]    $product_sku=${sku}
         ${quantity}=    SeleniumLibrary.Get Text  ${text}
         ${total}=  Evaluate  ${total} + int(${quantity})
    END
    [Return]  ${total}

Get product total quantity displaying at order summary
    ${text}=   SeleniumLibrary.Get Text  ${dictMyCartPage}[lbl_count_product]
    ${quantity}=  Get regexp matches  ${text}   \\d+
    [Return]  ${quantity}[0]

Verify total quantity must be displayed correctly
    ${count}=  Get sum total quantity of all products displaying on cart page
    ${total}=  Get product total quantity displaying at order summary
    Should Be Equal As Strings  ${count}  ${total}

Verify message if add item has total price less than 699 THB
    ${grand_total}=  Get grand price total
    ${extra_number}=  Evaluate  699-${grand_total}
    ${buy_more_msg}=    CommonKeywords.Format Text    ${cart_page.delivery_message.non_free_shipping}    $number=${extra_number}
    ${msg}=    SeleniumLibrary.Get Text    ${dictMyCartPage}[msg_spend_more]
    Should Be Equal As Strings    ${buy_more_msg}    ${msg}

Change quantity of product from the drop-down list
    [Arguments]    ${skunumber}    ${quantity}
    Set Test Variable    ${product_quantity}    ${quantity}
    ${elem}=  CommonKeywords.Format Text    ${dictMyCartPage}[ddl_quantity]    $skunumber=${sku_number}
    CommonWebKeywords.Click Element    ${elem}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[list_qty]    $skunumber=${sku_number}    $number=${quantity}
    Scroll To Element   ${elem}
    Wait Until Keyword Succeeds    3 x    2s    CommonWebKeywords.Click Element    ${elem}

Verify sub-total is displayed correctly on cart page
    [Arguments]    ${product_sku}
    ${quantity}=  Get item quantity by product sku  ${product_sku}
    ${price}=  Get price item total by product sku  ${product_sku}
    ${sub_total}=  Evaluate  ${quantity}*${price}
    ${sub_total_display}=  Get item sub-total by product sku  ${product_sku}
    Should Be Equal As Strings    ${sub_total}  ${sub_total_display}

Verify sub-total of multiple items is displayed correcty on cart page
    [Arguments]   @{product_sku}
    ${length}=  Get Length  ${product_sku}
    FOR  ${index}    IN RANGE   0    ${length}
         Verify sub-total is displayed correctly on cart page  @{product_sku}[${index}]
    END

Verify price sub-total should be displayed correctly (not include promo discount)
    [Arguments]  @{product_sku}
    ${length}=  Get Length  ${product_sku}
    ${total}=  Set Variable  0
    FOR  ${index}    IN RANGE   0    ${length}
         ${item_price_total}=  Get item sub-total by product sku  @{product_sku}[${index}]
         ${total}=  Evaluate  ${total}+${item_price_total}
    END
    ${sub_total}=  Get price sub-total on cart page
    Should Be Equal As Strings  ${sub_total}  ${total}

Verify price grand total should be caculated on cart page correctly
    ${price_sub_total}=  Get price sub-total on cart page
    ${status}  ${discount}=  Run Keyword And Ignore Error    Get discount on cart page
    ${price_grand_total}=  Run Keyword If  '${status}' == 'PASS'  Evaluate  ${price_sub_total}+${discount}
        ...  ELSE  BuiltIn.Set Variable    ${price_sub_total}
    ${grand_total}=  Get price grand total on cart page
    Should Be Equal As Strings  ${price_grand_total}  ${grand_total}

Get new product sku list after removing a product
    [Arguments]  ${product_sku}  @{product_sku_list}
    ${list}=  Create List
    FOR    ${item}  IN  @{product_sku_list}
         ${status}=  Evaluate  '${product_sku}'!='${item}'
         Run Keyword If  '${status}' == 'True'  Collections.Append To List  ${list}  ${item}
    END
    [Return]  ${list}

Verify free gift product in cart page
    [Arguments]    ${free_gift_product}
    my_cart_page.Free gift message should be displayed in cart page
    my_cart_page.Click on free gift message
    my_cart_page.Free gift product should be displayed in cart page    ${free_gift_product}

Verify not enough free gift message should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictMyCartPage}[msg_not_enough_free_gift]

Verify free gift image should be displayed on cart page
    [Arguments]  ${gift_sku}
    ${elem}=  CommonKeywords.Format Text    ${dictMyCartPage}[img_free_gift]    $gift_sku=${gift_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Verify free gift brand name should be displayed on cart page
    [Arguments]  ${gift_sku}
    ${elem}=  CommonKeywords.Format Text    ${dictMyCartPage}[lbl_free_gift_brand_name]    $gift_sku=${gift_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Verify free gift product name should be displayed on cart page
    [Arguments]  ${gift_sku}
    ${elem}=  CommonKeywords.Format Text    ${dictMyCartPage}[lbl_free_gift_product_name]    $gift_sku=${gift_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Verify free gift size or color should be displayed on cart page
    [Arguments]  ${gift_sku}
    ${elem}=  CommonKeywords.Format Text    ${dictMyCartPage}[lbl_free_gift_size]    $gift_sku=${gift_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Verify free gift quantity should be displayed on cart page
    [Arguments]  ${gift_sku}
    ${elem}=  CommonKeywords.Format Text    ${dictMyCartPage}[lbl_free_gift_quantity]    $gift_sku=${gift_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Verify free gift price should be displayed on cart page
    [Arguments]  ${gift_sku}
    ${elem}=  CommonKeywords.Format Text    ${dictMyCartPage}[lbl_free_gift_price]    $gift_sku=${gift_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Verify coupon code display in cart
    [Arguments]    ${coupon_code}
    ${coupon_code_at_cart}=    CommonKeywords.Format Text    ${dictMyCartPage}[txt_coupon_code_display_at_cart]    $coupon_code=${coupon_code}
    CommonWebKeywords.Verify Web Elements Are Visible    ${coupon_code_at_cart}

Verify product sku display on cart
    [Arguments]    ${product_sku}
    ${product_sku_cart}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_quantity]    $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${product_sku_cart}

Verify discount amount is displayed correctly after apply e-coupon
    [Arguments]    ${coupon}    ${coupon_discount_amount}    ${product_price}    ${qty}=1
    ${total_price}=    calculation.Calculate total price by summary product price with quantity    ${product_price}    ${qty}
    Verify e-coupon discount in cart page(Baht)    ${coupon}    ${total_price}    ${coupon_discount_amount}

Verify grand total price is display correctly after deduct discount for multiple items
    [Documentation]    This keyword receive the discount amount and dictionary of SKU and Qty that looping summarize price Multipiler number of Qty
    ...    And subtract with discount amount and verify grand total displayed correctly
    ...    Example=> Verify grand total price is display correctly after deduct discount for multiple items    100    CDS10395578=2    CDS11875437=1
    [Arguments]    ${total_discount}    &{product_qty}
    ${total_price}=    Set Variable    0
    FOR    ${product}    ${qty}    IN    &{product_qty}
        ${each_item_price}=    Calculate total price by summary product price with quantity    ${${product}}[price]    ${qty}
        ${total_price}=    Evaluate    ${total_price} + ${each_item_price}
    END
    ${grand_total_price}=    calculation.Calculate grand total price by deduct discount amount already    ${total_price}    ${total_discount}
    Verify grand total displayed correctly    ${grand_total_price}

Verify grand total price is display correctly after deduct discount
    [Arguments]    ${total_discount}    ${product_price}    ${qty}=1
    ${total_price}=    calculation.Calculate total price by summary product price with quantity    ${product_price}    ${qty}
    ${grand_totoal_price}=    calculation.Calculate grand total price by deduct discount amount already    ${total_price}    ${total_discount}
    Verify grand total displayed correctly    ${grand_totoal_price}

Verify button apply coupon is disabled after apply coupon code
    ${background_color}=    CommonWebKeywords.Get Web Element CSS Property Value    ${dictMyCartPage}[btn_apply_coupon]    background-color
    Should Be Equal As Strings    ${colors.grey_disable}    ${background_color}

Verify that the specific card popup is displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyCartPage}[txt_specific_card]
    CommonWebKeywords.Click Element    ${dictMyCartPage}[btn_ok]

Verify credit card on top discount promotion is displayed on cart page correctly
    [Arguments]    ${amount}    ${discount}
    calculation.Calculate discount(%) from original total price    ${amount}    ${discount}
    CommonWebKeywords.Verify Web Elements Are Visible   ${dictMyCartPage}[lbl_credit_card_discount]
    SeleniumLibrary.Element Text Should Be    ${dictMyCartPage}[lbl_credit_card_discount]    -฿${discount_amount_with_format}

Verify specific card coupon promotion is displayed on cart page correctly
    [Arguments]    ${coupon}    ${product_price}    ${quantity}    ${discount}
    ${total_price}=    calculation.Calculate total price by summary product price with quantity    ${product_price}    ${quantity}
    Verify e-coupon discount in cart page(%)    ${coupon}    ${total_price}    ${discount}
    ${total_price_after_deduct}=    Calculate grand total price by deduct discount amount already    ${total_price}    ${discount_amount}
    Set Test Variable    ${total_price_after_deduct}    ${total_price_after_deduct}

Verify that coupon code can be removed and re-applied normally
    [Arguments]    ${coupon_code}    ${product_price}    ${qty}    ${percent_discount.five}    ${percent_discount.ten}
    ${remove_coupon}=    CommonKeywords.Format Text    ${dictMyCartPage}[btn_remove_coupon]    $coupon_code=${coupon_code}
    CommonWebKeywords.Click Element    ${remove_coupon}
    my_cart_page.Apply coupon code    ${coupon_code}
    my_cart_page.Verify that the specific card popup is displayed
    my_cart_page.Verify specific card coupon promotion is displayed on cart page correctly    ${coupon_code}    ${product_price}    ${qty}    ${percent_discount.ten}
    my_cart_page.Verify credit card on top discount promotion is displayed on cart page correctly    ${total_price_after_deduct}    ${percent_discount.five}

Verify other discount amount is display correct after apply promotion rule
    [Arguments]    ${expect_discount}
    ${actual_discount}=    Get discount on cart page
    Verify expected and actual discount amount is the same value    ${actual_discount}    ${expect_discount}

Verify expected and actual discount amount is the same value
    [Arguments]    ${actual_discount}    ${expected_discount}
    #Due to "Get discount on cart page" keyword return minus value, then the next step "evaluate" will be used plus logic to return a positive number
    ${discount_diff}=    Evaluate    ${expected_discount}+${actual_discount}
    Should Be True    ${discount_diff}==${0}

Verify other discount should not display
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictMyCartPage}[other_discount]

Verify auto discount by percent(%) is displayed amount correctly
    [Arguments]    ${precent_discount}    ${product_price}    ${qty}=1
    Wait Until Page Loader Is Not Visible
    ${total_price}=    calculation.Calculate total price by summary product price with quantity    ${product_price}    ${qty}
    Verify other discount in cart page(%)    ${total_price}    ${precent_discount}

Verify auto discount and grand total is displayed amount correctly after apply mulitple promotion rule
    [Arguments]    ${fix_discount}    ${precent_discount}    ${product_price}    ${qty}=1
    ${total_price}=    calculation.Calculate total price by summary product price with quantity    ${product_price}    ${qty}
    Calculate discount(%) from original total price    ${total_price}    ${precent_discount}
    ${expected_discount}=    Evaluate    ${discount_amount_with_format}+${fix_discount}
    Verify other discount in cart page(Baht)    ${expected_discount}
    Verify grand total price is display correctly after deduct discount    ${expected_discount}    ${product_price}    ${qty}

Verify auto discount by fix amount is displayed amount correctly
    [Arguments]    ${fix_discount}    ${product_price}    ${qty}=1
    Wait Until Page Loader Is Not Visible
    ${total_price}=    calculation.Calculate total price by summary product price with quantity    ${product_price}    ${qty}
    Verify other discount in cart page(Baht)    ${fix_discount}

Verify auto discount and grand total is displayed amount correctly after apply mulitple fixed discount promotion rules
    [Arguments]    ${fix_discount_1}    ${fix_discount_2}    ${product_price}    ${qty}=1
    ${total_price}=    calculation.Calculate total price by summary product price with quantity    ${product_price}    ${qty}
    ${expected_discount}=    Evaluate    round(${fix_discount_1}+${fix_discount_2})
    ${expected_discount_integer}=    Convert To Integer    ${expected_discount}
    Verify other discount in cart page(Baht)    ${expected_discount_integer}
    Verify grand total price is display correctly after deduct discount    ${expected_discount_integer}    ${product_price}    ${qty}

Verify auto discount by fix price is displayed amount correctly
    [Arguments]    ${fix_discount}    ${product_price_1}    ${qty_1}    ${product_price_2}    ${qty_2}
    Wait Until Page Loader Is Not Visible
    ${total_price_product_1}=    calculation.Calculate total price by summary product price with quantity    ${product_price_1}    ${qty_1}
    ${total_price_product_2}=    calculation.Calculate total price by summary product price with quantity    ${product_price_2}    ${qty_2}
    ${total_price}=    Evaluate    ${total_price_product_1}+${total_price_product_2}
    Verify other discount in cart page(Baht)    ${fix_discount}
    Set Test Variable    ${total_price}    ${total_price}

Verify grand total price is display correctly after deduct discount from bundle promotion
    [Arguments]    ${fix_discount}    ${total_price}
    ${grand_total_price}=    calculation.Calculate grand total price by deduct discount amount already    ${total_price}    ${fix_discount}
    Verify grand total displayed correctly    ${grand_total_price}

Click 'free items with order purchase' link
    CommonWebKeywords.Scroll And Click Element    ${dictMyCartPage}[lbl_free_items_with_order_purchase]

Verify 'free items with order purchase' should be display correctly
    [Arguments]    ${product_sku}    ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_free_items_with_order_purchase_product]    $product_sku=${product_sku}    $product_name=${product_name}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}

Click 'free gift with this item purchase' link
    [Arguments]    ${purchase_item_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_free_gift_with_this_item_purchase]    $purchase_item_sku=${purchase_item_sku}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Verify 'free gift with this item purchase' should be display correctly
    [Arguments]    ${purchase_item_sku}    ${free_gift_product_name}
    ${elem}=    CommonKeywords.Format Text    ${dictMyCartPage}[lbl_free_gift_with_this_item_purchase_product]    $purchase_item_sku=${purchase_item_sku}    $free_gift_product_name=${free_gift_product_name}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}
