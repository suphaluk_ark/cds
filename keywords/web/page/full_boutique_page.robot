*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/full_boutique_page.robot

*** Keywords ***
Chanel - click category img
    [Arguments]    ${category_name}
    ${category_name}=    String.Convert To Uppercase    ${category_name}
    ${elem}=    CommonKeywords.Format Text    ${dictFullBoutiquePage}[img_category_chanel]    $category_name=${category_name}
    CommonWebKeywords.Scroll And Click Element    ${elem}
    
Dior - click product img
    [Arguments]    ${product_name}
    ${product_name}=    String.Convert To Uppercase    ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${dictFullBoutiquePage}[img_product_name_dior]    $product_name=${product_name}
    CommonWebKeywords.Scroll And Click Element    ${elem}
