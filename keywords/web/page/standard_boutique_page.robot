*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/standard_boutique_page.robot

*** Keywords ***
Click on Shop All Product link
    [Arguments]    ${brand_name}
    ${elem}=    CommonKeywords.Format Text    ${StandardBoutiquePageDict}[lnk_shop_all_product]    $brand_name=${brand_name}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Verify correct brand name on PLP
    [Arguments]    ${brand_name}
    ${brand_name}=    Convert To Upper Case    ${brand_name}
    ${elem}=    CommonKeywords.Format Text    ${StandardBoutiquePageDict}[lbl_brand_name]    $brand_name=${brand_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}