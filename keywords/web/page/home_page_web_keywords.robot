#Please do not put any keyword here 
#We will use home_page.robot instead of this file

*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/home_page.robot

*** Variables ***
${timeout}    10s

*** Keywords ***
Search product by product sku
    [Arguments]    ${product}
    Run Keyword And Ignore Error    Click close button in search box    
    Input text into search box    ${product}
    Select product in search box popup    ${product}

Search product by product name
    [Arguments]    ${product_name}
    Run Keyword And Ignore Error    Click close button in search box    
    Input text into search box    ${product_name}
    Select product name in search box popup    ${product_name}

Click on search box
   CommonWebKeywords.Click Element     ${search_box}

Input text into search box
    [Arguments]    ${text}
    CommonWebKeywords.Click Element    ${search_box}
    CommonWebKeywords.Input Text And Verify Input For Web Element     ${search_box}    ${text}

Press Enter key to search a product
    SeleniumLibrary.Press Keys    None    RETURN

Product should be displayed in search box popup
    [Arguments]    ${product}
    ${product_locator}=    CommonKeywords.Format Text    ${lbl_product_list}     $product=${product}
    CommonWebKeywords.Verify Web Elements Are Visible     ${product_locator}

Select product in search box popup
    [Arguments]    ${product}
    ${product_locator}=    CommonKeywords.Format Text    ${lbl_product_list}     $product=${product}
    CommonWebKeywords.Click Element     ${product_locator}

Select product name in search box popup
    [Arguments]    ${product_name}
    ${product_locator}=    CommonKeywords.Format Text    ${lbl_product_list_name}     $product_name=${product_name}
    CommonWebKeywords.Click Element     ${product_locator}

Close Popup
    SeleniumLibrary.Wait Until Page Contains Element    ${popup}
    SeleniumLibrary.Click Element    ${popup}

Click close button in search box
    SeleniumLibrary.Wait Until Page Contains Element    ${btn_search_close}
    SeleniumLibrary.Click Element    ${btn_search_close}

Verify homepage page should be displayed
    SeleniumLibrary.Wait Until Page Contains Element    ${home_page_title}    ${timeout}

Verify Best Seller Section Should Display
    Run Keyword If    '${BU.lower()}'=='${bu_type.rbs}'    Refresh page
    Wait Until Page Is Completely Loaded
    Scroll To Element    ${homepage_bestseller_section}
    SeleniumLibrary.Wait Until Element Is Visible    ${homepage_bestseller_section}    ${timeout}

Verify Hot Deal Section Should Display
    Scroll To Element    ${homepage_hotdeal_section}
    SeleniumLibrary.Wait Until Element Is Visible    ${homepage_hotdeal_section}    ${timeout}

Verify Hot Deal Show 20 Items
    SeleniumLibrary.Wait Until Page Contains Element    ${homepage_hotdeal_list}    ${timeout}
    ${count}=    SeleniumLibrary.Get Element Count    ${homepage_hotdeal_list}
    Should Be Equal As Integers    ${count}    20

Verify Hot Deal Should Contain Product
    [Arguments]    ${product_name}
    ${locator}=    Format Text    ${homepage_hotdeal_item}    product_name=${product_name}

Click women category in main menu
    SeleniumLibrary.Wait Until Page Contains Element    ${btn_women_category}
    CommonWebKeywords.Click Element    ${btn_women_category}

Click on brand
    CommonWebKeywords.Click Element     ${lbl_brand}