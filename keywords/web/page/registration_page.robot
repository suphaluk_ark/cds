*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/registration_page.robot

*** Variables ***
${PREFIX_EMAIL}   Robot
${SUFFIX_EMAIL}   @central.tech

*** Keywords ***
Get Unique Value From Datetime
    ${current_datetime}=    Get Time
    ${current_datetime}=    Remove String    ${current_datetime}    :    -    ${SPACE}
    ${milliseconds}=    Evaluate    int(round(time.time() * 10000))    time
    ${milliseconds}=    Convert To String    ${milliseconds}
    ${current_datetime}=    Set Variable    ${current_datetime}${milliseconds[10:14]}
    Log to console    Random value is : ${current_datetime}
    [Return]    ${current_datetime}

Generate Random Email
    ${random_value}=    Get Unique Value From Datetime
    ${email}    Set Variable    ${PREFIX_EMAIL}${random_value}${SUFFIX_EMAIL}
    [Return]    ${email}

Input Email
    [Arguments]    ${email}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_email]
    SeleniumLibrary.Input text    ${dictRegistrationPage}[txt_email]    ${email}

Input guest email
    [Arguments]    ${email}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_guest_email]
    SeleniumLibrary.Input text    ${dictRegistrationPage}[txt_guest_email]    ${email}

Input Password For Register Page
    [Arguments]    ${password}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_password]
    SeleniumLibrary.Input text    ${dictRegistrationPage}[txt_password]    ${password}

Input Firstname
    [Arguments]    ${firstname}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_firstname]
    SeleniumLibrary.Input text    ${dictRegistrationPage}[txt_firstname]    ${firstname}

Input Lastname
    [Arguments]    ${lastname}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_lastname]
    SeleniumLibrary.Input text    ${dictRegistrationPage}[txt_lastname]    ${lastname}

Input guest telephone
    [Arguments]    ${phone_number}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_guest_telephone]
    SeleniumLibrary.Input text    ${dictRegistrationPage}[txt_guest_telephone]    ${phone_number}
    Wait Until Page Is Completely Loaded

Input T1C number
    [Arguments]    ${number}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_the1no]
    SeleniumLibrary.Input text    ${dictRegistrationPage}[txt_the1no]    ${number}

Click Submit Button
    CommonWebKeywords.Click Element    ${dictRegistrationPage}[btn_submit]

Title Registration Page Should Be Visible
    [Arguments]    ${title}
    ${title_locator}=    CommonKeywords.Format Text    ${dictRegistrationPage}[title_register]    $title_register=${title}
    CommonWebKeywords.Click Element    ${title_locator}

Successfully Registered Message Should Be Visible
    [Arguments]    ${success_msg}
    ${title_locator}=    CommonKeywords.Format Text    ${dictRegistrationPage}[txt_register_success]    $txt_register_success=${success_msg}
    CommonWebKeywords.Verify Web Elements Are Visible    ${title_locator}

Verify registeration message success with welcome message
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_register_auto_login]

Verify registeration fail and should not contain welcome message
    SeleniumLibrary.Wait Until Page Does Not Contain Element    ${dictRegistrationPage}[txt_register_auto_login]

Verify registeration message success navigate in 3 secounds
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_register_success_3secounds]

Verify registeration page with error message in firstname field
    Wait Until Element Contains    ${dictRegistrationPage}[txt_register_verify_error_message_firstname]    ${error_message.my_account_page.required_field}

Verify registeration page with error message in email field
    Wait Until Element Contains    ${dictRegistrationPage}[txt_register_verify_error_message_email]    ${error_message.my_account_page.required_field}

Verify registeration page with error message with existed email
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[msg_existed_email]

Verify registeration page with error message with invalid email 
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[msg_invalid_email]

Verify registeration page with error message with invalid password 
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[msg_invalid_password]

Verify registration page should display error message
    [Arguments]    ${error_msg}
    ${message}=    CommonKeywords.Format Text    ${dictRegistrationPage}[msg_invalid_input]    $error_msg=${error_msg}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Header Web - Click Facebook login Button
    CommonWebKeywords.Click Element    ${dictRegistrationPage}[btn_facebook_login]

Input Email With Facebook
    [Arguments]    ${email_facebook_input}
    CommonWebKeywords.Click Element    ${dictRegistrationPage}[btn_facebook_email]
    SeleniumLibrary.Input text    ${dictRegistrationPage}[btn_facebook_email]    ${email_facebook_input}

Input Password With Facebook
    [Arguments]    ${password_facebook_input}
    CommonWebKeywords.Click Element    ${dictRegistrationPage}[btn_facebook_password]
    SeleniumLibrary.Input text    ${dictRegistrationPage}[btn_facebook_password]    ${password_facebook_input}

Click Submit Facebook Login Button
    CommonWebKeywords.Click Element    ${dictRegistrationPage}[btn_facebook_login_popup]

Click Confirm Facebook New Account Button
    CommonWebKeywords.Click Element    ${dictRegistrationPage}[btn_facebook_confirm]

Verify registeration message invalid facebook email
    SeleniumLibrary.Wait Until Page Contains Element    ${dictRegistrationPage}[txt_regis_invalid_facebook_email]

Select Window locator page title
    [Arguments]    ${locator_info}
    Wait Until Keyword Succeeds    5 x    5 sec    SeleniumLibrary.Select Window    ${locator_info}

Select Window by URL and verify title
    [Documentation]  locator_info can be URL, Name, Title
    [Arguments]    ${url}  ${title}
    Wait Until Page Is Completely Loaded
    Select Window    ${url}
    ${current_title}=  Get Title
    Should Contain  ${current_title}  ${title}

Click continue shopping button after registeration suuccess
    CommonWebKeywords.Click Element    ${dictRegistrationPage}[txt_register_button_continue_shopping]

Test Setup - Click login and register on header
    header_web_keywords.Header Web - Click Login on Header
    header_web_keywords.Header Web - Click Register Button

Verify registeration Error box facebook email
    SeleniumLibrary.Wait Until Page Contains Element    ${dictRegistrationPage}[txt_facebook_error_box]
    SeleniumLibrary.Wait Until Page Contains Element    ${dictRegistrationPage}[btn_facebook_email]
    SeleniumLibrary.Wait Until Page Contains Element    ${dictRegistrationPage}[btn_facebook_password]

Verify PDPA Checkbox is unselected as a defaut
    SeleniumLibrary.Wait Until Element Is Visible    ${dictRegistrationPage}[chkbox_pdpa]
    ${value}=    CommonWebKeywords.Get Web Element CSS Property Value    ${dictRegistrationPage}[chkbox_pdpa]    ${common_style.background}
    Should Not Contain    ${value}    ${common_pdpd.checkbox_value}

Verify PDPA Checkbox is able to clickable
    SeleniumLibrary.Wait Until Element Is Visible    ${dictRegistrationPage}[chkbox_pdpa]
    CommonWebKeywords.Click Element Using Javascript    ${dictRegistrationPage}[chkbox_pdpa]
    ${value}=    CommonWebKeywords.Get Web Element CSS Property Value    ${dictRegistrationPage}[chkbox_pdpa]    ${common_style.background}
    Should Contain    ${value}    ${common_pdpd.checkbox_value}

Verify pada text message is dislay correctly
    SeleniumLibrary.Wait Until Page Contains Element    ${dictRegistrationPage}[txt_pdpa_privilage]
    SeleniumLibrary.Wait Until Page Contains Element    ${dictRegistrationPage}[txt_pdpa_company_name]
    SeleniumLibrary.Wait Until Page Contains Element    ${dictRegistrationPage}[txt_pdpa_company_partner_specified]
    SeleniumLibrary.Wait Until Page Contains Element    ${dictRegistrationPage}[txt_pdpa_privacy]

Redirect home page
    SeleniumLibrary.Reload Page
    SeleniumLibrary.Click Element    ${dictRegistrationPage}[logo_central]

Verify elements at Login form
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictRegistrationPage}[txt_login_email]    ${dictRegistrationPage}[txt_login_password]    ${dictRegistrationPage}[btn_login]

Input email and password to login
    [Arguments]    ${email}    ${password}
    Verify elements at Login form
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictRegistrationPage}[txt_login_email]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictRegistrationPage}[txt_login_password]    ${password}
    CommonWebKeywords.Click Element    ${dictRegistrationPage}[btn_login]
    Wait Until Page Is Completely Loaded