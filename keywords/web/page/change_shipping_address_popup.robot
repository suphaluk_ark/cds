*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Variables ***
&{dictChangeShippingAddressPopup}
...    btn_add_shipping_address=css=#btn-addAddress
...    change_address_popup=css=#address-modal-list
...    lbl_shipping_address=css=#checkout-address
...    lnk_edit_shipping_address=xpath=//div[@id="address-modal-list"]//h4[@id="inf-viewAddressNameOnCheckout-{$address_id}"]//preceding::button[text()="${shipping_address_popup.edit}"][1]
...    lbl_change_shipping_address_name=xpath=//div[@id="address-modal-list"]//h4[@id="inf-viewAddressNameOnCheckout-{$address_id}"]
...    lbl_change_shipping_customer_name=xpath=//div[@id="address-modal-list"]//div[@id="inf-viewAddressCustomerNameOnCheckout-{$address_id}"]
...    lbl_change_shipping_telephone=xpath=//div[@id="address-modal-list"]//div[@id="inf-viewAddressTelephoneOnCheckout-{$address_id}"]
...    lbl_change_shipping_address_no=xpath=//div[@id="address-modal-list"]//div[@id="inf-viewAddressLine1OnCheckout-{$address_id}"]
...    lbl_change_shipping_address=xpath=//div[@id="address-modal-list"]//div[@id="inf-viewAddressLine2OnCheckout-{$address_id}"]

...    txt_edit_address_name=css=input[name="address_name"]
...    txt_edit_firstname=css=input[name="firstname"]
...    txt_edit_lastname=css=input[name="lastname"]
...    txt_edit_telephone=css=input[name="telephone"]
...    txt_edit_building=css=input[name="building"]
...    txt_edit_address_no=css=input[name="address_line"]
...    txt_edit_postcode=css=input[name="postcode"]
...    lst_edit_region_id=css=select[name="region_id"]
...    lst_edit_district_id=css=select[name="district_id"]
...    lst_edit_subdistrict_id=css=select[name="subdistrict_id"]
...    btn_use_selected_address=xpath=//div[@id="checkout-page"]//div[text()="${shipping_address_popup.use_selected_address}"]
...    btn_save_address=xpath=//div[@id="checkout-page"]//div[text()="${shipping_address_popup.save_address}"]
...    request_tax_invoice_option=xpath=//span[text()="${change_address_popup.add_full_tax_invoice_info}"]//div[@class="react-switch-bg"]
...    rdo_personal_invoice=css=input[name="billing_type"][value="personal"]
...    rdo_company_invoice=css=input[name="billing_type"][value="company"]
...    txt_id_card=css=input[name="vat_id"][placeholder="${change_address_popup.input_id_card}"]
...    txt_company=css=input[name="company"]
...    txt_tax_id=css=input[name="vat_id"][placeholder="${change_address_popup.input_tax_id}"]
...    txt_branch_id=css=input[name="branch_id"]
...    chk_default_shipping=css=input[name="default_shipping"]
...    chk_default_billing=css=input[name="default_billing"]
...    box_checkout_address=css=#address-modal-list #checkout-address
...    lbl_warning_msg=xpath=//div[@id="address-modal-list"]//following::span[text()="${change_address_popup.whitespace_warning_message}"]



*** Keywords ***
Verify change shipping address popup should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[change_address_popup]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[btn_add_shipping_address]

Click edit shipping address by address id
    [Arguments]    ${address_id}
    ${elem}=    CommonKeywords.Format Text    ${dictChangeShippingAddressPopup}[lnk_edit_shipping_address]    $address_id=${address_id}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    CommonWebKeywords.Click Element    ${elem}

Input address name to update shipping address
    [Arguments]    ${update_info}
    Update shipping address    ${dictChangeShippingAddressPopup}[txt_edit_address_name]    ${update_info}

Input firstname to update shipping address
    [Arguments]    ${update_info}
    Update shipping address    ${dictChangeShippingAddressPopup}[txt_edit_firstname]    ${update_info}

Input lastname to update shipping address
    [Arguments]    ${update_info}
    Update shipping address    ${dictChangeShippingAddressPopup}[txt_edit_lastname]    ${update_info}

Input telephone to update shipping address
    [Arguments]    ${update_info}
    Update shipping address    ${dictChangeShippingAddressPopup}[txt_edit_telephone]    ${update_info}

Input building to update shipping address
    [Arguments]    ${update_info}
    Update shipping address    ${dictChangeShippingAddressPopup}[txt_edit_building]    ${update_info}

Input address no to update shipping address
    [Arguments]    ${update_info}
    Update shipping address    ${dictChangeShippingAddressPopup}[txt_edit_address_no]    ${update_info}

Input postcode to update shipping address
    [Arguments]    ${update_info}
    Update shipping address    ${dictChangeShippingAddressPopup}[txt_edit_postcode]    ${update_info}
    Wait Until Page Is Completely Loaded

Click save address button
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[btn_save_address]
    CommonWebKeywords.Click Element    ${dictChangeShippingAddressPopup}[btn_save_address]

Click use selected address button
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[btn_use_selected_address]
    CommonWebKeywords.Click Element    ${dictChangeShippingAddressPopup}[btn_use_selected_address]

Update shipping address
    [Arguments]    ${element}    ${update_info}
    CommonWebKeywords.Verify Web Elements Are Visible    ${element}
    Clear Element Text    ${element}
    SeleniumLibrary.Input Text    ${element}    ${update_info}

Get shipping address info
    [Arguments]    ${locator}
    ${address_id}=    Set Variable    ${member_profile}[address_id]
    ${elem}=    CommonKeywords.Format Text    ${locator}    $address_id=${address_id}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${shipping_info}=    SeleniumLibrary.Get Text    ${elem}
    [Return]    ${shipping_info}

Get current shipping address name
    ${address_name}=    Get shipping address info    ${dictChangeShippingAddressPopup}[lbl_change_shipping_address_name]
    [Return]    ${address_name}

Get current shipping customer name 
    ${customer_name}=    Get shipping address info    ${dictChangeShippingAddressPopup}[lbl_change_shipping_customer_name]
    [Return]    ${customer_name}

Get current shipping telephone 
    ${phone}=    Get shipping address info    ${dictChangeShippingAddressPopup}[lbl_change_shipping_telephone]
    [Return]    ${phone}

Get current shipping building and address no 
    ${address}=    Get shipping address info    ${dictChangeShippingAddressPopup}[lbl_change_shipping_address_no]
    [Return]    ${address}

Get current shipping address 
    ${address}=    Get shipping address info    ${dictChangeShippingAddressPopup}[lbl_change_shipping_address]
    [Return]    ${address}

Verify tax invoice personal type is selected
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[rdo_personal_invoice]
    ${status}=    SeleniumLibrary.Get Element Attribute    ${dictChangeShippingAddressPopup}[rdo_personal_invoice]    checked
    Should Be True    "${status}"=="true"

Verify tax invoice company type is selected
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[rdo_company_invoice]
    ${status}=    SeleniumLibrary.Get Element Attribute    ${dictChangeShippingAddressPopup}[rdo_company_invoice]    checked
    Should Be True    "${status}"=="true"

Get ID card for personal tax invoice
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_id_card]
    ${id_card}=    Get Value    ${dictChangeShippingAddressPopup}[txt_id_card]
    [Return]    ${id_card}

Get tax ID for company tax invoice
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_tax_id]
    ${tax_id}=    Get Value    ${dictChangeShippingAddressPopup}[txt_tax_id]
    [Return]    ${tax_id}

Get tax invoice company name
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_company]
    ${company_name}=    Get Value    ${dictChangeShippingAddressPopup}[txt_company]
    [Return]    ${company_name}

Get tax invoice branch id
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_branch_id]
    ${branch_id}=    Get Value    ${dictChangeShippingAddressPopup}[txt_branch_id]
    [Return]    ${branch_id}

Click request tax invoice option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[request_tax_invoice_option]
    CommonWebKeywords.Click Element    ${dictChangeShippingAddressPopup}[request_tax_invoice_option]
    Wait Until Page Is Completely Loaded

Select tax invoice for personal
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[rdo_personal_invoice]
    CommonWebKeywords.Click Element    ${dictChangeShippingAddressPopup}[rdo_personal_invoice]
    Wait Until Page Is Completely Loaded

Select tax invoice for company
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[rdo_company_invoice]
    CommonWebKeywords.Click Element    ${dictChangeShippingAddressPopup}[rdo_company_invoice]
    Wait Until Page Is Completely Loaded

Input ID card
    [Arguments]    ${id_card}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_id_card]
    SeleniumLibrary.Input Text    ${dictChangeShippingAddressPopup}[txt_id_card]    ${id_card}

Input company name
    [Arguments]    ${company_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_company]
    SeleniumLibrary.Input Text    ${dictChangeShippingAddressPopup}[txt_company]    ${company_name}

Input tax ID
    [Arguments]    ${tax_id}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_tax_id]
    SeleniumLibrary.Input Text    ${dictChangeShippingAddressPopup}[txt_tax_id]    ${tax_id}
    Wait Until Page Is Completely Loaded

Input branch ID
    [Arguments]    ${branch_id}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[txt_branch_id]
    SeleniumLibrary.Input Text    ${dictChangeShippingAddressPopup}[txt_branch_id]    ${branch_id}
    Wait Until Page Is Completely Loaded

Click add address button
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[btn_add_shipping_address]
    CommonWebKeywords.Click Element    ${dictChangeShippingAddressPopup}[btn_add_shipping_address]
    Wait Until Page Is Completely Loaded

Select default shipping address option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[chk_default_shipping]
    CommonWebKeywords.Click Element    ${dictChangeShippingAddressPopup}[chk_default_shipping]
    Wait Until Page Is Completely Loaded

Select default billing address option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[chk_default_billing]
    CommonWebKeywords.Click Element    ${dictChangeShippingAddressPopup}[chk_default_billing]
    Wait Until Page Is Completely Loaded

Select address to be a default
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[box_checkout_address]
    CommonWebKeywords.Click Element    ${dictChangeShippingAddressPopup}[box_checkout_address]
    Wait Until Page Is Completely Loaded

Get address list by address type
    [Arguments]    ${username}    ${password}    ${address_type}
    ${all_address}=    Get customer address id list from MDC    ${username}    ${password}

    @{address_list}=    Create List
    FOR   ${item}    IN    @{all_address}
        ${type}=    Get From Dictionary    ${item}    address_type
        Run Keyword If    "${type}"=="${address_type}"    Append To List    ${address_list}    ${item}
    END
    [Return]    ${address_list}

Verify address is not allowed to selected if there is white space
    [Arguments]    ${username}    ${password}    ${address_type}

    ${shipping_address}=    Get address list by address type    ${username}    ${password}    ${address_type}
    FOR   ${item}    IN    @{shipping_address}
        ${address_id}=    Get From Dictionary    ${item}    address_id
        ${elem}=    CommonKeywords.Format Text    ${dictChangeShippingAddressPopup}[lbl_change_shipping_customer_name]    $address_id=${address_id}
        CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
        CommonWebKeywords.Click Element    ${elem}
        CommonWebKeywords.Verify Web Elements Are Visible    ${dictChangeShippingAddressPopup}[lbl_warning_msg]
    END


    

    