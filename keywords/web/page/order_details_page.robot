*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/order_details_page.robot

*** Keywords ***
Click continue button on payment page
    CommonWebKeywords.Scroll To Element    ${dictOrderdetails}[btn_continue_shopping]
    CommonWebKeywords.Click Element Using Javascript    ${dictOrderdetails}[btn_continue_shopping]
    Wait Until Page Is Completely Loaded

Click my customer name on home page
    CommonWebKeywords.Click Element     ${dictOrderdetails}[txt_customer_name_home_page]
    Wait Until Page Is Completely Loaded

Click my order on home page
    CommonWebKeywords.Click Element     ${dictOrderdetails}[btn_my_order_home]
    Wait Until Page Is Completely Loaded

Select order at my order page
    [Arguments]    ${order_no}
    ${order_locator}=    CommonKeywords.Format Text    ${dictOrderdetails}[txt_my_order_order]    $order_number=${order_no}
    CommonWebKeywords.Click Element    ${order_locator}
    Wait Until Page Is Completely Loaded

Verify order status bar is intransit
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_bar_intransit]

Verify order status progress bar is intransit
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_intransit]

Verify order status bar is ready to ship
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_bar_ready_to_ship]

Verify order status progress bar is ready to ship
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_ready_to_ship]

Verify order status bar is processing
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_bar_processing]

Verify order status progress bar is processing
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_processing]

Verify order details page delivery option is pickup at store
    CommonWebKeywords.Verify Web Elements Are Visible   ${dictOrderdetails}[text_delivery_option_pickup_at_store]

Verify order status bar is deliver
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_bar_deliver]

Verify order status progress bar is deliver
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_deliver]

Verify order status bar is awaiting payment
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_bar_awaiting_payment]

Verify order progress bar is payment
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_payment]

Get order id from checkout page
    SeleniumLibrary.Wait Until Element Is Visible    ${dictOrderdetails}[text_get_order_id_form_checkout_page]
    Wait Until Keyword Succeeds    30 x    10 sec    SeleniumLibrary.Element Should Contain    ${dictOrderdetails}[text_get_order_id_form_checkout_page]    STGCO
    ${increment_id}=    SeleniumLibrary.Get Text    ${dictOrderdetails}[text_get_order_id_form_checkout_page]
    Set Test Variable    ${increment_id}
    [Return]    ${increment_id}

Verify order status progress bar is intransit 1 box
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_intransit_first_box]

Verify order status progress bar is processing 2 box
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_processing_secound_box]

Verify order status progress bar is delivered 1 box
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_delivered_first_box]

Verify order status progress bar is shipped 2 box
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_intransit_secound_box]

Verify order status progress bar is cancel
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_cancel]

Verify tracking message in order details page display
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_tracking_number_verify_text]

Verify tracking number in order details page
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    Wait Until Element Contains    ${dictOrderdetails}[text_order_tracking_number]    ${order_details.tracking_number_prefixes}

Verify tracking number should not display in order details page
    Seleniumlibrary.Wait Until Page Does Not Contain Element    ${dictOrderdetails}[text_order_tracking_number]    timeout=${GLOBALTIMEOUT}
    Wait Until Page Is Completely Loaded

Verify tracking number 1 should display in order details page
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[test_tracking_box_1]

Verify tracking number 2 should display in order details page
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    Wait Until Page Loader Is Not Visible
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[test_tracking_box_2]

Verify tracking number 2 should not display in order details page
    Seleniumlibrary.Wait Until Page Does Not Contain Element    ${dictOrderdetails}[test_tracking_box_2]    timeout=${GLOBALTIMEOUT}
    Wait Until Page Is Completely Loaded

Click tracking number
    CommonWebKeywords.Click Element     ${dictOrderdetails}[text_order_tracking_number]
    Wait Until Page Is Completely Loaded

Verify tracking page when click tracking number
    ${tracking_URL}=    Get Location
    Select Window    locator=NEW
    ${tracking_URL}=    Get Location
    Should Be True  '${tracking_URL}' == '${order_details.tracking_url}'

Verify order status progress bar is ready to collect
    Wait Until Keyword Succeeds    15 x    10 sec    Run Keywords    Reload Page
    ...    AND    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[text_order_status_progressbar_ready_to_collect]

Verify re-payment button displayed on order details page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictOrderdetails}[btn_repayment]

Click on re-payment button
    CommonWebKeywords.Click Element     ${dictOrderdetails}[btn_repayment]

Verify order status at specific order detail page
    [Arguments]    ${order_status}
    Verify Web Elements Are Visible    ${dictOrderdetails}[lbl_order_status]    ${dictOrderdetails}[lbl_order_status_active_bar]
    ${ui_order_stt}=    SeleniumLibrary.Get Text    ${dictOrderdetails}[lbl_order_status]
    Should Contain    ${ui_order_stt.strip().lower()}    ${order_status.strip().lower()}