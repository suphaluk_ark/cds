*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/checkout_page.robot

*** Variables ***
${path_root}    $.items[0].

*** Keywords ***
Select region
    [Arguments]    ${region}
    CommonWebKeywords.Select dropdownlist by label    ${dictCheckoutPage}[region]    ${region}

Select district
    [Arguments]    ${district}
    CommonWebKeywords.Select dropdownlist by label    ${dictCheckoutPage}[district]    ${district}

Select Subdistrict
    [Arguments]    ${subdistrict}
    CommonWebKeywords.Select dropdownlist by label    ${dictCheckoutPage}[subdistrict]    ${subdistrict}

Standard delivery should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[standard_day_delivery]

Click on standard delivery button
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[standard_day_delivery]
    common_keywords.Wait Until Page Is Completely Loaded

Click on Same-day delivery button
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[same_day_delivery]
    Wait Until Page Is Completely Loaded

Click on Next-day delivery button
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[next_day_delivery]
    Wait Until Page Is Completely Loaded

Click and verify same-day delivery during configuration time
    ${status}=    Verify Same Day shipping option is not displayed in disable time setting
    Run Keyword If    ${status}==${FALSE}   Click on Same-day delivery button

Same-day delivery should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[same_day_delivery]

Next-day delivery should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[next_day_delivery]

Standdard delivery should not be visible
    SeleniumLibrary.Wait Until Element Is Not Visible    ${dictCheckoutPage}[standard_day_delivery]

Same-day delivery should not be visible
    SeleniumLibrary.Wait Until Element Is Not Visible    ${dictCheckoutPage}[same_day_delivery]

Next-day delivery should not be visible
    SeleniumLibrary.Wait Until Element Is Not Visible    ${dictCheckoutPage}[next_day_delivery]

Click re-payment now button
    CommonWebKeywords.Click Element        ${dictCheckoutPage}[btn_repayment_now]

Pickup location should be displayed in thank you page
    [Arguments]    ${store}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[family_pickup]    $store=${store}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify e-coupon discount in thank you page(%)
    [Arguments]    ${coupon}    ${total_price}    ${discount}
    Calculate discount(%) from original total price    ${total_price}    ${discount}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_ecoupon_thanku]    $coupon=${coupon}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}
    ${discount_amount}=    Evaluate    round(${total_price} * ${discount}/100.0, 2)
    ${discount_amount}=    Run Keyword If    ${${discount_amount}}.is_integer()    Convert price to total amount int format    ${discount_amount}
    ...    ELSE    Convert price to total amount format    ${discount_amount}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${elem}    -฿${discount_amount}

Verify e-coupon discount in thank you page(Baht)
    [Arguments]    ${coupon}    ${total_price}    ${discount}
    Calculate discount(Bath) from original total price    ${total_price}    ${discount}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_ecoupon_thanku]    $coupon=${coupon}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${elem}    -฿${discount_amount}

Verify other discount in checkout page(%)
    [Arguments]    ${total_price}    ${discount}
    Calculate discount(%) from original total price    ${total_price}    ${discount}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictCheckoutPage}[other_discount_thanku]    -฿${discount_amount_with_format}

Verify other discount in checkout page(Baht)
    [Arguments]    ${total_price}    ${discount}
    Calculate discount(Bath) from original total price    ${total_price}    ${discount}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictCheckoutPage}[other_discount_thanku]    -฿${discount_amount}

shipping charge for pick up at family mart should be displayed
    [Arguments]    ${store}
    ${elem}=    Run Keyword If    '${ENV.lower()}'=='staging'    CommonKeywords.Format Text    ${dictCheckoutPage}[shipping_fam_80]    $store=${store}
    ...    ELSE IF    '${ENV.lower()}'=='prod'    CommonKeywords.Format Text    ${dictCheckoutPage}[_prod]    $store=${store}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Payment type from thank you page should be
    [Arguments]    ${payment_type}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictCheckoutPage}[lbl_payment_type]    ${payment_type}

Order status from thank you page should be
    [Arguments]    ${order_status}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictCheckoutPage}[lbl_order_status]    ${order_status}

Click continue payment for full redeemtion
    CommonWebKeywords.Verify Web Elements Are Visible        ${dictCheckoutPage}[btn_checkout_full_redeem]
    CommonWebKeywords.Scroll To Element    ${dictCheckoutPage}[btn_checkout_full_redeem]
    CommonWebKeywords.Click Element Using Javascript    ${dictCheckoutPage}[btn_checkout_full_redeem]
    Wait Until Page Is Completely Loaded

Select on full redeem point
    Select on redeem point dropdown list
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[ddl_full_redeem]

Select on redeem point dropdown list
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[ddl_redeem_point]

Click connect to T1C link
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_connect_t1c]

Login to T1C account
    [Arguments]    ${email}    ${password}
    Input email T1C account    ${email}
    Input password T1C account    ${password}
    Click login to T1C account button

Input email T1C account
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_email]    ${email}

Input password T1C account
    [Arguments]    ${password}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_password]    ${password}

Click login to T1C account button
    CommonWebKeywords.Click Element        ${dictCheckoutPage}[btn_login_t1c]

Input T1C point
    [Arguments]    ${point}
    CommonWebKeywords.Verify Web Elements Are Visible     ${dictCheckoutPage}[txt_point]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_point]    ${point}

Verify order total
    [Arguments]    ${price}    ${discount}=${0}    ${redeem}=${0}    ${shipping}=${0}
    Summarize price with discount, T1redeem and shipping charge    ${price}    ${discount}    ${redeem}    ${shipping}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictCheckoutPage}[order_total]    ฿${order_total_format}

Verify order total in thank you page
    [Arguments]    ${price}    ${discount}=${0}    ${redeem}=${0}    ${shipping}=${0}
    ${price}=    Convert To Number    ${price}
    Summarize price with discount, T1redeem and shipping charge    ${price}    ${discount}    ${redeem}    ${shipping}
    CommonWebKeywords.Scroll To Element    ${dictCheckoutPage}[order_total_thank_u]
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictCheckoutPage}[order_total_thank_u]    ฿${order_total_format}

Verify order total in thank you page with new design
    [Arguments]    ${price}    ${discount}=${0}    ${redeem}=${0}    ${shipping}=${0}
    ${price}=    Convert To Number    ${price}
    Summarize price with discount, T1redeem and shipping charge    ${price}    ${discount}    ${redeem}    ${shipping}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[lbl_order_total_price_summary]    ฿${order_total_format}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[lbl_order_total_price]    ฿${order_total_format}

Verify order total in thank you page with split order
    [Arguments]    ${sku}    ${qty}    ${discount}=${0}    ${redeem}=${0}    ${shipping}=${0}
    Summarize price with discount, T1redeem and shipping charge with split order total    ${sku}    ${qty}    ${discount}    ${redeem}    ${shipping}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[lbl_sub_total_summary]    ฿${actual_sub_total}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[lbl_order_total_price_summary]    ฿${actual_order_total}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[lbl_order_total_price]    ฿${actual_order_total}

Click apply T1C point
    CommonWebKeywords.Click Element        ${dictCheckoutPage}[btn_apply_point]
    Wait Until Page Loader Is Not Visible

Click apply point after login T1C
    ${status}=    Run Keyword And Return Status    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[btn_apply_point_login]
    Return From Keyword If    '${status}'=='${FALSE}'
    CommonWebKeywords.Click Element        ${dictCheckoutPage}[btn_apply_point_login]

Click on Redeem button
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_apply_point_login]

Total redeem point and discount for the 1 redemption should be
    [Arguments]    ${point}    ${discount}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[txt_total_redeem_point]    $point=${point}    $discount=${discount}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Input customer information and shipping address
    Input customer details
    Input shipping address

Input customer details
    [Arguments]    ${firstName}=${guest_information}[firstname]
    ...    ${lastName}=${guest_information}[lastname]
    ...    ${email}=${guest_information}[email]
    ...    ${phone}=${guest_information}[tel]
    Input customer firstname    ${firstName}
    Input customer Lastname    ${lastName}
    Input customer email    ${email}
    Input customer phone number    ${phone}

Input customer firstname
    [Arguments]    ${firstname}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_customer_firstname]    ${firstname}

Input customer Lastname
    [Arguments]    ${lastname}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_customer_lastname]    ${lastname}

Input customer email
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_customer_email]    ${email}

Input customer phone number
    [Arguments]    ${phone}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_customer_phone]    ${phone}

Input credit card
    [Arguments]    ${card_number}=${credit_card}[number]
    ...    ${card_holder}=${credit_card}[holder]
    ...    ${card_cvv}=${credit_card}[cvv]
    ...    ${expiry_month}=${credit_card}[month]
    ...    ${expiry_year}=${credit_card}[year]
    ${count}    SeleniumLibrary.Get Element Count    ${dictCheckoutPage}[txt_save_card]
    Run Keyword And Return If    ${count}>0    Input credit card cvv   ${card_cvv}
    SeleniumLibrary.Wait Until Page Contains Element    ${dictCheckoutPage}[iframe_credit_card]     ${GLOBALTIMEOUT}
    SeleniumLibrary.Select Frame    ${dictCheckoutPage}[iframe_credit_card]
    Input credit card holder   ${card_holder}
    Input credit card number   ${card_number}
    Input credit card cvv   ${card_cvv}
    Input credit card expiry date    ${expiry_month}${expiry_year}
    SeleniumLibrary.Unselect Frame
    Uncheck Save credit card

Uncheck Save credit card
    ${count}    SeleniumLibrary.Get Element Count    ${dictCheckoutPage}[ckb_save_card]
    Run Keyword If    ${count}>0    SeleniumLibrary.Click Element    ${dictCheckoutPage}[ckb_save_card]

Input credit card number
    [Arguments]    ${card_number}
    SeleniumLibrary.Input Text    ${dictCheckoutPage}[txt_card_number]    ${card_number}

Input credit card holder
    [Arguments]    ${card_holder}
    CommonWebKeywords.Scroll To Element    ${dictCheckoutPage}[txt_card_holder]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_card_holder]    ${card_holder}

Input credit card cvv
    [Arguments]    ${card_cvv}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_card_cvv]    ${card_cvv}

Input credit card expiry date
    [Arguments]    ${card_expire_date}
    SeleniumLibrary.Input Text    ${dictCheckoutPage}[txt_card_expire_date]    ${card_expire_date}

Click Continue Shopping Button
    CommonWebKeywords.Scroll To Element        ${dictCheckoutPage}[btn_continue_shopping]
    CommonWebKeywords.Click Element Using Javascript    ${dictCheckoutPage}[btn_continue_shopping]

Input shipping details
    [Arguments]        ${address}=${shipping_address_${language}}[address]
    ...    ${postcode}=${shipping_address_${language}}[zip_code]
    ...    ${region}=${shipping_address_${language}}[region]
    ...    ${district}=${shipping_address_${language}}[district]
    ...    ${subdistrict}=${shipping_address_${language}}[sub_district]
    Input shipping address    ${address}
    Input shipping postcode    ${postcode}
    Wait Until Page Is Completely Loaded
    Select region    ${region}
    Select district    ${district}
    Select Subdistrict    ${subdistrict}

Input shipping address
    [Arguments]    ${building}=${shipping_address_en}[building]
    ...    ${street}=${shipping_address_en}[address]
    ...    ${postcode}=${shipping_address_en}[zip_code]
    Input shipping address line    ${building} ${street}
    Input shipping postcode    ${postcode}
    Wait Until Page Is Completely Loaded

Input shipping building
    [Arguments]    ${building}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_shipping_building]    ${building}

Input shipping street
    [Arguments]    ${street}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_shipping_address]    ${street}

Input shipping address line
    [Arguments]    ${address}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_shipping_address]    ${address}

Input shipping postcode
    [Arguments]    ${postcode}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_shipping_postcode]    ${postcode}

Select standard delivery option
    CommonWebKeywords.Click Element     ${dictCheckoutPage}[rad_standard_delivery]

Select option bank transfer and service counter
    CommonWebKeywords.Click Element     ${dictCheckoutPage}[rad_bank_transfer]
    Wait Until Page Loader Is Not Visible

Verify Repayment page is displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_repayment_page]    ${dictCheckoutPage}[payment_method_container]

Select specific bank name
    [Arguments]    ${bank}
    CommonWebKeywords.Select dropdownlist by label    ${dictCheckoutPage}[ddl_bank_name]    ${bank}

Select specific payment channel
    [Arguments]    ${channel}
    CommonWebKeywords.Select dropdownlist by label    ${dictCheckoutPage}[drp_payment_channel]    ${channel}

Input valid card information
    [Arguments]    ${card_name}    ${card_number}    ${card_expiry_date}    ${card_cvv}
    SeleniumLibrary.Wait Until Page Contains Element    ${dictCheckoutPage}[iframe_credit_card]
    SeleniumLibrary.Select Frame    ${dictCheckoutPage}[iframe_credit_card]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_card_name]    ${card_name}
    SeleniumLibrary.Input Text    ${dictCheckoutPage}[txt_card_number]    ${card_number}
    SeleniumLibrary.Input Text    ${dictCheckoutPage}[txt_card_expiry_date]    ${card_expiry_date}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_card_cvv]    ${card_cvv}
    SeleniumLibrary.Unselect Frame

Proceed to check out and verify with Pay At Store for 2 Hour pickup
    Select Payment Method Pay at Store
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For Pay At Store Payment Method
    Wait Until Page Loader Is Not Visible
    Verify each section on Thank you page displays correctly

Proceed to check out and verify with Bank Transfer for 2 Hour pickup
    Select bank transfer option and input bank information
    Click Pay now Button
    Verify each section on payment slip page displays correctly
    Verify each section on Thank you page displays correctly

Select bank transfer option and input bank information
    Select option bank transfer and service counter
    Select specific bank name    ${payment_page.counter_bank}
    Select specific payment channel    ${payment_page.payment_channel}
    Input phone number for 1 2 3 bank transfer    ${shipping_address_en}[phone]

Input invalid card information
    [Arguments]    ${card_name}    ${card_number}    ${card_expiry_date}    ${card_cvv}
    SeleniumLibrary.Wait Until Page Contains Element    ${dictCheckoutPage}[iframe_credit_card]
    SeleniumLibrary.Select Frame    ${dictCheckoutPage}[iframe_credit_card]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_card_name]    ${card_name}
    SeleniumLibrary.Input Text    ${dictCheckoutPage}[txt_card_number]    ${card_number}
    SeleniumLibrary.Input Text    ${dictCheckoutPage}[txt_card_expiry_date]    ${card_expiry_date}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_card_cvv]    ${card_cvv}
    SeleniumLibrary.Unselect Frame

Select option credit card and full payment
    CommonWebKeywords.Click Element     ${dictCheckoutPage}[rad_credit_card]
    Wait Until Page Loader Is Not Visible

Select credit card option and input invalid card information
    [Arguments]    ${card_name}    ${card_number}    ${card_expiry_date}    ${card_cvv}
    Wait Until Page Is Completely Loaded
    Select option credit card and full payment
    Input invalid card information    ${card_name}    ${card_number}    ${card_expiry_date}    ${card_cvv}

Verify each section on Failed payment page displays correctly
    Wait Until Page Is Completely Loaded
    Verify sorry payment failed title
    Verify order number is displayed correctly
    Verify created date is displayed correctly
    Verify payment status is displayed correctly
    Verify payment type is displayed correctly

Verify sorry payment failed title
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictCheckoutPage}[txt_title_paymentFailed]    ${invalid_credit_card.payment_failed_title}

Verify order number is displayed correctly
    ${orderNumber}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[txt_orderNumber_paymentFailed]
    Should Not Be Empty  ${orderNumber}

Verify created date is displayed correctly
    ${createdDate}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[txt_createdDate_paymentFailed]
    Should Not Be Empty  ${createdDate}

Verify payment status is displayed correctly
    ${paymentStatus}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[txt_paymentStatus_paymentFailed]
    Should Be Equal As Strings    ${invalid_credit_card.payment_status}    ${paymentStatus}

Verify payment type is displayed correctly
    ${paymentType}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[txt_paymentType_paymentFailed]
    Should Be Equal As Strings    ${invalid_credit_card.payment_type}    ${paymentType}

Click continue button
    CommonWebKeywords.Click Element     ${dictCheckoutPage}[btn_continue]
    Wait Until Page Loader Is Not Visible

Select bank or service counter for 1 2 3 bank transfer
    [Arguments]    ${bank}
    CommonWebKeywords.Select dropdownlist by label    ${dictCheckoutPage}[ddl_bank_group]    ${bank}

Select payment channel
    [Arguments]    ${channel}
    CommonWebKeywords.Select dropdownlist by label    ${dictCheckoutPage}[ddl_payment_channel]    ${channel}

Input phone number for 1 2 3 bank transfer
    [Arguments]    ${number}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_phone]    ${number}

Select BBL VISA discount 100 bath
    Wait Until Keyword Succeeds    3 x    2s    Select Radio Button    ${dictCheckoutPage}[bank_service_group]    ${dictCheckoutPage}[bbl_visa_100]

Select Credit Card BBL VISA
    Scroll To Element    ${dictCheckoutPage}[txt_BBL_Platinum]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_BBL_Platinum]
    CommonWebKeywords.Click Element     ${dictCheckoutPage}[txt_BBL_Platinum]
    Wait Until Page Loader Is Not Visible

Select shipping to address option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[shipping_to_address]
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[shipping_to_address]
    Wait Until Page Is Completely Loaded

Select pick at store option
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[btn_click_n_collect]
    Wait Until Page Is Completely Loaded

Select pick at store option with omni design
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[btn_click_n_collect]

Select standard pickup option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[rdo_standard_pickup]
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[rdo_standard_pickup]
    Wait Until Page Is Completely Loaded

Select 2 hour pickup option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[rdo_2_hour_pickup]
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[rdo_2_hour_pickup]
    Wait Until Page Is Completely Loaded

Select store
    [Arguments]    ${store_name}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_select_store]    $store=${store_name}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Store should be displayed on map
    [Arguments]    ${store_name}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[modal_store]    $store=${store_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click on select this store button
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[btn_select_this_store]
    Wait Until Page Is Completely Loaded

Select pick at skybox and family mart option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[pick_at_skybox]
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[pick_at_skybox]

Select pick at skybox and family mart branches
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[pickup_at_skybox_branches]
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[pickup_at_skybox_branches]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[btn_pickup_at_skybox_branches]
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[btn_pickup_at_skybox_branches]

COD payment method should not be visible
    Wait Until Element Is Not Visible    ${dictCheckoutPage}[cod_value]

Verify credit card payment method should not be visible
    Wait Until Element Is Not Visible    ${dictCheckoutPage}[label_credit_card]

Verify COD payment method should be visible
    SeleniumLibrary.Wait Until Element Is Visible     ${dictCheckoutPage}[rdo_cod]

Verify Line Up shoud be visible
    SeleniumLibrary.Wait Until Element Is Visible     ${dictCheckoutPage}[label_line_up]

Verify Bank Transfer shoud be visible
    SeleniumLibrary.Wait Until Element Is Visible     ${dictCheckoutPage}[label_bank_transfer]

Select default address
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[default_address]

Select Shipping Address Button
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[btn_shipping_address]
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[btn_shipping_address]

Select Billing Address Button
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[btn_billing_address]

Check Out Page Should Be Visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[checkout_page]

Continue Payment Button Should Be Visible
    CommonWebKeywords.Scroll To Element        ${dictCheckoutPage}[btn_continue_payment]

Click Continue Payment Button
    CommonWebKeywords.Verify Web Elements Are Visible        ${dictCheckoutPage}[btn_continue_payment]
    CommonWebKeywords.Scroll To Element    ${dictCheckoutPage}[btn_continue_payment]
    SeleniumLibrary.Wait Until Element Is Enabled    ${dictCheckoutPage}[btn_continue_payment]    ${GLOBALTIMEOUT}
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    CommonWebKeywords.Click Element Using Javascript    ${dictCheckoutPage}[btn_continue_payment]
    Wait Until Page Is Completely Loaded

Click On Selected Address Modal
    CommonWebKeywords.Click Element        ${dictCheckoutPage}[modal_address]

Payment Page Should Be Displayed
    SeleniumLibrary.Wait Until Page Contains Element  ${dictCheckoutPage}[payment_page]
    SeleniumLibrary.Page Should Contain Element    ${dictCheckoutPage}[payment_page]

Select Payment Method With Credit Card
    CommonWebKeywords.Scroll To Element    ${dictCheckoutPage}[group_payment]
    Wait Until Keyword Succeeds    3 x    2s    Select Radio Button    ${dictCheckoutPage}[group_payment]    ${dictCheckoutPage}[value_credit]

Select Payment Method COD
    Wait Until Keyword Succeeds    3 x    2s    Select Radio Button    ${dictCheckoutPage}[group_payment]    ${dictCheckoutPage}[value_cod]

Select Payment Method Pay at Store
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_pay_at_store]

Verify Payment Method COD should not be displayed
    Verify Web Elements Are Not Visible  ${dictCheckoutPage}[btn_cod]

Select Payment Option CASH For COD
    Wait Until Keyword Succeeds    3 x    2s    Select Radio Button    ${dictCheckoutPage}[cod_group]    ${dictCheckoutPage}[cod_cash]

Select Payment Option CREDIT For COD
    Wait Until Keyword Succeeds    3 x    2s    Select Radio Button    ${dictCheckoutPage}[cod_group]    ${dictCheckoutPage}[cod_credit]

Select payment method 2c2p Installment
    Wait Until Keyword Succeeds    3 x    2s    Select Radio Button    ${dictCheckoutPage}[group_payment]    ${dictCheckoutPage}[value_ipp]

Select Payment Method 1 2 3 bank transfer
    CommonWebKeywords.Scroll To Element    ${dictCheckoutPage}[group_payment]
    Wait Until Keyword Succeeds    3 x    2s    Select Radio Button    ${dictCheckoutPage}[group_payment]    ${dictCheckoutPage}[value_transfer]

Click Confirm Order For COD Payment Method
    CommonWebKeywords.Click Element Using Javascript    ${dictCheckoutPage}[btn_confirm_cod]
    Wait Until Page Loader Is Not Visible

Click Confirm Order For Pay At Store Payment Method
    CommonWebKeywords.Click Element Using Javascript    ${dictCheckoutPage}[btn_confirm_pay_at_store]
    Wait Until Page Loader Is Not Visible

Thank You Page Should Be Visible
    # ${url} =    Get Location
    # ${elem}=    Replace String    ${url}    //    //central:72KCm+Gk@
    # Go To    ${elem}
    Wait Until Page Loader Is Not Visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_payment_success]
    Run Keyword If    '${ENV.lower()}'=='staging'    Get order ID from thank you page

'Sorry your order is not complete' Msg should be displayed
    # ${url} =    Get Location
    # ${elem}=    Replace String    ${url}    //    //central:72KCm+Gk@
    # Go To    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_payment_fail]
    Run Keyword If    '${ENV.lower()}'=='staging'    Get order ID from thank you page
    ...    ELSE IF    '${ENV.lower()}'=='prod'    Get order ID from thank you page (CO)

Get order ID from thank you page
    Wait Until Page Is Completely Loaded
    Wait Until Element Contains    ${dictCheckoutPage}[lbl_order_id]    ${order_prefix}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_order_id]
    ${increment_id}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_order_id]
    Set Test Variable    ${increment_id}

Get order ID from thank you page (CO)
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_order_id_prod]
    ${increment_id}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_order_id_prod]
    Set Test Variable    ${increment_id}

Get customer firstname and lastname
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_customer_name]
    ${customer_name}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_customer_name]
    [Return]    ${customer_name}

Get customer email
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_customer_email]
    ${email}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_customer_email]
    [Return]    ${email}

Get customer telephone
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_customer_telephone]
    ${telephone}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_customer_telephone]
    [Return]    ${telephone}

Get default shipping address name
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_shipping_address_name]    $address_id=${member_profile}[address_id]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${address_name}=    SeleniumLibrary.Get Text    ${elem}
    [Return]    ${address_name}

Get default shipping customer firstname and lastname
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_shipping_customer_name]    $address_id=${member_profile}[address_id]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${customer_name}=    SeleniumLibrary.Get Text    ${elem}
    [Return]    ${customer_name}

Get default shipping telephone
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_shipping_telephone]    $address_id=${member_profile}[address_id]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${telephone}=    SeleniumLibrary.Get Text    ${elem}
    [Return]    ${telephone}

Get default shipping address number
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_shipping_address_no]    $address_id=${member_profile}[address_id]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${address_no}=    SeleniumLibrary.Get Text    ${elem}
    [Return]    ${address_no}

Get default shipping full address
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_shipping_full_address]    $address_id=${member_profile}[address_id]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${full_address}=    SeleniumLibrary.Get Text    ${elem}
    [Return]    ${full_address}

Get standard delivery shipping price
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_standard_day_delivery_price]
    ${price}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_standard_day_delivery_price]
    ${price}=    CommonKeywords.Convert price to number format  ${price}
    [Return]    ${price}

Get same day delivery shipping price
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_same_day_delivery_price]
    ${price}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_same_day_delivery_price]
    ${price}=    CommonKeywords.Convert price to number format  ${price}
    [Return]    ${price}

Get next day delivery shipping price
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_next_day_delivery_price]
    ${price}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_next_day_delivery_price]
    ${price}=    CommonKeywords.Convert price to number format  ${price}
    [Return]    ${price}

Get 3 hour delivery shipping price
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_3_hours_delivery_price]
    ${price}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_3_hours_delivery_price]
    ${price}=    CommonKeywords.Convert price to number format  ${price}
    [Return]    ${price}

Get subtotal from order summary
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_subtotal]
    ${subtotal}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_subtotal]
    ${subtotal}=    CommonKeywords.Convert price to number format    ${subtotal}
    [Return]    ${subtotal}

Click edit bag link
    CommonWebKeywords.Click Element     ${dictCheckoutPage}[lnk_edit_bag]

Click gift wrapping checkbox
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[chk_gift_wrapping]
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[chk_gift_wrapping]
    Wait Until Page Is Completely Loaded

Get product quantiy from order summary
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_product_qty]    $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${product_qty}=    SeleniumLibrary.Get Text    ${elem}
    ${product_qty}=    Convert To Integer    ${product_qty}
    [Return]    ${product_qty}

Get grand total from order summary
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_grand_total]
    ${grand_total}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[lbl_grand_total]
    ${grand_total}=    CommonKeywords.Convert price to number format    ${grand_total}
    [Return]    ${grand_total}

Select next day shipping option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[next_day_delivery]
    CommonWebKeywords.Scroll And Click Element        ${dictCheckoutPage}[next_day_delivery]

Verify pickup at store delivery option is displayed in delivery details page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[pick_at_store]

Verify pickup at skybox delivery option is displayed in delivery details page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[pick_at_skybox]

Pickup at store delivery option should not be visible
    SeleniumLibrary.Wait Until Element Is Not Visible    ${dictCheckoutPage}[pick_at_store]

Pickup at skybox and family mart delivery option should not be visible
    SeleniumLibrary.Wait Until Element Is Not Visible    ${dictCheckoutPage}[pick_at_skybox]

Standard delivery should not be visible
    SeleniumLibrary.Wait Until Element Is Not Visible    ${dictCheckoutPage}[standard_day_delivery]

Delivery option not available message should be displayed
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_delivery_message]    $message=${checkout_page}[delivery_not_available]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click change shipping address link
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[lnk_edit_shipping_address]

Delivery shipping option is not Selected
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[shipping_to_address]
    ${status}=    SeleniumLibrary.Get Element Attribute    ${dictCheckoutPage}[shipping_to_address]    checked
    Should Be True    ${status}==${None}

Click guest login button
    CommonWebKeywords.Click Element  ${dictCheckoutPage}[btn_guest_login]

Delivery pick up at store option is not Selected
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[pick_at_store]
    ${status}=    SeleniumLibrary.Get Element Attribute    ${dictCheckoutPage}[pick_at_store]    checked
    Should Be True    ${status}==${None}

Delivery pick up at skybox family mart option is not Selected
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[pick_at_skybox]
    ${status}=    SeleniumLibrary.Get Element Attribute    ${dictCheckoutPage}[pick_at_skybox]    checked
    Should Be True    ${status}==${None}

Standard shipping address is default Selected
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[standard_day_delivery]
    ${status}=    SeleniumLibrary.Get Element Attribute    ${dictCheckoutPage}[standard_day_delivery]    checked
    Should Be True    "${status}"=="true"

    Wait Until Page Is Completely Loaded

Verify that product and quantity should be displayed correctly
    [Arguments]  ${product_sku}  ${quantity}=1
    ${number}=  Get product quantiy from order summary  ${product_sku}
    Should Be True  ${quantity}==${number}

Verify Delivery Details page should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictCheckoutPage}[lbl_delivery_page]

Verify total of products should be displayed correctly at order summary on Delivery Details page
    [Arguments]  ${product_total}
    SeleniumLibrary.Wait Until Element Is Visible    ${dictCheckoutPage}[lbl_product_quantity]
    ${count}=  Get Element Count  ${dictCheckoutPage}[lbl_product_quantity]
    ${elems}=  SeleniumLibrary.Get WebElements  ${dictCheckoutPage}[lbl_product_quantity]
    ${total}=  Set Variable  0
    FOR  ${index}    IN RANGE   0    ${count}
         ${product_qty}=  SeleniumLibrary.Get Text  ${elems}[${index}]
         ${product_qty}=  Convert To Integer    ${product_qty}
         ${total}=  Evaluate  ${total} + ${product_qty}
    END
    Should Be True  '${total}'=='${product_total}'

Click request tax invoice
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[request_tax_invoice_option]
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[request_tax_invoice_option]

Click Connect The 1 Card Account
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[btn_connect_t1c]
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_connect_t1c]
    Wait Until Page Is Completely Loaded

Input Redeem point
    [Arguments]    ${point}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_point]    ${point}

Click Apply Redeem
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[btn_apply_redeem]
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_apply_redeem]
    Wait Until Page Is Completely Loaded

Click Pay now Button
    CommonWebKeywords.Scroll To Element        ${dictCheckoutPage}[btn_pay_now]
    CommonWebKeywords.Click Element Using Javascript    ${dictCheckoutPage}[btn_pay_now]
    Wait Until Page Loader Is Not Visible
    Wait Until Page Is Completely Loaded

Verify Click Pay now Button
   CommonWebKeywords.Verify Web Elements Are Visible       ${dictCheckoutPage}[btn_pay_now]

Input member email
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_member_email]    ${email}

Input member password
    [Arguments]    ${password}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_member_password]    ${password}

Click member login
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[btn_member_login]
    CommonWebKeywords.Click Element        ${dictCheckoutPage}[btn_member_login]
    Wait Until Page Is Completely Loaded

Verify home page for checkout process
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[home_page_id]

Verify cart merged for checkout process
    [Arguments]    ${cart_info}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[mini_cart_no]    $cart_info=${cart_info}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

click mini cart icon
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[mini_cart_ico]
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[mini_cart_ico]

Verify member login botton
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[btn_login_minicart]

Verify log in box text displayed on delivery page
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_login_box_1]    ${dictCheckoutPage}[txt_login_box_2]    ${dictCheckoutPage}[txt_login_box_3]

verify product after CHECKOUT AS GUEST
    [Arguments]    ${product_sku_name}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[verify_product_sku_name]    $product_sku_name=${product_sku_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Get discount amount that already apply e-coupon
    [Arguments]    ${coupon}
    ${elem}=  CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_price_promo_code]    $coupon=${coupon}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${discount_amount}=    SeleniumLibrary.Get Text    ${elem}
    [Return]    ${discount_amount}

Verify T1 redeem section method should not visible
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictCheckoutPage}[lbl_connect_t1c]

Verify T1 redeem section method should visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[lbl_connect_t1c]

Verify that only credit card KTC is displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_ktc]
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictCheckoutPage}[txt_BBL_Platinum]

Verify that other credit cards are displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_ktc]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_BBL_Platinum]

Select shipping option while checking out with home delivery
    [Arguments]    ${shipping_option}
    Run Keyword If    '${shipping_option}'=='${filters.shipping_options.standard_delivery}'    CommonWebKeywords.Click Element    ${dictCheckoutPage}[rad_standard_delivery]
    ...    ELSE IF    '${shipping_option}'=='${filters.shipping_options.sameday_delivery}'    CommonWebKeywords.Click Element    ${dictCheckoutPage}[rad_sameday_delivery]
    ...    ELSE IF    '${shipping_option}'=='${filters.shipping_options.nextday_delivery}'    CommonWebKeywords.Click Element    ${dictCheckoutPage}[rad_nextday_delivery]

Select the desire store
    [Arguments]    ${desireStore}
    CommonWebKeywords.Click Element    ${desireStore}
    Wait Until Page Loader Is Not Visible

Input name or postcode then select the store
    [Arguments]    ${name_or_postcode}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_StoreNameOrLocation]    ${name_or_postcode}
    Wait Until Page Is Completely Loaded

Verify the shops are returned contains keyword is name
    [Arguments]    ${name}
    Wait Until Page Is Completely Loaded
    ${elements}    SeleniumLibrary.Get Webelements    ${dictCheckoutPage}[specificStore]
    @{name_list}    Create List
    FOR    ${element}    IN    @{elements}
        ${element_text}    SeleniumLibrary.Get Text    ${element}
        Should Contain    ${element_text}    ${name}    ignore_case=True
    END

Verify the shops are returned contains keyword is post code
    [Arguments]    ${postcode}
    ${storeAddress}=    SeleniumLibrary.Get Text    ${dictCheckoutPage}[storeAddress]
    ${postcode}=    Evaluate    str(${postcode})
    Should Contain Any    ${storeAddress}    ${postcode}

Verify the list store be returned contains the keyword is post code or name
    [Arguments]    ${type_name}    ${name_or_postcode}
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[txt_StoreNameOrLocation]
    SeleniumLibrary.Element Should Be Visible    ${dictCheckoutPage}[specificStore]
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[specificStore]
    Run Keyword If    '${type_name}'=='${order_detail_select_store.type_postcode}'    Verify the shops are returned contains keyword is post code    ${name_or_postcode}
    ...    ELSE IF    '${type_name}'=='${order_detail_select_store.type_name}'    Verify the shops are returned contains keyword is name    ${name_or_postcode}

Select bank pay by installment
    [Arguments]    ${bank}
    ${elem}=  CommonKeywords.Format Text    ${dictCheckoutPage}[btn_bank_installment]    $bank=${bank}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Select installment month plan
    [Arguments]    ${months}
    ${elem}=  CommonKeywords.Format Text    ${dictCheckoutPage}[ddl_items_plan_installment]    $months=${months}
    ${value}=    SeleniumLibrary.Get Value    ${elem}
    SeleniumLibrary.Select From List By Value    ${dictCheckoutPage}[ddl_plan_installment]    ${value}

Verify same-day option should not display in shipping
    SeleniumLibrary.Page Should Not Contain Element    ${dictCheckoutPage}[same_day_delivery]

Verify Next-day option should not display in shipping
    SeleniumLibrary.Page Should Not Contain Element    ${dictCheckoutPage}[next_day_delivery]
       
Click on home delivery option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[rdo_home_delivery_option]
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[rdo_home_delivery_option]
    Wait Until Page Is Completely Loaded

Click on click and collect option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[rdo_pickup_at_store_option]
    Scroll To Element    ${dictCheckoutPage}[rdo_pickup_at_store_option]
    CommonWebKeywords.Click Element        ${dictCheckoutPage}[rdo_pickup_at_store_option]
    Wait Until Page Is Completely Loaded

Input phone number delivery option
    [Arguments]    ${telephone}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_phone_delivery_option]    ${telephone}
    Wait Until Page Is Completely Loaded

Input store name or postcode
    [Arguments]    ${name_or_postcode}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_StoreNameOrLocation]    ${name_or_postcode}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[btn_select_store]    $store=${name_or_postcode}
    Wait Until Page Is Completely Loaded

Click on select store button
    [Arguments]    ${name_or_postcode}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[btn_select_store]    $store=${name_or_postcode}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    CommonWebKeywords.Scroll And Click Element    ${elem}
    Wait Until Page Is Completely Loaded

Verify click and collect options should be visible
    [Arguments]    ${label_code}
    ${elem}=    Format Text    ${dictCheckoutPage}[rdo_click_and_collect]    $delivery_option=${label_code}
    Run Keyword If    '${label_code}'=='today_pickup'    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ...    ELSE    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    

Verify 2 hour pick up option should not be visible
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictCheckoutPage}[rdo_click_and_collect]

Verify standard pickup option should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[rdo_standard_pickup]

Verify number of items available for 2 hour pickup display correctly
    [Arguments]    ${name_or_postcode}    ${quantity}=1
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[lbl_number_of_items_store]    $store=${name_or_postcode}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${elem}   ${quantity}

Click on 2 hour pickup option after select store
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[rdo_2_hour_pick_up]
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[rdo_2_hour_pick_up]
    Wait Until Page Is Completely Loaded

Click on standard pickup option after select store
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[rdo_standard_pickup]
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[rdo_standard_pickup]
    Wait Until Page Is Completely Loaded

Get method label code
    [Arguments]    ${username}    ${password}    ${store}    ${api_version}    ${store_name}
    ${user_token}=    Get login token    ${username}    ${password}
    ${response}=    cart.Get cart estimate shipping method    ${user_token}    ${store}    ${api_version}
    ${value}=    Get Value From Json    ${response}    $..pickup_stores_location[?(@.name=='${store_name}')]..additional_text..method_label_code
    [Return]    ${value}[0]

Click pay now button at payment failed page
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_pay_now_cancel_payment]

Verify error payment message
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[msg_payment_failed]

Verify section on payment failed page after click cancel otp
    Verify sorry payment failed title
    Verify order number is displayed correctly
    Verify created date is displayed correctly
    Verify payment status is displayed correctly
    Verify payment type is displayed correctly

Verify section payment successful
    Verify order number is displayed correctly
    Verify created date is displayed correctly
    Verify payment status is displayed correctly
    Verify payment type is displayed correctly

Get discount price by apply coupon
    [Arguments]    ${coupon}
    ${elem}=    Format Text    ${dictCheckoutPage}[txt_coupon_price]    $coupon=${coupon}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${discount_price}=    SeleniumLibrary.Get Text    ${elem}
    ${number}=   Convert To Two Digit Number    ${discount_price}
    [Return]    ${number}

Get redeem point from account T1C
    [Arguments]    ${redeem_response}
    ${total_point}=    Get Value From Json    ${redeem_response}    $.points
    ${rate}=    Get Value From Json    ${redeem_response}    $.conversion_rate
    ${allowed_point}=    Get Value From Json    ${redeem_response}    $.min_allowed_points
    ${baht}=    Evaluate    ${allowed_point}[0]/${rate}[0]
    ${baht}=    Convert price to total amount int format    ${baht}
    [Return]    ${total_point}[0]    ${allowed_point}[0]    ${baht}

Verify redeem point display correctly
    [Arguments]    ${point}    ${discount}
    ${elem1}=    Format Text    ${dictCheckoutPage}[txt_redeem_point_summary]    $point=${point}
    ${elem2}=    Format Text    ${dictCheckoutPage}[txt_discount_redeem]    $discount=${discount}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem1}    ${elem2}

Verify Home Delivery section should not display in shipping
    CommonWebKeywords.Verify Web Elements Are Not Visible   ${dictCheckoutPage}[txt_verify_home_delivery_title]

Verify Standard Delivery option should display at Home Delivery option in shipping
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_verify_standard_delivery_title]

Verify Standard Pickup option should display at Click & Collect option in shipping
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_verify_standard_pickup_title]

Verify product view is not visible in order summary
    [Arguments]    ${product_skus}
    FOR    ${product_sku}    IN    @{product_skus}
        ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[product_view_container]    $sku=${product_sku}
        CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}   
    END    

Select Payment Method Pay at Store/Cash on Delivery
    Wait Until Keyword Succeeds    3 x    2s    Select Radio Button    ${dictCheckoutPage}[group_payment]    ${dictCheckoutPage}[rdo_payatstore_cod]

Click Confirm Order For Pay at Store/Cash on Delivery Payment Method
    CommonWebKeywords.Click Element Using Javascript    ${dictCheckoutPage}[btn_confirm_cod_payatStore]
    Wait Until Page Loader Is Not Visible        

Select Payment Method Cash on Delivery
    CommonWebKeywords.Click Element    ${dictCheckoutPage}[btn_cod]
    Wait Until Page Loader Is Not Visible

Select store by store name with new version
    [Arguments]    ${storeName}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[btn_select_store_with_name]    $storeName=${storeName}
    CommonWebKeywords.Click Element    ${elem}
    Wait Until Page Loader Is Not Visible

Verify cart total of member have valid total for Pay at Store Payment Method 
    [Arguments]    ${user_token}
    Verify cart total of member is higher than minimum total for Pay at Store Payment Method     ${user_token}    
    Verify cart total of member is lower than maximum total for Pay at Store Payment Method     ${user_token}           

Verify cart total of member is lower than minimum total for Pay at Store Payment Method 
    [Arguments]    ${user_token}
    ${totalCart}=    cart.Get cart total    ${user_token}
    ${flag}=    Evaluate    ${totalCart} <= 699
    Should Be True    ${flag}    msg=Cart total is higher than minimum order value (฿699).

Verify cart total of member is higher than minimum total for Pay at Store Payment Method 
    [Arguments]    ${user_token}
    ${totalCart}=    cart.Get cart total    ${user_token}
    ${flag}=    Evaluate    ${totalCart} >= 699
    Should Be True    ${flag}    msg=Cart total is lower than minimum order value (฿699).

Verify cart total of member is lower than maximum total for Pay at Store Payment Method 
    [Arguments]    ${user_token}
    ${totalCart}=    cart.Get cart total    ${user_token}
    ${flag}=    Evaluate    ${totalCart} <= 5000
    Should Be True    ${flag}    msg=Cart total is higher than maximum order value (฿5000).          

Verify cart total of member is higher than maximum total for Pay at Store Payment Method 
    [Arguments]    ${user_token}
    ${totalCart}=    cart.Get cart total    ${user_token}
    ${flag}=    Evaluate    ${totalCart} >= 5000
    Should Be True    ${flag}    msg=Cart total is lower than maximum order value (฿5000).    

Wait Until T1C Loader Is Not Visible  
    Seleniumlibrary.Wait Until Page Does Not Contain Element    ${dictCheckoutPage}[t1c_loader]    timeout=${GLOBALTIMEOUT}
    Wait Until Page Is Completely Loaded

Verify grand total is valid total for Pay at Store Payment Method
    Verify grand total is higher than minimum total for Pay at Store Payment Method
    Verify grand total is lower than maximum total for Pay at Store Payment Method 

Verify grand total is lower than minimum total for Pay at Store Payment Method 
    ${grand_total}=    checkout_page.Get grand total from order summary
    ${flag}=    Evaluate    ${grand_total} <= 699
    Should Be True    ${flag}    msg=Cart total is higher than minimum order value (฿699).

Verify grand total is higher than minimum total for Pay at Store Payment Method 
    ${grand_total}=    checkout_page.Get grand total from order summary
    ${flag}=    Evaluate    ${grand_total} >= 699
    Should Be True    ${flag}    msg=Cart total is lower than minimum order value (฿699).

Verify grand total is lower than maximum total for Pay at Store Payment Method 
    ${grand_total}=    checkout_page.Get grand total from order summary
    ${flag}=    Evaluate    ${grand_total} <= 5000
    Should Be True    ${flag}    msg=Cart total is higher than maximum order value (฿5000).          

Verify grand total is higher than maximum total for Pay at Store Payment Method 
    ${grand_total}=    checkout_page.Get grand total from order summary
    ${flag}=    Evaluate    ${grand_total} >= 5000
    Should Be True    ${flag}    msg=Cart total is lower than maximum order value (฿5000). 