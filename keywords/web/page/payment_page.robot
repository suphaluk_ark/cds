*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/payment_page.robot

*** Keywords ***
Get shipping customer name
    ${customer_name}=    common_cds_web.Get label text by element    ${dictPaymentPage}[lbl_shipping_full_name]
    [Return]    ${customer_name}

Get shipping address
    ${address}=    common_cds_web.Get label text by element    ${dictPaymentPage}[lbl_shipping_address]
    [Return]    ${address}

Get shipping telephone
    ${telephone}=    common_cds_web.Get label text by element    ${dictPaymentPage}[lbl_shipping_telephone]
    [Return]    ${telephone}

Get tax invoice customer name
    ${customer_name}=    common_cds_web.Get label text by element    ${dictPaymentPage}[lbl_taxinvoice_full_name]
    [Return]    ${customer_name}

Get tax invoice address
    ${address}=    common_cds_web.Get label text by element    ${dictPaymentPage}[lbl_taxinvoice_address]
    [Return]    ${address}

Get tax invoice telephone
    ${telephone}=    common_cds_web.Get label text by element    ${dictPaymentPage}[lbl_taxinvoice_telephone]
    [Return]    ${telephone}

Close view deatails on mobile
    CommonWebKeywords.Click Element    ${dictPaymentPage}[lbl_close_view_details]

Click on view details on mobile
    CommonWebKeywords.Click Element    ${dictPaymentPage}[lbl_view_details]

Verify payment icon of full payment method should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictPaymentPage}[icon_credit_card_full_payment]

Verify radio button with text as Credit Card (Full Payment) should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictPaymentPage}[lbl_credit_card_full_payment]

Verify on top discount icon shows beside Credit Card (Full Payment) should be displayed
    ${text}=    SeleniumLibrary.Get Text  ${dictPaymentPage}[lbl_on_top_discount]
    Should Be Equal As Strings  ${text}  ${payment_page.on_top_discount}

Verify shipping summary title should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictPaymentPage}[lbl_shipping_summary]

Verify shipping name should be displayed
    ${text}=  Get shipping customer name
    Should Not Be Empty  ${text}

Verify shipping address should be displayed
    ${text}=  Get shipping address
    Should Not Be Empty  ${text}

Verify shipping telephone should be displayed
    ${text}=  Get shipping telephone
    Should Not Be Empty  ${text}

Verify edit shipping summary should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictPaymentPage}[lnk_edit_shipping_summary]

####
Verify billing summary title should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictPaymentPage}[lbl_billing_address]

Verify billing name should be displayed
    ${text}=  payment_page.Get tax invoice customer name
    Should Not Be Empty  ${text}

Verify billing address should be displayed
    ${text}=  payment_page.Get tax invoice address
    Should Not Be Empty  ${text}

Verify billing telephone should be displayed
    ${text}=  payment_page.Get tax invoice telephone
    Should Not Be Empty  ${text}

Verify shipping address shows information on payment page correctly
    [Arguments]  ${customer_name}  ${shipping_address}  ${telephone}
    ${cust_name}=    payment_page.Get shipping customer name
    Should Be Equal As Strings  ${cust_name}  ${customer_name}
    ${address}=    payment_page.Get shipping address
    Should Be Equal As Strings  ${address}  ${shipping_address}
    ${tel}=    payment_page.Get shipping telephone
    Should Be Equal As Strings  ${tel}  ${telephone}

Verify billing address shows information on payment page correctly
    [Arguments]  ${cust_name}  ${billing_address}  ${telephone}
    ${name}=  payment_page.Get tax invoice customer name
    Should Be Equal As Strings  ${name}  ${cust_name}
    ${address}=  payment_page.Get tax invoice address
    Should Be Equal As Strings  ${address}  ${billing_address}
    ${tel}=  payment_page.Get tax invoice telephone
    Should Be Equal As Strings  ${tel}  ${telephone}

Click Edit shipping address link
    CommonWebKeywords.Click Element  ${dictPaymentPage}[lnk_edit_shipping_summary]

###  Order Summary ###
Verify Edit Bag should be display in Order Summary
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictPaymentPage}[lnk_edit_bag]

Click Edit Bag in Order Summary
    CommonWebKeywords.Click Element  ${dictPaymentPage}[lnk_edit_bag]

Verify product name should be displayed
    [Arguments]  ${product_sku}
    ${text}=    CommonKeywords.Format Text    ${dictPaymentPage}[lbl_product_name]     $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${text}

Verify product image should be displayed
    [Arguments]  ${img_link}
    ${text}=    CommonKeywords.Format Text    ${dictPaymentPage}[img_product]     $img_link=${img_link}
    CommonWebKeywords.Verify Web Elements Are Visible  ${text}

Verify product quanity should be displayed
    [Arguments]  ${product_sku}
    ${text}=    CommonKeywords.Format Text    ${dictPaymentPage}[lbl_product_quantity]     $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${text}

Verify product quantity should be the same as delivery page
    [Arguments]  ${product_sku}  ${product_quantity}
    ${text}=    CommonKeywords.Format Text    ${dictPaymentPage}[lbl_product_quantity]     $product_sku=${product_sku}
    ${qtty}=    SeleniumLibrary.Get Text  ${text}
    Should Be Equal As Strings  ${qtty}  ${product_quantity}

Verify product price should be displayed
    [Arguments]  ${product_sku}
    ${text}=    CommonKeywords.Format Text    ${dictPaymentPage}[lbl_product_price]     $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible  ${text}

Get price from order summary
    [Arguments]  ${product_sku}
    ${text}=    CommonKeywords.Format Text    ${dictPaymentPage}[lbl_product_price]     $product_sku=${product_sku}
    ${price}=  SeleniumLibrary.Get Text  ${text}
    ${price}=      CommonKeywords.Convert price to number format  ${price}
    [Return]  ${price}

Get product quantity from order summary
    [Arguments]  ${product_sku}
    ${text}=    CommonKeywords.Format Text    ${dictPaymentPage}[lbl_product_quantity]     $product_sku=${product_sku}
    ${qtty}=    SeleniumLibrary.Get Text  ${text}
    ${qtty}=  Convert To Integer  ${qtty}
    [Return]  ${qtty}

Get subtotal on payment summary
    ${text}=   SeleniumLibrary.Get Text  ${dictPaymentPage}[lbl_sub_total]
    ${subtotal}=    CommonKeywords.Convert price to number format  ${text}
    [Return]  ${subtotal}

Get grand total on payment summary
    ${text}=   SeleniumLibrary.Get Text  ${dictPaymentPage}[lbl_grand_total]
    ${grand_total}=    CommonKeywords.Convert price to number format  ${text}
    [Return]  ${grand_total}

Verify subtotal is calculated correctly by sku
    [Arguments]  ${product_sku}
    ${price}=  Get price from order summary  ${product_sku}
    ${quantity}=  payment_page.Get product quantity from order summary  ${product_sku}
    ${number}=  Evaluate  ${price}*${quantity}
    ${sub_total}=  Get subtotal on payment summary
    Should Be Equal As Strings  ${number}  ${sub_total}

Verify subtotal should be equal grand total on payment summary
    ${sub_total}=  Get subtotal on payment summary
    ${grand_total}=  Get grand total on payment summary
    Should Be Equal As Strings  ${sub_total}  ${grand_total}

Verify payment summary title should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictPaymentPage}[h3_payment_summary]

Verify total text should be displayed from payment summary
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictPaymentPage}[h4_total]

Verify order total text should be displayed from payment summary
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictPaymentPage}[h4_order_total]

Verify total should displayed in correct format
    ${text}=  SeleniumLibrary.Get Text  ${dictPaymentPage}[lbl_sub_total]
    ${number}=  CommonKeywords.Convert price to number format  ${text}
    ${number}=  common_cds_web.Convert price to string format  ${number}
    SeleniumLibrary.Element Text Should Be  ${dictPaymentPage}[lbl_sub_total]  ฿${number}

Verify order total should displayed in correct format
    ${text}=  SeleniumLibrary.Get Text  ${dictPaymentPage}[lbl_grand_total]
    ${number}=  CommonKeywords.Convert price to number format  ${text}
    ${number}=  common_cds_web.Convert price to string format  ${number}
    SeleniumLibrary.Element Text Should Be  ${dictPaymentPage}[lbl_grand_total]  ฿${number}

Select pay at store option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictPaymentPage}[rdo_pay_at_store]
    CommonWebKeywords.Click Element   ${dictPaymentPage}[rdo_pay_at_store]
    Wait Until Page Loader Is Not Visible
    
Click on confirm payment button for pay at store
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictPaymentPage}[btn_confirm_pay_at_store]
    CommonWebKeywords.Click Element Using Javascript    ${dictPaymentPage}[btn_confirm_pay_at_store]
    Wait Until Page Loader Is Not Visible

Get payment method name
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictPaymentPage}[rdo_pay_at_store]
    ${payment_method_name}=    SeleniumLibrary.Get Text    ${dictPaymentPage}[rdo_pay_at_store]
    [Return]    ${payment_method_name}

Get pay at store warning message
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictPaymentPage}[txt_pay_at_store_warning_msg]
    ${warning_msg}=    SeleniumLibrary.Get Text    ${dictPaymentPage}[txt_pay_at_store_warning_msg]
    [Return]    ${warning_msg}

Verify payment method pay at store should not Visible
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictPaymentPage}[rdo_pay_at_store]

Verify payment method pay at store should Visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictPaymentPage}[rdo_pay_at_store]

Verify input text T1C earn point is Visible
    Wait Until Keyword Succeeds    5 x    3 sec    CommonWebKeywords.Verify Web Elements Are Visible    ${dictPaymentPage}[txt_addT1_earn_point]

Verify Pay at Store/Cash on Delivery option not show at payment page
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictCheckoutPage}[txt_verify_cod_payatstore_title]   
    
Verify Cash on Delivery option show correctly at payment page   
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_verify_cod_title]

Verify Cash on Delivery option not show at payment page
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictCheckoutPage}[txt_verify_cod_title]    

Verify that Pay at Store/Cash on Delivery method is displayed details correctly at Checkout step 2
    Verify Pay at Store/Cash on Delivery title in payment page

Verify Pay at Store option show correctly at payment page 
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_verify_payatstore_title]

Verify Pay at Store option not show at payment page
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictCheckoutPage}[txt_verify_payatstore_title]

Verify Pay at Store/Cash on Delivery title in payment page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_verify_cod_payatstore_title]
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictCheckoutPage}[txt_verify_cod_payatstore_title]    ${payment_page.payatstore_cod_title}

Proceed for payment and verify with Pay at Store/Cash on Delivery
    checkout_page.Payment Page Should Be Displayed
    Verify that Pay at Store/Cash on Delivery method is displayed details correctly at Checkout step 2 
    Select Payment Method Pay at Store/Cash on Delivery
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For Pay at Store/Cash on Delivery Payment Method
    checkout_page.Thank You Page Should Be Visible    

Proceed for payment and verify with Pay at Store
    checkout_page.Payment Page Should Be Displayed
    Verify Pay at Store option show correctly at payment page 
    Select Payment Method Pay at Store
    Wait Until Page Loader Is Not Visible
    checkout_page.Click Confirm Order For Pay At Store Payment Method
    checkout_page.Thank You Page Should Be Visible