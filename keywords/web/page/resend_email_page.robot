*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Variables ***
${txt_sent_success}           xpath=//div/h1[text()='{$message}']
${btn_resend_email}           id=lnk-viewForgotPassword
${btn_back_to_login}          id=btn-viewLogin

*** Keywords ***
Send E-mail Success Message Should Be Visible
    [Arguments]    ${success_msg}
    ${message_locator}=    CommonKeywords.Format Text    ${txt_sent_success}    $message=${success_msg}
    CommonWebKeywords.Verify Web Elements Are Visible     ${message_locator}

Click Resend E-mail Button
    CommonWebKeywords.Click Element    ${btn_resend_email}

Click Back To Login Page Button
    CommonWebKeywords.Click Element    ${btn_back_to_login}