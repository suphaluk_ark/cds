*** Setting ***
Resource    ${CURDIR}/../web_imports.robot

*** Variables ***
&{order_history_dict}    search_order=xpath=//input[@name='orderNo']
...    lbl_view_order_details=xpath=//a[@to='/account/orders/{$order_number}']
...    lbl_order_number=xpath=//strong[contains(.,'{$order_number}')]
...    lbl_order_status=xpath=//strong[contains(.,'{$order_number}')]/../../..//span[text()='{$order_status}']
...    lbl_payment_type=xpath=//strong[contains(.,'{$order_number}')]/../../..//p[text()='{$payment_type}']
...    lbl_order_total=xpath=(//strong[contains(.,'{$order_number}')]/../../..//div[text()='${order_history.order_total}']/..//div)[2]
...    lbl_shipping_method=xpath=//strong[contains(.,'{$order_number}')]/../../..//p[text()='{$shipping_method}']
...    active_status=xpath=//li[@class='_3KB8D _1GvHC']
...    lbl_tracking_no=xpath=//p[text()='${order_history.tracking_no}']/..//span

...    lbl_order_status=xpath=//p[text()='${order_history.order_status}']/..//span[text()='{$order_status}']
...    lbl_delivery_option=xpath=//div/p[text()='${order_history.delivery_option}']/..//p[.='{$delivery_status}']
...    lbl_payment=xpath=//div/p[.='${order_history.payment_type}']/..//p[.='{$payment_type}']
...    img_order_product=xpath=//a[@to='/account/orders/{$order_number}']//following::img[contains(@src, "product")][1]

@{status_progress_list}    ${order_history}[status_payment]    ${order_history}[status_processing]    ${order_history}[status_ready_to_ship]    ${order_history}[status_shipping]    ${order_history}[status_ready_to_pick]    ${order_history}[status_complete]

*** Keywords ***
Click on searh order button
    CommonWebKeywords.Click Element    ${order_history_dict}[search_order]

Search order by order number
    [Arguments]    ${order_no}
    Click on searh order button
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${order_history_dict}[search_order]    ${order_no}
    SeleniumLibrary.Press Keys    ${order_history_dict}[search_order]    \\13

Order number should be displayed in order history
    [Arguments]    ${order_no}
    ${order_locator}=    CommonKeywords.Format Text    ${order_history_dict}[lbl_order_number]    $order_number=${order_no}
    CommonWebKeywords.Verify Web Elements Are Visible    ${order_locator}

Order status should be displayed correctly in order history
    [Arguments]    ${order_no}    ${order_status}
    ${status_locator}=    CommonKeywords.Format Text    ${order_history_dict}[lbl_order_status]    $order_number=${order_no}    $order_status=${order_status}
    CommonWebKeywords.Verify Web Elements Are Visible    ${status_locator}

Click view details button by order no.
    [Arguments]    ${order_no}
    ${order_locator}=    CommonKeywords.Format Text    ${order_history_dict}[lbl_view_order_details]    $order_number=${order_no}
    Scroll To Element    ${order_locator}
    CommonWebKeywords.Click Element    ${order_locator}

Order status on progress bar should be displayed correctly
    [Arguments]    ${status}
    CommonWebKeywords.Verify Web Elements Are Visible    ${order_history_dict}[active_status]
    ${active_elems}=    SeleniumLibrary.Get WebElements    ${order_history_dict}[active_status]
    ${len}=    Get Length  ${active_elems}
    : FOR   ${index}    IN RANGE    ${len}
    \    Run Keyword If    '@{status_progress_list}[${index}]'=='${status}'    Run Keywords    Should Be Equal    ${index}    ${${len}-1}    AND    Return From Keyword
    Log to console    Invalid status on progress bar
    Fail

Tracking number should be displayed
    [Arguments]    ${tracking_no}
    CommonWebKeywords.Verify Web Elements Are Visible    ${order_history_dict}[lbl_tracking_no]
    SeleniumLibrary.Element Text Should Be    ${order_history_dict}[lbl_tracking_no]    ${tracking_no}

Order status should be displayed
    [Arguments]    ${order_status}
    ${message}=    CommonKeywords.Format Text    ${order_history_dict}[lbl_order_status]    $order_status=${order_status}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Delivery option should be displayed
    [Arguments]    ${delivery_option}
    ${message}=    CommonKeywords.Format Text    ${order_history_dict}[lbl_delivery_option]    $delivery_status=${delivery_option}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Payment type should be displayed
    [Arguments]    ${payment_type}
    ${message}=    CommonKeywords.Format Text    ${order_history_dict}[lbl_payment]    $payment_type=${payment_type}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}




