*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/delivery_details_page.robot

*** Keywords ***
Click request tax invoice
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[request_tax_invoice_option]
    Wait Until Keyword Succeeds    3 x    2s    CommonWebKeywords.Scroll And Click Element    ${dictDeliveryDetailPage}[request_tax_invoice_option]

Get default tax invoice address
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[default_tax_invoice_address]
    ${tax_invoice_address}=    SeleniumLibrary.Get Text    ${dictDeliveryDetailPage}[default_tax_invoice_address]
    [Return]    ${tax_invoice_address}

Get element label value by tax invoice address id
    [Arguments]    ${locator}
    ${elem}=    CommonKeywords.Format Text    ${locator}    $address_id=${tax_invoice_info}[address_id]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${value}=    SeleniumLibrary.Get Text    ${elem}
    [Return]    ${value}

Get tax invoice address name
    ${address_name}=    Get element label value by tax invoice address id    ${dictDeliveryDetailPage}[lbl_tax_invoice_name]
    [Return]    ${address_name}

Get tax invoice customer name
    ${customer_name}=    Get element label value by tax invoice address id    ${dictDeliveryDetailPage}[lbl_tax_invoice_customer_name]
    [Return]    ${customer_name}

Get tax invoice telephone
    ${telephone}=    Get element label value by tax invoice address id    ${dictDeliveryDetailPage}[lbl_tax_invoice_telephone]
    [Return]    ${telephone}

Get tax invoice address no and building
    ${address_no_building}=    Get element label value by tax invoice address id    ${dictDeliveryDetailPage}[lbl_tax_invoice_address_no_building]
    [Return]    ${address_no_building}

Get tax invoice full address
    ${full_address}=    Get element label value by tax invoice address id    ${dictDeliveryDetailPage}[lbl_tax_invoice_full_address]
    [Return]    ${full_address}

Click change tax invoice address link
    common_keywords.Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[btn_change_tax_invoice_address]
    CommonWebKeywords.Scroll And Click Element    ${dictDeliveryDetailPage}[btn_change_tax_invoice_address]

    
Verify brand name should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_brand_name]

Verify product name should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_product_name]

Verify price should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_quantity]

Verify quantity should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictDeliveryDetailPage}[lbl_quantity]

Verify image of product should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictDeliveryDetailPage}[img_product]

Verify edit bag should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictDeliveryDetailPage}[lnk_edit_bag]

Verify promotion code should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictDeliveryDetailPage}[lbl_promotion]

Verify order summary should be displayed at delivery details page
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictDeliveryDetailPage}[h3_oder_summary]

Verify payment summary should be displayed at delivery details page
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictDeliveryDetailPage}[h3_payment_summary]

Click on edit bag on delivery page
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lnk_edit_bag]
    common_keywords.Wait Until Page Is Completely Loaded


Firstname textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_firstname]

Lastname textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_lastname]

Email textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_email]

Telephone textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_telephone]

The1no textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_the1no]
    
Shipping building textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_shipping_building]

Shipping address no textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_shipping_address_no]
    
Shipping postcode textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_shipping_postcode]

Tax ID card textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_tax_id_card]
    
Tax building textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_tax_building]

Tax address no textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_tax_address_no]
    
Tax postcode textbox is visible wihtout any default value
    common_cds_web.Verify element is displayed wihtout any default value      ${dictDeliveryDetailPage}[txt_tax_postcode]

Input firstname for customer info
    [Arguments]    ${firstname}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_firstname]    ${firstname}

Input lastname for customer info
    [Arguments]    ${lastname}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_lastname]    ${lastname}

Input email for customer info
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_email]    ${email}

Input telephone for customer info
    [Arguments]    ${telephone}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_telephone]    ${telephone}

Input building for shipping info
    [Arguments]    ${building}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_shipping_building]    ${building}

Input address no for shipping info
    [Arguments]    ${address_no}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_shipping_address_no]    ${address_no}

Input postcode for shipping info
    [Arguments]    ${postcode}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_shipping_postcode]    ${postcode}
    Wait Until Page Is Completely Loaded

Input ID card for tax invoice info
    [Arguments]    ${id_card}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[txt_tax_id_card]
    SeleniumLibrary.Input Text    ${dictDeliveryDetailPage}[txt_tax_id_card]    ${id_card}

Input company name for tax invoice info
    [Arguments]    ${company_name}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_tax_company]    ${company_name}

Input tax ID for tax invoice info
    [Arguments]    ${tax_id}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[txt_tax_id]
    SeleniumLibrary.Input Text    ${dictDeliveryDetailPage}[txt_tax_id]    ${tax_id}

Input branch ID for tax invoice info
    [Arguments]    ${branch_id}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_tax_branch_id]    ${branch_id}

Input building for tax invoice info
    [Arguments]    ${building}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_tax_building]    ${building}

Input address no for tax invoice info
    [Arguments]    ${address_no}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_tax_address_no]    ${address_no}

Input postcode for tax invoice info
    [Arguments]    ${postcode}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_tax_postcode]    ${postcode}
    Wait Until Page Is Completely Loaded

Click continue to payment button
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[btn_continue_payment]
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[btn_continue_payment]
    common_keywords.Wait Until Page Is Completely Loaded

Get shipping province selected value
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[sel_shipping_region_id]
    ${province}=    SeleniumLibrary.Get Selected List Label    ${dictDeliveryDetailPage}[sel_shipping_region_id]
    Set Global Variable    ${shipping_province}    ${province}

Get shipping district selected value
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[sel_shipping_district_id]
    ${district}=    SeleniumLibrary.Get Selected List Label    ${dictDeliveryDetailPage}[sel_shipping_district_id]
    Set Global Variable    ${shipping_district}    ${district}

Get shipping subdistrict selected value
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[sel_shipping_subdistrict_id]
    ${subdistrict}=    SeleniumLibrary.Get Selected List Label    ${dictDeliveryDetailPage}[sel_shipping_subdistrict_id]
    Set Global Variable    ${shipping_subdistrict}    ${subdistrict}

Get tax invoice province selected value
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[sel_tax_region_id]
    ${province}=    SeleniumLibrary.Get Selected List Label    ${dictDeliveryDetailPage}[sel_tax_region_id]
    Set Global Variable    ${tax_invoice_province}    ${province}

Get tax invoice district selected value
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[sel_tax_district_id]
    ${district}=    SeleniumLibrary.Get Selected List Label    ${dictDeliveryDetailPage}[sel_tax_district_id]
    Set Global Variable    ${tax_invoice_district}    ${district}

Get tax invoice subdistrict selected value
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[sel_tax_subdistrict_id]
    ${subdistrict}=    SeleniumLibrary.Get Selected List Label    ${dictDeliveryDetailPage}[sel_tax_subdistrict_id]
    Set Global Variable    ${tax_invoice_subdistrict}    ${subdistrict}

Click billing invoice option
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[rdo_billing_option]
    CommonWebKeywords.Scroll And Click Element    ${dictDeliveryDetailPage}[rdo_billing_option]

Verify warning message is displayed
    [Arguments]    ${locator}    ${warning_message}
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[txt_tax_building]
    ${elem}=    CommonKeywords.Format Text    ${locator}    $warning_message=${warning_message}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify warning message is displayed if input invalid personal ID card for tax invoice
    [Arguments]    ${warning_message}
    CommonWebKeywords.Scroll And Click Element    ${dictDeliveryDetailPage}[txt_tax_building]
    Verify warning message is displayed    ${dictDeliveryDetailPage}[lbl_id_card_warning_msg]    ${warning_message}
    Clear Element Text    ${dictDeliveryDetailPage}[txt_tax_id_card]

Verify warning message is displayed if input invalid company tax ID for tax invoice
    [Arguments]    ${warning_message}
    CommonWebKeywords.Scroll And Click Element    ${dictDeliveryDetailPage}[txt_tax_building]
    Verify warning message is displayed    ${dictDeliveryDetailPage}[lbl_tax_id_warning_msg]    ${warning_message}
    Clear Element Text    ${dictDeliveryDetailPage}[txt_tax_id]

Click select shipping address link
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lnk_select_shipping_address]
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lnk_select_shipping_address]

Verify pickup at store text is displayed correctly
    ${text}=    SeleniumLibrary.Wait Until Element Is Visible    ${dictDeliveryDetailPage}[txt_rdo_pickup_at_store]
    ${text}=    SeleniumLibrary.Get Text    ${dictDeliveryDetailPage}[txt_rdo_pickup_at_store]
    Should Be Equal As Strings    ${text}    ${delivery_details_page.pickup_at_store}

Verify Shipping Methods Must Display on Delivery Details Page
    [Arguments]    ${sku}
    ${response}=    Get product via product V2 API by SKU    ${sku}
    ${valueFromJson}=    Get Value From Json     ${response}[0]    $.custom_attributes[?(@.attribute_code == "shipping_methods")].value
    ${shipping_method}=    Split String    ${valueFromJson}[0]    ,
    FOR    ${index}    IN RANGE    len(${shipping_method})
        ${shipping_method_replace}=    Replace String    ${shipping_method}[${index}]    pickupatstore_    ${EMPTY}    count=-1
        ${shipping_method_name}=    Replace String    ${shipping_method_replace}    cds_    ${EMPTY}    count=-1
        ${shippingLocation}=    CommonKeywords.Format Text    ${dictCheckoutPage}[shipping_methods]    $shippingName=${shipping_method_name}
        Run Keyword If    '${shipping_method_name}' == 'pickupatstore'    CommonWebKeywords.Verify Web Elements Are Visible     ${dictCheckoutPage}[shipping_method_pickupatStore]
        ...    ELSE    CommonWebKeywords.Verify Web Elements Are Visible    ${shippingLocation}
    END

Get list active stores
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_store_name]
    ${raw_locator}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[lbl_store_name]
    ${texts}=    SeleniumLibrary.Execute Javascript    var elems = document.querySelectorAll("${raw_locator}"); texts = []; for (elem of elems) {texts.push(elem.innerText)}; return texts;
    [Return]    ${texts}

Get open, close and uptime hour of store by api
    [Arguments]    ${store_data}
    ${day}=    Get Current Date    result_format=%A
    ${open}=    JSONLibrary.Get Value From Json    ${store_data}    $..opening_hours[?(@.day=='${day}')].open
    ${close}=    JSONLibrary.Get Value From Json    ${store_data}    $..opening_hours[?(@.day=='${day}')].close
    ${open}=    CommonKeywords.Change date time to EN format    ${open}[0]    utc    HH:mm    HH:mm:ss
    ${close}=    CommonKeywords.Change date time to EN format    ${close}[0]    utc    HH:mm    HH:mm:ss
    ${uptime_hour}=    Subtract Time From Time    ${close}    ${open}    result_format=number
    ${uptime_hour}=    Set Variable    ${uptime_hour/3600}
    [Return]    ${open}    ${close}    ${uptime_hour}

Click random the active store and return name
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    CommonWebKeywords.Scroll to bottom page
    @{store_names}=    Get list active stores
    @{btn_selects}=    SeleniumLibrary.Get WebElements    ${dictDeliveryDetailPage}[btn_select_store]
    ${len}=    Get Length    ${store_names}
    ${index}=    Evaluate    random.randint(0, ${${len}-1})    modules=random
    ${store_name}=    Set Variable If    ${len}>0    ${store_names}[${index}]    ${NONE}
    CommonWebKeywords.Click Element    ${btn_selects}[${index}]
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[btn_change_store]
    [Return]    ${store_name}

Click random the 2hr pickup store and return name
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_hour_pickup_stores]
    ${hr_store_count}=    SeleniumLibrary.Get Element Count    ${dictDeliveryDetailPage}[lbl_hour_pickup_stores]
    ${hr_pickup_raw_loc}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[lbl_hour_pickup_stores]
    ${btn_select_raw_loc}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[btn_select_store]
    Return From Keyword If    ${hr_store_count}==${0}    ${None}
    ${index}=    Evaluate    random.randint(1, ${hr_store_count})    modules=random
    ${store_name}=    SeleniumLibrary.Get Text    xpath\=(${hr_pickup_raw_loc}//h4)[${index}]
    CommonWebKeywords.Click Element Using Javascript    xpath\=(${hr_pickup_raw_loc}${btn_select_raw_loc})[${index}]
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[btn_change_store]
    [Return]    ${store_name}

Click random the standard pickup store and return name
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_standard_pickup_stores]
    ${standard_store_count}=    SeleniumLibrary.Get Element Count    ${dictDeliveryDetailPage}[lbl_standard_pickup_stores]
    ${standard_pickup_raw_loc}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[lbl_standard_pickup_stores]
    ${btn_select_raw_loc}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[btn_select_store]
    Return From Keyword If    ${standard_store_count}==${0}    ${None}
    ${index}=    Evaluate    random.randint(1, ${standard_store_count})    modules=random
    ${store_name}=    SeleniumLibrary.Get Text    xpath\=(${standard_pickup_raw_loc}//h4)[${index}]
    CommonWebKeywords.Click Element Using Javascript    xpath\=(${standard_pickup_raw_loc}${btn_select_raw_loc})[${index}]
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[btn_change_store]
    [Return]    ${store_name}

Verify having two package delivery methods displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_delivery_method_headers]
    @{delivery_headers}=    SeleniumLibrary.Get WebElements    ${dictDeliveryDetailPage}[lbl_delivery_method_headers]
    ${len}=    Get Length    ${delivery_headers}
    Should Be True    ${len}==2

Verify having only one package delivery methods displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_delivery_method_headers]
    @{delivery_headers}=    SeleniumLibrary.Get WebElements    ${dictDeliveryDetailPage}[lbl_delivery_method_headers]
    ${len}=    Get Length    ${delivery_headers}
    Should Be True    ${len}==1

Verify having two sub package delivery methods displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_sub_delivery_method_headers]
    @{sub_delivery_headers}=    SeleniumLibrary.Get WebElements    ${dictDeliveryDetailPage}[lbl_sub_delivery_method_headers]
    ${len}=    Get Length    ${sub_delivery_headers}
    Should Be True    ${len}==2

Verify having one sub package delivery methods displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_sub_delivery_method_headers]
    @{sub_delivery_headers}=    SeleniumLibrary.Get WebElements    ${dictDeliveryDetailPage}[lbl_sub_delivery_method_headers]
    ${len}=    Get Length    ${sub_delivery_headers}
    Should Be True    ${len}==1

Verify the 2 hour or next day pickup at the top and standard pickup at the bottom for split order only
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[icon_hour_pickup]    ${dictDeliveryDetailPage}[icon_standard_pickup]
    ${hour_pickup_text}=    SeleniumLibrary.Get Text    ${dictDeliveryDetailPage}[lbl_hour_pickup]
    ${hour_pickup_check}=    Set Variable If    '${hour_pickup_text}'=='${shipping_method_label.one_hour_pickup.today}' or '${hour_pickup_text}'=='${shipping_method_label.one_hour_pickup.next_day}'     ${TRUE}    ${FALSE}
    Should Be True    ${hour_pickup_check}    msg=Please double-check position of the hour pickup header at main package. Expected result: The position of 2hr pickup option should be above the standard pickup
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictDeliveryDetailPage}[lbl_standard_pickup]    ${shipping_method_label.click_n_collect.label}

Verify the standard pickup at the top and standard delivery at the bottom for split order only
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[icon_sub_standard_pickup]    ${dictDeliveryDetailPage}[icon_sub_standard_delivery]
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictDeliveryDetailPage}[lbl_sub_standard_pickup]    ${shipping_method_label.click_n_collect.label}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictDeliveryDetailPage}[lbl_sub_standard_delivery]    ${shipping_method_label.home_delivery.label}

Verify the 2 hour or next day pickup is disable
    ${selected_icon}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[img_selected_icon]
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictDeliveryDetailPage}[lbl_2_hour_header]    ${dictDeliveryDetailPage}[lbl_2_hour_header] ${selected_icon}

Verify should be clickable on both with show 2hr pickup flow
    ${selected_icon}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[img_selected_icon]
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lbl_standard_pickup_header]
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_standard_pickup_header] ${selected_icon}
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lbl_2_hour_header]
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_2_hour_header] ${selected_icon}

Verify should be clickable on both sub package of split order
    ${selected_icon}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[img_selected_icon]
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lbl_sub_standard_pickup_header]
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_sub_standard_pickup_header] ${selected_icon}
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lbl_sub_standard_delivery_header]
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_sub_standard_delivery_header] ${selected_icon}

Verify should be clickable on standard pickup and 2hr pickup is not visible
    ${selected_icon}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[img_selected_icon]
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lbl_standard_pickup_header]
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_standard_pickup_header] ${selected_icon}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictDeliveryDetailPage}[lbl_2_hour_header]

Verify the product quantity displayed correctly on 2hr pickup
    [Arguments]    ${qty}
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[lbl_2_hour_header]
    Wait Until Page Is Completely Loaded
    ${stt_2hour}=    SeleniumLibrary.Get Text    ${dictDeliveryDetailPage}[lbl_sku_qty]
    @{nums}=    Get Regexp Matches    ${stt_2hour}    [0-9]+
    ${len}=    Get Length    ${nums}
    ${stt_2hour}=    Set Variable    ${nums}[${len-1}]
    Should Be True    '${stt_2hour}'=='${qty}'

Verify the product quantity displayed correctly on standard pickup
    [Arguments]    ${qty}    ${is_subpkg}
    ${std_pickup_header}=    Set Variable If    ${Is_subpkg}==${TRUE}    ${dictDeliveryDetailPage}[lbl_sub_standard_pickup_header]    ${dictDeliveryDetailPage}[lbl_standard_pickup_header]
    CommonWebKeywords.Click Element    ${std_pickup_header}
    Wait Until Page Is Completely Loaded
    ${lbl_qty}=    Set Variable If    ${Is_subpkg}==${TRUE}    ${dictDeliveryDetailPage}[lbl_sub_sku_qty]    ${dictDeliveryDetailPage}[lbl_sku_qty]
    ${status}=    SeleniumLibrary.Get Text    ${lbl_qty}
    @{nums}=    Get Regexp Matches    ${status}    [0-9]+
    ${status}=    Get From List    ${nums}    -1
    Should Be True    '${status}'=='${qty}'

Verify the product quantity displayed correctly on standard pickup of split order
    [Arguments]    ${qty}    ${is_subpkg}
    ${total_qty}=    Evaluate    sum([int(i) for i in $qty])
    ${std_pickup_header}=    Set Variable If    ${Is_subpkg}==${TRUE}    ${dictDeliveryDetailPage}[lbl_sub_standard_pickup_header]    ${dictDeliveryDetailPage}[lbl_standard_pickup_header]
    CommonWebKeywords.Click Element    ${std_pickup_header}
    Wait Until Page Is Completely Loaded
    ${lbl_qty}=    Set Variable If    ${Is_subpkg}==${TRUE}    ${dictDeliveryDetailPage}[lbl_sub_sku_qty]    ${dictDeliveryDetailPage}[lbl_sku_qty]
    ${status}=    SeleniumLibrary.Get Text    ${lbl_qty}
    @{nums}=    Get Regexp Matches    ${status}    [0-9]+
    ${status}=    Get From List    ${nums}    -1
    Should Be True    '${status}'=='${total_qty}'

Verify the product quantity displayed correctly on standard delivery
    [Arguments]    ${qty}    ${is_subpkg}
    ${std_delivery_header}=    Set Variable If    ${is_subpkg}==${TRUE}    ${dictDeliveryDetailPage}[lbl_sub_standard_delivery_header]    ${dictDeliveryDetailPage}[lbl_standard_delivery_header]
    CommonWebKeywords.Click Element    ${std_delivery_header}
    Wait Until Page Is Completely Loaded
    ${lbl_qty}=    Set Variable If    ${is_subpkg}==${TRUE}    ${dictDeliveryDetailPage}[lbl_sub_sku_qty]    ${dictDeliveryDetailPage}[lbl_sku_qty]
    ${status}=    SeleniumLibrary.Get Text    ${lbl_qty}
    @{nums}=    Get Regexp Matches    ${status}    [0-9]+
    ${status}=    Get From List    ${nums}    -1
    Should Be True    '${status}'=='${qty}'

Verify next day or 2 hour pickup title displayed correctly
    [Arguments]    ${store_data}
    ${2hr_flag}=    Set Variable If    ${store_uptime}==24    ${TRUE}    ${FALSE}
    ${cut_off}=    JSONLibrary.Get Value From Json    ${store_data}    $..cut_off_time
    ${dt_cut_off}=    CommonKeywords.Change date time to EN format    ${cut_off}[0]    utc    HH:mm    HH:mm:ss
    ${current}=    Get Current Date    result_format=%H:%M:%S
    ${time}=    Subtract Time From Time    ${current}    ${dt_cut_off}    result_format=number
    ${pickup_2hr}=    SeleniumLibrary.Get Text    ${dictDeliveryDetailPage}[lbl_hour_pickup]
    # Get label delivery option by API
    ${est_pickup_title}=    JSONLibrary.Get Value From Json    ${store_data}    $..additional_text.method_label_code
    ${est_pickup_time}=    JSONLibrary.Get Value From Json    ${store_data}    $..additional_text.date_time
    ${is_next_day}=    Set Variable If    '${est_pickup_title}[0]' == '${shipping_methods.two_hour_pickup.lbl_nextday_code}'    ${TRUE}    ${FALSE}
    Verify next day or 2 hour pickup time displayed correctly    ${est_pickup_time}[0]
    Run Keyword And Return If    ${2hr_flag} == ${TRUE}    Should Contain    ${pickup_2hr}    ${shipping_method_label.one_hour_pickup.today}
    Run Keyword If    ${is_next_day}==${TRUE}    Should Contain    ${pickup_2hr}    ${shipping_method_label.one_hour_pickup.next_day}
    ...    ELSE IF    ${time} >= 0    Should Contain    ${pickup_2hr}    ${shipping_method_label.one_hour_pickup.today}

Verify standard pickup title displayed correctly
    ${raw_locator}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[lbl_standard_pickup_header] p:first-child
    ${standard_pickup}=    SeleniumLibrary.Get Text    css=${raw_locator}
    Should Contain    ${standard_pickup}    ${shipping_method_label.click_n_collect.label}

Verify the sub standard pickup title displayed correctly
    ${raw_locator}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[lbl_sub_standard_pickup_header] p:first-child
    ${standard_pickup}=    SeleniumLibrary.Get Text    css=${raw_locator}
    Should Contain    ${standard_pickup}    ${shipping_method_label.click_n_collect.label}

Verify the sub standard delivery title displayed correctly
    ${raw_locator}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[lbl_sub_standard_delivery_header] p:first-child
    ${standard_pickup}=    SeleniumLibrary.Get Text    css=${raw_locator}
    Should Contain    ${standard_pickup}    ${shipping_method_label.home_delivery.label}

Verify the uptime of current store
    [Arguments]    ${store_data}
    ${open}    ${close}    ${uptime_hour}    Get open, close and uptime hour of store by api    ${store_data}
    Set Test Variable    ${store_uptime}    ${uptime_hour}
    Set Test Variable    ${store_close}    ${close}
    ${uptime_ui}=    SeleniumLibrary.Get Text    ${dictDeliveryDetailPage}[lbl_store_uptime]
    ${status}=    Set Variable If    '${open}'=='00:00:00' and '${close}'=='00:00:00'    ${TRUE}    ${FALSE}
    Run Keyword If    '${status}'=='${TRUE}'    Verify the uptime of current store is open 24 hours    ${uptime_ui}
    ...    ELSE    Verify the uptime of current store is not open 24 hours     ${open}    ${close}    ${uptime_ui}

Verify the uptime of current store is open 24 hours
    [Arguments]    ${uptime_ui}
    Should Be Equal As Strings    ${checkout_page.full_time_open}    ${uptime_ui}

Verify the uptime of current store is not open 24 hours
    [Arguments]    ${open}    ${close}    ${uptime_ui}
    ${uptime_ui}=    Get Regexp Matches    ${uptime_ui}    [0-9]{1,2}:[0-9]{2}
    ${ui_open}=    Set Variable    ${uptime_ui}[0]
    ${ui_close}=    Set Variable    ${uptime_ui}[1]
    ${ui_open}=    CommonKeywords.Change date time to EN format    ${ui_open.strip().upper()}    utc    HH:mm    HH:mm:ss
    ${ui_close}=    CommonKeywords.Change date time to EN format    ${ui_close.strip().upper()}    utc    HH:mm    HH:mm:ss
    ${subtract_open}=    Subtract Time From Time    ${ui_open}    ${open}    result_format=number
    ${subtract_close}=    Subtract Time From Time    ${ui_close}    ${close}    result_format=number
    Should Be True    ${subtract_open}==0.0 and ${subtract_close}==0.0

Verify next day or 2 hour pickup time displayed correctly
    [Arguments]    ${raw_time}
    ${api_time}=      Add Time To Date      ${raw_time}      7hour    result_format=%H:%M
    ${raw_locator}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[lbl_2_hour_header] div ~ p
    ${text}=    SeleniumLibrary.Get Text    css=${raw_locator}
    ${ui_time}=    get Regexp Matches    ${text}    [0-9]{1,2}:[0-9]{2}
    ${ui_time}=    CommonKeywords.Change date time to EN format    ${ui_time[0].strip().upper()}    utc    HH:mm    HH:mm:ss
    ${api_time}=    CommonKeywords.Change date time to EN format    ${api_time.strip().upper()}    utc    HH:mm    HH:mm:ss
    ${subtract_time}=    Subtract Time From Time    ${ui_time}    ${api_time}    result_format=number
    Should Be True    ${subtract_time}<=300.0 and ${subtract_time}>=0.0

Verify label free text display correctly with 2 hour pickup or next day pickup
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_hour_pickup_free_text]

Verify label free text display correctly with standard pickup
    [Arguments]    ${is_subpkg}
    ${std_pickup_header}=    Set Variable If    ${is_subpkg}==${TRUE}    ${dictDeliveryDetailPage}[lbl_sub_standard_pickup_free_text]    ${dictDeliveryDetailPage}[lbl_standard_pickup_free_text]
    CommonWebKeywords.Verify Web Elements Are Visible    ${std_pickup_header}

Verify label free text display correctly with home delivery
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_sub_standard_delivery_free_text]

Verify the phone number field display after select standard pickup
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_phone_delivery_option]    ${dictCheckoutPage}[lbl_phone_number_info]

Verify the store name or postcode text field
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_StoreNameOrLocation]    ${dictCheckoutPage}[btn_postcode_arrow]

Verify the user location section
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[icon_user_location]    ${dictCheckoutPage}[btn_user_location]

Input the user phone number
    [Arguments]    ${phone_number}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictCheckoutPage}[txt_phone_delivery_option]    ${phone_number}

Verify the Select store buttons disable
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    CommonWebKeywords.Scroll to bottom page
    ${btn_select_stores}=    SeleniumLibrary.Get Element Count    ${dictDeliveryDetailPage}[btn_select_store]
    ${raw_location}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[btn_select_store]
    ${count}=    Set Variable    ${0}
    FOR    ${idx}    IN RANGE    ${btn_select_stores}
        ${cursor}=    CommonWebKeywords.Get Web Element CSS Property Value    xpath=(${raw_location})[${${idx}+1}]    cursor
        ${count}=    Set Variable If    '${cursor}'=='not-allowed'    ${${count}+1}
    END
    Should Be Equal    ${btn_select_stores}    ${count}

Verify the Select store buttons enable
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    CommonWebKeywords.Scroll to bottom page
    ${btn_select_stores}=    SeleniumLibrary.Get Element Count   ${dictDeliveryDetailPage}[btn_select_store]
    ${raw_location}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[btn_select_store]
    ${count}=    Set Variable    ${0}
    FOR    ${idx}    IN RANGE    ${btn_select_stores}
        ${cursor}=    CommonWebKeywords.Get Web Element CSS Property Value    xpath=(${raw_location})[${${idx}+1}]    cursor
        ${count}=    Set Variable If    '${cursor}'=='default'    ${${count}+1}
    END
    Should Be Equal    ${btn_select_stores}    ${count}

Verify the continue payment button enable
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[btn_continue_payment]
    FOR    ${idx}    IN RANGE    ${${GLOBALTIMEOUT}/6}
        ${cursor}=    CommonWebKeywords.Get Web Element CSS Property Value    ${dictDeliveryDetailPage}[btn_continue_payment]    cursor
        Sleep    0.3 sec
        Exit For Loop If    '${cursor}'=='pointer'
    END
    Should Be Equal As Strings    ${cursor}    pointer

Verify the continue payment button disable
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[btn_continue_payment]
    ${cursor}=    CommonWebKeywords.Get Web Element CSS Property Value    ${dictDeliveryDetailPage}[btn_continue_payment]    cursor
    Should Be Equal As Strings    ${cursor}    not-allowed

Verify label free text display display correctly with 2 hour pickup or next day pickup
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_hour_pickup_free_text]

Verify label free text display display correctly with standard pickup 
    [Arguments]    ${is_subpkg}
    ${std_pickup_free_text}=    Set Variable If    ${Is_subpkg}==${TRUE}    ${dictDeliveryDetailPage}[lbl_sub_standard_pickup_free_text]    ${dictDeliveryDetailPage}[lbl_standard_pickup_free_text]
    CommonWebKeywords.Verify Web Elements Are Visible    ${std_pickup_free_text}

Verify label free text display display correctly with home delivery
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_sub_standard_delivery_free_text]

Input firstname for member info
    [Arguments]    ${firstname}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_firstname_member]    ${firstname}

Input lastname for member info
    [Arguments]    ${lastname}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_lastname_member]    ${lastname}

Input telephone for member info
    [Arguments]    ${phone}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_telephone_member]    ${phone}

Input address for member info
    [Arguments]    ${address}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_address_member]    ${address}

Input address for guest info
    [Arguments]    ${address}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_address_guest]    ${address}

Input postcode for member info
    [Arguments]    ${postcode}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictDeliveryDetailPage}[txt_postcode_member]    ${postcode}
    ${postcode}=    Convert To String    ${postcode}
    Set Test Variable    ${postcode_storage}    ${postcode}

Member get shipping address information
    ${province}=    SeleniumLibrary.Get Selected List Label   ${dictDeliveryDetailPage}[cbb_address_province]
    ${district}=    SeleniumLibrary.Get Selected List Label   ${dictDeliveryDetailPage}[cbb_address_district]
    ${sub_district}=    SeleniumLibrary.Get Selected List Label   ${dictDeliveryDetailPage}[cbb_address_subdistrict]
    Set Test Variable    ${province_storage}    ${province}
    Set Test Variable    ${district_storage}    ${district}
    Set Test Variable    ${sub_district_storage}    ${sub_district}

Guest get shipping address information
    ${province}=    SeleniumLibrary.Get Selected List Label   ${dictDeliveryDetailPage}[cbb_address_province_guest]
    ${district}=    SeleniumLibrary.Get Selected List Label   ${dictDeliveryDetailPage}[cbb_address_district_guest]
    ${sub_district}=    SeleniumLibrary.Get Selected List Label   ${dictDeliveryDetailPage}[cbb_address_subdistrict_guest]
    Set Test Variable    ${province_storage}    ${province}
    Set Test Variable    ${district_storage}    ${district}
    Set Test Variable    ${sub_district_storage}    ${sub_district}

Click address book button
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[btn_address_book]

Click add shipping address
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[btn_add_more_shipping_address]

Verify pin your location can not be selected if dont enter postcode
    ${cursor}=    Get Web Element CSS Property Value    ${dictDeliveryDetailPage}[btn_pin_location]    cursor
    Should be Equal As Strings    ${cursor}    not-allowed

Verify pin your location can be selected if enter postcode
    ${cursor}=    Get Web Element CSS Property Value    ${dictDeliveryDetailPage}[btn_pin_location]    cursor
    Should be Equal As Strings    ${cursor}    pointer

Guest verify pin location is not active correctly
    ${cursor}=    Get Web Element CSS Property Value    ${dictDeliveryDetailPage}[btn_pin_location_guest]    cursor
    Should be Equal As Strings    ${cursor}    not-allowed

Guest verify pin location is active correctly
    ${cursor}=    Get Web Element CSS Property Value    ${dictDeliveryDetailPage}[btn_pin_location_guest]    cursor
    Should be Equal As Strings    ${cursor}    pointer

Member input contact information
    delivery_details_page.Input firstname for member info    ${guest_contact_info}[firstname]
    delivery_details_page.Input lastname for member info    ${guest_contact_info}[lastname]
    delivery_details_page.Input telephone for member info    ${guest_contact_info}[telephone]

Member input shipping address information
    delivery_details_page.Input address for member info    ${guest_contact_info}[shipping_building]
    delivery_details_page.Input postcode for member info    ${guest_contact_info}[shipping_postcode]

Verify pin your location popup display correctly
    Wait Until Page Loader Is Not Visible
    delivery_details_page.Verify pin your location title display correctly
    delivery_details_page.Verify map pin location address display correctly

Click pin location confirm button
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[btn_continue_pin_location]

Click pin location button
    CommonWebKeywords.Click Element    ${dictDeliveryDetailPage}[btn_pin_location]

Verify location button message display correctly
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictDeliveryDetailPage}[btn_continue_pin_location]    ${checkout_page.location_is_not_within_district}

Select shipping address from address book
    ${lnk_checkout_address}=    SeleniumLibrary.Get Element Count    ${dictDeliveryDetailPage}[lnk_checkout_address]
    ${raw_locator}=    common_keywords.Get Raw Locator From Dict Locators    ${dictDeliveryDetailPage}[lnk_checkout_address]
    Run Keyword And Return If    ${lnk_checkout_address}==${1}    Fail    The address book must be more than 1 address. Please check again.
    CommonWebKeywords.Click Element    xpath\=(${raw_locator})[${lnk_checkout_address}]

Verify pin location is not active with default address
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictDeliveryDetailPage}[lbl_pin_location]    ${checkout_page.pin_location_not_active}

Verify 3 hour delivery option display correctly
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictDeliveryDetailPage}[lbl_3_hour_delivery]    ${shipping_method_label.three_hour_delivery.label}

Verify pin location require for 3 hour delivery
    delivery_details_page.Click 3 hour delivery option
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictDeliveryDetailPage}[lbl_require_3_hour]    ${checkout_page.require_3_hour_delivery}

Click 3 hour delivery option
    CommonWebKeywords.Scroll And Click Element    ${dictCheckoutPage}[rad_3_hour_delivery]

Verify member pin location address is display correctly
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_pin_location]    ${province_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_pin_location]    ${district_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_pin_location]    ${sub_district_storage}

Verify member pin location address is display correctly when selected 3 hour delivery option
    delivery_details_page.Click 3 hour delivery option
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_3_hour_shipping_address]    ${province_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_3_hour_shipping_address]    ${district_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_3_hour_shipping_address]    ${sub_district_storage}

Verify guest pin location address is display correctly
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_shipping_address_guest]    ${province_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_shipping_address_guest]    ${district_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_shipping_address_guest]    ${sub_district_storage}

Verify guest pin location address is display correctly when selected 3 hour delivery option
    delivery_details_page.Click 3 hour delivery option
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_3_hour_shipping_address]    ${province_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_3_hour_shipping_address]    ${district_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_3_hour_shipping_address]    ${sub_district_storage}

Member get latitude and longitude
    [Arguments]    ${customer_response}    ${post_code}
    ${latitude}=    Get Value From JSON    ${customer_response}    $..addresses[?(@.postcode=='${post_code}')].custom_attributes[?(@.attribute_code=='latitude')].value
    ${longitude}=    Get Value From JSON    ${customer_response}    $..addresses[?(@.postcode=='${post_code}')].custom_attributes[?(@.attribute_code=='longitude')].value
    [Return]    ${latitude}[0]    ${longitude}[0]

Guest get latitude and longitude
    SeleniumLibrary.wait Until Page Contains Element    ${dictDeliveryDetailPage}[lbl_latitude]
    SeleniumLibrary.wait Until Page Contains Element    ${dictDeliveryDetailPage}[lbl_longitude]
    ${latitude}=    SeleniumLibrary.Get Element Attribute    ${dictDeliveryDetailPage}[lbl_latitude]    value
    ${longitude}=    SeleniumLibrary.Get Element Attribute    ${dictDeliveryDetailPage}[lbl_longitude]    value
    [Return]    ${latitude}    ${longitude}

Verify pin your location title display correctly
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictDeliveryDetailPage}[lbl_pin_you_location]

Verify map pin location address display correctly
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_pin_location_map_address]    ${province_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_pin_location_map_address]    ${district_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictDeliveryDetailPage}[lbl_pin_location_map_address]    ${sub_district_storage}

Guest click pin location button
    CommonWebKeywords.Scroll And Click Element    ${dictDeliveryDetailPage}[btn_pin_location_guest]