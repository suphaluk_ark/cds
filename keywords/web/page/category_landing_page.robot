*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/category_PLP.robot

*** Keywords ***
Click subcategory clothing
    [Arguments]    ${sub_category_selected}
    ${elem}=    CommonKeywords.Format Text    ${dictCategoryPLP}[btn_clothing]    $sub_category_selected=${sub_category_selected}
    SeleniumLibrary.Wait Until Page Contains Element    ${elem}
    CommonWebKeywords.Click Element    ${elem}