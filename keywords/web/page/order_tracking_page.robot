*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/order_tracking_page.robot

*** Keywords ***
Click on Tracking your orders
    CommonWebKeywords.Click Element  ${dictOrderTrackingPage}[lbl_order_tracking]

Input Order number 
    [Arguments]    ${order_number}
    SeleniumLibrary.Input Text  ${dictOrderTrackingPage}[txt_order_number]  ${order_number}

Search oder textbox should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictOrderTrackingPage}[txt_order_number]

Click on the first order from menu
    CommonWebKeywords.Click Element  ${dictOrderTrackingPage}[lbl_first_order]

Input email
    [Arguments]    ${email}
    SeleniumLibrary.Input Text  ${dictOrderTrackingPage}[txt_email]  ${email}

Click Track order button
    CommonWebKeywords.Click Element  ${dictOrderTrackingPage}[txt_search_order]

Error message for valid email and order number combination should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictOrderTrackingPage}[msg_invalid_combination]

Require order field message should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictOrderTrackingPage}[msg_order_require]

The order number should be 2
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictOrderTrackingPage}[lbl_orders]
    ${size}=  Get Element Count  ${dictOrderTrackingPage}[lbl_orders]
    Should Be True  ${size}==2

No orders should be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible  ${dictOrderTrackingPage}[lbl_orders]

Order detail page should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictOrderTrackingPage}[lbl_order_ID]

Guest Order detail page should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictOrderTrackingPage}[lbl_order_ID_guest]
