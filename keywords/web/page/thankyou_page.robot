*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/thankyou_page.robot

*** Keywords ***
Verify full tax invoice name is displayed correctly
    [Arguments]    ${tax_invoice_name}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[lbl_tax_customer_name]    $tax_invoice_name=${tax_invoice_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify full tax invoice address is displayed correctly
    [Arguments]    ${tax_invoice_address}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[lbl_tax_address]    $tax_invoice_name=${tax_invoice_address}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Get tax invoice id
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[lbl_tax_id]
    ${tax_id}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[lbl_tax_id]
    [Return]    ${tax_id}

Verify full tax invoice id is displayed correctly
    [Arguments]    ${expected_tax_id}
    ${actual_tax_id}=    Get tax invoice id
    Should Contain    ${actual_tax_id}    ${expected_tax_id}

Verify delivery details icon form thank you page
    Wait Until Element Contains    ${dictCheckoutPage}[txt_verify_delivery_icon]    ${three_icon_status.delivery_details}

Verify payment icon form thank you page
    Wait Until Element Contains    ${dictCheckoutPage}[txt_verify_payment_icon]    ${three_icon_status.payment}

Verify complete icon form thank you page
    Wait Until Element Contains    ${dictCheckoutPage}[txt_verify_complete_icon]    ${three_icon_status.complete}

Verify bank transfer form thank you page
    Wait Until Element Contains    ${dictCheckoutPage}[txt_verify_bank_transfer]    ${thankyou_page.bank_transfer_details}

Verify three icon status on thank you page
    thankyou_page.Verify delivery details icon form thank you page
    thankyou_page.Verify payment icon form thank you page
    thankyou_page.Verify complete icon form thank you page

Verify order information form thank you page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_verify_order_title]

Verify order date form thank you page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_verify_order_date]

Verify phone number from thank you page
    [Arguments]  ${customer_phone}
    ${elem}=    CommonKeywords.Format Text    ${dictCheckoutPage}[txt_verify_phone_number]    $customer_phone=${customer_phone}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click guest login checkout Thank you page
    common_keywords.Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible        ${dictThankyouPage}[btn_guest_login]
    CommonWebKeywords.Click Element        ${dictThankyouPage}[btn_guest_login]

Login User for checkout Thank you page
    [Arguments]    ${email}    ${password}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_email_login]
    SeleniumLibrary.Input text    ${dictThankyouPage}[txt_email_login]    ${email}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_password_login]
    SeleniumLibrary.Input text    ${dictThankyouPage}[txt_password_login]    ${password}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[btn_login_user_Thankyou_Page]
    CommonWebKeywords.Click Element    ${dictThankyouPage}[btn_login_user_Thankyou_Page]
    common_keywords.Wait Until Page Is Completely Loaded

Verify ordered successfully messenger
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_ordered_successfully]

Verify print this order details messenger
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_print_this_order_details]

Verify Continue shopping messenger
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_continue_shopping]

Verify thankyou page display standard delivery
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_standard_delivery]

Verify element text is displayed correctly for shipping address
    [Arguments]    ${text}    ${index}=3
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[lbl_shipping_detail]    $shipping_detail=${text}    $index=${index}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify element text is displayed correctly for billing address
    [Arguments]    ${text}    ${index}=2
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[lbl_billing_detail]    $billing_detail=${text}    $index=${index}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Get order ID after completely order
    Wait Until Element Contains    ${dictThankyouPage}[lbl_order_id]    ${order_prefix}
    ${increment_id}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[lbl_order_id]
    Set Test Variable    ${increment_id}
    [Return]    ${increment_id}

Verify subscribe to central newsletters
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_subscribe_to_central_newsletters]

Verify i agree to central terms of service and privacy policy
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_terms_of_service_and_privacy_policy]

Verify check box subscribe to Central newsletters is checked
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[chk_verify_subscribe_newsletters]
    ${value}=    SeleniumLibrary.Get Element Attribute    ${dictThankyouPage}[chk_verify_subscribe_newsletters]    checked
    Should Be True    "${value}"=="true"

Verify check box i agree to central terms of service and privacy policy
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[chk_terms_of_service_and_privacy_policy]
    ${value}=    SeleniumLibrary.Get Element Attribute    ${dictThankyouPage}[chk_terms_of_service_and_privacy_policy]    checked
    Should Be True    "${value}"=="true"

Verify Title as "create new your central account now"
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_title_as_create_new_your_central_account_now]

Verify under title personal details
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_under_title_personal_details]

Verify email personal details
    [Arguments]    ${email}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_email_guest]
    ${value}=    SeleniumLibrary.Get Element Attribute    ${dictThankyouPage}[txt_verify_email_guest]    value
    Should Be Equal    ${value}    ${email}

Input random email personal details for the registration later
    [Tags]    robot:no-dry-run
    ${randomUser}=    FakerLibrary.email
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictThankyouPage}[txt_verify_email_guest]    ${randomUser}

Verify password personal details
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_password_guest]

Input password personal details for the registration
    [Arguments]    ${password}
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    Scroll to bottom page
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictThankyouPage}[txt_verify_password_guest]    ${password} 

Verify register for guest
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[btn_register_for_guest]

Verify shipping details for guest
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_shipping_details]

Verify shipping standard delivery for guest
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_shipping_type]

Verify button register for guest
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[btn_register_for_guest]

Click button register for guest
    CommonWebKeywords.Click Element    ${dictThankyouPage}[btn_register_for_guest]

Verify phone number info from thank you page
    [Arguments]    ${phone_info}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txt_verify_phone_guest_info]    $phone_info=${phone_info}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}   

Verify that information of pick up at store show on complete page correctly
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_msg_email_item_is_ready]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_msg_confirmation_code]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_msg_please_show_id_card]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_msg_hold_item]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_msg_7_days]

Verify pay by credit card from thank you page for guest
    Wait Until Element Contains    ${dictThankyouPage}[txt_verify_credit_card_title]    ${thankyou_page.msg_shipping_details}
    thankyou_page.Verify order date form thank you page
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_credit}
    common_cds_web.Order status from thank you page should be processing
    thankyou_page.Verify phone number from thank you page   ${guest_information_1}[tel]

Verify costomer name for guest
    [Arguments]    ${firstname}    ${lastname}
    ${costomer_name_guest}=    Set Variable    ${firstname}${SPACE}${lastname}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txa_verify_costomer_name_guest]    $costomer_name_guest=${costomer_name_guest}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify pickup location contact
    [Arguments]    ${contact_pickup}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txa_verify_pickup_location_contact]   $contact_pickup=${contact_pickup}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify pickup location address
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txa_verify_pickup_location_address]

Verify payment status
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_payment_status]  

Verify same day delivery
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_same_day_delivery]  

Verify next day delivery
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_next_day_delivery]  

Verify pickup at familymart
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_pickup_familymart]  

Verify pickup location address details
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_skybox_address_info] 
    ${skybox_address_info}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[txt_verify_skybox_address_info] 
    Should Be Equal    ${skybox_address_info}    ${thankyou_page.pickup_at_skybox_address}
    [Return]    ${skybox_address_info}

Verify gift wrapping details
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_gift_options]
    ...    ${dictThankyouPage}[txt_verify_gift_wrapping]    ${dictThankyouPage}[txt_wrapping_free]

Verify payment T1 full redeem type
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_T1_full_redeem_type]

Verify T1 card no
    [Arguments]    ${T1_no}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txt_T1_card_no]    $T1_no=${T1_no}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify payment credit card type
    checkout_page.Payment type from thank you page should be    ${thankyou_page.payment_credit}

Verify payment type
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_payment_type]
    ${ptype}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[txt_payment_type]
    Should not be empty    ${ptype}

Verify customer phone number
    [Arguments]    ${phone_number}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[customer_phone]
    ${phone}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[customer_phone]
    Should not be empty    ${phone}
    Should Be Equal As Strings    ${phone}    ${phone_number}

Verify purchased items title
    Wait Until Page Loader Is Not Visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_purchased_items_title]

Verify purchased items image
    [Arguments]    ${img_info}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txt_images_product]    $img_info=${img_info}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify purchased items quantity
    [Arguments]    ${items_quantity}    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txt_verify_items_quantity]    $items_quantity=${items_quantity}    $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify purchased items gift wrapping
    [Arguments]    ${purchased_items_gift}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txt_verify_purchased_items_gift_wrapping]    $purchased_items_gift=${purchased_items_gift}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify purchased items gift wrapping free
    [Arguments]    ${purchased_items_free_gift}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txt_verify_purchased_items_gift_wrapping_free]    $purchased_items_free_gift=${purchased_items_free_gift}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify purchased items free
    Wait Until Page Loader Is Not Visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_purchased_items_free]

Verify purchased items image free
    [Arguments]    ${img_info_free}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txt_images_product_free]    $img_info_free=${img_info_free}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify purchased items product name
    [Arguments]    ${name_purchased_items}    ${name_info}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txt_name_product_items]    $name_purchased_items=${name_purchased_items}     $name_info=${name_info}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify purchased items free product name
    [Arguments]    ${name_purchased_items_free}    ${name_info_free}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[txt_name_product_free]    $name_purchased_items_free=${name_purchased_items_free}    $name_info_free=${name_info_free}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click continue shopping button
    CommonWebKeywords.Click Element    ${dictThankyouPage}[btn_continue_shopping]
    Wait Until Page Loader Is Not Visible

Get payment type in Thank you page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_payment_type]
    ${payment_type}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[txt_payment_type]
    [Return]    ${payment_type}

Get payment status in Thank you page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_payment_status]
    ${payment_status}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[txt_payment_status]
    [Return]    ${payment_status}

Get payment type in Thank you page with new design
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[lbl_payment_type]
    ${payment_type}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[lbl_payment_type]
    [Return]    ${payment_type}

Get payment status in Thank you page with new design
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[lbl_payment_status]
    ${payment_status}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[lbl_payment_status]
    [Return]    ${payment_status}

Verify the succecssfull registration message
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[lbl_register_success_msg]

Verify Click and Collect section
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[form_click_and_collect_status]

Verify Click and Collect location
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[form_click_and_collect_location]

Verify top message
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[top_message]
    ${top_message}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[top_message]
    Should not be empty    ${top_message}
    Should Contain Any    ${top_message}    ${payment_slip.heading_message_line_1}
    Should Contain Any    ${top_message}    ${payment_slip.heading_message_line_2}

Verify payment code
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[payment_code]
    ${code}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[payment_code]
    Should not be empty    ${code}

Verify money amount
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[money_amount]
    ${amount}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[money_amount]
    Should not be empty    ${amount}

Verify pay before
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[pay_before]
    ${paybefore}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[pay_before]
    Should not be empty    ${paybefore}
    Should Contain    ${paybefore}    ${payment_slip.pay_before}

Verify bar code
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_bar_code]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[img_bar_code]
    ${txt_bar_code}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[txt_bar_code]
    Should not be empty    ${txt_bar_code}
    ${img_bar_code}=    SeleniumLibrary.Get Element Attribute    ${dictThankyouPage}[img_bar_code]    attribute=src
    Should not be empty    ${img_bar_code}

Verify qr code
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_qr_code]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[img_qr_code]
    ${txt_qr_code}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[txt_qr_code]
    Should not be empty    ${txt_qr_code}
    ${img_qr_code}=    SeleniumLibrary.Get Element Attribute    ${dictThankyouPage}[img_qr_code]    attribute=src
    Should not be empty    ${img_qr_code}

Verify re-payment button displayed on thank you page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[btn_repayment]

Click on re-payment button
    CommonWebKeywords.Click Element     ${dictThankyouPage}[btn_repayment]

Verify Email Input Field Disable
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_email_login]
    SeleniumLibrary.Element Should Be Disabled    ${dictThankyouPage}[txt_email_login]

Verify customer information displayed on thank you page with new design
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[lbl_customer_name]    ${dictThankyouPage}[lbl_customer_email]    ${dictThankyouPage}[lbl_customer_phone]
    
Verify Pickup Instruction Should Be Visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_how_to_pickup_instruction_1]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_how_to_pickup_instruction_2]

Verify Stock Availability Notice Should Be Visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_stock_availability_notice]

Verify How To PickUp Title Should Be Visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_how_to_pickup_title]

Verify password input field should display error message
    [Arguments]    ${error_msg_text}
    ${message}=    CommonKeywords.Format Text    ${dictThankyouPage}[error_message_password]    $err_msg=${error_msg_text}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Verify the duplicate email message should be visible
    [Arguments]    ${error_msg_text}
    ${message}=    CommonKeywords.Format Text    ${dictThankyouPage}[error_message_duplicate_email]    $err_msg=${error_msg_text}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Verify account create successfully
    [Arguments]    ${success_msg}
    Verify icon create account successfully should be visible
    Verify create account successfully message should be visible    ${success_msg}

Verify icon create account successfully should be visible 
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[icon_create_account_successful]

Verify create account successfully message should be visible
    [Arguments]    ${success_msg}
    ${message}=    CommonKeywords.Format Text    ${dictThankyouPage}[successful_message_create_account]    $success_msg=${success_msg}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Verify create account section are display correctly
    Verify create an account title should be visible    ${thankyou_page.title_create_account}
    Verify create an account instruction should be visible
    Verify email and password field should be visible
    Verify checkbox privacy policy should be visible

Verify create an account title should be visible
    [Arguments]    ${create_account}
    ${elem}=    CommonKeywords.Format Text    ${dictThankyouPage}[title_create_account]    $create_account=${create_account}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify email and password field should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_email_login]    ${dictThankyouPage}[txt_verify_password_guest]

Verify checkbox privacy policy should be visible
    Scroll To Element    ${dictThankyouPage}[chk_privace_policy]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[chk_privace_policy]

Verify create an account instruction should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[txt_verify_create_account_instruction]    ${dictThankyouPage}[txt_verify_msg_fast_checkout]
    ...    ${dictThankyouPage}[txt_verify_msg_email]    ${dictThankyouPage}[txt_verify_msg_create_wishlist]
    ...    ${dictThankyouPage}[txt_verify_msg_invite]    ${dictThankyouPage}[txt_verify_msg_alert]

Click on continue shopping button
    CommonWebKeywords.Click Element    ${dictThankyouPage}[btn_continue_shopping]
    Wait Until Page Loader Is Not Visible

Verify the order is completed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictCheckoutPage}[txt_payment_success]

Verify order status is display correctly
    [Arguments]    ${order_status}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[lbl_payment_status]    ${order_status}

Verify order number is display correctly by API
    [Arguments]    ${increment_id}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[lbl_order_id]    ${increment_id}

Verify order status is display correctly at thank you page
    [Arguments]    ${order_status}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[lbl_payment_status]    ${order_status}

Verify payment type is displayed correctly at thank you page
    [Arguments]    ${payment_type}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[lbl_payment_type]    ${payment_type}

Verify order numbers are displayed correctly
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[lbl_order_id]    ${dictThankyouPage}[lbl_sub_order_id]

 Verify product process bar are display correctly with split order
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictThankyouPage}[product_process_bar]
    ${len}=    Get Element Count    ${dictThankyouPage}[product_process_bar]
    Should Be True    ${len}==2

Verify product status bar is display correctly with bank transfer
    ${elem1}=    Format Text    ${dictThankyouPage}[icon_checked_bar]    $product_status=${thankyou_page.product_status_bar.payment_status}
    ${elem2}=    Format Text    ${dictThankyouPage}[icon_not_checked_bar]    $product_status=${thankyou_page.product_status_bar.processing_status}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem1}    ${elem2}

Verify product status bar is display correctly with credit cart
    ${elem1}=    Format Text    ${dictThankyouPage}[icon_checked_bar]    $product_status=${thankyou_page.product_status_bar.processing_status}
    ${elem2}=    Format Text    ${dictThankyouPage}[icon_not_checked_bar]    $product_status=${thankyou_page.product_status_bar.ready_status}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem1}    ${elem2}

Verify product status bar is display correctly with split order
    [Arguments]    ${payment_type}
    Run Keyword If    '${payment_type}'=='Bank Transfer / Service Counter'    Verify product status bar is display correctly with bank transfer
    ...    ELSE    Verify product status bar is display correctly with credit cart

Get order ID and sub order ID from thank you page
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictThankyouPage}[lbl_order_id]    ${order_prefix}
    ${increment_id}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[lbl_order_id]
    ${increment_sub_id}=    SeleniumLibrary.Get Text    ${dictThankyouPage}[lbl_sub_order_id]
    Set Test Variable    ${increment_id}
    Set Test Variable    ${increment_sub_id}

Verify the each section are display correctly
    [Arguments]    ${order_status}    ${payment_type}
    Verify order number is displayed correctly
    Verify created date is displayed correctly
    Verify order status is display correctly at thank you page    ${order_status}
    Verify payment type is displayed correctly at thank you page    ${payment_type}
    Verify product status bar is display correctly with split order    ${payment_type}

Verify the each section are display correctly with split order
    [Arguments]    ${order_status}    ${payment_type}
    Verify order numbers are displayed correctly
    Verify created date is displayed correctly
    Verify order status is display correctly at thank you page    ${order_status}
    Verify payment type is displayed correctly at thank you page    ${payment_type}
    Verify product process bar are display correctly with split order
    Verify product status bar is display correctly with split order    ${payment_type}

Verify coupon code display correctly
    [Arguments]    ${coupon_code}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictThankyouPage}[txt_coupon_code]    ${coupon_code}