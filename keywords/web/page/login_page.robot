*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/login_page.robot

*** Keywords ***
Input username
    [Documentation]    Input username to username text box
    [Arguments]    ${username}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${loginpagelist}[txt_username]    ${username}

Input password
    [Documentation]    Input password to password text box
    [Arguments]    ${password}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${loginpagelist}[txt_password]    ${password}

Click login button
    [Documentation]    Click login button
    CommonWebKeywords.Click Element         ${loginpagelist}[btn_login]

Check error label appear
    [Arguments]    ${message}
    [Documentation]    Assert error message on error label
    Verify Web Element Text Should Be Equal    ${loginpagelist}[lbl_error_message]    ${message}

Click checkout as guest button
    CommonWebKeywords.Click Element        ${loginpagelist}[btn_checkout_guest]