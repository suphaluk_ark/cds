*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/product_category_page.robot

*** Keywords ***
Click on a main category name
    [Arguments]  ${name}
    ${category}=    CommonKeywords.Format Text    ${dictProductCategory}[lbl_main_category]    $name=${name}
    CommonWebKeywords.Click Element  ${category}
    Wait Until Page Is Completely Loaded

Move over a main category on menu
    [Arguments]  ${name}
    ${category}=    CommonKeywords.Format Text    ${dictProductCategory}[lbl_main_category]    $name=${name}
    SeleniumLibrary.Mouse Over  ${category}
    Wait Until Page Is Completely Loaded

Click on a sub-category name
    [Arguments]  ${name}
    ${category}=    CommonKeywords.Format Text    ${dictProductCategory}[lbl_sub_category]    $name=${name}
    CommonWebKeywords.Click Element  ${category}
    Wait Until Page Is Completely Loaded

Click on a sub-category on pop-up
    [Arguments]    ${url}
    ${category}=    CommonKeywords.Format Text    ${dictProductCategory}[lbl_sub_category_popup]    $url=${url}
    CommonWebKeywords.Click Element  ${category}
    Wait Until Page Is Completely Loaded

Click on a sub-category on Category filter
    [Arguments]    ${url}
    ${category}=    CommonKeywords.Format Text    ${dictProductCategory}[lbl_sub_category_filter]    $url=${url}
    CommonWebKeywords.Click Element  ${category}
    Wait Until Page Is Completely Loaded

Verify last sub category direct to correct page
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${dictProductCategory}[h1_sub_category]     $text=${text}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
