*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/home_page.robot

*** Keywords ***
Verify flashdeal section should display
    Wait Until Page Is Completely Loaded
    Scroll To Element    ${dictHomePage}[lbl_flashdeal]
    SeleniumLibrary.Wait Until Element Is Visible    ${dictHomePage}[lbl_flashdeal]    ${timeout}

Get list display product sku
    [Arguments]    ${range_of_products}=${5}
    ${list_sku}=    Create List
    FOR    ${index}    IN RANGE    ${1}   ${range_of_products} + 1
        ${dict_product_sku}    home_page.Get display product sku by index    ${index}
        Append To List    ${list_sku}    ${dict_product_sku}
    END
    [Return]    ${list_sku}

Get display product sku by index
    [Arguments]   ${index}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    ${elem}=    CommonKeywords.Format Text    ${dictHomePage}[data-product-sku_by_index]    $index=${index}
    CommonWebKeywords.Scroll To Element    ${elem}
    ${product_displayed_sku}=    SeleniumLibrary.Get Element Attribute    ${elem}    id
    ${product_displayed_sku}=    Remove String    ${product_displayed_sku}    lnk-viewProduct-
    ${elem}=    CommonKeywords.Format Text    ${dictHomePage}[img_data-product-id_by_index]    $index=${index}
    ${product_displayed_id}=    SeleniumLibrary.Get Element Attribute    ${elem}    data-product-id
    Run Keyword And Ignore Error    Enable Testability Automatic Wait
    ${dict_result}    BuiltIn.Create Dictionary
    ...    data-product-sku    ${product_displayed_sku}
    ...    data-product-id    ${product_displayed_id}
    ...    index    ${index}
    [Return]    ${dict_result}