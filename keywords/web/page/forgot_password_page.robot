*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/forgot_password_page.robot

*** Keywords ***
Input E-mail
    [Arguments]    ${email}
    CommonWebKeywords.Click Element    ${dictForgotPasswordPage}[txt_forgot_title]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictForgotPasswordPage}[txt_email_forgot]    ${email}

Click Send E-mail Button
    CommonWebKeywords.Click Element    ${dictForgotPasswordPage}[btn_send_email]

Forgot Password Page Should Be Displayed
    CommonWebKeywords.Verify Web Elements Are Visible     ${dictForgotPasswordPage}[txt_forgot_title]    ${dictForgotPasswordPage}[txt_forgot_subtitle]

Forgot Password - E-mail Field Display Error Message
    [Arguments]    ${err_message}
    SeleniumLibrary.Wait Until Element Is Visible    ${dictForgotPasswordPage}[txt_err_message]
    Verify Web Element Text Should Be Equal    ${dictForgotPasswordPage}[txt_err_message]    ${err_message}
