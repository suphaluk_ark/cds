*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/shop_by_brand_page.robot

*** Keywords ***
Click on brand
    [Arguments]    ${brand_name}
    ${elem}=    CommonKeywords.Format Text    ${dictShopByBrand}[brand_name]    $brand_name=${brand_name.lower()}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Verify product in What's new section is shown correctly
    [Arguments]    ${brand_name}
    ${brand_name}=    Convert To Upper Case    ${brand_name}
    ${elem}=    CommonKeywords.Format Text    ${dictShopByBrand}[lbl_brand_name]    $brand_name=${brand_name}
    Scroll down to fetch data until element is contained in page    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click on shop by brands category
    CommonWebKeywords.Click Element    ${dictShopByBrand}[lbl_shop_by_brands]