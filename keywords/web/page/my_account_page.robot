*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/my_account_page.robot

*** Keywords ***
Re Import My Account Page
    Import Resource    ${CURDIR}/my_account_page.robot

Address book page should be displayed customer default shipping address book
    [Arguments]    ${cust_name}=${shipping_address_th.firstname} ${shipping_address_th.lastname}
    ...    ${cust_phone}=${shipping_address_th.phone}
    ...    ${cust_address}=${shipping_address_th.address}
    ...    ${cust_region}=${shipping_address_th.sub_district}, ${shipping_address_th.district}, ${shipping_address_th.region} ${shipping_address_th.zip_code}
    Customer name on Default shipping address should be displayed   ${cust_name}
    Customer phone on Default shipping address should be displayed    ${cust_phone}
    Customer address on Default shipping address should be displayed    ${cust_address}
    Customer region on Default shipping address should be displayed    ${cust_region}

Address book page should be displayed customer default billing address book
    [Arguments]    ${cust_name}=${billing_address_th.firstname} ${billing_address_th.lastname}
    ...    ${cust_phone}=${billing_address_th.phone}
    ...    ${cust_address}=${billing_address_th.address}${SPACE}
    ...    ${cust_region}=${billing_address_th.sub_district}, ${billing_address_th.district}, ${billing_address_th.region} ${billing_address_th.zip_code}
    Customer name on Default shipping address should be displayed   ${cust_name}
    Customer phone on Default shipping address should be displayed    ${cust_phone}
    Customer address on Default shipping address should be displayed    ${cust_address}
    Customer region on Default shipping address should be displayed    ${cust_region}

Verify default shipping address book should be selected
    [Arguments]    ${username}  ${password}
    common_cds_web.Get customer info with current shipping address from MDC api by customer member  ${username}  ${password}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_default_shipping]    $id=${member_profile}[address_id]
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}
    ${border-color}=  CommonWebKeywords.Get Web Element CSS Property Value  ${message}  border-color
    Should Be Equal As Strings  ${border-color}  ${default_selected_checkbox_color}

Verify default billing address book should be selected
    [Arguments]    ${username}  ${password}
    common_cds_web.Get customer info with current shipping address from MDC api by customer member  ${username}  ${password}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_default_billing]    $id=${member_profile}[address_id]
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}
    ${border-color}=  CommonWebKeywords.Get Web Element CSS Property Value  ${message}  border-color
    Should Be Equal As Strings  ${border-color}  ${default_selected_checkbox_color}

Click on My Account page from menu
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[my_account_page]
    Wait Until Page Is Completely Loaded

Click on my address book page
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[address_book_page]
    Wait Until Page Is Completely Loaded

Click on my order tab
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[order_page]
    Wait Until Page Loader Is Not Visible

Click on the user icon
    SeleniumLibrary.Wait Until Element Is Visible  ${dictMyAccountPage}[img_user_icon]  30
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[img_user_icon]
    Wait Until Page Is Completely Loaded

Click on my profile tab
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[my_profile_page]
    Wait Until Page Is Completely Loaded

Click on wishlist tab
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[wishlist_page]
    Wait Until Page Is Completely Loaded
## my profile page
Firstname field should be displayed customer data
    [Arguments]    ${cust_fname}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[txt_box_firstname]    $cust_fname=${cust_fname}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Click Save Changes button
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[btn_edit]
    Wait Until Page Is Completely Loaded

Reload page
    SeleniumLibrary.Reload Page

Leave Firstname field empty
    [Arguments]    ${cust_fname}
    ${fname}=    CommonKeywords.Format Text    ${dictMyAccountPage}[txt_box_firstname]    $cust_fname=${cust_fname}
    SeleniumLibrary.Input Text  ${fname}  ${EMPTY}
    Wait Until Page Is Completely Loaded

Leave Lastname field empty
    [Arguments]    ${cust_lname}
    ${lname}=    CommonKeywords.Format Text    ${dictMyAccountPage}[txt_box_lastname]    $cust_lname=${cust_lname}
    SeleniumLibrary.Input Text  ${lname}  ${EMPTY}

Leave Phone number field empty
    [Arguments]    ${number}
    ${cust_phone}=    CommonKeywords.Format Text    ${dictMyAccountPage}[txt_box_phone]    $cust_phone=${number}
    SeleniumLibrary.Input Text  ${cust_phone}  ${EMPTY}

Leave Date of birth field empty
    [Arguments]    ${dob}
    ${cust_dob}=    CommonKeywords.Format Text    ${dictMyAccountPage}[txt_box_birthday]    $cust_dob=${dob}
    SeleniumLibrary.Input Text  ${cust_dob}  ${EMPTY}

Input text for Firstname field
    [Arguments]    ${fname}
    SeleniumLibrary.Input Text    ${dictMyAccountPage}[txt_fname]  ${fname}

Input text for Lastname field
    [Arguments]    ${lname}
    SeleniumLibrary.Input Text    ${dictMyAccountPage}[txt_lname]  ${lname}

Input text for Phone number field
    [Arguments]    ${phone}
    SeleniumLibrary.Input Text    ${dictMyAccountPage}[txt_phone]    ${phone}

Input text for Date of birth field
    [Arguments]    ${date}
    SeleniumLibrary.Input Text    ${dictMyAccountPage}[txt_dob]    ${date}

Warning message for Firstname text should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[lbl_first_name_require]

Warning message for Lastname text should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[lbl_last_name_require]

Warning message for Phone number text should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[lbl_phone_require]

Warning message for Date of birth text should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[lbl_dob_require]

Warning Message Phone Should Start With 0 Should Display
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[lbl_phone_err_msg]

Lastname field should be displayed customer data
    [Arguments]    ${cust_lname}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[txt_box_lastname]    $cust_lname=${cust_lname}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Date of birth field should be displayed customer data
    [Arguments]    ${cust_dob}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[txt_box_birthday]    $cust_dob=${cust_dob}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

E-mail field should be displayed customer data and disabled
    [Arguments]    ${cust_email}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[txt_box_email_disabled]    $cust_email=${cust_email}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Phone field should be displayed customer data
    [Arguments]    ${cust_phone}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[txt_box_phone]    $cust_phone=${cust_phone}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Gender radio button should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[radio_gender_female]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[radio_gender_male]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[radio_gender_none]

Check if only one gender is checked
    ${count}=  Get Element Count  ${dictMyAccountPage}[rdo_checked_gender]
    Should Be Equal As Strings    ${count}    1

Language radio button should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[radio_lang_en]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[radio_lang_th]

Edit button should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[btn_edit]

Click edit button on my profile page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[btn_edit]

Account overview page should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[h3_account]

Click Disconnect The 1 Account Button
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[btn_disconnect_t1c]

#### T1C account
Verify connect to The 1 Account button should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[btn_connect_t1]

Disconnect button should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible      ${dictMyAccountPage}[btn_disconnect_t1c]

Click Connect To The 1 Account Button
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[btn_connect_t1]

Login to The 1 Account Dialog should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[Login_t1c_dialog]

Click close button on the The 1 Account Dialog
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[btn_t1c_dialog_close]

Input T1C e-mail account
    [Arguments]    ${e-mail}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictMyAccountPage}[txt_email_t1c]    ${e-mail}

Input T1C password account
    [Arguments]    ${e-mail}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictMyAccountPage}[txt_password_t1c]    ${e-mail}

Click Login to T1C account button on account overview page
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[btn_login_t1c]

Customer T1C Information should be displayed correctly
    [Arguments]    ${t1c_no}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[cust_t1c_no]    $t1c_no=${t1c_no}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[lbl_point_info]

##Address
Verify Add New address button should be displayed on address book page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[bnt_add_new_address]

Click Add New address button on address book page
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[bnt_add_new_address]

Get the number of address is added
    ${more_actions}=    SeleniumLibrary.Get WebElements  ${dictMyAccountPage}[btn_more_action]
    ${length}=    Get Length    ${more_actions}
    [Return]    ${length}

Verify the lastest address name should be displayed correctly
    [Arguments]  ${address_name}
    Scroll To Element  ${dictMyAccountPage}[lbl_lastest_address_name]
    CommonWebKeywords.Verify Web Element Text Should Be Equal  ${dictMyAccountPage}[lbl_lastest_address_name]  ${address_name}

Verify the lastest customer name should be displayed correctly
    [Arguments]  ${customer_name}
    CommonWebKeywords.Verify Web Element Text Should Be Equal  ${dictMyAccountPage}[lbl_lastest_customer_name]  ${customer_name}

Verify the lastest customer phone should be displayed correctly
    [Arguments]  ${phone}
    CommonWebKeywords.Verify Web Element Text Should Be Equal  ${dictMyAccountPage}[lbl_lastest_customer_phone]  ${phone}

Verify the lastest address line 1 should be displayed correctly
    [Arguments]  ${address_line_1}
    CommonWebKeywords.Verify Web Element Text Should Be Equal  ${dictMyAccountPage}[lbl_lastest_address_line1]  ${address_line_1}

Verify the lastest address line 2 should be displayed correctly
    [Arguments]  ${address_line_2}
    CommonWebKeywords.Verify Web Element Text Should Be Equal  ${dictMyAccountPage}[lbl_lastest_address_line2]  ${address_line_2}

Verify address name should be displayed correctly by id
    [Arguments]  ${id}  ${address_name}
    ${elem}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_address_name]    $id=${id}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${elem}    ${address_name}

Verify name should be displayed correctly by id
    [Arguments]    ${id}    ${name}
    ${elem}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_customer_name]    $id=${id}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${elem}    ${name}

Verify phone should be displayed correctly by id
    [Arguments]  ${id}  ${phone}
    ${elem}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_customer_phone]    $id=${id}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${elem}    ${phone}

Verify address line 1 should be displayed correctly by id
    [Arguments]    ${id}    ${address_line_1}
    ${elem}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_address_line1]    $id=${id}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${elem}    ${address_line_1}

Verify address line 2 should be displayed correctly by id
    [Arguments]  ${id}  ${address_line_2}
    ${elem}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_address_line2]    $id=${id}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${elem}    ${address_line_2}

Delete all address on the address book page
    ${more_actions}=    SeleniumLibrary.Get WebElements    ${dictMyAccountPage}[btn_more_action]
    ${deletes}=    SeleniumLibrary.Get WebElements    ${dictMyAccountPage}[btn_delete_all]
    ${length}=    Get Length    ${more_actions}
    FOR    ${INDEX}    IN RANGE    0    ${length}
        CommonWebKeywords.Click Element    ${more_actions}[${INDEX}]
        CommonWebKeywords.Click Element    ${deletes}[${INDEX}]
        SeleniumLibrary.Handle Alert    action=ACCEPT    timeout=3
    END

Delete all addresses if existed
    ${count}=    Get the number of address is added
    Run Keyword If  ${count}>0    Delete all address on the address book page

Delete address except the default address
    ${more_actions}=    SeleniumLibrary.Get WebElements    ${dictMyAccountPage}[btn_more_action]
    ${css_web}=    SeleniumLibrary.Get WebElements    ${dictMyAccountPage}[rdo_default_address]
    ${deletes}=    SeleniumLibrary.Get WebElements    ${dictMyAccountPage}[btn_delete_all]
    Run Keyword And Ignore Error    home_page_web_keywords.Close Popup
    ${length}=    Get Length    ${more_actions}
    FOR    ${INDEX}    IN RANGE    ${length}
        CommonWebKeywords.Click Element    ${more_actions}[${INDEX}]
        ${value}=    Get Web Element CSS Property Value    ${css_web}[${INDEX}]    border-bottom-color
        Run Keyword If    '${value}'=='rgba(102, 102, 102, 1)'     Run Keywords     CommonWebKeywords.Click Element    ${deletes}[${INDEX}]
        ...    AND    SeleniumLibrary.Handle Alert    action=ACCEPT    timeout=3
    END

Verify system displays warning message under the field
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[msg_firstname_require]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[msg_lastname_require]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[msg_telephone_require]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[msg_address_line_require]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[msg_postcode_require]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[msg_region_require]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[msg_district_require]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[msg_subdistrict_require]

Click and input text for address name on address book page
    [Arguments]  ${address_name}
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[txt_address_name]
    SeleniumLibrary.Input Text  ${dictMyAccountPage}[txt_address_name]  ${address_name}

Click and input text for first name on address book page
    [Arguments]  ${first_name}
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[txt_address_firstname]
    SeleniumLibrary.Input Text  ${dictMyAccountPage}[txt_address_firstname]  ${first_name}

Click and input text for last name on address book page
    [Arguments]  ${last_name}
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[txt_address_lastname]
    SeleniumLibrary.Input Text  ${dictMyAccountPage}[txt_address_lastname]  ${last_name}

Click and input text for telephone number on address book page
    [Arguments]    ${phone_number}
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[txt_address_telephone]
    SeleniumLibrary.Input Text  ${dictMyAccountPage}[txt_address_telephone]    ${phone_number}

Click and input text for house number and street on address book page
    [Arguments]    ${house_no}
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[txt_address_line]
    SeleniumLibrary.Input Text  ${dictMyAccountPage}[txt_address_line]    ${house_no}

Click and input text for postcode on address book page
    [Arguments]    ${postcode}
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[txt_address_postcode]
    SeleniumLibrary.Input Text  ${dictMyAccountPage}[txt_address_postcode]    ${postcode}
    Wait Until Page Is Completely Loaded

Select a province from the drop-down list
    [Arguments]    ${province}
    CommonWebKeywords.Select dropdownlist by label    ${dictMyAccountPage}[ddl_address_region]    ${province}

Select a district from the drop-down list
    [Arguments]    ${district}
    CommonWebKeywords.Select dropdownlist by label    ${dictMyAccountPage}[ddl_address_district]    ${district}

Select a sub-district from the drop-down list
    [Arguments]    ${sub_district}
    CommonWebKeywords.Select dropdownlist by label    ${dictMyAccountPage}[ddl_address_subdistrict]    ${sub_district}

Get a random address and click edit on it
    ${length}=  Get the number of address is added
    ${random} =    Evaluate    random.randint(1, ${length})    modules=random
    ${random_more}=    CommonKeywords.Format Text    ${dictMyAccountPage}[btn_random_more]    $random=${random}
    ${random_edit}=    CommonKeywords.Format Text    ${dictMyAccountPage}[btn_random_edit]    $random=${random}
    CommonWebKeywords.Scroll To Element    ${random_more}
    CommonWebKeywords.Click Element  ${random_more}
    CommonWebKeywords.Click Element  ${random_edit}

Delete an address by id
    [Arguments]  ${id}
    ${elem_delete}=    CommonKeywords.Format Text    ${dictMyAccountPage}[btn_delete]    $id=${id}
    ${elem_more}=    CommonKeywords.Format Text    ${dictMyAccountPage}[btn_more]    $id=${id}
    CommonWebKeywords.Scroll To Element  ${elem_more}
    CommonWebKeywords.Click Element  ${elem_more}
    CommonWebKeywords.Click Element  ${elem_delete} 
    Handle Alert  action=ACCEPT  timeout=3

Verify an adress should be not displayed by id
    [Arguments]  ${id}
    ${elem}=    CommonKeywords.Format Text    ${dictMyAccountPage}[btn_delete]    $id=${id}
    CommonWebKeywords.Verify Web Elements Are Not Visible  ${elem}

Get a random address id from the address list
    ${length}=  Get the number of address is added
    ${random} =    Evaluate    random.randint(1, ${length})    modules=random
    ${random_delete}=    CommonKeywords.Format Text    ${dictMyAccountPage}[btn_random_address]    $random=${random}
    ${url}=    SeleniumLibrary.Get Element Attribute  ${random_delete}  to
    ${random_id}=  Get regexp matches  ${url}   \\d+
    [Return]  ${random_id}[0]

Get the current address id from url
    ${url}=  Get Location
    ${current_id}=  Get regexp matches  ${url}   \\d+
    [Return]  ${current_id}[0]

Enable add tax invoice
    CommonWebKeywords.Click Element  ${dictMyAccountPage}[btn_add_tax_invoice]

Click and input text for card id
    [Arguments]  ${vat}
    CommonWebKeywords.Click Element  ${dictMyAccountPage}[txt_cardId]
    SeleniumLibrary.Input Text  ${dictMyAccountPage}[txt_cardId]  ${vat}

Click and input text for VAT
    [Arguments]  ${vat}
    CommonWebKeywords.Click Element  ${dictMyAccountPage}[txt_vat]
    SeleniumLibrary.Input Text  ${dictMyAccountPage}[txt_vat]  ${vat}

Verify shipping address is checked
    SeleniumLibrary.Get Text    (//span[text()='${overview_page.default_shipping_address}']/preceding-sibling::span/div)[last()]

Check default shipping option
    CommonWebKeywords.Click Element  ${dictMyAccountPage}[chk_default_shipping]

Check default billing option
    CommonWebKeywords.Click Element  ${dictMyAccountPage}[chk_default_billing]

Click SAVE CHANGES button on the Address Book page
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[btn_address_save]
    Wait Until Page Is Completely Loaded

Customer name on Default shipping address should be displayed
    [Arguments]    ${cust_name}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_name_address]    $cust_name=${cust_name}
    CommonWebKeywords.Scroll To Element    ${message}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Customer phone on Default shipping address should be displayed
    [Arguments]    ${cust_phone}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_phone_address]    $cust_phone=${cust_phone}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Customer address on Default shipping address should be displayed
    [Arguments]    ${cust_address}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_default_address]    $cust_address=${cust_address}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Customer region on Default shipping address should be displayed
    [Arguments]    ${cust_region}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_region_address]    $cust_region=${cust_region}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Warning Message Shipping Address Phone Should Start With 0 Should Display
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[btn_address_save]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[lbl_address_phone_err_msg]

Warning Message Shipping Address Phone Should Start With 0 Should Display For Mobile
    CommonWebKeywords.Click Element    ${dictMyAccountPage}[btn_address_save]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictMyAccountPage}[lbl_address_phone_err_msg]

## Tax
Customer name on Full tax invoice address should be displayed
    [Arguments]    ${cust_name}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_name_tax_address]    $cust_name=${cust_name}
    CommonWebKeywords.Scroll To Element    ${message}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Customer phone on Full tax invoice address should be displayed
    [Arguments]    ${cust_phone}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_phone_tax_address]    $cust_phone=${cust_phone}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Customer address on Full tax invoice address should be displayed
    [Arguments]    ${cust_address}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_default_tax_address]    $cust_address=${cust_address}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Customer region on Full tax invoice address should be displayed
    [Arguments]    ${cust_region}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_region_tax_address]    $cust_region=${cust_region}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

## Overview - Information
Customer name should be displayed in personal information
    [Arguments]    ${cust_name}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_cust_name]    $name=${cust_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Customer e-mail should be displayed in personal information
    [Arguments]    ${cust_email}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_cust_email]    $email=${cust_email}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Customer phone no should be displayed in personal information
    [Arguments]    ${cust_phone}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_cust_phone]    $phone=${cust_phone}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Default shipping address label should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictMyAccountPage}[lbl_overview_default_shipping_address]

Full tax invoice address label should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictMyAccountPage}[lbl_overview_full_tax_invoice_address]

Lasted order label should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${dictMyAccountPage}[lbl_overview_lastest_order]

## Lasted Order
Scroll to lastest orders section
    CommonWebKeywords.Scroll To Element    ${dictMyAccountPage}[lbl_lastest_order]

Order number of lastest order should be displayed
    [Arguments]    ${order_no}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_order_number]    $order_no=${order_no}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Order status of lastest order should be displayed
    [Arguments]    ${order_status}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_order_status]    $order_status=${order_status}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Delivery option of lastest order should be displayed
    [Arguments]    ${delivery_option}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_delivery_option]    $delivery_status=${delivery_option}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Payment type of lastest order should be displayed
    [Arguments]    ${payment_type}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_payment]    $payment_type=${payment_type}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}

Total price of lastest order should be displayed
    [Arguments]    ${total_price}
    ${message}=    CommonKeywords.Format Text    ${dictMyAccountPage}[lbl_price]    $total_price=${total_price}
    CommonWebKeywords.Verify Web Elements Are Visible    ${message}