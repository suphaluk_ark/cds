*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/header.robot

*** Keywords ***
Click my account menu under my account dropdown
    CommonWebKeywords.Click Element     ${btn_my_account}

Click track order button
    CommonWebKeywords.Click Element     ${btn_track_order}
    Wait Until Page Loader Is Not Visible

Click order tracking button
    CommonWebKeywords.Click Element     ${btn_order_tracking}

Input E-mail into tracking pop-up
    [Arguments]     ${e-mail}
    CommonWebKeywords.Input Text And Verify Input For Web Element     ${lbl_email_tracking}    ${e-mail}

Input order number into tracking pop-up
    [Arguments]     ${order_no}
    CommonWebKeywords.Input Text And Verify Input For Web Element     ${lbl_order_tracking}    ${order_no}

Click my orders button
    CommonWebKeywords.Click Element     ${btn_my_orders}

Click my account button
    CommonWebKeywords.Click Element     ${btn_account}

Remove all product in shopping cart
    ${status}=    Run Keyword And Return Status     CommonWebKeywords.Verify Web Elements Are Visible    ${txt_count_mini_cart}
    Return From Keyword If    '${status}' == '${FALSE}'
    Header Web - Click Mini Cart Button
    CommonWebKeywords.Verify Web Elements Are Visible    ${btn_delete_product}
    ${elems}=   SeleniumLibrary.Get WebElements     ${btn_delete_product}
    ${len}=     Get Length  ${elems}
    FOR   ${index}    IN RANGE    ${len}
        Click delete product in mini cart button
        CommonWebKeywords.Verify Web Elements Are Not Visible    ${loading_mini_cart}
    END
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${btn_delete_product}
    Header Web - Click Mini Cart Button

Click delete product in mini cart button
    CommonWebKeywords.Click Element     ${btn_delete_product}

Product should be displayed in mini cart
    [Arguments]     ${sku_number}   ${product_name}
    ${elem}=    CommonKeywords.Format Text  ${product_mini_cart}    $skunumber=${sku_number}    $product_name=${product_name}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}

Click view cart button should not be visible
    Seleniumlibrary.Wait Until Element Is Not Visible   ${btn_view_cart}

Header Web - Click View Cart Button
    CommonWebKeywords.Click Element     ${btn_view_cart}

Header Web - Click Mini Cart Button
    CommonWebKeywords.Click Element     ${btn_mini_cart}

Header Web - Click Register Button
    CommonWebKeywords.Click Element     ${btn_register}

Header Web - Click Login on Header
    CommonWebKeywords.Click Element    ${btn_login}

Header Web - Input Email on Header
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${txt_email}    ${email}

Header Web - Click Logout on Header
    CommonWebKeywords.Click Element    ${btn_logout}
    Wait Until Page Is Completely Loaded
    CommonWebKeywords.Verify Web Elements Are Visible    ${btn_login}

Header Web - Input Password on Header
    [Arguments]    ${password}
    CommonWebKeywords.Input Text And Verify Input For Web Element        ${txt_password}    ${password}

Header Web - Click Submit Button on Header
    CommonWebKeywords.Click Element    ${btn_submit}

Header Web - Submit Button Not Visible
    Wait Until Element Is Not Visible    ${btn_submit}    30s

Header Web - Click Facebook Button on Header
    CommonWebKeywords.Click Element    ${btn_facebook}

Header Web - Click Forgot Password on Header
    CommonWebKeywords.Click Element    ${txt_forgot_password}

Header Web - Click Accont Button on Header
    CommonWebKeywords.Click Element    ${btn_account}
    Wait Until Page Is Completely Loaded

Header Web - Verify Display Name Is Visible
    [Arguments]    ${expect_displayname}
    ${elem}=    CommonKeywords.Format Text  ${lbl_account_name}    $account_name=${expect_displayname}
    SeleniumLibrary.Wait Until Element Is Visible     ${elem}    15s

Header Web - Email Field Display Error Message
    [Arguments]     ${expect_message}
    #username field index = 1, password field index =2
    Verify Web Element Text Should Be Equal    ${err_required_field_1}    ${expect_message}

Hearder Web - Email should be displayed error message
    [Arguments]     ${expect_message}
    ${elem}=    CommonKeywords.Format Text  ${msg_error_email}    $error_msg=${expect_message}
    CommonWebKeywords.Verify Web Elements Are Visible  ${elem}

Header Web - Password Field Display Error Message
    [Arguments]     ${expect_message}
    #username field index = 1, password field index =2
    Verify Web Element Text Should Be Equal    ${err_required_field_2}    ${expect_message}

Header Web - Verify Error Message Afer Login Failed
    [Arguments]    ${expect_message}
    Verify Web Element Text Should Be Equal    ${err_login_failed}    ${expect_message}

Header Web - Email Field Not Visible Error Message
    #username field index = 1, password field index =2
    Wait Until Element Is Not Visible    ${err_required_field_1}    15s

Header Web - Password Field Not Visible Error Message
    #username field index = 1, password field index =2
    Wait Until Element Is Not Visible    ${err_required_field_2}    15s

Header Web - Verify Error Message Not Visible
    Wait Until Element Is Not Visible    ${err_login_failed}    15s

Header Web - Verify Running Text
    [Arguments]    ${expect_text}
    ${length}=    Get Length    ${expect_text}
    SeleniumLibrary.Wait Until Element Is Visible    ${span_running_text}[${length}]    15s
    :FOR    ${INDEX}    IN RANGE    0    ${length}
      \    ${index_locator}=    Evaluate    ${INDEX}+1
      \    ${actual_message}=    SeleniumLibrary.Get Text    ${span_running_text}[${index_locator}]
      \    Should Be Equal    ${expect_text[${INDEX}]}    ${actual_message}

Verify display name with welcome message should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible  ${lbl_welcome}

Is Logged in user
    ${isDisplay}=    Run Keyword And Return Status    SeleniumLibrary.Wait Until Element Is Visible   ${btn_account_name}
    [Return]    ${isDisplay}

Verify pop-up should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible  ${lbl_login_popup}

# Header Web - Verify Session
#      $x("//span[text()='ตอบโจทย์ทุกการช้อปที่เซ็นทรัล']")

# $x("//div[@class='why-us—section-wrapping']")
# $x("//div[@class='trendy-cards']")

Click English flag icon 
    CommonWebKeywords.Click Element  ${english_flag_icon}

Click Thai flag icon
    CommonWebKeywords.Click Element  ${thai_flag_icon}

Switch to English language
    Scroll To Element    ${ddl_switch_language}
    CommonWebKeywords.Click Element  ${ddl_switch_language}
    header_web_keywords.Click English flag icon

Switch to Thai language
    CommonWebKeywords.Click Element  ${ddl_switch_language}
    header_web_keywords.Click Thai flag icon

## MINI CART ##
Verify view cart button should be displayed correctly as a guest
    CommonWebKeywords.Verify Web Elements Are Visible  ${lbl_view_cart_guest}

Verify view cart button should be displayed correctly when member login
    CommonWebKeywords.Verify Web Elements Are Visible  ${lbl_view_cart_member}

Get grand price total on mini cart
    ${text}=  SeleniumLibrary.Get Text  ${lbl_grand_total}
    ${price}=  CommonKeywords.Convert price to number format  ${text}
    [Return]  ${price}

Verify message if add item has total price less than 699 THB on mini cart
    ${grand_total}=  Get grand price total on mini cart
    ${extra_number}=  Evaluate  699-${grand_total}
    ${buy_more_msg}=    CommonKeywords.Format Text    ${mini_cart.delivery_message.non_free_shipping}    $number=${extra_number}
    ${msg}=  SeleniumLibrary.Get Text  ${msg_deliver}
    Should Be Equal As Strings  ${buy_more_msg}  ${msg}

Verify FREE standard delivery message
    CommonWebKeywords.Verify Web Elements Are Visible    ${msg_deliver}
    ${msg}=    SeleniumLibrary.Get Text    ${msg_deliver}
    BuiltIn.Run Keyword If    '${TEST_PLATFORM}' == 'web'    Should Be Equal As Strings    ${mini_cart.delivery_message.free_shipping}    ${msg}    ELSE    Should Be Equal As Strings    ${checkout_page.standard_delivery}    ${msg}    

Verify product quantity should be displayed correctly on mini cart
    [Arguments]  ${product_sku}  ${quantity}
    ${elem}=    CommonKeywords.Format Text    ${lbl_quantity}    $product_sku=${product_sku}
    ${number}=  SeleniumLibrary.Get Text  ${elem}
    Should Be Equal As Strings  ${number}  ${quantity}