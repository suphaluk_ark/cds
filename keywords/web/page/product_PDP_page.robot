*** Setting ***
Resource    ${CURDIR}/../web_imports.robot
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/product_PDP_page.robot

*** Keywords ***
Verify view pickup location link display correctly 
    CommonWebKeywords.Click Element Using Javascript    ${dictproductPDP}[lnk_pickup_location]

Verify close button display correctly
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[btn_close_button]

Verify product details on product PDP page
    [Arguments]    ${product_details}
    SeleniumLibrary.Wait Until Element Is Visible    ${dictproductPDP}[text_PDP_details]
    SeleniumLibrary.Page Should Contain    ${product_details}

Verify product name on product PDP page
    [Arguments]    ${product_sku}
    ${locator_product_name}=    Format Text  ${dictproductPDP}[text_PDP_product_name]  product_sku=${product_sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${locator_product_name}
    ${actual_product_name}=    SeleniumLibrary.Get Text    ${locator_product_name}
    Should Be Equal As Strings    ${actual_product_name}    ${${product_sku}.name}

Verify redeem point text display on product PDP page
    [Arguments]    ${product_sku}    ${expected_redeem_point}
    ${locator_product_redeem}=    Format Text  ${dictproductPDP}[text_PDP_redeem_point]  product_sku=${product_sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${locator_product_redeem}
    ${actual_redeem_point}=    SeleniumLibrary.Get Text    ${locator_product_redeem}
    ${actual_redeem_point}=    CommonKeywords.Convert price to number format    ${actual_redeem_point}
    Should Be Equal As Integers    ${actual_redeem_point}    ${expected_redeem_point}

Verify social icon facebook on product PDP page
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[icon_PDP_facebook]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[icon_PDP_facebook]

Verify social icon twitter on product PDP page
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[icon_PDP_twitter]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[icon_PDP_twitter]

Verify social icon email on product PDP page
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[icon_PDP_email]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[icon_PDP_email]

Verify social icon line on product PDP page
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[icon_PDP_line]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[icon_PDP_line]

Verify shipping type on product PDP page
    [Arguments]    ${product_shipping_type}
    ${locator_product_shipping_type}=    Format Text  ${dictproductPDP}[text_PDP_shipping_type]  shipping_type=${product_shipping_type}
    SeleniumLibrary.Wait Until Element Is Visible     ${locator_product_shipping_type}

Verify payment type on product PDP page
    [Arguments]    ${product_payment_type}
    ${locator_product_payment_type}=    Format Text  ${dictproductPDP}[text_PDP_payment_type]  payment_type=${product_payment_type}
    SeleniumLibrary.Wait Until Element Is Visible     ${locator_product_payment_type}
    
Verify brand name on product PDP page
    [Arguments]    ${expected_brand_name}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[text_brand_name]    ${expected_brand_name}

Verify add to bag button on product PDP page
    [Arguments]    ${add_button_PDP}
    ${locator_add_button_PDP}=    Format Text  ${dictproductPDP}[button_add_to_bag]  product_sku=${add_button_PDP}
    SeleniumLibrary.Wait Until Element Is Visible    ${locator_add_button_PDP}

Verify and click add to bag button on product PDP page
    [Arguments]    ${add_button_PDP}
    ${locator_add_button_PDP}=    Format Text  ${dictproductPDP}[button_add_to_bag]  product_sku=${add_button_PDP}
    CommonWebKeywords.Click Element  ${locator_add_button_PDP}

Verify product picture display on product PDP page
    [Arguments]    ${product_sku}
    ${locator_product_picture}=    Format Text  ${dictproductPDP}[picture_product_pdp]  picture_name=${${product_sku}.name}
    SeleniumLibrary.Wait Until Element Is Visible     ${locator_product_picture}

Verify product relate display on slider
    [Arguments]    ${relate_product}
    ${new_locator}=      Format Text  ${dictproductPDP}[text_PDP_product_relate]  product=${relate_product}
    SeleniumLibrary.Wait Until Element Is Visible    ${new_locator}

Verify product relate secound page display on slider
    [Arguments]    ${relate_product}
    ${new_locator}=      Format Text  ${dictproductPDP}[text_PDP_product_relate_second_page]  product=${relate_product}
    SeleniumLibrary.Wait Until Element Is Visible    ${new_locator}

Verify promotion text display on product PDP page
    [Arguments]    ${product_promotion_text}
    ${locator_product_promotion_text}=    Format Text  ${dictproductPDP}[text_promotion_text]  product_sku=${product_promotion_text}
    SeleniumLibrary.Wait Until Element Is Visible    ${locator_product_promotion_text}

Verify full price on product PDP page
    [Arguments]    ${product_sku}    ${full_price}
    ${locator_full_price}=    Format Text  ${dictproductPDP}[text_full_price]  product_sku=${product_sku}
    ${get_full_price}=    SeleniumLibrary.Get Text    ${locator_full_price}
    ${get_full_price}=    CommonKeywords.Convert price to number format    ${get_full_price}
    Should Be Equal As Numbers    ${get_full_price}    ${full_price}

Verify discount price on product PDP page
    [Arguments]    ${product_sku}    ${discount_price}
    ${locator_discount_price}=    Format Text  ${dictproductPDP}[text_discount_price]  product_sku=${product_sku}
    ${get_discount_price}=    SeleniumLibrary.Get Text    ${locator_discount_price}
    ${get_discount_price}=    CommonKeywords.Convert price to number format    ${get_discount_price}
    Should Be Equal As Numbers    ${get_discount_price}    ${discount_price}

Verify redeem point on product PDP page
    [Arguments]    ${product_sku}    ${redeem_point}
    ${locator_redeem_point}=    Format Text  ${dictproductPDP}[text_PDP_redeem_point]  product_sku=${product_sku}
    ${get_redeem_point}=    SeleniumLibrary.Get Text    ${locator_redeem_point}
    ${get_redeem_point}=    CommonKeywords.Convert price to number format    ${get_redeem_point}
    Should Be Equal As Numbers    ${get_redeem_point}    ${redeem_point}

Verify save price percent on product PDP page
    [Arguments]    ${save_price_percent}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_product_save_price_percent]
    ${percent_discount_ui}=    SeleniumLibrary.Get Text    ${elem}
    BuiltIn.Should Be Equal As Strings    ${percent_discount_ui}    ${save_price_percent}%

Verify Product Preview Discount Should Display
    [Arguments]    ${product_sku}    ${saving_price}
    ${locator}=    CommonKeywords.Format Text    ${dictProductPreview}[lbl_saving_price]    product_sku=${product_sku}    saving_price=${saving_price}
    CommonWebKeywords.Verify Web Elements Are Visible    ${locator}

Verify Add To Bag Button Should Not Display
    [Arguments]    ${sku}
    ${locator}=    Format Text   ${dictproductPDP}[button_add_to_bag]    product_sku=${sku}
    SeleniumLibrary.Wait Until Page Does Not Contain Element    ${locator}

Verify Out Of Stock Message Should Display
    SeleniumLibrary.Wait Until Element Is Visible    ${dictproductPDP}[lbl_out_of_stock]

Go to pdp by product type url and return sku
    [Arguments]    ${product_type}
    Run Keyword If    '${product_type}'=='${plp_page_section_scope.configurable_product}'     common_keywords.Go To Direct Url    ${configurable_product.url_contain_delivery}
    ...    ELSE    common_keywords.Go To Direct Url    ${simple_product.url}
    ${product_sku}=    Run Keyword If    '${product_type}'=='${plp_page_section_scope.configurable_product}'    Set Variable    ${configurable_product.sku}
    ...    ELSE    Set Variable    ${simple_product.sku}
    [Return]    ${product_sku}

Go to section on pdp
    [Arguments]    ${skus}    ${section}
    Run Keyword If    '${section}'=='${plp_page_section_scope.recently_viewed}'
    ...    Initialize recently viewed section on pdp
    ${sku}=    Evaluate    random.choice(${skus})    random
    common_keywords.Go To PDP Directly By Product Sku    ${sku}
    Scroll to text section on PDP    ${section}

Scroll to text section on PDP
    [Arguments]    ${section}
    ${text_section}=    Run Keyword If    '${section}'=='${plp_page_section_scope.recently_viewed}'    Set Variable    ${product_detail_page.preview_section}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.you_may_like}'    Set Variable    ${product_detail_page.you_may_like}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.similar_product}'    Set Variable    ${product_detail_page.similar_section}
    ...    ELSE IF    '${section}'=='${plp_page_section_scope.complete_the_look}'    Set Variable    ${product_detail_page.complete_the_look}
    common_keywords.Scroll To Text Section    ${text_section}

Initialize recently viewed section on pdp
    FOR    ${index}    IN RANGE    len(${product_sku_list})
        Exit For Loop If    ${index}==5
        Go to PDP Directly By Product Sku    ${product_sku_list}[${index}]
    END

Postcode field and apply button should be visible
    ${status}=    Run Keyword And Return Status    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[txt_postcode]    ${dictproductPDP}[btn_postcode_apply]
    Run Keyword If    '${status}'=='${FALSE}'    Fail    Please check sku with available delivery options again.
    SeleniumLibrary.Element Should Be Enabled    ${dictproductPDP}[btn_postcode_apply]

Use my current location field should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lnk_use_current_location]

'Get estimated dates by postcode' link should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lnk_get_estimated_dates]

The invalid postcode message should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lbl_invalid_postcode]

The postcode textfield allows maximum 5 characters
    common_keywords.Validate Length Of Characters Entered Into Textbox    ${dictproductPDP}[txt_postcode]    5

Click 'Get estimated dates by postcode' link
    CommonWebKeywords.Click Element    ${dictproductPDP}[lnk_get_estimated_dates]

Click postcode apply button
    CommonWebKeywords.Click Element Using Javascript    ${dictproductPDP}[btn_postcode_apply]

Enter to apply postcode value
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[txt_postcode]
    SeleniumLibrary.Press Keys    ${dictproductPDP}[txt_postcode]    RETURN

Input value into postcode textbox
    [Arguments]    ${text}
    CommonWebKeywords.Scroll And Click Element    ${dictproductPDP}[txt_postcode]
    SeleniumLibrary.Input Text     ${dictproductPDP}[txt_postcode]    ${text}

Verify the postcode will be saved during the customer session
    ${entered_text}=    SeleniumLibrary.Get Value    ${dictproductPDP}[txt_postcode]
    common_keywords.Go To PDP Directly By Product Sku    ${product_sku_list}[0]    ${FALSE}
    Verify product name on product PDP page    ${sku}
    ${text_content}=    SeleniumLibrary.Get Value    ${dictproductPDP}[txt_postcode]
    Should Be Equal As Strings    ${entered_text}    ${text_content}

Verify the postcode will be not saved when customer session end
    ${entered_text}=    SeleniumLibrary.Get Value    ${dictproductPDP}[txt_postcode]
    common_keywords.Go To PDP Directly By Product Sku    ${product_sku_list}[1]
    ${text_content}=    SeleniumLibrary.Get Value    ${dictproductPDP}[txt_postcode]
    Should Not Be Equal As Strings    ${entered_text}    ${text_content}

Verify product id display on product PDP page
    BuiltIn.Run Keyword If   '${TEST_PLATFORM}' == 'mobile_web'    Run Keywords    SeleniumLibrary.Wait Until Element Is Not Visible    ${dictproductPDP}[lbl_product_id]    AND    BuiltIn.Log    Product ID Not Show    ELSE    SeleniumLibrary.Wait Until Element Is Visible    ${dictproductPDP}[lbl_product_id]

Verify slide image product
    SeleniumLibrary.Wait Until Element Is Visible    ${dictproductPDP}[icon_arrow_left]
    SeleniumLibrary.Click Element    ${dictproductPDP}[icon_arrow_left]
    SeleniumLibrary.Wait Until Element Is Visible    ${dictproductPDP}[icon_arrow_right]
    SeleniumLibrary.Click Element    ${dictproductPDP}[icon_arrow_right]

Available delivery options should be visible
    [Arguments]    ${delivery_option_list}
    Run Keyword If    '${filters.delivery_methods.delivery}' in ${delivery_option_list}    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lbl_home_lead_time]    ${dictproductPDP}[lbl_home_free_lead_time]
    Run Keyword If    '${filters.delivery_methods.click_and_collect}' in ${delivery_option_list}    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lbl_clickncollect_lead_time]    ${dictproductPDP}[lbl_clickncollect_free_lead_time]
    Run Keyword If    '${filters.delivery_methods.two_hours_pick_up}' in ${delivery_option_list}    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lbl_2hour_lead_time]    ${dictproductPDP}[lbl_2hour_free_lead_time]

Verify available delivery options by default lead time label
    [Arguments]    ${product_sku}
    ${response}=    product.Get delivery methods response by sku and postcode    ${store_view.name.en}    ${product_sku}    0
    @{delivery_methods}=    Get Value From Json    ${response}    $..delivery_method
    @{lead_times}=    Get Value From Json    ${response}    $..delivery_method_lead_times_label
    @{frees}=    Get Value From Json    ${response}    $..delivery_method_free_label
    Return From Keyword If    len(${delivery_methods})==0    ${FALSE}
    Available delivery options should be visible    ${delivery_methods}
    FOR    ${index}    IN RANGE    len(${delivery_methods})
        Run Keyword If    '${delivery_methods}[${index}]'=='${filters.delivery_methods.delivery}'    Home delivery option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
        ...    ELSE IF    '${delivery_methods}[${index}]'=='${filters.delivery_methods.express_delivery}'    Express delivery option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
        ...    ELSE IF    '${delivery_methods}[${index}]'=='${filters.delivery_methods.click_and_collect}'    Click n collect option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
        ...    ELSE IF    '${delivery_methods}[${index}]'=='${filters.delivery_methods.two_hours_pick_up}'    Pickup 2 hour option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
        ...    ELSE IF    '${delivery_methods}[${index}]'=='${filters.delivery_methods.three_hours_delivery}'    3 hour delivery option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
    END

Verify available delivery options by postcode value
    [Arguments]    ${product_sku}    ${post_code}
    ${post_code}=    Evaluate    str(${post_code})
    ${response}=    product.Get delivery methods response by sku and postcode    ${store_view.name.en}    ${product_sku}    ${post_code}
    @{delivery_methods}=    Get Value From Json    ${response}    $..delivery_method
    @{lead_times}=    Get Value From Json    ${response}    $..delivery_method_lead_times_label
    @{frees}=    Get Value From Json    ${response}    $..delivery_method_free_label
    Return From Keyword If    len(${delivery_methods})==0    ${FALSE}
    FOR    ${index}    IN RANGE    len(${delivery_methods})
        Run Keyword If    '${delivery_methods}[${index}]'=='${filters.delivery_methods.delivery}'    Home delivery option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
        ...    ELSE IF    '${delivery_methods}[${index}]'=='${filters.delivery_methods.express_delivery}'    Express delivery option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
        ...    ELSE IF    '${delivery_methods}[${index}]'=='${filters.delivery_methods.click_and_collect}'    Click n collect option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
        ...    ELSE IF    '${delivery_methods}[${index}]'=='${filters.delivery_methods.two_hours_pick_up}'    Pickup 2 hour option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
        ...    ELSE IF    '${delivery_methods}[${index}]'=='${filters.delivery_methods.three_hours_delivery}'    3 hour delivery option labels should be equal lead time data    ${lead_times}[${index}]    ${frees}[${index}]
    END
    Run Keyword Unless    '${filters.delivery_methods.two_hours_pick_up}' in ${delivery_methods}     Pickup 2 hour option labels should be not displayed

Home delivery option labels should be equal lead time data
    [Arguments]    ${home_lead_text}    ${home_free_text}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_home_lead_time]    ${home_lead_text}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_home_free_lead_time]    ${home_free_text}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[icon_home_delivery]

Click n collect option labels should be equal lead time data
    [Arguments]    ${cnc_lead_text}    ${cnc_free_text}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_clickncollect_lead_time]    ${cnc_lead_text}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_clickncollect_free_lead_time]    ${cnc_free_text}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[icon_clickncollect]

Pickup 2 hour option labels should be not displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictproductPDP}[lbl_2hour_lead_time]    ${dictproductPDP}[lbl_2hour_free_lead_time]    ${dictproductPDP}[icon_hour_pickup]

Pickup 2 hour option labels should be equal lead time data
    [Arguments]    ${hour_lead_text}    ${hour_free_text}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_2hour_free_lead_time]    ${hour_free_text}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[icon_hour_pickup]
    ${lead_text}=    SeleniumLibrary.Get Text    ${dictproductPDP}[lbl_2hour_lead_time]
    ${hour_lead_text}=    Get Regexp Matches    ${hour_lead_text}    [0-9]{1,2}:[0-9]{2}
    ${lead_text}=    Get Regexp Matches    ${lead_text}    [0-9]{1,2}:[0-9]{2}
    ${hour_lead_text}=    Convert Time    ${hour_lead_text}[0]    result_format=number
    ${lead_text}=    Convert Time    ${lead_text}[0]    result_format=number
    ${subtract_time}=    Subtract Time From Time    ${hour_lead_text}    ${lead_text}    result_format=number 
    Should Be True    ${subtract_time}<=120.0 and ${subtract_time}>=0.0

Express delivery option labels should be equal lead time data
    [Arguments]    ${express_lead_text}    ${express_free_text}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_express_lead_time]    ${express_lead_text}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_express_free_lead_time]    ${express_free_text}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[icon_express_delivery]

3 hour delivery option labels should be equal lead time data
    [Arguments]    ${3hour_lead_text}    ${3hour_free_text}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_3hour_free_lead_time]    ${3hour_free_text}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[icon_3hour_delivery]
    ${lead_text}=    SeleniumLibrary.Get Text    ${dictproductPDP}[lbl_3hour_lead_time]
    ${3hour_lead_text}=    Get Regexp Matches by date time    ${3hour_lead_text}
    ${lead_text}=    Get Regexp Matches by date time    ${lead_text}
    ${3hour_lead_text}=    Convert Time    ${3hour_lead_text}[0]    result_format=number
    ${lead_text}=    Convert Time    ${lead_text}[0]    result_format=number
    ${subtract_time}=    Subtract Time From Time    ${3hour_lead_text}    ${lead_text}    result_format=number 
    Should Be True    ${subtract_time}<=120.0 and ${subtract_time}>=0.0

Verify message line for out of stock or preorder or byorder was displayed
    [Arguments]    ${msgLine}
    CommonWebKeywords.Verify Web Elements Are Visible    ${msgLine}

Verify available delivery options section was not display
    SeleniumLibrary.Element Should Not Be Visible    ${dictproductPDP}[lnk_get_estimated_dates]
    SeleniumLibrary.Element Should Not Be Visible    ${dictproductPDP}[txt_postcode]
    SeleniumLibrary.Element Should Not Be Visible    ${dictproductPDP}[btn_postcode_apply]

Verify value into postcode textbox
    [Arguments]    ${text}
    CommonWebKeywords.Click Element    ${dictproductPDP}[txt_postcode]
    SeleniumLibrary.Input Text     ${dictproductPDP}[txt_postcode]    ${text}

Verify Postcode Are Still Saved During Customer Session
    [Arguments]    ${product_type}
    SeleniumLibrary.Go Back
    product_PDP_page.Go to pdp by product type url and return sku    ${product_type}
    product_PDP_page.Click 'delivery & return' tab
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[txt_postcode]
    SeleniumLibrary.Set Focus To Element    ${dictproductPDP}[txt_postcode]
    ${WebElement}    Execute Javascript       return document.activeElement
    ${post_code}=    SeleniumLibrary.Get Element Attribute    ${WebElement}    value
    ${post_code}=    Convert To Integer    ${post_code}
    Should Be Equal    ${post_code_list}[0]    ${post_code}

Get product details to verify in PDP pageby sku
    [Arguments]    ${sku}
    ${product_details}=    product.Get product via product V2 API by SKU    ${sku}

    ${description}=    JSONLibrary.Get Value From Json    ${product_details}[0]    $..custom_attributes[?(@.attribute_code=='description')].value
    ${description}=    Get product description    ${description}[0]

    ${redeem_point}=    JSONLibrary.Get Value From Json    ${product_details}[0]    $..extension_attributes.t1c_redeemable_points
    ${brand_name}=    JSONLibrary.Get Value From Json    ${product_details}[0]    $..custom_attributes_option[?(@.attribute_code=='brand_name')].value

    &{product_info}=   Create Dictionary
    ...    description=${description}
    ...    redeem_point=${redeem_point}[0][0]
    ...    brand_name=${brand_name}[0]

    [Return]    ${product_info}

Get product description
    [Arguments]    ${description}
    ${description}=    Fetch From Right    ${description}    <br />
    ${description}=    Fetch From Left    ${description}    </p>
    ${description}=    Strip String    ${description}

    [Return]    ${description}

Verify complete the look default tab is visible
    SeleniumLibrary.Wait Until Element Is Visible    ${dictproductPDP}[tab_active_complete_the_look]

Verify complete the look other tab is visible
    SeleniumLibrary.Wait Until Element Is Visible    ${dictproductPDP}[tab_prev_complete_the_look]

Verify previous button of complete the look tab is visible
    SeleniumLibrary.Wait Until Element Is Visible    ${dictproductPDP}[prev_btn_complete_the_look]

Verify next button of complete the look tab is visible
    SeleniumLibrary.Wait Until Element Is Visible    ${dictproductPDP}[next_btn_complete_the_look]

Verify previous button of complete the look tab is not visible
    SeleniumLibrary.Wait Until Element Is Not Visible    ${dictproductPDP}[prev_btn_complete_the_look]

Verify next button of complete the look tab is not visible
    SeleniumLibrary.Wait Until Element Is Not Visible    ${dictproductPDP}[next_btn_complete_the_look]

Verify 'img product src' display on product PDP page
    [Arguments]    ${img_src}    ${timeout}=5s
    ${locator_img}=    Format Text  ${dictproductPDP}[img_main_product_src]  $img_src=${img_src}
    SeleniumLibrary.Wait Until Element Is Visible     ${locator_img}    ${timeout}

Verify 'img product overlay src' display on product PDP page
    [Arguments]    ${img_src}    ${timeout}=5s
    ${is_img_box_thumnail_visible}=     Run Keyword And Return Status    SeleniumLibrary.Element Should Be Visible    ${img_box_thumbnail}
    ${locator_overlay_img}    Set Variable If    ${is_img_box_thumnail_visible}    ${dictproductPDP}[img_product_overlay_src_with_slide]    ${dictproductPDP}[img_product_overlay_src]

    ${locator_img}=    Format Text  ${locator_overlay_img}  $img_src=${img_src}
    SeleniumLibrary.Wait Until Element Is Visible     ${locator_img}    ${timeout}

Verify 'product name label' should be displayed correctly
    [Arguments]    ${product_sku}    ${product_name}
    ${locator_product_name}=    Format Text  ${dictproductPDP}[text_PDP_product_name]  product_sku=${product_sku}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${locator_product_name}    ${product_name}

Verify product details attr 'Shade' should be displayed correctly
    [Arguments]    ${shade}
    ${locator}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_product_details_attr_shade]    $shade=${shade}
    CommonWebKeywords.Verify Web Elements Are Visible    ${locator}
    
Verify product details attr 'Size' should be displayed correctly
    [Arguments]    ${size}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_product_details_attr_size]    ${size}

Verify product details attr 'Color Template' should be displayed correctly
    [Arguments]    ${color_template}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_product_details_attr_color_template]    ${color_template}

Verify 'rating label' should be displayed correctly
    [Arguments]    ${rating}
    ${rating}=    CommonKeywords.Convert To Two Digit Number    ${rating}
    ${rating}=    Evaluate    round(${rating}, 1)
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[tab_product_details]
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[lbl_rating]
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_rating]    ${rating}/5

Verify 'total vote label' should be displayed correctly
    [Arguments]    ${total_vote}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dictproductPDP}[lbl_review_total_vote]    ${total_vote} ${product_detail_page.reviews}

Verify 'all label' should be displayed correctly
    [Arguments]    ${total_vote}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[btn_all]    (${total_vote})

Verify 'five star label' should be displayed correctly
    [Arguments]    ${five_star}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[btn_five_star]    (${five_star})

Verify 'four star label' should be displayed correctly
    [Arguments]    ${four_star}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[btn_four_star]    (${four_star})

Verify 'three star label' should be displayed correctly
    [Arguments]    ${three_star}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[btn_three_star]    (${three_star})

Verify 'two star label' should be displayed correctly
    [Arguments]    ${two_star}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[btn_two_star]    (${two_star})

Verify 'one star label' should be displayed correctly
    [Arguments]    ${one_star}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[btn_one_star]    (${one_star})

Verify 'no review label' should be displayed
    CommonWebKeywords.Scroll To Center Page
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[lbl_product_details]
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[lbl_customer_reviews]
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[lbl_no_review]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lbl_no_review]

Verify 'no review label' should not be displayed
    CommonWebKeywords.Scroll To Element    ${dictproductPDP}[lbl_product_details]
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictproductPDP}[lbl_no_review]

Verify 'add a review link' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lnk_add_a_review]

Verify 'customer reviews details' should be displayed correctly
    [Arguments]    ${reviews_row}    ${text}
    ${nickname}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_review_index]    $index=${reviews_row}    $text=${text}
    CommonWebKeywords.Verify Web Elements Are Visible    ${nickname}

Verify full price on product PDP page should not be displayed
    [Arguments]    ${product_sku}
    ${elem}=    Format Text  ${dictproductPDP}[text_full_price]  product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify save price percent on product PDP page should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictproductPDP}[lbl_product_save_price_percent]  

Verify 'product promo tag' should be displayed correctly
    [Arguments]    ${sku}    ${promo}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_product_promo_tag]    $promo=${promo}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}   

Verify 'product new tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_product_new_tag]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}   

Verify 'product new tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_product_new_tag]
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}   

Verify 'product only at tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_product_only_at_tag]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}   

Verify 'product only at tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_product_only_at_tag]
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}   

Verify 'product online exclusive tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_product_online_exclusive_tag]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}   

Verify 'product online exclusive tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[lbl_product_online_exclusive_tag]
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Select configurable option Shade
    [Arguments]    ${shade}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[btn_configurable_option_shade]    $shade=${shade}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Select configurable option Size
    [Arguments]    ${size}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[ddl_configurable_option_size]
    CommonWebKeywords.Scroll And Click Element    ${elem}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[list_configurable_option_size]    $size=${size}
    CommonWebKeywords.Click Element    ${elem}

Verify 'product marketplace sold by' should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictproductPDP}[lbl_marketplace_sold_by]

Verify 'product marketplace sold by' should be displayed correctly
    [Arguments]    ${marketplace_sold_by}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_marketplace_sold_by]    ${product_detail_page.sold_by} ${marketplace_sold_by}

Verify 'pay by installment' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[btn_installment]

Verify 'pay by installment' should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictproductPDP}[btn_installment]

Click 'pay by installment' button
    CommonWebKeywords.Scroll And Click Element    ${dictproductPDP}[btn_installment]

Verify 'installment pop up' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lbl_installment_pop_up]

Click 'OK' installment pop up button
    CommonWebKeywords.Click Element    ${dictproductPDP}[btn_ok_installment_pop_up]

Verify 'product id' should be displayed correctly
    [Arguments]    ${product_id}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dictproductPDP}[lbl_product_id]    ${product_detail_page.product_id} ${product_id}

Click 'promotions' tab
    CommonWebKeywords.Scroll And Click Element    ${dictproductPDP}[tab_promotions]

Verify 'promotions tab details' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[tab_promotions_details]

Click 'delivery & return' tab
    Run Keyword And Ignore Error    CommonWebKeywords.Scroll And Click Element    ${dictproductPDP}[tab_delivery_and_return]

Verify 'delivery & return tab details' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[tab_delivery_and_return_details]

Verify 'add wishlist button' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[btn_add_wishlist]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click 'add wishlist' button
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[btn_add_wishlist]    $sku=${sku}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Verify 'remove wishlist button' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dictproductPDP}[btn_remove_wishlist]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify Check stock at store option should not be displayed
    Verify Buy in Store title should not be displayed
    Verify Check stock at store title should not be displayed

Verify Buy in Store title should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictproductPDP}[text_buy_in_store]

Verify Check stock at store title should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dictproductPDP}[btn_check_stock_at_store]

Verify 'please login pop up' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lbl_please_login_pop_up]    ${dictproductPDP}[lbl_please_login_to_add_product_to_wish_list_pop_up]

Click 'OK' please login pop up button
    CommonWebKeywords.Click Element    ${dictproductPDP}[btn_ok_please_login_pop_up]

Verify view more pickup location button display correctly
    [Arguments]    ${product_sku}
    ${date_time_list}=    Get List Date Time From PickUp Location    ${product_sku}
    ${length}=     Get Length    ${date_time_list}
    ${number_of_pages}=    Evaluate    math.ceil(${length}/5)
    FOR    ${index}    IN RANGE    ${number_of_pages}
        ${status}=    Run Keyword And Return Status    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[btn_view_more_pickup_locations]
        Run Keyword If    ${status}==${True}    CommonWebKeywords.Click Element    ${dictproductPDP}[btn_view_more_pickup_locations]
        ...    ELSE    Exit For Loop
    END

Get List Date Time From PickUp Location
    [Arguments]    ${product_sku}
    ${response}=    search.Get store pickup location    ${product_sku}
    @{date_time_list}=    Get Value From Json    ${response}    $..date_time
    [Return]    ${date_time_list}

Verify format lead time for each store
    [Arguments]    ${product_sku}
    ${estimated_times}=    Get List Text Elements    ${dictproductPDP}[lbl_estimate_time_of_stores]
    ${date_time_list}=    Get List Date Time From PickUp Location    ${product_sku}
    ${len}=     Get Length    ${estimated_times}
    ${curent_dateTime}=    Get Current Date    result_format=%Y-%m-%d
    FOR    ${index}    IN RANGE    ${len}
        ${date_time}=    Convert Date    ${date_time_list}[${index}]     result_format=%Y-%m-%d
        Run Keyword If    '${date_time}'=='${curent_dateTime}'    Verify format time to UTC+7    ${date_time_list}[${index}]    ${estimated_times}[${index}]
    END

Verify format time to UTC+7
    [Arguments]    ${datetime_api}    ${date_time_ui}
    ${datetime_UTC+7}=    Add Time To Date    ${datetime_api}    07:00:00    result_format=%H:%M
    ${date_time_ui}=    Get Regexp Matches by date time    ${date_time_ui}
    ${datetime_UTC+7}=    Convert Time    ${datetime_UTC+7}    result_format=number
    ${date_time_ui}=    Convert Time    ${date_time_ui}[0]    result_format=number
    ${subtract_time}=    Subtract Time From Time    ${datetime_UTC+7}    ${date_time_ui}    result_format=number 
    Should Be True    ${subtract_time}<=120.0 and ${subtract_time}>=0.0

Verify Check stock at store should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[btn_check_stock_at_store]

Click Check stock at store
    CommonWebKeywords.Click Element    ${dictproductPDP}[btn_check_stock_at_store]

Verify search field should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[txt_search_check_stock_at_store]

Input store name into postcode textbox
    [Arguments]    ${text}
    SeleniumLibrary.Input Text     ${dictproductPDP}[txt_search_check_stock_at_store]    ${text}

Verify Use my current location in check stock at store should be visible
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lnk_use_current_location_check_stock_at_store]

Verify list store in check stock at store should be visible
    [Arguments]    ${product_sku}
    ${response}=    product.Get store pickup location available    ${store_view.name.th}    ${product_sku}
    @{stores_name_response}=    Get Value From Json    ${response}    $..name
    ${stores_name_ui}=    Get List Text Elements    ${dictproductPDP}[store_name_check_stock_at_store]
    Lists Should Be Equal    ${stores_name_response}    ${stores_name_ui}

Input postcode into postcode textbox
    [Arguments]    ${text}
    CommonWebKeywords.Click Element    ${dictproductPDP}[txt_search_check_stock_at_store]
    SeleniumLibrary.Input Text     ${dictproductPDP}[txt_search_check_stock_at_store]    ${text}

Click postcode apply button on check stock at store
    CommonWebKeywords.Click Element Using Javascript    ${dictproductPDP}[btn_postcode_apply_check_stock_at_store]

Verify store are searched by store name
    [Arguments]    ${text}
    ${list_stores}=    Get List Text Elements    ${dictproductPDP}[store_name_check_stock_at_store]
    ${len}=    Get Length    ${list_stores}
    Run Keyword If    ${len}==1    Should Be Equal As Strings    ${list_stores}[0]    ${text}
    ...     ELSE    Fail    No store found

Get list store name by postcode value
    [Arguments]    ${product_sku}    ${text}
    ${response}=    product.Get store pickup location available    ${store_view.name.th}    ${product_sku}
    @{stores_name}=    Get Value From Json    ${response}    $..name
    @{post_code}=    Get Value From Json    ${response}    $..post_code
    @{list}=    Create List
    FOR    ${index}    IN RANGE    len(@{post_code})
        Run Keyword If    '@{post_code}[${index}]'=='${text}'    Append To List    ${list}    ${stores_name}[${index}]
    END
    [Return]    @{list}

Verify store are searched by postcode value
    [Arguments]    ${product_sku}    ${post_code}
    ${stores_name_response}=    Get list store name by postcode value    ${product_sku}    ${post_code}
    ${stores_name_ui}=    Get List Text Elements    ${dictproductPDP}[store_name_check_stock_at_store]
    Lists Should Be Equal    ${stores_name_response}    ${stores_name_ui}

The invalid postcode message should be visible on check stock at store
    CommonWebKeywords.Verify Web Elements Are Visible    ${dictproductPDP}[lbl_invalid_postcode_check_stock_at_store]

Verify stock status on Check stock at store
    [Arguments]    ${product_sku}
    ${response}=    product.Get store pickup location available    ${store_view.name.th}    ${product_sku}
    ${stock_status}=    Get Value From Json    ${response}    $..stock_status_code
    ${stock_status_ui}=    Get List Text Elements    ${dictproductPDP}[lbl_stock_status_check_stock_at_store]
    FOR    ${index}    IN RANGE    len(@{stock_status})
        Should Be Equal As Strings    ${${stock_status}[${index}]}    ${stock_status_ui}[${index}] 
    END
