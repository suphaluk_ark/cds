*** Variables ***
&{bank_transfer}    txt_amount=.amount
...    txt_phone=css=.mobile-number
...    txt_ref=xpath=//*[@data-testid='payment-code']
...    btn_back=xpath=//div[@data-testid='continue-button']

*** Keywords ***
Click return to merchant button
    CommonWebKeywords.Verify Web Elements Are Visible    ${bank_transfer}[btn_back]
    CommonWebKeywords.Scroll And Click Element    ${bank_transfer}[btn_back]

Payment Code Should Display In Payment Slip
    CommonWebKeywords.Verify Web Elements Are Visible    ${bank_transfer}[txt_ref]
