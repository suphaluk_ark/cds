*** Keywords ***
Summarize price with quantity
    [Documentation]    This keyword summarize price with vat and quantity
    ...    After calling this keyword ‘test variable’ price_with_quantity and price_with_quantity_format would be assigned
    [Arguments]    ${product_quantity}    ${price_with_vat}=${price_with_vat}
    ${price_with_quantity}=    Evaluate    ${price_with_vat} * ${product_quantity}
    # ${price_with_quantity_format}=    Convert price to total amount format    ${price_with_quantity}

    ${price_with_quantity_format}=    Run Keyword If    ${price_with_quantity}.is_integer()    Convert To Integer    ${price_with_quantity}
    ...    ELSE    Convert price to total amount format    ${price_with_quantity}
    Set Test Variable    ${price_with_quantity}    ${price_with_quantity}
    Set Test Variable    ${price_with_quantity_format}    ${price_with_quantity_format}

Calculate discount(%) from original total price
    [Arguments]    ${original_total}    ${discount}
    ${discount_amount}=    Evaluate    round(${original_total} * ${discount}/100.0, 2)
    ${discount_amount_with_format}=    Convert price to total amount format    ${discount_amount}
    ${discount_amount}=     Run Keyword If    ${discount_amount}.is_integer()    Convert price to total amount int format    ${discount_amount}
    ...    ELSE    Set Variable    ${discount_amount}
    Set Test Variable    ${discount_amount}    ${discount_amount}
    Set Test Variable    ${discount_amount_with_format}    ${discount_amount_with_format}

Calculate discount(Bath) from original total price
    [Arguments]    ${original_total}    ${discount}
    ${discount_amount}=    Evaluate    round(${discount}, 2)
    ${discount_amount_format}=    Run Keyword If    ${discount_amount}.is_integer()    Convert price to total amount format    ${discount_amount}
    ...    ELSE    Convert price to total amount format    ${discount_amount}
    ${discount_amount}=     Convert price to total amount int format    ${discount_amount}
    Set Test Variable    ${discount_amount}    ${discount_amount}
    Set Test Variable    ${discount_amount_with_format}    ${discount_amount_format}

Summarize price with discount, T1redeem and shipping charge
    [Arguments]    ${price}    ${discount}    ${redeem}    ${shipping}
    ${order_total}=    Evaluate    round(${price}+${shipping}-${discount}-${redeem}, 2)
    ${order_total_format}=        Run Keyword If    ${order_total}.is_integer()    Convert price to total amount int format    ${order_total}
    ...    ELSE    Convert price to total amount format    ${${order_total}}
    Set Test Variable    ${order_total}    ${order_total}
    Set Test Variable    ${order_total_format}    ${order_total_format}

Summarize price with discount, T1redeem and shipping charge with split order total
    [Arguments]    ${sku}    ${qty}    ${discount}    ${redeem}   ${shipping}
    ${len_price}=    Get Length    ${sku}
    ${sub_total}    Set Variable    ${0}
    FOR    ${index}    IN RANGE    ${len_price}
        ${price}=    Evaluate    round(${${sku}[${index}].price}*${qty}[${index}], 2)
        ${sub_total}=    Evaluate    ${sub_total}+${price}
    END
    ${order_total}=    Evaluate    round(${sub_total}+${shipping}-${discount}-${redeem}, 2)
    ${order_total_format}=        Run Keyword If    ${order_total}.is_integer()    Convert price to total amount int format    ${order_total}
    ...    ELSE    Convert price to total amount format    ${${order_total}}
    ${sub_total_format}=        Run Keyword If    ${sub_total}.is_integer()    Convert price to total amount int format    ${sub_total}
    ...    ELSE    Convert price to total amount format    ${${sub_total}}
    Set Test Variable    ${actual_sub_total}    ${sub_total_format}
    Set Test Variable    ${actual_order_total}    ${order_total_format}

Summarize grand total
    [Arguments]    ${total_price_vat}    ${special_fee_delivery}    ${coupon_price}
    ${grand_total}=    Evaluate    round(${price_with_quantity}+${special_fee_delivery}+${coupon_price}, 2)
    ${grand_total_with_format}=    Convert price to total amount format    ${grand_total}
    Set Test Variable    ${grand_total}    ${grand_total}
    Set Test Variable    ${grand_total_with_format}    ${grand_total_with_format}

Calculate total price by summary product price with quantity
    [Arguments]    ${price}    ${qty}
    ${total_price}=    Evaluate    round(${price}*${qty}, 2)
    [Return]    ${total_price}

Calculate grand total price by deduct discount amount already
    [Arguments]    ${price}    ${discount_amount}
    ${total_price}=    Evaluate    round(${price}-${discount_amount}, 2 )
    [Return]    ${total_price}

Evaluate percent discount
    [Arguments]    ${price}    ${special_price}
    ${percent_discount}=    Run Keyword If    ${special_price}==${None}    Set variable    0
    ...   ELSE    Evaluate    round((100*(${price} - ${special_price}))/${price})
    ${percent_discount}    Convert To Integer    ${percent_discount}
    [Return]    ${percent_discount}

Evaluate save price
    [Arguments]    ${price}    ${special_price}
    ${save_price}=    Evaluate    ${price} - ${special_price}
    [Return]    ${save_price}