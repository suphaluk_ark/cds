*** Setting ***
Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/web_authentication.robot

*** Variables ***
${GLOBALTIMEOUT}     ${15}
${GLOBALWAITTIME}    ${5}
${speed}             ${0.1}
${browserName}       chrome

*** Keywords ***
Common Keyword - Open Browser And Set Window size
    [Arguments]    ${BROWSER}=CHROME    ${width}=1440    ${height}=900
    Open Browser    data:,    ${BROWSER}
    Set Window Size    ${width}    ${height}

Common Keyword - Open CDS Home Page
    [Arguments]    ${language}=TH
    Common Keyword - Open Browser And Set Window size
    SeleniumLibrary.Set Selenium Speed     ${speed}
    # Handle Alert    action=LEAVE
    ${windows}=    Get Window Names
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//*[@id="btn-popup-close"]
    SeleniumLibrary.Click Element    xpath=//*[@id="btn-popup-close"]

Wait Until Page Is Completely Loaded
    Instrument Browser
    Wait For Testability Ready

Force cancel order via MCOM directly
    checkout_page.Get order ID from thank you page
    Run Keyword If  '${increment_id}'!='${NONE}'  Wait Until Keyword Succeeds     10 x    10 sec    force_shipment.Fully force cancel via MCOM by order number    ${increment_id}

Import Env Language Translation
    [Arguments]    ${language_reload}=${language}
    Import Variables    ${CURDIR}/../../../resources/testdata/common/translation_${language_reload}.yaml

Import Locator following by platform
    [Arguments]    ${page_locator}
    Import Resource    ${CURDIR}/../../../resources/locator/${TEST_PLATFORM}/${page_locator}.robot

Go To Direct Url
    [Arguments]    ${path}
    ${url_default}=    Set Variable If    '${BU.lower()}'=='${bu_type.rbs}'    ${rbs_url}    ${central_url}
    SeleniumLibrary.Go To    ${url_default}/${language}/${path}

Go To Home Page
    ${url_default}=    Set Variable If    '${BU.lower()}'=='${bu_type.rbs}'    ${rbs_url}    ${central_url}
    SeleniumLibrary.Go To    ${url_default}/${language}

Get Product Price From Product Data
    [Arguments]    ${sku}
    ${original_price}=    Set Variable    ${${sku}}[price]
    [Return]     ${original_price}

Add Cookie For Disable Capcha
    SeleniumLibrary.Add Cookie    tokenCaptcha    centralfrontend

Scroll to text section
    [Documentation]    Handle the lazy load on the current page
    [Arguments]    ${text_section}
    ${text_section_locator}=    Set Variable    xpath=//*[text()='{$text_section}']
    ${elem}=    CommonKeywords.Format Text    ${text_section_locator}    $text_section=${text_section}
    CommonWebKeywords.Scroll To Center Page
    CommonWebKeywords.Scroll to bottom page
    CommonWebKeywords.Scroll To Element    ${elem}

Get Product Url And Name By Sku
    [Arguments]    ${mdc_sku}
    ${response}=    Get product via product V2 API by SKU    ${mdc_sku}
    ${product_url}=    Get Value From Json     ${response}[0]    $.custom_attributes[?(@.attribute_code == "url_key")].value
    ${product_name}=    Get Value From Json     ${response}[0]    $.name
    [Return]    ${product_url}[0]    ${product_name}[0]

Go To PDP Directly By Product Sku
    [Arguments]    ${product_sku}    ${reset}=${TRUE}
    ${product_url}    ${product_name}    Get Product Url And Name By Sku    ${product_sku}
    Run Keyword If    ${reset}==${FALSE}    Go To Direct Url Without Reset Session    ${product_url}
    ...    ELSE    Go To Direct Url    ${product_url}
    Run Keyword And Ignore Error    home_page_web_keywords.Close Popup
    Wait Until Page Is Completely Loaded

Validate Length Of Characters Entered Into Textbox
    [Arguments]    ${locator}    ${amount}
    CommonWebKeywords.Verify Web Elements Are Visible    ${locator}
    ${returned_text}=    SeleniumLibrary.Get Value    ${locator}
    ${str_len}=    Get Length    ${returned_text}
    Should Be Equal As Numbers    ${str_len}    ${amount}

Go To Direct Url Without Reset Session
    [Arguments]    ${path}
    ${url_default}=    Set Variable If    '${BU.lower()}'=='${bu_type.rbs}'    ${rbs_url}    ${central_url}
    ${url}=    Set Variable    ${url_default}/${language.lower()}/${path}
    Execute Javascript    window.location.href='${url}'

Get current language display on the site
    CommonWebKeywords.Verify Web Elements Are Visible    ${lbl_language_display}
    ${current_lang}=    SeleniumLibrary.Get Text    ${lbl_language_display}
    ${current_lang}=    Set Variable If    "${current_lang}"=="${web_common.switch_language.thai}"    ${common_language.en}    ${common_language.th}
    [Return]    ${current_lang}

Is Browser Openning
    ${s2l}=    Get Library Instance    SeleniumLibrary
    ${driver}     Evaluate    $s2l._current_browser() if "_current_browser" in dir($s2l) else $s2l._drivers.current
    ${status}=    Evaluate    not("NoConnection" in """${driver}""")
    [Return]    ${status}

Get Raw Locator From Dict Locators
    [Arguments]    ${str_locator}
    ${separator}=    Run Keyword If    "css" in "${str_locator}"    Set Variable    css\=
    ...    ELSE    Set Variable    xpath\=
    ${selector}=    Split String From Right    ${str_locator}    separator=${separator}    max_split=-1
    [Return]    ${selector}[-1]

Go To Thank You Page With Order Id By Member
    [Arguments]    ${increment_id}    ${email}    ${password}
    ${is_open}=    common_keywords.Is Browser Openning
    Run Keyword If    ${is_open}==False    common_cds_web.Setup - Open browser
    ...     ELSE    Go To Home Page
    Wait Until Page Is Completely Loaded
    ${is_logged_in}=    header_web_keywords.Is Logged in user
    Run Keyword If    ${is_logged_in}==False    Run Keywords    Go To Direct Url    register
    ...    AND    Wait Until Page Is Completely Loaded
    ...    AND    registration_keywords.Login Account At Register Page    ${email}    ${password}
    ...    AND    Wait Until Page Is Completely Loaded
    Set Test Variable    ${oder_id}    ${increment_id}
    Go To Direct Url    checkout/completed/${increment_id}
    Wait Until Page Is Completely Loaded

Go To Thank You Page With Order Id By Guest
    [Arguments]    ${increment_id}
    ${is_open}=    common_keywords.Is Browser Openning
    Run Keyword If    ${is_open}==False    common_cds_web.Setup - Open browser
    ...     ELSE    Go To Home Page
    Wait Until Page Is Completely Loaded
    ${is_logged_in}=    header_web_keywords.Is Logged in user
    Run Keyword If    ${is_logged_in}==True    Run Keywords    header_web_keywords.Click my account button
    ...    AND    header_web_keywords.Header Web - Click Logout on Header
    Set Test Variable    ${oder_id}    ${increment_id}
    Go To Direct Url    checkout/completed/${increment_id}
    Wait Until Page Is Completely Loaded

Go To Specific Page By Member
    [Arguments]    ${url}    ${email}    ${password}
    ${is_open}=    common_keywords.Is Browser Openning
    Run Keyword If    ${is_open}==False    common_cds_web.Setup - Open browser
    Wait Until Page Is Completely Loaded
    Go To Direct Url    register
    Wait Until Page Is Completely Loaded
    Run Keyword And Ignore Error    home_page_web_keywords.Close Popup
    ${current_url}=    SeleniumLibrary.Get Location
    Run Keyword If    '${TEST_PLATFORM}'=='${common_platform.mobile_web}'    registration_page_mobile_web.Open Login form from register page
    Run Keyword If    'register' in '${current_url}'    registration_keywords.Login Account At Register Page    ${email}    ${password}
    Go To Direct Url    ${url}
    Wait Until Page Is Completely Loaded

Go To Specific Page By Guest
    [Arguments]    ${url}
    ${is_open}=    common_keywords.Is Browser Openning
    Run Keyword If    ${is_open}==False    common_cds_web.Setup - Open browser
    Wait Until Page Is Completely Loaded
    Go To Direct Url    ${url}
    Wait Until Page Is Completely Loaded

Add skus to cart and return user token
    [Arguments]    ${email}    ${password}    ${store}    ${quantities}    ${skus}
    ${user_token}=    Get login token    ${email}    ${password}
    Run Keyword And Ignore Error    Delete cart    ${user_token}
    ${status}    ${cart_id}    Run Keyword And Ignore Error    Post customer cart    ${user_token}    store=${store}
    Run Keyword If    isinstance($skus,list) and isinstance($quantities,list)    Add Multiple Skus To Cart    ${user_token}    ${store}    ${quantities}    ${skus}
    ...    ELSE    cart.Add product to cart    ${user_token}    ${skus}    ${quantities}    ${store}
    [Return]    ${user_token}    ${cart_id}

Add Multiple Skus To Cart
    [Arguments]    ${user_token}    ${store}    ${quantities}    ${skus}
    FOR    ${idx}    IN RANGE    len(${skus})
        cart.Add product to cart    ${user_token}    ${skus}[${idx}]    ${quantities}[${idx}]    ${store}
    END
    [Return]    ${user_token}

Get Cookie Value From Browser
    [Arguments]    ${name}
    ${cookie_object}=    SeleniumLibrary.Get Cookie    ${name}
    [Return]    ${cookie_object.value}

SignIn Web Authentication
    SeleniumLibrary.Wait Until Element Is Visible    ${dictWebAuthen}[txt_signin_username]
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictWebAuthen.txt_signin_username}    ${signin_authentication.username}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dictWebAuthen.txt_signin_password}    ${signin_authentication.password}
    SeleniumLibrary.Click Element    ${dictWebAuthen.btn_signin}
    Wait Until Page Is Completely Loaded

Guest Add Multi Skus To Cart
    [Arguments]    ${skus}    ${quantities}    ${quote_id}    ${store}    
    FOR    ${idx}    IN RANGE    len(${skus})
        cart.Guest add items to cart     ${skus}[${idx}]    ${quantities}[${idx}]    ${quote_id}    ${store} 
    END    
    [Return]    ${quote_id}

Get List Text Elements
    [Arguments]    ${locator}
    SeleniumLibrary.Page Should Contain Element    ${locator}
    ${elems}=    SeleniumLibrary.Get Webelements    ${locator}
    @{list_text}=    Create List
    FOR    ${elem}    IN    @{elems}
        ${elem}=    SeleniumLibrary.Get Text    ${elem}
        Append To List    ${list_text}    ${elem}
    END
    [Return]    ${list_text}

Get Regexp Matches by date time
    [Arguments]    ${date_time}
    ${date_time}=    Get Regexp Matches    ${date_time}    [0-9]{2}:[0-9]{2}
    [Return]    ${date_time}