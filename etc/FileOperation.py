import yaml


def write_product_data_yaml_file(file, product):
    with open(file) as f:
        doc = yaml.load(f)

    print(product)
    doc[product['product_sku']] = product
    with open(file, 'w') as f:
        yaml.dump(doc, f, default_flow_style=False, allow_unicode=True)
