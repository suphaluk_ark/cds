import pymysql
import paramiko
import pandas as pd
from paramiko import SSHClient
from sshtunnel import SSHTunnelForwarder
import os
import csv
import random
import sys
import yaml
from pathlib import Path

query_url_key = '''
select {select_value}
from catalog_product_entity product
LEFT JOIN cataloginventory_stock_status stock ON product.entity_id = stock.product_id
LEFT JOIN cataloginventory_stock_item sale ON product.entity_id = sale.product_id
LEFT JOIN catalog_product_entity_varchar url ON product.row_id = url.row_id and url.attribute_id = (select attribute_id from eav_attribute where entity_type_id = 4 and attribute_code = 'url_key' )
WHERE stock.qty {qty_value}
AND sale.max_sale_qty >= 10
AND product.sku LIKE 'CDS%'
AND product.sku in (select product.sku from catalog_product_entity product
LEFT JOIN catalog_product_entity_decimal deci ON product.row_id = deci.row_id and deci.attribute_id = (select attribute_id from eav_attribute where entity_type_id = 4 and attribute_code = 'price' )
WHERE deci.value {price_value})
GROUP BY value
ORDER BY stock.qty DESC
LIMIT {limit_value};'''

staging_db = {
    'pkeyfilepath': os.environ['CDS_PKEY_FILE'],
    'sql_hostname': 'rds.staging.cds-magento.local',
    'sql_username': os.environ['CDS_SQL_USER'],
    'sql_password': os.environ['CDS_SQL_PWD'],
    'sql_main_database': 'cds-prod-20181114',
    'sql_port': 3306,
    'ssh_host': '52.221.217.206',
    'ssh_user': 'ec2-user',
    'ssh_port': 22
}

def get_export_result_path(env, file_name):
    current_dir = os.getcwd()
    result_path = os.path.join(current_dir, "resources", "testdata", "cds", env.lower(), file_name)
    return result_path

def setSQLcommand(sql, **kwargs):
    sql = sql.format(**kwargs)
    return sql

def query_data_by_value(env, sql, select='sku', qty="=0", limit='2', price="=0"):
    sql = setSQLcommand(sql, select_value=select,
                        qty_value=qty, limit_value=limit, price_value=price)
    env = eval(env+'_db')
    mypkey = paramiko.RSAKey.from_private_key_file(env['pkeyfilepath'])

    with SSHTunnelForwarder(
        (env['ssh_host'], env['ssh_port']),
        ssh_username=env['ssh_user'],
        ssh_pkey=mypkey,
            remote_bind_address=(env['sql_hostname'], env['sql_port'])) as tunnel:

        conn = pymysql.connect(
            host='127.0.0.1', user=env['sql_username'], passwd=env['sql_password'],
            db=env['sql_main_database'],
            port=tunnel.local_bind_port)

        query_result = pd.read_sql_query(sql, conn)
        data = pd.DataFrame(query_result).values.tolist()
        conn.close()
        return data

def generate_product_list_sku(var_key, env, language, price_value='<600', sql=query_url_key, select_value='sku', qty_value=">50", limit='10'):
    skus = query_data_by_value(env, sql, select_value, qty_value, limit, price_value)
    sku_list = [val for sub_skus in skus for val in sub_skus]
    with open(get_export_result_path(env, 'product_data.yaml'), 'r') as stream:
        data = yaml.safe_load(stream)
    try:
        status = data[var_key]
    except Exception:
        status = 'Unavailable data {}'.format(sys.exc_info()[0])
    if 'Unavailable data' in status:
        dict_skus = {var_key: sku_list}
        with open(get_export_result_path(env, 'product_data.yaml'), 'a') as outfile:
            outfile.write('\n')
            yaml.dump(dict_skus, outfile, default_flow_style=False)
        outfile.close()
    else:
        with open(get_export_result_path(env, 'product_data.yaml'), 'w') as outfile:
            data[var_key] = sku_list
            yaml.dump(data, outfile, default_flow_style=False,
                      allow_unicode=True)
        outfile.close()