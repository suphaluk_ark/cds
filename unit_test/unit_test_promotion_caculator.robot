*** Settings ***
Resource    ${CURDIR}/imports.robot

*** Test Cases ***
Test calculate multiple discounts - amount and amount
    ${actual}=    promotion_calculator.Calculate Multiple Discounts    total=500    discount_amount_1=50     discount_amount_2=100
    Should Be Equal    ${350.0}    ${actual}

Test calculate multiple discounts - amount and percent
    ${actual}=    promotion_calculator.Calculate Multiple Discounts    total=500    discount_amount=50     discount_percent=10
    Should Be Equal    ${405.0}    ${actual}

Test calculate multiple discounts - percent and percent
    ${actual}=    promotion_calculator.Calculate Multiple Discounts    total=500    discount_percent_1=10      discount_percent_2=5
    Should Be Equal    ${427.5}    ${actual}

Test calculate multiple discounts - percent and amount
    ${actual}=    promotion_calculator.Calculate Multiple Discounts    total=500    discount_percent=10    discount_amount=50
    Should Be Equal    ${400.0}    ${actual}

Test calculate if there is no discount
    ${actual}=    promotion_calculator.Calculate Multiple Discounts    total=500
    Should Be Equal    ${500.0}    ${actual}