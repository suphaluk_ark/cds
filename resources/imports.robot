*** Setting ***
Library      Collections
Library      DateTime
Library      String
Library      RequestsLibrary
Library      JSONLibrary
Library      REST
Library      SeleniumLibrary    run_on_failure=None    plugins=SeleniumTestability;True;30 Seconds;False;True, ${CURDIR}/../keywords/common-keywords/plugins/SeleniumCaptureScreen.py, ${CURDIR}/../keywords/common-keywords/plugins/SeleniumRetryRunKeyword.py
Library      AppiumLibrary    run_on_failure=No Operation
Library      DatabaseLibrary
Library      Dialogs
Library      FakerLibrary    locale=en_US
Library      TemplatedData    jinja_template=True
Variables    common_configs.yaml
Variables    configs/${BU.lower()}/${ENV.lower()}/env_config.yaml
Variables    testdata/${BU.lower()}/${ENV.lower()}/test_data.yaml
Variables    testdata/${BU.lower()}/${ENV.lower()}/product_data.yaml
