*** Variables ***
&{dictWishlistpage}
...     txt_product_name=xpath=(//div/a[text()='{$product_name}'])[1]
...     btn_addToCart=xpath=//*[@id='btn-addCart-{$skunumber}']//span[text()='${web_common.add_to_bag}']/..
...     btn_loading=xpath=//*[@id='btn-addCart-{$skunumber}']//span[text()='${web_common.adding}']/..
...     btn_add_success=xpath=//*[@id='btn-addCart-{$skunumber}']//span[text()='${web_common.added}']/..
...     btn_remove=css=div.nS9u8 div._1yEAC svg.sc-bdVaJa.fUuvxv

# on a product list page
...     btn_wishlist_list=//div[contains(@id,"btn-viewFlip")]
...     btn_wishlist_random=xpath=(//div[contains(@id,'btn-addWishlist-{$sku_number}')]//*[local-name() = 'svg'])
...     lbl_product_title_random=xpath=(//div[contains(@id,'btn-addWishlist')])[{$random}]//preceding::h4[1]
...     img_product=xpath=(//div[contains(@id,'btn-addWishlist')])[{$random}]//preceding::h4[1]
...     img_product_wishlist=xpath=//div[@data-product-sku][{$random}]
...     img_product_wishlist_info=xpath=//div[@data-product-sku][{$random}]//*[contains(@id,'btn-addWishlist')]//preceding::h4[1]
...     lbl_added_product_title=xpath=(//a[@title='{$title}'])[last()]
...     img_wishlist_icon=xpath=(//a[@to='/account/wishlist'])[1]
...     img_wishlist_icon_not_loggin=xpath=(//div[@id='mini-cart']/../..//a/*[local-name() = 'svg'])[1]
...     msg_login_require=xpath=(//*[@class='sweet-alert '])[1]
...     btn_OK=xpath=//*[text()='OK']
### in wishlist page ###
...     lbl_wishlist_title=xpath=//*[text()='${wishlist_page.title}']
...     btn_continue_shopping=xpath=//*[text()='${wishlist_page.continue_shopping}']
...     btn_previous=xpath=//li[@class='previous']/a
...     btn_next=xpath=//li[@class='nextArrow']/a
...     btn_last_page=xpath=(//li[@class='nextArrow']/preceding-sibling::li/a)[last()]
...     lbl_product_number=xpath=//*[text()='${wishlist_page.title}']/following-sibling::span
...     btn_add_to_bag=xpath=//*[text()='${wishlist_page.add_to_bag}']
...     btn_remove_first_product=xpath=(//*[@id='account-container']//*[local-name()='svg'])[4]
...     img_social_icon=xpath=(//img[@src='/icons/socials/line.svg'])[1]
...     div_products=xpath=//a[contains(@id,"lnk-viewProduct")]//img[@data-product-name]
...     lnk_last_product=xpath=(//a[starts-with(@id,'lnk-viewProduct')])[last()]
