*** Variables ***
&{productPage}    btn_addToCart=xpath=//div[contains(@data-testid, 'btn-addCart-{$skunumber}')]//span[text()='${web_common.add_to_bag}']/..
...    ddl_quantity=xpath=//div[@id='sel-addQty-{$skunumber}']
...    list_qty=xpath=//div[@id='sel-addQty-{$skunumber}']/following-sibling::div//span[text()='{$number}']/../..
...    btn_loading=xpath=//div[contains(@data-testid, 'btn-addCart-{$skunumber}')]//span[text()='${web_common.adding}']/..
...    btn_add_success=xpath=//div[contains(@data-testid, 'btn-addCart-{$skunumber}')]//span[text()='${web_common.added}']/..
...    btn_add_to_wishlist=xpath=//span[text()='${web_common.wishlist}']/../../../div[@class='_3nLeX']
...    icon_product_wishlist=xpath=//div[@id='btn-addAddToWishlistV2View-{$product_sku}']
...    txt_pre_order_popup=xpath=//div[@id='app']//following::div[@class='sweet-alert '][2]//following::div[contains(text(),'{$txt_pre_order_popup}')]
...    btn_popup_ok=xpath=//*[text()='OK']
...    txt_pre_order_onpage=xpath=//div[@id='app']//following::div[@class='sweet-alert ']//following::div[contains(text(),'{$txt_pre_order}')]
...    img_product_images=css=a[id^='lnk-viewProduct'] img[class$='trackProductClick']
...    img_product_images_into_section=css=img[data-product-list$='{$section}']
...    lbl_badge_homedelivery=xpath=//div[@data-testid='icon-delivery-badge-delivery-{$flag}-{$sku_number}']
...    lbl_badge_clickncollect=xpath=//div[@data-testid='icon-delivery-badge-click-collect-{$flag}-{$sku_number}']
...    lbl_badge_2hours=xpath=//div[@data-testid='icon-delivery-badge-2hour-pickup-{$flag}-{$sku_number}']
...    lbl_badge_3hours=xpath=//*[@data-testid='icon-delivery-badge-3hour-delivery-{$flag}-{$sku_number}']
...    txt_section_homedelivery=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-delivery-{$flag}-{$sku_number}']
...    txt_section_clickncollect=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-click-collect-{$flag}-{$sku_number}']
...    txt_section_2hours=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-2hour-pickup-{$flag}-{$sku_number}']
...    txt_section_3hours=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-3hour-delivery-{$flag}-{$sku_number}']
...    ddl_sort=xpath=//div[@data-testid='Available_Delivery_Options']//preceding-sibling::div[./div[@data-testid='select-filter-default']]
...    new_arrivals=xpath=//div[./div[@data-testid='select-filter-default'] and (following-sibling::div[@data-testid='Available_Delivery_Options'])]//*[contains(text(),'${plp_sort_dropdownlist.NewArrivals}')]
...    ddl_filter=xpath=//div[@data-testid='Available_Delivery_Options']
...    chk_clickAndCollect=css=[data-testid='txt-viewDeliveryOption-Click & Collect-total']
...    chk_delivery=css=[data-testid='txt-viewDeliveryOption-Delivery-total']
...    chk_hourPickup=css=[data-testid='txt-viewDeliveryOption-2 Hours Pick Up-total']
...    fil_products=xpath=//img[contains(@class,'js-trackProductClick')]
...    btn_clear=css=[data-testid='Available_Delivery_Options'] button
...    txt_number_selected=css=[data-testid='Available_Delivery_Options'] p:nth-child(1)
...    list_product_plp=xpath=//*[contains(@id,'lnk-viewProduct') and not(@data-product-id)]
...    list_product_section_plp=xpath=//a[not(@data-product-id)][*[@data-product-list='{$section}']]
