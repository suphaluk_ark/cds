*** Variables ***
&{dictMyAccountPage}    btn_connect_t1=xpath=//*[text()='${my_account_page.connect_t1_account}']
...    lbl_change_pwd=xpath=//a[@to='/account/change-password']

###overview
...    lbl_overview_default_shipping_address=xpath=//*[text()='${overview_page.default_shipping_address}']
...    lbl_overview_full_tax_invoice_address=xpath=//*[text()='${overview_page.default_billing_address}']
...    lbl_overview_lastest_order=xpath=//*[text()='${overview_page.lastest_order}']

...    lbl_cust_name=xpath=//*[text()='${my_account_page.name}']/../div[.='{$name}']
...    lbl_cust_email=xpath=//*[text()='${my_account_page.email}']/../div[.='{$email}']
...    lbl_cust_phone=xpath=//*[text()='${my_account_page.phone_no}']/../div[.='{$phone}']
...    h3_account=xpath=//h3[text()='${my_account_page.account_overview}']
...    Login_t1c_dialog=xpath=//*[text()='${overview_page.login_T1C}']
...    btn_t1c_dialog_close=xpath=(//img[@src='/icons/t-1-c-logo.svg'])[2]/../../../child::div
...    txt_email=xpath=//*[@name='customerEmail']
...    txt_password=xpath=//*[@name='customerPassword']
...    btn_login_t1c=xpath=//button[@type='submit']
...    cust_t1c_no=xpath=//*[text()='${my_account_page.t1c_number}']/..//div[text()='{$t1c_no}']
...    lbl_point_info=xpath=//div/div[text()='${my_account_page.t1c_point}']/..//span[.='${my_account_page.you_have_point}']

...    lbl_name_address=xpath=//*[@id='checkout-address']//div[(.='{$cust_name}')]
...    lbl_phone_address=xpath=//*[@id='checkout-address']//div[(.='{$cust_phone}')]
...    lbl_default_address=xpath=//*[@id='checkout-address']//div[(.='{$cust_address}')]
...    lbl_region_address=xpath=//*[@id='checkout-address']//div[(.='{$cust_region}')]

...    lbl_name_tax_address=xpath=//div/div[text()='${my_account_page.full_tax_address}']/../..//div[.='{$cust_name}']
...    lbl_phone_tax_address=xpath=//div/div[text()='${my_account_page.full_tax_address}']/../..//div[text()='{$cust_phone}']
...    lbl_default_tax_address=xpath=//div/div[text()='${my_account_page.full_tax_address}']/../..//div[text()='{$cust_address} ']
...    lbl_region_tax_address=xpath=//div/div[text()='${my_account_page.full_tax_address}']/../..//div[text()='{$cust_region}']

...    lbl_lastest_order=xpath=//*[text()='${my_account_page.lastest_orders}']

...    lbl_order_number=xpath=//*[contains(.,'${my_account_page.order} {$order_no}')]
...    lbl_order_status=xpath=//*[text()='${order_history.order_status}']/..//span[text()='{$order_status}']
...    lbl_delivery_option=xpath=//div/p[text()='${order_history.delivery_option}']/..//p[.='{$delivery_status}']
...    lbl_payment=xpath=//div/p[.='${order_history.payment_type}']/..//p[.='{$payment_type}']

...    lbl_price=xpath=//*[text()='{$total_price}']
...    btn_disconnect_t1c=xpath=//div/a[text()='${my_account_page.disconnect_t1c}']

## my profile page
...    txt_box_firstname=xpath=//*[@name='firstname' and @value='{$cust_fname}']
...    txt_box_lastname=xpath=//*[@name='lastname' and @value='{$cust_lname}']
...    txt_box_email_disabled=xpath=//*[@name='email' and @value='{$cust_email}' and @disabled]
...    txt_box_phone=xpath=//*[@name='phone' and @value='{$cust_phone}']
...    txt_box_birthday=xpath=//*[@name='dob' and @value='{$cust_dob}']
...    txt_fname=xpath=//*[@name='firstname']
...    txt_lname=//*[@name='lastname']
...    txt_phone=xpath=//*[@name='phone']
...    txt_dob=xpath=//*[@name='dob']
...    radio_gender_female=xpath=//*[@name='gender' and @value='2']
...    radio_gender_male=xpath=//*[@name='gender' and @value='1']
...    radio_gender_none=xpath=//*[@name='gender' and @value='3']
...    radio_lang_en=xpath=//*[@name='language' and @value='en']
...    radio_lang_th=xpath=//*[@name='language' and @value='th']
...    btn_edit=xpath=//*[text()='${my_account_page.edit}']
...    lbl_phone_err_msg=xpath=//*[@name='phone']//following-sibling::div[text()='${error_message.my_account_page.phone_err_start_0}']

...    lbl_first_name_require=xpath=//*[@name='firstname']/following-sibling::div[contains(text(),'${error_message.my_account_page.required_field}')]
...    lbl_last_name_require=xpath=//*[@name='lastname']/following-sibling::div[contains(text(),'${error_message.my_account_page.required_field}')]
...    lbl_phone_require=xpath=//*[@name='phone']/following-sibling::div[contains(text(),'${error_message.my_account_page.required_field}')]
...    lbl_dob_require=xpath=//*[@name='dob']/following-sibling::div[contains(text(),'${error_message.my_account_page.required_field}')]

...    my_profile_page=xpath=//a[@to='/account/profile']
...    wishlist_page=xpath=//a[@to='/account/wishlist' and text()='${my_account_page.wishlist}']
...    my_account_page=xpath=//div/a[@to='/account/overview']
...    order_page=xpath=//a[@to='/account/orders']
...    address_book_page=xpath=//a[@to='/account/address']
...    img_user_icon=xpath=//img[@src='/icons/user_icon.svg']
...    rdo_checked_gender=xpath=//input[@name='gender' and @checked]
## address box
...    lbl_default_shipping=xpath=//*[@id='inf-viewAddressCustomerNameOnCheckout-{$id}']//following::span[text()='${my_account_page.default_shipping_address}'][1]//preceding::div[1]
...    lbl_default_billing=xpath=//*[@id='inf-viewAddressCustomerNameOnCheckout-{$id}']//following::span[text()='${my_account_page.default_billing_address}'][1]//preceding::div[1]
...    bnt_add_new_address=xpath=(//a[@to="/account/address/new"])[1]
...    lbl_address_name=xpath=//*[starts-with(@id,'inf-viewAddressNameOnCheckout-{$id}')]
...    lbl_customer_name=xpath=//*[starts-with(@id,'inf-viewAddressCustomerNameOnCheckout-{$id}')]
...    lbl_customer_phone=xpath=//*[starts-with(@id,'inf-viewAddressTelephoneOnCheckout-{$id}')]
...    lbl_address_line1=xpath=//*[starts-with(@id,'inf-viewAddressLine1OnCheckout-{$id}')]
...    lbl_address_line2=xpath=//*[starts-with(@id,'inf-viewAddressLine2OnCheckout-{$id}')]

...    lbl_lastest_address_name=xpath=(//*[starts-with(@id,'inf-viewAddressNameOnCheckout')])[last()]
...    lbl_lastest_customer_name=xpath=(//*[starts-with(@id,'inf-viewAddressCustomerNameOnCheckout')])[last()]
...    lbl_lastest_customer_phone=xpath=(//*[starts-with(@id,'inf-viewAddressTelephoneOnCheckout')])[last()]
...    lbl_lastest_address_line1=xpath=(//*[starts-with(@id,'inf-viewAddressLine1OnCheckout')])[last()]
...    lbl_lastest_address_line2=xpath=(//*[starts-with(@id,'inf-viewAddressLine2OnCheckout')])[last()]

...    address_info_section=xpath=//*[@id="checkout-address"]
...    btn_delete_all=xpath=//*[starts-with(@to,'/account/address/edit/')]/../parent::div/div[text()='${my_account_page.address_book.delete}']
...    btn_random_address=xpath=(//*[starts-with(@to,'/account/address/edit/')])[{$random}]
...    btn_random_more=xpath=(//*[starts-with(@to,'/account/address/edit/')]/../../parent::div)[{$random}]
...    btn_random_edit=xpath=(//*[starts-with(@to,'/account/address/edit/')])[{$random}]
...    btn_random_delete=xpath=(//*[starts-with(@to,'/account/address/edit/')]/../following-sibling::div)[{$random}]
...    btn_more=xpath=//*[starts-with(@to,'/account/address/edit/{$id}')]/../../parent::div
...    btn_delete=xpath=//*[starts-with(@to,'/account/address/edit/{$id}')]/../parent::div/div[text()='${my_account_page.address_book.delete}']
...    btn_more_action=xpath=//*[starts-with(@to,'/account/address/edit/')]/../../parent::div
...    msg_firstname_require=xpath=//*[@name="firstName"]/following-sibling::p[text()='${error_message.address_book.require_field}']
...    msg_lastname_require=xpath=//*[@name="lastName"]/following-sibling::p[text()='${error_message.address_book.require_field}']
...    msg_telephone_require=xpath=//*[@name="phone"]/..//p[text()='${error_message.address_book.require_field}']
...    msg_address_line_require=xpath=//*[@name="address"]/following-sibling::p[text()='${error_message.address_book.require_field}']
...    msg_postcode_require=xpath=//*[@name="postcode"]/following-sibling::p[text()='${error_message.address_book.require_field}']
...    msg_region_require=xpath=//*[text()='${address_box.province}']/..//p[text()='${error_message.address_book.require_field}']
...    msg_district_require=xpath=//*[text()='${address_box.district}']/..//p[text()='${error_message.address_book.require_field}']
...    msg_subdistrict_require=xpath=//*[text()='${address_box.sub_district}']/..//p[text()='${error_message.address_book.require_field}']

...    btn_add_tax_invoice=xpath=//*[text()='${address_box.set_tax_invoice}']/..//div[@class='react-switch-bg']
...    txt_cardId=xpath=//*[@name='cardID']
...    chk_default_shipping=xpath=//*[text()='${address_box.set_default_shipping}']/..//div[@class='react-switch-bg']
...    chk_default_billing=xpath=//*[text()='${address_box.set_billing_shipping}']/..//div[@class='react-switch-bg']
...    chk_default_shipping_selected=xpath=//div[@class='_3dQ1i _1Q-g3']/../following-sibling::span[text()='${my_account_page.default_shipping_address}']
...    rdo_default_address=xpath=//*[text()='${my_account_page.default_shipping_address}']/preceding-sibling::span//div

# create and edit address 
...    txt_address_name=xpath=//input[@name='addressName']
...    txt_address_firstname=xpath=//input[@name='firstName']
...    txt_address_lastname=xpath=//input[@name='lastName']
...    txt_address_telephone=xpath=//input[@name='phone']
...    txt_address_line=xpath=//input[@name='address']
...    txt_address_postcode=xpath=//input[@name='postcode']
...    ddl_address_region=xpath=//select[@name='province']
...    ddl_address_district=xpath=//select[@name='district']
...    ddl_address_subdistrict=xpath=//select[@name='subdistrict']
...    btn_address_save=xpath=//div[text()='${my_account_page.save_address}']
...    btn_address_discard=xpath=//div[text()='${my_account_page.discard}']
...    lbl_address_phone_err_msg=xpath=//*[@name='phone']/..//p[text()='${error_message.my_account_page.phone_err_start_0}']

# the T1
...    txt_email_t1c=xpath=//input[@name="email"]
...    txt_password_t1c=xpath=//input[@name="password"]
