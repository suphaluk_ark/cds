*** Variables ***
#please do not follow this format anymore
${popup}    css=#btn-popup-close
${search_box}    xpath=(//*[@id='txt-searchProductOnSearchBar'])[1]
${lbl_product_list}    xpath=//a[contains(@id,'{$product}')]
${lbl_product_list_name}    xpath=//div[@data-product-name="{$product_name}"]
${homepage_title_locator}    ${windows_locator_title}
${home_page_title}    xpath=//*[text()='${windows_locator_title}']
${btn_search_close}    xpath=(//*[@id='txt-searchProductOnSearchBar'])[1]//following-sibling::a
${homepage_bestseller_section}    xpath=//h2[text()='${homepage.section.best_seller}']
${homepage_hotdeal_section}    xpath=//h2[text()='${homepage.section.hotdeal}']
${homepage_hotdeal_list}    xpath=//*[.='${homepage.section.hotdeal}']//following-sibling::div//div[contains(@class, 'trackProductClick')]
${homepage_hotdeal_item}    xpath=//*[.='${homepage.section.hotdeal}']//following-sibling::div//h4[text()="{product_name}"]
${btn_x_on_search_box}    xpath=(//*[@id='txt-searchProductOnSearchBar'])[1]/following-sibling::*/*[@viewBox]
${lbl_language_display}    xpath=//div[@id="switch-language"]//span[1]
${btn_women_category}    css=[to*="/category/women"]
${lbl_brand}    xpath=//*[@to="shopbybrand"]

#Please create new locator for home page here (using data dict)
&{dictHomePage}
...    lbl_flashdeal=xpath=//h2[text()='${homepage.section.flash_deal}']
...    data-product-sku_by_index=xpath=//*[contains(@id,'lnk-viewProduct')][./img[@data-product-list='Flash Deal' and @data-product-position='{$index}']]
...    img_data-product-id_by_index=xpath=//*[contains(@id,'lnk-viewProduct')][./img[@data-product-list='Flash Deal' and @data-product-position='{$index}']]/img
...    lbl_flash_deal_price=xpath=(//*[./*[@id="lnk-viewProduct-{$sku}"]]/following-sibling::*//div[text()='฿'])[1]
...    lbl_homepage=id=layout