*** Variables ***
&{dictMyCartPage}
...    lbl_cart_page_title=xpath=//div[starts-with(text(),'${page_title.my_cart.title}')]
...    btn_checkout=xpath=(//*[@id='lnk-viewCheckout']//div[text()='${web_common.secure_checkout}'])[1]
...    page_load=id=full-page-loader
...    lbl_product_name=xpath=//*[@id="lnk-viewProduct-{$product_sku}"]/label[text()='{$product_name}']
...    lbl_quantity=xpath=(//*[@id='sel-addQty-{$product_sku}']//span)[2]
...    lnk_edit_bag=id=lnk-editCart
...    msg_deliver=id=inf-viewShippingSuggest
...    msg_free_deliver=xpath=//*[@id='inf-viewShippingSuggest']
...    msg_spend_more=xpath=//*[@id='inf-viewShippingSuggest']
...    list_qty=xpath=//div[@id='sel-addQty-{$skunumber}']/following-sibling::div//*[text()='{$number}']
...    ddl_quantity=id=sel-addQty-{$skunumber}
...    lbl_count_product=id=inf-viewcountProduct
...    lbl_product_brands=xpath=//*[starts-with(@id,'lnk-viewBrandOn')]
...    lbl_product_name=xpath=//*[starts-with(@id,'lnk-viewProduct') and @title]/label
...    icn_remove_product=css=#mini-cart img[src*="trash-icon"]
...    txt_coupon_code_display_at_cart=xpath=//*[@id='cart-summary']//*[contains(text(),'{$coupon_code}')]

## promotion
...    txt_apply_coupon=xpath=//input[@placeholder='${cart_page.apply_coupon}']
...    lbl_coupon_error_msg=xpath=//*[text()='${cart_page.coupon_err_msg}']
...    txt_free_gift=xpath=//*[text()='${cart_page.free_gift}']
...    lbl_free_items_with_order_purchase=//*[text()='${cart_page.promo_item_with_order_purchase}' and .//img[contains(@src,'ic-gift-red')]]
...    lbl_free_items_with_order_purchase_product=(//*[text()='${cart_page.free_items}']/parent::*//*[@data-product-id="{$product_sku}"][text()="{$product_name}"])[1]
...    lbl_free_gift_with_this_item_purchase=//*[./child::*/img[@data-pid="{$purchase_item_sku}"]]/following-sibling::*//*[text()='${cart_page.free_gift}']
...    lbl_free_gift_with_this_item_purchase_product=//*[./child::*/img[@data-pid="{$purchase_item_sku}"]]/following-sibling::*//*[text()='{$free_gift_product_name}']
...    lbl_free_gift_product=xpath=//*[text()='{$product}']
...    btn_apply_coupon=xpath=//*[text()='${cart_page.apply}']
...    btn_view_coupon=xpath=//*[text()='${cart_page.view_promo}']
...    lbl_view_coupon=xpath=//tr/td/div[text()='{$title}']/../../td/div[text()='{$coupon}']
...    btn_close_popup=xpath=//*[text()='${cart_page.special_promo}']/../..//div/div
...    txt_specific_card=xpath=(//*[text()='${cart_page.noti_specific_card}'])[1]
...    btn_ok=xpath=//*[text()='${cart_page.ok}']
...    lbl_credit_card_discount=xpath=//*[text()='${cart_page.credit_card_discount}']/../..//div
...    btn_remove_coupon=xpath=//*[text()='{$coupon_code}']/..//a

## price
...    grand_total=xpath=//div/span[text()='${cart_page.total}']/..//div
...    lbl_coupon_code=xpath=//*[text()='{$coupon}']/..//div
...    lbl_items=xpath=//*[contains(.,'${cart_page.product_items}')]/..//div
...    other_discount=xpath=//*[text()='${cart_page.other_discount}']/..//div
...    lbl_remove_all=xpath=//*[starts-with(@id,'btn-removeProduct')]
...    lnk_remove_product=id=btn-removeProduct-{$sku_number}
...    lbl_sub_total=id=inf-viewPriceTotalItem-{$product_sku}
...    lbl_price=id=inf-viewPriceItem-{$product_sku}
...    lbl_selected_quantity=xpath=(//*[@id='sel-addQty-{$product_sku}']//span[2])[1]
...    lbl_price_total=id=inf-viewPriceSubtotal
...    lbl_grand_total=id=inf-viewPriceGrandTotal
...    lbl_sub_quantity=xpath=(//*[@id='sel-addQty-{$product_sku}']//span[2])[1]
...    lbl_coupon=xpath=(//*[@id='cart-summary']//span)[2]
...    btn_apply_coupon=id=btn-addCouponCode

## free gift ##
...    msg_not_enough_free_gift=xpath=//*[text()='${error_message.my_cart_page.not_enough_free_gift}']
...    lbl_free_gift_quantity=xpath=//img[contains(@src,'{$gift_sku}')]/following::div[6]
...    lbl_free_gift_size=xpath=//img[contains(@src,'{$gift_sku}')]/following::div[5]
...    lbl_free_gift_product_name=xpath=//img[contains(@src,'{$gift_sku}')]/following::div[3]
...    lbl_free_gift_brand_name=xpath=//img[contains(@src,'{$gift_sku}')]/following::div[2]
...    lbl_free_gift_price=xpath=//img[contains(@src,'{$gift_sku}')]/following::div[8]
...    img_free_gift=xpath=//img[contains(@src,'{$gift_sku}')]