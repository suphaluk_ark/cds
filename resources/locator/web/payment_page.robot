
*** Variables ***
&{dictPaymentPage}
### Shipping Summary ###
...    lnk_edit_shipping_summary=id=lnk-editShippingSummary
...    lbl_shipping_summary=xpath=//*[text()='${payment_page.shipping_summary}']
...    lbl_billing_address=xpath=//*[text()='${payment_page.billing_address}']
...    lbl_shipping_full_name=xpath=(//*[@id="inf-viewShippingSummary-fullname"])[1]
...    lbl_shipping_address=xpath=(//*[@id="inf-viewShippingSummary-addressLine"])[1]
...    lbl_shipping_telephone=xpath=(//*[@id="inf-viewShippingSummary-telephone"])[1]
...    lbl_taxinvoice_full_name=xpath=//*[text()="${payment_page.billing_address}"]//following::div[@id="inf-viewShippingSummary-fullname"]
...    lbl_taxinvoice_address=xpath=//*[text()="${payment_page.billing_address}"]//following::div[@id="inf-viewShippingSummary-addressLine"]
...    lbl_taxinvoice_telephone=xpath=//*[text()="${payment_page.billing_address}"]//following::div[@id="inf-viewShippingSummary-telephone"]
...    icon_credit_card_full_payment=//img[@src='/images/payment/credit-card.svg']
...    lbl_credit_card_full_payment=xpath=//*[text()='${payment_page.credit_card}']
...    lbl_on_top_discount=xpath=//*[@data-testid='inf-viewPaymentMethodTabHeaderOnOnTopDiscount-payment_service_fullpayment']

### Order Summary ###
...    lnk_edit_bag=id=lnk-editCart
...    lbl_product_name=xpath=//*[@id='inf-viewProductNameOnOrderSummary-{$product_sku}']
...    img_product=xpath=//img[@src='{$img_link}']
...    lbl_product_price=xpath=//*[@id='inf-viewPriceOnOrderSummary-{$product_sku}']
...    lbl_product_quantity=//*[@id='inf-viewQtyOnOrderSummary-{$product_sku}']

### Payment Summary ###
...    h3_payment_summary=xpath=//*[text()='${payment_page.payment_summary}']
...    h4_total=xpath=//*[text()='${payment_page.total}']
...    h4_order_total=xpath=//*[text()='${payment_page.order_total}']
...    lbl_sub_total=id=inf-viewPrice-subtotal
...    lbl_grand_total=id=inf-viewPrice-grand_total
...    rdo_pay_at_store=id=inf-addPaymentMethodTabHeader-payatstore
...    btn_confirm_pay_at_store=xpath=//*[@id="inf-addPaymentMethodTabHeader-payatstore"]//following::button[@id="btn-viewCheckout"][1]
...    txt_pay_at_store_warning_msg=xpath=//*[@id="inf-addPaymentMethodTabHeader-payatstore"]//following::button[@id="btn-viewCheckout"][1]//following::span[1]
...    rdo_delivery_option=//div[contains(@id,'rdo-addDeliveryOption')]
...    txt_addT1_earn_point=css=#txt-addT1Earn
