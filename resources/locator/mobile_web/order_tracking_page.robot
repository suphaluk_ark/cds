*** Variables ***
&{dictOrderTrackingPage}    lbl_order_tracking=xpath=//*[text()='${web_common.track_your_order}']
...    txt_email=xpath=//*[@name='email']
...    txt_order_number=xpath=//*[@name='orderNumber']
...    txt_search_order=xpath=//*[@type='submit' and text()='${web_common.track_order}']
...    msg_invalid_combination=xpath=//*[text()='${error_message.track_order.invalid_combination}']
...    msg_order_require=xpath=//*[@name='orderNumber']/following-sibling::div[text()='${error_message.track_order.require_field}']
...    lbl_orders=xpath=//*[@name='orderNumber']/../a
...    lbl_first_order=xpath=(//*[@name='orderNumber']/../a)[1]
...    lbl_order_ID=xpath=//*[contains(@id, 'txt-viewOrderHeaderOnOrderID')]
...    lbl_order_ID_guest=xpath=//*[contains(@id, 'txt-viewOrderDetailOnOrderID')]