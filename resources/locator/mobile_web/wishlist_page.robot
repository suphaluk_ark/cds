*** Variables ***
&{dictWishlistpage}
### in wishlist page ###
...     lbl_wishlist_title=xpath=(//*[contains(text(),'${my_account_page.wishlist}')])[2]
...     btn_continue_shopping=xpath=//*[text()='${wishlist_page.continue_shopping}']
...     lbl_product_number=xpath=//*[text()='${wishlist_page.title}']/following-sibling::span
...     btn_remove_first_product=xpath=(//div[@id='account-container']//*[local-name()='svg'])[4]
...     btn_add_to_bag=xpath=(//span[text()='${wishlist_page.add_to_bag}'])[4]
...     btn_next=xpath=//li[@class='next']/a/*[local-name()='svg']
...     div_products=xpath=//a[contains(@id,"lnk-viewProduct")]//img[@data-product-name]
...     lnk_last_product=xpath=(//a[starts-with(@id,'lnk-viewProduct')])[last()]
...     btn_last_page=xpath=(//li[@class='next']/preceding-sibling::li/a)[last()]
# on a product list page
...     lbl_added_product_title=xpath=(//a[@title='{$title}'])[last()]
...     btn_OK=xpath=//a[text()='OK']
...     msg_login_require=css=a[to="/register"]