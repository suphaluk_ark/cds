*** Variables ***
&{dictDeliveryDetailPage}
### Tax invoice ###
...    request_tax_invoice_option=css=.react-switch-bg
...    default_tax_invoice_address=xpath=//div[@class="react-switch-bg"]//following::div[@id="checkout-address"]
...    btn_edit_tax_invoice_address=xpath=//div[@class="react-switch-bg"]//following::button[@id="btn-editAddress"]
...    lbl_tax_invoice_name=xpath=//div[@class="react-switch-bg"]//following::h4[@id="inf-viewAddressNameOnCheckout-{$address_id}"]
...    lbl_tax_invoice_customer_name=xpath=//div[@class="react-switch-bg"]//following::div[@id="inf-viewAddressCustomerNameOnCheckout-{$address_id}"]
...    lbl_tax_invoice_telephone=xpath=//div[@class="react-switch-bg"]//following::div[@id="inf-viewAddressTelephoneOnCheckout-{$address_id}"]
...    lbl_tax_invoice_address_no_building=xpath=//div[@class="react-switch-bg"]//following::div[@id="inf-viewAddressLine1OnCheckout-{$address_id}"]
...    lbl_tax_invoice_full_address=xpath=//div[@class="react-switch-bg"]//following::div[@id="inf-viewAddressLine2OnCheckout-{$address_id}"]
...    btn_change_tax_invoice_address=xpath=//div[@class="react-switch-bg"]//following::button[@id="btn-editAddress"]
...    txt_firstname=css=input[name='firstname']
...    txt_lastname=css=input[name='lastname']
...    txt_email=css=input[name='email']
...    txt_telephone=css=input[name='telephone']
...    txt_the1no=css=input[name='the1no']
...    txt_shipping_building=xpath=//input[@name='building']
...    txt_shipping_address_no=xpath=//input[@name='address_line']
...    txt_shipping_postcode=xpath=//input[@name='postcode']
...    sel_shipping_region_id=xpath=//select[@name='region_id']
...    sel_shipping_district_id=xpath=//select[@name='district_id']
...    sel_shipping_subdistrict_id=xpath=//select[@name='subdistrict_id']
...    txt_tax_id_card=xpath=//div[@class="react-switch-handle"]//following::input[@name="vat_id" and @placeholder="${change_address_popup.input_id_card}"]
...    txt_tax_company=xpath=//div[@class="react-switch-handle"]//following::input[@name="company"]
...    txt_tax_id=xpath=//div[@class="react-switch-handle"]//following::input[@name="vat_id" and @placeholder="${change_address_popup.input_tax_id}"]
...    txt_tax_branch_id=xpath=//div[@class="react-switch-handle"]//following::input[@name="branch_id"]
...    txt_tax_building=xpath=//*[@name="building"]
...    txt_tax_address_no=xpath=//div[@class="react-switch-handle"]//following::input[@name="address_line"]
...    txt_tax_postcode=xpath=//div[@class="react-switch-handle"]//following::input[@name="postcode"]
...    sel_tax_region_id=xpath=//div[@class="react-switch-handle"]//following::select[@name="region_id"]
...    sel_tax_district_id=xpath=//div[@class="react-switch-handle"]//following::select[@name="district_id"]
...    sel_tax_subdistrict_id=xpath=//div[@class="react-switch-handle"]//following::select[@name="subdistrict_id"]
...    btn_continue_payment=css=#btn-viewPayment
...    rdo_billing_option=css=input[name="billing_type"][value="company"]
...    lbl_id_card_warning_msg=xpath=//div[text()="${delivery_details_page.id_card_no}"]//following-sibling::div[text()="{$warning_message}"]
...    lbl_tax_id_warning_msg=xpath=//div[text()="${delivery_details_page.tax_id}"]//following-sibling::div[text()="{$warning_message}"]
...    lnk_select_shipping_address=xpath=//div[@class="react-switch-bg"]//preceding::button[@id="btn-editAddress"]
...    txt_rdo_pickup_at_store=css=div[id="rdo-addDeliveryOption-pickupAtStore"] div[data-testid="cmsblock-delivery-option"]
...    lbl_store_name=css=[data-testid='store-item'] h4
...    btn_select_store=xpath=//button[@data-testid='btn-set-store']
...    lbl_hour_pickup_stores=xpath=//div[@data-testid='store-item' and .//*[@data-testid='${pickup_time.express}']]
...    lbl_standard_pickup_stores=xpath=//div[@data-testid='store-item' and .//*[@data-testid='${pickup_time.standard_3d}'] | .//*[@data-testid='${pickup_time.standard_6d}']]
...    lbl_delivery_method_headers=css=[data-testid='inf-viewCheckoutPackage-PackageContainer'] [data-testid^='inf-viewPackageHeaderOnCheckoutPackage']
...    lbl_sub_delivery_method_headers=css=[data-testid='inf-viewCheckoutPackage-PackageContainer']:last-child [data-testid^='inf-viewPackageHeaderOnCheckoutPackage']
...    lbl_delivery_method_text_headers=css=[data-testid='inf-viewCheckoutPackage-PackageContainer'] [data-testid^='inf-viewPackageHeaderOnCheckoutPackage'] p:nth-child(1)
...    lbl_2_hour_header=css=[data-testid='inf-viewCheckoutPackage-PackageContainer'] [data-testid^='inf-viewPackageHeaderOnCheckoutPackage']:not([data-testid$='future_day_pickup'])
...    lbl_standard_pickup_header=css=[data-testid='inf-viewCheckoutPackage-PackageContainer'] [data-testid='inf-viewPackageHeaderOnCheckoutPackage-future_day_pickup']
...    lbl_standard_delivery_header=css=[data-testid='inf-viewCheckoutPackage-PackageContainer'] [data-testid='inf-viewPackageHeaderOnCheckoutPackage-home_delivery']
...    lbl_sub_standard_pickup_header=css=[data-testid='inf-viewCheckoutPackage-PackageContainer']:last-child [data-testid='inf-viewPackageHeaderOnCheckoutPackage-future_day_pickup']
...    lbl_sub_standard_delivery_header=css=[data-testid='inf-viewCheckoutPackage-PackageContainer']:last-child [data-testid='inf-viewPackageHeaderOnCheckoutPackage-home_delivery']
...    lbl_store_uptime=css=[data-testid='inf-viewCheckoutPackage-PackageContainer'] p[padding='0']:not([data-testid='distance-text'])
...    img_selected_icon=css=[data-testid^='inf-viewactiveCircleOnPackageHeader'] > img
...    lbl_sku_qty=xpath=//div[@data-testid='inf-viewCheckoutPackage-PackageContainer']/div[not(descendant::*)]
...    lbl_sub_sku_qty=xpath=//div[@data-testid='inf-viewCheckoutPackage-PackageContainer'][2]/div[not(descendant::*)]
...    icon_hour_pickup=xpath=//div/preceding-sibling::div[starts-with(@data-testid, 'inf-viewPackageHeaderOnCheckoutPackage')]/*[contains(@src, 'hour_pickup')]
...    icon_standard_pickup=xpath=//div/following-sibling::div[starts-with(@data-testid, 'inf-viewPackageHeaderOnCheckoutPackage')]/*[contains(@src, 'click_collect')]
...    icon_sub_standard_pickup=xpath=//div/preceding-sibling::div[starts-with(@data-testid, 'inf-viewPackageHeaderOnCheckoutPackage')]/*[contains(@src, 'click_collect')]
...    icon_sub_standard_delivery=xpath=//div/following-sibling::div[starts-with(@data-testid, 'inf-viewPackageHeaderOnCheckoutPackage')]/*[contains(@src, 'delivery.svg')]
...    lbl_hour_pickup=css=p[package-info='checkout-package-mainPackage-today_pickup']:first-child, p[package-info='checkout-package-mainPackage-next_day_pickup']:first-child
...    lbl_standard_pickup=css=p[package-info='checkout-package-mainPackage-future_day_pickup']:first-child
...    lbl_sub_standard_pickup=css=p[package-info='checkout-package-subPackage-future_day_pickup']:first-child
...    lbl_sub_standard_delivery=css=p[package-info='checkout-package-subPackage-home_delivery']:first-child
...    lbl_hour_pickup_free_text=xpath=//p[(@package-info='checkout-package-mainPackage-today_pickup' or @package-info='checkout-package-mainPackage-next_day_pickup') and contains(., '${checkout_page.shipping_fee}')]
...    lbl_standard_pickup_free_text=xpath=//p[@package-info='checkout-package-mainPackage-future_day_pickup' and contains(., '${checkout_page.shipping_fee}')]
...    lbl_sub_standard_pickup_free_text=xpath=//p[@package-info='checkout-package-subPackage-future_day_pickup' and contains(., '${checkout_page.shipping_fee}')]
...    lbl_sub_standard_delivery_free_text=xpath=//p[@package-info='checkout-package-subPackage-home_delivery' and contains(., '${checkout_page.shipping_fee}')]
...    btn_edit_shipping_address=css=#address-modal-list button
...    btn_add_more_shipping_address=id=btn-addAddress
...    default_address=id=checkout-address
...    btn_address_list=css=button#btn-editAddress > svg
...    btn_change_store=xpath=//*[@data-testid='btn-change-store']
...    btn_address_book=css=[data-testid='btn-editCustomerShippingAddressOnCheckout-AddressBook']
...    txt_firstname_member=css=input[name='firstName']
...    txt_lastname_member=css=input[name='lastName']
...    txt_telephone_member=css=input[name='phone']
...    txt_address_member=css=input[name='address']
...    txt_postcode_member=css=input[name='postcode']
...    lnk_checkout_address=xpath=//div[div[*[@id='checkout-address']]]
...    btn_edit_change_shipping_address=xpath=//div[div[*[@id='checkout-address']]][2]//button
...    lbl_pin_location=css=[data-testid='inf-viewCustomerShippingAddressOnCheckout-PinLocation']
...    lbl_3_hour_delivery=//*[@id='rdo-addShippingMethod-ship_from_store']//div[text()='${shipping_method_label.three_hour_delivery.label}']
...    lbl_require_3_hour=css=[data-testid=PinLocationRequire-openModal] > div > p:last-child
...    btn_pin_location=css=[data-testid=btn-toggleAddressFormWithPinLocation-OpenPin]
...    lbl_pin_you_location=xpath=//*[text()='${checkout_page.pin_your_location}']
...    btn_continue_pin_location=css=[data-testid=btn-savePinYourLocationModal-PinButton]
...    cbb_address_province=css=[name='province']
...    cbb_address_district=css=[name='district']
...    cbb_address_subdistrict=css=[name='subdistrict']
...    cbb_address_province_guest=css=[name='region_id']
...    cbb_address_district_guest=css=[name='district_id']
...    cbb_address_subdistrict_guest=css=[name='subdistrict_id']
...    lbl_pin_location_map_address=xpath=//button[@title='Close']/preceding-sibling::*//div//div
...    lbl_3_hour_shipping_address=css=[data-testid='inf-viewPinLocationAddress-Location-Name']
...    btn_pin_location_guest=xpath=//div[*[@alt='Location Icon']]
...    txt_address_guest=css=[name='address_line']
...    lbl_shipping_address_guest=css=[alt='Location Icon'] ~ div > p:last-child
...    lbl_latitude=css=[name='latitude']
...    lbl_longitude=css=[name='longitude']

### order summary ###
...    h3_oder_summary=xpath=//h3[text()='${checkout_page.order_summary}']
...    lnk_edit_bag=id=lnk-editCart
...    lbl_brand_name=xpath=//div[starts-with(@id, 'inf-viewProductNameOnOrderSummary')]/preceding-sibling::div
...    lbl_product_name=xpath=//div[starts-with(@id, 'inf-viewProductNameOnOrderSummary')]
...    lbl_price=xpath=//div[starts-with(@id, 'inf-viewPriceOnOrderSummary')]
...    lbl_quantity=xpath=//span[starts-with(@id, 'inf-viewQtyOnOrderSummary')]
...    img_product=xpath=//div[starts-with(@id, 'inf-viewProductOnOrderSummary')]//img

### payment summary ###
...    h3_payment_summary=xpath=//h3[text()='${checkout_page.payment_summary}']
...    lbl_promotion=xpath=//div[starts-with(@id,'inf-viewPricePromocode')]
