*** Variables ***
&{dictForgotPasswordPage}
...    txt_email_forgot=id=txt-formForgotPassword-email
...    btn_send_email=id=btn-forgotPassword
...    txt_forgot_title=xpath=//h1[text()='${page_title.forgot_password.title}']
...    txt_forgot_subtitle=xpath=//p[text()='${page_title.forgot_password.subtitle}']
...    txt_err_message=xpath=(//*[.//*[@name="email"]]/preceding-sibling::*)[last()]
