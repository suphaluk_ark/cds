*** Variables ***
&{2c2p} 	txt_credit_card=id=tel-cardNumber
... 	txt_cardholder=id=name
... 	txt_cvv=number-cvv
... 	sel_expiry_year=id=expyear
... 	btn_submit=xpath=//button[@type="submit"]
... 	lal_otp=xpath=//*[contains(.,"${payment_page}[OTP_value]")]
... 	btn_proceed=id=continue
... 	txt_otp=name=otp
... 	lbl_product_details=xpath=//*[text()='{label}']/following-sibling::div[text()="${payment_page}[2c2p_description]"]
... 	lbl_product_amount=xpath=//*[text()='{label}']/following-sibling::span[text()[normalize-space() = '{amount} THB']]
... 	cancel_btn=xpath=//*[@id='cancel']
... 	cancel_btn_prod=css=#credit_card_details_form #btnCancel_CC
... 	lbl_order_number=xpath=//*[text()="${payment_page}[invoice_number]"]/following-sibling::div
... 	return_to_page=xpath=*[text()="${payment_page}[return_to_merchant]"]
...    btn_arrow=xpath=//*[@class='arrow']

&{installmentpayment}
...     icon_bank_citi=id=liippBank_2
...     icon_bank_bbl=id=liippBank_3
...     icon_bank_ktc=id=liippBank_4
...     icon_bank_scb=id=liippBank_6
...     icon_bank_tbank=liippBank_22
...     txt_amount=.amount
...     txt_phone=css=.mobile-number
...     bbl_rdo_4month=xpath=//input[@id='3rdo_ipp_option' and @value='4']
...     bbl_rdo_6month=xpath=//input[@id='3rdo_ipp_option' and @value='6']
...     bbl_rdo_10month=xpath=//input[@id='3rdo_ipp_option' and @value='10']
...     citi_rdo_4month=xpath=//input[@id='2rdo_ipp_option' and @value='4']
...     citi_rdo_6month=xpath=//input[@id='2rdo_ipp_option' and @value='6']
...     citi_rdo_10month=xpath=//input[@id='2rdo_ipp_option' and @value='10']
...     lbl_product_details=xpath=//label[text()='{label}']/following-sibling::span[text()="${payment_page}[2c2p_installment]"]
...     txt_credit_card=id=ipp_credit_card_number
...     txt_cardholder=id=ipp_credit_card_holder_name
...     txt_cvv=id=ipp_credit_card_cvv
...     sel_expiry_month=id=ipp_credit_card_expiry_month
...     sel_expiry_year=id=ipp_credit_card_expiry_year
...     btn_submit=id=btnIPPCCSubmit
...     txt_accept_tc=id=chkIPPTermCond
...     rdo_option_kbank=(//*[@name="24rdo_ipp_option"])[1]    #first options
...     chk_term_and_condition=id=chkIPPTermCond
...     txt_cardholder_installment=id=ipp_credit_card_holder_name
...     txt_cardnumber_installment=id=ipp_credit_card_number
...     ddl_expiry_month_installment=id=ipp_credit_card_expiry_month
...     ddl_expiry_year_installment=id=ipp_credit_card_expiry_year
...     txt_cvv_installment=id=ipp_credit_card_cvv
...     btn_continuepayment_installment=id=btnIPPCCSubmit
...     btn_cancelpayment_installment=id=btnCancel_CC
...     btn_return_to_merchant=xpath=(//span[contains(@class,'btn-primary')])
