*** Variables ***
&{dictproductPDP}
...    text_PDP_details=xpath=//div[@data-anchor='content']
...    text_PDP_product_name=xpath=//div[@id='inf-viewProduct-{product_sku}']
...    text_PDP_redeem_point=xpath=//div[@id='inf-viewPriceGetPoints-{product_sku}']
...    icon_PDP_facebook=xpath=//div[@class='_1H3zz']//div[@class='_2UdEo'][1]//img
...    icon_PDP_twitter=xpath=//div[@class='_1H3zz']//div[@class='_2UdEo'][2]//img
...    icon_PDP_email=xpath=//div[@class='_3qWn9']//div[@class='_22BRX']
...    icon_PDP_line=xpath=//div[@class='_1H3zz']//div[@class='_2UdEo'][5]//img
...    text_PDP_shipping_type=xpath=//div[@class="delivery-option-item__title"]//*[contains(text(),{shipping_type})]
...    text_PDP_payment_type=xpath=//div[@class="delivery-option-item__info"]//following::*[contains(text(),{payment_type})]
...    text_brand_name=xpath=//*[@data-testid="brandname-link"]
...    button_add_to_bag=xpath=//*[@id='btn-addCart-{product_sku}']
...    picture_product_pdp=xpath=//*[@alt="{picture_name}"]
...    text_PDP_product_relate=xpath=//*[@title="{product}"]
...    text_PDP_product_relate_second_page=xpath=//*[@data-product-list="{product}"]
...    text_promotion_text=xpath=//*[contains(text(),"${order_PDP.text_promotion_save}")]
...    text_full_price=xpath=//*[@id='inf-viewPriceSave-{product_sku}']
...    text_discount_price=xpath=//*[@id='inf-viewPriceSell-{product_sku}']
...    text_PDP_redeem_point=xpath=//div[@id='inf-viewPriceGetPoints-{product_sku}']
...    lbl_product_id=xpath=//*[@data-testid="detail-component"]//*[starts-with(.,'${product_detail_page.product_id}')]
...    icon_arrow_left=xpath=//button[@class='slick-arrow slick-prev']
...    icon_arrow_right=xpath=//button[@class='slick-arrow slick-next']
...    lbl_out_of_stock=xpath=//*[@data-testid="product-detail-addtocart-soldout"][text()='${product_detail_page.msg_out_of_stock}']
...    icon_cart=xpath=//a[@class='_33BvM LQZsD _36nNv']//span
...    tab_active_complete_the_look=xpath=//*[text()="${product_detail_page.customer_reviews}"]//preceding::div[@class="swiper-slide swiper-slide-active"]
...    tab_prev_complete_the_look=xpath=//*[text()="${product_detail_page.customer_reviews}"]//preceding::div[@class="swiper-slide swiper-slide-prev"]
...    prev_btn_complete_the_look=xpath=//*[text()="${product_detail_page.customer_reviews}"]//preceding::button[contains(@class,"swiper-button-prev")]
...    next_btn_complete_the_look=xpath=//*[text()="${product_detail_page.customer_reviews}"]//preceding::button[contains(@class,"swiper-button-next")]
...    txt_product_id=xpath=//*[contains(text(),"${product_detail_page.product_id}")]
...    product_active_section=xpath=//div[@class="swiper-slide swiper-slide-active"]
...    img_main_product_src=xpath=(//img[contains(@src,'{$img_src}')])[1]
...    img_box_thumbnail=xpath=//*[contains(@class,'boxMobileCss')]
...    img_product_overlay_src=xpath=(//img[contains(@src,'{$img_src}')])[1]
...    img_product_overlay_src_with_slide=xpath=(//img[contains(@src,'{$img_src}')])[2]
...    lbl_product_save_price_percent=xpath=//*[@data-testid="detail-component"]//*[@data-testid="percen-discount"]
...    lbl_product_details_attr_shade=xpath=//*[@data-testid="boxShade"]/preceding-sibling::label[text()='{$shade}']
...    lbl_product_details_attr_size=xpath=(//*[text()='${product_details_attr.size}']/parent::*//span)[2]
...    lbl_product_details_attr_color_template=(//*[text()='${product_details_attr.color_template}']/parent::*//span)[2]
...    tab_product_details=xpath=//*[@id="tabDetail"]
...    lbl_customer_reviews=//*[@data-testid="detail-component"]//*[text()='${product_detail_page.customer_reviews}']
...    lbl_rating=xpath=//*[@data-testid="list-review-filters"]/preceding-sibling::*//*[contains(.,'/5')]
...    lbl_review_total_vote=xpath=(//*[@data-testid="list-review-filters"]/preceding-sibling::*//*[contains(.,'${product_detail_page.reviews}')])[1]
...    lbl_no_review=xpath=//*[@data-testid="placeholder-no-review"]
...    btn_all=xpath=(//*[@data-testid="list-review-filters"]/*)[1]/span
...    btn_five_star=xpath=(//*[@data-testid="list-review-filters"]/*)[2]/span
...    btn_four_star=xpath=(//*[@data-testid="list-review-filters"]/*)[3]/span
...    btn_three_star=xpath=(//*[@data-testid="list-review-filters"]/*)[4]/span
...    btn_two_star=xpath=(//*[@data-testid="list-review-filters"]/*)[5]/span
...    btn_one_star=xpath=(//*[@data-testid="list-review-filters"]/*)[6]/span
...    lnk_add_a_review=xpath=//*[@data-testid="popup-write-review"]
...    lbl_review_index=xpath=(//*[@data-testid="lst-viewReviewListItemOnroot"])[{$index}]//*[text()='{$text}']
...    lbl_product_details=xpath=(//*[text()='${product_detail_page.product_details}'])[2]
...    lbl_product_promo_tag=//*[@data-testid="wrapper-product-tag"]//*[@data-testid="promotion-bage-text"]/*[text()='{$promo}']
...    lbl_product_new_tag=//*[@data-testid="wrapper-product-tag"]//*[@data-testid="promotion-bage-text"]/*[text()='${product_tag.new}']
...    lbl_product_only_at_tag=//*[@data-testid="wrapper-product-tag"]//*[@data-testid="tag-central"]//*[text()='${product_tag.only_at_central}']
...    lbl_product_online_exclusive_tag=//*[@data-testid="wrapper-product-tag"]//*[@data-testid="tag-central"]//*[text()='${product_tag.online_exclusive}']
...    btn_configurable_option_shade=//*[@data-testid="boxShade"]//*[@data-testid="{$shade}"]/div[@style]
...    ddl_configurable_option_size=//*[@data-testid="product-detail-attributes"]//*[text()='${product_details_attr.size}']/following-sibling::*//*[contains(@src,'arrow-down.svg')]
...    list_configurable_option_size=(//*[@data-testid="product-detail-attributes"]//*[text()='${product_details_attr.size}']/following-sibling::*//span[text()='{$size}'])[last()]
...    lbl_marketplace_sold_by=//*[@data-testid="product-seller"]/div
...    btn_installment=//*[@data-testid="installment"]
...    btn_ok_installment_pop_up=//*[@data-testid="installment-popup"]//*[text()='${product_detail_page.ok}']
...    lbl_installment_pop_up=//*[@data-testid="installment-popup"]//h2[text()='${product_detail_page.installment_pop_up_header}']
...    btn_add_wishlist=//*[@id='btn-addWishlist-{$sku}']
...    btn_remove_wishlist=//*[@id='btn-removeWishlist-{$sku}']
...    tab_promotions=//*[@data-anchor="content"]//*[text()='${product_detail_page.tab_promotions}']
...    tab_promotions_details=//*[@data-anchor="header" and //*[@data-active="true"][text()='${product_detail_page.tab_promotions}']]/following-sibling::*[@data-anchor="content"]
...    tab_delivery_and_return=//*[@data-anchor="content"]//*[text()='${product_detail_page.tab_delivery_and_return}']
...    tab_delivery_and_return_details=//*[@data-anchor="header" and //*[@data-active="true"][text()='${product_detail_page.tab_delivery_and_return}']]/following-sibling::*[@data-anchor="content"]
...    lbl_please_login_pop_up=//*[@class="sweet-alert "]//*[text()='${product_detail_page.please_login}']
...    lbl_please_login_to_add_product_to_wish_list_pop_up=//*[@class="sweet-alert "]//*[text()='${product_detail_page.please_login_to_add_product_to_wish_list}']
...    btn_ok_please_login_pop_up=//*[@class="sweet-alert "]//*[text()='${product_detail_page.ok}']
...    txt_postcode=css=#postcode
...    btn_postcode_apply=css=#postcode + img
...    lbl_invalid_postcode=css=[data-testid='txt-viewDeliveryOptionOnPDP-invalid-postcode']
...    lnk_pickup_location=xpath=//*[text()='${product_detail_page.view_pickup_location}']
...    btn_close_button=xpath=//*[contains(@src, 'close')]
...    lnk_filter_by=xpath=//*[@data-testid='sel-filterPickupLocationsModal']
...    lnk_filter_option=xpath=//*[@data-testid='sel-filterPickupLocationsModal']//*[text()='{$filter}']
...    lbl_store_name=xpath=//div[*[text()='${product_detail_page.pickup_location}']]//h4
...    txt_search_postcode=css=[name='searchInput']
...    lbl_estimate_time_of_stores=xpath=//*[text()='${product_detail_page.pickup_location}']/..//strong
...    btn_view_more_pickup_locations=xpath=//*[text()='${product_detail_page.view_more_pickup_locations}']
...    txt_search_check_stock_at_store=xpath=//*[text()='${product_detail_page.check_stock_at_store}']/..//*[@name='postcode']
...    btn_postcode_apply_check_stock_at_store=xpath=//*[text()='${product_detail_page.check_stock_at_store}']/..//*[@data-testid='btn-formCheckStockModalOnPDP-submit']
...    lnk_use_current_location_check_stock_at_store=xpath=//*[text()='${product_detail_page.check_stock_at_store}']/..//*[text()='${product_detail_page.use_my_current_location}']
...    store_name_check_stock_at_store=xpath=//*[contains(text(),'${product_detail_page.km}')]/../../div[text()]
...    lbl_invalid_postcode_check_stock_at_store=xpath=//*[text()='${product_detail_page.check_stock_at_store}']/..//*[@data-testid='txt-viewCheckStockModalOnPDP-invalidPostcode']
...    lbl_stock_status_check_stock_at_store=xpath=//*[text()='${product_detail_page.check_stock_at_store}']/..//a/following-sibling::div
...    btn_check_stock_at_store=xpath=//*[@data-testid="detail-component"]//*[text()='${product_detail_page.check_stock_at_store}']

&{dictProductPreview}    lbl_saving_price=xpath=//div[.='${product_detail_page.preview_section}']//following-sibling::div//div[..//a[contains(@id,'{product_sku}')]]//div[.='${product_detail_page.preview_discount}']
