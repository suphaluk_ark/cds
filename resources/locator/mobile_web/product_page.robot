*** Variables ***
&{productPage}    
...    btn_addToCart=xpath=//*[@id='btn-addCart-{$skunumber}']//span[text()='${web_common.add_to_bag}']/..
...    ddl_quantity=xpath=//*[@id='sel-addQty-{$skunumber}']
...    list_qty=xpath=//*[@id='sel-addQty-{$skunumber}']/following-sibling::div//span[text()='{$number}']/../..
...    btn_loading=xpath=//*[@id='btn-addCart-{$skunumber}']//span[text()='${web_common.adding}']/..
...    btn_add_success=xpath=//*[@id='btn-addCart-{$skunumber}']//span[text()='${web_common.added}']/..
...    btn_add_to_wishlist=xpath=//*[text()='${web_common.wishlist}']/../../../div[@class='_3nLeX']
...    icon_product_wishlist=xpath=//*[@id='btn-addAddToWishlistV2View-{$product_sku}']
...    txt_pre_order_popup=xpath=//*[@id='app']//following::div[@class='sweet-alert '][2]//following::div[contains(text(),'{$txt_pre_order_popup}')]
...    btn_popup_ok=xpath=//*[@id="app"]//a[text()='OK']
...    txt_pre_order_onpage=xpath=//*[@id='app']//following::div[@class='sweet-alert ']//following::div[contains(text(),'{$txt_pre_order}')]
...    img_product_images=css=a[id^='lnk-viewProduct'] img[class$='trackProductClick']
...    img_product_images_into_section=css=img[data-product-list$='{$section}']
...    lbl_badge_homedelivery=xpath=//*[@data-testid='icon-delivery-badge-delivery-{$flag}-{$sku_number}']
...    lbl_badge_clickncollect=xpath=//*[@data-testid='icon-delivery-badge-click-collect-{$flag}-{$sku_number}']
...    lbl_badge_2hours=xpath=//div[@data-testid='icon-delivery-badge-2hour-pickup-{$flag}-{$sku_number}']
...    lbl_badge_3hours=xpath=//*[@data-testid='icon-delivery-badge-3hour-delivery-{$flag}-{$sku_number}']
...    txt_section_homedelivery=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-delivery-{$flag}-{$sku_number}']
...    txt_section_clickncollect=xpath=//*[text()='{$text_section}']//following::div[@data-testid='icon-delivery-badge-click-collect-{$flag}-{$sku_number}']
...    ddl_filter=xpath=//*[@data-testid='Available_Delivery_Options']
...    fil_products=xpath=//img[contains(@class,'js-trackProductClick')]
...    chk_clickAndCollect=xpath=//*[@data-testid='chk-filterDeliveryOptionOnmobile-Click & Collect']
...    chk_delivery=xpath=//*[@data-testid='chk-filterDeliveryOptionOnmobile-Delivery']
...    chk_2hr_pick_up=xpath=//*[@data-testid='txt-viewDeliveryOptionOnmobile-2 Hours Pick Up-total']
...    list_product_plp=xpath=//*[contains(@id,'lnk-viewProduct') and not(@data-product-id)]
...    list_product_section_plp=xpath=//a[not(@data-product-id)][*[@data-product-list='{$section}']]

