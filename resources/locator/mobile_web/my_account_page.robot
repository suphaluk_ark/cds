
*** Variables ***
&{dictMyAccountPage}
## overview page
...    btn_connect_t1=xpath=//*[text()='${my_account_page.connect_t1_account}']
...    Login_t1c_dialog=xpath=//*[text()='${overview_page.login_T1C}']
...    btn_t1c_dialog_close=xpath=(//img[@src='/icons/t-1-c-logo.svg'])[2]/../../../child::div
...    txt_email=xpath=//*[@name='customerEmail']
...    txt_password=xpath=//*[@name='customerPassword']
...    txt_email_t1c=xpath=//*[@name='email']
...    txt_password_t1c=xpath=//*[@name='password']
...    btn_login_t1c=xpath=//button[@type='submit']
...    cust_t1c_no=xpath=//*[text()='${my_account_page.t1c_number}']/..//div[text()='{$t1c_no}']
...    lbl_point_info=xpath=//div/div[text()='${my_account_page.t1c_point}']/..//span[.='${my_account_page.you_have_point}']
...    btn_disconnect_t1c=xpath=//div/a[text()='${my_account_page.disconnect_t1c}']
...    btn_edit_profile=xpath=//*[contains(text(),'${my_account_page}[edit_profile]')]
...    btn_edit_address=xpath=//*[text()='${overview_page}[default_shipping_address]']/following-sibling::div//*[text()='${my_account_page}[change]']

## my profile page
...    txt_box_firstname=xpath=//*[@name='firstname' and @value='{$cust_fname}']
...    txt_box_lastname=xpath=//*[@name='lastname' and @value='{$cust_lname}']
...    txt_box_email_disabled=xpath=//*[@name='email' and @value='{$cust_email}' and @disabled]
...    txt_box_phone=xpath=//*[@name='phone' and @value='{$cust_phone}']
...    txt_box_birthday=xpath=//*[@name='dob' and @value='{$cust_dob}']
...    txt_phone=xpath=//*[@name='phone']
...    radio_gender_female=xpath=//*[@name='gender' and @value='2']
...    radio_gender_male=xpath=//*[@name='gender' and @value='1']
...    radio_gender_none=xpath=//*[@name='gender' and @value='3']
...    radio_lang_en=xpath=//*[@name='language' and @value='en']
...    radio_lang_th=xpath=//*[@name='language' and @value='th']
...    btn_edit=xpath=//*[text()='${my_account_page.edit}']
...    rdo_checked_gender=xpath=//*[@name='gender' and @checked]
...    lbl_phone_err_msg=xpath=//*[@name='phone']//following-sibling::div[text()='${error_message.my_account_page.phone_err_start_0}']

...    lbl_first_name_require=xpath=//*[@name='firstname']/following-sibling::div[contains(text(),'${error_message.my_account_page.required_field}')]
...    lbl_last_name_require=xpath=//*[@name='lastname']/following-sibling::div[contains(text(),'${error_message.my_account_page.required_field}')]
...    lbl_phone_require=xpath=//*[@name='phone']/following-sibling::div[contains(text(),'${error_message.my_account_page.required_field}')]
...    lbl_dob_require=xpath=//*[@name='dob']/following-sibling::div[contains(text(),'${error_message.my_account_page.required_field}')]

## address box
...    lbl_default_shipping=xpath=//*[text()='${my_account_page.default_shipping_address}']//preceding::div[1]
...    lbl_default_billing=xpath=//*[text()='${my_account_page.default_billing_address}']//preceding::div[1]
...    bnt_add_new_address=xpath=(//a[@to="/account/address/new"])[1]
...    lbl_address_name=xpath=//*[starts-with(@id,'inf-viewAddressNameOnCheckout-{$id}')]
...    lbl_customer_name=xpath=//*[starts-with(@id,'inf-viewAddressCustomerNameOnCheckout-{$id}')]
...    lbl_customer_phone=xpath=//*[starts-with(@id,'inf-viewAddressTelephoneOnCheckout-{$id}')]
...    lbl_address_line1=xpath=//*[starts-with(@id,'inf-viewAddressLine1OnCheckout-{$id}')]
...    lbl_address_line2=xpath=//*[starts-with(@id,'inf-viewAddressLine2OnCheckout-{$id}')]

...    lbl_lastest_address_name=xpath=(//*[starts-with(@id,'inf-viewAddressNameOnCheckout')])[last()]
...    lbl_lastest_customer_name=xpath=(//*[starts-with(@id,'inf-viewAddressCustomerNameOnCheckout')])[last()]
...    lbl_lastest_customer_phone=xpath=(//*[starts-with(@id,'inf-viewAddressTelephoneOnCheckout')])[last()]
...    lbl_lastest_address_line1=xpath=(//*[starts-with(@id,'inf-viewAddressLine1OnCheckout')])[last()]
...    lbl_lastest_address_line2=xpath=(//*[starts-with(@id,'inf-viewAddressLine2OnCheckout')])[last()]

...    address_info_section=xpath=//*[@id="checkout-address"]
...    btn_delete_all=xpath=//a[starts-with(@to,'/account/address/edit/')]/../parent::div/div[text()='${my_account_page.address_book.delete}']
...    btn_random_address=xpath=(//a[starts-with(@to,'/account/address/edit/')])[{$random}]
...    btn_random_more=xpath=(//a[starts-with(@to,'/account/address/edit/')]/../../parent::div)[{$random}]
...    btn_random_edit=xpath=(//a[starts-with(@to,'/account/address/edit/')])[{$random}]
...    btn_random_delete=xpath=(//a[starts-with(@to,'/account/address/edit/')]/../following-sibling::div)[{$random}]
...    btn_more=xpath=//a[starts-with(@to,'/account/address/edit/{$id}')]/../../parent::div
...    btn_delete=xpath=//a[starts-with(@to,'/account/address/edit/{$id}')]/../parent::div/div[text()='${my_account_page.address_book.delete}']
...    btn_more_action=xpath=//a[starts-with(@to,'/account/address/edit/')]/../../parent::div
...    msg_firstname_require=xpath=//input[@name="firstName"]/following::p[text()='${error_message.address_book.require_field}']
...    msg_lastname_require=xpath=//input[@name="lastName"]/following::p[text()='${error_message.address_book.require_field}']
...    msg_telephone_require=xpath=//input[@name="phone"]/following::p[text()='${error_message.address_book.require_field}']
...    msg_address_line_require=xpath=//input[@name="address"]/following::p[text()='${error_message.address_book.require_field}']
...    msg_postcode_require=xpath=//input[@name="postcode"]/following::p[text()='${error_message.address_book.require_field}']
...    msg_region_require=xpath=//*[@name="province"]/following::p[text()='${error_message.address_book.require_field}']
...    msg_district_require=xpath=//*[@name="district"]/following::p[text()='${error_message.address_book.require_field}']
...    msg_subdistrict_require=xpath=//*[@name="subdistrict"]/following::p[text()='${error_message.address_book.require_field}']

...    btn_add_tax_invoice=xpath=(//div[@class='react-switch-bg'])[2]
...    txt_cardId=xpath=//input[@name='cardID']
...    txt_vat=xpath=//input[@name='vat_id']
...    chk_default_shipping=xpath=(//div[@class='react-switch-bg'])[1]
...    chk_default_billing=xpath=(//div[@class='react-switch-bg'])[3]
...    chk_default_shipping_selected=xpath=//div[@class='_3dQ1i _1Q-g3']/../following-sibling::span[text()='${overview_page.default_shipping_address}']
...    rdo_default_address=xpath=//*[text()='${my_account_page.default_shipping_address}']/preceding-sibling::span//div


# create and edit address 
...    txt_address_name=xpath=//input[@name='addressName']
...    txt_address_firstname=xpath=//input[@name='firstName']
...    txt_address_lastname=xpath=//input[@name='lastName']
...    txt_address_telephone=xpath=//input[@name='phone']
...    txt_address_building=xpath=//input[@name='building']
...    txt_address_line=xpath=//input[@name='address']
...    txt_address_postcode=xpath=//input[@name='postcode']
...    ddl_address_region=xpath=//select[@name='region_id']
...    ddl_address_district=xpath=//select[@name='district_id']
...    ddl_address_subdistrict=xpath=//select[@name='subdistrict_id']
...    btn_address_save=xpath=//*[text()='${my_account_page.save_address}']
...    btn_address_discard=xpath=//*[text()='${my_account_page.discard}']
...    lbl_address_phone_err_msg=xpath=//*[@name='phone']//following::p[text()='${error_message.my_account_page.phone_err_start_0}']
