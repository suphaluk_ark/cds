*** Variables ***
&{dictCheckoutPage}
...    btn_guest_login=xpath=//a[@to='/guest-login']
...    shipping_to_address=(//*[@alt='check mark icon'])[1]
...    pick_at_store=(//*[@alt='check mark icon'])[2]
...    pick_at_skybox=css=#rdo-addDeliveryOption-pickupAtSkybox input[name=delivery-option]
#...    default_address=css=div#checkout-address
...    btn_shipping_address=xpath=(//div[@id='checkout-address']/..//button)[1]
...    btn_billing_address=xpath=(//div[@id='checkout-address']/..//button)[2]
...    checkout_page=id=checkout-page
...    btn_continue_payment=xpath=//*[@id='btn-viewPayment']
...    btn_continue_shopping=xpath=//a[@to='/']//div
...    iframe_credit_card=xpath=//*[@data-testid='credit-card-frame']
...    iframe_credit_card_installment=xpath=//*[@data-testid="inf-viewBankPromotion-CreditCardFrame"]
...    btn_pay_now=xpath=//*[text()='${checkout_page.continue_payment_2}']
...    modal_address=xpath=//div[@id='address-modal-list']//div[@class='_1_PUC']
...    btn_credit_card=css=div#rdo-addPaymentMethod-payment_service_fullpayment
...    btn_cod=css=div#rdo-addPaymentMethod-cashondelivery
...    btn_transfer=css=div#rdo-addPaymentMethod-payment_service_bank_transfer
...    payment_page=//*[@name='viewport']//following::*[contains(text(),'${payment_page.title}')]
...    group_payment=payment_methods
...    value_credit=payment_service_fullpayment
...    value_cod=cashondelivery
...    value_transfer=payment_service_bank_transfer
...    value_ipp=payment_service_installment
...    cod_group=cod-service-group
...    cod_cash=CASH
...    cod_credit=CREDITCARD
...    btn_confirm_cod=xpath=//div[text()='${checkout_page.confirm_order}']
...    txt_payment_success=xpath=//*[text()='${thankyou_page.msg_thankyou}']
...    txt_payment_fail=xpath=//*[text()='${thankyou_page.msg_error}']
...    lbl_order_id=xpath=//*[@data-testid="inf-viewThankyouPageOnOrderDetail-orderId"]
...    lbl_order_id_prod=xpath=//span[contains(.,'CO')]
...    lbl_select_store=xpath=//*[@data-testid="store-item"]//*[contains(text(),'{$store}')]
...    modal_store=xpath=//div[@title='{$store}']
...    btn_select_this_store=xpath=//*[text()='${checkout_page.select_this_store}']
...    bank_service_group=bank-service-group
...    bbl_visa_100=1
...    bbl_visa_500=2
...    scb_visa_10=4
...    kbank_10=6
...    ddl_bank_group=xpath=//select[@data-testid='sel-addPaymentMethodBankTransferOnAgentsName']
...    ddl_payment_channel=xpath=//select[@data-testid='sel-addPaymentMethodBankTransferOnAgentsChannel']
...    txt_phone=css=input[name='telephone']
...    txt_customer_firstname=css=input[name='firstname']
...    txt_customer_lastname=css=input[name='lastname']
...    txt_customer_email=css=input[name='email']
...    txt_customer_phone=css=input[name='telephone']
...    txt_shipping_building=css=input[name='building']
...    txt_shipping_address=css=input[name='address_line']
...    txt_shipping_postcode=css=input[name='postcode']
...    txt_member_email=xpath=//input[@type='email']
...    txt_member_password=css=input[name="password"][placeholder="${guest_login_page.password_place_holder}"]
...    btn_member_login=xpath=//div[@id="app"]//following::button[text()="${guest_login_page.login}"]
...    home_page_id=id=app
...    mini_cart_no=xpath=//a[@to="/cart"]//following::span[text()="{$cart_info}"]
...    mini_cart_ico=xpath=//a[@to="/cart"]
...    btn_checkout_full_redeem=xpath=(//*[text()='${checkout_page.continue_payment_2}'])[2]
...    shipping_fam_80=xpath=//div//*[contains(text(),'{$store}')]
...    shipping_fam_80_prod=xpath=//div/*[text()='{$store}']/../div[text()='80']
...    region=css=select[name='region_id']
...    subdistrict=css=select[name='subdistrict_id']
...    district=css=select[name='district_id']
...    rdo_cod=xpath=//div[contains(@id,'cashondelivery')]
...    cod_value=xpath=//input[@value='cashondelivery']
...    label_cod=xpath=//*[text()='${payment_page.payment_cod}']
...    label_credit_card=//*[text()='${payment_page.credit_card}']
...    label_bank_transfer=//*[text()='${payment_page.payment_banktransfer}']
...    label_line_up=//*[text()='${payment_page.payment_linepay}']
...    other_discount_thanku=xpath=(//div[text()='${cart_page.other_discount}']/..//div)[2]
...    lbl_ecoupon_thanku=xpath=(//div[text()='{$coupon}']/..//div)[2]
...    pickup_at_skybox_branches=xpath=//div[@id="rdo-addStore-44"]
...    btn_pickup_at_skybox_branches=xpath=//button[@id="btn-addStore-44"]
...    verify_product_sku_name=xpath=//div[@id="app"]//following::div[text()="{$product_sku_name}"]
...    txt_BBL_Platinum=xpath=//div[@id="layout-checkout"]//following::span[text()="${credit_card_list.BLL}"]

...    lbl_delivery_page=xpath=(//div[text()='${checkout_page.delivery_detail}'])[2]
...    standard_day_delivery=xpath=//*[@data-testid='rdo-addShippingMethodFee-main-package-options-standard']  

...    same_day_delivery=xpath=//*[@id="rdo-addShippingMethod-same_day"]//input
...    next_day_delivery=xpath=//*[@data-testid="rdo-addShippingMethodFee-main-package-options-next_day"]
...    lbl_standard_day_delivery_price=xpath=//div[@id="rdo-addShippingMethod-standard"]//div[text()="฿"]
...    lbl_same_day_delivery_price=xpath=//div[@id="rdo-addShippingMethod-same_day"]//div[text()="฿"]
...    lbl_next_day_delivery_price=xpath=//div[@id="rdo-addShippingMethod-next_day"]//div[text()="฿"]
...    lbl_3_hours_delivery_price=xpath=//div[@id="rdo-addShippingMethod-ship_from_store"]//div[text()="฿"]
...    lbl_product_quantity=xpath=//span[contains(@id,"inf-viewQtyOnOrderSummary")]

...    lbl_customer_name=css=#inf-viewCustomer-name
...    lbl_customer_email=css=#inf-viewCustomer-email
...    lbl_customer_telephone=css=#inf-viewCustomer-telephone
...    lbl_shipping_address_name=css=#inf-viewAddressNameOnCheckout-{$address_id}
...    lbl_shipping_customer_name=css=#inf-viewAddressCustomerNameOnCheckout-{$address_id}
...    lbl_shipping_telephone=css=#inf-viewAddressTelephoneOnCheckout-{$address_id}
...    lbl_shipping_address_no=css=#inf-viewAddressLine1OnCheckout-{$address_id}
...    lbl_shipping_full_address=css=#inf-viewAddressLine2OnCheckout-{$address_id}

...    lbl_subtotal=css=#inf-viewPrice-subtotal
...    lnk_edit_bag=css=#lnk-editCart
...    chk_gift_wrapping=css=#chk-addGift
...    lbl_product_qty=//span[@id="inf-viewQtyOnOrderSummary-{$product_sku}"]
...    lbl_gift_wrapping_fee=css=#inf-viewPrice-gift_wrapping_fee
...    lbl_shipping_delivery_fee=css=#inf-viewPrice-delivery
...    lbl_grand_total=css=#inf-viewPrice-grand_total
...    lbl_delivery_message=xpath=//div[@id="checkout-page"]//div[text()="{$message}"]
...    lnk_edit_shipping_address=css=#btn-editAddress
...    lbl_price_promo_code=xpath=//div[@id="inf-viewPricePromocode-{$coupon}"]
...    txt_ktc=xpath=//div[@id="layout-checkout"]//following::span[text()='${payment_page.ktc}']

...    btn_view_details=//*[text()='${checkout_page.view_details}']
...    btn_view_detail=//*[text()='${checkout_page.view_detail}']

...    btn_bank_installment=//*[@data-testid="btn-editBankPromotion-ChangeInstallment"]//*[text()='{$bank}']
...    ddl_plan_installment=//*[@data-testid="field-installmentPlanId"]
...    ddl_items_plan_installment=//*[@data-testid="field-installmentPlanId"]//option[contains(.,'{$months}')]

### Redeem point ####
...    lbl_connect_t1c=xpath=//label[text()='${checkout_page.connect_t1c}']
...    btn_connect_t1c=xpath=//button[text()='${checkout_page.connect_t1c}']
...    btn_login_t1c=css=button[type='submit']
...    txt_email=css=label>input[type='email']
...    txt_password=css=label>input[type='password']
...    txt_point=css=input[name='point']
...    ddl_redeem_point=css=div#sel-the1Option
...    btn_apply_point=xpath=//div[text()='${checkout_page.apply_point}']
...    btn_apply_point_login=xpath=//*[text()='${checkout_page.apply_point}']
...    txt_total_redeem_point=xpath=//*[text()='${checkout_page.redeem_point} {$point} ${checkout_page.point} ${checkout_page.baht} {$discount}']
...    txt_redeem_point_summary=xpath=//*[text()='${checkout_page.redeem_point} {$point} pts']
...    txt_discount_redeem=xpath=//*[text()='-${checkout_page.baht}' and text()='{$discount}']
...    order_total=xpath=//div/h4[text()='${checkout_page.order_total}']/..//div
...    ddl_full_redeem=xpath=//span[contains(text(), '${checkout_page.full_redeem}')]
...    btn_hide_detail=id=btn-viewShowHideSummary

##thank u page
...    lbl_payment_type=xpath=//*[@data-testid="inf-viewThankyouPageOnOrderDetail-paymentMethod"]
...    lbl_order_status=xpath=//*[@data-testid="inf-viewThankyouPageOnOrderDetail-orderStatus"]
...    order_total_thank_u=xpath=//*[@data-testid='inf-viewThankyouPageOnOrderSummary-grandTotal']
...    family_pickup=xpath=(//h3[text()='${web_common.pickup_location}']/..//div[contains(.,'{$store}')])[1]
...    btn_repayment_now=xpath=//div[text()='${thankyou_page.re_payment_now}']
...    txt_verify_delivery_icon=xpath=//*[@id="lst-viewCheckoutStep-1"][1]
...    txt_verify_payment_icon=xpath=//*[@id="lst-viewCheckoutStep-2"][1]
...    txt_verify_complete_icon=xpath=//*[@id="lst-viewCheckoutStep-3"][1]
...    txt_verify_bank_transfer=xpath=//*[@id='inf-viewOrderInfo-payment_type']
...    txt_verify_order_title=css=[data-testid=inf-viewThankyouPageOnOrderDetail-orderId]
...    txt_verify_order_date=css=[data-testid=inf-viewThankyouPageOnOrderDetail-createdDate]
...    txt_verify_phone_number=xpath=//*[@id='inf-viewOrderInfo-payment_type']//following::div[contains(text(),'{$customer_phone}')][1]
...    btn_apply_redeem=id=btn-addRedeemPoint-t1

...    windows_checkout_locator=${web_common.windows_locator_checkout_title_cds}
...    btn_login_minicart=xpath=//div[@id="mini-cart"]//following::div[text()="${guest_login_page.login}"]
...    txt_login_box_1=xpath=//div[@id="layout-checkout"]//following::div[text()="${checkout_page.txt_log_in_box.member}"]
...    txt_login_box_2=xpath=//div[@id="layout-checkout"]//following::a[text()="${checkout_page.txt_log_in_box.login}"]
...    txt_login_box_3=xpath=//div[@id="layout-checkout"]//following::div[text()="${checkout_page.txt_log_in_box.checkout}"]

...    txt_card_number=xpath=//*[@id="cardNumber"]
...    txt_card_holder=xpath=//*[@id='cardName']
...    txt_card_expire_date=xpath=//*[@id='cardExpiredDate']
...    txt_card_cvv=xpath=//*[@id='cardCVV']
...    ckb_save_card=xpath=//*[@id='saveCard']
...    txt_save_card=xpath=//*[@data-testid='btn-editSavedCreditCardListItem-OnSetSelectCard']

...    rdo_2_hour_pickup=xpath=//*[@data-testid='inf-viewactiveCircleOnPackageHeader-today_pickup']
...    rdo_standard_pickup=xpath=//*[@data-testid='inf-viewactiveCircleOnPackageHeader-future_day_pickup']
...    lbl_shipping_fee=id=inf-viewPrice-delivery

#    New design for Omnichannel
...    icon_user_location=css=#checkout-enable-user-location-icon
...    btn_user_location=css=#checkout-enable-user-location-icon ~ p
...    txt_StoreNameOrLocation=xpath=//input[@id='txt-searchStore']
...    btn_postcode_arrow=xpath=//div[input[@id='txt-searchStore']]/following-sibling::*
...    btn_home_delivery=xpath=//div[./div/img[contains(@src, 'delivery.svg')]]
...    btn_click_n_collect=xpath=//div[./div/img[contains(@src, 'pick-up')]]
...    btn_view_more=xpath=//*[@data-testid='view-more-btn']
...    rdo_home_delivery_option=xpath=//strong[text()='${checkout_page.home_delivery}']
...    rdo_pickup_at_store_option=xpath=//strong[text()='${checkout_page.click_and_collect}']
...    txt_phone_delivery_option=css=[name='form-phone-input']
...    lbl_phone_number_info=css=[data-testid='txt-viewPhoneForm-infoPart'] p
...    btn_select_store=xpath=//*[text()='{$store}']/ancestor::div[@data-testid='store-detail']//button
...    rdo_click_and_collect=xpath=//*[@data-testid='inf-viewPackageHeaderOnCheckoutPackage-{$delivery_option}']
...    lbl_number_of_items_store=xpath=//*[text()='{$store}']/ancestor::div[@data-testid='store-item']//p[@data-testid='additional-text']
...    lbl_number_of_items_pickup_after_selected=//*[@data-testid='inf-viewactiveCircleOnPackageHeader-today_pickup']/preceding::div[contains(text(),'SELECT OPTION')]
...    lbl_number_of_items_standard_pickup_after_selected=//*[@data-testid='inf-viewactiveCircleOnPackageHeader-today_pickup']/preceding::div[contains(text(),'SELECT OPTION')]
...    txt_coupon_price=xpath=//*[contains(text(),'{$coupon}')]/following-sibling::*
...    btn_pay_now_cancel_payment=xpath=//*[@data-testid='inf-viewThankyouPage-repaymentNowButton']
...    txt_title_paymentFailed=xpath=//div[./div[@data-testid='inf-viewThankyouPage-repaymentNowButton']]/preceding-sibling::div/div
...    payment_method_container=xpath=//*[@data-testid='repayment-container']
...    txt_orderNumber_paymentFailed=css=div[data-testid=inf-viewThankyouPageOnOrderDetail-orderId]
...    txt_createdDate_paymentFailed=css=div[data-testid=inf-viewThankyouPageOnOrderDetail-createdDate]
...    txt_paymentStatus_paymentFailed=css=div[data-testid=inf-viewThankyouPageOnOrderDetail-orderStatus]>span
...    txt_paymentType_paymentFailed=css=div[data-testid=inf-viewThankyouPageOnOrderDetail-paymentMethod]
...    lbl_repayment_page=xpath=(//*[@data-testid='repayment-container']//*[text()='${checkout_page.repayment}'])[2]
...    product_view_container=xpath=//*[@data-testid='inf-viewPackageItemsOnCheckoutPackage-{$sku}']
...    rad_3_hour_delivery=css=[id='rdo-addShippingMethod-ship_from_store'] div input