*** Variables ***
${btn_login}                        xpath=//button[text()='${mobile_web_common.btn_login}']
${btn_logout}                       xpath=//div[@id='account-button']//a[text()='${web_common.logout}']
${txt_email}                        xpath=//*[@name='customerEmail']
${txt_password}                     xpath=//*[@name='customerPassword']
${btn_submit}                       xpath=//button[@type='submit']
${btn_facebook}                     xpath=//*[contains(text(),"${guest_login_page.login_facebook}")]
${txt_forgot_password}              xpath=//a[@to='/user/forgot-password']
${span_running_text}                xpath=(//span[contains(text(),'${home_page.running_span}')])[3]
${btn_register}                     xpath=//a[@to='/register']

${ddl_switch_language}              xpath=//div[@id='switch-language']
${lbl_language}                     xpath=//span[text()='{$language}']
${btn_store_location}               xpath=//a[@href='/th/store-location']
${ico_minicart}                     xpath=//div[@id='mini-cart']
${ico_wishlist}                     xpath=//a[@to='wishlist']

${lbl_herobanner}                   xpath=//div[@class='swiper-pagination swiper-pagination-clickable swiper-pagination-bullets']//span

${err_required_field_1}             xpath=//*[@name='customerEmail']//../div/div[2]
${err_required_field_2}             xpath=//*[@name='customerPassword']//../div/div[2]
${err_login_failed}                 xpath=(//a[@to='/user/forgot-password']//preceding::div//label)[2]
${btn_viewhome}                     xpath=//a[@id='lnk-viewHome']
${msg_error_email}                  xpath=//*[@name='customerEmail']//preceding::div[text()='{$error_msg}']

${btn_mini_cart}                    xpath=//a[@to='/cart']
${btn_view_cart}                    id=lnk-viewCartOnMiniCart
${lbl_view_cart_guest}              xpath=//a[@id='lnk-viewCartOnMiniCart']/div[text()='${web_common.view_cart_guest}']
${lbl_view_cart_member}             xpath=//a[@id='lnk-viewCartOnMiniCart']/div[text()='${web_common.view_cart_member}']
${product_mini_cart}                xpath=(//label[@data-product-id='{$skunumber}' and @data-product-name="{$product_name}"])[2]
${count_mini_cart}                  css=#mini-cart>div>div>span
${msg_deliver}                      //*[@id="rdo-addShippingMethod-standard"]/div[2]
${lbl_grand_total}                  id=lnk-viewPriceSubtotalOnMiniCart
${lbl_quantity}                     xpath=//div[@id='inf-viewQty-{$product_sku}']

${btn_delete_product}               xpath=//div[starts-with(@id,'btn-removeCart')]
${txt_count_mini_cart}              xpath=//a[@to='/cart']//span
${loading_mini_cart}                css=div.ulX0u
${lbl_empty_mini_cart}              xpath=//label[text()='${web_common.empty_cart}']
${btn_my_orders}                    xpath=//div/a[@to='/account/orders']
${btn_account}                      css=#account-button

${btn_order_tracking}               id=order-tracking-buttin
${lbl_email_tracking}               xpath=//*[@name='customerEmail']
${lbl_order_tracking}               xpath=//input[@name='orderNumber']
${btn_track_order}                  //button[contains(.,'${web_common.track_order}')]
${btn_my_account}                   //a[@to='/account/overview']

${lbl_account_name}                 //a[@id="btn-viewCustomerMenuOnMainHeader"]//*[contains(text(),"{$account_name}")]
${lbl_welcome}                      xpath=(//div[starts-with(text(),'${success_message.login.welcome}')])[2]
${lbl_login_popup}                  xpath=//p[text()='${web_common.login}']
${english_flag_icon}                xpath=//img[@src='/icons/en-flag.png']
${thai_flag_icon}                   xpath=//img[@src='/icons/th-flag.png']