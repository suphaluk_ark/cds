*** Variables ***
#please do not follow this format anymore
${popup}    css=#btn-popup-close
${search_box}    xpath=(//input[@id='txt-searchProductOnSearchBar'])[2]
${lbl_product_list}    xpath=//div[@data-product-id='{$product}']
${lbl_product_list_name}    xpath=//div[@data-product-name="{$product_name}"]
${homepage_title_locator}    ${windows_locator_title}
${home_page_title}    xpath=//title[text()='${windows_locator_title}']
${btn_search_close}    xpath=(//input[@id='txt-searchProductOnSearchBar'])[2]//following-sibling::a
${homepage_bestseller_section}    xpath=//h2[text()='${homepage.section.best_seller}']
${homepage_hotdeal_section}    xpath=//h2[text()='${homepage.section.hotdeal}']
${homepage_hotdeal_list}    xpath=//div[.='${homepage.section.hotdeal}']//following-sibling::div//div[contains(@class, 'trackProductClick')]
${homepage_hotdeal_item}    xpath=//div[.='${homepage.section.hotdeal}']//following-sibling::div//h4[text()="{product_name}"]
${btn_x_on_search_box}    xpath=(//input[@id='txt-searchProductOnSearchBar'])[2]/following-sibling::*/*[@viewBox]
${logo_central}    xpath=//a[@data-testid='mnu-viewMenu-hamburger']/../..//a[@to='/']
${lbl_language_display}    xpath=//div[@id="switch-language"]//span[1]
${lbl_brand}    xpath=//div[@data-testid='mnu-viewMenuOnmobile-brand']

#Please create new locator for home page here (using data dict)
*** Variables ***
&{dictHomepage}
...    lbl_homepage=id=layout
