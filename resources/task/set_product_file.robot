*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot
Library    ${CURDIR}/../../etc/FileOperation.py
Library    ${CURDIR}/../../etc/queryAvailableProduct.py

*** Tasks ***
Set Product Data To File
    [Tags]    set_product_api
    Generate product list sku    product_query_lt_600    ${ENV.lower()}    ${language.lower()}
    Generate product list sku    product_query_gt_700    ${ENV.lower()}    ${language.lower()}    >700
