*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot

*** Variables ***
${product_data_file_path}    ${testdata_filepath}${ENV.lower()}${/}product_data.yaml

*** Keywords ***
Task - Set Product Special Price To Price
    [Arguments]    ${sku_list}
    FOR    ${sku}    IN    @{sku_list}
        ${result}=    Set Variable    ${${sku}}
        ${sale_price}=    Run Keyword And Ignore Error    Set Variable    ${${sku}}[special_price]
        ${sale_price}=    Set Variable If    'FAIL' == '${sale_price[0]}'    ${${sku}}[price]
        ...    ${${sku}}[special_price]
        Set To Dictionary    ${result}    ${attribute_field}[price]    ${sale_price}
        ${result}=    Convert To Dictionary    ${result}
        FileOperation.Write Product Data To File    ${attribute_field}[sku]    ${product_data_file_path}    ${result}
    END

*** Tasks ***
Create Query Product Data File
    [Tags]    query_product
    set_product_file.Set Product Dictionary Data To File    ${product_query_lt_600}    ${product_data_file_path}    ${store}
    Import Variables    ${product_data_file_path}
    Task - Set Product Special Price To Price    ${product_query_lt_600}
    set_product_file.Set Product Dictionary Data To File    ${product_query_gt_700}    ${product_data_file_path}    ${store}
    Import Variables    ${product_data_file_path}
    Task - Set Product Special Price To Price    ${product_query_gt_700}