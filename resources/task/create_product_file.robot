*** Settings ***
Resource    ${CURDIR}/../../keywords/web/web_imports.robot

*** Variables ***
${product_data_file_path}    ${testdata_filepath}${ENV.lower()}${/}product_data.yaml

*** Keywords ***
Task - Set Product Special Price To Price
    [Arguments]    ${sku_list}
    FOR    ${sku}    IN    @{sku_list}
        ${result}=    Set Variable    ${${sku}}
        ${sale_price}=    Run Keyword And Ignore Error    Set Variable    ${${sku}}[special_price]

        ${sale_price}=    Set Variable If    'FAIL' == '${sale_price[0]}'    ${${sku}}[price]
        ...    'PASS' == '${sale_price[0]}' and ${${sku}}[special_price] > ${${sku}}[price]    ${${sku}}[price]    # 21 DEC 2020 Added logic for one project - price service
        ...    ${${sku}}[special_price]

        Set To Dictionary    ${result}    ${attribute_field}[price]    ${sale_price}
        ${result}=    Convert To Dictionary    ${result}
        FileOperation.Write Product Data To File    ${attribute_field}[sku]    ${product_data_file_path}    ${result}
    END

*** Tasks ***
create product data file and get customer profile
    set_product_file.Set Product Dictionary Data To File    ${product_sku_list}    ${product_data_file_path}    ${store}
    Import Variables    ${product_data_file_path}
    Task - Set Product Special Price To Price    ${product_sku_list}